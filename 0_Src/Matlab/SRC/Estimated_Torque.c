/*
 * Trial License - for use to evaluate programs for possible purchase as
 * an end-user only.
 *
 * File: Estimated_Torque.c
 *
 * Code generated for Simulink model 'Estimated_Torque'.
 *
 * Model version                  : 1.4937
 * Simulink Coder version         : 9.2 (R2019b) 18-Jul-2019
 * C/C++ source code generated on : Mon Mar 16 09:54:40 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Infineon->TriCore
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Estimated_Torque.h"
#include "Estimated_Torque_private.h"

/* External inputs (root inport signals with default storage) */
ExtU_Estimated_Torque_T Estimated_Torque_U;

/* External outputs (root outports fed by signals with default storage) */
ExtY_Estimated_Torque_T Estimated_Torque_Y;

/* Real-time model */
RT_MODEL_Estimated_Torque_T Estimated_Torque_M_;
RT_MODEL_Estimated_Torque_T *const Estimated_Torque_M = &Estimated_Torque_M_;

/* Model step function */
void Estimated_Torque_step(void)
{
  /* Outport: '<Root>/torque' incorporates:
   *  Gain: '<S1>/P_1_5'
   *  Inport: '<Root>/OuterLoopControl'
   *  Product: '<S1>/Product'
   *  Product: '<S1>/Product1'
   *  Sum: '<S1>/Add1'
   *  Sum: '<S1>/DeltaL'
   */
  Estimated_Torque_Y.torque_f.Est_torque =
    ((Estimated_Torque_U.OuterLoopControl_i.Id -
      Estimated_Torque_U.OuterLoopControl_i.Iq) *
     Estimated_Torque_U.OuterLoopControl_i.Foc_Idq_Real +
     Estimated_Torque_U.OuterLoopControl_i.FluxM) * (1.5F *
    Estimated_Torque_U.OuterLoopControl_i.PolePair *
    Estimated_Torque_U.OuterLoopControl_i.Foc_Idq_Imag);
}

/* Model initialize function */
void Estimated_Torque_initialize(void)
{
  /* (no initialization code required) */
}

/* Model terminate function */
void Estimated_Torque_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
