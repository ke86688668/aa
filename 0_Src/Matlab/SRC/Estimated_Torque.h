/*
 * Trial License - for use to evaluate programs for possible purchase as
 * an end-user only.
 *
 * File: Estimated_Torque.h
 *
 * Code generated for Simulink model 'Estimated_Torque'.
 *
 * Model version                  : 1.4937
 * Simulink Coder version         : 9.2 (R2019b) 18-Jul-2019
 * C/C++ source code generated on : Mon Mar 16 09:54:40 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Infineon->TriCore
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Estimated_Torque_h_
#define RTW_HEADER_Estimated_Torque_h_
#ifndef Estimated_Torque_COMMON_INCLUDES_
# define Estimated_Torque_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* Estimated_Torque_COMMON_INCLUDES_ */

#include "Estimated_Torque_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* External inputs (root inport signals with default storage) */
typedef struct {
  OuterLoopControl OuterLoopControl_i; /* '<Root>/OuterLoopControl' */
} ExtU_Estimated_Torque_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  torque torque_f;                     /* '<Root>/torque' */
} ExtY_Estimated_Torque_T;

/* Real-time Model Data Structure */
struct tag_RTM_Estimated_Torque_T {
  const char_T * volatile errorStatus;
};

/* External inputs (root inport signals with default storage) */
extern ExtU_Estimated_Torque_T Estimated_Torque_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_Estimated_Torque_T Estimated_Torque_Y;

/* Model entry point functions */
extern void Estimated_Torque_initialize(void);
extern void Estimated_Torque_step(void);
extern void Estimated_Torque_terminate(void);

/* Real-time Model object */
extern RT_MODEL_Estimated_Torque_T *const Estimated_Torque_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'Estimated_Torque'
 * '<S1>'   : 'Estimated_Torque/Estimated_Torque_20200311'
 */
#endif                                 /* RTW_HEADER_Estimated_Torque_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
