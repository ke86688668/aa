/**
 * \file Ifx_DeviceCache.c
 * \brief Device cache
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */
#include "Ifx_DeviceCache.h"
#include <string.h>

/** Force to synchronize the cache with the device for a single register (device register read, 8 bit version)
 *
 * \image html "DeviceCache_readDevice.png"
 *
 * The operation updates the cache.
 * \param cache pointer to the cache driver
 * \param address Register address
 *
 * \return return TRUE in case of success else false
 */
static boolean Ifx_DeviceCache_readDevice8(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address);

/** Force to synchronize the cache with the device for a single register (device register read, 16 bit version)
 *
 * \image html "DeviceCache_readDevice.png"
 *
 * The operation updates the cache.
 * \param cache pointer to the cache driver
 * \param address Register address
 *
 *
 * \return return TRUE in case of success else false
 */
static boolean Ifx_DeviceCache_readDevice16(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address);

/** Write data directly to the device (device register write, 8 bit version)
 *
 * \image html "DeviceCache_writeSingle.png"
 *
 * \param cache pointer to the cache driver
 * \param address Register address
 * \param data Data to be written
 * \return return TRUE in case of success else false
 */
static boolean Ifx_DeviceCache_writeSingle8(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint8 data);
/** Write data directly to the device (device register write, 16 bit version)
 *
 * \image html "DeviceCache_writeSingle.png"
 *
 * \param cache pointer to the cache driver
 * \param address Register address
 * \param data Data to be written
 * \return return TRUE in case of success else false
 */
static boolean Ifx_DeviceCache_writeSingle16(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint16 data);

void Ifx_DeviceCache_enable(Ifx_DeviceCache *cache)
{
	Ifx_DeviceCache *first = cache->line.first;
	if (!first->enabled)
	{ /* Clean up*/
		Ifx_DeviceCache_invalidate(first);
		first->enabled = 0xFFFFFFFF;
		first->writeBackError = 0;
	}
}

boolean Ifx_DeviceCache_isCacheEnabled(Ifx_DeviceCache *cache)
{
	return cache->line.first->enabled != 0;
}

boolean Ifx_DeviceCache_disable(Ifx_DeviceCache *cache)
{
	boolean result;
	Ifx_DeviceCache *first = cache->line.first;
	if (first->enabled)
	{
		Ifx_DeviceCache_invalidate(first);
		first->enabled = 0;
		first->writeBackError = 0;
		result = TRUE;
	}
	else
	{
		result = FALSE;
	}
	return result;
}


boolean Ifx_DeviceCache_getValue8(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint8 *data)
{
    boolean result = TRUE;

    if (!Ifx_DeviceCache_isValid(cache, address))
    {
        result = Ifx_DeviceCache_readDevice8(cache, address);
    }

    if (result)
    {
        *data = ((uint8 *)(cache->buffer))[address];
    }

    return result;
}


boolean Ifx_DeviceCache_getValue16(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint16 *data)
{
    boolean result = TRUE;

    if (!Ifx_DeviceCache_isValid(cache, address))
    {
        result = Ifx_DeviceCache_readDevice16(cache, address);
    }

    if (result)
    {
        *data = ((uint16 *)(cache->buffer))[address];
    }

    return result;
}


boolean Ifx_DeviceCache_init(Ifx_DeviceCache *cache, Ifx_DeviceCache_Config *config)
{
    boolean result = TRUE;
    cache->ItemCount            = config->ItemCount;
    cache->buffer               = config->buffer;
    cache->bufferNoOpMaskActual = config->bufferNoOpMaskActual;
    cache->bufferNoOpMaskDefault= config->bufferNoOpMaskDefault;
    cache->bufferNoOpValue 	    = config->bufferNoOpValue;
    cache->modificationMask     = config->modificationMask;
    cache->validAddress         = config->validAddress;
    cache->volatileRegisters    = config->volatileRegisters;
    cache->actionReadRegisters  = config->actionReadRegisters;
    cache->actionWriteRegisters = config->actionWriteRegisters;
    cache->valid                = config->valid;
    cache->writeBackErrorFlags  = config->writeBackErrorFlags;
    cache->flagArraySize        = config->flagArraySize;
    cache->read                 = config->read;
    cache->write                = config->write;
    cache->writeBack            = config->writeBack;
    cache->deviceDriver         = config->deviceDriver;
    cache->registerWidth        = config->registerWidth;
    cache->line.temp            = config->tempCacheLine;
    cache->volatileEnabled      = 0;
    cache->enabled				= 0xFFFFFFFF;
    cache->writeBackError		= 0;
    cache->line.next            = NULL_PTR;

    if (config->previousCache)
    {
        Ifx_DeviceCache *c;
        uint8            index = 0;

        cache->line.first                = config->previousCache->line.first;
        config->previousCache->line.next = cache;

        c                                = cache->line.first;

        do
        {
            c->line.index = index++;
            c             = c->line.next;
        } while (c);
    }
    else
    {
        cache->line.first = cache;
        cache->line.index = 0;
    }

    /* initialize buffers (one buffer for each cache when in daisy chain) */
    memset(cache->valid, 0, cache->flagArraySize * 4);
    memset(cache->modificationMask, 0, cache->flagArraySize * 4);
    memset(cache->writeBackErrorFlags, 0, cache->flagArraySize * 4);
    memcpy(cache->bufferNoOpMaskActual, cache->bufferNoOpMaskDefault, cache->ItemCount * cache->registerWidth);
    memset(cache->buffer, 0, cache->ItemCount * cache->registerWidth);

    return result;
}


void Ifx_DeviceCache_initConfig(Ifx_DeviceCache_Config *config)
{
    memset(config, 0, sizeof(Ifx_DeviceCache_Config));
}


void Ifx_DeviceCache_invalidate(Ifx_DeviceCache *cache)
{
    Ifx_DeviceCache *first = cache->line.first;
    cache = first;

    do
    {
        uint16 i;

        for (i = 0; i < cache->flagArraySize; i++)
        {   /* Clear all cache valid register flags  */
            /* FIXME [Optimization] data access is currently optimized for 32bit architecture, might require a version optimized for 16 bit architecture */
            cache->valid[i]            = 0;
            cache->modificationMask[i] = 0;

        }

    	cache                               = cache->line.next;
    } while (cache);

}


void Ifx_DeviceCache_invalidateVolatile(Ifx_DeviceCache *cache)
{
    cache = cache->line.first;

    do
    {
        uint16 i;

        for (i = 0; i < cache->flagArraySize; i++)
        {
            cache->valid[i]            &= ~cache->volatileRegisters[i];
            cache->modificationMask[i] &= ~cache->volatileRegisters[i];
        }

    	cache                               = cache->line.next;
    } while (cache);
}


boolean Ifx_DeviceCache_isCachable(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address)
{
    uint32 index  = 1 << (address % 32);
    uint32 offset = address >> 5;
    return ((~cache->volatileRegisters[offset] | cache->line.first->volatileEnabled) & index) != 0;
}


boolean Ifx_DeviceCache_isInitialized(Ifx_DeviceCache *cache)
{
    return cache->buffer != NULL_PTR;
}


boolean Ifx_DeviceCache_isValid(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address)
{
    /* FIXME [Optimization] optimization possible by storing the index and offset in the driver for each register address */
    uint32 index  = 1 << (address % 32);
    uint32 offset = address >> 5;
    return (cache->valid[offset] & index) != 0;
}


boolean Ifx_DeviceCache_setValue8(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint8 data)
{
	Ifx_DeviceCache *first = cache->line.first;
    boolean result = TRUE;
    uint32  index  = 1 << (address % 32);
    uint32  offset = address >> 5;
    boolean writeToCache = TRUE;


    if (first->enabled)
    {
    	first->writeBackError = 0;

    	if (!first->volatileEnabled)
    	{
    		if (cache->actionWriteRegisters[offset] & index)
    		{
    			result = cache->writeBack(cache);
    			if (result)
    			{   /* Write data to buffer, in case the valid bit is set from earlier read, new read will return modified value */
    	            ((uint8 *)(cache->buffer))[address] = data;
    			}
    	    	writeToCache = FALSE;
    		}
    		else
    		{
    			writeToCache = (cache->volatileRegisters[offset] & index) == 0;
    		}

    	}
    	else if (cache->valid[offset] & index)
    	{
        	((uint8*)cache->bufferNoOpMaskActual)[address] = ((uint8*)cache->bufferNoOpMaskDefault)[address];
    	}
    }
    else
    {
    	writeToCache = FALSE;
    }

    if (result)
    {
    	/* FIXME possible optimization: Only write if data differs from valid cached value and not action write register */
        if (writeToCache)
        {
            ((uint8 *)(cache->buffer))[address] = data;
            cache->modificationMask[offset]    |= index;
            cache->valid[offset]               |= index;
        	((uint8*)cache->bufferNoOpMaskActual)[address] = 0;
        }
        else
        {
        	((uint8*)cache->bufferNoOpMaskActual)[address] = 0;
            result &= Ifx_DeviceCache_writeSingle8(cache, address, data);
        	((uint8*)cache->bufferNoOpMaskActual)[address] = ((uint8*)cache->bufferNoOpMaskDefault)[address];
        }
    }

    return result;
}

boolean Ifx_DeviceCache_setValue8Star(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint8 data, uint8 noOpMask)
{
	Ifx_DeviceCache *first = cache->line.first;
    boolean result = TRUE;
    uint32  index  = 1 << (address % 32);
    uint32  offset = address >> 5;
    boolean writeToCache = TRUE;


    if (first->enabled)
    {
    	first->writeBackError = 0;

    	if (!first->volatileEnabled)
    	{
    		if (cache->actionWriteRegisters[offset] & index)
    		{
    			result = cache->writeBack(cache);
    			if (result)
    			{   /* Write data to buffer, in case the valid bit is set from earlier read, new read will return modified value */
    	            ((uint8 *)(cache->buffer))[address] = data;
    			}
    	    	writeToCache = FALSE;
    		}
    		else
    		{
    			writeToCache = (cache->volatileRegisters[offset] & index) == 0;
    		}

    	}
    	else if (cache->valid[offset] & index)
    	{
        	((uint8*)cache->bufferNoOpMaskActual)[address] = ((uint8*)cache->bufferNoOpMaskDefault)[address];
    	}
    }
    else
    {
    	writeToCache = FALSE;
    }

    if (result)
    {
    	/* FIXME possible optimization: Only write if data differs from valid cached value and not action write register */
        if (writeToCache)
        {
            ((uint8 *)(cache->buffer))[address] = data;
            cache->modificationMask[offset]    |= index;
            cache->valid[offset]               |= index;
        	((uint8*)cache->bufferNoOpMaskActual)[address] &= ~noOpMask;
        }
        else
        {
        	((uint8*)cache->bufferNoOpMaskActual)[address] &= ~noOpMask;
            result &= Ifx_DeviceCache_writeSingle8(cache, address, data);
        	((uint8*)cache->bufferNoOpMaskActual)[address] = ((uint8*)cache->bufferNoOpMaskDefault)[address];
        }
    }

    return result;

}


boolean Ifx_DeviceCache_setValue16(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint16 data)
{
	Ifx_DeviceCache *first = cache->line.first;
    boolean result = TRUE;
    uint32  index  = 1 << (address % 32);
    uint32  offset = address >> 5;
    boolean writeToCache = TRUE;


    if (first->enabled)
    {
    	first->writeBackError = 0;

    	if (!first->volatileEnabled)
    	{
    		if (cache->actionWriteRegisters[offset] & index)
    		{
    			result = cache->writeBack(cache);
    			if (result)
    			{   /* Write data to buffer, in case the valid bit is set from earlier read, new read will return modified value */
    	            ((uint16 *)(cache->buffer))[address] = data;
    			}
    	    	writeToCache = FALSE;
    		}
    		else
    		{
    			writeToCache = (cache->volatileRegisters[offset] & index) == 0;
    		}

    	}
    	else if (cache->valid[offset] & index)
    	{
        	((uint16*)cache->bufferNoOpMaskActual)[address] = ((uint16*)cache->bufferNoOpMaskDefault)[address];
    	}
    }
    else
    {
    	writeToCache = FALSE;
    }

    if (result)
    {
    	/* FIXME possible optimization: Only write if data differs from valid cached value and not action write register */
        if (writeToCache)
        {
            ((uint16 *)(cache->buffer))[address] = data;
            cache->modificationMask[offset]    |= index;
            cache->valid[offset]               |= index;
        	((uint16*)cache->bufferNoOpMaskActual)[address] = 0;
        }
        else
        {
        	((uint16*)cache->bufferNoOpMaskActual)[address] = 0;
            result &= Ifx_DeviceCache_writeSingle16(cache, address, data);
        	((uint16*)cache->bufferNoOpMaskActual)[address] = ((uint16*)cache->bufferNoOpMaskDefault)[address];
        }
    }

    return result;
}

boolean Ifx_DeviceCache_setValue16Star(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint16 data, uint16 noOpMask)
{
	Ifx_DeviceCache *first = cache->line.first;
    boolean result = TRUE;
    uint32  index  = 1 << (address % 32);
    uint32  offset = address >> 5;
    boolean writeToCache = TRUE;


    if (first->enabled)
    {
    	first->writeBackError = 0;

    	if (!first->volatileEnabled)
    	{
    		if (cache->actionWriteRegisters[offset] & index)
    		{
    			result = cache->writeBack(cache);
    			if (result)
    			{   /* Write data to buffer, in case the valid bit is set from earlier read, new read will return modified value */
    	            ((uint16 *)(cache->buffer))[address] = data;
    			}
    	    	writeToCache = FALSE;
    		}
    		else
    		{
    			writeToCache = (cache->volatileRegisters[offset] & index) == 0;
    		}

    	}
    	else if (cache->valid[offset] & index)
    	{
        	((uint16*)cache->bufferNoOpMaskActual)[address] = ((uint16*)cache->bufferNoOpMaskDefault)[address];
    	}
    }
    else
    {
    	writeToCache = FALSE;
    }

    if (result)
    {
    	/* FIXME possible optimization: Only write if data differs from valid cached value and not action write register */
        if (writeToCache)
        {
            ((uint16 *)(cache->buffer))[address] = data;
            cache->modificationMask[offset]    |= index;
            cache->valid[offset]               |= index;
        	((uint16*)cache->bufferNoOpMaskActual)[address]&= ~noOpMask;
        }
        else
        {
        	((uint16*)cache->bufferNoOpMaskActual)[address] &= ~noOpMask;
            result &= Ifx_DeviceCache_writeSingle16(cache, address, data);
        	((uint16*)cache->bufferNoOpMaskActual)[address] = ((uint16*)cache->bufferNoOpMaskDefault)[address];
        }
    }

    return result;
}


boolean Ifx_DeviceCache_setVolatileCache(Ifx_DeviceCache *cache, boolean enabled)
{
    /* FIXME [Optimization] possible optimization: merge cache->volatileEnabled and deviceCache->volatileRegisters[offset] into  cache->volatileEnabled[] */
	Ifx_DeviceCache *first = cache->line.first;
    boolean                 result = TRUE;
    if (enabled != (first->volatileEnabled != 0))
    {
        if (enabled)
        {
        	first->writeBackError = 0;
        	Ifx_DeviceCache_invalidateVolatile(first);
        	first->volatileEnabled = 0xFFFFFFFF;
        }
        else
        {
        	result = cache->writeBack(first);
        	Ifx_DeviceCache_invalidateVolatile(first);
        	first->volatileEnabled = 0;
        }
    }
    return result;
}


boolean Ifx_DeviceCache_synchronize8(Ifx_DeviceCache *cache)
{   /* FIXME [Optimization] Enable sending multiple register read in one SPI frame */
    Ifx_DeviceCache_Address address;
    boolean                 result = TRUE;
    Ifx_DeviceCache *deviceCache;
	Ifx_DeviceCache *first = cache->line.first;

	if (first->enabled)
	{
	    for (address = 0; address < cache->ItemCount; address++)
	    {
	        uint32 index  = 1 << (address % 32);
	        uint32 offset = address >> 5;
	        uint32 mask   = cache->validAddress[offset] & (~cache->actionReadRegisters[offset]) & ((~cache->volatileRegisters[offset]) | first->volatileEnabled);

	        uint32 invalid = 0;
	        deviceCache = cache->line.first;
	        do
	        {
	        	invalid |= ~deviceCache->valid[offset];
	            deviceCache                               = deviceCache->line.next;
	        } while (deviceCache);

	        mask &= invalid;

	        /* Update only if valid address and cache invalid. */
	        if (mask & index)
	        {
	            uint8            i = 0;
	            uint8  *data = cache->line.temp;
	            boolean status;
	            status = cache->read(cache->deviceDriver, address, cache->line.temp);

	            if (status)
	            {
	                deviceCache = cache->line.first;
	                do
	                {
	                    ((uint8 *)(deviceCache->buffer))[address] = data[i++];
	                    deviceCache->valid[offset]               |= index;
	                    deviceCache->modificationMask[offset]    &= ~index;
	                	((uint8 *)deviceCache->bufferNoOpMaskActual)[address] = ((uint8 *)deviceCache->bufferNoOpMaskDefault)[address];

	                    deviceCache                               = deviceCache->line.next;
	                } while (deviceCache);
	            }
	            else
	            {
	                deviceCache = cache->line.first;
	                do
	                {
	                    deviceCache->valid[offset]               &= ~index;
	                    deviceCache->modificationMask[offset]    &= ~index;
	                	((uint8 *)deviceCache->bufferNoOpMaskActual)[address] = ((uint8 *)deviceCache->bufferNoOpMaskDefault)[address];

	                    deviceCache                               = deviceCache->line.next;
	                } while (deviceCache);
	                result = FALSE;
	            }
	        }
	    }

	}


    return result;
}


boolean Ifx_DeviceCache_synchronize16(Ifx_DeviceCache *cache)
{
    Ifx_DeviceCache_Address address;
    boolean                 result = TRUE;
    Ifx_DeviceCache *deviceCache;
	Ifx_DeviceCache *first = cache->line.first;

	if (first->enabled)
	{
	    for (address = 0; address < cache->ItemCount; address++)
	    {
	        uint32 index  = 1 << (address % 32);
	        uint32 offset = address >> 5;
	        uint32 mask   = cache->validAddress[offset] & (~cache->actionReadRegisters[offset]) & ((~cache->volatileRegisters[offset]) | first->volatileEnabled);

	        uint32 invalid = 0;
	        deviceCache = cache->line.first;
	        do
	        {
	        	invalid |= ~deviceCache->valid[offset];
	            deviceCache                               = deviceCache->line.next;
	        } while (deviceCache);

	        mask &= invalid;

	        /* Update only if valid address and cache invalid. */
	        if (mask & index)
	        {
	            uint8            i = 0;
	            uint16  *data = cache->line.temp;
	            boolean status;
	            status = cache->read(cache->deviceDriver, address, cache->line.temp);

	            if (status)
	            {
	                deviceCache = cache->line.first;
	                do
	                {
	                    ((uint16 *)(deviceCache->buffer))[address] = data[i++];
	                    deviceCache->valid[offset]               |= index;
	                    deviceCache->modificationMask[offset]    &= ~index;
	                	((uint16 *)deviceCache->bufferNoOpMaskActual)[address] = ((uint16 *)deviceCache->bufferNoOpMaskDefault)[address];

	                    deviceCache                               = deviceCache->line.next;
	                } while (deviceCache);
	            }
	            else
	            {
	                deviceCache = cache->line.first;
	                do
	                {
	                    deviceCache->valid[offset]               &= ~index;
	                    deviceCache->modificationMask[offset]    &= ~index;
	                	((uint16 *)deviceCache->bufferNoOpMaskActual)[address] = ((uint16 *)deviceCache->bufferNoOpMaskDefault)[address];

	                    deviceCache                               = deviceCache->line.next;
	                } while (deviceCache);
	                result = FALSE;
	            }
	        }
	    }

	}

    return result;
}


static boolean Ifx_DeviceCache_readDevice8(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address)
{
    boolean result = TRUE;

	Ifx_DeviceCache *first = cache->line.first;
    uint32  index  = 1 << (address % 32);
    uint32  offset = address >> 5;
    uint8  *data   = cache->line.temp;
    boolean status;
    status = cache->read(cache->deviceDriver, address, cache->line.temp);

    if (status)
    {
        uint8 i = 0;
        cache = cache->line.first;

        do
        {
            ((uint8 *)(cache->buffer))[address] = data[i++];
            cache->valid[offset]               |= (~cache->volatileRegisters[offset] | first->volatileEnabled) & first->enabled & index;
            cache->modificationMask[offset]    &= ~index;
        	((uint8 *)cache->bufferNoOpMaskActual)[address] = ((uint8 *)cache->bufferNoOpMaskDefault)[address];

            cache                               = cache->line.next;
        } while (cache);
    }
    else
    {
        cache->valid[offset]               &= ~index;
        cache->modificationMask[offset]    &= ~index;
    	((uint8 *)cache->bufferNoOpMaskActual)[address] = ((uint8 *)cache->bufferNoOpMaskDefault)[address];
        result = FALSE;
    }

    return result;
}


static boolean Ifx_DeviceCache_readDevice16(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address)
{
    boolean result = TRUE;

	Ifx_DeviceCache *first = cache->line.first;
    uint32  index  = 1 << (address % 32);
    uint32  offset = address >> 5;
    uint16  *data   = cache->line.temp;
    boolean status;
    status = cache->read(cache->deviceDriver, address, cache->line.temp);

    if (status)
    {
        uint8 i = 0;
        cache = cache->line.first;

        do
        {
            ((uint16 *)(cache->buffer))[address] = data[i++];
            cache->valid[offset]               |= (~cache->volatileRegisters[offset] | first->volatileEnabled) & first->enabled & index;
            cache->modificationMask[offset]    &= ~index;
        	((uint16 *)cache->bufferNoOpMaskActual)[address] = ((uint16 *)cache->bufferNoOpMaskDefault)[address];

            cache                               = cache->line.next;
        } while (cache);
    }
    else
    {
        cache->valid[offset]               &= ~index;
        cache->modificationMask[offset]    &= ~index;
    	((uint16 *)cache->bufferNoOpMaskActual)[address] = ((uint16 *)cache->bufferNoOpMaskDefault)[address];
        result = FALSE;
    }

    return result;
}


boolean Ifx_DeviceCache_readAll8(Ifx_DeviceCache *cache)
{
    Ifx_DeviceCache_Address address;
    boolean                 result = TRUE;
    Ifx_DeviceCache *deviceCache;

	Ifx_DeviceCache *first = cache->line.first;

	if (first->enabled)
	{
	    for (address = 0; address < cache->ItemCount; address++)
	    {
	        uint32 index  = 1 << (address % 32);
	        uint32 offset = address >> 5;
	        uint32 mask   = cache->validAddress[offset];

	        /* Update only if valid address and cache invalid. */
	        if (mask & index)
	        {
	            uint8            i = 0;
	            uint8  *data = cache->line.temp;
	            boolean status;
	            status = cache->read(cache->deviceDriver, address, cache->line.temp);

	            if (status)
	            {
	                deviceCache = cache->line.first;
	                do
	                {
	                    ((uint8 *)(deviceCache->buffer))[address] = data[i++];
	                    deviceCache->valid[offset]               |= index;
	                    deviceCache->modificationMask[offset]    &= ~index;
	                	((uint8 *)deviceCache->bufferNoOpMaskActual)[address] = ((uint8 *)deviceCache->bufferNoOpMaskDefault)[address];

	                    deviceCache                               = deviceCache->line.next;
	                } while (deviceCache);
	            }
	            else
	            {
	                deviceCache = cache->line.first;
	                do
	                {
	                    deviceCache->valid[offset]               &= ~index;
	                    deviceCache->modificationMask[offset]    &= ~index;
	                	((uint8 *)deviceCache->bufferNoOpMaskActual)[address] = ((uint8 *)deviceCache->bufferNoOpMaskDefault)[address];

	                    deviceCache                               = deviceCache->line.next;
	                } while (deviceCache);
	                result = FALSE;
	            }
	        }
	    }

	}

    return result;
}


boolean Ifx_DeviceCache_readAll16(Ifx_DeviceCache *cache)
{
    Ifx_DeviceCache_Address address;
    boolean                 result = TRUE;
    Ifx_DeviceCache *deviceCache;
	Ifx_DeviceCache *first = cache->line.first;

	if (first->enabled)
	{
	    for (address = 0; address < cache->ItemCount; address++)
	    {
	        uint32 index  = 1 << (address % 32);
	        uint32 offset = address >> 5;
	        uint32 mask   = cache->validAddress[offset];

	        /* Update only if valid address and cache invalid. */
	        if (mask & index)
	        {
	            uint8            i = 0;
	            uint16  *data = cache->line.temp;
	            boolean status;
	            status = cache->read(cache->deviceDriver, address, cache->line.temp);

	            if (status)
	            {
	                deviceCache = cache->line.first;
	                do
	                {
	                    ((uint16 *)(deviceCache->buffer))[address] = data[i++];
	                    deviceCache->valid[offset]               |= index;
	                    deviceCache->modificationMask[offset]    &= ~index;
	                	((uint16 *)deviceCache->bufferNoOpMaskActual)[address] = ((uint16 *)deviceCache->bufferNoOpMaskDefault)[address];

	                    deviceCache                               = deviceCache->line.next;
	                } while (deviceCache);
	            }
	            else
	            {
	                deviceCache = cache->line.first;
	                do
	                {
	                    deviceCache->valid[offset]               &= ~index;
	                    deviceCache->modificationMask[offset]    &= ~index;
	                	((uint16 *)deviceCache->bufferNoOpMaskActual)[address] = ((uint16 *)deviceCache->bufferNoOpMaskDefault)[address];

	                    deviceCache                               = deviceCache->line.next;
	                } while (deviceCache);
	                result = FALSE;
	            }
	        }
	    }

	}

    return result;

}


boolean Ifx_DeviceCache_writeBack8(Ifx_DeviceCache *cache)
{   /* FIXME [Optimization] Enable sending multiple register write in one SPI frame for daisy chained device*/
    boolean result = TRUE;
	Ifx_DeviceCache *first = cache->line.first;
    if (first->enabled)
    {
        uint32  offset;
        uint32  lastOffset;
        uint8  *data = cache->line.temp;
        Ifx_DeviceCache        *deviceCache;

        first->writeBackError = 0;

        lastOffset = (cache->ItemCount - 1) >> 5;
        offset     = 0;


        while (offset <= lastOffset)
        {
            uint32                  dataModified;
            Ifx_DeviceCache_Address base;

            /* Check if some registers are modified for the given offset */
            dataModified = 0;
            deviceCache  = cache->line.first;

            do
            {
                dataModified |= deviceCache->modificationMask[offset];
            	deviceCache->writeBackErrorFlags[offset] = 0;

                deviceCache   = deviceCache->line.next;
            } while (deviceCache);

            base = (Ifx_DeviceCache_Address)(offset << 5);

            while (dataModified != 0)
            {   /* Some registers are modified */
                sint8 index;

                index = 31 - __clz(dataModified);

                if (index >= 0)
                {
                    uint32                  mask;
                    uint8                   i;
                    Ifx_DeviceCache_Address address;
                    uint32                  indexMask;
                    address       = base + index;
                    indexMask     = 1 << index;
                    dataModified &= ~indexMask;

                    /* Fill the cache line */
                    deviceCache = cache->line.first;
                    i           = 0;
                    mask        = 0;

                    do
                    {
                        if (deviceCache->modificationMask[offset] & indexMask)
                        {
                        	uint8 maskNoOp;
                            maskNoOp = ((uint8*)deviceCache->bufferNoOpMaskActual)[address];
                        	data[i] = (((uint8 *)(deviceCache->buffer))[address] & ~maskNoOp) | ((((uint8*)deviceCache->bufferNoOpValue)[address]) & maskNoOp);
                            mask   |= 1 << i;
                        }
                        else
                        {}

                        i++;
                        deviceCache = deviceCache->line.next;
                    } while (deviceCache);

                    /* Write the data to the device */
                    if (mask != 0) /* FIXME condition is always true */
                    {
                        boolean status;
                        status = cache->write(cache->deviceDriver, mask, address, data);

                        /* Update the cache flags */
                        if (status)
                        {
                            deviceCache = cache->line.first;

                            do
                            {
                                deviceCache->modificationMask[offset] &= ~indexMask;
                            	((uint8 *)deviceCache->bufferNoOpMaskActual)[address] = ((uint8 *)deviceCache->bufferNoOpMaskDefault)[address];
                                deviceCache                            = deviceCache->line.next;
                            } while (deviceCache);
                        }
                        else
                        {
                            deviceCache = cache->line.first;

                            do
                            {
                            	deviceCache->writeBackErrorFlags[offset] |= indexMask & deviceCache->modificationMask[offset];
                                deviceCache->modificationMask[offset] &= ~indexMask;
                            	((uint8 *)deviceCache->bufferNoOpMaskActual)[address] = ((uint8 *)deviceCache->bufferNoOpMaskDefault)[address];
                            	deviceCache->valid[offset]            &= ~indexMask;	/* FIXME should mark only for the corresponding device */
                            	first->writeBackError = 0xFFFFFFFF;

                                deviceCache                            = deviceCache->line.next;
                            } while (deviceCache);

                            result = FALSE;
                            /* FIXME [New feature] Retry on failure */
                        }
                    }
                }
            }

            offset++;
        }
    }

    return result;
}




boolean Ifx_DeviceCache_writeBack16(Ifx_DeviceCache *cache)
{   /* FIXME [Optimization] Enable sending multiple register write in one SPI frame */
    boolean result = TRUE;
	Ifx_DeviceCache *first = cache->line.first;
    if (first->enabled)
    {
        uint32  offset;
        uint32  lastOffset;
        uint16  *data = cache->line.temp;
        Ifx_DeviceCache        *deviceCache;

        first->writeBackError = 0;

        lastOffset = (cache->ItemCount - 1) >> 5;
        offset     = 0;


        while (offset <= lastOffset)
        {
            uint32                  dataModified;
            Ifx_DeviceCache_Address base;

            /* Check if some registers are modified for the given offset */
            dataModified = 0;
            deviceCache  = cache->line.first;

            do
            {
                dataModified |= deviceCache->modificationMask[offset];
            	deviceCache->writeBackErrorFlags[offset] = 0;

                deviceCache   = deviceCache->line.next;
            } while (deviceCache);

            base = (Ifx_DeviceCache_Address)(offset << 5);

            while (dataModified != 0)
            {   /* Some registers are modified */
                sint8 index;

                index = 31 - __clz(dataModified);

                if (index >= 0)
                {
                    uint32                  mask;
                    uint8                   i;
                    Ifx_DeviceCache_Address address;
                    uint32                  indexMask;
                    address       = base + index;
                    indexMask     = 1 << index;
                    dataModified &= ~indexMask;

                    /* Fill the cache line */
                    deviceCache = cache->line.first;
                    i           = 0;
                    mask        = 0;

                    do
                    {
                        if (deviceCache->modificationMask[offset] & indexMask)
                        {
                        	uint16 maskNoOp;
                            maskNoOp = ((uint16*)deviceCache->bufferNoOpMaskActual)[address];
                        	data[i] = (((uint16 *)(deviceCache->buffer))[address] & ~maskNoOp) | ((((uint16*)deviceCache->bufferNoOpValue)[address]) & maskNoOp);
                            mask   |= 1 << i;
                        }
                        else
                        {}

                        i++;
                        deviceCache = deviceCache->line.next;
                    } while (deviceCache);

                    /* Write the data to the device */
                    if (mask != 0) /* FIXME condition is always true */
                    {
                        boolean status;
                        status = cache->write(cache->deviceDriver, mask, address, data);

                        /* Update the cache flags */
                        if (status)
                        {
                            deviceCache = cache->line.first;

                            do
                            {
                                deviceCache->modificationMask[offset] &= ~indexMask;
                            	((uint16 *)deviceCache->bufferNoOpMaskActual)[address] = ((uint16 *)deviceCache->bufferNoOpMaskDefault)[address];
                                deviceCache                            = deviceCache->line.next;
                            } while (deviceCache);
                        }
                        else
                        {
                            deviceCache = cache->line.first;

                            do
                            {
                            	deviceCache->writeBackErrorFlags[offset] |= indexMask & deviceCache->modificationMask[offset];
                                deviceCache->modificationMask[offset] &= ~indexMask;
                            	((uint16 *)deviceCache->bufferNoOpMaskActual)[address] = ((uint16 *)deviceCache->bufferNoOpMaskDefault)[address];
                            	deviceCache->valid[offset]            &= ~indexMask;	/* FIXME should mark only for the corresponding device */
                            	first->writeBackError = 0xFFFFFFFF;

                                deviceCache                            = deviceCache->line.next;
                            } while (deviceCache);

                            result = FALSE;
                            /* FIXME [New feature] Retry on failure */
                        }
                    }
                }
            }

            offset++;
        }
    }

    return result;
}


static boolean Ifx_DeviceCache_writeSingle8(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint8 data)
{
    boolean status;

    uint8  *temp = cache->line.temp;
    uint8 maskNoOp;
    maskNoOp = ((uint8*)cache->bufferNoOpMaskActual)[address];
	data = (data & ~maskNoOp) | ((((uint8*)cache->bufferNoOpValue)[address]) & maskNoOp);

	temp[cache->line.index] = data;
    status                  = cache->write(cache->deviceDriver, 1 << cache->line.index, address, cache->line.temp);

    return status;
}


static boolean Ifx_DeviceCache_writeSingle16(Ifx_DeviceCache *cache, Ifx_DeviceCache_Address address, uint16 data)
{
    boolean status;

    uint16  *temp = cache->line.temp;
    uint16 maskNoOp;
    maskNoOp = ((uint16*)cache->bufferNoOpMaskActual)[address];
	data = (data & ~maskNoOp) | ((((uint16*)cache->bufferNoOpValue)[address]) & maskNoOp);

	temp[cache->line.index] = data;
    status                  = cache->write(cache->deviceDriver, 1 << cache->line.index, address, cache->line.temp);

    return status;
}
