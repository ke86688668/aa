/**
 * \file Ifx_AngleGenF32.h
 * \brief Rotating angle generator
 * \ingroup library_srvsw_sysse_math_f32_anglegen
 *
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_math_f32_anglegen Angle Generator
 * This module implements rotating angle generator functionalities
 * \ingroup library_srvsw_sysse_math_f32
 *
 */
#ifndef IFX_ANGLEGENF32_H
#define IFX_ANGLEGENF32_H

#include "SysSe/Math/Ifx_Lut.h"
#include "StdIf/IfxStdIf_Pos.h"

typedef enum
{
    Ifx_AngleGenF32_SpeedUnit_rps,     /**< \brief in rotation per second or Hertz */
    Ifx_AngleGenF32_SpeedUnit_rad_s    /**< \brief in radian per second */
} Ifx_AngleGenF32_SpeedUnit;

typedef enum
{
    Ifx_AngleGenF32_Status_underflow,
    Ifx_AngleGenF32_Status_overflow,
    Ifx_AngleGenF32_Status_ok
} Ifx_AngleGenF32_Status;

/**
 * \brief Angle generation object definition.
 */
typedef struct
{
    float32                 speed;
    float32                 value;
    float32                 delta;
    Ifx_AngleGenF32_Status status;
} Ifx_AngleGenF32;

/** \addtogroup library_srvsw_sysse_math_f32_anglegen
 * \{ */

/**
 * \brief Reset the internal variables (speed, value)
 */
IFX_INLINE void Ifx_AngleGenF32_reset(Ifx_AngleGenF32 *self)
{
    self->value  = 0.0;
    self->speed  = 0.0;
    self->status = Ifx_AngleGenF32_Status_ok;
}


/**
 * \brief Initialize the Ifx_AngleGenF32 object
 *
 * \param self pointer to the Ifx_AngleGenF32 object
 * \param tsample sample time in s
 */
IFX_INLINE void Ifx_AngleGenF32_init(Ifx_AngleGenF32 *self, Ifx_AngleGenF32_SpeedUnit speedUnit, float32 tsample)
{
    if (speedUnit == Ifx_AngleGenF32_SpeedUnit_rad_s)
    {
        self->delta = tsample * (IFX_LUT_ANGLE_RESOLUTION / 2) / (IFX_PI * 2);
    }
    else
    {
        self->delta = tsample * (IFX_LUT_ANGLE_RESOLUTION / 2);
    }

    Ifx_AngleGenF32_reset(self);
}


IFX_INLINE float32 Ifx_AngleGenF32_getValue(Ifx_AngleGenF32 *self)
{
    return self->value;
}


IFX_INLINE void Ifx_AngleGenF32_setValue(Ifx_AngleGenF32 *self, float32 value)
{
    self->value = value;
}


/**
 * \brief Execute the Angle generation function.
 *
 * This function shall be called periodically as specified by tsample in Ifx_AngleGenF32_init()
 *
 * The following is executed:
 *   angle[k] = angle[k-1] + (speed[k] + speed[k-1]) * 0.5 * Ts
 *   speed[k] = speed[k-1]
 *
 * \param self Pointer to the Ifx_AngleGenF32 object
 * \param speed Actual speed (the speed unit is specified by the initialisation)
 * \return None
 */
IFX_INLINE void Ifx_AngleGenF32_step(Ifx_AngleGenF32 *self, float32 speed)
{
    float32 value = self->value + ((speed + self->speed) * self->delta);
    self->speed = speed;

    /* limit the value between 0 .. IFX_LUT_ANGLE_RESOLUTION */
    if (value >= IFX_LUT_ANGLE_RESOLUTION)
    {
        value       -= IFX_LUT_ANGLE_RESOLUTION;
        self->status = Ifx_AngleGenF32_Status_overflow; /* FIXME Does this flag makes sense (normal operation behavior)?  */
    }
    else if (value < 0)
    {
        value       += IFX_LUT_ANGLE_RESOLUTION;
        self->status = Ifx_AngleGenF32_Status_underflow;/* FIXME Does this flag makes sense (normal operation behavior)?  */
    }
    else
    {
        self->status = Ifx_AngleGenF32_Status_ok;
    }

    self->value = value;
}


/** \} */

#endif /* IFX_ANGLEGENF32_H */
