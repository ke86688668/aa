/**
 * \file Ifx_AnalogInputF32.c
 * \brief Analog input interface.
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

//________________________________________________________________________________________

#include "Ifx_AnalogInputF32.h"
#include "_Utilities/Ifx_Assert.h"
#include "Evadc/Std/IfxEvadc.h"

#if PRINT_RESOURCE_USED
#include "SysSe/Comm/Console.h"
#endif
//________________________________________________________________________________________

/** \brief scale the input value result = (value + scale->offset) * scale->gain.
 *
 * \param scale Specifies scale object.
 * \param value Specifies the input raw value.
 *
 * \return Return the scaled value
 */
float32 Scale_execute(Ifx_AnanogInput_Scale *scale, float32 value)
{
    return scale->gain * (value + scale->offset);
}


//________________________________________________________________________________________

float32 Limits_execute(Ifx_AnanogInput_Limits *limits, float32 value)
{
    if (value > limits->upper)
    {
        limits->status = Limits_Status_Max;

        if (limits->onOutOfRange != NULL_PTR)
        {
            limits->onOutOfRange(limits->object, limits->status, limits->info);
        }
    }
    else if (value < limits->lower)
    {
        limits->status = Limits_Status_Min;

        if (limits->onOutOfRange != NULL_PTR)
        {
            limits->onOutOfRange(limits->object, limits->status, limits->info);
        }
    }
    else
    {
        limits->status = Limits_Status_Ok;
    }

    return value;
}


/**
 * \brief Update the Ifx_AnalogInputF32 state
 */
float32 Ifx_AnalogInputF32_update(Ifx_AnalogInputF32 *input, float32 raw)
{
    float32 value;

#if ANALOG_I_COPY_RAW
    input->raw = raw;
#endif

    if (input->lut)
    {
        value = Ifx_LutLinearF32_searchBin(input->lut, raw);
    }
    else
    {
        /* scale */
        value = Scale_execute(&input->scale, raw);
    }

#if ANALOG_I_FILTERING

    /* apply filter */
    if (input->filterEnabled == TRUE)
    {
        value = Ifx_LowPassPt1F32_do(&input->filter, value);
    }

#endif

    /* store value */
    input->value = value;

#if ANALOG_I_LIMIT_CHECKING
    /* check limits */
    Limits_execute(&input->limits, value);
#endif

    return value;
}


void Ifx_AnalogInputF32_forceInput(Ifx_AnalogInputF32 *input, float32 raw)
{
    float32 value;

#if ANALOG_I_COPY_RAW
    input->raw = raw;
#endif

    if (input->lut)
    {
        value = Ifx_LutLinearF32_searchBin(input->lut, raw);
    }
    else
    {
        /* scale */
        value = Scale_execute(&input->scale, raw);
    }

#if ANALOG_I_FILTERING

    /* apply filter */
    if (input->filterEnabled == TRUE)
    {
        input->filter.out = value;
    }

#endif

    /* store value */
    input->value = value;
}


/**
 * \brief Reset the Ifx_AnalogInputF32 state
 */
void Ifx_AnalogInputF32_reset(Ifx_AnalogInputF32 *input)
{
#if ANALOG_I_FILTERING
    Ifx_LowPassPt1F32_reset(&input->filter);
#endif
}


/** \brief Initialize an analog input internal state
 *
 * \param input specifies the analog input to initialize
 * \param config specifies the analog input configuration
 * FIXME add Ifx_AnalogInputF32_initConfig()
 * \return None.
 * \see Adc_getInputChannel()
 */
void Ifx_AnalogInputF32_init(Ifx_AnalogInputF32 *input, const Ifx_AnalogInputF32_Config *config)
{
    const Ifx_AnalogInputF32_Params *params = &config->params;

    /* connection to HW */
    //input->hwInfo = config->hwInfo; /* FIXME To be handle separately, currently initialize at a later time */

    /* scale */
    input->scale.gain   = params->gain;
    input->scale.offset = params->offset;
    input->lut          = params->lut;

#if ANALOG_I_LIMIT_CHECKING
    /* limits */
    input->limits.lower        = params->lower;
    input->limits.upper        = params->upper;
    input->limits.status       = Limits_Status_Ok;
    input->limits.onOutOfRange = (Limit_onOutOfRange)params->onOutOfRange;
    input->limits.object       = params->object;
    input->limits.info         = params->info;
#endif

#if ANALOG_I_FILTERING
    /* filter */
    input->filterEnabled = params->filterEnabled;

    if (input->filterEnabled != FALSE)
    {
        Ifx_LowPassPt1F32_init(&input->filter, &config->params.filterConfig);
    }

#endif

    input->name = config->name;
}
