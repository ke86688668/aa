/**
 * \file Ifx_OsciShell.h
 * \brief Oscilloscope interface using Shell
 * \ingroup library_srvsw_sysse_debug_osciShell
 *
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_debug_osciShell Oscilloscope interface over Shell
 * \ingroup library_srvsw_sysse_debug_osci
 *
 */

#ifndef OSCISHELL_H
#define OSCISHELL_H

#include "Ifx_Cfg.h"

#include "SysSe/Comm/Ifx_Shell.h"
#include "Ifx_Osci.h"

#define IFX_OSCI_SHELL_PREFIX "osci"

typedef struct
{
    Ifx_Osci *osci;
}Ifx_OsciShell;

typedef struct
{
    Ifx_Osci *osci;
}Ifx_OsciShell_Config;

IFX_EXTERN Ifx_Shell_Command Ifx_g_OsciShell_commands[];

/** \addtogroup library_srvsw_sysse_debug_osciShell
 * \{ */
boolean Ifx_OsciShell_init(Ifx_OsciShell *osciShell, Ifx_OsciShell_Config *config);
void    Ifx_OsciShell_initConfig(Ifx_OsciShell_Config *config, Ifx_Osci *osci);

/** \} */

#endif /* OSCISHELL_H */
