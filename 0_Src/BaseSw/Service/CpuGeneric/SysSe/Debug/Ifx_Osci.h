/**
 * \file Ifx_Osci.h
 * \brief Oscilloscope functions.
 *
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_debug_osci Oscilloscope
 * This module implements oscilloscope functionalities
 * \ingroup library_srvsw_sysse_debug
 *
 * The oscilloscope enable to monitor internal
 * signals by recording multiple application software internal variable.
 *
 * \section channelSelection Channel selection
 *
 * \section dataTypes Supported data types
 * Please refer to \ref Ifx_Osci_DataType
 *
 * \section triggers Triggers
 *
 * \subsection triggerModes Trigger modes
 * - Normal: the recording is re-started each the trigger condition is true and the buffer is full
 * - Automatic: the recording runs continuously, in a circular way
 * (i.e. new data overwrite old data when the buffer is full).
 * The recording is stopped only if the mode is changed to another one
 * , or the oscilloscope stopped.
 *
 * \subsection forcedTriggerMode Forced trigger mode
 * In this mode,the recording is started immediately, ignoring the trigger condition. The recording
 * is started from the start of buffer and stopped as soon as the buffer is full.
 *
 * The Force mode is only available in Normal mode
 *
 * \subsection singleTriggerMode Single trigger mode
 * In single mode,the recording is started on a valid trigger condition.
 * The trigger is immediately disabled. The recording ends when the buffer is
 * full and the  and osci will enter idle mode (indicated with Ifx_Osci_isReady()).
 * The trigger has to be explicitely enabled again.
 *
 * The Single mode is only avaialble in Normal mode.
 *
 * \subsection edgeTriggerMode Edge-triggered mode
 * In edge trigger mode, the recodring is started when the signal match the selected trigger conditions:
 * - signal cross the trigger level with a rising edge
 * - signal cross the trigger level with a falling edge
 * - signal cross the trigger level with a rising or falling edge
 * The recording is started from the start of buffer.
 *
 * Edge-triggered mode is indicated with Ifx_Osci_isEdgeTriggered() returns TRUE (\todo Implement the function).
 *
 * \subsection timeTriggerMode Time-triggered mode
 * In the time triggered mode, the data logger records data starting periodically when
 * specified time elapsed (\todo to be implemented).
 *
 * Time triggered mode is indicated with Ifx_Osci_isTimeTriggered() returns TRUE (\todo Implement the function).
 *
 * \section userInterface User-interface protocol
 *
 * Two interfaces are available:
 * - \ref library_srvsw_sysse_debug_osciShell for shell interface
 * - \ref sysse_debug_osci_can for interface over CAN bus
 * - A hardware debugger through OCDS1/JTAG
 *
 */

#if !defined(IFX_OSCI_H)
#define IFX_OSCI_H
//________________________________________________________________________________________
// INCLUDES
#include "Ifx_Cfg.h"
#include "SysSe/Bsp/Bsp.h"
#include "SysSe/Math/IFX_Cf32.h"

//________________________________________________________________________________________
// CONFIGURATION DEFINES

#ifndef IFX_CFG_OSCI_CHANNEL_COUNT
#define IFX_CFG_OSCI_CHANNEL_COUNT (4)
#endif

#ifndef IFX_CFG_OSCI_DATA_LENGTH
/** \brief Number of samples per channels */
#define IFX_CFG_OSCI_DATA_LENGTH   (512)
#endif

#ifndef IFX_CFG_OSCI_FFT_ENABLED
#define IFX_CFG_OSCI_FFT_ENABLED   (TRUE) /**< \brief If TRUE, the FFT feature is enabled else disabled */
#endif

#if IFX_CFG_OSCI_FFT_ENABLED
#    define IFX_OSCI_FFT_LENGTH    IFX_CFG_OSCI_DATA_LENGTH
#else
#    define IFX_OSCI_FFT_LENGTH    2        /**< Minimal value is 2 */
#endif

//________________________________________________________________________________________
// ENUMERATIONS

/** \brief Oscilloscope data type enumeration. */
typedef enum
{
    Ifx_Osci_DataType_Unknown    = 0,  /**<\brief Unknown data type */
    Ifx_Osci_DataType_Float32    = 1,  /**<\brief Signal data type is float32 */
    Ifx_Osci_DataType_SInt16     = 2,  /**<\brief Signal data type is sint16 */
    Ifx_Osci_DataType_UInt16     = 3,  /**<\brief Signal data type is uint16 */
    Ifx_Osci_DataType_SInt32     = 4,  /**<\brief Signal data type is sint32 */
    Ifx_Osci_DataType_UInt32     = 5,  /**<\brief Signal data type is uint32 */
    Ifx_Osci_DataType_FixPoint16 = 6,  /**<\brief Signal data type is FixPoint16 */
    Ifx_Osci_DataType_FixPoint32 = 7,  /**<\brief Signal data type is FixPoint32 */
    Ifx_Osci_DataType_SInt8      = 8,  /**<\brief Signal data type is sint8 */
    Ifx_Osci_DataType_UInt8      = 9,  /**<\brief Signal data type is uint8 */
    Ifx_Osci_DataType_boolean    = 10, /**<\brief Signal data type is boolean */
    Ifx_Osci_DataType_SInt64     = 11, /**<\brief Signal data type is sint64 */
    Ifx_Osci_DataType_UInt64     = 12  /**<\brief Signal data type is uint64 */
                                       /* FIXME  fix name case  */
} Ifx_Osci_DataType;

//________________________________________________________________________________________
// DATA STRUCTURES
typedef struct Ifx_Osci_ Ifx_Osci;

/** \brief Oscilloscope channel configuration. */
typedef struct
{
    const void       *source;       /**<\brief Pointer to the signal source */
    Ifx_Osci_DataType type;         /**<\brief Specifies the signal data type */
    sint16            QFormatShift; /**<\brief Specifies the shifts if type is FixPoint16 or FixPoint32 */
} Ifx_Osci_Channel_Config;

/** \brief Oscilloscope channel object. */
typedef struct
{
    const void       *source;       /**<\brief Pointer to the signal source */
    Ifx_Osci_DataType type;         /**<\brief Specifies the signal data type */
    sint16            QFormatShift; /**<\brief Specifies the shifts if type is FixPoint16 or FixPoint32 */
    float32          *values;       /**<\brief Pointer to the oscilloscope data/log storage */
    Ifx_Osci         *osci;
} Ifx_Osci_Channel;

#define IFX_OSCI_SIGNAL_CONFIG_TABLE_TERMINATOR {0, {0, (Ifx_Osci_DataType)0, 0}, 0}

/** \brief Oscilloscope signal configuration. */
typedef struct
{
    sint32                  id; /**<\brief Specifies the signal index, will be matched when calling Ifx_Osci_setChannelSignal() */
    Ifx_Osci_Channel_Config config;
    pchar                   name;
} Ifx_Osci_Signal_Config;

typedef enum
{
    Ifx_Osci_TriggerMode_normal    = 0,
    Ifx_Osci_TriggerMode_automatic = 1
}Ifx_Osci_TriggerMode;

typedef enum
{
    Ifx_Osci_Operation_clr,
    Ifx_Osci_Operation_add,
    Ifx_Osci_Operation_sub,
    Ifx_Osci_Operation_mul,
    Ifx_Osci_Operation_div,
    Ifx_Osci_Operation_set,
    Ifx_Osci_Operation_atan2
} Ifx_Osci_Operation;

typedef enum
{
    Ifx_Osci_SignalType_sine,
    Ifx_Osci_SignalType_square
} Ifx_Osci_SignalType;

typedef struct
{
    float32 dcOffset;                   /**< \brief DC offset */
    float32 pTot;                       /**< \brief Total power at half spectrum */
    float32 pPeak;                      /**< \brief Peak power */
    float32 fPeak;                      /**< \brief Peak frequency */
    float32 pSig;                       /**< \brief Signal power */
    float32 pnd;                        /**< \brief Noise and Distortion */
    float32 signalPeakDb;               /**< \brief Signal Peak in DB */
    float32 signalNoiseDistortionDb;    /**< \brief Signal + Noise + Distortion in dB*/
    float32 noiseDistortionDb;          /**< \brief Noise + Distortion in DB */
    float32 sinad;                      /**< \brief SINAD in DB*/
    float32 enob;                       /**< \brief ENOB = %f bits in bits*/
}Ifx_Osci_analyzeResult;

/** \brief Oscilloscope trigger object */
typedef struct
{
    boolean              force;      /**< \brief */
    boolean              enabled;    /**< \brief */
    boolean              single;     /**< \brief */
    Ifx_Osci_TriggerMode mode;       /**< \brief */
    boolean              risingEdge; /**< \brief */
    Ifx_TickTime         autoNext;   /**< \brief */
    float32              baseTime;   /**< \brief */
    uint16               groupIndex; /**< \brief */
    uint16               itemIndex;  /**< \brief */
    sint32               source;     /**< \brief */
    float32              level;      /**< \brief */
    float32              lastLevel;  /**< \brief */
} Ifx_Osci_Trigger;

/** \brief Oscilloscope object. */
struct Ifx_Osci_
{
    boolean          enabled;      /**< \brief */
    Ifx_Osci_Trigger trigger;      /**< \brief */

    float32          samplePeriod; /**< \brief */
    Ifx_TickTime     startTime;    /**< \brief */
    sint16           interval;     /**< \brief */
    sint16           intervalTmp;  /**< \brief */
    uint16           readIndex;    /**< \brief */
    uint16           writeIndex;   /**< \brief */

    uint16           channelCount; /**< \brief number of active channels */
    uint16           dataLength;   /**< \brief Number of acquired data per channel */
    Ifx_Osci_Channel channels[IFX_CFG_OSCI_CHANNEL_COUNT];
    struct
    {
        sint16                        count;
        const Ifx_Osci_Signal_Config *config;
    }       signals;
    float32 values[IFX_CFG_OSCI_CHANNEL_COUNT][IFX_CFG_OSCI_DATA_LENGTH];

    struct
    {
        cfloat32 in[IFX_OSCI_FFT_LENGTH];

        union
        {
            cfloat32 X[IFX_OSCI_FFT_LENGTH];
            float32  db[IFX_OSCI_FFT_LENGTH / 2];
            float32  pwr[IFX_OSCI_FFT_LENGTH / 2];
        }       out;
        boolean enabled;
        uint16  length;             /**< \brief FFT buffer length. Must be bigger or equal to dataLength to enable FFT. Must be smaller or equal to IFX_OSCI_FFT_LENGTH */
        uint32  padLength;
    } fft;
};

typedef struct
{
    float32              samplePeriod; /**< \brief Sample period in s */
    uint16               channelCount; /**< \brief number of active channels. Must be smaller or equal to IFX_CFG_OSCI_CHANNEL_COUNT */
    uint16               dataLength;   /**< \brief Number of acquired data per channel. Must be smaller or equal to IFX_CFG_OSCI_DATA_LENGTH */
    uint16               fftLength;    /**< \brief FFT buffer length. Must be bigger or equal to dataLength to enable FFT. Must be smaller or equal to IFX_OSCI_FFT_LENGTH */

    Ifx_Osci_TriggerMode triggerMode;  /**< \brief Trigger mode */
}Ifx_Osci_Config;

//________________________________________________________________________________________
// FUNCTION PROTOTYPES

/** \addtogroup library_srvsw_sysse_debug_osci
 * \{ */

/** \name Initialisation functions
 * \{ */
IFX_EXTERN boolean Ifx_Osci_init(Ifx_Osci *osci, Ifx_Osci_Config *config);
IFX_EXTERN void    Ifx_Osci_initConfig(Ifx_Osci_Config *config);
IFX_EXTERN void    Ifx_Osci_setSignalsConfig(Ifx_Osci *osci, const Ifx_Osci_Signal_Config *config);
/** \} */
//IFX_EXTERN boolean Ifx_Osci_setChannel(Ifx_Osci* osci, uint16 channelIndex, const Ifx_Osci_Channel_Config* config);
/** \name Acquisition settings and status
 * \{ */
IFX_EXTERN boolean Ifx_Osci_setChannelSignal(Ifx_Osci *osci, uint16 osciChannel, uint16 signalIndex);
IFX_INLINE void    Ifx_Osci_setInterval(Ifx_Osci *osci, uint16 interval);
IFX_EXTERN boolean Ifx_Osci_getNextData(Ifx_Osci *osci, float32 *data);
IFX_EXTERN pchar   Ifx_Osci_getDataTypeName(Ifx_Osci_DataType type);
IFX_EXTERN pchar   Ifx_Osci_getSignalName(Ifx_Osci *osci, const void *source);
/** \} */
/** \name Triggered settings
 * \{ */
IFX_INLINE void Ifx_Osci_enableEdgeTrigger(Ifx_Osci *osci);
IFX_INLINE void Ifx_Osci_enableSingleTrigger(Ifx_Osci *osci);
IFX_INLINE void Ifx_Osci_setEdgeTriggerLevel(Ifx_Osci *osci, float32 level);
IFX_INLINE void Ifx_Osci_setEdgeTriggerSource(Ifx_Osci *osci, sint32 channel);
IFX_INLINE void Ifx_Osci_forceTrigger(Ifx_Osci *osci);
/** \} */
/** \name Continuous mode settings
 * \{ */
IFX_INLINE void Ifx_Osci_enableModeAuto(Ifx_Osci *osci, boolean *stopFlag);
/** \} */
/** \name Run/stop functions
 * \{ */
IFX_INLINE void    Ifx_Osci_run(Ifx_Osci *osci);
IFX_INLINE void    Ifx_Osci_stop(Ifx_Osci *osci);
IFX_INLINE boolean Ifx_Osci_isRunning(Ifx_Osci *osci);
/** \} */
/** \name Background processing functions
 * \{ */
IFX_EXTERN void Ifx_Osci_step(Ifx_Osci *osci);
IFX_EXTERN void Ifx_Osci_stepForced(Ifx_Osci *osci);
/** \} */

/** \name Signal operation
 * \{ */
void                   Ifx_Osci_doSignalOperation(Ifx_Osci_Channel *result, Ifx_Osci_Channel *a, Ifx_Osci_Operation op, Ifx_Osci_SignalType type, float32 freq, float32 ampl, float32 offset, float32 phase);
void                   Ifx_Osci_doChannelOperation(Ifx_Osci_Channel *result, Ifx_Osci_Channel *a, Ifx_Osci_Operation op, Ifx_Osci_Channel *b);
Ifx_Osci_analyzeResult Ifx_Osci_analyze(Ifx_Osci_Channel *channel, sint32 span);
/** \} */

/** \} */
//________________________________________________________________________________________
// INLINE FUNCTION IMPLEMENTATIONS

/** \brief Run the oscilloscope */
IFX_INLINE void Ifx_Osci_run(Ifx_Osci *osci)
{
    osci->enabled = TRUE;
}


/** \brief Run the oscilloscope */
IFX_INLINE boolean Ifx_Osci_isRunning(Ifx_Osci *osci)
{
    return osci->enabled;
}


/** \brief Stop the oscilloscope */
IFX_INLINE void Ifx_Osci_stop(Ifx_Osci *osci)
{
    osci->enabled = FALSE;
}


/** \brief Enable edge-triggering mode */
IFX_INLINE void Ifx_Osci_enableEdgeTrigger(Ifx_Osci *osci)
{   /* FIXME according to the API name, should only enable the trigger (osci->trigger.enabled, and set trigger to edge mode)*/
    osci->trigger.single  = FALSE;
    osci->trigger.enabled = TRUE;
    osci->trigger.mode    = Ifx_Osci_TriggerMode_normal;
    Ifx_Osci_run(osci);
}


/** \brief Set single triggering mode if trigger is enabled by Ifx_Osci_enableEdgeTrigger().
 * Automatic mode will be disabled if this function is called */
IFX_INLINE void Ifx_Osci_enableSingleTrigger(Ifx_Osci *osci)
{   /* FIXME according to API name, should not modify the trigger mode but only enable single (osci->trigger.single = TRUE)*/
    Ifx_Osci_enableEdgeTrigger(osci);
    osci->trigger.single = TRUE;
}


/** \brief Set the trigger level */
IFX_INLINE void Ifx_Osci_setEdgeTriggerLevel(Ifx_Osci *osci, float32 level)
{
    osci->trigger.level = level;
}


/** \brief Set the trigger source */
IFX_INLINE void Ifx_Osci_setEdgeTriggerSource(Ifx_Osci *osci, sint32 channel)
{
    osci->trigger.source = __saturate(channel, 0, osci->channelCount - 1);
}


/** \brief Force trigger if trigger is enabled by Ifx_Osci_enableEdgeTrigger() */
IFX_INLINE void Ifx_Osci_forceTrigger(Ifx_Osci *osci)
{   /* FIXME according to API name, should only enable force (osci->trigger.force = TRUE) */
    Ifx_Osci_run(osci);
    osci->trigger.force = TRUE;
}


IFX_INLINE void Ifx_Osci_setInterval(Ifx_Osci *osci, uint16 interval)
{
    osci->interval = __saturate((sint16)interval, 1, 256);
}


IFX_INLINE void Ifx_Osci_enableModeAuto(Ifx_Osci *osci, boolean *stopFlag)
{
    osci->trigger.enabled = FALSE;
    osci->trigger.mode    = Ifx_Osci_TriggerMode_automatic;
    osci->readIndex       = 0;
    osci->writeIndex      = osci->dataLength;
    Ifx_Osci_run(osci);
}


IFX_INLINE boolean Ifx_Osci_isReady(Ifx_Osci *osci)
{
    return osci->writeIndex == IFX_CFG_OSCI_DATA_LENGTH;
}


//----------------------------------------------------------------------------------------
#endif
