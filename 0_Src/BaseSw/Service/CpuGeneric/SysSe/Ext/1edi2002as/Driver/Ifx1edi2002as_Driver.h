/**
 * \file Ifx1edi2002as_Driver.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup IfxLld_1edi2002as_driver 1EDI2002AS Driver
 * \ingroup IfxLld_1edi2002as
 */

/**
 * Daisy chain strategy:
 * To initialize the daisy chain, each of the devices must be initialize using the  Ifx1edi2002as_init() API, starting by the 1st device in the chain.
 */

#ifndef IFX1EDI2002AS_DRIVER_H_
#define IFX1EDI2002AS_DRIVER_H_

#include "Ifx_Cfg.h"

#ifndef IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
#define IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE (FALSE)
#endif


#include "SysSe/Bsp/Bsp.h"
#include "If/SpiIf.h"
#include "SysSe/Ext/1edi2002as/_Reg/Ifx1edi2002as_regdef.h"
#include "SysSe/Ext/1edi2002as/_Reg/Ifx1edi2002as_reg.h"
#include "SysSe/Ext/1edi2002as/_Reg/Ifx1edi2002as_enum.h"

#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
#include "SysSe/General/Ifx_DeviceCache.h"
#endif


#ifndef IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH
#define IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH (6) /**<  Maximal length of the daisy chain.*/
#endif

typedef struct Ifx1edi2002as_Driver_ Ifx1edi2002as_Driver;
struct Ifx1edi2002as_Driver_
{
    /* FIXME SpiIf_Ch is a iLLD definition, to be portable an other type name should be defined that enable the user to redefine it
     * Device driver should not use propriaritary definition */
    SpiIf_Ch *channel;

#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    struct
    {
        /* Device specific part */
        Ifx_1EDI2002AS  cache;                                                         /**< \brief Cache of the device registers */
        uint32          modificationMask[sizeof(Ifx_g_1EDI2002AS_validAddress) / 4];   /**< \brief register modification mask. If bit with offset x is 1, corresponding register address x has been modified and need to be written to the device */
        uint32          cacheValid[sizeof(Ifx_g_1EDI2002AS_validAddress) / 4];         /**< \brief register cache valid. If bit with offset x is 1, corresponding register cache x has is valid */
        uint32          cacheValidVolatile[sizeof(Ifx_g_1EDI2002AS_validAddress) / 4]; /**< \brief register cache valid (volatile registers). If bit with offset x is 1, corresponding register cache x has is valid */
        Ifx_DeviceCache engine;
        boolean         enabled;                                                       /* Cache status: enabled (TRUE) or disabled (FALSE). Enabled by default */
        uint16          tempCacheLine[IFX1EDI2002AS_MAX_DAISYCHAIN_LENGTH];            /** Buffer for the temporary cache line */
    } cache;
#endif

    struct
    {
/* FIXME IfxPort_Pin is a iLLD definition, to be portable an other type name should be defined that enable the user to redefine it */
        IfxPort_Pin fltAN;     /**< \brief FLTA\\*/
        IfxPort_Pin fltBN;     /**< \brief FLTB\\*/
        IfxPort_Pin rstN;      /**< \brief RST\\RDY */
        IfxPort_Pin en;        /**< \brief EN */
    } pins;
    struct
    {
        Ifx1edi2002as_Driver *first;  /**< First daisy chain device. */
        Ifx1edi2002as_Driver *next;   /**< Next device in the chain */
        uint8          length; /**< Number of device in the daisy chain */
        uint8          index;  /**< \brief index of the device in the daisy chain. First device has index 0 */
    } daisyChain;
};

typedef struct
{
    SpiIf_Ch      *channel;
    IfxPort_Pin    fltAN;          /**< \brief FLTA\\*/
    IfxPort_Pin    fltBN;          /**< \brief FLTB\\*/
    IfxPort_Pin    rstN;           /**< \brief RST\\RDY */
    IfxPort_Pin    en;             /**< \brief EN */
    Ifx1edi2002as_Driver *previousDevice; /**< Previous devices in the daisy chain. The first device in the daisy chain must be NULL. */
}Ifx1edi2002as_Driver_Config;

/** \addtogroup IfxLld_1edi2002as_driver
 * \{ */

/** Disable the cache
 * If the device are in daisy chain, cache for all device in the chain is disabled
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 */
void Ifx1edi2002as_Driver_disableAllDevicesCache(Ifx1edi2002as_Driver *driver);
/** Enable the cache
 * Enabling the cache will automatically invalidate the cache
 * If the device are in daisy chain, cache for all device in the chain is disabled
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 */
void Ifx1edi2002as_Driver_enableAllDevicesCache(Ifx1edi2002as_Driver *driver);

boolean Ifx1edi2002as_init(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_Driver_Config *config);
void    Ifx1edi2002as_initConfig(Ifx1edi2002as_Driver_Config *config);
/** Invalidate the cache
 *
 * If the device are in daisy chain, cache for all device in the chain is invalidated
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 */
IFX_INLINE void Ifx1edi2002as_Driver_invalidateAllDevicesCache(Ifx1edi2002as_Driver *driver);

/** read a register value either from cache or the device if the cache is not valid
 *
 * If the cache is enabled, the full cache line is read. If the cache is disabled only the requested device register is read.
 *
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 * \param index Index of the device to be accessed. One must care that the data parameter points to and memory area of corresponding size
 * \param address Register address. Unit is the device address unit.
 * \param data Returned register value
 */
boolean Ifx1edi2002as_Driver_readRegister(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_Address address, uint16 *data);

/** Synchonize the cache with the device (read)
 * If the devices are in daisy chain, all devices are synchronized
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 */
boolean Ifx1edi2002as_Driver_readSync(Ifx1edi2002as_Driver *driver);

/** Write a register in the cache
 *
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 * \param address Register address. Unit is the device address unit.
 * \param data Register value to be written
 */
boolean Ifx1edi2002as_Driver_writeRegister(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_Address address, uint16 data);

/** Synchonize the cache with the device (write)
 * If the devices are in daisy chain, all devices are written
 *
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 */
boolean Ifx1edi2002as_Driver_writeSyncAllDevices(Ifx1edi2002as_Driver *driver);

IFX_INLINE void Ifx1edi2002as_Driver_invalidateAllDevicesCache(Ifx1edi2002as_Driver *driver)
{
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
	driver = driver->daisyChain.first;

    do
    {
        Ifx_DeviceCache_invalidate(&driver->cache.engine);
        driver = driver->daisyChain.next;
    } while (driver);
#else
    (void)driver;
#endif
}


IFX_INLINE void Ifx1edi2002as_Driver_invalidateAllDevicesCacheVolatile(Ifx1edi2002as_Driver *driver)
{
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    driver = driver->daisyChain.first;

    do
    {
        Ifx_DeviceCache_invalidateVolatile(&driver->cache.engine);
        driver = driver->daisyChain.next;
    } while (driver);
#else
    (void)driver;
#endif
}


/** Disable the PWM signal
 *
 * \param mask Specifies which device must be disabled. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 */
void Ifx1edi2002as_Driver_disable(Ifx1edi2002as_Driver *driver, uint32 mask);
/** Enable the PWM signal
 *
 * \param mask Specifies which device must be enabled. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 */
void Ifx1edi2002as_Driver_enable(Ifx1edi2002as_Driver *driver, uint32 mask);
/** Execute a reset of the driver
 *
 * \param mask Specifies which device must be switched off. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 */
void Ifx1edi2002as_Driver_reset(Ifx1edi2002as_Driver *driver, uint32 mask);
/** Switch the driver off
 *
 * \param mask Specifies which device must be switched off. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 */
void Ifx1edi2002as_Driver_switchOff(Ifx1edi2002as_Driver *driver, uint32 mask);
/** Switch the driver on
 *
 * \param mask Specifies which device must be switched on. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 */
void Ifx1edi2002as_Driver_switchOn(Ifx1edi2002as_Driver *driver, uint32 mask);

/** Indicates if the driver is not ready
 *
 * In case of daisy chain, the return value has the following meaning
 * - bit 0: 1st device in the chain. 1 means not ready, 0 means ready or no pin ready assigned
 * - bit 1: 2nd device in the chain
 * - ...
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 */
uint32 Ifx1edi2002as_Driver_isAnyDeviceNotReady(Ifx1edi2002as_Driver *driver);

/** Return the fault status
 *
 * The returned value has the following meaning:
 * [15-0] : FLTA. Bit 0 being the 1st device in the chain. A bit set indicates a fault.
 * [31-16]: FLTB. Bit 16 being the 1st device in the chain. A bit set indicates a fault.
 *
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 */
uint32 Ifx1edi2002as_Driver_isAnyDeviceFault(Ifx1edi2002as_Driver *driver);

/** Enter the configuration mode
 *
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 * \param mask Specifies which device must be switched on. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 */
boolean Ifx1edi2002as_Driver_enterConfigurationMode(Ifx1edi2002as_Driver *driver, uint32 mask);

/** Exit the configuration mode
 *
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 * \param mask Specifies which device must be switched on. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 */
boolean Ifx1edi2002as_Driver_exitConfigurationMode(Ifx1edi2002as_Driver *driver, uint32 mask);

/** Enter the verification mode
 *
 * \param driver Pointer to the device driver. if the device is in daisy chain, any device in the daisy chain can be used
 * \param mask Specifies which device must be switched on. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 */
boolean Ifx1edi2002as_Driver_enterVerificationMode(Ifx1edi2002as_Driver *driver, uint32 mask);
/** \} */

#endif /* IFX1EDI2002AS_DRIVER_H_ */
