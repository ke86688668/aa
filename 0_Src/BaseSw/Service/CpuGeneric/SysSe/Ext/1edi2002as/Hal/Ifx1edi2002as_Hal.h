/**
 * \file Ifx1edi2002as_Hal.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup IfxLld_1edi2002as_Hal Hardware abstraction layer
 * \ingroup IfxLld_1edi2002as
 *
 * This layer abstracts the SPI and port modules used to interact with the device.
 * An example is provided here, but the use must re-implement this layer for it targeted microcontroller.
 *
 */

#ifndef IFX1EDI2002AS_HAL_H_
#define IFX1EDI2002AS_HAL_H_

#include "SysSe/Ext/1edi2002as/Driver/Ifx1edi2002as_Driver.h"

#define IFX1EDI2002AS_DEBUG (0)

/** \addtogroup IfxLld_1edi2002as_Hal
 * \{ */
/* Pure virtual function. to be implemented outside of the driver */

/** Perform the SPI exchange operation.
 *
 * \param spiChannel Pointer to the SPI channel used
 * \param src Pointer to the start of data buffer for data to transmit
 * \param dest Pointer to the start of data buffer for received data
 * \param length Number of raw SPI frame to be exchanged
 *
 * \return TRUE in case of success else FALSE
 * \note the src and dest may be the same data location.
 * \note This function is not implemented in the driver (pure virtual). it must be implemented outside of this module
 */
boolean Ifx1edi2002as_Hal_exchangeSpi(void *spiChannel, const void *src, void *dest, uint8 length);

boolean Ifx1edi2002as_Hal_init(Ifx1edi2002as_Driver_Config *config);

IFX_INLINE void Ifx1edi2002as_Hal_setPinHigh(IfxPort_Pin *pin)
{
    IfxPort_setPinState(pin->port, pin->pinIndex, IfxPort_State_high);
}


IFX_INLINE void Ifx1edi2002as_Hal_setPinLow(IfxPort_Pin *pin)
{
    IfxPort_setPinState(pin->port, pin->pinIndex, IfxPort_State_low);
}


IFX_INLINE void Ifx1edi2002as_Hal_setPinState(IfxPort_Pin *pin, IfxPort_State action)
{
    IfxPort_setPinState(pin->port, pin->pinIndex, action);
}


IFX_INLINE boolean Ifx1edi2002as_Hal_getPinState(IfxPort_Pin *pin)
{
    return IfxPort_getPinState(pin->port, pin->pinIndex);
}

/** \} */

#endif /* IFX1EDI2002AS_HAL_H_ */
