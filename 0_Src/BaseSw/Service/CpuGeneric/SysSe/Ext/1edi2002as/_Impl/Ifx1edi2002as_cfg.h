/**
 * \file Ifx1edi2002as_cfg.h
 * \brief 1EDI2002AS on-chip implementation data 
 *
 * \copyright Copyright (c) 2016 Infineon Technologies AG. All rights reserved.
 *
 * $Date: 2016-01-05 14:12:21
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_1edi2002as 1EDI2002AS
 * \ingroup library_srvsw_sysse
 * \defgroup IfxLld_1edi2002as_Impl Implementation
 * \ingroup IfxLld_1edi2002as
 * \defgroup IfxLld_1edi2002as_Std Standard Driver
 * \ingroup IfxLld_1edi2002as
 */

#ifndef IFX1EDI2002AS_CFG_H
#define IFX1EDI2002AS_CFG_H 1

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "SysSe/Ext/1edi2002as/_Reg/Ifx1edi2002as_regdef.h"
#include "SysSe/Ext/1edi2002as/_Reg/Ifx1edi2002as_enum.h"

#endif /* IFX1EDI2002AS_CFG_H */
