/**
 * \file Ifx1edi2002as.h
 * \brief 1EDI2002AS  basic functionality 
 *
 * \copyright Copyright (c) 2016 Infineon Technologies AG. All rights reserved.
 *
 * $Date: 2016-01-05 14:43:03
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_1edi2002as_Std_Std Std  
 * \ingroup IfxLld_1edi2002as_Std
 */

#ifndef IFX1EDI2002AS_H
#define IFX1EDI2002AS_H 1

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "SysSe/Ext/1edi2002as/_Impl/Ifx1edi2002as_cfg.h"
#include "SysSe/Ext/1edi2002as/Driver/Ifx1edi2002as_Driver.h"

/** \addtogroup IfxLld_1edi2002as_Std_Std
 * \{ */

/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \brief Get Primary Chip Identification 
 * \param driver Device driver object  
 * \param value Value returned  
 * \return TRUE in case of success else FALSE  
 */
IFX_EXTERN boolean Ifx1edi2002as_getPrimaryChipIdentification(Ifx1edi2002as_Driver *driver, uint16 *id);
IFX_EXTERN boolean Ifx1edi2002as_isSecondaryReady(Ifx1edi2002as_Driver *driver, boolean *ready);
IFX_EXTERN boolean Ifx1edi2002as_enableAdvancedPrimaryConfiguration(Ifx1edi2002as_Driver *driver);
IFX_EXTERN boolean Ifx1edi2002as_disableAdvancedPrimaryConfiguration(Ifx1edi2002as_Driver *driver);
IFX_EXTERN boolean Ifx1edi2002as_setDio1Direction(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_Dio1Direction direction);
IFX_EXTERN boolean Ifx1edi2002as_enableDout(Ifx1edi2002as_Driver *driver);
IFX_EXTERN boolean Ifx1edi2002as_disableDout(Ifx1edi2002as_Driver *driver);
IFX_EXTERN boolean Ifx1edi2002as_clearPrimaryRequestBit(Ifx1edi2002as_Driver *driver);
IFX_EXTERN boolean Ifx1edi2002as_clearSecondaryRequestBit(Ifx1edi2002as_Driver *driver);
/** \brief Get Read/Write value 
 * \param driver Device driver object  
 * \param value Value returned  
 * \return TRUE in case of success else FALSE  
 */
IFX_EXTERN boolean Ifx1edi2002as_getPrimariReadWriteRegister(Ifx1edi2002as_Driver *driver, uint16 *value);
/** \brief Set Read/Write value 
 * \param driver Device driver object  
 * \param value Value to be set  
 * \return TRUE in case of success else FALSE  
 */
IFX_EXTERN boolean Ifx1edi2002as_setPrimariReadWriteRegister(Ifx1edi2002as_Driver *driver, uint16 value);
/** \brief Get Secondary Chip Identification 
 * \param driver Device driver object  
 * \param value Value returned  
 * \return TRUE in case of success else FALSE  
 */
IFX_EXTERN boolean Ifx1edi2002as_getSecondaryChipIdentification(Ifx1edi2002as_Driver *driver, uint16 *id);
IFX_EXTERN boolean Ifx1edi2002as_getOperationMode(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_OperationMode *value);
IFX_EXTERN boolean Ifx1edi2002as_enableAdvancedSecondaryConfiguration(Ifx1edi2002as_Driver *driver);
IFX_EXTERN boolean Ifx1edi2002as_disableDesatClamping(Ifx1edi2002as_Driver *driver);
IFX_EXTERN boolean Ifx1edi2002as_disableAdvancedSecondaryConfiguration(Ifx1edi2002as_Driver *driver);
IFX_EXTERN boolean Ifx1edi2002as_setDio2Direction(Ifx1edi2002as_Driver *driver, Ifx1edi2002as_Dio2Direction direction);
IFX_EXTERN boolean Ifx1edi2002as_enableIgbtStateMonitoring(Ifx1edi2002as_Driver *driver);


boolean Ifx1edi2002as_setTwoLevelTurnOffLevel(Ifx1edi2002as_Driver* driver, Ifx1edi2002as_GateTurnOffPlateauLevel value);
boolean Ifx1edi2002as_setTwoLevelTurnOffDelay(Ifx1edi2002as_Driver* driver, uint16 value);
boolean Ifx1edi2002as_setTwoLevelTurnOnLevel(Ifx1edi2002as_Driver* driver, Ifx1edi2002as_GateTurnOnPlateauLevel value);
boolean Ifx1edi2002as_setTwoLevelTurnOnDelay(Ifx1edi2002as_Driver* driver, uint16 value);
boolean Ifx1edi2002as_setVbeCompensation(Ifx1edi2002as_Driver* driver, Ifx1edi2002as_VbeCompensation value);
boolean Ifx1edi2002as_setActiveClampingMode(Ifx1edi2002as_Driver* driver, uint8 value);
boolean Ifx1edi2002as_setActiveClampingTime(Ifx1edi2002as_Driver* driver, uint8 value);
/** \} */  


#endif /* IFX1EDI2002AS_H */
