/**
 * \file Ifx1edi2002as_reg.h
 * \brief
 * \copyright Copyright (c) 2016 Infineon Technologies AG. All rights reserved.
 *
 *
 * Date: 2016-01-05 15:54:14 GMT
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_1edi2002as_Cfg 1edi2002as address
 * \ingroup IfxLld_1edi2002as
 * 
 * \defgroup IfxLld_1edi2002as_Cfg_BaseAddress Base address
 * \ingroup IfxLld_1edi2002as_Cfg
 *
 * \defgroup IfxLld_1edi2002as_Cfg_1edi2002as 2-1EDI2002AS
 * \ingroup IfxLld_1edi2002as_Cfg
 *
 *
 */
#ifndef IFX1EDI2002AS_REG_H
#define IFX1EDI2002AS_REG_H 1
/******************************************************************************/
#include "Ifx1edi2002as_regdef.h"
/******************************************************************************/
/** \addtogroup IfxLld_1edi2002as_Cfg_BaseAddress
 * \{  */

/** \brief 1EDI2002AS object */
#define MODULE_1EDI2002AS /*lint --e(923)*/ ((*(Ifx_1EDI2002AS*)0x0u))
/** \}  */

/******************************************************************************/
/******************************************************************************/
/** \addtogroup IfxLld_1edi2002as_Cfg_1edi2002as
 * \{  */

/** \}  */
/******************************************************************************/
/******************************************************************************/
#endif /* IFX1EDI2002AS_REG_H */
