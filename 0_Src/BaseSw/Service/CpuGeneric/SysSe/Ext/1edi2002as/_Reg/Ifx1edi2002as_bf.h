/**
 * \file Ifx1edi2002as_bf.h
 * \brief
 * \copyright Copyright (c) 2016 Infineon Technologies AG. All rights reserved.
 *
 *
 * Date: 2016-01-05 14:12:28 GMT
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_1edi2002as_BitfieldsMask Bitfields mask and offset
 * \ingroup IfxLld_1edi2002as
 * 
 */
#ifndef IFX1EDI2002AS_BF_H
#define IFX1EDI2002AS_BF_H 1
/******************************************************************************/
/******************************************************************************/
/** \addtogroup IfxLld_1edi2002as_BitfieldsMask
 * \{  */
/** \}  */
/******************************************************************************/
/******************************************************************************/
#endif /* IFX1EDI2002AS_BF_H */
