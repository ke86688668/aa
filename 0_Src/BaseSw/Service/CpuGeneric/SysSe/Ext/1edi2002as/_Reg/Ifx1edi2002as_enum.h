/**
 * \file Ifx1edi2002as_enum.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup IfxLld_1edi2002as_Enum Enumeration
 * \ingroup IfxLld_1edi2002as
 */

#ifndef IFX1EDI2002AS_ENM_H_
#define IFX1EDI2002AS_ENM_H_

/** \addtogroup IfxLld_1edi2002as_Enum
 * \{ */

typedef enum
{
    Ifx1edi2002as_OperationMode_0,
    Ifx1edi2002as_OperationMode_1,
    Ifx1edi2002as_OperationMode_2,
    Ifx1edi2002as_OperationMode_3,
    Ifx1edi2002as_OperationMode_4,
    Ifx1edi2002as_OperationMode_5,
    Ifx1edi2002as_OperationMode_6,
}Ifx1edi2002as_OperationMode;

typedef enum
{
    Ifx1edi2002as_Dio1Direction_input  = 0,
    Ifx1edi2002as_Dio1Direction_output = 1,
}Ifx1edi2002as_Dio1Direction;

typedef enum
{
    Ifx1edi2002as_Dio2Direction_input  = 0,
    Ifx1edi2002as_Dio2Direction_output = 1,
}Ifx1edi2002as_Dio2Direction;

/** PCTRL.GPON */
typedef enum
{
	Ifx1edi2002as_GateTurnOnPlateauLevel_9Dot25Volt = 0,
	Ifx1edi2002as_GateTurnOnPlateauLevel_10Dot5Volt = 1,
	Ifx1edi2002as_GateTurnOnPlateauLevel_11Dot4Volt = 2,
	Ifx1edi2002as_GateTurnOnPlateauLevel_HardSwitching = 3,
}Ifx1edi2002as_GateTurnOnPlateauLevel;

/** PCTRL2.GPOF */
typedef enum
{
	Ifx1edi2002as_GateTurnOffPlateauLevel_0 = 0,
	Ifx1edi2002as_GateTurnOffPlateauLevel_1 = 1,
	Ifx1edi2002as_GateTurnOffPlateauLevel_2 = 2,
	Ifx1edi2002as_GateTurnOffPlateauLevel_3 = 3,
	Ifx1edi2002as_GateTurnOffPlateauLevel_4 = 4,
	Ifx1edi2002as_GateTurnOffPlateauLevel_5 = 5,
	Ifx1edi2002as_GateTurnOffPlateauLevel_6 = 6,
	Ifx1edi2002as_GateTurnOffPlateauLevel_7 = 7,
	Ifx1edi2002as_GateTurnOffPlateauLevel_8 = 8,
	Ifx1edi2002as_GateTurnOffPlateauLevel_9 = 9,
	Ifx1edi2002as_GateTurnOffPlateauLevel_10 = 10,
	Ifx1edi2002as_GateTurnOffPlateauLevel_11 = 11,
	Ifx1edi2002as_GateTurnOffPlateauLevel_12 = 12,
	Ifx1edi2002as_GateTurnOffPlateauLevel_13 = 13,
	Ifx1edi2002as_GateTurnOffPlateauLevel_14 = 14,
	Ifx1edi2002as_GateTurnOffPlateauLevel_15 = 15,
}Ifx1edi2002as_GateTurnOffPlateauLevel;

/** SCFG.VBEC */
typedef enum
{
	Ifx1edi2002as_VbeCompensation_disabled = 0,
	Ifx1edi2002as_VbeCompensation_enabled,
}Ifx1edi2002as_VbeCompensation;
/** \} */

#endif /* IFX1EDI2002AS_ENM_H_ */
