/**
 * \file Ifx1edi2002as_regdef.c
 * \brief
 * \copyright Copyright (c) 2016 Infineon Technologies AG. All rights reserved.
 *
 * Date: 2016-01-05 14:12:30 GMT
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 */
/******************************************************************************/
#include "Ifx1edi2002as_regdef.h"
/******************************************************************************/
/* Every valid address has a flag set to 1 in the Ifx_g_1EDI2002AS_validAddress flag array */
uint32 Ifx_g_1EDI2002AS_validAddress[1] = {
    0xFFFF0FFF
};

/* Register with bitfield marked are r (except fixed value), rwh, rh have a 1 in the Ifx_g_1EDI2002AS_volatileRegisters flag array. Bitfields P and LMI are ignored. */
uint32 Ifx_g_1EDI2002AS_volatileRegisters[1] = {
    0xB0CE0D4E     /* FIXME to be updated */
};

uint32 Ifx_g_1EDI2002AS_actionReadRegisters[1] = {
0x00000000,
};

uint32 Ifx_g_1EDI2002AS_actionWriteRegisters[1] = {
0x00000000, /* FIXME to be updated */ 
};
