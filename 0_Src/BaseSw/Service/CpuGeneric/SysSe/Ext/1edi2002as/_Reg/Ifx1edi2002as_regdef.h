/**
 * \file Ifx1edi2002as_regdef.h
 * \brief
 * \copyright Copyright (c) 2016 Infineon Technologies AG. All rights reserved.
 *
 * Date: 2016-01-05 14:43:11 GMT
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_1edi2002as_Bitfields Bitfields
 * \ingroup IfxLld_1edi2002as
 * 
 * \defgroup IfxLld_1edi2002as_union Register unions
 * \ingroup IfxLld_1edi2002as
 * 
 * \defgroup IfxLld_1edi2002as_struct Memory map
 * \ingroup IfxLld_1edi2002as
 */
#ifndef IFX1EDI2002AS_REGDEF_H
#define IFX1EDI2002AS_REGDEF_H 1
/******************************************************************************/
#include "Platform_Types.h"
#include "Compilers.h"

typedef uint8 Ifx1edi2002as_Address;

#define IFX1EDI2002AS_ADDRESS_UNIT  (2) /*Register width in byte */
#define IFX1EDI2002AS_ADDRESS_SHIFT (1) /* Shift required to convert byte to address unit */


/******************************************************************************/
/** \addtogroup IfxLld_1edi2002as_Bitfields
 * \{  */
/** \brief Primary Configuration Register */
typedef struct _Ifx_1EDI2002AS_PCFG_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 PAREN:1;                   /**< \brief [2:2] Parity Enable Bit (rw) */
    unsigned short CFG1 : 1;
    uint16 OSTAEN:1;                  /**< \brief [4:4] NFLTA Activation on Tristate Event Enable Bit (rw) */
    uint16 OSMAEN:1;                  /**< \brief [5:5] NFLTA Activation on OSM Event Enable Bit (rw) */
    unsigned short reserved_6 : 10;
} Ifx_1EDI2002AS_PCFG_Bits;

/** \brief Primary Second Configuration Register */
typedef struct _Ifx_1EDI2002AS_PCFG2_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 STPDEL:5;                  /**< \brief [6:2] Shoot Through Protection Delay Configuration (rw) */
    uint16 FEN:1;                     /**< \brief [7:7]  (rw) */
    uint16 DIO1:1;                    /**< \brief [8:8] Digital Channel Configuration (rw) */
    uint16 DOEN1:1;                   /**< \brief [9:9]  (rw) */
    unsigned short reserved_A : 6;
} Ifx_1EDI2002AS_PCFG2_Bits;

/** \brief Primary Control Register */
typedef struct _Ifx_1EDI2002AS_PCTRL_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 GPON:2;                    /**< \brief [3:2] Gate Turn-On Plateau Level Configuration (rw) */
    uint16 GTCT:1;                    /**< \brief [4:4] Gate Timing Capture Trigger Bit (rwh) */
    uint16 CLRP:1;                    /**< \brief [5:5] Clear Primary Request Bit (rwh) */
    uint16 CLRS:1;                    /**< \brief [6:6] Clear Secondary Request Bit (rwh) */
    unsigned short reserved_7 : 9;        /**< \brief \internal Reserved */
} Ifx_1EDI2002AS_PCTRL_Bits;

/** \brief Primary Second Control Register */
typedef struct _Ifx_1EDI2002AS_PCTRL2_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 GPOF:4;                    /**< \brief [5:2] Gate Turn-Off Plateau Level Configuration (regular turn-off) (rw) */
    uint16 reserved_5:10;             /**< \brief [15:6] \internal Reserved */
} Ifx_1EDI2002AS_PCTRL2_Bits;

/** \brief Primary Error Register */
typedef struct _Ifx_1EDI2002AS_PER_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 CER1:1;                    /**< \brief [2:2] Communication Error Primary Flag (rwh) */
    uint16 OSTER:1;                   /**< \brief [3:3] Output Stage Tristate Event Flag (rh) */
    uint16 OTER:1;                    /**< \brief [4:4] Overtemperature Error Flag (rh) */
    uint16 OVLO3ER:1;                 /**< \brief [5:5] OVLO3 Error Flag (rh) */
    uint16 GER:1;                     /**< \brief [6:6] GATE Monitoring Error Flag (rh) */
    uint16 VMTO:1;                    /**< \brief [7:7] Verif. Mode Time-Out Flag (rh) */
    uint16 SPIER:1;                   /**< \brief [8:8] SPI Error Flag (rwh) */
    uint16 STPER:1;                   /**< \brief [9:9] Shoot Through Protection Error Flag (rwh) */
    uint16 ENER:1;                    /**< \brief [10:10]  (rwh) */
    uint16 RST1:1;                    /**< \brief [11:11] Reset Primary Flag (rwh) */
    uint16 RSTE1:1;                   /**< \brief [12:12] External Hard Reset Primary Flag (rwh) */
    unsigned short reserved_13 : 3;
} Ifx_1EDI2002AS_PER_Bits;

/** \brief Primary ID Register */
typedef struct _Ifx_1EDI2002AS_PID_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 reserved_2:2;              /**< \brief [3:2] \internal Reserved */
    uint16 PVERS:12;                  /**< \brief [15:4] Primary Chip Identification (rh) */
} Ifx_1EDI2002AS_PID_Bits;

/** \brief Primary Read/Write Register */
typedef struct _Ifx_1EDI2002AS_PRW_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Flag (rh) */
    uint16 RWVAL:14;                  /**< \brief [15:2] Read/Write value (rw) */
} Ifx_1EDI2002AS_PRW_Bits;

/** \brief Primary Status Register */
typedef struct _Ifx_1EDI2002AS_PSTAT_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 GPOFS:4;                   /**< \brief [5:2] Gate Turn-Off Plateau Level Configuration Status (regular\nturn-off) (rh) */
    uint16 SRDY:1;                    /**< \brief [6:6] Secondary Ready Status (rh) */
    uint16 ACT:1;                     /**< \brief [7:7] Active State Status (rh) */
    uint16 GPONS:2;                   /**< \brief [9:8] Gate Turn-On Plateau Level Configuration Status (rh) */
    uint16 reserved_10:1;             /**< \brief [10:10] \internal Reserved */
    uint16 ERR:1;                     /**< \brief [11:11] Error Status (rh) */
    unsigned short reserved_12 : 4;       /**< \brief \internal Reserved */
} Ifx_1EDI2002AS_PSTAT_Bits;

/** \brief Primary Second Status Register */
typedef struct _Ifx_1EDI2002AS_PSTAT2_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 ENVAL:1;                   /**< \brief [2:2]  (rh) */
    uint16 FLTA:1;                    /**< \brief [3:3]  (rwh) */
    uint16 FLTB:1;                    /**< \brief [4:4]  (rwh) */
    uint16 OPM:3;                     /**< \brief [7:5] Operating Mode (rh) */
    uint16 HZ:1;                      /**< \brief [8:8] Tristate Output Stage Status (rh) */
    uint16 OT:1;                      /**< \brief [9:9] Over Temperature Status (rh) */
    uint16 STP:1;                     /**< \brief [10:10] Shoot Through Protection Status (rh) */
    uint16 OSTC:1;                    /**< \brief [11:11] Output Stage Tristate Control (rh) */
    unsigned short reserved_12 : 4;
} Ifx_1EDI2002AS_PSTAT2_Bits;


/** \brief Secondary Active Clamping Configuration Register */
typedef struct _Ifx_1EDI2002AS_SACLT_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 reserved_2:6;              /**< \brief [7:2] \internal Reserved */
    uint16 AT:8;                      /**< \brief [15:8] Activation time (rw) */
} Ifx_1EDI2002AS_SACLT_Bits;

/** \brief Secondary Configuration Register */
typedef struct _Ifx_1EDI2002AS_SCFG_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 reserved_2:2;              /**< \brief [3:2] \internal Reserved */
    uint16 VBEC:1;                    /**< \brief [4:4] V (rw) */
    unsigned short CFG2 : 1;
    uint16 OSMD:1;                    /**< \brief [6:6] Output Stage Monitoring Disable Bit (rw) */
    uint16 OSDAD:1;                   /**< \brief [7:7] OSD Event Class A Disable Bit (rw) */
    uint16 DSTCEN:1;                  /**< \brief [8:8] DESAT Clamping Enable Bit (rw) */
    uint16 PSEN:1;                    /**< \brief [9:9] Pulse Suppressor Enable Bit (rw) */
    uint16 TOSEN:1;                   /**< \brief [10:10] Verification Mode Time Out Duration Selection (rw) */
    unsigned short reserved_11 : 5;
} Ifx_1EDI2002AS_SCFG_Bits;

/** \brief Secondary Second Configuration Register */
typedef struct _Ifx_1EDI2002AS_SCFG2_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 reserved_2:2;              /**< \brief [3:2] \internal Reserved */
    uint16 DSATL:2;                   /**< \brief [5:4] DESAT Threshold Level Selection (rw) */
    uint16 TTOND:4;                   /**< \brief [9:6] TTON Delay Configuration (rw) */
    uint16 DIO2:1;                    /**< \brief [10:10] Digital Channel Configuration (rw) */
    uint16 ACLPM:1;                   /**< \brief [11:11] Active Clamping Mode (rw) */
    uint16 OVLO3D:1;                  /**< \brief [12:12] OVLO3 Mode Configuration (rw) */
    uint16 ISMEN:1;                   /**< \brief [13:13] IGBT State Monitoring Function Enable Bit (rw) */
    unsigned short reserved_14 : 2;
} Ifx_1EDI2002AS_SCFG2_Bits;
/** \brief Secondary Error Register */
typedef struct _Ifx_1EDI2002AS_SER_Bits
{
    unsigned short P : 1;                /**< \brief [0:0] Parity Bit (rh) */
    unsigned short LMI : 1;              /**< \brief [1:1] Last Message Invalid Notification */
    unsigned short reserved_2 : 2;
    unsigned short CER2 : 1;
    unsigned short OSTER : 1;
    unsigned short OTER : 1;
    unsigned short OVLO3ER : 1;
    unsigned short GER : 1;
    unsigned short VMTO : 1;
    unsigned short UVLO3ER : 1;
    unsigned short OVLO2ER : 1;
    unsigned short UVLO2ER : 1;
    unsigned short DESATER : 1;
    unsigned short OCPER : 1;
    unsigned short RST2 : 1;
} Ifx_1EDI2002AS_SER_Bits;

/** \brief Secondary ID Register */
typedef struct _Ifx_1EDI2002AS_SID_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 reserved_2:2;              /**< \brief [3:2] \internal Reserved */
    uint16 SVERS:12;                  /**< \brief [15:4] Secondary Chip Identification (rh) */
} Ifx_1EDI2002AS_SID_Bits;
/** \brief Secondary Regular TTOFF Configuration Register */
typedef struct _Ifx_1EDI2002AS_SRTTOF_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 reserved_2:6;              /**< \brief [7:2] \internal Reserved */
    uint16 RTVAL:8;                   /**< \brief [15:8] TTOFF Delay Value (regular turn-off). (rw) */
} Ifx_1EDI2002AS_SRTTOF_Bits;
typedef struct _Ifx_1EDI2002AS_SSTAT_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 reserved_2:2;              /**< \brief [3:2] \internal Reserved */
    uint16 PWM:1;                     /**< \brief [4:4] PWM Command Status (rh) */
    uint16 FLTA:1;                    /**< \brief [5:5] Event Class A Error (rh) */
    uint16 FLTB:1;                    /**< \brief [6:6] Event Class B Status (rh) */
    uint16 OPM:3;                     /**< \brief [9:7] Operating Mode (rh) */
    uint16 DBG:1;                     /**< \brief [10:10] Debug Mode Active Bit (rh) */
    uint16 OCPCD:1;                   /**< \brief [11:11] OCP Current Detection Flag (rwh) */
    uint16 HZ:1;                      /**< \brief [12:12] Output Stage Status (rh) */
    uint16 OT:1;                      /**< \brief [13:13] Overtemperature Status (rh) */
    unsigned short reserved_14 : 2;
} Ifx_1EDI2002AS_SSTAT_Bits;

/** \brief Secondary Second Status Register */
typedef struct _Ifx_1EDI2002AS_SSTAT2_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 reserved_2:2;              /**< \brief [3:2] \internal Reserved */
    uint16 DSATC:1;                   /**< \brief [4:4] DESAT Comparator Result (rh) */
    uint16 OSDL:1;                    /**< \brief [5:5]  (rh) */
    uint16 OCPC1:1;                   /**< \brief [6:6] OCP First Comparator Result (rh) */
    uint16 OCPC2:1;                   /**< \brief [7:7] OCP Second Comparator Result (rh) */
    uint16 UVLO2M:1;                  /**< \brief [8:8] UVLO2 Monitoring Result (rh) */
    uint16 OVLO2M:1;                  /**< \brief [9:9] OVLO2 Monitoring Result (rh) */
    uint16 UVLO3M:1;                  /**< \brief [10:10] UVLO3 Monitoring Result (rh) */
    uint16 OVLO3M:1;                  /**< \brief [11:11] OVLO3 Comparator Status (rh) */
    uint16 GC1:1;                     /**< \brief [12:12] Gate First Comparator Status (rh) */
    uint16 GC2:1;                     /**< \brief [13:13] Gate Second Comparator Status (rh) */
    uint16 DACLPL:1;                  /**< \brief [14:14]  (rh) */
    unsigned short DIO2L : 1;
} Ifx_1EDI2002AS_SSTAT2_Bits;

/** \brief Secondary Safe TTOFF Configuration Register */
typedef struct _Ifx_1EDI2002AS_SSTTOF_Bits
{
    uint16 P:1;                       /**< \brief [0:0] Parity Bit (rh) */
    uint16 LMI:1;                     /**< \brief [1:1] Last Message Invalid Notification (rh) */
    uint16 reserved_2:2;              /**< \brief [3:2] \internal Reserved */
    uint16 GPS:4;                     /**< \brief [7:4] TTOFF Plateau voltage (safe turn-off) (rw) */
    uint16 STVAL:8;                   /**< \brief [15:8] TTOFF Delay Value (safe turn-off). (rw) */
} Ifx_1EDI2002AS_SSTTOF_Bits;

/** \}  */
/******************************************************************************/
/******************************************************************************/
/** \addtogroup IfxLld_1edi2002as_union
 * \{   */
/** \brief Primary Configuration Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_PCFG_Bits B;       /**< \brief Bitfield access */
} Ifx_1EDI2002AS_PCFG;

/** \brief Primary Second Configuration Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_PCFG2_Bits B;      /**< \brief Bitfield access */
} Ifx_1EDI2002AS_PCFG2;

/** \brief Primary Control Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_PCTRL_Bits B;      /**< \brief Bitfield access */
} Ifx_1EDI2002AS_PCTRL;

/** \brief Primary Second Control Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_PCTRL2_Bits B;     /**< \brief Bitfield access */
} Ifx_1EDI2002AS_PCTRL2;

/** \brief Primary Error Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_PER_Bits B;        /**< \brief Bitfield access */
} Ifx_1EDI2002AS_PER;

/** \brief Primary ID Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_PID_Bits B;        /**< \brief Bitfield access */
} Ifx_1EDI2002AS_PID;

/** \brief Primary Read/Write Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_PRW_Bits B;        /**< \brief Bitfield access */
} Ifx_1EDI2002AS_PRW;

/** \brief Primary Status Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_PSTAT_Bits B;      /**< \brief Bitfield access */
} Ifx_1EDI2002AS_PSTAT;

/** \brief Primary Second Status Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_PSTAT2_Bits B;     /**< \brief Bitfield access */
} Ifx_1EDI2002AS_PSTAT2;

/** \brief Secondary Active Clamping Configuration Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_SACLT_Bits B;      /**< \brief Bitfield access */
} Ifx_1EDI2002AS_SACLT;

/** \brief Secondary Configuration Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_SCFG_Bits B;       /**< \brief Bitfield access */
} Ifx_1EDI2002AS_SCFG;

/** \brief Secondary Second Configuration Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_SCFG2_Bits B;      /**< \brief Bitfield access */
} Ifx_1EDI2002AS_SCFG2;

/** \brief Secondary Error Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_SER_Bits B;        /**< \brief Bitfield access */
} Ifx_1EDI2002AS_SER;

/** \brief Secondary ID Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_SID_Bits B;
} Ifx_1EDI2002AS_SID;
/** \brief Secondary Regular TTOFF Configuration Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_SRTTOF_Bits B;     /**< \brief Bitfield access */
} Ifx_1EDI2002AS_SRTTOF;

/** \brief Secondary Status Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_SSTAT_Bits B;      /**< \brief Bitfield access */
} Ifx_1EDI2002AS_SSTAT;

/** \brief Secondary Second Status Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_SSTAT2_Bits B;     /**< \brief Bitfield access */
} Ifx_1EDI2002AS_SSTAT2;

/** \brief Secondary Safe TTOFF Configuration Register   */
typedef union
{
    uint16 U;                         /**< \brief Unsigned access */
    sint16 I;                         /**< \brief Signed access */
    Ifx_1EDI2002AS_SSTTOF_Bits B;     /**< \brief Bitfield access */
} Ifx_1EDI2002AS_SSTTOF;

/** \}  */


/******************************************************************************/
/** \addtogroup IfxLld_1edi2002as_struct
 * \{  */
/******************************************************************************/
/** \name Object L0
 * \{  */

/** \brief 1EDI2002AS object */
typedef volatile struct _Ifx_1EDI2002AS
{
       Ifx_1EDI2002AS_PID                  PID;                    /**< \brief 0, Primary ID Register*/
       Ifx_1EDI2002AS_PSTAT                PSTAT;                  /**< \brief 1, Primary Status Register*/
       Ifx_1EDI2002AS_PSTAT2               PSTAT2;                 /**< \brief 2, Primary Second Status Register*/
       Ifx_1EDI2002AS_PER                  PER;                    /**< \brief 3, Primary Error Register*/
       Ifx_1EDI2002AS_PCFG                 PCFG;                   /**< \brief 4, Primary Configuration Register*/
       Ifx_1EDI2002AS_PCFG2                PCFG2;                  /**< \brief 5, Primary Second Configuration Register*/
       Ifx_1EDI2002AS_PCTRL                PCTRL;                  /**< \brief 6, Primary Control Register*/
       Ifx_1EDI2002AS_PCTRL2               PCTRL2;                 /**< \brief 7, Primary Second Control Register*/
    unsigned short        reserved_7[1];   /**< \brief FIXME to do implementation */
       Ifx_1EDI2002AS_PRW                  PRW;                    /**< \brief 9, Primary Read/Write Register*/
    unsigned short        reserved_A[6];   /**< \brief FIXME to do implementation */
       Ifx_1EDI2002AS_SID                  SID;                    /**< \brief 10, Secondary ID Register*/
       Ifx_1EDI2002AS_SSTAT                SSTAT;                  /**< \brief 11, Secondary Status Register*/
       Ifx_1EDI2002AS_SSTAT2               SSTAT2;                 /**< \brief 12, Secondary Second Status Register*/
       Ifx_1EDI2002AS_SER                  SER;                    /**< \brief 13, Secondary Error Register*/
       Ifx_1EDI2002AS_SCFG                 SCFG;                   /**< \brief 14, Secondary Configuration Register*/
       Ifx_1EDI2002AS_SCFG2                SCFG2;                  /**< \brief 15, Secondary Second Configuration Register*/
    unsigned short        reserved_16[4]; /**< \brief FIXME to do implementation */
       Ifx_1EDI2002AS_SRTTOF               SRTTOF;                 /**< \brief 1A, Secondary Regular TTOFF Configuration Register*/
       Ifx_1EDI2002AS_SSTTOF               SSTTOF;                 /**< \brief 1B, Secondary Safe TTOFF Configuration Register*/
    unsigned short        reserved_1B[2]; /**< \brief FIXME to do implementation */
       Ifx_1EDI2002AS_SACLT                SACLT;                  /**< \brief 1E, Secondary Active Clamping Configuration Register*/
    unsigned short        reserved_1F[1]; /**< \brief FIXME to do implementation */
} Ifx_1EDI2002AS;

/** \}  */
/******************************************************************************/
/** \}  */
/******************************************************************************/
/******************************************************************************/
IFX_EXTERN uint32 Ifx_g_1EDI2002AS_validAddress[1];

IFX_EXTERN uint32 Ifx_g_1EDI2002AS_volatileRegisters[1];

IFX_EXTERN uint32 Ifx_g_1EDI2002AS_actionReadRegisters[1];
IFX_EXTERN uint32 Ifx_g_1EDI2002AS_actionWriteRegisters[1];

#endif /* IFX1EDI2002AS_REGDEF_H */
