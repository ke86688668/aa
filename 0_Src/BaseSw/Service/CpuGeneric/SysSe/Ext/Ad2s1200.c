/*
 * \file Ad2s1200.c
 * \brief Ad2s1200 resolver interface.
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

//________________________________________________________________________________________
// INCLUDES

#include "Ad2s1200.h"
#include "_Utilities/Ifx_Assert.h"
#include "SysSe/Comm/Ifx_Console.h"
#include <stddef.h>
#include "string.h"

//________________________________________________________________________________________
// LOCAL FUNCTION PROTOTYPES

sint32      Ad2s1200_resetOffset(Ad2s1200 *driver);
float32     Ad2s1200_updateSpeed(Ad2s1200 *driver, boolean update);
void        Ad2s1200_setOffset(Ad2s1200 *driver, sint32 offset);
void        Ad2s1200_Serial_update(Ad2s1200 *driver);
void        Ad2s1200_Parallel_update(Ad2s1200 *driver);
void        Ad2s1200_ExtBus_update(Ad2s1200 *driver);
static void Ad2s1200_setupSpeedConst(Ad2s1200 *driver);

static void Ad2s1200_setMode(Ad2s1200 *driver, Ad2s1200_OperationMode mode);
static void Ad2s1200_setResolution(Ad2s1200 *driver, Ad2s1200_Resolution resolution);

//________________________________________________________________________________________
// VARIABLES

//________________________________________________________________________________________
// CONSTANTS

/******************************************************************************/
/*-------------------------Function Implementations---------------------------*/
/******************************************************************************/
sint32 Ad2s1200_getResolutionBit(Ad2s1200_Resolution resolution)
{
    sint32 result = 0;

    switch (resolution)
    {
    case Ad2s1200_Resolution_12Bits:
        result = 4096;
        break;
    }

    return result;
}


float32 Ad2s1200_getAbsolutePosition(Ad2s1200 *driver)
{
    return ((float32)driver->turn + (float32)driver->position / (float32)driver->resolution) * 2.0 * IFX_PI;
}


IfxStdIf_Pos_Dir Ad2s1200_getDirection(Ad2s1200 *driver)
{
    return driver->direction;
}


IfxStdIf_Pos_Status Ad2s1200_getFault(Ad2s1200 *driver)
{
    return driver->stdifStatus;
}


sint32 Ad2s1200_getOffset(Ad2s1200 *driver)
{
    return driver->offset;
}


uint16 Ad2s1200_getPeriodPerRotation(Ad2s1200 *driver)
{
    return driver->periodPerRotation;
}


float32 Ad2s1200_getRefreshPeriod(Ad2s1200 *driver)
{
    return 0.0; /* FIXME */
}


IfxStdIf_Pos_SensorType Ad2s1200_getSensorType(Ad2s1200 *driver)
{
    return IfxStdIf_Pos_SensorType_resolver;
}


float32 Ad2s1200_getSpeed(Ad2s1200 *driver)
{
    return driver->speed;
}


IFX_INLINE void Ad2s1200_initInputPort(const IfxPort_Pin *pin)
{
    Pin_setMode(pin, IfxPort_Mode_inputNoPullDevice);
    //Pin_setDriver(pin, IfxPort_PadDriver_strongSharp);
}


IFX_INLINE void Ad2s1200_initOutputPort(const IfxPort_Pin *pin, IfxPort_State initiallevel)
{
    Pin_setState(pin, initiallevel);
    Pin_setMode(pin, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(pin, PIN_DRIVER_STRONG_SHARP);
}


IFX_INLINE void Port_print(const IfxPort_Pin *pin, pchar name)
{
    if (pin->port != NULL_PTR)
    {
        Ifx_Console_printAlign("- %8s  : P%d.%d"ENDL, name, IfxPort_getIndex(pin->port), pin->pinIndex);
    }
}


IFX_INLINE void Port_printGroup(const IfxPort_Pin *pin, pchar name, uint32 pins)
{
    if (pin->port != NULL_PTR)
    {
        Ifx_Console_printAlign("- %s[0..%d] : P%d.[%d..%d]"ENDL, name, (pins - 1), IfxPort_getIndex(pin->port), pin->pinIndex, (pin->pinIndex + pins - 1));
    }
}


static void Ad2s1200_initCommon(Ad2s1200 *driver, const Ad2s1200_Config *config)
{
    const Ad2s1200_Common *common = &config->modes.common;

    Ad2s1200_initOutputPort(&common->RDVEL, IfxPort_State_high);
    Ad2s1200_initOutputPort(&common->RESET, IfxPort_State_low);

    if (common->FS1.port != NULL_PTR)
    {
        Ad2s1200_initOutputPort(&common->FS1, IfxPort_State_high);
    }

    if (common->FS2.port != NULL_PTR)
    {
        Ad2s1200_initOutputPort(&common->FS2, IfxPort_State_high);
    }

#if PRINT_RESOURCE_USED
    Port_print(&common->RDVEL, "RDVEL");
    Port_print(&common->FS1, "FS1");
    Port_print(&common->FS1, "FS2");
#endif

    driver->modes.selected = config->modes.selected;
    driver->modes.common   = config->modes.common;
    driver->speedReload    = config->speedUpdateFactor;
    driver->speedCounter   = driver->speedReload;
    driver->state          = Ad2s1200_State_reset;
    {
        driver->offset                        = config->offset;
        driver->periodPerRotation             = config->periodPerRotation;
        driver->resolution                    = Ad2s1200_getResolutionBit(Ad2s1200_Resolution_12Bits);
        driver->reversed                      = config->reversed;
        driver->position                      = 0;
        driver->speed                         = 0;
        driver->direction                     = IfxStdIf_Pos_Dir_unknown;
        driver->turn                          = 0;
        driver->stdifStatus.status            = 0;
#if AD2S1200_DEBUG
        driver->diag.byType.signalLoss        = 0;
        driver->diag.byType.signalDegradation = 0;
        driver->diag.byType.trackingLoss      = 0;
        driver->diag.byType.commError         = 0;
        driver->diag.byType.noError           = 0;
#endif

        Ad2s1200_setupSpeedConst(driver);
    }
}


static void Ad2s1200_initSerial(Ad2s1200 *driver, const Ad2s1200_Config *config)
{
    const Ad2s1200_Serial *interface = &config->modes.serial;
    driver->modes.i.serial = *interface;
    Ad2s1200_initOutputPort(&interface->RDn, IfxPort_State_high);
    Ad2s1200_initOutputPort(&interface->CSn, IfxPort_State_high);
}


static void Ad2s1200_initExtBus(Ad2s1200 *driver, const Ad2s1200_Config *config)
{
    const Ad2s1200_Ebu *interface = config->modes.extbus;
    Ad2s1200_initInputPort(&interface->DOS);
    Ad2s1200_initInputPort(&interface->LOT);
#if PRINT_RESOURCE_USED
    Port_print(&interface->LOT, "LOT");
    Port_print(&interface->DOS, "DOS");
#endif

    driver->modes.i.extbus = *interface;
}


static void Ad2s1200_initParallel(Ad2s1200 *driver, const Ad2s1200_Config *config)
{
    const Ad2s1200_Par *interface = config->modes.parallel;
    Pin_setGroupModeInput(&interface->DB0, 0xFFFFU, IfxPort_InputMode_noPullDevice);
    Ad2s1200_initOutputPort(&interface->CSn, IfxPort_State_high);
    Ad2s1200_initOutputPort(&interface->RDn, IfxPort_State_high);
    Ad2s1200_initInputPort(&interface->DOS);
    Ad2s1200_initInputPort(&interface->LOT);
#if PRINT_RESOURCE_USED
    Port_print(&interface->LOT, "LOT");
    Port_print(&interface->DOS, "DOS");
    Port_print(&interface->CSn, "CSn");
    Port_printGroup(&interface->DB0, "DB", driver->dataBits);
#endif

    driver->modes.i.parallel = *interface;
}


void Ad2s1200_initConfig(Ad2s1200_Config *config)
{
    config->offset                    = 0;
    config->periodPerRotation         = 1;
    config->reversed                  = FALSE;
    config->frequency                 = 10000;
    config->updateFrequency           = 10000;
    memset(&config->modes, 0, sizeof(config->modes));
    config->modes.selected            = Ad2s1200_Mode_parallel;
    config->speedFilerCutOffFrequency = 0.0;
    config->speedUpdateFactor         = 10;
}


/** \brief Initialise the resolver object.
 *
 * FIXME Ad2s1200_initConfig() required. the initConfig shall also initialize the required SSC parameters
 *
 * This function calls Ad2s1200_initHw() and Ad2s1200_initSw().
 *
 * \param driver Specifies resolver object. This parameter is filled in by the function.
 * \param config Specifies resolver object configuration.
 *
 * \retval TRUE Returns TRUE if the initialisation was successful
 * \retval FALSE Returns FALSE if the resolver could not be initialise
 *
 * \see Ad2s1200_initHw(), Ad2s1200_initSw()
 */
boolean Ad2s1200_init(Ad2s1200 *driver, const Ad2s1200_Config *config)
{
    boolean result = TRUE;

#if PRINT_RESOURCE_USED
    Ifx_Console_printAlign("- AD2S120x driver interface:"ENDL);
    Ifx_Console_incrAlign(0);
#endif
    Ad2s1200_initCommon(driver, config);
    driver->dataBits = 10 + (2 * Ad2s1200_Resolution_12Bits);

    if (config->modes.selected == Ad2s1200_Mode_serial)
    {
        Ad2s1200_initSerial(driver, config);
    }
    else if (config->modes.selected == Ad2s1200_Mode_extbus)
    {
        Ad2s1200_initExtBus(driver, config);
    }
    else if (config->modes.selected == Ad2s1200_Mode_parallel)
    {
        Ad2s1200_initParallel(driver, config);
    }
    else
    {}

    /* Default frequency is 10kHz. Set new frequency as specified by configuration */
    Ad2s1200_setFrequency(driver, config->frequency);

    /* Set the resolution as specified by configuration */
    Ad2s1200_setResolution(driver, Ad2s1200_Resolution_12Bits);

#if PRINT_RESOURCE_USED
    Ifx_Console_decrAlign(0);
#endif

    driver->speedFilterEnabled = config->speedFilerCutOffFrequency > 0.0;

    if (driver->speedFilterEnabled != FALSE)
    {
        Ifx_LowPassPt1F32_Config filterConfig;            /**< \brief PT1 filter configuration for speed  */
        filterConfig.cutOffFrequency = config->speedFilerCutOffFrequency;
        filterConfig.gain            = 1.0;
        filterConfig.samplingTime    = (1.0 / config->updateFrequency) * config->speedUpdateFactor;

        Ifx_LowPassPt1F32_init(&driver->speedLpf, &filterConfig);
    }

    return result;
}


/** \brief reset and enable the resolver.
 * FIXME there is a need to have a defined start when a NULL pointer is passed for waitFunction. the application can not ensure that the first call is done with Ad2s1200_State_startUp. additinal paramater required
 * \param driver Specifies resolver object.
 * \param waitFunction Specifies wait function to be used.
 *             If NULL_PTR, each call to the function executes the next reset step, 3 calls
 *             are required to finish the reset sequence.
 *
 * \retval Ad2s1200_State_reset if the resolver reset signal is active (Min time 10us)
 * \retval Ad2s1200_State_startUp if the resolver is powering up (Min time 20ms)
 * \retval Ad2s1200_State_ready if the reset sequence is finished.
 */
Ad2s1200_State Ad2s1200_enable(Ad2s1200 *driver, WaitTimeFunction waitFunction)
{
    if (waitFunction != NULL_PTR)
    {
        Pin_setStateLow(&driver->modes.common.RESET);
        driver->state = Ad2s1200_State_reset;
        waitFunction(TimeConst_10us);            // Wait 10us for the RESET\low signal

        Pin_setStateHigh(&driver->modes.common.RESET);
        driver->state = Ad2s1200_State_startUp;
        waitFunction(TimeConst_10ms * 2);        // Wait 20ms before resolver ready status

        driver->state = Ad2s1200_State_ready;
    }
    else
    {
        if (driver->state == Ad2s1200_State_startUp)
        {
            driver->state = Ad2s1200_State_ready;
        }
        else if (driver->state == Ad2s1200_State_reset)
        {
            Pin_setStateHigh(&driver->modes.common.RESET);
            driver->state = Ad2s1200_State_startUp;
        }
        else
        {
            Pin_setStateLow(&driver->modes.common.RESET);
            driver->state = Ad2s1200_State_reset;
        }
    }

    return driver->state;
}


/** \brief reset and enable the resolver.
 *
 * \param driver Specifies resolver object.
 *
 * \retval None. See Ad2s1200->state
 */
void Ad2s1200_reset(Ad2s1200 *driver)
{
    Ad2s1200_enable(driver, NULL_PTR);          /* activate reset pin*/
    Ad2s1200_enable(driver, NULL_PTR);          /* deactivate reset pin */
    driver->position                      = 0;
    driver->turn                          = 0;
    driver->speed                         = 0;
    driver->stdifStatus.status            = 0;
    driver->stdifStatus.B.notSynchronised = 1;
    Ad2s1200_enable(driver, NULL_PTR);   /* ready state */
}


void Ad2s1200_resetFaults(Ad2s1200 *driver)
{   /* FIXME Implement reset fault only */
    Ad2s1200_reset(driver);
}


void Ad2s1200_setRefreshPeriod(Ad2s1200 *driver, float32 updatePeriod)
{
    /* FIXME make use of updatePeriod */
    Ad2s1200_setupSpeedConst(driver);
}


boolean Ad2s1200_stdIfPosInit(IfxStdIf_Pos *stdif, Ad2s1200 *driver)
{
    /* Ensure the stdif is reset to zeros */
    memset(stdif, 0, sizeof(IfxStdIf_Pos));

    /* Set the driver */
    stdif->driver = driver;

    /* *INDENT-OFF* Note: this file was indented manually by the author. */
    /* Set the API link */
	stdif->onZeroIrq          =(IfxStdIf_Pos_OnZeroIrq               )NULL_PTR; /* NO zero signal */
	stdif->getAbsolutePosition=(IfxStdIf_Pos_GetAbsolutePosition     )&Ad2s1200_getAbsolutePosition;
	stdif->getDirection		  =(IfxStdIf_Pos_GetDirection		     )&Ad2s1200_getDirection;
	stdif->getFault           =(IfxStdIf_Pos_GetFault			     )&Ad2s1200_getFault;
	stdif->getPeriodPerRotation =(IfxStdIf_Pos_GetPeriodPerRotation  )&Ad2s1200_getPeriodPerRotation;
	stdif->getOffset		  =(IfxStdIf_Pos_GetOffset			     )&Ad2s1200_getOffset;
	stdif->getPosition		  =(IfxStdIf_Pos_GetPosition			 )&Ad2s1200_getPosition;
	stdif->getRawPosition	  =(IfxStdIf_Pos_GetRawPosition          )&Ad2s1200_getRawPosition;
	stdif->getRefreshPeriod   =(IfxStdIf_Pos_GetRefreshPeriod        )&Ad2s1200_getRefreshPeriod;
	stdif->getResolution      =(IfxStdIf_Pos_GetResolution           )&Ad2s1200_getResolution;
	stdif->getSensorType      =(IfxStdIf_Pos_GetSensorType           )&Ad2s1200_getSensorType;
	stdif->reset			  =(IfxStdIf_Pos_Reset				     )&Ad2s1200_reset;
	stdif->resetFaults		  =(IfxStdIf_Pos_ResetFaults			 )&Ad2s1200_resetFaults;
	stdif->getSpeed           =(IfxStdIf_Pos_GetSpeed                )&Ad2s1200_getSpeed;
	stdif->getTurn            =(IfxStdIf_Pos_GetTurn                 )&Ad2s1200_getTurn;
	switch (driver->modes.selected)
	{
	case Ad2s1200_Mode_serial:
		stdif->update			  =(IfxStdIf_Pos_Update				     )&Ad2s1200_Serial_update;
		break;
	case Ad2s1200_Mode_parallel:
		stdif->update			  =(IfxStdIf_Pos_Update				     )&Ad2s1200_Parallel_update;
		break;
	case Ad2s1200_Mode_extbus:
		stdif->update			  =(IfxStdIf_Pos_Update				     )&Ad2s1200_ExtBus_update;
		break;
	}
	stdif->setOffset		  =(IfxStdIf_Pos_SetOffset			     )&Ad2s1200_setOffset;
	stdif->setRefreshPeriod   =(IfxStdIf_Pos_SetRefreshPeriod        )&Ad2s1200_setRefreshPeriod;
    /* *INDENT-ON* */

    return TRUE;
}


IFX_INLINE boolean Ad2s1200_Serial_isParityOk(uint16 Value)
{
    uint16 numOfOnes = 0;
    uint16 mask      = 0x0001u;  /* start at first bit */

    while (mask != 0)            /* until all bits tested */
    {
        if ((mask & Value) != 0) /* if bit is 1, increment numOfOnes */
        {
            numOfOnes++;
        }

        mask = mask << 1; /* go to next bit */
    }

    /* if numOfOnes is odd, least significant bit will be 1 */
    return (numOfOnes & 1) != 0;
}


#if AD2S1200_DEBUG
#define Ad2s1200_countError(index) driver->diag.byIndex[index]++
#else
#define Ad2s1200_countError(index)
#endif

IFX_INLINE boolean Ad2s1200_updatePositionPrivate(Ad2s1200 *driver, sint32 rawPosition, boolean result)
{
    if (driver->reversed != FALSE)
    {
        rawPosition  = -rawPosition;
        rawPosition += (sint16)driver->resolution;
    }

    rawPosition += (sint16)driver->offset;
    rawPosition &= (sint16)driver->resolution - 1;

    //if (result != FALSE)
    {   // Although result is FALSE (error occurred), always assign the result..
        // because in some cases the value is still usable.
        // It's up to the user to decide based on the error status.
        driver->position = rawPosition;
    }

    if (driver->status == Ad2s1200_Status_signalDegradation)
    {
        driver->stdifStatus.B.signalDegradation = 1;
        Ad2s1200_countError(1);
    }
    else if (driver->status == Ad2s1200_Status_signalLoss)
    {
        driver->stdifStatus.B.signalLoss = 1;
        Ad2s1200_countError(0);
    }

    if (driver->status == Ad2s1200_Status_trackingLoss)
    {
        driver->stdifStatus.B.trackingLoss = 1;
        Ad2s1200_countError(2);
    }

    /* FIXME Should other error flags be checked ? */
    return result;
}


static boolean Ad2s1200_updateSpeedPrivate(Ad2s1200 *driver, sint16 rawSpeed, boolean result)
{
    if (driver->reversed != FALSE)
    {
        rawSpeed = -rawSpeed;
    }

    if ((rawSpeed >= -1) && (rawSpeed <= 1))
    {   // Increase null speed stability
        rawSpeed = 0;
    }

    if (result != FALSE)
    {
        float32 speed = driver->speedConstPulseCount * rawSpeed;

        if (driver->speedFilterEnabled == TRUE)
        {
            speed = Ifx_LowPassPt1F32_do(&driver->speedLpf, speed);
        }

        driver->speed     = speed;
        driver->direction = (driver->speed < 0) ? IfxStdIf_Pos_Dir_backward : IfxStdIf_Pos_Dir_forward;
    }

    return result;
}


/* Raw position value is on DB(16-resolution) up to DB15 */
static boolean Ad2s1200_Parallel_updatePosition(Ad2s1200 *driver)
{
    boolean       result    = TRUE;
    sint32        rawPosition;
    Ad2s1200_Par *interface = &driver->modes.i.parallel;

    Pin_setStateLow(&interface->CSn);
    Pin_setStateLow(&interface->RDn);
    AD2S1200_RD_TO_DATA_WAIT;
    rawPosition = Pin_getGroupState(&interface->DB0, 0xFFFU);
    Pin_setStateHigh(&interface->RDn);
    Pin_setStateHigh(&interface->CSn);

    driver->status = (Pin_getState(&interface->DOS) ? 0x2 : 0x0) |
                     (Pin_getState(&interface->LOT) ? 0x1 : 0x0);

    result = Ad2s1200_updatePositionPrivate(driver, rawPosition, result);

    return result;
}


/* Raw position value is on DB(16-resolution) up to DB15 */
static boolean Ad2s1200_Parallel_updateSpeed(Ad2s1200 *driver)
{
    sint16        rawSpeed;
    boolean       result    = TRUE;
    Ad2s1200_Par *interface = &driver->modes.i.parallel;

    Ad2s1200_setMode(driver, Ad2s1200_OperationMode_normalVelocity);
    Pin_setStateLow(&interface->CSn);
    Pin_setStateLow(&interface->RDn);
    // Ad2s1200: 12ns required between CSn & RDn to Data valid => this is 2 cycles \180Mhz
    AD2S1200_RD_TO_DATA_WAIT;
    rawSpeed = (sint16)Pin_getGroupState(&interface->DB0, 0xFFFU);
    Pin_setStateHigh(&interface->RDn);
    Pin_setStateHigh(&interface->CSn);
    Ad2s1200_setMode(driver, Ad2s1200_OperationMode_normalPosition);

    // Extend sign to data size
    rawSpeed = rawSpeed << (16 - driver->dataBits);
    rawSpeed = rawSpeed >> (16 - driver->dataBits);

    result   = Ad2s1200_updateSpeedPrivate(driver, rawSpeed, result);
    return result;
}


static boolean Ad2s1200_Serial_updatePosition(Ad2s1200 *driver)
{
    boolean          result = TRUE;
    uint16           tempData;
    sint16           rawPosition;
    Ad2s1200_Serial *interface = &driver->modes.i.serial;

    //Pin_setStateLow(&interface->CSn); /* FIXME Ensured by serial interface */
    // mode is default Ad2s1200_OperationMode_normalPosition
    SpiIf_exchange(interface->channel, &tempData, &tempData, 1);
    SpiIf_wait(interface->channel);
    //Pin_setStateHigh(&interface->CSn);

    driver->status = (tempData >> 1) & 0x3;

    if (Ad2s1200_Serial_isParityOk(tempData) == FALSE)
    {
        driver->stdifStatus.B.commError = 1;
        Ad2s1200_countError(4);
    }

    // Check that the RDVEL is high (position)
    if (__getbit(&tempData, 3) != 1)
    {
        result = FALSE;
    }

    rawPosition = tempData;
    rawPosition = (rawPosition >> (16 - driver->dataBits));
    result      = Ad2s1200_updatePositionPrivate(driver, rawPosition, result);

    return result;
}


static boolean Ad2s1200_Serial_updateSpeed(Ad2s1200 *driver)
{
    boolean          result = TRUE;
    sint16           rawSpeed;
    uint16           tempData;
    Ad2s1200_Serial *interface = &driver->modes.i.serial;

    Ad2s1200_setMode(driver, Ad2s1200_OperationMode_normalVelocity);
    Pin_setStateLow(&interface->CSn);
    SpiIf_exchange(interface->channel, &tempData, &tempData, 1);
    SpiIf_wait(interface->channel);
    Pin_setStateHigh(&interface->CSn);
    Ad2s1200_setMode(driver, Ad2s1200_OperationMode_normalPosition);

    if (__getbit(&tempData, 3) != 0)
    {
        result = FALSE;
    }                                                    // Check that the RDVEL is low (speed)

    // Extend sign to data size
    rawSpeed = tempData;
    rawSpeed = rawSpeed << (16 - driver->dataBits);
    rawSpeed = rawSpeed >> (16 - driver->dataBits);

    result   = Ad2s1200_updateSpeedPrivate(driver, rawSpeed, result);
    return result;
}


static boolean Ad2s1200_ExtBus_updatePosition(Ad2s1200 *driver)
{
    sint32  rawPosition;
    boolean result = TRUE;

    rawPosition    = (sint16)__ld32(driver->modes.i.extbus.data);
#if AD2S1200_LOT_DOS_PIN_ENABLED == 0
    driver->status = (rawPosition >> 12) & 0x3;
#endif

    result = Ad2s1200_updatePositionPrivate(driver, rawPosition, result);

    return result;
}


static boolean Ad2s1200_ExtBus_updateSpeed(Ad2s1200 *driver)
{
    sint16  rawSpeed;
    boolean result = TRUE;

#if AD2S1200_EXT_BUS_AUTO_SELECT != 0
    rawSpeed = (__ld32(driver->modes.i.extbus.dataVel) & (driver->resolution - 1));
#else
    Ad2s1200_setMode(driver, Ad2s1200_OperationMode_normalVelocity);
    rawSpeed = (sint16)(__ld32(driver->modes.i.extbus.data) & (driver->resolution - 1));
    Ad2s1200_setMode(driver, Ad2s1200_OperationMode_normalPosition);
#endif
    // Extend sign to data size
    rawSpeed = rawSpeed << (16 - driver->dataBits);

    result   = Ad2s1200_updateSpeedPrivate(driver, rawSpeed, result);
    return result;
}


/** \brief Update the resolver position, speed and direction.
 *
 * This function updates the resolver position and direction.
 * This function also updates the speed periodically.
 *
 * \param driver Specifies resolver object.
 *
 * \return None
 */
void Ad2s1200_Serial_update(Ad2s1200 *driver)
{
    Ad2s1200_Serial_updatePosition(driver);

    driver->speedCounter--;

    if (driver->speedCounter <= 0)
    {
        driver->speedCounter = driver->speedReload;
        Ad2s1200_Serial_updateSpeed(driver);
    }
}


/** \brief Update the resolver position, speed and direction.
 *
 * This function updates the resolver position and direction.
 * This function also updates the speed periodically.
 *
 * \param driver Specifies resolver object.
 *
 * \return None
 */
void Ad2s1200_Parallel_update(Ad2s1200 *driver)
{
    Ad2s1200_Parallel_updatePosition(driver);

    driver->speedCounter--;

    if (driver->speedCounter <= 0)
    {
        driver->speedCounter = driver->speedReload;
        Ad2s1200_Parallel_updateSpeed(driver);
    }
}


/** \brief Update the resolver position, speed and direction.
 *
 * This function updates the resolver position and direction.
 * This function also updates the speed periodically.
 *
 * \param driver Specifies resolver object.
 *
 * \return None
 */
void Ad2s1200_ExtBus_update(Ad2s1200 *driver)
{
    Ad2s1200_ExtBus_updatePosition(driver);

    driver->speedCounter--;

    if (driver->speedCounter <= 0)
    {
        driver->speedCounter = driver->speedReload;
        Ad2s1200_ExtBus_updateSpeed(driver);
    }
}


/** \brief Set the resolver oscillator frequency
 *
 * Range in Hz = {10000, 12000, 15000, 20000}.
 * In case a value different from the given range is selected, the next higher frequency is used.
 * In case FS1 or FS2 is not used (\see Ad2s1200_init), the pin is ignored for the setting.
 *
 * \param driver Specifies resolver object.
 * \param frequency Specifies resolver oscillator frequency in Hz.
 *
 * \return return the selected frequency in Hz.
 */
sint32 Ad2s1200_setFrequency(Ad2s1200 *driver, sint32 frequency)
{
    Ad2s1200_Common *common = &driver->modes.common;

    if (frequency <= 10000)
    {   // 10 kHz
        if (common->FS1.port != NULL_PTR)
        {
            Pin_setStateHigh(&common->FS1);
        }

        if (common->FS2.port != NULL_PTR)
        {
            Pin_setStateHigh(&common->FS2);
        }

        frequency = 10000;
    }
    else if (frequency <= 12000)
    {   // 12 kHz
        if (common->FS1.port != NULL_PTR)
        {
            Pin_setStateHigh(&common->FS1);
        }

        if (common->FS2.port != NULL_PTR)
        {
            Pin_setStateLow(&common->FS2);
        }

        frequency = 12000;
    }
    else if (frequency <= 15000)
    {   // 15 kHz
        if (common->FS1.port != NULL_PTR)
        {
            Pin_setStateLow(&common->FS1);
        }

        if (common->FS2.port != NULL_PTR)
        {
            Pin_setStateHigh(&common->FS2);
        }

        frequency = 15000;
    }
    else
    {   // 20 kHz
        if (common->FS1.port != NULL_PTR)
        {
            Pin_setStateLow(&common->FS1);
        }

        if (common->FS2.port != NULL_PTR)
        {
            Pin_setStateLow(&common->FS2);
        }

        frequency = 20000;
    }

    return frequency;
}


void Ad2s1200_setOffset(Ad2s1200 *driver, sint32 offset)
{
	while (offset <0)
	{ /* Make sure the offset is positive */
		offset += driver->resolution;
	}
    driver->position = driver->position - driver->offset + offset;

    driver->offset = offset;
}


float32 Ad2s1200_updateSpeed(Ad2s1200 *driver, boolean update)
{   /* FIXME shall also update the speed */
    return driver->speed;
}


sint32 Ad2s1200_resetOffset(Ad2s1200 *driver)
{   /* FIXME obsolete */
    sint32 offset;
    offset = (driver->position - driver->offset);
    offset = -(offset & (driver->resolution - 1));
    Ad2s1200_setOffset(driver, offset);
    offset = driver->offset;
    return offset;
}


static void Ad2s1200_setMode(Ad2s1200 *driver, Ad2s1200_OperationMode mode)
{
    if (mode == Ad2s1200_OperationMode_normalVelocity)
    {
        Pin_setStateLow(&driver->modes.common.RDVEL);
    }
    else
    {
        Pin_setStateHigh(&driver->modes.common.RDVEL);
    }
}


Ad2s1200_OperationMode Ad2s1200_getMode(Ad2s1200 *driver)
{
    Ad2s1200_OperationMode result;
    Ad2s1200_Common       *common = &driver->modes.common;
    result = ((!Pin_getState(&common->RDVEL)) << 1);
    return result;
}


void Ad2s1200_setResolution(Ad2s1200 *driver, Ad2s1200_Resolution resolution)
{
    resolution         = Ad2s1200_Resolution_12Bits; /* Only supported config */
    driver->dataBits   = 10 + (2 * resolution);
    driver->resolution = (sint32)(1U << driver->dataBits);
    Ad2s1200_setupSpeedConst(driver);
}


static void Ad2s1200_setupSpeedConst(Ad2s1200 *driver)
{
    float32 speedFullScale;
    speedFullScale               = AD2S1200_SPEED_FULL_SCALE_12BITS;
    driver->speedConstPulseCount = speedFullScale / (driver->periodPerRotation * driver->resolution / 2);
}


/**
 * Return the selected resolution
 */
sint32 Ad2s1200_getResolution(Ad2s1200 *driver)
{
    return Ad2s1200_getResolutionBit(Ad2s1200_Resolution_12Bits);
}
