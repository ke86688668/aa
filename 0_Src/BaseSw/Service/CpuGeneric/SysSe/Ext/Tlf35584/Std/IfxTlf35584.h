/**
 * \file IfxTlf35584.h
 * \brief TLF35584  basic functionality 
 * \ingroup IfxLld_Tlf35584 
 *
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved.
 *
 * $Date: 2017-05-15 04:24:16
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_Tlf35584_Std_Std Std  
 * \ingroup IfxLld_Tlf35584_Std
 */

#ifndef IFXTLF35584_H
#define IFXTLF35584_H 1

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "SysSe/Ext/Tlf35584/_Impl/IfxTlf35584_cfg.h"
#include "SysSe/Ext/Tlf35584/Driver/IfxTlf35584_Driver.h"
#include "SysSe/Ext/Tlf35584/_Reg/IfxTlf35584_bf.h"

/** \addtogroup IfxLld_Tlf35584_Std_Std
 * \{ */

/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \brief Get Transition delay into low power states
 * Bitfield: DEVCFG0.TRDEL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Transition delay into low power states  
 */
IFX_EXTERN IfxTlf35584_TransitionDelayIntoLowPowerStates IfxTlf35584_getTransitionDelayIntoLowPowerStates(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Transition delay into low power states
 * Bitfield: DEVCFG0.TRDEL 
 * \param driver Device driver object  
 * \param value Transition delay into low power states  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setTransitionDelayIntoLowPowerStates(IfxTlf35584_Driver* driver, IfxTlf35584_TransitionDelayIntoLowPowerStates value, boolean* status);

/** \brief Set Transition delay into low power states in Us
 * Bitfield: DEVCFG0.TRDEL 
 * \param driver Device driver object  
 * \param value Transition delay into low power states  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setTransitionDelayIntoLowPowerStatesUs(IfxTlf35584_Driver* driver, sint16 value, boolean* status);

/** \brief Get Transition delay into low power states in Us
 * Bitfield: DEVCFG0.TRDEL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Transition delay into low power states  
 */
IFX_EXTERN sint16 IfxTlf35584_getTransitionDelayIntoLowPowerStatesUs(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Wake timer cycle period
 * Bitfield: DEVCFG0.WKTIMCYC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Wake timer cycle period  
 */
IFX_EXTERN IfxTlf35584_WakeTimerCyclePeriod IfxTlf35584_getWakeTimerCyclePeriod(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Wake timer cycle period
 * Bitfield: DEVCFG0.WKTIMCYC 
 * \param driver Device driver object  
 * \param value Wake timer cycle period  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setWakeTimerCyclePeriod(IfxTlf35584_Driver* driver, IfxTlf35584_WakeTimerCyclePeriod value, boolean* status);

/** \brief Get Wake timer enable
 * Bitfield: DEVCFG0.WKTIMEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Wake timer enable  
 */
IFX_EXTERN IfxTlf35584_WakeTimerEnable IfxTlf35584_getWakeTimerEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Wake timer enable
 * Bitfield: DEVCFG0.WKTIMEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isWakeTimerEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Wake timer enable
 * Bitfield: DEVCFG0.WKTIMEN 
 * \param driver Device driver object  
 * \param value Wake timer enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setWakeTimerEnable(IfxTlf35584_Driver* driver, IfxTlf35584_WakeTimerEnable value, boolean* status);

/** \brief Set Wake timer enable [Enable]
 * Bitfield: DEVCFG0.WKTIMEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableWakeTimer(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Wake timer enable [Disable]
 * Bitfield: DEVCFG0.WKTIMEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableWakeTimer(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Reset release delay time
 * Bitfield: DEVCFG1.RESDEL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Reset release delay time  
 */
IFX_EXTERN IfxTlf35584_ResetReleaseDelayTime IfxTlf35584_getResetReleaseDelayTime(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Reset release delay time
 * Bitfield: DEVCFG1.RESDEL 
 * \param driver Device driver object  
 * \param value Reset release delay time  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setResetReleaseDelayTime(IfxTlf35584_Driver* driver, IfxTlf35584_ResetReleaseDelayTime value, boolean* status);

/** \brief Get Synchronization output for external switchmode regulator\nenable
 * Bitfield: DEVCFG2.ESYNEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Synchronization output for external switchmode regulator\nenable  
 */
IFX_EXTERN IfxTlf35584_SynchronizationOutputForExternalSwitchmodeRegulatorEnable IfxTlf35584_getSynchronizationOutputForExternalSwitchmodeRegulatorEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Synchronization output for external switchmode regulator\nenable
 * Bitfield: DEVCFG2.ESYNEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSynchronizationOutputForExternalSwitchmodeRegulatorEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Synchronization output for external switchmode regulator\nenable
 * Bitfield: DEVCFG2.ESYNEN 
 * \param driver Device driver object  
 * \param value Synchronization output for external switchmode regulator\nenable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSynchronizationOutputForExternalSwitchmodeRegulatorEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SynchronizationOutputForExternalSwitchmodeRegulatorEnable value, boolean* status);

/** \brief Set Synchronization output for external switchmode regulator\nenable [Enable]
 * Bitfield: DEVCFG2.ESYNEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSynchronizationOutputForExternalSwitchmodeRegulator(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Synchronization output for external switchmode regulator\nenable [Disable]
 * Bitfield: DEVCFG2.ESYNEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSynchronizationOutputForExternalSwitchmodeRegulator(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get External synchronization output phase
 * Bitfield: DEVCFG2.ESYNPHA 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return External synchronization output phase  
 */
IFX_EXTERN IfxTlf35584_ExternalSynchronizationOutputPhase IfxTlf35584_getExternalSynchronizationOutputPhase(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set External synchronization output phase
 * Bitfield: DEVCFG2.ESYNPHA 
 * \param driver Device driver object  
 * \param value External synchronization output phase  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setExternalSynchronizationOutputPhase(IfxTlf35584_Driver* driver, IfxTlf35584_ExternalSynchronizationOutputPhase value, boolean* status);

/** \brief Get QUC current monitoring threshold value
 * Bitfield: DEVCFG2.CTHR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return QUC current monitoring threshold value  
 */
IFX_EXTERN IfxTlf35584_QucCurrentMonitoringThresholdValue IfxTlf35584_getQucCurrentMonitoringThresholdValue(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set QUC current monitoring threshold value
 * Bitfield: DEVCFG2.CTHR 
 * \param driver Device driver object  
 * \param value QUC current monitoring threshold value  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setQucCurrentMonitoringThresholdValue(IfxTlf35584_Driver* driver, IfxTlf35584_QucCurrentMonitoringThresholdValue value, boolean* status);

/** \brief Get QUC current monitor enable for transition to a low\npower state
 * Bitfield: DEVCFG2.CMONEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return QUC current monitor enable for transition to a low\npower state  
 */
IFX_EXTERN IfxTlf35584_QucCurrentMonitorEnableForTransitionToALowPowerState IfxTlf35584_getQucCurrentMonitorEnableForTransitionToALowPowerState(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set QUC current monitor enable for transition to a low\npower state
 * Bitfield: DEVCFG2.CMONEN 
 * \param driver Device driver object  
 * \param value QUC current monitor enable for transition to a low\npower state  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setQucCurrentMonitorEnableForTransitionToALowPowerState(IfxTlf35584_Driver* driver, IfxTlf35584_QucCurrentMonitorEnableForTransitionToALowPowerState value, boolean* status);

/** \brief Get Step-down converter frequency selection status
 * Bitfield: DEVCFG2.FRE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Step-down converter frequency selection status  
 */
IFX_EXTERN IfxTlf35584_StepDownConverterFrequencySelectionStatus IfxTlf35584_getStepDownConverterFrequencySelectionStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Step-up converter enable status
 * Bitfield: DEVCFG2.STU 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Step-up converter enable status  
 */
IFX_EXTERN IfxTlf35584_StepUpConverterEnableStatus IfxTlf35584_getStepUpConverterEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Step-up converter enable status
 * Bitfield: DEVCFG2.STU 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isStepUpConverterEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get External core supply enable status
 * Bitfield: DEVCFG2.EVCEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return External core supply enable status  
 */
IFX_EXTERN IfxTlf35584_ExternalCoreSupplyEnableStatus IfxTlf35584_getExternalCoreSupplyEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get External core supply enable status
 * Bitfield: DEVCFG2.EVCEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isExternalCoreSupplyEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Protection key
 * Bitfield: PROTCFG.KEY 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Protection key  
 */
IFX_EXTERN IfxTlf35584_ProtectionKey IfxTlf35584_getProtectionKey(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Protection key
 * Bitfield: PROTCFG.KEY 
 * \param driver Device driver object  
 * \param value Protection key  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setProtectionKey(IfxTlf35584_Driver* driver, IfxTlf35584_ProtectionKey value, boolean* status);

/** \brief Get Request standby regulator QST enable
 * Bitfield: SYSPCFG0.STBYEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request standby regulator QST enable  
 */
IFX_EXTERN IfxTlf35584_RequestStandbyRegulatorQstEnable IfxTlf35584_getRequestStandbyRegulatorQstEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request standby regulator QST enable
 * Bitfield: SYSPCFG0.STBYEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isRequestStandbyRegulatorQstEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request standby regulator QST enable
 * Bitfield: SYSPCFG0.STBYEN 
 * \param driver Device driver object  
 * \param value Request standby regulator QST enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestStandbyRegulatorQstEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestStandbyRegulatorQstEnable value, boolean* status);

/** \brief Set Request standby regulator QST enable [Enable]
 * Bitfield: SYSPCFG0.STBYEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableRequestStandbyRegulatorQst(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request standby regulator QST enable [Disable]
 * Bitfield: SYSPCFG0.STBYEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableRequestStandbyRegulatorQst(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request ERR pin monitor recovery time
 * Bitfield: SYSPCFG1.ERRREC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request ERR pin monitor recovery time  
 */
IFX_EXTERN IfxTlf35584_RequestErrPinMonitorRecoveryTime IfxTlf35584_getRequestErrPinMonitorRecoveryTime(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request ERR pin monitor recovery time
 * Bitfield: SYSPCFG1.ERRREC 
 * \param driver Device driver object  
 * \param value Request ERR pin monitor recovery time  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestErrPinMonitorRecoveryTime(IfxTlf35584_Driver* driver, IfxTlf35584_RequestErrPinMonitorRecoveryTime value, boolean* status);

/** \brief Get Request ERR pin monitor recovery enable
 * Bitfield: SYSPCFG1.ERRRECEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request ERR pin monitor recovery enable  
 */
IFX_EXTERN IfxTlf35584_RequestErrPinMonitorRecoveryEnable IfxTlf35584_getRequestErrPinMonitorRecoveryEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request ERR pin monitor recovery enable
 * Bitfield: SYSPCFG1.ERRRECEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isRequestErrPinMonitorRecoveryEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request ERR pin monitor recovery enable
 * Bitfield: SYSPCFG1.ERRRECEN 
 * \param driver Device driver object  
 * \param value Request ERR pin monitor recovery enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestErrPinMonitorRecoveryEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestErrPinMonitorRecoveryEnable value, boolean* status);

/** \brief Set Request ERR pin monitor recovery enable [Enable]
 * Bitfield: SYSPCFG1.ERRRECEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableRequestErrPinMonitorRecovery(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request ERR pin monitor recovery enable [Disable]
 * Bitfield: SYSPCFG1.ERRRECEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableRequestErrPinMonitorRecovery(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request ERR pin monitor enable
 * Bitfield: SYSPCFG1.ERREN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request ERR pin monitor enable  
 */
IFX_EXTERN IfxTlf35584_RequestErrPinMonitorEnable IfxTlf35584_getRequestErrPinMonitorEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request ERR pin monitor enable
 * Bitfield: SYSPCFG1.ERREN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isRequestErrPinMonitorEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request ERR pin monitor enable
 * Bitfield: SYSPCFG1.ERREN 
 * \param driver Device driver object  
 * \param value Request ERR pin monitor enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestErrPinMonitorEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestErrPinMonitorEnable value, boolean* status);

/** \brief Set Request ERR pin monitor enable [Enable]
 * Bitfield: SYSPCFG1.ERREN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableRequestErrPinMonitor(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request ERR pin monitor enable [Disable]
 * Bitfield: SYSPCFG1.ERREN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableRequestErrPinMonitor(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request ERR pin monitor functionaility enable while\nthe system is in SLEEP
 * Bitfield: SYSPCFG1.ERRSLPEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request ERR pin monitor functionaility enable while\nthe system is in SLEEP  
 */
IFX_EXTERN IfxTlf35584_RequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep IfxTlf35584_getRequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request ERR pin monitor functionaility enable while\nthe system is in SLEEP
 * Bitfield: SYSPCFG1.ERRSLPEN 
 * \param driver Device driver object  
 * \param value Request ERR pin monitor functionaility enable while\nthe system is in SLEEP  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep(IfxTlf35584_Driver* driver, IfxTlf35584_RequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep value, boolean* status);

/** \brief Get Request safe state 2 delay
 * Bitfield: SYSPCFG1.SS2DEL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request safe state 2 delay  
 */
IFX_EXTERN IfxTlf35584_RequestSafeState2Delay IfxTlf35584_getRequestSafeState2Delay(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request safe state 2 delay
 * Bitfield: SYSPCFG1.SS2DEL 
 * \param driver Device driver object  
 * \param value Request safe state 2 delay  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestSafeState2Delay(IfxTlf35584_Driver* driver, IfxTlf35584_RequestSafeState2Delay value, boolean* status);

/** \brief Get Request watchdog cycle time
 * Bitfield: WDCFG0.WDCYC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request watchdog cycle time  
 */
IFX_EXTERN IfxTlf35584_RequestWatchdogCycleTime IfxTlf35584_getRequestWatchdogCycleTime(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request watchdog cycle time
 * Bitfield: WDCFG0.WDCYC 
 * \param driver Device driver object  
 * \param value Request watchdog cycle time  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestWatchdogCycleTime(IfxTlf35584_Driver* driver, IfxTlf35584_RequestWatchdogCycleTime value, boolean* status);

/** \brief Get Request window watchdog trigger selection
 * Bitfield: WDCFG0.WWDTSEL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request window watchdog trigger selection  
 */
IFX_EXTERN IfxTlf35584_RequestWindowWatchdogTriggerSelection IfxTlf35584_getRequestWindowWatchdogTriggerSelection(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request window watchdog trigger selection
 * Bitfield: WDCFG0.WWDTSEL 
 * \param driver Device driver object  
 * \param value Request window watchdog trigger selection  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestWindowWatchdogTriggerSelection(IfxTlf35584_Driver* driver, IfxTlf35584_RequestWindowWatchdogTriggerSelection value, boolean* status);

/** \brief Get Request functional watchdog enable
 * Bitfield: WDCFG0.FWDEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request functional watchdog enable  
 */
IFX_EXTERN IfxTlf35584_RequestFunctionalWatchdogEnable IfxTlf35584_getRequestFunctionalWatchdogEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request functional watchdog enable
 * Bitfield: WDCFG0.FWDEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isRequestFunctionalWatchdogEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request functional watchdog enable
 * Bitfield: WDCFG0.FWDEN 
 * \param driver Device driver object  
 * \param value Request functional watchdog enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestFunctionalWatchdogEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestFunctionalWatchdogEnable value, boolean* status);

/** \brief Set Request functional watchdog enable [Enable]
 * Bitfield: WDCFG0.FWDEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableRequestFunctionalWatchdog(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request functional watchdog enable [Disable]
 * Bitfield: WDCFG0.FWDEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableRequestFunctionalWatchdog(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request window watchdog enable
 * Bitfield: WDCFG0.WWDEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request window watchdog enable  
 */
IFX_EXTERN IfxTlf35584_RequestWindowWatchdogEnable IfxTlf35584_getRequestWindowWatchdogEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request window watchdog enable
 * Bitfield: WDCFG0.WWDEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isRequestWindowWatchdogEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request window watchdog enable
 * Bitfield: WDCFG0.WWDEN 
 * \param driver Device driver object  
 * \param value Request window watchdog enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestWindowWatchdogEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestWindowWatchdogEnable value, boolean* status);

/** \brief Set Request window watchdog enable [Enable]
 * Bitfield: WDCFG0.WWDEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableRequestWindowWatchdog(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request window watchdog enable [Disable]
 * Bitfield: WDCFG0.WWDEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableRequestWindowWatchdog(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request window watchdog error threshold
 * Bitfield: WDCFG0.WWDETHR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request window watchdog error threshold  
 */
IFX_EXTERN uint8 IfxTlf35584_getRequestWindowWatchdogErrorThreshold(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request window watchdog error threshold
 * Bitfield: WDCFG0.WWDETHR 
 * \param driver Device driver object  
 * \param value Request window watchdog error threshold  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestWindowWatchdogErrorThreshold(IfxTlf35584_Driver* driver, uint8 value, boolean* status);

/** \brief Get Request functional watchdog error threshold
 * Bitfield: WDCFG1.FWDETHR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request functional watchdog error threshold  
 */
IFX_EXTERN uint8 IfxTlf35584_getRequestFunctionalWatchdogErrorThreshold(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request functional watchdog error threshold
 * Bitfield: WDCFG1.FWDETHR 
 * \param driver Device driver object  
 * \param value Request functional watchdog error threshold  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestFunctionalWatchdogErrorThreshold(IfxTlf35584_Driver* driver, uint8 value, boolean* status);

/** \brief Get Request watchdog functionality enable while the\ndevice is in SLEEP
 * Bitfield: WDCFG1.WDSLPEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request watchdog functionality enable while the\ndevice is in SLEEP  
 */
IFX_EXTERN IfxTlf35584_RequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep IfxTlf35584_getRequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request watchdog functionality enable while the\ndevice is in SLEEP
 * Bitfield: WDCFG1.WDSLPEN 
 * \param driver Device driver object  
 * \param value Request watchdog functionality enable while the\ndevice is in SLEEP  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep(IfxTlf35584_Driver* driver, IfxTlf35584_RequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep value, boolean* status);

/** \brief Get Request functional watchdog heartbeat timer period
 * Bitfield: FWDCFG.WDHBTP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request functional watchdog heartbeat timer period  
 */
IFX_EXTERN IfxTlf35584_RequestFunctionalWatchdogHeartbeatTimerPeriod IfxTlf35584_getRequestFunctionalWatchdogHeartbeatTimerPeriod(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request functional watchdog heartbeat timer period
 * Bitfield: FWDCFG.WDHBTP 
 * \param driver Device driver object  
 * \param value Request functional watchdog heartbeat timer period  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestFunctionalWatchdogHeartbeatTimerPeriod(IfxTlf35584_Driver* driver, IfxTlf35584_RequestFunctionalWatchdogHeartbeatTimerPeriod value, boolean* status);

/** \brief Set Request functional watchdog heartbeat timer period in WdCycle
 * Bitfield: FWDCFG.WDHBTP 
 * \param driver Device driver object  
 * \param value Request functional watchdog heartbeat timer period  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestFunctionalWatchdogHeartbeatTimerPeriodWdCycle(IfxTlf35584_Driver* driver, sint16 value, boolean* status);

/** \brief Get Request functional watchdog heartbeat timer period in WdCycle
 * Bitfield: FWDCFG.WDHBTP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request functional watchdog heartbeat timer period  
 */
IFX_EXTERN sint16 IfxTlf35584_getRequestFunctionalWatchdogHeartbeatTimerPeriodWdCycle(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request window watchdog closed window time
 * Bitfield: WWDCFG0.CW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request window watchdog closed window time  
 */
IFX_EXTERN IfxTlf35584_RequestWindowWatchdogClosedWindowTime IfxTlf35584_getRequestWindowWatchdogClosedWindowTime(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request window watchdog closed window time
 * Bitfield: WWDCFG0.CW 
 * \param driver Device driver object  
 * \param value Request window watchdog closed window time  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestWindowWatchdogClosedWindowTime(IfxTlf35584_Driver* driver, IfxTlf35584_RequestWindowWatchdogClosedWindowTime value, boolean* status);

/** \brief Set Request window watchdog closed window time in WdCycle
 * Bitfield: WWDCFG0.CW 
 * \param driver Device driver object  
 * \param value Request window watchdog closed window time  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestWindowWatchdogClosedWindowTimeWdCycle(IfxTlf35584_Driver* driver, sint16 value, boolean* status);

/** \brief Get Request window watchdog closed window time in WdCycle
 * Bitfield: WWDCFG0.CW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request window watchdog closed window time  
 */
IFX_EXTERN sint16 IfxTlf35584_getRequestWindowWatchdogClosedWindowTimeWdCycle(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request window watchdog open window time
 * Bitfield: WWDCFG1.OW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request window watchdog open window time  
 */
IFX_EXTERN IfxTlf35584_RequestWindowWatchdogOpenWindowTime IfxTlf35584_getRequestWindowWatchdogOpenWindowTime(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request window watchdog open window time
 * Bitfield: WWDCFG1.OW 
 * \param driver Device driver object  
 * \param value Request window watchdog open window time  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestWindowWatchdogOpenWindowTime(IfxTlf35584_Driver* driver, IfxTlf35584_RequestWindowWatchdogOpenWindowTime value, boolean* status);

/** \brief Set Request window watchdog open window time in WdCycle
 * Bitfield: WWDCFG1.OW 
 * \param driver Device driver object  
 * \param value Request window watchdog open window time  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestWindowWatchdogOpenWindowTimeWdCycle(IfxTlf35584_Driver* driver, sint16 value, boolean* status);

/** \brief Get Request window watchdog open window time in WdCycle
 * Bitfield: WWDCFG1.OW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request window watchdog open window time  
 */
IFX_EXTERN sint16 IfxTlf35584_getRequestWindowWatchdogOpenWindowTimeWdCycle(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby regulator QST enable status
 * Bitfield: RSYSPCFG0.STBYEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Standby regulator QST enable status  
 */
IFX_EXTERN IfxTlf35584_StandbyRegulatorQstEnableStatus IfxTlf35584_getStandbyRegulatorQstEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby regulator QST enable status
 * Bitfield: RSYSPCFG0.STBYEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isStandbyRegulatorQstEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get ERR pin monitor recovery time status
 * Bitfield: RSYSPCFG1.ERRREC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ERR pin monitor recovery time status  
 */
IFX_EXTERN IfxTlf35584_ErrPinMonitorRecoveryTimeStatus IfxTlf35584_getErrPinMonitorRecoveryTimeStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get ERR pin monitor recovery enable status
 * Bitfield: RSYSPCFG1.ERRRECEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ERR pin monitor recovery enable status  
 */
IFX_EXTERN IfxTlf35584_ErrPinMonitorRecoveryEnableStatus IfxTlf35584_getErrPinMonitorRecoveryEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get ERR pin monitor recovery enable status
 * Bitfield: RSYSPCFG1.ERRRECEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isErrPinMonitorRecoveryEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get ERR pin monitor enable status
 * Bitfield: RSYSPCFG1.ERREN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ERR pin monitor enable status  
 */
IFX_EXTERN IfxTlf35584_ErrPinMonitorEnableStatus IfxTlf35584_getErrPinMonitorEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get ERR pin monitor enable status
 * Bitfield: RSYSPCFG1.ERREN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isErrPinMonitorEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get ERR pin monitor functionality enable status while\nthe device is in SLEEP
 * Bitfield: RSYSPCFG1.ERRSLPEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ERR pin monitor functionality enable status while\nthe device is in SLEEP  
 */
IFX_EXTERN IfxTlf35584_ErrPinMonitorFunctionalityEnableStatusWhileTheDeviceIsInSleep IfxTlf35584_getErrPinMonitorFunctionalityEnableStatusWhileTheDeviceIsInSleep(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Safe state 2 delay status
 * Bitfield: RSYSPCFG1.SS2DEL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Safe state 2 delay status  
 */
IFX_EXTERN IfxTlf35584_SafeState2DelayStatus IfxTlf35584_getSafeState2DelayStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Watchdog cycle time status
 * Bitfield: RWDCFG0.WDCYC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Watchdog cycle time status  
 */
IFX_EXTERN IfxTlf35584_WatchdogCycleTimeStatus IfxTlf35584_getWatchdogCycleTimeStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog trigger selection status
 * Bitfield: RWDCFG0.WWDTSEL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Window watchdog trigger selection status  
 */
IFX_EXTERN IfxTlf35584_WindowWatchdogTriggerSelectionStatus IfxTlf35584_getWindowWatchdogTriggerSelectionStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog enable status
 * Bitfield: RWDCFG0.FWDEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Functional watchdog enable status  
 */
IFX_EXTERN IfxTlf35584_FunctionalWatchdogEnableStatus IfxTlf35584_getFunctionalWatchdogEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog enable status
 * Bitfield: RWDCFG0.FWDEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isFunctionalWatchdogEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog enable status
 * Bitfield: RWDCFG0.WWDEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Window watchdog enable status  
 */
IFX_EXTERN IfxTlf35584_WindowWatchdogEnableStatus IfxTlf35584_getWindowWatchdogEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog enable status
 * Bitfield: RWDCFG0.WWDEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isWindowWatchdogEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog error threshold status
 * Bitfield: RWDCFG0.WWDETHR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Window watchdog error threshold status  
 */
IFX_EXTERN uint8 IfxTlf35584_getWindowWatchdogErrorThresholdStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog error threshold status
 * Bitfield: RWDCFG1.FWDETHR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Functional watchdog error threshold status  
 */
IFX_EXTERN uint8 IfxTlf35584_getFunctionalWatchdogErrorThresholdStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Watchdog functionality enable status while the device\nis in SLEEP
 * Bitfield: RWDCFG1.WDSLPEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Watchdog functionality enable status while the device\nis in SLEEP  
 */
IFX_EXTERN IfxTlf35584_WatchdogFunctionalityEnableStatusWhileTheDeviceIsInSleep IfxTlf35584_getWatchdogFunctionalityEnableStatusWhileTheDeviceIsInSleep(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog heartbeat timer period status
 * Bitfield: RFWDCFG.WDHBTP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Functional watchdog heartbeat timer period status  
 */
IFX_EXTERN IfxTlf35584_FunctionalWatchdogHeartbeatTimerPeriodStatus IfxTlf35584_getFunctionalWatchdogHeartbeatTimerPeriodStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog heartbeat timer period status in WdCycle
 * Bitfield: RFWDCFG.WDHBTP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Functional watchdog heartbeat timer period status  
 */
IFX_EXTERN sint16 IfxTlf35584_getFunctionalWatchdogHeartbeatTimerPeriodStatusWdCycle(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog closed window time status
 * Bitfield: RWWDCFG0.CW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Window watchdog closed window time status  
 */
IFX_EXTERN IfxTlf35584_WindowWatchdogClosedWindowTimeStatus IfxTlf35584_getWindowWatchdogClosedWindowTimeStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog closed window time status in WdCycle
 * Bitfield: RWWDCFG0.CW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Window watchdog closed window time status  
 */
IFX_EXTERN sint16 IfxTlf35584_getWindowWatchdogClosedWindowTimeStatusWdCycle(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog open window time status
 * Bitfield: RWWDCFG1.OW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Window watchdog open window time status  
 */
IFX_EXTERN IfxTlf35584_WindowWatchdogOpenWindowTimeStatus IfxTlf35584_getWindowWatchdogOpenWindowTimeStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog open window time status in WdCycle
 * Bitfield: RWWDCFG1.OW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Window watchdog open window time status  
 */
IFX_EXTERN sint16 IfxTlf35584_getWindowWatchdogOpenWindowTimeStatusWdCycle(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Wake timer value lower bits
 * Bitfield: WKTIMCFG0.TIMVALL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Wake timer value lower bits  
 */
IFX_EXTERN uint8 IfxTlf35584_getWakeTimerValueLowerBits(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Wake timer value lower bits
 * Bitfield: WKTIMCFG0.TIMVALL 
 * \param driver Device driver object  
 * \param value Wake timer value lower bits  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setWakeTimerValueLowerBits(IfxTlf35584_Driver* driver, uint8 value, boolean* status);

/** \brief Set Wake timer value lower bits in WkTimCycle
 * Bitfield: WKTIMCFG0.TIMVALL 
 * \param driver Device driver object  
 * \param value Wake timer value lower bits  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setWakeTimerValueLowerBitsWkTimCycle(IfxTlf35584_Driver* driver, sint16 value, boolean* status);

/** \brief Get Wake timer value lower bits in WkTimCycle
 * Bitfield: WKTIMCFG0.TIMVALL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Wake timer value lower bits  
 */
IFX_EXTERN sint16 IfxTlf35584_getWakeTimerValueLowerBitsWkTimCycle(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Wake timer value middle bits
 * Bitfield: WKTIMCFG1.TIMVALM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Wake timer value middle bits  
 */
IFX_EXTERN uint8 IfxTlf35584_getWakeTimerValueMiddleBits(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Wake timer value middle bits
 * Bitfield: WKTIMCFG1.TIMVALM 
 * \param driver Device driver object  
 * \param value Wake timer value middle bits  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setWakeTimerValueMiddleBits(IfxTlf35584_Driver* driver, uint8 value, boolean* status);

/** \brief Set Wake timer value middle bits in WkTimCycle
 * Bitfield: WKTIMCFG1.TIMVALM 
 * \param driver Device driver object  
 * \param value Wake timer value middle bits  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setWakeTimerValueMiddleBitsWkTimCycle(IfxTlf35584_Driver* driver, sint32 value, boolean* status);

/** \brief Get Wake timer value middle bits in WkTimCycle
 * Bitfield: WKTIMCFG1.TIMVALM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Wake timer value middle bits  
 */
IFX_EXTERN sint32 IfxTlf35584_getWakeTimerValueMiddleBitsWkTimCycle(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Wake timer value higher bits
 * Bitfield: WKTIMCFG2.TIMVALH 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Wake timer value higher bits  
 */
IFX_EXTERN uint8 IfxTlf35584_getWakeTimerValueHigherBits(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Wake timer value higher bits
 * Bitfield: WKTIMCFG2.TIMVALH 
 * \param driver Device driver object  
 * \param value Wake timer value higher bits  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setWakeTimerValueHigherBits(IfxTlf35584_Driver* driver, uint8 value, boolean* status);

/** \brief Set Wake timer value higher bits in WkTimCycle
 * Bitfield: WKTIMCFG2.TIMVALH 
 * \param driver Device driver object  
 * \param value Wake timer value higher bits  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setWakeTimerValueHigherBitsWkTimCycle(IfxTlf35584_Driver* driver, sint32 value, boolean* status);

/** \brief Get Wake timer value higher bits in WkTimCycle
 * Bitfield: WKTIMCFG2.TIMVALH 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Wake timer value higher bits  
 */
IFX_EXTERN sint32 IfxTlf35584_getWakeTimerValueHigherBitsWkTimCycle(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request for device state transition
 * Bitfield: DEVCTRL.STATEREQ 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request for device state transition  
 */
IFX_EXTERN IfxTlf35584_RequestForDeviceStateTransition IfxTlf35584_getRequestForDeviceStateTransition(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request for device state transition
 * Bitfield: DEVCTRL.STATEREQ 
 * \param driver Device driver object  
 * \param value Request for device state transition  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestForDeviceStateTransition(IfxTlf35584_Driver* driver, IfxTlf35584_RequestForDeviceStateTransition value, boolean* status);

/** \brief Get Request voltage reference QVR enable
 * Bitfield: DEVCTRL.VREFEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request voltage reference QVR enable  
 */
IFX_EXTERN IfxTlf35584_RequestVoltageReferenceQvrEnable IfxTlf35584_getRequestVoltageReferenceQvrEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request voltage reference QVR enable
 * Bitfield: DEVCTRL.VREFEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isRequestVoltageReferenceQvrEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request voltage reference QVR enable
 * Bitfield: DEVCTRL.VREFEN 
 * \param driver Device driver object  
 * \param value Request voltage reference QVR enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestVoltageReferenceQvrEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestVoltageReferenceQvrEnable value, boolean* status);

/** \brief Set Request voltage reference QVR enable [Enable]
 * Bitfield: DEVCTRL.VREFEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableRequestVoltageReferenceQvr(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request voltage reference QVR enable [Disable]
 * Bitfield: DEVCTRL.VREFEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableRequestVoltageReferenceQvr(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request communication ldo QCO enable
 * Bitfield: DEVCTRL.COMEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request communication ldo QCO enable  
 */
IFX_EXTERN IfxTlf35584_RequestCommunicationLdoQcoEnable IfxTlf35584_getRequestCommunicationLdoQcoEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request communication ldo QCO enable
 * Bitfield: DEVCTRL.COMEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isRequestCommunicationLdoQcoEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request communication ldo QCO enable
 * Bitfield: DEVCTRL.COMEN 
 * \param driver Device driver object  
 * \param value Request communication ldo QCO enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestCommunicationLdoQcoEnable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestCommunicationLdoQcoEnable value, boolean* status);

/** \brief Set Request communication ldo QCO enable [Enable]
 * Bitfield: DEVCTRL.COMEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableRequestCommunicationLdoQco(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request communication ldo QCO enable [Disable]
 * Bitfield: DEVCTRL.COMEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableRequestCommunicationLdoQco(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request tracker1 QT1 enable
 * Bitfield: DEVCTRL.TRK1EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request tracker1 QT1 enable  
 */
IFX_EXTERN IfxTlf35584_RequestTracker1Qt1Enable IfxTlf35584_getRequestTracker1Qt1Enable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request tracker1 QT1 enable
 * Bitfield: DEVCTRL.TRK1EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isRequestTracker1Qt1Enabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request tracker1 QT1 enable
 * Bitfield: DEVCTRL.TRK1EN 
 * \param driver Device driver object  
 * \param value Request tracker1 QT1 enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestTracker1Qt1Enable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestTracker1Qt1Enable value, boolean* status);

/** \brief Set Request tracker1 QT1 enable [Enable]
 * Bitfield: DEVCTRL.TRK1EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableRequestTracker1Qt1(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request tracker1 QT1 enable [Disable]
 * Bitfield: DEVCTRL.TRK1EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableRequestTracker1Qt1(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request tracker2 QT2 enable
 * Bitfield: DEVCTRL.TRK2EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Request tracker2 QT2 enable  
 */
IFX_EXTERN IfxTlf35584_RequestTracker2Qt2Enable IfxTlf35584_getRequestTracker2Qt2Enable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Request tracker2 QT2 enable
 * Bitfield: DEVCTRL.TRK2EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isRequestTracker2Qt2Enabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request tracker2 QT2 enable
 * Bitfield: DEVCTRL.TRK2EN 
 * \param driver Device driver object  
 * \param value Request tracker2 QT2 enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setRequestTracker2Qt2Enable(IfxTlf35584_Driver* driver, IfxTlf35584_RequestTracker2Qt2Enable value, boolean* status);

/** \brief Set Request tracker2 QT2 enable [Enable]
 * Bitfield: DEVCTRL.TRK2EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableRequestTracker2Qt2(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Request tracker2 QT2 enable [Disable]
 * Bitfield: DEVCTRL.TRK2EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableRequestTracker2Qt2(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog SPI trigger command
 * Bitfield: WWDSCMD.TRIG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Window watchdog SPI trigger command  
 */
IFX_EXTERN boolean IfxTlf35584_getWindowWatchdogSpiTriggerCommand(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Window watchdog SPI trigger command
 * Bitfield: WWDSCMD.TRIG 
 * \param driver Device driver object  
 * \param value Window watchdog SPI trigger command  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setWindowWatchdogSpiTriggerCommand(IfxTlf35584_Driver* driver, boolean value, boolean* status);

/** \brief Get Last SPI trigger received
 * Bitfield: WWDSCMD.TRIG_STATUS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Last SPI trigger received  
 */
IFX_EXTERN boolean IfxTlf35584_getLastSpiTriggerReceived(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog response
 * Bitfield: FWDRSP.FWDRSP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Functional watchdog response  
 */
IFX_EXTERN uint8 IfxTlf35584_getFunctionalWatchdogResponse(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Functional watchdog response
 * Bitfield: FWDRSP.FWDRSP 
 * \param driver Device driver object  
 * \param value Functional watchdog response  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setFunctionalWatchdogResponse(IfxTlf35584_Driver* driver, uint8 value, boolean* status);

/** \brief Get Functional watchdog heartbeat synchronization response
 * Bitfield: FWDRSPSYNC.FWDRSPS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Functional watchdog heartbeat synchronization response  
 */
IFX_EXTERN uint8 IfxTlf35584_getFunctionalWatchdogHeartbeatSynchronizationResponse(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Functional watchdog heartbeat synchronization response
 * Bitfield: FWDRSPSYNC.FWDRSPS 
 * \param driver Device driver object  
 * \param value Functional watchdog heartbeat synchronization response  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setFunctionalWatchdogHeartbeatSynchronizationResponse(IfxTlf35584_Driver* driver, uint8 value, boolean* status);

/** \brief Get Double Bit error on voltage selection flag
 * Bitfield: SYSFAIL.VOLTSELERR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Double Bit error on voltage selection flag  
 */
IFX_EXTERN IfxTlf35584_DoubleBitErrorOnVoltageSelectionFlag IfxTlf35584_getDoubleBitErrorOnVoltageSelectionFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Double Bit error on voltage selection flag
 * Bitfield: SYSFAIL.VOLTSELERR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isDoubleBitErrorOnVoltageSelectionFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Double Bit error on voltage selection flag [Clear]
 * Bitfield: SYSFAIL.VOLTSELERR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearDoubleBitErrorOnVoltageSelectionFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Over temperature failure flag
 * Bitfield: SYSFAIL.OTF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Over temperature failure flag  
 */
IFX_EXTERN IfxTlf35584_OverTemperatureFailureFlag IfxTlf35584_getOverTemperatureFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Over temperature failure flag
 * Bitfield: SYSFAIL.OTF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isOverTemperatureFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Over temperature failure flag [Clear]
 * Bitfield: SYSFAIL.OTF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearOverTemperatureFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage monitor failure flag
 * Bitfield: SYSFAIL.VMONF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Voltage monitor failure flag  
 */
IFX_EXTERN IfxTlf35584_VoltageMonitorFailureFlag IfxTlf35584_getVoltageMonitorFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage monitor failure flag
 * Bitfield: SYSFAIL.VMONF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isVoltageMonitorFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Voltage monitor failure flag [Clear]
 * Bitfield: SYSFAIL.VMONF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearVoltageMonitorFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get ABIST operation interrupted flag
 * Bitfield: SYSFAIL.ABISTERR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ABIST operation interrupted flag  
 */
IFX_EXTERN IfxTlf35584_AbistOperationInterruptedFlag IfxTlf35584_getAbistOperationInterruptedFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get ABIST operation interrupted flag
 * Bitfield: SYSFAIL.ABISTERR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isAbistOperationInterruptedFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set ABIST operation interrupted flag [Clear]
 * Bitfield: SYSFAIL.ABISTERR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearAbistOperationInterruptedFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get INIT failure flag
 * Bitfield: SYSFAIL.INITF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return INIT failure flag  
 */
IFX_EXTERN IfxTlf35584_InitFailureFlag IfxTlf35584_getInitFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get INIT failure flag
 * Bitfield: SYSFAIL.INITF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isInitFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set INIT failure flag [Clear]
 * Bitfield: SYSFAIL.INITF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearInitFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage monitor init failure flag
 * Bitfield: INITERR.VMONF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Voltage monitor init failure flag  
 */
IFX_EXTERN IfxTlf35584_VoltageMonitorInitFailureFlag IfxTlf35584_getVoltageMonitorInitFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage monitor init failure flag
 * Bitfield: INITERR.VMONF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isVoltageMonitorInitFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Voltage monitor init failure flag [Clear]
 * Bitfield: INITERR.VMONF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearVoltageMonitorInitFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog error counter overflow failure flag
 * Bitfield: INITERR.WWDF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Window watchdog error counter overflow failure flag  
 */
IFX_EXTERN IfxTlf35584_WindowWatchdogErrorCounterOverflowFailureFlag IfxTlf35584_getWindowWatchdogErrorCounterOverflowFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog error counter overflow failure flag
 * Bitfield: INITERR.WWDF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isWindowWatchdogErrorCounterOverflowFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Window watchdog error counter overflow failure flag [Clear]
 * Bitfield: INITERR.WWDF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearWindowWatchdogErrorCounterOverflowFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog error counter overflow failure\nflag
 * Bitfield: INITERR.FWDF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Functional watchdog error counter overflow failure\nflag  
 */
IFX_EXTERN IfxTlf35584_FunctionalWatchdogErrorCounterOverflowFailureFlag IfxTlf35584_getFunctionalWatchdogErrorCounterOverflowFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog error counter overflow failure\nflag
 * Bitfield: INITERR.FWDF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isFunctionalWatchdogErrorCounterOverflowFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Functional watchdog error counter overflow failure\nflag [Clear]
 * Bitfield: INITERR.FWDF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearFunctionalWatchdogErrorCounterOverflowFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get MCU error monitor failure flag
 * Bitfield: INITERR.ERRF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return MCU error monitor failure flag  
 */
IFX_EXTERN IfxTlf35584_McuErrorMonitorFailureFlag IfxTlf35584_getMcuErrorMonitorFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get MCU error monitor failure flag
 * Bitfield: INITERR.ERRF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isMcuErrorMonitorFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set MCU error monitor failure flag [Clear]
 * Bitfield: INITERR.ERRF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearMcuErrorMonitorFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Soft reset flag
 * Bitfield: INITERR.SOFTRES 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Soft reset flag  
 */
IFX_EXTERN IfxTlf35584_SoftResetFlag IfxTlf35584_getSoftResetFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Soft reset flag
 * Bitfield: INITERR.SOFTRES 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSoftResetFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Soft reset flag [Clear]
 * Bitfield: INITERR.SOFTRES 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearSoftResetFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Hard reset flag
 * Bitfield: INITERR.HARDRES 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Hard reset flag  
 */
IFX_EXTERN IfxTlf35584_HardResetFlag IfxTlf35584_getHardResetFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Hard reset flag
 * Bitfield: INITERR.HARDRES 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isHardResetFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Hard reset flag [Clear]
 * Bitfield: INITERR.HARDRES 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearHardResetFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get System interrupt flag
 * Bitfield: IF.SYS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return System interrupt flag  
 */
IFX_EXTERN IfxTlf35584_SystemInterruptFlag IfxTlf35584_getSystemInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get System interrupt flag
 * Bitfield: IF.SYS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSystemInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set System interrupt flag [Clear]
 * Bitfield: IF.SYS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearSystemInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Wake interrupt flag
 * Bitfield: IF.WK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Wake interrupt flag  
 */
IFX_EXTERN IfxTlf35584_WakeInterruptFlag IfxTlf35584_getWakeInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Wake interrupt flag
 * Bitfield: IF.WK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isWakeInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Wake interrupt flag [Clear]
 * Bitfield: IF.WK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearWakeInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get SPI interrupt flag
 * Bitfield: IF.SPI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return SPI interrupt flag  
 */
IFX_EXTERN IfxTlf35584_SpiInterruptFlag IfxTlf35584_getSpiInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get SPI interrupt flag
 * Bitfield: IF.SPI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSpiInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set SPI interrupt flag [Clear]
 * Bitfield: IF.SPI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearSpiInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Monitor interrupt flag
 * Bitfield: IF.MON 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Monitor interrupt flag  
 */
IFX_EXTERN IfxTlf35584_MonitorInterruptFlag IfxTlf35584_getMonitorInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Monitor interrupt flag
 * Bitfield: IF.MON 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isMonitorInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Monitor interrupt flag [Clear]
 * Bitfield: IF.MON 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearMonitorInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Over temperature warning interrupt flag
 * Bitfield: IF.OTW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Over temperature warning interrupt flag  
 */
IFX_EXTERN IfxTlf35584_OverTemperatureWarningInterruptFlag IfxTlf35584_getOverTemperatureWarningInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Over temperature warning interrupt flag
 * Bitfield: IF.OTW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isOverTemperatureWarningInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Over temperature warning interrupt flag [Clear]
 * Bitfield: IF.OTW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearOverTemperatureWarningInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Over temperature failure interrupt flag
 * Bitfield: IF.OTF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Over temperature failure interrupt flag  
 */
IFX_EXTERN IfxTlf35584_OverTemperatureFailureInterruptFlag IfxTlf35584_getOverTemperatureFailureInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Over temperature failure interrupt flag
 * Bitfield: IF.OTF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isOverTemperatureFailureInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Over temperature failure interrupt flag [Clear]
 * Bitfield: IF.OTF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearOverTemperatureFailureInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Requested ABIST operation performed flag
 * Bitfield: IF.ABIST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Requested ABIST operation performed flag  
 */
IFX_EXTERN IfxTlf35584_RequestedAbistOperationPerformedFlag IfxTlf35584_getRequestedAbistOperationPerformedFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Requested ABIST operation performed flag
 * Bitfield: IF.ABIST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isRequestedAbistOperationPerformedFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Requested ABIST operation performed flag [Clear]
 * Bitfield: IF.ABIST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearRequestedAbistOperationPerformedFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Interrupt not serviced in time flag
 * Bitfield: IF.INTMISS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Interrupt not serviced in time flag  
 */
IFX_EXTERN IfxTlf35584_InterruptNotServicedInTimeFlag IfxTlf35584_getInterruptNotServicedInTimeFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Interrupt not serviced in time flag
 * Bitfield: IF.INTMISS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isInterruptNotServicedInTimeFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Protected configuration double bit error flag
 * Bitfield: SYSSF.CFGE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Protected configuration double bit error flag  
 */
IFX_EXTERN IfxTlf35584_ProtectedConfigurationDoubleBitErrorFlag IfxTlf35584_getProtectedConfigurationDoubleBitErrorFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Protected configuration double bit error flag
 * Bitfield: SYSSF.CFGE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isProtectedConfigurationDoubleBitErrorFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Protected configuration double bit error flag [Clear]
 * Bitfield: SYSSF.CFGE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearProtectedConfigurationDoubleBitErrorFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog error interrupt flag
 * Bitfield: SYSSF.WWDE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Window watchdog error interrupt flag  
 */
IFX_EXTERN IfxTlf35584_WindowWatchdogErrorInterruptFlag IfxTlf35584_getWindowWatchdogErrorInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog error interrupt flag
 * Bitfield: SYSSF.WWDE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isWindowWatchdogErrorInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Window watchdog error interrupt flag [Clear]
 * Bitfield: SYSSF.WWDE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearWindowWatchdogErrorInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog error interrupt flag
 * Bitfield: SYSSF.FWDE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Functional watchdog error interrupt flag  
 */
IFX_EXTERN IfxTlf35584_FunctionalWatchdogErrorInterruptFlag IfxTlf35584_getFunctionalWatchdogErrorInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog error interrupt flag
 * Bitfield: SYSSF.FWDE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isFunctionalWatchdogErrorInterruptFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Functional watchdog error interrupt flag [Clear]
 * Bitfield: SYSSF.FWDE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearFunctionalWatchdogErrorInterruptFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get MCU error miss status flag
 * Bitfield: SYSSF.ERRMISS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return MCU error miss status flag  
 */
IFX_EXTERN IfxTlf35584_McuErrorMissStatusFlag IfxTlf35584_getMcuErrorMissStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get MCU error miss status flag
 * Bitfield: SYSSF.ERRMISS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isMcuErrorMissStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set MCU error miss status flag [Clear]
 * Bitfield: SYSSF.ERRMISS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearMcuErrorMissStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Transition to low power failed flag
 * Bitfield: SYSSF.TRFAIL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Transition to low power failed flag  
 */
IFX_EXTERN IfxTlf35584_TransitionToLowPowerFailedFlag IfxTlf35584_getTransitionToLowPowerFailedFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Transition to low power failed flag
 * Bitfield: SYSSF.TRFAIL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isTransitionToLowPowerFailedFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Transition to low power failed flag [Clear]
 * Bitfield: SYSSF.TRFAIL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearTransitionToLowPowerFailedFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get State transition request failure flag
 * Bitfield: SYSSF.NO_OP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return State transition request failure flag  
 */
IFX_EXTERN IfxTlf35584_StateTransitionRequestFailureFlag IfxTlf35584_getStateTransitionRequestFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get State transition request failure flag
 * Bitfield: SYSSF.NO_OP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isStateTransitionRequestFailureFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set State transition request failure flag [Clear]
 * Bitfield: SYSSF.NO_OP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearStateTransitionRequestFailureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get WAK signal wakeup flag
 * Bitfield: WKSF.WAK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return WAK signal wakeup flag  
 */
IFX_EXTERN IfxTlf35584_WakSignalWakeupFlag IfxTlf35584_getWakSignalWakeupFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get WAK signal wakeup flag
 * Bitfield: WKSF.WAK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isWakSignalWakeupFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set WAK signal wakeup flag [Clear]
 * Bitfield: WKSF.WAK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearWakSignalWakeupFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get ENA signal wakeup flag
 * Bitfield: WKSF.ENA 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ENA signal wakeup flag  
 */
IFX_EXTERN IfxTlf35584_EnaSignalWakeupFlag IfxTlf35584_getEnaSignalWakeupFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get ENA signal wakeup flag
 * Bitfield: WKSF.ENA 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isEnaSignalWakeupFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set ENA signal wakeup flag [Clear]
 * Bitfield: WKSF.ENA 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearEnaSignalWakeupFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get QUC current monitor threshold wakeup flag
 * Bitfield: WKSF.CMON 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return QUC current monitor threshold wakeup flag  
 */
IFX_EXTERN IfxTlf35584_QucCurrentMonitorThresholdWakeupFlag IfxTlf35584_getQucCurrentMonitorThresholdWakeupFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get QUC current monitor threshold wakeup flag
 * Bitfield: WKSF.CMON 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isQucCurrentMonitorThresholdWakeupFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set QUC current monitor threshold wakeup flag [Clear]
 * Bitfield: WKSF.CMON 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearQucCurrentMonitorThresholdWakeupFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Wake timer wakeup flag
 * Bitfield: WKSF.WKTIM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Wake timer wakeup flag  
 */
IFX_EXTERN IfxTlf35584_WakeTimerWakeupFlag IfxTlf35584_getWakeTimerWakeupFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Wake timer wakeup flag
 * Bitfield: WKSF.WKTIM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isWakeTimerWakeupFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Wake timer wakeup flag [Clear]
 * Bitfield: WKSF.WKTIM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearWakeTimerWakeupFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Wakeup from SLEEP by SPI flag
 * Bitfield: WKSF.WKSPI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Wakeup from SLEEP by SPI flag  
 */
IFX_EXTERN IfxTlf35584_WakeupFromSleepBySpiFlag IfxTlf35584_getWakeupFromSleepBySpiFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Wakeup from SLEEP by SPI flag
 * Bitfield: WKSF.WKSPI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isWakeupFromSleepBySpiFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Wakeup from SLEEP by SPI flag [Clear]
 * Bitfield: WKSF.WKSPI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearWakeupFromSleepBySpiFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get SPI frame parity error flag
 * Bitfield: SPISF.PARE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return SPI frame parity error flag  
 */
IFX_EXTERN IfxTlf35584_SpiFrameParityErrorFlag IfxTlf35584_getSpiFrameParityErrorFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get SPI frame parity error flag
 * Bitfield: SPISF.PARE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSpiFrameParityErrorFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set SPI frame parity error flag [Clear]
 * Bitfield: SPISF.PARE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearSpiFrameParityErrorFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get SPI frame length invalid flag
 * Bitfield: SPISF.LENE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return SPI frame length invalid flag  
 */
IFX_EXTERN IfxTlf35584_SpiFrameLengthInvalidFlag IfxTlf35584_getSpiFrameLengthInvalidFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get SPI frame length invalid flag
 * Bitfield: SPISF.LENE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSpiFrameLengthInvalidFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set SPI frame length invalid flag [Clear]
 * Bitfield: SPISF.LENE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearSpiFrameLengthInvalidFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get SPI address invalid flag
 * Bitfield: SPISF.ADDRE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return SPI address invalid flag  
 */
IFX_EXTERN IfxTlf35584_SpiAddressInvalidFlag IfxTlf35584_getSpiAddressInvalidFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get SPI address invalid flag
 * Bitfield: SPISF.ADDRE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSpiAddressInvalidFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set SPI address invalid flag [Clear]
 * Bitfield: SPISF.ADDRE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearSpiAddressInvalidFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get SPI frame duration error flag
 * Bitfield: SPISF.DURE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return SPI frame duration error flag  
 */
IFX_EXTERN IfxTlf35584_SpiFrameDurationErrorFlag IfxTlf35584_getSpiFrameDurationErrorFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get SPI frame duration error flag
 * Bitfield: SPISF.DURE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSpiFrameDurationErrorFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set SPI frame duration error flag [Clear]
 * Bitfield: SPISF.DURE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearSpiFrameDurationErrorFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get LOCK or UNLOCK procedure error flag
 * Bitfield: SPISF.LOCK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return LOCK or UNLOCK procedure error flag  
 */
IFX_EXTERN IfxTlf35584_LockOrUnlockProcedureErrorFlag IfxTlf35584_getLockOrUnlockProcedureErrorFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get LOCK or UNLOCK procedure error flag
 * Bitfield: SPISF.LOCK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isLockOrUnlockProcedureErrorFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set LOCK or UNLOCK procedure error flag [Clear]
 * Bitfield: SPISF.LOCK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearLockOrUnlockProcedureErrorFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Pre-regulator voltage short to ground status flag
 * Bitfield: MONSF0.PREGSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Pre-regulator voltage short to ground status flag  
 */
IFX_EXTERN IfxTlf35584_PreRegulatorVoltageShortToGroundStatusFlag IfxTlf35584_getPreRegulatorVoltageShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Pre-regulator voltage short to ground status flag
 * Bitfield: MONSF0.PREGSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isPreRegulatorVoltageShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Pre-regulator voltage short to ground status flag [Clear]
 * Bitfield: MONSF0.PREGSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearPreRegulatorVoltageShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get uC LDO short to ground status flag
 * Bitfield: MONSF0.UCSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return uC LDO short to ground status flag  
 */
IFX_EXTERN IfxTlf35584_UcLdoShortToGroundStatusFlag IfxTlf35584_getUcLdoShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get uC LDO short to ground status flag
 * Bitfield: MONSF0.UCSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isUcLdoShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set uC LDO short to ground status flag [Clear]
 * Bitfield: MONSF0.UCSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearUcLdoShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby LDO short to ground status flag
 * Bitfield: MONSF0.STBYSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Standby LDO short to ground status flag  
 */
IFX_EXTERN IfxTlf35584_StandbyLdoShortToGroundStatusFlag IfxTlf35584_getStandbyLdoShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby LDO short to ground status flag
 * Bitfield: MONSF0.STBYSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isStandbyLdoShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Standby LDO short to ground status flag [Clear]
 * Bitfield: MONSF0.STBYSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearStandbyLdoShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Core voltage short to ground status flag
 * Bitfield: MONSF0.VCORESG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Core voltage short to ground status flag  
 */
IFX_EXTERN IfxTlf35584_CoreVoltageShortToGroundStatusFlag IfxTlf35584_getCoreVoltageShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Core voltage short to ground status flag
 * Bitfield: MONSF0.VCORESG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isCoreVoltageShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Core voltage short to ground status flag [Clear]
 * Bitfield: MONSF0.VCORESG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearCoreVoltageShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO short to ground status flag
 * Bitfield: MONSF0.COMSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Communication LDO short to ground status flag  
 */
IFX_EXTERN IfxTlf35584_CommunicationLdoShortToGroundStatusFlag IfxTlf35584_getCommunicationLdoShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO short to ground status flag
 * Bitfield: MONSF0.COMSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isCommunicationLdoShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Communication LDO short to ground status flag [Clear]
 * Bitfield: MONSF0.COMSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearCommunicationLdoShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage reference short to ground status flag
 * Bitfield: MONSF0.VREFSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Voltage reference short to ground status flag  
 */
IFX_EXTERN IfxTlf35584_VoltageReferenceShortToGroundStatusFlag IfxTlf35584_getVoltageReferenceShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage reference short to ground status flag
 * Bitfield: MONSF0.VREFSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isVoltageReferenceShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Voltage reference short to ground status flag [Clear]
 * Bitfield: MONSF0.VREFSG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearVoltageReferenceShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker1 short to ground status flag
 * Bitfield: MONSF0.TRK1SG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Tracker1 short to ground status flag  
 */
IFX_EXTERN IfxTlf35584_Tracker1ShortToGroundStatusFlag IfxTlf35584_getTracker1ShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker1 short to ground status flag
 * Bitfield: MONSF0.TRK1SG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isTracker1ShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Tracker1 short to ground status flag [Clear]
 * Bitfield: MONSF0.TRK1SG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearTracker1ShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker2 short to ground status flag
 * Bitfield: MONSF0.TRK2SG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Tracker2 short to ground status flag  
 */
IFX_EXTERN IfxTlf35584_Tracker2ShortToGroundStatusFlag IfxTlf35584_getTracker2ShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker2 short to ground status flag
 * Bitfield: MONSF0.TRK2SG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isTracker2ShortToGroundStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Tracker2 short to ground status flag [Clear]
 * Bitfield: MONSF0.TRK2SG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearTracker2ShortToGroundStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Pre-regulator voltage over voltage status flag
 * Bitfield: MONSF1.PREGOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Pre-regulator voltage over voltage status flag  
 */
IFX_EXTERN IfxTlf35584_PreRegulatorVoltageOverVoltageStatusFlag IfxTlf35584_getPreRegulatorVoltageOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Pre-regulator voltage over voltage status flag
 * Bitfield: MONSF1.PREGOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isPreRegulatorVoltageOverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Pre-regulator voltage over voltage status flag [Clear]
 * Bitfield: MONSF1.PREGOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearPreRegulatorVoltageOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get uC LDO over voltage status flag
 * Bitfield: MONSF1.UCOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return uC LDO over voltage status flag  
 */
IFX_EXTERN IfxTlf35584_UcLdoOverVoltageStatusFlag IfxTlf35584_getUcLdoOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get uC LDO over voltage status flag
 * Bitfield: MONSF1.UCOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isUcLdoOverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set uC LDO over voltage status flag [Clear]
 * Bitfield: MONSF1.UCOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearUcLdoOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby LDO over voltage status flag
 * Bitfield: MONSF1.STBYOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Standby LDO over voltage status flag  
 */
IFX_EXTERN IfxTlf35584_StandbyLdoOverVoltageStatusFlag IfxTlf35584_getStandbyLdoOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby LDO over voltage status flag
 * Bitfield: MONSF1.STBYOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isStandbyLdoOverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Standby LDO over voltage status flag [Clear]
 * Bitfield: MONSF1.STBYOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearStandbyLdoOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Core voltage over voltage status flag
 * Bitfield: MONSF1.VCOREOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Core voltage over voltage status flag  
 */
IFX_EXTERN IfxTlf35584_CoreVoltageOverVoltageStatusFlag IfxTlf35584_getCoreVoltageOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Core voltage over voltage status flag
 * Bitfield: MONSF1.VCOREOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isCoreVoltageOverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Core voltage over voltage status flag [Clear]
 * Bitfield: MONSF1.VCOREOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearCoreVoltageOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO over voltage status flag
 * Bitfield: MONSF1.COMOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Communication LDO over voltage status flag  
 */
IFX_EXTERN IfxTlf35584_CommunicationLdoOverVoltageStatusFlag IfxTlf35584_getCommunicationLdoOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO over voltage status flag
 * Bitfield: MONSF1.COMOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isCommunicationLdoOverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Communication LDO over voltage status flag [Clear]
 * Bitfield: MONSF1.COMOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearCommunicationLdoOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage reference over voltage status flag
 * Bitfield: MONSF1.VREFOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Voltage reference over voltage status flag  
 */
IFX_EXTERN IfxTlf35584_VoltageReferenceOverVoltageStatusFlag IfxTlf35584_getVoltageReferenceOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage reference over voltage status flag
 * Bitfield: MONSF1.VREFOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isVoltageReferenceOverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Voltage reference over voltage status flag [Clear]
 * Bitfield: MONSF1.VREFOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearVoltageReferenceOverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker1 over voltage status flag
 * Bitfield: MONSF1.TRK1OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Tracker1 over voltage status flag  
 */
IFX_EXTERN IfxTlf35584_Tracker1OverVoltageStatusFlag IfxTlf35584_getTracker1OverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker1 over voltage status flag
 * Bitfield: MONSF1.TRK1OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isTracker1OverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Tracker1 over voltage status flag [Clear]
 * Bitfield: MONSF1.TRK1OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearTracker1OverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker2 over voltage status flag
 * Bitfield: MONSF1.TRK2OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Tracker2 over voltage status flag  
 */
IFX_EXTERN IfxTlf35584_Tracker2OverVoltageStatusFlag IfxTlf35584_getTracker2OverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker2 over voltage status flag
 * Bitfield: MONSF1.TRK2OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isTracker2OverVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Tracker2 over voltage status flag [Clear]
 * Bitfield: MONSF1.TRK2OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearTracker2OverVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Pre-regulator voltage under voltage status flag
 * Bitfield: MONSF2.PREGUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Pre-regulator voltage under voltage status flag  
 */
IFX_EXTERN IfxTlf35584_PreRegulatorVoltageUnderVoltageStatusFlag IfxTlf35584_getPreRegulatorVoltageUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Pre-regulator voltage under voltage status flag
 * Bitfield: MONSF2.PREGUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isPreRegulatorVoltageUnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Pre-regulator voltage under voltage status flag [Clear]
 * Bitfield: MONSF2.PREGUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearPreRegulatorVoltageUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get uC LDO under voltage status flag
 * Bitfield: MONSF2.UCUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return uC LDO under voltage status flag  
 */
IFX_EXTERN IfxTlf35584_UcLdoUnderVoltageStatusFlag IfxTlf35584_getUcLdoUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get uC LDO under voltage status flag
 * Bitfield: MONSF2.UCUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isUcLdoUnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set uC LDO under voltage status flag [Clear]
 * Bitfield: MONSF2.UCUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearUcLdoUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby LDO under voltage status flag
 * Bitfield: MONSF2.STBYUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Standby LDO under voltage status flag  
 */
IFX_EXTERN IfxTlf35584_StandbyLdoUnderVoltageStatusFlag IfxTlf35584_getStandbyLdoUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby LDO under voltage status flag
 * Bitfield: MONSF2.STBYUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isStandbyLdoUnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Standby LDO under voltage status flag [Clear]
 * Bitfield: MONSF2.STBYUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearStandbyLdoUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Core voltage under voltage status flag
 * Bitfield: MONSF2.VCOREUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Core voltage under voltage status flag  
 */
IFX_EXTERN IfxTlf35584_CoreVoltageUnderVoltageStatusFlag IfxTlf35584_getCoreVoltageUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Core voltage under voltage status flag
 * Bitfield: MONSF2.VCOREUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isCoreVoltageUnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Core voltage under voltage status flag [Clear]
 * Bitfield: MONSF2.VCOREUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearCoreVoltageUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO under voltage status flag
 * Bitfield: MONSF2.COMUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Communication LDO under voltage status flag  
 */
IFX_EXTERN IfxTlf35584_CommunicationLdoUnderVoltageStatusFlag IfxTlf35584_getCommunicationLdoUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO under voltage status flag
 * Bitfield: MONSF2.COMUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isCommunicationLdoUnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Communication LDO under voltage status flag [Clear]
 * Bitfield: MONSF2.COMUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearCommunicationLdoUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage reference under voltage status flag
 * Bitfield: MONSF2.VREFUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Voltage reference under voltage status flag  
 */
IFX_EXTERN IfxTlf35584_VoltageReferenceUnderVoltageStatusFlag IfxTlf35584_getVoltageReferenceUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage reference under voltage status flag
 * Bitfield: MONSF2.VREFUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isVoltageReferenceUnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Voltage reference under voltage status flag [Clear]
 * Bitfield: MONSF2.VREFUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearVoltageReferenceUnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker1 under voltage status flag
 * Bitfield: MONSF2.TRK1UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Tracker1 under voltage status flag  
 */
IFX_EXTERN IfxTlf35584_Tracker1UnderVoltageStatusFlag IfxTlf35584_getTracker1UnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker1 under voltage status flag
 * Bitfield: MONSF2.TRK1UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isTracker1UnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Tracker1 under voltage status flag [Clear]
 * Bitfield: MONSF2.TRK1UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearTracker1UnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker2 under voltage status flag
 * Bitfield: MONSF2.TRK2UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Tracker2 under voltage status flag  
 */
IFX_EXTERN IfxTlf35584_Tracker2UnderVoltageStatusFlag IfxTlf35584_getTracker2UnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker2 under voltage status flag
 * Bitfield: MONSF2.TRK2UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isTracker2UnderVoltageStatusFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Tracker2 under voltage status flag [Clear]
 * Bitfield: MONSF2.TRK2UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearTracker2UnderVoltageStatusFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Supply voltage VS12 over voltage flag
 * Bitfield: MONSF3.VBATOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Supply voltage VS12 over voltage flag  
 */
IFX_EXTERN IfxTlf35584_SupplyVoltageVs12OverVoltageFlag IfxTlf35584_getSupplyVoltageVs12OverVoltageFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Supply voltage VS12 over voltage flag
 * Bitfield: MONSF3.VBATOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSupplyVoltageVs12OverVoltageFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Supply voltage VS12 over voltage flag [Clear]
 * Bitfield: MONSF3.VBATOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearSupplyVoltageVs12OverVoltageFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Bandgap comparator under voltage condition flag
 * Bitfield: MONSF3.BG12UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Bandgap comparator under voltage condition flag  
 */
IFX_EXTERN IfxTlf35584_BandgapComparatorUnderVoltageConditionFlag IfxTlf35584_getBandgapComparatorUnderVoltageConditionFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Bandgap comparator under voltage condition flag
 * Bitfield: MONSF3.BG12UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isBandgapComparatorUnderVoltageConditionFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Bandgap comparator under voltage condition flag [Clear]
 * Bitfield: MONSF3.BG12UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearBandgapComparatorUnderVoltageConditionFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Bandgap comparator over voltage condition flag
 * Bitfield: MONSF3.BG12OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Bandgap comparator over voltage condition flag  
 */
IFX_EXTERN IfxTlf35584_BandgapComparatorOverVoltageConditionFlag IfxTlf35584_getBandgapComparatorOverVoltageConditionFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Bandgap comparator over voltage condition flag
 * Bitfield: MONSF3.BG12OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isBandgapComparatorOverVoltageConditionFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Bandgap comparator over voltage condition flag [Clear]
 * Bitfield: MONSF3.BG12OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearBandgapComparatorOverVoltageConditionFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Bias current too low flag
 * Bitfield: MONSF3.BIASLOW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Bias current too low flag  
 */
IFX_EXTERN IfxTlf35584_BiasCurrentTooLowFlag IfxTlf35584_getBiasCurrentTooLowFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Bias current too low flag
 * Bitfield: MONSF3.BIASLOW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isBiasCurrentTooLowFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Bias current too low flag [Clear]
 * Bitfield: MONSF3.BIASLOW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearBiasCurrentTooLowFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Bias current too high flag
 * Bitfield: MONSF3.BIASHI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Bias current too high flag  
 */
IFX_EXTERN IfxTlf35584_BiasCurrentTooHighFlag IfxTlf35584_getBiasCurrentTooHighFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Bias current too high flag
 * Bitfield: MONSF3.BIASHI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isBiasCurrentTooHighFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Bias current too high flag [Clear]
 * Bitfield: MONSF3.BIASHI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearBiasCurrentTooHighFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Pre-regulator over temperature flag
 * Bitfield: OTFAIL.PREG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Pre-regulator over temperature flag  
 */
IFX_EXTERN IfxTlf35584_PreRegulatorOverTemperatureFlag IfxTlf35584_getPreRegulatorOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Pre-regulator over temperature flag
 * Bitfield: OTFAIL.PREG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isPreRegulatorOverTemperatureFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Pre-regulator over temperature flag [Clear]
 * Bitfield: OTFAIL.PREG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearPreRegulatorOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get uC LDO over temperature flag
 * Bitfield: OTFAIL.UC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return uC LDO over temperature flag  
 */
IFX_EXTERN IfxTlf35584_UcLdoOverTemperatureFlag IfxTlf35584_getUcLdoOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get uC LDO over temperature flag
 * Bitfield: OTFAIL.UC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isUcLdoOverTemperatureFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set uC LDO over temperature flag [Clear]
 * Bitfield: OTFAIL.UC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearUcLdoOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO over temperature flag
 * Bitfield: OTFAIL.COM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Communication LDO over temperature flag  
 */
IFX_EXTERN IfxTlf35584_CommunicationLdoOverTemperatureFlag IfxTlf35584_getCommunicationLdoOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO over temperature flag
 * Bitfield: OTFAIL.COM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isCommunicationLdoOverTemperatureFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Communication LDO over temperature flag [Clear]
 * Bitfield: OTFAIL.COM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearCommunicationLdoOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Monitoring over temperature flag
 * Bitfield: OTFAIL.MON 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Monitoring over temperature flag  
 */
IFX_EXTERN IfxTlf35584_MonitoringOverTemperatureFlag IfxTlf35584_getMonitoringOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Monitoring over temperature flag
 * Bitfield: OTFAIL.MON 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isMonitoringOverTemperatureFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Monitoring over temperature flag [Clear]
 * Bitfield: OTFAIL.MON 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearMonitoringOverTemperatureFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Pre-regulator over temperature warning flag
 * Bitfield: OTWRNSF.PREG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Pre-regulator over temperature warning flag  
 */
IFX_EXTERN IfxTlf35584_PreRegulatorOverTemperatureWarningFlag IfxTlf35584_getPreRegulatorOverTemperatureWarningFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Pre-regulator over temperature warning flag
 * Bitfield: OTWRNSF.PREG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isPreRegulatorOverTemperatureWarningFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Pre-regulator over temperature warning flag [Clear]
 * Bitfield: OTWRNSF.PREG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearPreRegulatorOverTemperatureWarningFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get uC LDO over temperature warning flag
 * Bitfield: OTWRNSF.UC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return uC LDO over temperature warning flag  
 */
IFX_EXTERN IfxTlf35584_UcLdoOverTemperatureWarningFlag IfxTlf35584_getUcLdoOverTemperatureWarningFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get uC LDO over temperature warning flag
 * Bitfield: OTWRNSF.UC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isUcLdoOverTemperatureWarningFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set uC LDO over temperature warning flag [Clear]
 * Bitfield: OTWRNSF.UC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearUcLdoOverTemperatureWarningFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby LDO over load flag
 * Bitfield: OTWRNSF.STDBY 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Standby LDO over load flag  
 */
IFX_EXTERN IfxTlf35584_StandbyLdoOverLoadFlag IfxTlf35584_getStandbyLdoOverLoadFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby LDO over load flag
 * Bitfield: OTWRNSF.STDBY 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isStandbyLdoOverLoadFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Standby LDO over load flag [Clear]
 * Bitfield: OTWRNSF.STDBY 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearStandbyLdoOverLoadFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO over temperature warning flag
 * Bitfield: OTWRNSF.COM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Communication LDO over temperature warning flag  
 */
IFX_EXTERN IfxTlf35584_CommunicationLdoOverTemperatureWarningFlag IfxTlf35584_getCommunicationLdoOverTemperatureWarningFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO over temperature warning flag
 * Bitfield: OTWRNSF.COM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isCommunicationLdoOverTemperatureWarningFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Communication LDO over temperature warning flag [Clear]
 * Bitfield: OTWRNSF.COM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearCommunicationLdoOverTemperatureWarningFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage reference over load flag
 * Bitfield: OTWRNSF.VREF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Voltage reference over load flag  
 */
IFX_EXTERN IfxTlf35584_VoltageReferenceOverLoadFlag IfxTlf35584_getVoltageReferenceOverLoadFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage reference over load flag
 * Bitfield: OTWRNSF.VREF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isVoltageReferenceOverLoadFlagSet(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Voltage reference over load flag [Clear]
 * Bitfield: OTWRNSF.VREF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_clearVoltageReferenceOverLoadFlag(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby LDO voltage ready status
 * Bitfield: VMONSTAT.STBYST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Standby LDO voltage ready status  
 */
IFX_EXTERN IfxTlf35584_StandbyLdoVoltageReadyStatus IfxTlf35584_getStandbyLdoVoltageReadyStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby LDO voltage ready status
 * Bitfield: VMONSTAT.STBYST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isStandbyLdoVoltageReady(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Core voltage ready status
 * Bitfield: VMONSTAT.VCOREST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Core voltage ready status  
 */
IFX_EXTERN IfxTlf35584_CoreVoltageReadyStatus IfxTlf35584_getCoreVoltageReadyStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Core voltage ready status
 * Bitfield: VMONSTAT.VCOREST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isCoreVoltageReady(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO voltage ready status
 * Bitfield: VMONSTAT.COMST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Communication LDO voltage ready status  
 */
IFX_EXTERN IfxTlf35584_CommunicationLdoVoltageReadyStatus IfxTlf35584_getCommunicationLdoVoltageReadyStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO voltage ready status
 * Bitfield: VMONSTAT.COMST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isCommunicationLdoVoltageReady(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage reference voltage ready status
 * Bitfield: VMONSTAT.VREFST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Voltage reference voltage ready status  
 */
IFX_EXTERN IfxTlf35584_VoltageReferenceVoltageReadyStatus IfxTlf35584_getVoltageReferenceVoltageReadyStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Voltage reference voltage ready status
 * Bitfield: VMONSTAT.VREFST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isVoltageReferenceVoltageReady(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker1 voltage ready status
 * Bitfield: VMONSTAT.TRK1ST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Tracker1 voltage ready status  
 */
IFX_EXTERN IfxTlf35584_Tracker1VoltageReadyStatus IfxTlf35584_getTracker1VoltageReadyStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker1 voltage ready status
 * Bitfield: VMONSTAT.TRK1ST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isTracker1VoltageReady(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker2 voltage ready status
 * Bitfield: VMONSTAT.TRK2ST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Tracker2 voltage ready status  
 */
IFX_EXTERN IfxTlf35584_Tracker2VoltageReadyStatus IfxTlf35584_getTracker2VoltageReadyStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker2 voltage ready status
 * Bitfield: VMONSTAT.TRK2ST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isTracker2VoltageReady(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Device state
 * Bitfield: DEVSTAT.STATE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Device state  
 */
IFX_EXTERN IfxTlf35584_DeviceState IfxTlf35584_getDeviceState(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Reference voltage enable status
 * Bitfield: DEVSTAT.VREFEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Reference voltage enable status  
 */
IFX_EXTERN IfxTlf35584_ReferenceVoltageEnableStatus IfxTlf35584_getReferenceVoltageEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Reference voltage enable status
 * Bitfield: DEVSTAT.VREFEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isReferenceVoltageEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby LDO enable status
 * Bitfield: DEVSTAT.STBYEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Standby LDO enable status  
 */
IFX_EXTERN IfxTlf35584_StandbyLdoEnableStatus IfxTlf35584_getStandbyLdoEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Standby LDO enable status
 * Bitfield: DEVSTAT.STBYEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isStandbyLdoEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO enable status
 * Bitfield: DEVSTAT.COMEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Communication LDO enable status  
 */
IFX_EXTERN IfxTlf35584_CommunicationLdoEnableStatus IfxTlf35584_getCommunicationLdoEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Communication LDO enable status
 * Bitfield: DEVSTAT.COMEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isCommunicationLdoEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker1 voltage enable status
 * Bitfield: DEVSTAT.TRK1EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Tracker1 voltage enable status  
 */
IFX_EXTERN IfxTlf35584_Tracker1VoltageEnableStatus IfxTlf35584_getTracker1VoltageEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker1 voltage enable status
 * Bitfield: DEVSTAT.TRK1EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isTracker1VoltageEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker2 voltage enable status
 * Bitfield: DEVSTAT.TRK2EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Tracker2 voltage enable status  
 */
IFX_EXTERN IfxTlf35584_Tracker2VoltageEnableStatus IfxTlf35584_getTracker2VoltageEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Tracker2 voltage enable status
 * Bitfield: DEVSTAT.TRK2EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isTracker2VoltageEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Protected register lock status
 * Bitfield: PROTSTAT.LOCK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Protected register lock status  
 */
IFX_EXTERN IfxTlf35584_ProtectedRegisterLockStatus IfxTlf35584_getProtectedRegisterLockStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Protected register lock status
 * Bitfield: PROTSTAT.LOCK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isProtectedRegisterLocked(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Key1 ok status
 * Bitfield: PROTSTAT.KEY1OK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Key1 ok status  
 */
IFX_EXTERN IfxTlf35584_Key1OkStatus IfxTlf35584_getKey1OkStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Key2 ok status
 * Bitfield: PROTSTAT.KEY2OK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Key2 ok status  
 */
IFX_EXTERN IfxTlf35584_Key2OkStatus IfxTlf35584_getKey2OkStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Key3 ok status
 * Bitfield: PROTSTAT.KEY3OK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Key3 ok status  
 */
IFX_EXTERN IfxTlf35584_Key3OkStatus IfxTlf35584_getKey3OkStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Key4 ok status
 * Bitfield: PROTSTAT.KEY4OK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Key4 ok status  
 */
IFX_EXTERN IfxTlf35584_Key4OkStatus IfxTlf35584_getKey4OkStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Window watchdog error counter status
 * Bitfield: WWDSTAT.WWDECNT 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Window watchdog error counter status  
 */
IFX_EXTERN uint8 IfxTlf35584_getWindowWatchdogErrorCounterStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog question
 * Bitfield: FWDSTAT0.FWDQUEST 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Functional watchdog question  
 */
IFX_EXTERN uint8 IfxTlf35584_getFunctionalWatchdogQuestion(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog response counter value
 * Bitfield: FWDSTAT0.FWDRSPC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Functional watchdog response counter value  
 */
IFX_EXTERN uint8 IfxTlf35584_getFunctionalWatchdogResponseCounterValue(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog response check error status
 * Bitfield: FWDSTAT0.FWDRSPOK 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Functional watchdog response check error status  
 */
IFX_EXTERN IfxTlf35584_FunctionalWatchdogResponseCheckErrorStatus IfxTlf35584_getFunctionalWatchdogResponseCheckErrorStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Functional watchdog error counter value
 * Bitfield: FWDSTAT1.FWDECNT 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Functional watchdog error counter value  
 */
IFX_EXTERN uint8 IfxTlf35584_getFunctionalWatchdogErrorCounterValue(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Start ABIST operation
 * Bitfield: ABIST_CTRL0.START 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Start ABIST operation  
 */
IFX_EXTERN IfxTlf35584_StartAbistOperation IfxTlf35584_getStartAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Start ABIST operation
 * Bitfield: ABIST_CTRL0.START 
 * \param driver Device driver object  
 * \param value Start ABIST operation  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setStartAbistOperation(IfxTlf35584_Driver* driver, IfxTlf35584_StartAbistOperation value, boolean* status);

/** \brief Get Full path test selection
 * Bitfield: ABIST_CTRL0.PATH 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Full path test selection  
 */
IFX_EXTERN IfxTlf35584_FullPathTestSelection IfxTlf35584_getFullPathTestSelection(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Full path test selection
 * Bitfield: ABIST_CTRL0.PATH 
 * \param driver Device driver object  
 * \param value Full path test selection  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setFullPathTestSelection(IfxTlf35584_Driver* driver, IfxTlf35584_FullPathTestSelection value, boolean* status);

/** \brief Get ABIST Sequence selection
 * Bitfield: ABIST_CTRL0.SINGLE 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ABIST Sequence selection  
 */
IFX_EXTERN IfxTlf35584_AbistSequenceSelection IfxTlf35584_getAbistSequenceSelection(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set ABIST Sequence selection
 * Bitfield: ABIST_CTRL0.SINGLE 
 * \param driver Device driver object  
 * \param value ABIST Sequence selection  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setAbistSequenceSelection(IfxTlf35584_Driver* driver, IfxTlf35584_AbistSequenceSelection value, boolean* status);

/** \brief Get Safety path selection
 * Bitfield: ABIST_CTRL0.INT 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Safety path selection  
 */
IFX_EXTERN IfxTlf35584_SafetyPathSelection IfxTlf35584_getSafetyPathSelection(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Safety path selection
 * Bitfield: ABIST_CTRL0.INT 
 * \param driver Device driver object  
 * \param value Safety path selection  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSafetyPathSelection(IfxTlf35584_Driver* driver, IfxTlf35584_SafetyPathSelection value, boolean* status);

/** \brief Get ABIST global error status
 * Bitfield: ABIST_CTRL0.STATUS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ABIST global error status  
 */
IFX_EXTERN IfxTlf35584_AbistGlobalErrorStatus IfxTlf35584_getAbistGlobalErrorStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Overvoltage trigger for secondary internal monitor enable
 * Bitfield: ABIST_CTRL1.OV_TRIG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Overvoltage trigger for secondary internal monitor enable  
 */
IFX_EXTERN IfxTlf35584_OvervoltageTriggerForSecondaryInternalMonitorEnable IfxTlf35584_getOvervoltageTriggerForSecondaryInternalMonitorEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Overvoltage trigger for secondary internal monitor enable
 * Bitfield: ABIST_CTRL1.OV_TRIG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isOvervoltageTriggerForSecondaryInternalMonitorEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Overvoltage trigger for secondary internal monitor enable
 * Bitfield: ABIST_CTRL1.OV_TRIG 
 * \param driver Device driver object  
 * \param value Overvoltage trigger for secondary internal monitor enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setOvervoltageTriggerForSecondaryInternalMonitorEnable(IfxTlf35584_Driver* driver, IfxTlf35584_OvervoltageTriggerForSecondaryInternalMonitorEnable value, boolean* status);

/** \brief Set Overvoltage trigger for secondary internal monitor enable [Enable]
 * Bitfield: ABIST_CTRL1.OV_TRIG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableOvervoltageTriggerForSecondaryInternalMonitor(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Overvoltage trigger for secondary internal monitor enable [Disable]
 * Bitfield: ABIST_CTRL1.OV_TRIG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableOvervoltageTriggerForSecondaryInternalMonitor(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get ABIST clock check enable
 * Bitfield: ABIST_CTRL1.ABIST_CLK_EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ABIST clock check enable  
 */
IFX_EXTERN IfxTlf35584_AbistClockCheckEnable IfxTlf35584_getAbistClockCheckEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get ABIST clock check enable
 * Bitfield: ABIST_CTRL1.ABIST_CLK_EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isAbistClockCheckEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set ABIST clock check enable
 * Bitfield: ABIST_CTRL1.ABIST_CLK_EN 
 * \param driver Device driver object  
 * \param value ABIST clock check enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setAbistClockCheckEnable(IfxTlf35584_Driver* driver, IfxTlf35584_AbistClockCheckEnable value, boolean* status);

/** \brief Set ABIST clock check enable [Enable]
 * Bitfield: ABIST_CTRL1.ABIST_CLK_EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableAbistClockCheck(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set ABIST clock check enable [Disable]
 * Bitfield: ABIST_CTRL1.ABIST_CLK_EN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableAbistClockCheck(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select Pre-regulator OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.PREGOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select Pre-regulator OV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectPreRegulatorOvComparatorForAbistOperationEnable IfxTlf35584_getSelectPreRegulatorOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select Pre-regulator OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.PREGOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectPreRegulatorOvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select Pre-regulator OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.PREGOV 
 * \param driver Device driver object  
 * \param value Select Pre-regulator OV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectPreRegulatorOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectPreRegulatorOvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select Pre-regulator OV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT0.PREGOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectPreRegulatorOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select Pre-regulator OV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT0.PREGOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectPreRegulatorOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select uC LDO OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.UCOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select uC LDO OV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectUcLdoOvComparatorForAbistOperationEnable IfxTlf35584_getSelectUcLdoOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select uC LDO OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.UCOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectUcLdoOvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select uC LDO OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.UCOV 
 * \param driver Device driver object  
 * \param value Select uC LDO OV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectUcLdoOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectUcLdoOvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select uC LDO OV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT0.UCOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectUcLdoOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select uC LDO OV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT0.UCOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectUcLdoOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select Standby LDO OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.STBYOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select Standby LDO OV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectStandbyLdoOvComparatorForAbistOperationEnable IfxTlf35584_getSelectStandbyLdoOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select Standby LDO OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.STBYOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectStandbyLdoOvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select Standby LDO OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.STBYOV 
 * \param driver Device driver object  
 * \param value Select Standby LDO OV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectStandbyLdoOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectStandbyLdoOvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select Standby LDO OV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT0.STBYOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectStandbyLdoOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select Standby LDO OV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT0.STBYOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectStandbyLdoOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select Core voltage OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.VCOREOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select Core voltage OV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectCoreVoltageOvComparatorForAbistOperationEnable IfxTlf35584_getSelectCoreVoltageOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select Core voltage OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.VCOREOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectCoreVoltageOvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select Core voltage OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.VCOREOV 
 * \param driver Device driver object  
 * \param value Select Core voltage OV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectCoreVoltageOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectCoreVoltageOvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select Core voltage OV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT0.VCOREOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectCoreVoltageOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select Core voltage OV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT0.VCOREOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectCoreVoltageOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select COM OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.COMOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select COM OV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectComOvComparatorForAbistOperationEnable IfxTlf35584_getSelectComOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select COM OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.COMOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectComOvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select COM OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.COMOV 
 * \param driver Device driver object  
 * \param value Select COM OV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectComOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectComOvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select COM OV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT0.COMOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectComOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select COM OV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT0.COMOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectComOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select VREF OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.VREFOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select VREF OV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectVrefOvComparatorForAbistOperationEnable IfxTlf35584_getSelectVrefOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select VREF OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.VREFOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectVrefOvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select VREF OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.VREFOV 
 * \param driver Device driver object  
 * \param value Select VREF OV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectVrefOvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectVrefOvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select VREF OV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT0.VREFOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectVrefOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select VREF OV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT0.VREFOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectVrefOvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select TRK1 OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.TRK1OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select TRK1 OV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectTrk1OvComparatorForAbistOperationEnable IfxTlf35584_getSelectTrk1OvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select TRK1 OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.TRK1OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectTrk1OvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select TRK1 OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.TRK1OV 
 * \param driver Device driver object  
 * \param value Select TRK1 OV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectTrk1OvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectTrk1OvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select TRK1 OV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT0.TRK1OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectTrk1OvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select TRK1 OV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT0.TRK1OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectTrk1OvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select TRK2 OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.TRK2OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select TRK2 OV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectTrk2OvComparatorForAbistOperationEnable IfxTlf35584_getSelectTrk2OvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select TRK2 OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.TRK2OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectTrk2OvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select TRK2 OV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT0.TRK2OV 
 * \param driver Device driver object  
 * \param value Select TRK2 OV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectTrk2OvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectTrk2OvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select TRK2 OV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT0.TRK2OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectTrk2OvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select TRK2 OV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT0.TRK2OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectTrk2OvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select pre regulator UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.PREGUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select pre regulator UV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectPreRegulatorUvComparatorForAbistOperationEnable IfxTlf35584_getSelectPreRegulatorUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select pre regulator UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.PREGUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectPreRegulatorUvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select pre regulator UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.PREGUV 
 * \param driver Device driver object  
 * \param value Select pre regulator UV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectPreRegulatorUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectPreRegulatorUvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select pre regulator UV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT1.PREGUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectPreRegulatorUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select pre regulator UV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT1.PREGUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectPreRegulatorUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select uC UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.UCUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select uC UV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectUcUvComparatorForAbistOperationEnable IfxTlf35584_getSelectUcUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select uC UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.UCUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectUcUvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select uC UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.UCUV 
 * \param driver Device driver object  
 * \param value Select uC UV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectUcUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectUcUvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select uC UV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT1.UCUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectUcUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select uC UV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT1.UCUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectUcUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select STBY UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.STBYUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select STBY UV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectStbyUvComparatorForAbistOperationEnable IfxTlf35584_getSelectStbyUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select STBY UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.STBYUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectStbyUvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select STBY UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.STBYUV 
 * \param driver Device driver object  
 * \param value Select STBY UV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectStbyUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectStbyUvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select STBY UV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT1.STBYUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectStbyUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select STBY UV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT1.STBYUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectStbyUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select VCORE UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.VCOREUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select VCORE UV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectVcoreUvComparatorForAbistOperationEnable IfxTlf35584_getSelectVcoreUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select VCORE UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.VCOREUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectVcoreUvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select VCORE UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.VCOREUV 
 * \param driver Device driver object  
 * \param value Select VCORE UV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectVcoreUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectVcoreUvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select VCORE UV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT1.VCOREUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectVcoreUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select VCORE UV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT1.VCOREUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectVcoreUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select COM UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.COMUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select COM UV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectComUvComparatorForAbistOperationEnable IfxTlf35584_getSelectComUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select COM UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.COMUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectComUvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select COM UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.COMUV 
 * \param driver Device driver object  
 * \param value Select COM UV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectComUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectComUvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select COM UV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT1.COMUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectComUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select COM UV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT1.COMUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectComUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select VREF UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.VREFUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select VREF UV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectVrefUvComparatorForAbistOperationEnable IfxTlf35584_getSelectVrefUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select VREF UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.VREFUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectVrefUvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select VREF UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.VREFUV 
 * \param driver Device driver object  
 * \param value Select VREF UV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectVrefUvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectVrefUvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select VREF UV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT1.VREFUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectVrefUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select VREF UV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT1.VREFUV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectVrefUvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select TRK1 UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.TRK1UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select TRK1 UV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectTrk1UvComparatorForAbistOperationEnable IfxTlf35584_getSelectTrk1UvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select TRK1 UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.TRK1UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectTrk1UvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select TRK1 UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.TRK1UV 
 * \param driver Device driver object  
 * \param value Select TRK1 UV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectTrk1UvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectTrk1UvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select TRK1 UV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT1.TRK1UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectTrk1UvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select TRK1 UV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT1.TRK1UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectTrk1UvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select TRK2 UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.TRK2UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select TRK2 UV comparator for ABIST operation enable  
 */
IFX_EXTERN IfxTlf35584_SelectTrk2UvComparatorForAbistOperationEnable IfxTlf35584_getSelectTrk2UvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select TRK2 UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.TRK2UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectTrk2UvComparatorForAbistOperationEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select TRK2 UV comparator for ABIST operation enable
 * Bitfield: ABIST_SELECT1.TRK2UV 
 * \param driver Device driver object  
 * \param value Select TRK2 UV comparator for ABIST operation enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectTrk2UvComparatorForAbistOperationEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectTrk2UvComparatorForAbistOperationEnable value, boolean* status);

/** \brief Set Select TRK2 UV comparator for ABIST operation enable [Enable]
 * Bitfield: ABIST_SELECT1.TRK2UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectTrk2UvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select TRK2 UV comparator for ABIST operation enable [Disable]
 * Bitfield: ABIST_SELECT1.TRK2UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectTrk2UvComparatorForAbistOperation(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select supply VS12 overvoltage enable
 * Bitfield: ABIST_SELECT2.VBATOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select supply VS12 overvoltage enable  
 */
IFX_EXTERN IfxTlf35584_SelectSupplyVs12OvervoltageEnable IfxTlf35584_getSelectSupplyVs12OvervoltageEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select supply VS12 overvoltage enable
 * Bitfield: ABIST_SELECT2.VBATOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectSupplyVs12OvervoltageEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select supply VS12 overvoltage enable
 * Bitfield: ABIST_SELECT2.VBATOV 
 * \param driver Device driver object  
 * \param value Select supply VS12 overvoltage enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectSupplyVs12OvervoltageEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectSupplyVs12OvervoltageEnable value, boolean* status);

/** \brief Set Select supply VS12 overvoltage enable [Enable]
 * Bitfield: ABIST_SELECT2.VBATOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectSupplyVs12Overvoltage(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select supply VS12 overvoltage enable [Disable]
 * Bitfield: ABIST_SELECT2.VBATOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectSupplyVs12Overvoltage(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select internal supply OV condition enable
 * Bitfield: ABIST_SELECT2.INTOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select internal supply OV condition enable  
 */
IFX_EXTERN IfxTlf35584_SelectInternalSupplyOvConditionEnable IfxTlf35584_getSelectInternalSupplyOvConditionEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select internal supply OV condition enable
 * Bitfield: ABIST_SELECT2.INTOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectInternalSupplyOvConditionEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select internal supply OV condition enable
 * Bitfield: ABIST_SELECT2.INTOV 
 * \param driver Device driver object  
 * \param value Select internal supply OV condition enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectInternalSupplyOvConditionEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectInternalSupplyOvConditionEnable value, boolean* status);

/** \brief Set Select internal supply OV condition enable [Enable]
 * Bitfield: ABIST_SELECT2.INTOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectInternalSupplyOvCondition(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select internal supply OV condition enable [Disable]
 * Bitfield: ABIST_SELECT2.INTOV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectInternalSupplyOvCondition(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select bandgap comparator UV condition enable
 * Bitfield: ABIST_SELECT2.BG12UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select bandgap comparator UV condition enable  
 */
IFX_EXTERN IfxTlf35584_SelectBandgapComparatorUvConditionEnable IfxTlf35584_getSelectBandgapComparatorUvConditionEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select bandgap comparator UV condition enable
 * Bitfield: ABIST_SELECT2.BG12UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectBandgapComparatorUvConditionEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select bandgap comparator UV condition enable
 * Bitfield: ABIST_SELECT2.BG12UV 
 * \param driver Device driver object  
 * \param value Select bandgap comparator UV condition enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectBandgapComparatorUvConditionEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectBandgapComparatorUvConditionEnable value, boolean* status);

/** \brief Set Select bandgap comparator UV condition enable [Enable]
 * Bitfield: ABIST_SELECT2.BG12UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectBandgapComparatorUvCondition(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select bandgap comparator UV condition enable [Disable]
 * Bitfield: ABIST_SELECT2.BG12UV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectBandgapComparatorUvCondition(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select bandgap comparator OV condition enable
 * Bitfield: ABIST_SELECT2.BG12OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select bandgap comparator OV condition enable  
 */
IFX_EXTERN IfxTlf35584_SelectBandgapComparatorOvConditionEnable IfxTlf35584_getSelectBandgapComparatorOvConditionEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select bandgap comparator OV condition enable
 * Bitfield: ABIST_SELECT2.BG12OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectBandgapComparatorOvConditionEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select bandgap comparator OV condition enable
 * Bitfield: ABIST_SELECT2.BG12OV 
 * \param driver Device driver object  
 * \param value Select bandgap comparator OV condition enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectBandgapComparatorOvConditionEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectBandgapComparatorOvConditionEnable value, boolean* status);

/** \brief Set Select bandgap comparator OV condition enable [Enable]
 * Bitfield: ABIST_SELECT2.BG12OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectBandgapComparatorOvCondition(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select bandgap comparator OV condition enable [Disable]
 * Bitfield: ABIST_SELECT2.BG12OV 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectBandgapComparatorOvCondition(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select bias current too low enable
 * Bitfield: ABIST_SELECT2.BIASLOW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select bias current too low enable  
 */
IFX_EXTERN IfxTlf35584_SelectBiasCurrentTooLowEnable IfxTlf35584_getSelectBiasCurrentTooLowEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select bias current too low enable
 * Bitfield: ABIST_SELECT2.BIASLOW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectBiasCurrentTooLowEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select bias current too low enable
 * Bitfield: ABIST_SELECT2.BIASLOW 
 * \param driver Device driver object  
 * \param value Select bias current too low enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectBiasCurrentTooLowEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectBiasCurrentTooLowEnable value, boolean* status);

/** \brief Set Select bias current too low enable [Enable]
 * Bitfield: ABIST_SELECT2.BIASLOW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectBiasCurrentTooLow(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select bias current too low enable [Disable]
 * Bitfield: ABIST_SELECT2.BIASLOW 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectBiasCurrentTooLow(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select bias current too high enable
 * Bitfield: ABIST_SELECT2.BIASHI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Select bias current too high enable  
 */
IFX_EXTERN IfxTlf35584_SelectBiasCurrentTooHighEnable IfxTlf35584_getSelectBiasCurrentTooHighEnable(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Select bias current too high enable
 * Bitfield: ABIST_SELECT2.BIASHI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isSelectBiasCurrentTooHighEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select bias current too high enable
 * Bitfield: ABIST_SELECT2.BIASHI 
 * \param driver Device driver object  
 * \param value Select bias current too high enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSelectBiasCurrentTooHighEnable(IfxTlf35584_Driver* driver, IfxTlf35584_SelectBiasCurrentTooHighEnable value, boolean* status);

/** \brief Set Select bias current too high enable [Enable]
 * Bitfield: ABIST_SELECT2.BIASHI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableSelectBiasCurrentTooHigh(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Select bias current too high enable [Disable]
 * Bitfield: ABIST_SELECT2.BIASHI 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableSelectBiasCurrentTooHigh(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get BUCK switching frequency change
 * Bitfield: BCK_FREQ_CHANGE.BCK_FREQ_SEL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return BUCK switching frequency change  
 */
IFX_EXTERN IfxTlf35584_BuckSwitchingFrequencyChange IfxTlf35584_getBuckSwitchingFrequencyChange(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set BUCK switching frequency change
 * Bitfield: BCK_FREQ_CHANGE.BCK_FREQ_SEL 
 * \param driver Device driver object  
 * \param value BUCK switching frequency change  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setBuckSwitchingFrequencyChange(IfxTlf35584_Driver* driver, IfxTlf35584_BuckSwitchingFrequencyChange value, boolean* status);

/** \brief Get Spread spectrum down spread
 * Bitfield: BCK_FRE_SPREAD.FRE_SP_THR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Spread spectrum down spread  
 */
IFX_EXTERN IfxTlf35584_SpreadSpectrumDownSpread IfxTlf35584_getSpreadSpectrumDownSpread(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Spread spectrum down spread
 * Bitfield: BCK_FRE_SPREAD.FRE_SP_THR 
 * \param driver Device driver object  
 * \param value Spread spectrum down spread  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setSpreadSpectrumDownSpread(IfxTlf35584_Driver* driver, IfxTlf35584_SpreadSpectrumDownSpread value, boolean* status);

/** \brief Get Enable buck update
 * Bitfield: BCK_MAIN_CTRL.DATA_VALID 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Enable buck update  
 */
IFX_EXTERN IfxTlf35584_EnableBuckUpdate IfxTlf35584_getEnableBuckUpdate(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Enable buck update
 * Bitfield: BCK_MAIN_CTRL.DATA_VALID 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isBuckUpdateEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Enable buck update
 * Bitfield: BCK_MAIN_CTRL.DATA_VALID 
 * \param driver Device driver object  
 * \param value Enable buck update  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_setEnableBuckUpdate(IfxTlf35584_Driver* driver, IfxTlf35584_EnableBuckUpdate value, boolean* status);

/** \brief Set Enable buck update
 * Bitfield: BCK_MAIN_CTRL.DATA_VALID 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_enableBuckUpdate(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Set Enable buck update
 * Bitfield: BCK_MAIN_CTRL.DATA_VALID 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void IfxTlf35584_disableBuckUpdate(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get DATA_VALID parameter update ready status
 * Bitfield: BCK_MAIN_CTRL.BUSY 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return DATA_VALID parameter update ready status  
 */
IFX_EXTERN IfxTlf35584_DatavalidParameterUpdateReadyStatus IfxTlf35584_getDatavalidParameterUpdateReadyStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Test mode enable status
 * Bitfield: GTM.TM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Test mode enable status  
 */
IFX_EXTERN IfxTlf35584_TestModeEnableStatus IfxTlf35584_getTestModeEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Test mode enable status
 * Bitfield: GTM.TM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean IfxTlf35584_isTestModeEnabled(IfxTlf35584_Driver* driver, boolean* status);

/** \brief Get Test mode inverted enable status
 * Bitfield: GTM.NTM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Test mode inverted enable status  
 */
IFX_EXTERN IfxTlf35584_TestModeInvertedEnableStatus IfxTlf35584_getTestModeInvertedEnableStatus(IfxTlf35584_Driver* driver, boolean* status);

/** \} */  


/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \brief Register information dump 
 * \param address Register address  
 * \param value Register value  
 * \param text Returned information text  
 * \param maxLength Text max length  
 * \return TRUE in case of success else FALSE  
 */
IFX_EXTERN boolean IfxTlf35584_registerDump(IfxTlf35584_Address address, IfxTlf35584_UData value, char* text, size_t maxLength);

/******************************************************************************/
/*-------------------Global Exported Variables/Constants----------------------*/
/******************************************************************************/

/** \brief Action read register flags
 *  Bitfield list:  
 */
IFX_EXTERN IFX_CONST uint32 IfxTlf35584_g_actionReadRegisters[2];

/** \brief Action write register flags
 *  Bitfield list:
 * - PROTCFG: KEY
 * - DEVCTRL: STATEREQ
 * - DEVCTRLN: STATEREQ
 * - WWDSCMD: TRIG
 * - FWDRSP: FWDRSP
 * - FWDRSPSYNC: FWDRSPS
 * - SYSFAIL: VOLTSELERR, OTF, VMONF, ABISTERR, INITF
 * - INITERR: VMONF, WWDF, FWDF, ERRF, SOFTRES, HARDRES
 * - IF: SYS, WK, SPI, MON, OTW, OTF, ABIST
 * - SYSSF: CFGE, WWDE, FWDE, ERRMISS, TRFAIL, NO_OP
 * - WKSF: WAK, ENA, CMON, WKTIM, WKSPI
 * - SPISF: PARE, LENE, ADDRE, DURE, LOCK
 * - MONSF0: PREGSG, UCSG, STBYSG, VCORESG, COMSG, VREFSG, TRK1SG, TRK2SG
 * - MONSF1: PREGOV, UCOV, STBYOV, VCOREOV, COMOV, VREFOV, TRK1OV, TRK2OV
 * - MONSF2: PREGUV, UCUV, STBYUV, VCOREUV, COMUV, VREFUV, TRK1UV, TRK2UV
 * - MONSF3: VBATOV, BG12UV, BG12OV, BIASLOW, BIASHI
 * - OTFAIL: PREG, UC, COM, MON
 * - OTWRNSF: PREG, UC, STDBY, COM, VREF
 * - ABIST_CTRL0: START  
 */
IFX_EXTERN IFX_CONST uint32 IfxTlf35584_g_actionWriteRegisters[2];

/** \brief No operation masks  
 */
IFX_EXTERN IFX_CONST Ifx_TLF35584 IfxTlf35584_g_noOpMask;

/** \brief No operation values  
 */
IFX_EXTERN IFX_CONST Ifx_TLF35584 IfxTlf35584_g_noOpValue;

/** \brief Valid address flags  
 */
IFX_EXTERN IFX_CONST uint32 IfxTlf35584_g_validAddress[2];

/** \brief Volatile register flags
 *  Bitfield list:
 * - DEVCFG2: FRE, STU, EVCEN
 * - RSYSPCFG0: STBYEN
 * - RSYSPCFG1: ERRREC, ERRRECEN, ERREN, ERRSLPEN, SS2DEL
 * - RWDCFG0: WDCYC, WWDTSEL, FWDEN, WWDEN, WWDETHR
 * - RWDCFG1: FWDETHR, WDSLPEN
 * - RFWDCFG: WDHBTP
 * - RWWDCFG0: CW
 * - RWWDCFG1: OW
 * - DEVCTRL: STATEREQ
 * - DEVCTRLN: STATEREQ
 * - WWDSCMD: TRIG_STATUS
 * - SYSFAIL: VOLTSELERR, OTF, VMONF, ABISTERR, INITF
 * - INITERR: VMONF, WWDF, FWDF, ERRF, SOFTRES, HARDRES
 * - IF: SYS, WK, SPI, MON, OTW, OTF, ABIST, INTMISS
 * - SYSSF: CFGE, WWDE, FWDE, ERRMISS, TRFAIL, NO_OP
 * - WKSF: WAK, ENA, CMON, WKTIM, WKSPI
 * - SPISF: PARE, LENE, ADDRE, DURE, LOCK
 * - MONSF0: PREGSG, UCSG, STBYSG, VCORESG, COMSG, VREFSG, TRK1SG, TRK2SG
 * - MONSF1: PREGOV, UCOV, STBYOV, VCOREOV, COMOV, VREFOV, TRK1OV, TRK2OV
 * - MONSF2: PREGUV, UCUV, STBYUV, VCOREUV, COMUV, VREFUV, TRK1UV, TRK2UV
 * - MONSF3: VBATOV, BG12UV, BG12OV, BIASLOW, BIASHI
 * - OTFAIL: PREG, UC, COM, MON
 * - OTWRNSF: PREG, UC, STDBY, COM, VREF
 * - VMONSTAT: STBYST, VCOREST, COMST, VREFST, TRK1ST, TRK2ST
 * - DEVSTAT: STATE, VREFEN, STBYEN, COMEN, TRK1EN, TRK2EN
 * - PROTSTAT: LOCK, KEY1OK, KEY2OK, KEY3OK, KEY4OK
 * - WWDSTAT: WWDECNT
 * - FWDSTAT0: FWDQUEST, FWDRSPC, FWDRSPOK
 * - FWDSTAT1: FWDECNT
 * - ABIST_CTRL0: START, STATUS
 * - ABIST_SELECT0: PREGOV, UCOV, STBYOV, VCOREOV, COMOV, VREFOV, TRK1OV, TRK2OV
 * - ABIST_SELECT1: PREGUV, UCUV, STBYUV, VCOREUV, COMUV, VREFUV, TRK1UV, TRK2UV
 * - ABIST_SELECT2: VBATOV, INTOV, BG12UV, BG12OV, BIASLOW, BIASHI
 * - BCK_MAIN_CTRL: BUSY
 * - GTM: TM, NTM  
 */
IFX_EXTERN IFX_CONST uint32 IfxTlf35584_g_volatileRegisters[2];


#endif /* IFXTLF35584_H */
