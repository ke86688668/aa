/**
 * \file IfxTlf35584_regdef.h
 * \brief
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved.
 *
 *
 * Date: 2017-05-15 04:24:31 GMT
 * Version: TBD
 * Specification: TBD
 * MAY BE CHANGED BY USER [yes/no]: Yes
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_Tlf35584_Registers Tlf35584 Registers
 * \ingroup IfxLld_Tlf35584
 * 
 * \defgroup IfxLld_Tlf35584_Registers_Bitfields Bitfields
 * \ingroup IfxLld_Tlf35584_Registers
 * 
 * \defgroup IfxLld_Tlf35584_Registers_union Register unions
 * \ingroup IfxLld_Tlf35584_Registers
 * 
 * \defgroup IfxLld_Tlf35584_Registers_struct Memory map
 * \ingroup IfxLld_Tlf35584_Registers
 */
#ifndef IFXTLF35584_REGDEF_H
#define IFXTLF35584_REGDEF_H 1
/******************************************************************************/
#include "Platform_Types.h"
#include "Compilers.h"
#include "Ifx_TypesReg.h"
#define IFXTLF35584_ADDRESS_UNIT  (1)
#define IFXTLF35584_ADDRESS_SHIFT (0)
typedef uint8 IfxTlf35584_Address;
typedef Ifx_UReg_8Bit IfxTlf35584_UData;
typedef Ifx_UReg_8Bit IfxTlf35584_SData;
/******************************************************************************/
/** \addtogroup IfxLld_Tlf35584_Registers_Bitfields
 * \{  */
/** \brief ABIST control0 *R2) */
typedef struct _Ifx_TLF35584_ABIST_CTRL0_Bits
{
    Ifx_UReg_8Bit START:1;            /**< \brief [0:0] Start ABIST operation (rwh) */
    Ifx_UReg_8Bit PATH:1;             /**< \brief [1:1] Full path test selection (rw) */
    Ifx_UReg_8Bit SINGLE:1;           /**< \brief [2:2] ABIST Sequence selection (rw) */
    Ifx_UReg_8Bit INT:1;              /**< \brief [3:3] Safety path selection (rw) */
    Ifx_UReg_8Bit STATUS:4;           /**< \brief [7:4] ABIST global error status (rh) */
} Ifx_TLF35584_ABIST_CTRL0_Bits;

/** \brief ABIST control1 *R2) */
typedef struct _Ifx_TLF35584_ABIST_CTRL1_Bits
{
    Ifx_UReg_8Bit OV_TRIG:1;          /**< \brief [0:0] Overvoltage trigger for secondary internal monitor enable (rw) */
    Ifx_UReg_8Bit ABIST_CLK_EN:1;     /**< \brief [1:1] ABIST clock check enable (rw) */
    Ifx_UReg_8Bit reserved_2:6;       /**< \brief [7:2] \internal Reserved */
} Ifx_TLF35584_ABIST_CTRL1_Bits;

/** \brief ABIST select 0 *R2) */
typedef struct _Ifx_TLF35584_ABIST_SELECT0_Bits
{
    Ifx_UReg_8Bit PREGOV:1;           /**< \brief [0:0] Select Pre-regulator OV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit UCOV:1;             /**< \brief [1:1] Select uC LDO OV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit STBYOV:1;           /**< \brief [2:2] Select Standby LDO OV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit VCOREOV:1;          /**< \brief [3:3] Select Core voltage OV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit COMOV:1;            /**< \brief [4:4] Select COM OV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit VREFOV:1;           /**< \brief [5:5] Select VREF OV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit TRK1OV:1;           /**< \brief [6:6] Select TRK1 OV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit TRK2OV:1;           /**< \brief [7:7] Select TRK2 OV comparator for ABIST operation enable (rwh) */
} Ifx_TLF35584_ABIST_SELECT0_Bits;

/** \brief ABIST select 1 *R2) */
typedef struct _Ifx_TLF35584_ABIST_SELECT1_Bits
{
    Ifx_UReg_8Bit PREGUV:1;           /**< \brief [0:0] Select pre regulator UV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit UCUV:1;             /**< \brief [1:1] Select uC UV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit STBYUV:1;           /**< \brief [2:2] Select STBY UV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit VCOREUV:1;          /**< \brief [3:3] Select VCORE UV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit COMUV:1;            /**< \brief [4:4] Select COM UV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit VREFUV:1;           /**< \brief [5:5] Select VREF UV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit TRK1UV:1;           /**< \brief [6:6] Select TRK1 UV comparator for ABIST operation enable (rwh) */
    Ifx_UReg_8Bit TRK2UV:1;           /**< \brief [7:7] Select TRK2 UV comparator for ABIST operation enable (rwh) */
} Ifx_TLF35584_ABIST_SELECT1_Bits;

/** \brief ABIST select 2 *R2) */
typedef struct _Ifx_TLF35584_ABIST_SELECT2_Bits
{
    Ifx_UReg_8Bit VBATOV:1;           /**< \brief [0:0] Select supply VS12 overvoltage enable (rwh) */
    Ifx_UReg_8Bit reserved_1:2;       /**< \brief [2:1] \internal Reserved */
    Ifx_UReg_8Bit INTOV:1;            /**< \brief [3:3] Select internal supply OV condition enable (rwh) */
    Ifx_UReg_8Bit BG12UV:1;           /**< \brief [4:4] Select bandgap comparator UV condition enable (rwh) */
    Ifx_UReg_8Bit BG12OV:1;           /**< \brief [5:5] Select bandgap comparator OV condition enable (rwh) */
    Ifx_UReg_8Bit BIASLOW:1;          /**< \brief [6:6] Select bias current too low enable (rwh) */
    Ifx_UReg_8Bit BIASHI:1;           /**< \brief [7:7] Select bias current too high enable (rwh) */
} Ifx_TLF35584_ABIST_SELECT2_Bits;

/** \brief Buck switching frequency change *R2) */
typedef struct _Ifx_TLF35584_BCK_FREQ_CHANGE_Bits
{
    Ifx_UReg_8Bit BCK_FREQ_SEL:3;     /**< \brief [2:0] BUCK switching frequency change (rw) */
    Ifx_UReg_8Bit reserved_3:5;       /**< \brief [7:3] \internal Reserved */
} Ifx_TLF35584_BCK_FREQ_CHANGE_Bits;

/** \brief Buck Frequency spread *R2) */
typedef struct _Ifx_TLF35584_BCK_FRE_SPREAD_Bits
{
    Ifx_UReg_8Bit FRE_SP_THR:8;       /**< \brief [7:0] Spread spectrum down spread (rw) */
} Ifx_TLF35584_BCK_FRE_SPREAD_Bits;

/** \brief Buck main control *R2) */
typedef struct _Ifx_TLF35584_BCK_MAIN_CTRL_Bits
{
    Ifx_UReg_8Bit reserved_0:6;       /**< \brief [5:0] \internal Reserved */
    Ifx_UReg_8Bit DATA_VALID:1;       /**< \brief [6:6] Enable buck update (rw) */
    Ifx_UReg_8Bit BUSY:1;             /**< \brief [7:7] DATA_VALID parameter update ready status (rh) */
} Ifx_TLF35584_BCK_MAIN_CTRL_Bits;

/** \brief Device configuration 0  *R2) */
typedef struct _Ifx_TLF35584_DEVCFG0_Bits
{
    Ifx_UReg_8Bit TRDEL:4;            /**< \brief [3:0] Transition delay into low power states (rw) */
    Ifx_UReg_8Bit reserved_4:2;       /**< \brief [5:4] \internal Reserved */
    Ifx_UReg_8Bit WKTIMCYC:1;         /**< \brief [6:6] Wake timer cycle period (rw) */
    Ifx_UReg_8Bit WKTIMEN:1;          /**< \brief [7:7] Wake timer enable (rw) */
} Ifx_TLF35584_DEVCFG0_Bits;

/** \brief Device configuration 1 *R0) */
typedef struct _Ifx_TLF35584_DEVCFG1_Bits
{
    Ifx_UReg_8Bit RESDEL:3;           /**< \brief [2:0] Reset release delay time (rw) */
    Ifx_UReg_8Bit reserved_3:5;       /**< \brief [7:3] \internal Reserved */
} Ifx_TLF35584_DEVCFG1_Bits;

/** \brief Device configuration 2 *R2) */
typedef struct _Ifx_TLF35584_DEVCFG2_Bits
{
    Ifx_UReg_8Bit ESYNEN:1;           /**< \brief [0:0] Synchronization output for external switchmode regulator\nenable (rw) */
    Ifx_UReg_8Bit ESYNPHA:1;          /**< \brief [1:1] External synchronization output phase (rw) */
    Ifx_UReg_8Bit CTHR:2;             /**< \brief [3:2] QUC current monitoring threshold value (rw) */
    Ifx_UReg_8Bit CMONEN:1;           /**< \brief [4:4] QUC current monitor enable for transition to a low\npower state (rw) */
    Ifx_UReg_8Bit FRE:1;              /**< \brief [5:5] Step-down converter frequency selection status (rh) */
    Ifx_UReg_8Bit STU:1;              /**< \brief [6:6] Step-up converter enable status (rh) */
    Ifx_UReg_8Bit EVCEN:1;            /**< \brief [7:7] External core supply enable status (rh) */
} Ifx_TLF35584_DEVCFG2_Bits;

/** \brief Device control request *R2) */
typedef struct _Ifx_TLF35584_DEVCTRL_Bits
{
    Ifx_UReg_8Bit STATEREQ:3;         /**< \brief [2:0] Request for device state transition (rwh) */
    Ifx_UReg_8Bit VREFEN:1;           /**< \brief [3:3] Request voltage reference QVR enable (rw) */
    Ifx_UReg_8Bit reserved_4:1;       /**< \brief [4:4] \internal Reserved */
    Ifx_UReg_8Bit COMEN:1;            /**< \brief [5:5] Request communication ldo QCO enable (rw) */
    Ifx_UReg_8Bit TRK1EN:1;           /**< \brief [6:6] Request tracker1 QT1 enable (rw) */
    Ifx_UReg_8Bit TRK2EN:1;           /**< \brief [7:7] Request tracker2 QT2 enable (rw) */
} Ifx_TLF35584_DEVCTRL_Bits;

/** \brief Device control inverted request *R2) */
typedef struct _Ifx_TLF35584_DEVCTRLN_Bits
{
    Ifx_UReg_8Bit STATEREQ:3;         /**< \brief [2:0] Request for device state transition (rwh) */
    Ifx_UReg_8Bit VREFEN:1;           /**< \brief [3:3] Request voltage reference QVR enable (rw) */
    Ifx_UReg_8Bit reserved_4:1;       /**< \brief [4:4] \internal Reserved */
    Ifx_UReg_8Bit COMEN:1;            /**< \brief [5:5] Request communication ldo QCO enable (rw) */
    Ifx_UReg_8Bit TRK1EN:1;           /**< \brief [6:6] Request tracker1 QT1 enable (rw) */
    Ifx_UReg_8Bit TRK2EN:1;           /**< \brief [7:7] Request tracker2 QT2 enable (rw) */
} Ifx_TLF35584_DEVCTRLN_Bits;

/** \brief Device status  *R2) */
typedef struct _Ifx_TLF35584_DEVSTAT_Bits
{
    Ifx_UReg_8Bit STATE:3;            /**< \brief [2:0] Device state (rh) */
    Ifx_UReg_8Bit VREFEN:1;           /**< \brief [3:3] Reference voltage enable status (rh) */
    Ifx_UReg_8Bit STBYEN:1;           /**< \brief [4:4] Standby LDO enable status (rh) */
    Ifx_UReg_8Bit COMEN:1;            /**< \brief [5:5] Communication LDO enable status (rh) */
    Ifx_UReg_8Bit TRK1EN:1;           /**< \brief [6:6] Tracker1 voltage enable status (rh) */
    Ifx_UReg_8Bit TRK2EN:1;           /**< \brief [7:7] Tracker2 voltage enable status (rh) */
} Ifx_TLF35584_DEVSTAT_Bits;

/** \brief Protected Functional watchdog configuration request *R2) */
typedef struct _Ifx_TLF35584_FWDCFG_Bits
{
    Ifx_UReg_8Bit WDHBTP:5;           /**< \brief [4:0] Request functional watchdog heartbeat timer period (rwh) */
    Ifx_UReg_8Bit reserved_5:3;       /**< \brief [7:5] \internal Reserved */
} Ifx_TLF35584_FWDCFG_Bits;

/** \brief Functional watchdog response command  *R2) */
typedef struct _Ifx_TLF35584_FWDRSP_Bits
{
    Ifx_UReg_8Bit FWDRSP:8;           /**< \brief [7:0] Functional watchdog response (rw) */
} Ifx_TLF35584_FWDRSP_Bits;

/** \brief Functional watchdog response command with synchronization\n*R2) */
typedef struct _Ifx_TLF35584_FWDRSPSYNC_Bits
{
    Ifx_UReg_8Bit FWDRSPS:8;          /**< \brief [7:0] Functional watchdog heartbeat synchronization response (rw) */
} Ifx_TLF35584_FWDRSPSYNC_Bits;

/** \brief Functional watchdog status 0 *R3) */
typedef struct _Ifx_TLF35584_FWDSTAT0_Bits
{
    Ifx_UReg_8Bit FWDQUEST:4;         /**< \brief [3:0] Functional watchdog question (rh) */
    Ifx_UReg_8Bit FWDRSPC:2;          /**< \brief [5:4] Functional watchdog response counter value (rh) */
    Ifx_UReg_8Bit FWDRSPOK:1;         /**< \brief [6:6] Functional watchdog response check error status (rh) */
    Ifx_UReg_8Bit reserved_7:1;       /**< \brief [7:7] \internal Reserved */
} Ifx_TLF35584_FWDSTAT0_Bits;

/** \brief Functional watchdog status 1 *R3) */
typedef struct _Ifx_TLF35584_FWDSTAT1_Bits
{
    Ifx_UReg_8Bit FWDECNT:4;          /**< \brief [3:0] Functional watchdog error counter value (rh) */
    Ifx_UReg_8Bit reserved_4:4;       /**< \brief [7:4] \internal Reserved */
} Ifx_TLF35584_FWDSTAT1_Bits;

/** \brief Global testmode *R2) */
typedef struct _Ifx_TLF35584_GTM_Bits
{
    Ifx_UReg_8Bit TM:1;               /**< \brief [0:0] Test mode enable status (rh) */
    Ifx_UReg_8Bit NTM:1;              /**< \brief [1:1] Test mode inverted enable status (rh) */
    Ifx_UReg_8Bit reserved_2:6;       /**< \brief [7:2] \internal Reserved */
} Ifx_TLF35584_GTM_Bits;

/** \brief Interrupt flags  *R2) */
typedef struct _Ifx_TLF35584_IF_Bits
{
    Ifx_UReg_8Bit SYS:1;              /**< \brief [0:0] System interrupt flag (rwh) */
    Ifx_UReg_8Bit WK:1;               /**< \brief [1:1] Wake interrupt flag (rwh) */
    Ifx_UReg_8Bit SPI:1;              /**< \brief [2:2] SPI interrupt flag (rwh) */
    Ifx_UReg_8Bit MON:1;              /**< \brief [3:3] Monitor interrupt flag (rwh) */
    Ifx_UReg_8Bit OTW:1;              /**< \brief [4:4] Over temperature warning interrupt flag (rwh) */
    Ifx_UReg_8Bit OTF:1;              /**< \brief [5:5] Over temperature failure interrupt flag (rwh) */
    Ifx_UReg_8Bit ABIST:1;            /**< \brief [6:6] Requested ABIST operation performed flag (rwh) */
    Ifx_UReg_8Bit INTMISS:1;          /**< \brief [7:7] Interrupt not serviced in time flag (rh) */
} Ifx_TLF35584_IF_Bits;

/** \brief Init error status flags *R2) */
typedef struct _Ifx_TLF35584_INITERR_Bits
{
    Ifx_UReg_8Bit reserved_0:2;       /**< \brief [1:0] \internal Reserved */
    Ifx_UReg_8Bit VMONF:1;            /**< \brief [2:2] Voltage monitor init failure flag (rwh) */
    Ifx_UReg_8Bit WWDF:1;             /**< \brief [3:3] Window watchdog error counter overflow failure flag (rwh) */
    Ifx_UReg_8Bit FWDF:1;             /**< \brief [4:4] Functional watchdog error counter overflow failure\nflag (rwh) */
    Ifx_UReg_8Bit ERRF:1;             /**< \brief [5:5] MCU error monitor failure flag (rwh) */
    Ifx_UReg_8Bit SOFTRES:1;          /**< \brief [6:6] Soft reset flag (rwh) */
    Ifx_UReg_8Bit HARDRES:1;          /**< \brief [7:7] Hard reset flag (rwh) */
} Ifx_TLF35584_INITERR_Bits;

/** \brief Monitor status flags 0 *R1) */
typedef struct _Ifx_TLF35584_MONSF0_Bits
{
    Ifx_UReg_8Bit PREGSG:1;           /**< \brief [0:0] Pre-regulator voltage short to ground status flag (rwh) */
    Ifx_UReg_8Bit UCSG:1;             /**< \brief [1:1] uC LDO short to ground status flag (rwh) */
    Ifx_UReg_8Bit STBYSG:1;           /**< \brief [2:2] Standby LDO short to ground status flag (rwh) */
    Ifx_UReg_8Bit VCORESG:1;          /**< \brief [3:3] Core voltage short to ground status flag (rwh) */
    Ifx_UReg_8Bit COMSG:1;            /**< \brief [4:4] Communication LDO short to ground status flag (rwh) */
    Ifx_UReg_8Bit VREFSG:1;           /**< \brief [5:5] Voltage reference short to ground status flag (rwh) */
    Ifx_UReg_8Bit TRK1SG:1;           /**< \brief [6:6] Tracker1 short to ground status flag (rwh) */
    Ifx_UReg_8Bit TRK2SG:1;           /**< \brief [7:7] Tracker2 short to ground status flag (rwh) */
} Ifx_TLF35584_MONSF0_Bits;

/** \brief Monitor status flags 1 *R1) */
typedef struct _Ifx_TLF35584_MONSF1_Bits
{
    Ifx_UReg_8Bit PREGOV:1;           /**< \brief [0:0] Pre-regulator voltage over voltage status flag (rwh) */
    Ifx_UReg_8Bit UCOV:1;             /**< \brief [1:1] uC LDO over voltage status flag (rwh) */
    Ifx_UReg_8Bit STBYOV:1;           /**< \brief [2:2] Standby LDO over voltage status flag (rwh) */
    Ifx_UReg_8Bit VCOREOV:1;          /**< \brief [3:3] Core voltage over voltage status flag (rwh) */
    Ifx_UReg_8Bit COMOV:1;            /**< \brief [4:4] Communication LDO over voltage status flag (rwh) */
    Ifx_UReg_8Bit VREFOV:1;           /**< \brief [5:5] Voltage reference over voltage status flag (rwh) */
    Ifx_UReg_8Bit TRK1OV:1;           /**< \brief [6:6] Tracker1 over voltage status flag (rwh) */
    Ifx_UReg_8Bit TRK2OV:1;           /**< \brief [7:7] Tracker2 over voltage status flag (rwh) */
} Ifx_TLF35584_MONSF1_Bits;

/** \brief Monitor status flags 2 *R2) */
typedef struct _Ifx_TLF35584_MONSF2_Bits
{
    Ifx_UReg_8Bit PREGUV:1;           /**< \brief [0:0] Pre-regulator voltage under voltage status flag (rwh) */
    Ifx_UReg_8Bit UCUV:1;             /**< \brief [1:1] uC LDO under voltage status flag (rwh) */
    Ifx_UReg_8Bit STBYUV:1;           /**< \brief [2:2] Standby LDO under voltage status flag (rwh) */
    Ifx_UReg_8Bit VCOREUV:1;          /**< \brief [3:3] Core voltage under voltage status flag (rwh) */
    Ifx_UReg_8Bit COMUV:1;            /**< \brief [4:4] Communication LDO under voltage status flag (rwh) */
    Ifx_UReg_8Bit VREFUV:1;           /**< \brief [5:5] Voltage reference under voltage status flag (rwh) */
    Ifx_UReg_8Bit TRK1UV:1;           /**< \brief [6:6] Tracker1 under voltage status flag (rwh) */
    Ifx_UReg_8Bit TRK2UV:1;           /**< \brief [7:7] Tracker2 under voltage status flag (rwh) */
} Ifx_TLF35584_MONSF2_Bits;

/** \brief Monitor status flags 3 *R1) */
typedef struct _Ifx_TLF35584_MONSF3_Bits
{
    Ifx_UReg_8Bit VBATOV:1;           /**< \brief [0:0] Supply voltage VS12 over voltage flag (rwh) */
    Ifx_UReg_8Bit reserved_1:3;       /**< \brief [3:1] \internal Reserved */
    Ifx_UReg_8Bit BG12UV:1;           /**< \brief [4:4] Bandgap comparator under voltage condition flag (rwh) */
    Ifx_UReg_8Bit BG12OV:1;           /**< \brief [5:5] Bandgap comparator over voltage condition flag (rwh) */
    Ifx_UReg_8Bit BIASLOW:1;          /**< \brief [6:6] Bias current too low flag (rwh) */
    Ifx_UReg_8Bit BIASHI:1;           /**< \brief [7:7] Bias current too high flag (rwh) */
} Ifx_TLF35584_MONSF3_Bits;

/** \brief Over temperature failure status flags *R1) */
typedef struct _Ifx_TLF35584_OTFAIL_Bits
{
    Ifx_UReg_8Bit PREG:1;             /**< \brief [0:0] Pre-regulator over temperature flag (rwh) */
    Ifx_UReg_8Bit UC:1;               /**< \brief [1:1] uC LDO over temperature flag (rwh) */
    Ifx_UReg_8Bit reserved_2:2;       /**< \brief [3:2] \internal Reserved */
    Ifx_UReg_8Bit COM:1;              /**< \brief [4:4] Communication LDO over temperature flag (rwh) */
    Ifx_UReg_8Bit reserved_5:2;       /**< \brief [6:5] \internal Reserved */
    Ifx_UReg_8Bit MON:1;              /**< \brief [7:7] Monitoring over temperature flag (rwh) */
} Ifx_TLF35584_OTFAIL_Bits;

/** \brief Over temperature warning status flags  *R2) */
typedef struct _Ifx_TLF35584_OTWRNSF_Bits
{
    Ifx_UReg_8Bit PREG:1;             /**< \brief [0:0] Pre-regulator over temperature warning flag (rwh) */
    Ifx_UReg_8Bit UC:1;               /**< \brief [1:1] uC LDO over temperature warning flag (rwh) */
    Ifx_UReg_8Bit STDBY:1;            /**< \brief [2:2] Standby LDO over load flag (rwh) */
    Ifx_UReg_8Bit reserved_3:1;       /**< \brief [3:3] \internal Reserved */
    Ifx_UReg_8Bit COM:1;              /**< \brief [4:4] Communication LDO over temperature warning flag (rwh) */
    Ifx_UReg_8Bit VREF:1;             /**< \brief [5:5] Voltage reference over load flag (rwh) */
    Ifx_UReg_8Bit reserved_6:2;       /**< \brief [7:6] \internal Reserved */
} Ifx_TLF35584_OTWRNSF_Bits;

/** \brief Protection register  *R2) */
typedef struct _Ifx_TLF35584_PROTCFG_Bits
{
    Ifx_UReg_8Bit KEY:8;              /**< \brief [7:0] Protection key (rw) */
} Ifx_TLF35584_PROTCFG_Bits;

/** \brief Protection status  *R1) */
typedef struct _Ifx_TLF35584_PROTSTAT_Bits
{
    Ifx_UReg_8Bit LOCK:1;             /**< \brief [0:0] Protected register lock status (rh) */
    Ifx_UReg_8Bit reserved_1:3;       /**< \brief [3:1] \internal Reserved */
    Ifx_UReg_8Bit KEY1OK:1;           /**< \brief [4:4] Key1 ok status (rh) */
    Ifx_UReg_8Bit KEY2OK:1;           /**< \brief [5:5] Key2 ok status (rh) */
    Ifx_UReg_8Bit KEY3OK:1;           /**< \brief [6:6] Key3 ok status (rh) */
    Ifx_UReg_8Bit KEY4OK:1;           /**< \brief [7:7] Key4 ok status (rh) */
} Ifx_TLF35584_PROTSTAT_Bits;

/** \brief Functional watchdog configuration status *R3) */
typedef struct _Ifx_TLF35584_RFWDCFG_Bits
{
    Ifx_UReg_8Bit WDHBTP:5;           /**< \brief [4:0] Functional watchdog heartbeat timer period status (rh) */
    Ifx_UReg_8Bit reserved_5:3;       /**< \brief [7:5] \internal Reserved */
} Ifx_TLF35584_RFWDCFG_Bits;

/** \brief System  configuration 0 status *R0) */
typedef struct _Ifx_TLF35584_RSYSPCFG0_Bits
{
    Ifx_UReg_8Bit STBYEN:1;           /**< \brief [0:0] Standby regulator QST enable status (rh) */
    Ifx_UReg_8Bit reserved_1:7;       /**< \brief [7:1] \internal Reserved */
} Ifx_TLF35584_RSYSPCFG0_Bits;

/** \brief System configuration 1 status  *R3) */
typedef struct _Ifx_TLF35584_RSYSPCFG1_Bits
{
    Ifx_UReg_8Bit ERRREC:2;           /**< \brief [1:0] ERR pin monitor recovery time status (rh) */
    Ifx_UReg_8Bit ERRRECEN:1;         /**< \brief [2:2] ERR pin monitor recovery enable status (rh) */
    Ifx_UReg_8Bit ERREN:1;            /**< \brief [3:3] ERR pin monitor enable status (rh) */
    Ifx_UReg_8Bit ERRSLPEN:1;         /**< \brief [4:4] ERR pin monitor functionality enable status while\nthe device is in SLEEP (rh) */
    Ifx_UReg_8Bit SS2DEL:3;           /**< \brief [7:5] Safe state 2 delay status (rh) */
} Ifx_TLF35584_RSYSPCFG1_Bits;

/** \brief Watchdog configuration 0 status *R3) */
typedef struct _Ifx_TLF35584_RWDCFG0_Bits
{
    Ifx_UReg_8Bit WDCYC:1;            /**< \brief [0:0] Watchdog cycle time status (rh) */
    Ifx_UReg_8Bit WWDTSEL:1;          /**< \brief [1:1] Window watchdog trigger selection status (rh) */
    Ifx_UReg_8Bit FWDEN:1;            /**< \brief [2:2] Functional watchdog enable status (rh) */
    Ifx_UReg_8Bit WWDEN:1;            /**< \brief [3:3] Window watchdog enable status (rh) */
    Ifx_UReg_8Bit WWDETHR:4;          /**< \brief [7:4] Window watchdog error threshold status (rh) */
} Ifx_TLF35584_RWDCFG0_Bits;

/** \brief Watchdog configuration 1 status *R3) */
typedef struct _Ifx_TLF35584_RWDCFG1_Bits
{
    Ifx_UReg_8Bit FWDETHR:4;          /**< \brief [3:0] Functional watchdog error threshold status (rh) */
    Ifx_UReg_8Bit WDSLPEN:1;          /**< \brief [4:4] Watchdog functionality enable status while the device\nis in SLEEP (rh) */
    Ifx_UReg_8Bit reserved_5:3;       /**< \brief [7:5] \internal Reserved */
} Ifx_TLF35584_RWDCFG1_Bits;

/** \brief Window watchdog configuration 0 status *R3) */
typedef struct _Ifx_TLF35584_RWWDCFG0_Bits
{
    Ifx_UReg_8Bit CW:5;               /**< \brief [4:0] Window watchdog closed window time status (rh) */
    Ifx_UReg_8Bit reserved_5:3;       /**< \brief [7:5] \internal Reserved */
} Ifx_TLF35584_RWWDCFG0_Bits;

/** \brief Window watchdog configuration 1 status *R3) */
typedef struct _Ifx_TLF35584_RWWDCFG1_Bits
{
    Ifx_UReg_8Bit OW:5;               /**< \brief [4:0] Window watchdog open window time status (rh) */
    Ifx_UReg_8Bit reserved_5:3;       /**< \brief [7:5] \internal Reserved */
} Ifx_TLF35584_RWWDCFG1_Bits;

/** \brief SPI status flags *R2) */
typedef struct _Ifx_TLF35584_SPISF_Bits
{
    Ifx_UReg_8Bit PARE:1;             /**< \brief [0:0] SPI frame parity error flag (rwh) */
    Ifx_UReg_8Bit LENE:1;             /**< \brief [1:1] SPI frame length invalid flag (rwh) */
    Ifx_UReg_8Bit ADDRE:1;            /**< \brief [2:2] SPI address invalid flag (rwh) */
    Ifx_UReg_8Bit DURE:1;             /**< \brief [3:3] SPI frame duration error flag (rwh) */
    Ifx_UReg_8Bit LOCK:1;             /**< \brief [4:4] LOCK or UNLOCK procedure error flag (rwh) */
    Ifx_UReg_8Bit reserved_5:3;       /**< \brief [7:5] \internal Reserved */
} Ifx_TLF35584_SPISF_Bits;

/** \brief Failure status flags *R1) */
typedef struct _Ifx_TLF35584_SYSFAIL_Bits
{
    Ifx_UReg_8Bit VOLTSELERR:1;       /**< \brief [0:0] Double Bit error on voltage selection flag (rwh) */
    Ifx_UReg_8Bit OTF:1;              /**< \brief [1:1] Over temperature failure flag (rwh) */
    Ifx_UReg_8Bit VMONF:1;            /**< \brief [2:2] Voltage monitor failure flag (rwh) */
    Ifx_UReg_8Bit reserved_3:3;       /**< \brief [5:3] \internal Reserved */
    Ifx_UReg_8Bit ABISTERR:1;         /**< \brief [6:6] ABIST operation interrupted flag (rwh) */
    Ifx_UReg_8Bit INITF:1;            /**< \brief [7:7] INIT failure flag (rwh) */
} Ifx_TLF35584_SYSFAIL_Bits;

/** \brief Protected System configuration request 0 *R1) */
typedef struct _Ifx_TLF35584_SYSPCFG0_Bits
{
    Ifx_UReg_8Bit STBYEN:1;           /**< \brief [0:0] Request standby regulator QST enable (rwh) */
    Ifx_UReg_8Bit reserved_1:7;       /**< \brief [7:1] \internal Reserved */
} Ifx_TLF35584_SYSPCFG0_Bits;

/** \brief Protected System configuration request 1 *R2) */
typedef struct _Ifx_TLF35584_SYSPCFG1_Bits
{
    Ifx_UReg_8Bit ERRREC:2;           /**< \brief [1:0] Request ERR pin monitor recovery time (rwh) */
    Ifx_UReg_8Bit ERRRECEN:1;         /**< \brief [2:2] Request ERR pin monitor recovery enable (rwh) */
    Ifx_UReg_8Bit ERREN:1;            /**< \brief [3:3] Request ERR pin monitor enable (rwh) */
    Ifx_UReg_8Bit ERRSLPEN:1;         /**< \brief [4:4] Request ERR pin monitor functionaility enable while\nthe system is in SLEEP (rwh) */
    Ifx_UReg_8Bit SS2DEL:3;           /**< \brief [7:5] Request safe state 2 delay (rwh) */
} Ifx_TLF35584_SYSPCFG1_Bits;

/** \brief System status flags *R2) */
typedef struct _Ifx_TLF35584_SYSSF_Bits
{
    Ifx_UReg_8Bit CFGE:1;             /**< \brief [0:0] Protected configuration double bit error flag (rwh) */
    Ifx_UReg_8Bit WWDE:1;             /**< \brief [1:1] Window watchdog error interrupt flag (rwh) */
    Ifx_UReg_8Bit FWDE:1;             /**< \brief [2:2] Functional watchdog error interrupt flag (rwh) */
    Ifx_UReg_8Bit ERRMISS:1;          /**< \brief [3:3] MCU error miss status flag (rwh) */
    Ifx_UReg_8Bit TRFAIL:1;           /**< \brief [4:4] Transition to low power failed flag (rwh) */
    Ifx_UReg_8Bit NO_OP:1;            /**< \brief [5:5] State transition request failure flag (rwh) */
    Ifx_UReg_8Bit reserved_6:2;       /**< \brief [7:6] \internal Reserved */
} Ifx_TLF35584_SYSSF_Bits;

/** \brief Voltage monitor status  *R2) */
typedef struct _Ifx_TLF35584_VMONSTAT_Bits
{
    Ifx_UReg_8Bit reserved_0:2;       /**< \brief [1:0] \internal Reserved */
    Ifx_UReg_8Bit STBYST:1;           /**< \brief [2:2] Standby LDO voltage ready status (rh) */
    Ifx_UReg_8Bit VCOREST:1;          /**< \brief [3:3] Core voltage ready status (rh) */
    Ifx_UReg_8Bit COMST:1;            /**< \brief [4:4] Communication LDO voltage ready status (rh) */
    Ifx_UReg_8Bit VREFST:1;           /**< \brief [5:5] Voltage reference voltage ready status (rh) */
    Ifx_UReg_8Bit TRK1ST:1;           /**< \brief [6:6] Tracker1 voltage ready status (rh) */
    Ifx_UReg_8Bit TRK2ST:1;           /**< \brief [7:7] Tracker2 voltage ready status (rh) */
} Ifx_TLF35584_VMONSTAT_Bits;

/** \brief Protected  Watchdog configuration request 0 *R2) */
typedef struct _Ifx_TLF35584_WDCFG0_Bits
{
    Ifx_UReg_8Bit WDCYC:1;            /**< \brief [0:0] Request watchdog cycle time (rwh) */
    Ifx_UReg_8Bit WWDTSEL:1;          /**< \brief [1:1] Request window watchdog trigger selection (rwh) */
    Ifx_UReg_8Bit FWDEN:1;            /**< \brief [2:2] Request functional watchdog enable (rwh) */
    Ifx_UReg_8Bit WWDEN:1;            /**< \brief [3:3] Request window watchdog enable (rwh) */
    Ifx_UReg_8Bit WWDETHR:4;          /**< \brief [7:4] Request window watchdog error threshold (rwh) */
} Ifx_TLF35584_WDCFG0_Bits;

/** \brief Protected Watchdog configuration request 1 *R2) */
typedef struct _Ifx_TLF35584_WDCFG1_Bits
{
    Ifx_UReg_8Bit FWDETHR:4;          /**< \brief [3:0] Request functional watchdog error threshold (rwh) */
    Ifx_UReg_8Bit WDSLPEN:1;          /**< \brief [4:4] Request watchdog functionality enable while the\ndevice is in SLEEP (rwh) */
    Ifx_UReg_8Bit reserved_5:3;       /**< \brief [7:5] \internal Reserved */
} Ifx_TLF35584_WDCFG1_Bits;

/** \brief Wakeup status flags *R2) */
typedef struct _Ifx_TLF35584_WKSF_Bits
{
    Ifx_UReg_8Bit WAK:1;              /**< \brief [0:0] WAK signal wakeup flag (rwh) */
    Ifx_UReg_8Bit ENA:1;              /**< \brief [1:1] ENA signal wakeup flag (rwh) */
    Ifx_UReg_8Bit CMON:1;             /**< \brief [2:2] QUC current monitor threshold wakeup flag (rwh) */
    Ifx_UReg_8Bit WKTIM:1;            /**< \brief [3:3] Wake timer wakeup flag (rwh) */
    Ifx_UReg_8Bit WKSPI:1;            /**< \brief [4:4] Wakeup from SLEEP by SPI flag (rwh) */
    Ifx_UReg_8Bit reserved_5:3;       /**< \brief [7:5] \internal Reserved */
} Ifx_TLF35584_WKSF_Bits;

/** \brief Wake timer configuration 0 *R2) */
typedef struct _Ifx_TLF35584_WKTIMCFG0_Bits
{
    Ifx_UReg_8Bit TIMVALL:8;          /**< \brief [7:0] Wake timer value lower bits (rw) */
} Ifx_TLF35584_WKTIMCFG0_Bits;

/** \brief Wake timer configuration 1 *R2) */
typedef struct _Ifx_TLF35584_WKTIMCFG1_Bits
{
    Ifx_UReg_8Bit TIMVALM:8;          /**< \brief [7:0] Wake timer value middle bits (rw) */
} Ifx_TLF35584_WKTIMCFG1_Bits;

/** \brief Wake timer configuration 2 *R2) */
typedef struct _Ifx_TLF35584_WKTIMCFG2_Bits
{
    Ifx_UReg_8Bit TIMVALH:8;          /**< \brief [7:0] Wake timer value higher bits (rw) */
} Ifx_TLF35584_WKTIMCFG2_Bits;

/** \brief Protected Window watchdog configuration request 0 *R2) */
typedef struct _Ifx_TLF35584_WWDCFG0_Bits
{
    Ifx_UReg_8Bit CW:5;               /**< \brief [4:0] Request window watchdog closed window time (rwh) */
    Ifx_UReg_8Bit reserved_5:3;       /**< \brief [7:5] \internal Reserved */
} Ifx_TLF35584_WWDCFG0_Bits;

/** \brief Protected Window watchdog configuration request 1 *R2) */
typedef struct _Ifx_TLF35584_WWDCFG1_Bits
{
    Ifx_UReg_8Bit OW:5;               /**< \brief [4:0] Request window watchdog open window time (rwh) */
    Ifx_UReg_8Bit reserved_5:3;       /**< \brief [7:5] \internal Reserved */
} Ifx_TLF35584_WWDCFG1_Bits;

/** \brief Window watchdog service command  *R2) */
typedef struct _Ifx_TLF35584_WWDSCMD_Bits
{
    Ifx_UReg_8Bit TRIG:1;             /**< \brief [0:0] Window watchdog SPI trigger command (rw) */
    Ifx_UReg_8Bit reserved_1:6;       /**< \brief [6:1] \internal Reserved */
    Ifx_UReg_8Bit TRIG_STATUS:1;      /**< \brief [7:7] Last SPI trigger received (rh) */
} Ifx_TLF35584_WWDSCMD_Bits;

/** \brief Window watchdog status  *R3) */
typedef struct _Ifx_TLF35584_WWDSTAT_Bits
{
    Ifx_UReg_8Bit WWDECNT:4;          /**< \brief [3:0] Window watchdog error counter status (rh) */
    Ifx_UReg_8Bit reserved_4:4;       /**< \brief [7:4] \internal Reserved */
} Ifx_TLF35584_WWDSTAT_Bits;

/** \}  */
/******************************************************************************/
/******************************************************************************/
/** \addtogroup IfxLld_Tlf35584_Registers_union
 * \{   */
/** \brief ABIST control0 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_ABIST_CTRL0_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_ABIST_CTRL0;

/** \brief ABIST control1 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_ABIST_CTRL1_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_ABIST_CTRL1;

/** \brief ABIST select 0 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_ABIST_SELECT0_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_ABIST_SELECT0;

/** \brief ABIST select 1 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_ABIST_SELECT1_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_ABIST_SELECT1;

/** \brief ABIST select 2 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_ABIST_SELECT2_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_ABIST_SELECT2;

/** \brief Buck switching frequency change *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_BCK_FREQ_CHANGE_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_BCK_FREQ_CHANGE;

/** \brief Buck Frequency spread *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_BCK_FRE_SPREAD_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_BCK_FRE_SPREAD;

/** \brief Buck main control *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_BCK_MAIN_CTRL_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_BCK_MAIN_CTRL;

/** \brief Device configuration 0  *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_DEVCFG0_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_DEVCFG0;

/** \brief Device configuration 1 *R0)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_DEVCFG1_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_DEVCFG1;

/** \brief Device configuration 2 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_DEVCFG2_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_DEVCFG2;

/** \brief Device control request *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_DEVCTRL_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_DEVCTRL;

/** \brief Device control inverted request *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_DEVCTRLN_Bits B;     /**< \brief Bitfield access */
} Ifx_TLF35584_DEVCTRLN;

/** \brief Device status  *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_DEVSTAT_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_DEVSTAT;

/** \brief Protected Functional watchdog configuration request *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_FWDCFG_Bits B;       /**< \brief Bitfield access */
} Ifx_TLF35584_FWDCFG;

/** \brief Functional watchdog response command  *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_FWDRSP_Bits B;       /**< \brief Bitfield access */
} Ifx_TLF35584_FWDRSP;

/** \brief Functional watchdog response command with synchronization\n*R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_FWDRSPSYNC_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_FWDRSPSYNC;

/** \brief Functional watchdog status 0 *R3)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_FWDSTAT0_Bits B;     /**< \brief Bitfield access */
} Ifx_TLF35584_FWDSTAT0;

/** \brief Functional watchdog status 1 *R3)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_FWDSTAT1_Bits B;     /**< \brief Bitfield access */
} Ifx_TLF35584_FWDSTAT1;

/** \brief Global testmode *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_GTM_Bits B;          /**< \brief Bitfield access */
} Ifx_TLF35584_GTM;

/** \brief Interrupt flags  *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_IF_Bits B;           /**< \brief Bitfield access */
} Ifx_TLF35584_IF;

/** \brief Init error status flags *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_INITERR_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_INITERR;

/** \brief Monitor status flags 0 *R1)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_MONSF0_Bits B;       /**< \brief Bitfield access */
} Ifx_TLF35584_MONSF0;

/** \brief Monitor status flags 1 *R1)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_MONSF1_Bits B;       /**< \brief Bitfield access */
} Ifx_TLF35584_MONSF1;

/** \brief Monitor status flags 2 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_MONSF2_Bits B;       /**< \brief Bitfield access */
} Ifx_TLF35584_MONSF2;

/** \brief Monitor status flags 3 *R1)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_MONSF3_Bits B;       /**< \brief Bitfield access */
} Ifx_TLF35584_MONSF3;

/** \brief Over temperature failure status flags *R1)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_OTFAIL_Bits B;       /**< \brief Bitfield access */
} Ifx_TLF35584_OTFAIL;

/** \brief Over temperature warning status flags  *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_OTWRNSF_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_OTWRNSF;

/** \brief Protection register  *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_PROTCFG_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_PROTCFG;

/** \brief Protection status  *R1)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_PROTSTAT_Bits B;     /**< \brief Bitfield access */
} Ifx_TLF35584_PROTSTAT;

/** \brief Functional watchdog configuration status *R3)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_RFWDCFG_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_RFWDCFG;

/** \brief System  configuration 0 status *R0)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_RSYSPCFG0_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_RSYSPCFG0;

/** \brief System configuration 1 status  *R3)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_RSYSPCFG1_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_RSYSPCFG1;

/** \brief Watchdog configuration 0 status *R3)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_RWDCFG0_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_RWDCFG0;

/** \brief Watchdog configuration 1 status *R3)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_RWDCFG1_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_RWDCFG1;

/** \brief Window watchdog configuration 0 status *R3)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_RWWDCFG0_Bits B;     /**< \brief Bitfield access */
} Ifx_TLF35584_RWWDCFG0;

/** \brief Window watchdog configuration 1 status *R3)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_RWWDCFG1_Bits B;     /**< \brief Bitfield access */
} Ifx_TLF35584_RWWDCFG1;

/** \brief SPI status flags *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_SPISF_Bits B;        /**< \brief Bitfield access */
} Ifx_TLF35584_SPISF;

/** \brief Failure status flags *R1)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_SYSFAIL_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_SYSFAIL;

/** \brief Protected System configuration request 0 *R1)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_SYSPCFG0_Bits B;     /**< \brief Bitfield access */
} Ifx_TLF35584_SYSPCFG0;

/** \brief Protected System configuration request 1 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_SYSPCFG1_Bits B;     /**< \brief Bitfield access */
} Ifx_TLF35584_SYSPCFG1;

/** \brief System status flags *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_SYSSF_Bits B;        /**< \brief Bitfield access */
} Ifx_TLF35584_SYSSF;

/** \brief Voltage monitor status  *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_VMONSTAT_Bits B;     /**< \brief Bitfield access */
} Ifx_TLF35584_VMONSTAT;

/** \brief Protected  Watchdog configuration request 0 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_WDCFG0_Bits B;       /**< \brief Bitfield access */
} Ifx_TLF35584_WDCFG0;

/** \brief Protected Watchdog configuration request 1 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_WDCFG1_Bits B;       /**< \brief Bitfield access */
} Ifx_TLF35584_WDCFG1;

/** \brief Wakeup status flags *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_WKSF_Bits B;         /**< \brief Bitfield access */
} Ifx_TLF35584_WKSF;

/** \brief Wake timer configuration 0 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_WKTIMCFG0_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_WKTIMCFG0;

/** \brief Wake timer configuration 1 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_WKTIMCFG1_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_WKTIMCFG1;

/** \brief Wake timer configuration 2 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_WKTIMCFG2_Bits B;    /**< \brief Bitfield access */
} Ifx_TLF35584_WKTIMCFG2;

/** \brief Protected Window watchdog configuration request 0 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_WWDCFG0_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_WWDCFG0;

/** \brief Protected Window watchdog configuration request 1 *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_WWDCFG1_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_WWDCFG1;

/** \brief Window watchdog service command  *R2)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_WWDSCMD_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_WWDSCMD;

/** \brief Window watchdog status  *R3)   */
typedef union
{
    Ifx_UReg_8Bit U;                  /**< \brief Unsigned access */
    Ifx_SReg_8Bit I;                  /**< \brief Signed access */
    Ifx_TLF35584_WWDSTAT_Bits B;      /**< \brief Bitfield access */
} Ifx_TLF35584_WWDSTAT;

/** \}  */

/******************************************************************************/
/** \addtogroup IfxLld_Tlf35584_Registers_struct
 * \{  */
/******************************************************************************/
/** \name Object L0
 * \{  */

/** \brief TLF35584 object */
typedef volatile struct _Ifx_TLF35584
{
       Ifx_TLF35584_DEVCFG0                DEVCFG0;                /**< \brief 0, Device configuration 0  *R2)*/
       Ifx_TLF35584_DEVCFG1                DEVCFG1;                /**< \brief 1, Device configuration 1 *R0)*/
       Ifx_TLF35584_DEVCFG2                DEVCFG2;                /**< \brief 2, Device configuration 2 *R2)*/
       Ifx_TLF35584_PROTCFG                PROTCFG;                /**< \brief 3, Protection register  *R2)*/
       Ifx_TLF35584_SYSPCFG0               SYSPCFG0;               /**< \brief 4, Protected System configuration request 0 *R1)*/
       Ifx_TLF35584_SYSPCFG1               SYSPCFG1;               /**< \brief 5, Protected System configuration request 1 *R2)*/
       Ifx_TLF35584_WDCFG0                 WDCFG0;                 /**< \brief 6, Protected  Watchdog configuration request 0 *R2)*/
       Ifx_TLF35584_WDCFG1                 WDCFG1;                 /**< \brief 7, Protected Watchdog configuration request 1 *R2)*/
       Ifx_TLF35584_FWDCFG                 FWDCFG;                 /**< \brief 8, Protected Functional watchdog configuration request *R2)*/
       Ifx_TLF35584_WWDCFG0                WWDCFG0;                /**< \brief 9, Protected Window watchdog configuration request 0 *R2)*/
       Ifx_TLF35584_WWDCFG1                WWDCFG1;                /**< \brief A, Protected Window watchdog configuration request 1 *R2)*/
       Ifx_TLF35584_RSYSPCFG0              RSYSPCFG0;              /**< \brief B, System  configuration 0 status *R0)*/
       Ifx_TLF35584_RSYSPCFG1              RSYSPCFG1;              /**< \brief C, System configuration 1 status  *R3)*/
       Ifx_TLF35584_RWDCFG0                RWDCFG0;                /**< \brief D, Watchdog configuration 0 status *R3)*/
       Ifx_TLF35584_RWDCFG1                RWDCFG1;                /**< \brief E, Watchdog configuration 1 status *R3)*/
       Ifx_TLF35584_RFWDCFG                RFWDCFG;                /**< \brief F, Functional watchdog configuration status *R3)*/
       Ifx_TLF35584_RWWDCFG0               RWWDCFG0;               /**< \brief 10, Window watchdog configuration 0 status *R3)*/
       Ifx_TLF35584_RWWDCFG1               RWWDCFG1;               /**< \brief 11, Window watchdog configuration 1 status *R3)*/
       Ifx_TLF35584_WKTIMCFG0              WKTIMCFG0;              /**< \brief 12, Wake timer configuration 0 *R2)*/
       Ifx_TLF35584_WKTIMCFG1              WKTIMCFG1;              /**< \brief 13, Wake timer configuration 1 *R2)*/
       Ifx_TLF35584_WKTIMCFG2              WKTIMCFG2;              /**< \brief 14, Wake timer configuration 2 *R2)*/
       Ifx_TLF35584_DEVCTRL                DEVCTRL;                /**< \brief 15, Device control request *R2)*/
       Ifx_TLF35584_DEVCTRLN               DEVCTRLN;               /**< \brief 16, Device control inverted request *R2)*/
       Ifx_TLF35584_WWDSCMD                WWDSCMD;                /**< \brief 17, Window watchdog service command  *R2)*/
       Ifx_TLF35584_FWDRSP                 FWDRSP;                 /**< \brief 18, Functional watchdog response command  *R2)*/
       Ifx_TLF35584_FWDRSPSYNC             FWDRSPSYNC;             /**< \brief 19, Functional watchdog response command with synchronization\n*R2)*/
       Ifx_TLF35584_SYSFAIL                SYSFAIL;                /**< \brief 1A, Failure status flags *R1)*/
       Ifx_TLF35584_INITERR                INITERR;                /**< \brief 1B, Init error status flags *R2)*/
       Ifx_TLF35584_IF                     IF;                     /**< \brief 1C, Interrupt flags  *R2)*/
       Ifx_TLF35584_SYSSF                  SYSSF;                  /**< \brief 1D, System status flags *R2)*/
       Ifx_TLF35584_WKSF                   WKSF;                   /**< \brief 1E, Wakeup status flags *R2)*/
       Ifx_TLF35584_SPISF                  SPISF;                  /**< \brief 1F, SPI status flags *R2)*/
       Ifx_TLF35584_MONSF0                 MONSF0;                 /**< \brief 20, Monitor status flags 0 *R1)*/
       Ifx_TLF35584_MONSF1                 MONSF1;                 /**< \brief 21, Monitor status flags 1 *R1)*/
       Ifx_TLF35584_MONSF2                 MONSF2;                 /**< \brief 22, Monitor status flags 2 *R2)*/
       Ifx_TLF35584_MONSF3                 MONSF3;                 /**< \brief 23, Monitor status flags 3 *R1)*/
       Ifx_TLF35584_OTFAIL                 OTFAIL;                 /**< \brief 24, Over temperature failure status flags *R1)*/
       Ifx_TLF35584_OTWRNSF                OTWRNSF;                /**< \brief 25, Over temperature warning status flags  *R2)*/
       Ifx_TLF35584_VMONSTAT               VMONSTAT;               /**< \brief 26, Voltage monitor status  *R2)*/
       Ifx_TLF35584_DEVSTAT                DEVSTAT;                /**< \brief 27, Device status  *R2)*/
       Ifx_TLF35584_PROTSTAT               PROTSTAT;               /**< \brief 28, Protection status  *R1)*/
       Ifx_TLF35584_WWDSTAT                WWDSTAT;                /**< \brief 29, Window watchdog status  *R3)*/
       Ifx_TLF35584_FWDSTAT0               FWDSTAT0;               /**< \brief 2A, Functional watchdog status 0 *R3)*/
       Ifx_TLF35584_FWDSTAT1               FWDSTAT1;               /**< \brief 2B, Functional watchdog status 1 *R3)*/
       Ifx_TLF35584_ABIST_CTRL0            ABIST_CTRL0;            /**< \brief 2C, ABIST control0 *R2)*/
       Ifx_TLF35584_ABIST_CTRL1            ABIST_CTRL1;            /**< \brief 2D, ABIST control1 *R2)*/
       Ifx_TLF35584_ABIST_SELECT0          ABIST_SELECT0;          /**< \brief 2E, ABIST select 0 *R2)*/
       Ifx_TLF35584_ABIST_SELECT1          ABIST_SELECT1;          /**< \brief 2F, ABIST select 1 *R2)*/
       Ifx_TLF35584_ABIST_SELECT2          ABIST_SELECT2;          /**< \brief 30, ABIST select 2 *R2)*/
       Ifx_TLF35584_BCK_FREQ_CHANGE        BCK_FREQ_CHANGE;        /**< \brief 31, Buck switching frequency change *R2)*/
       Ifx_TLF35584_BCK_FRE_SPREAD         BCK_FRE_SPREAD;         /**< \brief 32, Buck Frequency spread *R2)*/
       Ifx_TLF35584_BCK_MAIN_CTRL          BCK_MAIN_CTRL;          /**< \brief 33, Buck main control *R2)*/
       Ifx_UReg_8Bit                       reserved_34[11];        /**< \brief 34, \internal Reserved */
       Ifx_TLF35584_GTM                    GTM;                    /**< \brief 3F, Global testmode *R2)*/
} Ifx_TLF35584;

/** \}  */
/******************************************************************************/
/** \}  */
/******************************************************************************/
/******************************************************************************/
#endif /* IFXTLF35584_REGDEF_H */
