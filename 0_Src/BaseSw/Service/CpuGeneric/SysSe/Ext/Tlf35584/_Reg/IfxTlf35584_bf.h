/**
 * \file IfxTlf35584_bf.h
 * \brief
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved.
 *
 *
 * Date: 2017-05-15 04:24:32 GMT
 * Version: TBD
 * Specification: TBD
 * MAY BE CHANGED BY USER [yes/no]: Yes
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_Tlf35584_Registers_BitfieldsMask Bitfields mask and offset
 * \ingroup IfxLld_Tlf35584_Registers
 * 
 */
#ifndef IFXTLF35584_BF_H
#define IFXTLF35584_BF_H 1
/******************************************************************************/
/******************************************************************************/
/** \addtogroup IfxLld_Tlf35584_Registers_BitfieldsMask
 * \{  */
/** \brief Length for Ifx_TLF35584_DEVCFG0_Bits.TRDEL */
#define IFX_TLF35584_DEVCFG0_TRDEL_LEN (4u)

/** \brief Mask for Ifx_TLF35584_DEVCFG0_Bits.TRDEL */
#define IFX_TLF35584_DEVCFG0_TRDEL_MSK (0xfu)

/** \brief Offset for Ifx_TLF35584_DEVCFG0_Bits.TRDEL */
#define IFX_TLF35584_DEVCFG0_TRDEL_OFF (0u)

/** \brief Length for Ifx_TLF35584_DEVCFG0_Bits.WKTIMCYC */
#define IFX_TLF35584_DEVCFG0_WKTIMCYC_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCFG0_Bits.WKTIMCYC */
#define IFX_TLF35584_DEVCFG0_WKTIMCYC_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCFG0_Bits.WKTIMCYC */
#define IFX_TLF35584_DEVCFG0_WKTIMCYC_OFF (6u)

/** \brief Length for Ifx_TLF35584_DEVCFG0_Bits.WKTIMEN */
#define IFX_TLF35584_DEVCFG0_WKTIMEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCFG0_Bits.WKTIMEN */
#define IFX_TLF35584_DEVCFG0_WKTIMEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCFG0_Bits.WKTIMEN */
#define IFX_TLF35584_DEVCFG0_WKTIMEN_OFF (7u)

/** \brief Length for Ifx_TLF35584_DEVCFG1_Bits.RESDEL */
#define IFX_TLF35584_DEVCFG1_RESDEL_LEN (3u)

/** \brief Mask for Ifx_TLF35584_DEVCFG1_Bits.RESDEL */
#define IFX_TLF35584_DEVCFG1_RESDEL_MSK (0x7u)

/** \brief Offset for Ifx_TLF35584_DEVCFG1_Bits.RESDEL */
#define IFX_TLF35584_DEVCFG1_RESDEL_OFF (0u)

/** \brief Length for Ifx_TLF35584_DEVCFG2_Bits.ESYNEN */
#define IFX_TLF35584_DEVCFG2_ESYNEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCFG2_Bits.ESYNEN */
#define IFX_TLF35584_DEVCFG2_ESYNEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCFG2_Bits.ESYNEN */
#define IFX_TLF35584_DEVCFG2_ESYNEN_OFF (0u)

/** \brief Length for Ifx_TLF35584_DEVCFG2_Bits.ESYNPHA */
#define IFX_TLF35584_DEVCFG2_ESYNPHA_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCFG2_Bits.ESYNPHA */
#define IFX_TLF35584_DEVCFG2_ESYNPHA_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCFG2_Bits.ESYNPHA */
#define IFX_TLF35584_DEVCFG2_ESYNPHA_OFF (1u)

/** \brief Length for Ifx_TLF35584_DEVCFG2_Bits.CTHR */
#define IFX_TLF35584_DEVCFG2_CTHR_LEN (2u)

/** \brief Mask for Ifx_TLF35584_DEVCFG2_Bits.CTHR */
#define IFX_TLF35584_DEVCFG2_CTHR_MSK (0x3u)

/** \brief Offset for Ifx_TLF35584_DEVCFG2_Bits.CTHR */
#define IFX_TLF35584_DEVCFG2_CTHR_OFF (2u)

/** \brief Length for Ifx_TLF35584_DEVCFG2_Bits.CMONEN */
#define IFX_TLF35584_DEVCFG2_CMONEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCFG2_Bits.CMONEN */
#define IFX_TLF35584_DEVCFG2_CMONEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCFG2_Bits.CMONEN */
#define IFX_TLF35584_DEVCFG2_CMONEN_OFF (4u)

/** \brief Length for Ifx_TLF35584_DEVCFG2_Bits.FRE */
#define IFX_TLF35584_DEVCFG2_FRE_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCFG2_Bits.FRE */
#define IFX_TLF35584_DEVCFG2_FRE_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCFG2_Bits.FRE */
#define IFX_TLF35584_DEVCFG2_FRE_OFF (5u)

/** \brief Length for Ifx_TLF35584_DEVCFG2_Bits.STU */
#define IFX_TLF35584_DEVCFG2_STU_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCFG2_Bits.STU */
#define IFX_TLF35584_DEVCFG2_STU_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCFG2_Bits.STU */
#define IFX_TLF35584_DEVCFG2_STU_OFF (6u)

/** \brief Length for Ifx_TLF35584_DEVCFG2_Bits.EVCEN */
#define IFX_TLF35584_DEVCFG2_EVCEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCFG2_Bits.EVCEN */
#define IFX_TLF35584_DEVCFG2_EVCEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCFG2_Bits.EVCEN */
#define IFX_TLF35584_DEVCFG2_EVCEN_OFF (7u)

/** \brief Length for Ifx_TLF35584_PROTCFG_Bits.KEY */
#define IFX_TLF35584_PROTCFG_KEY_LEN (8u)

/** \brief Mask for Ifx_TLF35584_PROTCFG_Bits.KEY */
#define IFX_TLF35584_PROTCFG_KEY_MSK (0xffu)

/** \brief Offset for Ifx_TLF35584_PROTCFG_Bits.KEY */
#define IFX_TLF35584_PROTCFG_KEY_OFF (0u)

/** \brief Length for Ifx_TLF35584_SYSPCFG0_Bits.STBYEN */
#define IFX_TLF35584_SYSPCFG0_STBYEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSPCFG0_Bits.STBYEN */
#define IFX_TLF35584_SYSPCFG0_STBYEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSPCFG0_Bits.STBYEN */
#define IFX_TLF35584_SYSPCFG0_STBYEN_OFF (0u)

/** \brief Length for Ifx_TLF35584_SYSPCFG1_Bits.ERRREC */
#define IFX_TLF35584_SYSPCFG1_ERRREC_LEN (2u)

/** \brief Mask for Ifx_TLF35584_SYSPCFG1_Bits.ERRREC */
#define IFX_TLF35584_SYSPCFG1_ERRREC_MSK (0x3u)

/** \brief Offset for Ifx_TLF35584_SYSPCFG1_Bits.ERRREC */
#define IFX_TLF35584_SYSPCFG1_ERRREC_OFF (0u)

/** \brief Length for Ifx_TLF35584_SYSPCFG1_Bits.ERRRECEN */
#define IFX_TLF35584_SYSPCFG1_ERRRECEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSPCFG1_Bits.ERRRECEN */
#define IFX_TLF35584_SYSPCFG1_ERRRECEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSPCFG1_Bits.ERRRECEN */
#define IFX_TLF35584_SYSPCFG1_ERRRECEN_OFF (2u)

/** \brief Length for Ifx_TLF35584_SYSPCFG1_Bits.ERREN */
#define IFX_TLF35584_SYSPCFG1_ERREN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSPCFG1_Bits.ERREN */
#define IFX_TLF35584_SYSPCFG1_ERREN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSPCFG1_Bits.ERREN */
#define IFX_TLF35584_SYSPCFG1_ERREN_OFF (3u)

/** \brief Length for Ifx_TLF35584_SYSPCFG1_Bits.ERRSLPEN */
#define IFX_TLF35584_SYSPCFG1_ERRSLPEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSPCFG1_Bits.ERRSLPEN */
#define IFX_TLF35584_SYSPCFG1_ERRSLPEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSPCFG1_Bits.ERRSLPEN */
#define IFX_TLF35584_SYSPCFG1_ERRSLPEN_OFF (4u)

/** \brief Length for Ifx_TLF35584_SYSPCFG1_Bits.SS2DEL */
#define IFX_TLF35584_SYSPCFG1_SS2DEL_LEN (3u)

/** \brief Mask for Ifx_TLF35584_SYSPCFG1_Bits.SS2DEL */
#define IFX_TLF35584_SYSPCFG1_SS2DEL_MSK (0x7u)

/** \brief Offset for Ifx_TLF35584_SYSPCFG1_Bits.SS2DEL */
#define IFX_TLF35584_SYSPCFG1_SS2DEL_OFF (5u)

/** \brief Length for Ifx_TLF35584_WDCFG0_Bits.WDCYC */
#define IFX_TLF35584_WDCFG0_WDCYC_LEN (1u)

/** \brief Mask for Ifx_TLF35584_WDCFG0_Bits.WDCYC */
#define IFX_TLF35584_WDCFG0_WDCYC_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_WDCFG0_Bits.WDCYC */
#define IFX_TLF35584_WDCFG0_WDCYC_OFF (0u)

/** \brief Length for Ifx_TLF35584_WDCFG0_Bits.WWDTSEL */
#define IFX_TLF35584_WDCFG0_WWDTSEL_LEN (1u)

/** \brief Mask for Ifx_TLF35584_WDCFG0_Bits.WWDTSEL */
#define IFX_TLF35584_WDCFG0_WWDTSEL_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_WDCFG0_Bits.WWDTSEL */
#define IFX_TLF35584_WDCFG0_WWDTSEL_OFF (1u)

/** \brief Length for Ifx_TLF35584_WDCFG0_Bits.FWDEN */
#define IFX_TLF35584_WDCFG0_FWDEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_WDCFG0_Bits.FWDEN */
#define IFX_TLF35584_WDCFG0_FWDEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_WDCFG0_Bits.FWDEN */
#define IFX_TLF35584_WDCFG0_FWDEN_OFF (2u)

/** \brief Length for Ifx_TLF35584_WDCFG0_Bits.WWDEN */
#define IFX_TLF35584_WDCFG0_WWDEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_WDCFG0_Bits.WWDEN */
#define IFX_TLF35584_WDCFG0_WWDEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_WDCFG0_Bits.WWDEN */
#define IFX_TLF35584_WDCFG0_WWDEN_OFF (3u)

/** \brief Length for Ifx_TLF35584_WDCFG0_Bits.WWDETHR */
#define IFX_TLF35584_WDCFG0_WWDETHR_LEN (4u)

/** \brief Mask for Ifx_TLF35584_WDCFG0_Bits.WWDETHR */
#define IFX_TLF35584_WDCFG0_WWDETHR_MSK (0xfu)

/** \brief Offset for Ifx_TLF35584_WDCFG0_Bits.WWDETHR */
#define IFX_TLF35584_WDCFG0_WWDETHR_OFF (4u)

/** \brief Length for Ifx_TLF35584_WDCFG1_Bits.FWDETHR */
#define IFX_TLF35584_WDCFG1_FWDETHR_LEN (4u)

/** \brief Mask for Ifx_TLF35584_WDCFG1_Bits.FWDETHR */
#define IFX_TLF35584_WDCFG1_FWDETHR_MSK (0xfu)

/** \brief Offset for Ifx_TLF35584_WDCFG1_Bits.FWDETHR */
#define IFX_TLF35584_WDCFG1_FWDETHR_OFF (0u)

/** \brief Length for Ifx_TLF35584_WDCFG1_Bits.WDSLPEN */
#define IFX_TLF35584_WDCFG1_WDSLPEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_WDCFG1_Bits.WDSLPEN */
#define IFX_TLF35584_WDCFG1_WDSLPEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_WDCFG1_Bits.WDSLPEN */
#define IFX_TLF35584_WDCFG1_WDSLPEN_OFF (4u)

/** \brief Length for Ifx_TLF35584_FWDCFG_Bits.WDHBTP */
#define IFX_TLF35584_FWDCFG_WDHBTP_LEN (5u)

/** \brief Mask for Ifx_TLF35584_FWDCFG_Bits.WDHBTP */
#define IFX_TLF35584_FWDCFG_WDHBTP_MSK (0x1fu)

/** \brief Offset for Ifx_TLF35584_FWDCFG_Bits.WDHBTP */
#define IFX_TLF35584_FWDCFG_WDHBTP_OFF (0u)

/** \brief Length for Ifx_TLF35584_WWDCFG0_Bits.CW */
#define IFX_TLF35584_WWDCFG0_CW_LEN (5u)

/** \brief Mask for Ifx_TLF35584_WWDCFG0_Bits.CW */
#define IFX_TLF35584_WWDCFG0_CW_MSK (0x1fu)

/** \brief Offset for Ifx_TLF35584_WWDCFG0_Bits.CW */
#define IFX_TLF35584_WWDCFG0_CW_OFF (0u)

/** \brief Length for Ifx_TLF35584_WWDCFG1_Bits.OW */
#define IFX_TLF35584_WWDCFG1_OW_LEN (5u)

/** \brief Mask for Ifx_TLF35584_WWDCFG1_Bits.OW */
#define IFX_TLF35584_WWDCFG1_OW_MSK (0x1fu)

/** \brief Offset for Ifx_TLF35584_WWDCFG1_Bits.OW */
#define IFX_TLF35584_WWDCFG1_OW_OFF (0u)

/** \brief Length for Ifx_TLF35584_RSYSPCFG0_Bits.STBYEN */
#define IFX_TLF35584_RSYSPCFG0_STBYEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_RSYSPCFG0_Bits.STBYEN */
#define IFX_TLF35584_RSYSPCFG0_STBYEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_RSYSPCFG0_Bits.STBYEN */
#define IFX_TLF35584_RSYSPCFG0_STBYEN_OFF (0u)

/** \brief Length for Ifx_TLF35584_RSYSPCFG1_Bits.ERRREC */
#define IFX_TLF35584_RSYSPCFG1_ERRREC_LEN (2u)

/** \brief Mask for Ifx_TLF35584_RSYSPCFG1_Bits.ERRREC */
#define IFX_TLF35584_RSYSPCFG1_ERRREC_MSK (0x3u)

/** \brief Offset for Ifx_TLF35584_RSYSPCFG1_Bits.ERRREC */
#define IFX_TLF35584_RSYSPCFG1_ERRREC_OFF (0u)

/** \brief Length for Ifx_TLF35584_RSYSPCFG1_Bits.ERRRECEN */
#define IFX_TLF35584_RSYSPCFG1_ERRRECEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_RSYSPCFG1_Bits.ERRRECEN */
#define IFX_TLF35584_RSYSPCFG1_ERRRECEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_RSYSPCFG1_Bits.ERRRECEN */
#define IFX_TLF35584_RSYSPCFG1_ERRRECEN_OFF (2u)

/** \brief Length for Ifx_TLF35584_RSYSPCFG1_Bits.ERREN */
#define IFX_TLF35584_RSYSPCFG1_ERREN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_RSYSPCFG1_Bits.ERREN */
#define IFX_TLF35584_RSYSPCFG1_ERREN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_RSYSPCFG1_Bits.ERREN */
#define IFX_TLF35584_RSYSPCFG1_ERREN_OFF (3u)

/** \brief Length for Ifx_TLF35584_RSYSPCFG1_Bits.ERRSLPEN */
#define IFX_TLF35584_RSYSPCFG1_ERRSLPEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_RSYSPCFG1_Bits.ERRSLPEN */
#define IFX_TLF35584_RSYSPCFG1_ERRSLPEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_RSYSPCFG1_Bits.ERRSLPEN */
#define IFX_TLF35584_RSYSPCFG1_ERRSLPEN_OFF (4u)

/** \brief Length for Ifx_TLF35584_RSYSPCFG1_Bits.SS2DEL */
#define IFX_TLF35584_RSYSPCFG1_SS2DEL_LEN (3u)

/** \brief Mask for Ifx_TLF35584_RSYSPCFG1_Bits.SS2DEL */
#define IFX_TLF35584_RSYSPCFG1_SS2DEL_MSK (0x7u)

/** \brief Offset for Ifx_TLF35584_RSYSPCFG1_Bits.SS2DEL */
#define IFX_TLF35584_RSYSPCFG1_SS2DEL_OFF (5u)

/** \brief Length for Ifx_TLF35584_RWDCFG0_Bits.WDCYC */
#define IFX_TLF35584_RWDCFG0_WDCYC_LEN (1u)

/** \brief Mask for Ifx_TLF35584_RWDCFG0_Bits.WDCYC */
#define IFX_TLF35584_RWDCFG0_WDCYC_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_RWDCFG0_Bits.WDCYC */
#define IFX_TLF35584_RWDCFG0_WDCYC_OFF (0u)

/** \brief Length for Ifx_TLF35584_RWDCFG0_Bits.WWDTSEL */
#define IFX_TLF35584_RWDCFG0_WWDTSEL_LEN (1u)

/** \brief Mask for Ifx_TLF35584_RWDCFG0_Bits.WWDTSEL */
#define IFX_TLF35584_RWDCFG0_WWDTSEL_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_RWDCFG0_Bits.WWDTSEL */
#define IFX_TLF35584_RWDCFG0_WWDTSEL_OFF (1u)

/** \brief Length for Ifx_TLF35584_RWDCFG0_Bits.FWDEN */
#define IFX_TLF35584_RWDCFG0_FWDEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_RWDCFG0_Bits.FWDEN */
#define IFX_TLF35584_RWDCFG0_FWDEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_RWDCFG0_Bits.FWDEN */
#define IFX_TLF35584_RWDCFG0_FWDEN_OFF (2u)

/** \brief Length for Ifx_TLF35584_RWDCFG0_Bits.WWDEN */
#define IFX_TLF35584_RWDCFG0_WWDEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_RWDCFG0_Bits.WWDEN */
#define IFX_TLF35584_RWDCFG0_WWDEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_RWDCFG0_Bits.WWDEN */
#define IFX_TLF35584_RWDCFG0_WWDEN_OFF (3u)

/** \brief Length for Ifx_TLF35584_RWDCFG0_Bits.WWDETHR */
#define IFX_TLF35584_RWDCFG0_WWDETHR_LEN (4u)

/** \brief Mask for Ifx_TLF35584_RWDCFG0_Bits.WWDETHR */
#define IFX_TLF35584_RWDCFG0_WWDETHR_MSK (0xfu)

/** \brief Offset for Ifx_TLF35584_RWDCFG0_Bits.WWDETHR */
#define IFX_TLF35584_RWDCFG0_WWDETHR_OFF (4u)

/** \brief Length for Ifx_TLF35584_RWDCFG1_Bits.FWDETHR */
#define IFX_TLF35584_RWDCFG1_FWDETHR_LEN (4u)

/** \brief Mask for Ifx_TLF35584_RWDCFG1_Bits.FWDETHR */
#define IFX_TLF35584_RWDCFG1_FWDETHR_MSK (0xfu)

/** \brief Offset for Ifx_TLF35584_RWDCFG1_Bits.FWDETHR */
#define IFX_TLF35584_RWDCFG1_FWDETHR_OFF (0u)

/** \brief Length for Ifx_TLF35584_RWDCFG1_Bits.WDSLPEN */
#define IFX_TLF35584_RWDCFG1_WDSLPEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_RWDCFG1_Bits.WDSLPEN */
#define IFX_TLF35584_RWDCFG1_WDSLPEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_RWDCFG1_Bits.WDSLPEN */
#define IFX_TLF35584_RWDCFG1_WDSLPEN_OFF (4u)

/** \brief Length for Ifx_TLF35584_RFWDCFG_Bits.WDHBTP */
#define IFX_TLF35584_RFWDCFG_WDHBTP_LEN (5u)

/** \brief Mask for Ifx_TLF35584_RFWDCFG_Bits.WDHBTP */
#define IFX_TLF35584_RFWDCFG_WDHBTP_MSK (0x1fu)

/** \brief Offset for Ifx_TLF35584_RFWDCFG_Bits.WDHBTP */
#define IFX_TLF35584_RFWDCFG_WDHBTP_OFF (0u)

/** \brief Length for Ifx_TLF35584_RWWDCFG0_Bits.CW */
#define IFX_TLF35584_RWWDCFG0_CW_LEN (5u)

/** \brief Mask for Ifx_TLF35584_RWWDCFG0_Bits.CW */
#define IFX_TLF35584_RWWDCFG0_CW_MSK (0x1fu)

/** \brief Offset for Ifx_TLF35584_RWWDCFG0_Bits.CW */
#define IFX_TLF35584_RWWDCFG0_CW_OFF (0u)

/** \brief Length for Ifx_TLF35584_RWWDCFG1_Bits.OW */
#define IFX_TLF35584_RWWDCFG1_OW_LEN (5u)

/** \brief Mask for Ifx_TLF35584_RWWDCFG1_Bits.OW */
#define IFX_TLF35584_RWWDCFG1_OW_MSK (0x1fu)

/** \brief Offset for Ifx_TLF35584_RWWDCFG1_Bits.OW */
#define IFX_TLF35584_RWWDCFG1_OW_OFF (0u)

/** \brief Length for Ifx_TLF35584_WKTIMCFG0_Bits.TIMVALL */
#define IFX_TLF35584_WKTIMCFG0_TIMVALL_LEN (8u)

/** \brief Mask for Ifx_TLF35584_WKTIMCFG0_Bits.TIMVALL */
#define IFX_TLF35584_WKTIMCFG0_TIMVALL_MSK (0xffu)

/** \brief Offset for Ifx_TLF35584_WKTIMCFG0_Bits.TIMVALL */
#define IFX_TLF35584_WKTIMCFG0_TIMVALL_OFF (0u)

/** \brief Length for Ifx_TLF35584_WKTIMCFG1_Bits.TIMVALM */
#define IFX_TLF35584_WKTIMCFG1_TIMVALM_LEN (8u)

/** \brief Mask for Ifx_TLF35584_WKTIMCFG1_Bits.TIMVALM */
#define IFX_TLF35584_WKTIMCFG1_TIMVALM_MSK (0xffu)

/** \brief Offset for Ifx_TLF35584_WKTIMCFG1_Bits.TIMVALM */
#define IFX_TLF35584_WKTIMCFG1_TIMVALM_OFF (0u)

/** \brief Length for Ifx_TLF35584_WKTIMCFG2_Bits.TIMVALH */
#define IFX_TLF35584_WKTIMCFG2_TIMVALH_LEN (8u)

/** \brief Mask for Ifx_TLF35584_WKTIMCFG2_Bits.TIMVALH */
#define IFX_TLF35584_WKTIMCFG2_TIMVALH_MSK (0xffu)

/** \brief Offset for Ifx_TLF35584_WKTIMCFG2_Bits.TIMVALH */
#define IFX_TLF35584_WKTIMCFG2_TIMVALH_OFF (0u)

/** \brief Length for Ifx_TLF35584_DEVCTRL_Bits.STATEREQ */
#define IFX_TLF35584_DEVCTRL_STATEREQ_LEN (3u)

/** \brief Mask for Ifx_TLF35584_DEVCTRL_Bits.STATEREQ */
#define IFX_TLF35584_DEVCTRL_STATEREQ_MSK (0x7u)

/** \brief Offset for Ifx_TLF35584_DEVCTRL_Bits.STATEREQ */
#define IFX_TLF35584_DEVCTRL_STATEREQ_OFF (0u)

/** \brief Length for Ifx_TLF35584_DEVCTRL_Bits.VREFEN */
#define IFX_TLF35584_DEVCTRL_VREFEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCTRL_Bits.VREFEN */
#define IFX_TLF35584_DEVCTRL_VREFEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCTRL_Bits.VREFEN */
#define IFX_TLF35584_DEVCTRL_VREFEN_OFF (3u)

/** \brief Length for Ifx_TLF35584_DEVCTRL_Bits.COMEN */
#define IFX_TLF35584_DEVCTRL_COMEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCTRL_Bits.COMEN */
#define IFX_TLF35584_DEVCTRL_COMEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCTRL_Bits.COMEN */
#define IFX_TLF35584_DEVCTRL_COMEN_OFF (5u)

/** \brief Length for Ifx_TLF35584_DEVCTRL_Bits.TRK1EN */
#define IFX_TLF35584_DEVCTRL_TRK1EN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCTRL_Bits.TRK1EN */
#define IFX_TLF35584_DEVCTRL_TRK1EN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCTRL_Bits.TRK1EN */
#define IFX_TLF35584_DEVCTRL_TRK1EN_OFF (6u)

/** \brief Length for Ifx_TLF35584_DEVCTRL_Bits.TRK2EN */
#define IFX_TLF35584_DEVCTRL_TRK2EN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCTRL_Bits.TRK2EN */
#define IFX_TLF35584_DEVCTRL_TRK2EN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCTRL_Bits.TRK2EN */
#define IFX_TLF35584_DEVCTRL_TRK2EN_OFF (7u)

/** \brief Length for Ifx_TLF35584_DEVCTRLN_Bits.STATEREQ */
#define IFX_TLF35584_DEVCTRLN_STATEREQ_LEN (3u)

/** \brief Mask for Ifx_TLF35584_DEVCTRLN_Bits.STATEREQ */
#define IFX_TLF35584_DEVCTRLN_STATEREQ_MSK (0x7u)

/** \brief Offset for Ifx_TLF35584_DEVCTRLN_Bits.STATEREQ */
#define IFX_TLF35584_DEVCTRLN_STATEREQ_OFF (0u)

/** \brief Length for Ifx_TLF35584_DEVCTRLN_Bits.VREFEN */
#define IFX_TLF35584_DEVCTRLN_VREFEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCTRLN_Bits.VREFEN */
#define IFX_TLF35584_DEVCTRLN_VREFEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCTRLN_Bits.VREFEN */
#define IFX_TLF35584_DEVCTRLN_VREFEN_OFF (3u)

/** \brief Length for Ifx_TLF35584_DEVCTRLN_Bits.COMEN */
#define IFX_TLF35584_DEVCTRLN_COMEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCTRLN_Bits.COMEN */
#define IFX_TLF35584_DEVCTRLN_COMEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCTRLN_Bits.COMEN */
#define IFX_TLF35584_DEVCTRLN_COMEN_OFF (5u)

/** \brief Length for Ifx_TLF35584_DEVCTRLN_Bits.TRK1EN */
#define IFX_TLF35584_DEVCTRLN_TRK1EN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCTRLN_Bits.TRK1EN */
#define IFX_TLF35584_DEVCTRLN_TRK1EN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCTRLN_Bits.TRK1EN */
#define IFX_TLF35584_DEVCTRLN_TRK1EN_OFF (6u)

/** \brief Length for Ifx_TLF35584_DEVCTRLN_Bits.TRK2EN */
#define IFX_TLF35584_DEVCTRLN_TRK2EN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVCTRLN_Bits.TRK2EN */
#define IFX_TLF35584_DEVCTRLN_TRK2EN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVCTRLN_Bits.TRK2EN */
#define IFX_TLF35584_DEVCTRLN_TRK2EN_OFF (7u)

/** \brief Length for Ifx_TLF35584_WWDSCMD_Bits.TRIG */
#define IFX_TLF35584_WWDSCMD_TRIG_LEN (1u)

/** \brief Mask for Ifx_TLF35584_WWDSCMD_Bits.TRIG */
#define IFX_TLF35584_WWDSCMD_TRIG_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_WWDSCMD_Bits.TRIG */
#define IFX_TLF35584_WWDSCMD_TRIG_OFF (0u)

/** \brief Length for Ifx_TLF35584_WWDSCMD_Bits.TRIG_STATUS */
#define IFX_TLF35584_WWDSCMD_TRIG_STATUS_LEN (1u)

/** \brief Mask for Ifx_TLF35584_WWDSCMD_Bits.TRIG_STATUS */
#define IFX_TLF35584_WWDSCMD_TRIG_STATUS_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_WWDSCMD_Bits.TRIG_STATUS */
#define IFX_TLF35584_WWDSCMD_TRIG_STATUS_OFF (7u)

/** \brief Length for Ifx_TLF35584_FWDRSP_Bits.FWDRSP */
#define IFX_TLF35584_FWDRSP_FWDRSP_LEN (8u)

/** \brief Mask for Ifx_TLF35584_FWDRSP_Bits.FWDRSP */
#define IFX_TLF35584_FWDRSP_FWDRSP_MSK (0xffu)

/** \brief Offset for Ifx_TLF35584_FWDRSP_Bits.FWDRSP */
#define IFX_TLF35584_FWDRSP_FWDRSP_OFF (0u)

/** \brief Length for Ifx_TLF35584_FWDRSPSYNC_Bits.FWDRSPS */
#define IFX_TLF35584_FWDRSPSYNC_FWDRSPS_LEN (8u)

/** \brief Mask for Ifx_TLF35584_FWDRSPSYNC_Bits.FWDRSPS */
#define IFX_TLF35584_FWDRSPSYNC_FWDRSPS_MSK (0xffu)

/** \brief Offset for Ifx_TLF35584_FWDRSPSYNC_Bits.FWDRSPS */
#define IFX_TLF35584_FWDRSPSYNC_FWDRSPS_OFF (0u)

/** \brief Length for Ifx_TLF35584_SYSFAIL_Bits.VOLTSELERR */
#define IFX_TLF35584_SYSFAIL_VOLTSELERR_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSFAIL_Bits.VOLTSELERR */
#define IFX_TLF35584_SYSFAIL_VOLTSELERR_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSFAIL_Bits.VOLTSELERR */
#define IFX_TLF35584_SYSFAIL_VOLTSELERR_OFF (0u)

/** \brief Length for Ifx_TLF35584_SYSFAIL_Bits.OTF */
#define IFX_TLF35584_SYSFAIL_OTF_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSFAIL_Bits.OTF */
#define IFX_TLF35584_SYSFAIL_OTF_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSFAIL_Bits.OTF */
#define IFX_TLF35584_SYSFAIL_OTF_OFF (1u)

/** \brief Length for Ifx_TLF35584_SYSFAIL_Bits.VMONF */
#define IFX_TLF35584_SYSFAIL_VMONF_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSFAIL_Bits.VMONF */
#define IFX_TLF35584_SYSFAIL_VMONF_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSFAIL_Bits.VMONF */
#define IFX_TLF35584_SYSFAIL_VMONF_OFF (2u)

/** \brief Length for Ifx_TLF35584_SYSFAIL_Bits.ABISTERR */
#define IFX_TLF35584_SYSFAIL_ABISTERR_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSFAIL_Bits.ABISTERR */
#define IFX_TLF35584_SYSFAIL_ABISTERR_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSFAIL_Bits.ABISTERR */
#define IFX_TLF35584_SYSFAIL_ABISTERR_OFF (6u)

/** \brief Length for Ifx_TLF35584_SYSFAIL_Bits.INITF */
#define IFX_TLF35584_SYSFAIL_INITF_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSFAIL_Bits.INITF */
#define IFX_TLF35584_SYSFAIL_INITF_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSFAIL_Bits.INITF */
#define IFX_TLF35584_SYSFAIL_INITF_OFF (7u)

/** \brief Length for Ifx_TLF35584_INITERR_Bits.VMONF */
#define IFX_TLF35584_INITERR_VMONF_LEN (1u)

/** \brief Mask for Ifx_TLF35584_INITERR_Bits.VMONF */
#define IFX_TLF35584_INITERR_VMONF_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_INITERR_Bits.VMONF */
#define IFX_TLF35584_INITERR_VMONF_OFF (2u)

/** \brief Length for Ifx_TLF35584_INITERR_Bits.WWDF */
#define IFX_TLF35584_INITERR_WWDF_LEN (1u)

/** \brief Mask for Ifx_TLF35584_INITERR_Bits.WWDF */
#define IFX_TLF35584_INITERR_WWDF_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_INITERR_Bits.WWDF */
#define IFX_TLF35584_INITERR_WWDF_OFF (3u)

/** \brief Length for Ifx_TLF35584_INITERR_Bits.FWDF */
#define IFX_TLF35584_INITERR_FWDF_LEN (1u)

/** \brief Mask for Ifx_TLF35584_INITERR_Bits.FWDF */
#define IFX_TLF35584_INITERR_FWDF_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_INITERR_Bits.FWDF */
#define IFX_TLF35584_INITERR_FWDF_OFF (4u)

/** \brief Length for Ifx_TLF35584_INITERR_Bits.ERRF */
#define IFX_TLF35584_INITERR_ERRF_LEN (1u)

/** \brief Mask for Ifx_TLF35584_INITERR_Bits.ERRF */
#define IFX_TLF35584_INITERR_ERRF_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_INITERR_Bits.ERRF */
#define IFX_TLF35584_INITERR_ERRF_OFF (5u)

/** \brief Length for Ifx_TLF35584_INITERR_Bits.SOFTRES */
#define IFX_TLF35584_INITERR_SOFTRES_LEN (1u)

/** \brief Mask for Ifx_TLF35584_INITERR_Bits.SOFTRES */
#define IFX_TLF35584_INITERR_SOFTRES_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_INITERR_Bits.SOFTRES */
#define IFX_TLF35584_INITERR_SOFTRES_OFF (6u)

/** \brief Length for Ifx_TLF35584_INITERR_Bits.HARDRES */
#define IFX_TLF35584_INITERR_HARDRES_LEN (1u)

/** \brief Mask for Ifx_TLF35584_INITERR_Bits.HARDRES */
#define IFX_TLF35584_INITERR_HARDRES_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_INITERR_Bits.HARDRES */
#define IFX_TLF35584_INITERR_HARDRES_OFF (7u)

/** \brief Length for Ifx_TLF35584_IF_Bits.SYS */
#define IFX_TLF35584_IF_SYS_LEN (1u)

/** \brief Mask for Ifx_TLF35584_IF_Bits.SYS */
#define IFX_TLF35584_IF_SYS_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_IF_Bits.SYS */
#define IFX_TLF35584_IF_SYS_OFF (0u)

/** \brief Length for Ifx_TLF35584_IF_Bits.WK */
#define IFX_TLF35584_IF_WK_LEN (1u)

/** \brief Mask for Ifx_TLF35584_IF_Bits.WK */
#define IFX_TLF35584_IF_WK_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_IF_Bits.WK */
#define IFX_TLF35584_IF_WK_OFF (1u)

/** \brief Length for Ifx_TLF35584_IF_Bits.SPI */
#define IFX_TLF35584_IF_SPI_LEN (1u)

/** \brief Mask for Ifx_TLF35584_IF_Bits.SPI */
#define IFX_TLF35584_IF_SPI_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_IF_Bits.SPI */
#define IFX_TLF35584_IF_SPI_OFF (2u)

/** \brief Length for Ifx_TLF35584_IF_Bits.MON */
#define IFX_TLF35584_IF_MON_LEN (1u)

/** \brief Mask for Ifx_TLF35584_IF_Bits.MON */
#define IFX_TLF35584_IF_MON_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_IF_Bits.MON */
#define IFX_TLF35584_IF_MON_OFF (3u)

/** \brief Length for Ifx_TLF35584_IF_Bits.OTW */
#define IFX_TLF35584_IF_OTW_LEN (1u)

/** \brief Mask for Ifx_TLF35584_IF_Bits.OTW */
#define IFX_TLF35584_IF_OTW_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_IF_Bits.OTW */
#define IFX_TLF35584_IF_OTW_OFF (4u)

/** \brief Length for Ifx_TLF35584_IF_Bits.OTF */
#define IFX_TLF35584_IF_OTF_LEN (1u)

/** \brief Mask for Ifx_TLF35584_IF_Bits.OTF */
#define IFX_TLF35584_IF_OTF_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_IF_Bits.OTF */
#define IFX_TLF35584_IF_OTF_OFF (5u)

/** \brief Length for Ifx_TLF35584_IF_Bits.ABIST */
#define IFX_TLF35584_IF_ABIST_LEN (1u)

/** \brief Mask for Ifx_TLF35584_IF_Bits.ABIST */
#define IFX_TLF35584_IF_ABIST_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_IF_Bits.ABIST */
#define IFX_TLF35584_IF_ABIST_OFF (6u)

/** \brief Length for Ifx_TLF35584_IF_Bits.INTMISS */
#define IFX_TLF35584_IF_INTMISS_LEN (1u)

/** \brief Mask for Ifx_TLF35584_IF_Bits.INTMISS */
#define IFX_TLF35584_IF_INTMISS_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_IF_Bits.INTMISS */
#define IFX_TLF35584_IF_INTMISS_OFF (7u)

/** \brief Length for Ifx_TLF35584_SYSSF_Bits.CFGE */
#define IFX_TLF35584_SYSSF_CFGE_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSSF_Bits.CFGE */
#define IFX_TLF35584_SYSSF_CFGE_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSSF_Bits.CFGE */
#define IFX_TLF35584_SYSSF_CFGE_OFF (0u)

/** \brief Length for Ifx_TLF35584_SYSSF_Bits.WWDE */
#define IFX_TLF35584_SYSSF_WWDE_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSSF_Bits.WWDE */
#define IFX_TLF35584_SYSSF_WWDE_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSSF_Bits.WWDE */
#define IFX_TLF35584_SYSSF_WWDE_OFF (1u)

/** \brief Length for Ifx_TLF35584_SYSSF_Bits.FWDE */
#define IFX_TLF35584_SYSSF_FWDE_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSSF_Bits.FWDE */
#define IFX_TLF35584_SYSSF_FWDE_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSSF_Bits.FWDE */
#define IFX_TLF35584_SYSSF_FWDE_OFF (2u)

/** \brief Length for Ifx_TLF35584_SYSSF_Bits.ERRMISS */
#define IFX_TLF35584_SYSSF_ERRMISS_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSSF_Bits.ERRMISS */
#define IFX_TLF35584_SYSSF_ERRMISS_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSSF_Bits.ERRMISS */
#define IFX_TLF35584_SYSSF_ERRMISS_OFF (3u)

/** \brief Length for Ifx_TLF35584_SYSSF_Bits.TRFAIL */
#define IFX_TLF35584_SYSSF_TRFAIL_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSSF_Bits.TRFAIL */
#define IFX_TLF35584_SYSSF_TRFAIL_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSSF_Bits.TRFAIL */
#define IFX_TLF35584_SYSSF_TRFAIL_OFF (4u)

/** \brief Length for Ifx_TLF35584_SYSSF_Bits.NO_OP */
#define IFX_TLF35584_SYSSF_NO_OP_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SYSSF_Bits.NO_OP */
#define IFX_TLF35584_SYSSF_NO_OP_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SYSSF_Bits.NO_OP */
#define IFX_TLF35584_SYSSF_NO_OP_OFF (5u)

/** \brief Length for Ifx_TLF35584_WKSF_Bits.WAK */
#define IFX_TLF35584_WKSF_WAK_LEN (1u)

/** \brief Mask for Ifx_TLF35584_WKSF_Bits.WAK */
#define IFX_TLF35584_WKSF_WAK_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_WKSF_Bits.WAK */
#define IFX_TLF35584_WKSF_WAK_OFF (0u)

/** \brief Length for Ifx_TLF35584_WKSF_Bits.ENA */
#define IFX_TLF35584_WKSF_ENA_LEN (1u)

/** \brief Mask for Ifx_TLF35584_WKSF_Bits.ENA */
#define IFX_TLF35584_WKSF_ENA_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_WKSF_Bits.ENA */
#define IFX_TLF35584_WKSF_ENA_OFF (1u)

/** \brief Length for Ifx_TLF35584_WKSF_Bits.CMON */
#define IFX_TLF35584_WKSF_CMON_LEN (1u)

/** \brief Mask for Ifx_TLF35584_WKSF_Bits.CMON */
#define IFX_TLF35584_WKSF_CMON_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_WKSF_Bits.CMON */
#define IFX_TLF35584_WKSF_CMON_OFF (2u)

/** \brief Length for Ifx_TLF35584_WKSF_Bits.WKTIM */
#define IFX_TLF35584_WKSF_WKTIM_LEN (1u)

/** \brief Mask for Ifx_TLF35584_WKSF_Bits.WKTIM */
#define IFX_TLF35584_WKSF_WKTIM_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_WKSF_Bits.WKTIM */
#define IFX_TLF35584_WKSF_WKTIM_OFF (3u)

/** \brief Length for Ifx_TLF35584_WKSF_Bits.WKSPI */
#define IFX_TLF35584_WKSF_WKSPI_LEN (1u)

/** \brief Mask for Ifx_TLF35584_WKSF_Bits.WKSPI */
#define IFX_TLF35584_WKSF_WKSPI_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_WKSF_Bits.WKSPI */
#define IFX_TLF35584_WKSF_WKSPI_OFF (4u)

/** \brief Length for Ifx_TLF35584_SPISF_Bits.PARE */
#define IFX_TLF35584_SPISF_PARE_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SPISF_Bits.PARE */
#define IFX_TLF35584_SPISF_PARE_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SPISF_Bits.PARE */
#define IFX_TLF35584_SPISF_PARE_OFF (0u)

/** \brief Length for Ifx_TLF35584_SPISF_Bits.LENE */
#define IFX_TLF35584_SPISF_LENE_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SPISF_Bits.LENE */
#define IFX_TLF35584_SPISF_LENE_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SPISF_Bits.LENE */
#define IFX_TLF35584_SPISF_LENE_OFF (1u)

/** \brief Length for Ifx_TLF35584_SPISF_Bits.ADDRE */
#define IFX_TLF35584_SPISF_ADDRE_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SPISF_Bits.ADDRE */
#define IFX_TLF35584_SPISF_ADDRE_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SPISF_Bits.ADDRE */
#define IFX_TLF35584_SPISF_ADDRE_OFF (2u)

/** \brief Length for Ifx_TLF35584_SPISF_Bits.DURE */
#define IFX_TLF35584_SPISF_DURE_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SPISF_Bits.DURE */
#define IFX_TLF35584_SPISF_DURE_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SPISF_Bits.DURE */
#define IFX_TLF35584_SPISF_DURE_OFF (3u)

/** \brief Length for Ifx_TLF35584_SPISF_Bits.LOCK */
#define IFX_TLF35584_SPISF_LOCK_LEN (1u)

/** \brief Mask for Ifx_TLF35584_SPISF_Bits.LOCK */
#define IFX_TLF35584_SPISF_LOCK_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_SPISF_Bits.LOCK */
#define IFX_TLF35584_SPISF_LOCK_OFF (4u)

/** \brief Length for Ifx_TLF35584_MONSF0_Bits.PREGSG */
#define IFX_TLF35584_MONSF0_PREGSG_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF0_Bits.PREGSG */
#define IFX_TLF35584_MONSF0_PREGSG_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF0_Bits.PREGSG */
#define IFX_TLF35584_MONSF0_PREGSG_OFF (0u)

/** \brief Length for Ifx_TLF35584_MONSF0_Bits.UCSG */
#define IFX_TLF35584_MONSF0_UCSG_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF0_Bits.UCSG */
#define IFX_TLF35584_MONSF0_UCSG_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF0_Bits.UCSG */
#define IFX_TLF35584_MONSF0_UCSG_OFF (1u)

/** \brief Length for Ifx_TLF35584_MONSF0_Bits.STBYSG */
#define IFX_TLF35584_MONSF0_STBYSG_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF0_Bits.STBYSG */
#define IFX_TLF35584_MONSF0_STBYSG_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF0_Bits.STBYSG */
#define IFX_TLF35584_MONSF0_STBYSG_OFF (2u)

/** \brief Length for Ifx_TLF35584_MONSF0_Bits.VCORESG */
#define IFX_TLF35584_MONSF0_VCORESG_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF0_Bits.VCORESG */
#define IFX_TLF35584_MONSF0_VCORESG_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF0_Bits.VCORESG */
#define IFX_TLF35584_MONSF0_VCORESG_OFF (3u)

/** \brief Length for Ifx_TLF35584_MONSF0_Bits.COMSG */
#define IFX_TLF35584_MONSF0_COMSG_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF0_Bits.COMSG */
#define IFX_TLF35584_MONSF0_COMSG_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF0_Bits.COMSG */
#define IFX_TLF35584_MONSF0_COMSG_OFF (4u)

/** \brief Length for Ifx_TLF35584_MONSF0_Bits.VREFSG */
#define IFX_TLF35584_MONSF0_VREFSG_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF0_Bits.VREFSG */
#define IFX_TLF35584_MONSF0_VREFSG_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF0_Bits.VREFSG */
#define IFX_TLF35584_MONSF0_VREFSG_OFF (5u)

/** \brief Length for Ifx_TLF35584_MONSF0_Bits.TRK1SG */
#define IFX_TLF35584_MONSF0_TRK1SG_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF0_Bits.TRK1SG */
#define IFX_TLF35584_MONSF0_TRK1SG_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF0_Bits.TRK1SG */
#define IFX_TLF35584_MONSF0_TRK1SG_OFF (6u)

/** \brief Length for Ifx_TLF35584_MONSF0_Bits.TRK2SG */
#define IFX_TLF35584_MONSF0_TRK2SG_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF0_Bits.TRK2SG */
#define IFX_TLF35584_MONSF0_TRK2SG_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF0_Bits.TRK2SG */
#define IFX_TLF35584_MONSF0_TRK2SG_OFF (7u)

/** \brief Length for Ifx_TLF35584_MONSF1_Bits.PREGOV */
#define IFX_TLF35584_MONSF1_PREGOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF1_Bits.PREGOV */
#define IFX_TLF35584_MONSF1_PREGOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF1_Bits.PREGOV */
#define IFX_TLF35584_MONSF1_PREGOV_OFF (0u)

/** \brief Length for Ifx_TLF35584_MONSF1_Bits.UCOV */
#define IFX_TLF35584_MONSF1_UCOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF1_Bits.UCOV */
#define IFX_TLF35584_MONSF1_UCOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF1_Bits.UCOV */
#define IFX_TLF35584_MONSF1_UCOV_OFF (1u)

/** \brief Length for Ifx_TLF35584_MONSF1_Bits.STBYOV */
#define IFX_TLF35584_MONSF1_STBYOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF1_Bits.STBYOV */
#define IFX_TLF35584_MONSF1_STBYOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF1_Bits.STBYOV */
#define IFX_TLF35584_MONSF1_STBYOV_OFF (2u)

/** \brief Length for Ifx_TLF35584_MONSF1_Bits.VCOREOV */
#define IFX_TLF35584_MONSF1_VCOREOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF1_Bits.VCOREOV */
#define IFX_TLF35584_MONSF1_VCOREOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF1_Bits.VCOREOV */
#define IFX_TLF35584_MONSF1_VCOREOV_OFF (3u)

/** \brief Length for Ifx_TLF35584_MONSF1_Bits.COMOV */
#define IFX_TLF35584_MONSF1_COMOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF1_Bits.COMOV */
#define IFX_TLF35584_MONSF1_COMOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF1_Bits.COMOV */
#define IFX_TLF35584_MONSF1_COMOV_OFF (4u)

/** \brief Length for Ifx_TLF35584_MONSF1_Bits.VREFOV */
#define IFX_TLF35584_MONSF1_VREFOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF1_Bits.VREFOV */
#define IFX_TLF35584_MONSF1_VREFOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF1_Bits.VREFOV */
#define IFX_TLF35584_MONSF1_VREFOV_OFF (5u)

/** \brief Length for Ifx_TLF35584_MONSF1_Bits.TRK1OV */
#define IFX_TLF35584_MONSF1_TRK1OV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF1_Bits.TRK1OV */
#define IFX_TLF35584_MONSF1_TRK1OV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF1_Bits.TRK1OV */
#define IFX_TLF35584_MONSF1_TRK1OV_OFF (6u)

/** \brief Length for Ifx_TLF35584_MONSF1_Bits.TRK2OV */
#define IFX_TLF35584_MONSF1_TRK2OV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF1_Bits.TRK2OV */
#define IFX_TLF35584_MONSF1_TRK2OV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF1_Bits.TRK2OV */
#define IFX_TLF35584_MONSF1_TRK2OV_OFF (7u)

/** \brief Length for Ifx_TLF35584_MONSF2_Bits.PREGUV */
#define IFX_TLF35584_MONSF2_PREGUV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF2_Bits.PREGUV */
#define IFX_TLF35584_MONSF2_PREGUV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF2_Bits.PREGUV */
#define IFX_TLF35584_MONSF2_PREGUV_OFF (0u)

/** \brief Length for Ifx_TLF35584_MONSF2_Bits.UCUV */
#define IFX_TLF35584_MONSF2_UCUV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF2_Bits.UCUV */
#define IFX_TLF35584_MONSF2_UCUV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF2_Bits.UCUV */
#define IFX_TLF35584_MONSF2_UCUV_OFF (1u)

/** \brief Length for Ifx_TLF35584_MONSF2_Bits.STBYUV */
#define IFX_TLF35584_MONSF2_STBYUV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF2_Bits.STBYUV */
#define IFX_TLF35584_MONSF2_STBYUV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF2_Bits.STBYUV */
#define IFX_TLF35584_MONSF2_STBYUV_OFF (2u)

/** \brief Length for Ifx_TLF35584_MONSF2_Bits.VCOREUV */
#define IFX_TLF35584_MONSF2_VCOREUV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF2_Bits.VCOREUV */
#define IFX_TLF35584_MONSF2_VCOREUV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF2_Bits.VCOREUV */
#define IFX_TLF35584_MONSF2_VCOREUV_OFF (3u)

/** \brief Length for Ifx_TLF35584_MONSF2_Bits.COMUV */
#define IFX_TLF35584_MONSF2_COMUV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF2_Bits.COMUV */
#define IFX_TLF35584_MONSF2_COMUV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF2_Bits.COMUV */
#define IFX_TLF35584_MONSF2_COMUV_OFF (4u)

/** \brief Length for Ifx_TLF35584_MONSF2_Bits.VREFUV */
#define IFX_TLF35584_MONSF2_VREFUV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF2_Bits.VREFUV */
#define IFX_TLF35584_MONSF2_VREFUV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF2_Bits.VREFUV */
#define IFX_TLF35584_MONSF2_VREFUV_OFF (5u)

/** \brief Length for Ifx_TLF35584_MONSF2_Bits.TRK1UV */
#define IFX_TLF35584_MONSF2_TRK1UV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF2_Bits.TRK1UV */
#define IFX_TLF35584_MONSF2_TRK1UV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF2_Bits.TRK1UV */
#define IFX_TLF35584_MONSF2_TRK1UV_OFF (6u)

/** \brief Length for Ifx_TLF35584_MONSF2_Bits.TRK2UV */
#define IFX_TLF35584_MONSF2_TRK2UV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF2_Bits.TRK2UV */
#define IFX_TLF35584_MONSF2_TRK2UV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF2_Bits.TRK2UV */
#define IFX_TLF35584_MONSF2_TRK2UV_OFF (7u)

/** \brief Length for Ifx_TLF35584_MONSF3_Bits.VBATOV */
#define IFX_TLF35584_MONSF3_VBATOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF3_Bits.VBATOV */
#define IFX_TLF35584_MONSF3_VBATOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF3_Bits.VBATOV */
#define IFX_TLF35584_MONSF3_VBATOV_OFF (0u)

/** \brief Length for Ifx_TLF35584_MONSF3_Bits.BG12UV */
#define IFX_TLF35584_MONSF3_BG12UV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF3_Bits.BG12UV */
#define IFX_TLF35584_MONSF3_BG12UV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF3_Bits.BG12UV */
#define IFX_TLF35584_MONSF3_BG12UV_OFF (4u)

/** \brief Length for Ifx_TLF35584_MONSF3_Bits.BG12OV */
#define IFX_TLF35584_MONSF3_BG12OV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF3_Bits.BG12OV */
#define IFX_TLF35584_MONSF3_BG12OV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF3_Bits.BG12OV */
#define IFX_TLF35584_MONSF3_BG12OV_OFF (5u)

/** \brief Length for Ifx_TLF35584_MONSF3_Bits.BIASLOW */
#define IFX_TLF35584_MONSF3_BIASLOW_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF3_Bits.BIASLOW */
#define IFX_TLF35584_MONSF3_BIASLOW_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF3_Bits.BIASLOW */
#define IFX_TLF35584_MONSF3_BIASLOW_OFF (6u)

/** \brief Length for Ifx_TLF35584_MONSF3_Bits.BIASHI */
#define IFX_TLF35584_MONSF3_BIASHI_LEN (1u)

/** \brief Mask for Ifx_TLF35584_MONSF3_Bits.BIASHI */
#define IFX_TLF35584_MONSF3_BIASHI_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_MONSF3_Bits.BIASHI */
#define IFX_TLF35584_MONSF3_BIASHI_OFF (7u)

/** \brief Length for Ifx_TLF35584_OTFAIL_Bits.PREG */
#define IFX_TLF35584_OTFAIL_PREG_LEN (1u)

/** \brief Mask for Ifx_TLF35584_OTFAIL_Bits.PREG */
#define IFX_TLF35584_OTFAIL_PREG_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_OTFAIL_Bits.PREG */
#define IFX_TLF35584_OTFAIL_PREG_OFF (0u)

/** \brief Length for Ifx_TLF35584_OTFAIL_Bits.UC */
#define IFX_TLF35584_OTFAIL_UC_LEN (1u)

/** \brief Mask for Ifx_TLF35584_OTFAIL_Bits.UC */
#define IFX_TLF35584_OTFAIL_UC_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_OTFAIL_Bits.UC */
#define IFX_TLF35584_OTFAIL_UC_OFF (1u)

/** \brief Length for Ifx_TLF35584_OTFAIL_Bits.COM */
#define IFX_TLF35584_OTFAIL_COM_LEN (1u)

/** \brief Mask for Ifx_TLF35584_OTFAIL_Bits.COM */
#define IFX_TLF35584_OTFAIL_COM_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_OTFAIL_Bits.COM */
#define IFX_TLF35584_OTFAIL_COM_OFF (4u)

/** \brief Length for Ifx_TLF35584_OTFAIL_Bits.MON */
#define IFX_TLF35584_OTFAIL_MON_LEN (1u)

/** \brief Mask for Ifx_TLF35584_OTFAIL_Bits.MON */
#define IFX_TLF35584_OTFAIL_MON_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_OTFAIL_Bits.MON */
#define IFX_TLF35584_OTFAIL_MON_OFF (7u)

/** \brief Length for Ifx_TLF35584_OTWRNSF_Bits.PREG */
#define IFX_TLF35584_OTWRNSF_PREG_LEN (1u)

/** \brief Mask for Ifx_TLF35584_OTWRNSF_Bits.PREG */
#define IFX_TLF35584_OTWRNSF_PREG_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_OTWRNSF_Bits.PREG */
#define IFX_TLF35584_OTWRNSF_PREG_OFF (0u)

/** \brief Length for Ifx_TLF35584_OTWRNSF_Bits.UC */
#define IFX_TLF35584_OTWRNSF_UC_LEN (1u)

/** \brief Mask for Ifx_TLF35584_OTWRNSF_Bits.UC */
#define IFX_TLF35584_OTWRNSF_UC_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_OTWRNSF_Bits.UC */
#define IFX_TLF35584_OTWRNSF_UC_OFF (1u)

/** \brief Length for Ifx_TLF35584_OTWRNSF_Bits.STDBY */
#define IFX_TLF35584_OTWRNSF_STDBY_LEN (1u)

/** \brief Mask for Ifx_TLF35584_OTWRNSF_Bits.STDBY */
#define IFX_TLF35584_OTWRNSF_STDBY_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_OTWRNSF_Bits.STDBY */
#define IFX_TLF35584_OTWRNSF_STDBY_OFF (2u)

/** \brief Length for Ifx_TLF35584_OTWRNSF_Bits.COM */
#define IFX_TLF35584_OTWRNSF_COM_LEN (1u)

/** \brief Mask for Ifx_TLF35584_OTWRNSF_Bits.COM */
#define IFX_TLF35584_OTWRNSF_COM_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_OTWRNSF_Bits.COM */
#define IFX_TLF35584_OTWRNSF_COM_OFF (4u)

/** \brief Length for Ifx_TLF35584_OTWRNSF_Bits.VREF */
#define IFX_TLF35584_OTWRNSF_VREF_LEN (1u)

/** \brief Mask for Ifx_TLF35584_OTWRNSF_Bits.VREF */
#define IFX_TLF35584_OTWRNSF_VREF_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_OTWRNSF_Bits.VREF */
#define IFX_TLF35584_OTWRNSF_VREF_OFF (5u)

/** \brief Length for Ifx_TLF35584_VMONSTAT_Bits.STBYST */
#define IFX_TLF35584_VMONSTAT_STBYST_LEN (1u)

/** \brief Mask for Ifx_TLF35584_VMONSTAT_Bits.STBYST */
#define IFX_TLF35584_VMONSTAT_STBYST_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_VMONSTAT_Bits.STBYST */
#define IFX_TLF35584_VMONSTAT_STBYST_OFF (2u)

/** \brief Length for Ifx_TLF35584_VMONSTAT_Bits.VCOREST */
#define IFX_TLF35584_VMONSTAT_VCOREST_LEN (1u)

/** \brief Mask for Ifx_TLF35584_VMONSTAT_Bits.VCOREST */
#define IFX_TLF35584_VMONSTAT_VCOREST_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_VMONSTAT_Bits.VCOREST */
#define IFX_TLF35584_VMONSTAT_VCOREST_OFF (3u)

/** \brief Length for Ifx_TLF35584_VMONSTAT_Bits.COMST */
#define IFX_TLF35584_VMONSTAT_COMST_LEN (1u)

/** \brief Mask for Ifx_TLF35584_VMONSTAT_Bits.COMST */
#define IFX_TLF35584_VMONSTAT_COMST_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_VMONSTAT_Bits.COMST */
#define IFX_TLF35584_VMONSTAT_COMST_OFF (4u)

/** \brief Length for Ifx_TLF35584_VMONSTAT_Bits.VREFST */
#define IFX_TLF35584_VMONSTAT_VREFST_LEN (1u)

/** \brief Mask for Ifx_TLF35584_VMONSTAT_Bits.VREFST */
#define IFX_TLF35584_VMONSTAT_VREFST_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_VMONSTAT_Bits.VREFST */
#define IFX_TLF35584_VMONSTAT_VREFST_OFF (5u)

/** \brief Length for Ifx_TLF35584_VMONSTAT_Bits.TRK1ST */
#define IFX_TLF35584_VMONSTAT_TRK1ST_LEN (1u)

/** \brief Mask for Ifx_TLF35584_VMONSTAT_Bits.TRK1ST */
#define IFX_TLF35584_VMONSTAT_TRK1ST_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_VMONSTAT_Bits.TRK1ST */
#define IFX_TLF35584_VMONSTAT_TRK1ST_OFF (6u)

/** \brief Length for Ifx_TLF35584_VMONSTAT_Bits.TRK2ST */
#define IFX_TLF35584_VMONSTAT_TRK2ST_LEN (1u)

/** \brief Mask for Ifx_TLF35584_VMONSTAT_Bits.TRK2ST */
#define IFX_TLF35584_VMONSTAT_TRK2ST_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_VMONSTAT_Bits.TRK2ST */
#define IFX_TLF35584_VMONSTAT_TRK2ST_OFF (7u)

/** \brief Length for Ifx_TLF35584_DEVSTAT_Bits.STATE */
#define IFX_TLF35584_DEVSTAT_STATE_LEN (3u)

/** \brief Mask for Ifx_TLF35584_DEVSTAT_Bits.STATE */
#define IFX_TLF35584_DEVSTAT_STATE_MSK (0x7u)

/** \brief Offset for Ifx_TLF35584_DEVSTAT_Bits.STATE */
#define IFX_TLF35584_DEVSTAT_STATE_OFF (0u)

/** \brief Length for Ifx_TLF35584_DEVSTAT_Bits.VREFEN */
#define IFX_TLF35584_DEVSTAT_VREFEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVSTAT_Bits.VREFEN */
#define IFX_TLF35584_DEVSTAT_VREFEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVSTAT_Bits.VREFEN */
#define IFX_TLF35584_DEVSTAT_VREFEN_OFF (3u)

/** \brief Length for Ifx_TLF35584_DEVSTAT_Bits.STBYEN */
#define IFX_TLF35584_DEVSTAT_STBYEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVSTAT_Bits.STBYEN */
#define IFX_TLF35584_DEVSTAT_STBYEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVSTAT_Bits.STBYEN */
#define IFX_TLF35584_DEVSTAT_STBYEN_OFF (4u)

/** \brief Length for Ifx_TLF35584_DEVSTAT_Bits.COMEN */
#define IFX_TLF35584_DEVSTAT_COMEN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVSTAT_Bits.COMEN */
#define IFX_TLF35584_DEVSTAT_COMEN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVSTAT_Bits.COMEN */
#define IFX_TLF35584_DEVSTAT_COMEN_OFF (5u)

/** \brief Length for Ifx_TLF35584_DEVSTAT_Bits.TRK1EN */
#define IFX_TLF35584_DEVSTAT_TRK1EN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVSTAT_Bits.TRK1EN */
#define IFX_TLF35584_DEVSTAT_TRK1EN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVSTAT_Bits.TRK1EN */
#define IFX_TLF35584_DEVSTAT_TRK1EN_OFF (6u)

/** \brief Length for Ifx_TLF35584_DEVSTAT_Bits.TRK2EN */
#define IFX_TLF35584_DEVSTAT_TRK2EN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_DEVSTAT_Bits.TRK2EN */
#define IFX_TLF35584_DEVSTAT_TRK2EN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_DEVSTAT_Bits.TRK2EN */
#define IFX_TLF35584_DEVSTAT_TRK2EN_OFF (7u)

/** \brief Length for Ifx_TLF35584_PROTSTAT_Bits.LOCK */
#define IFX_TLF35584_PROTSTAT_LOCK_LEN (1u)

/** \brief Mask for Ifx_TLF35584_PROTSTAT_Bits.LOCK */
#define IFX_TLF35584_PROTSTAT_LOCK_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_PROTSTAT_Bits.LOCK */
#define IFX_TLF35584_PROTSTAT_LOCK_OFF (0u)

/** \brief Length for Ifx_TLF35584_PROTSTAT_Bits.KEY1OK */
#define IFX_TLF35584_PROTSTAT_KEY1OK_LEN (1u)

/** \brief Mask for Ifx_TLF35584_PROTSTAT_Bits.KEY1OK */
#define IFX_TLF35584_PROTSTAT_KEY1OK_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_PROTSTAT_Bits.KEY1OK */
#define IFX_TLF35584_PROTSTAT_KEY1OK_OFF (4u)

/** \brief Length for Ifx_TLF35584_PROTSTAT_Bits.KEY2OK */
#define IFX_TLF35584_PROTSTAT_KEY2OK_LEN (1u)

/** \brief Mask for Ifx_TLF35584_PROTSTAT_Bits.KEY2OK */
#define IFX_TLF35584_PROTSTAT_KEY2OK_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_PROTSTAT_Bits.KEY2OK */
#define IFX_TLF35584_PROTSTAT_KEY2OK_OFF (5u)

/** \brief Length for Ifx_TLF35584_PROTSTAT_Bits.KEY3OK */
#define IFX_TLF35584_PROTSTAT_KEY3OK_LEN (1u)

/** \brief Mask for Ifx_TLF35584_PROTSTAT_Bits.KEY3OK */
#define IFX_TLF35584_PROTSTAT_KEY3OK_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_PROTSTAT_Bits.KEY3OK */
#define IFX_TLF35584_PROTSTAT_KEY3OK_OFF (6u)

/** \brief Length for Ifx_TLF35584_PROTSTAT_Bits.KEY4OK */
#define IFX_TLF35584_PROTSTAT_KEY4OK_LEN (1u)

/** \brief Mask for Ifx_TLF35584_PROTSTAT_Bits.KEY4OK */
#define IFX_TLF35584_PROTSTAT_KEY4OK_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_PROTSTAT_Bits.KEY4OK */
#define IFX_TLF35584_PROTSTAT_KEY4OK_OFF (7u)

/** \brief Length for Ifx_TLF35584_WWDSTAT_Bits.WWDECNT */
#define IFX_TLF35584_WWDSTAT_WWDECNT_LEN (4u)

/** \brief Mask for Ifx_TLF35584_WWDSTAT_Bits.WWDECNT */
#define IFX_TLF35584_WWDSTAT_WWDECNT_MSK (0xfu)

/** \brief Offset for Ifx_TLF35584_WWDSTAT_Bits.WWDECNT */
#define IFX_TLF35584_WWDSTAT_WWDECNT_OFF (0u)

/** \brief Length for Ifx_TLF35584_FWDSTAT0_Bits.FWDQUEST */
#define IFX_TLF35584_FWDSTAT0_FWDQUEST_LEN (4u)

/** \brief Mask for Ifx_TLF35584_FWDSTAT0_Bits.FWDQUEST */
#define IFX_TLF35584_FWDSTAT0_FWDQUEST_MSK (0xfu)

/** \brief Offset for Ifx_TLF35584_FWDSTAT0_Bits.FWDQUEST */
#define IFX_TLF35584_FWDSTAT0_FWDQUEST_OFF (0u)

/** \brief Length for Ifx_TLF35584_FWDSTAT0_Bits.FWDRSPC */
#define IFX_TLF35584_FWDSTAT0_FWDRSPC_LEN (2u)

/** \brief Mask for Ifx_TLF35584_FWDSTAT0_Bits.FWDRSPC */
#define IFX_TLF35584_FWDSTAT0_FWDRSPC_MSK (0x3u)

/** \brief Offset for Ifx_TLF35584_FWDSTAT0_Bits.FWDRSPC */
#define IFX_TLF35584_FWDSTAT0_FWDRSPC_OFF (4u)

/** \brief Length for Ifx_TLF35584_FWDSTAT0_Bits.FWDRSPOK */
#define IFX_TLF35584_FWDSTAT0_FWDRSPOK_LEN (1u)

/** \brief Mask for Ifx_TLF35584_FWDSTAT0_Bits.FWDRSPOK */
#define IFX_TLF35584_FWDSTAT0_FWDRSPOK_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_FWDSTAT0_Bits.FWDRSPOK */
#define IFX_TLF35584_FWDSTAT0_FWDRSPOK_OFF (6u)

/** \brief Length for Ifx_TLF35584_FWDSTAT1_Bits.FWDECNT */
#define IFX_TLF35584_FWDSTAT1_FWDECNT_LEN (4u)

/** \brief Mask for Ifx_TLF35584_FWDSTAT1_Bits.FWDECNT */
#define IFX_TLF35584_FWDSTAT1_FWDECNT_MSK (0xfu)

/** \brief Offset for Ifx_TLF35584_FWDSTAT1_Bits.FWDECNT */
#define IFX_TLF35584_FWDSTAT1_FWDECNT_OFF (0u)

/** \brief Length for Ifx_TLF35584_ABIST_CTRL0_Bits.START */
#define IFX_TLF35584_ABIST_CTRL0_START_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_CTRL0_Bits.START */
#define IFX_TLF35584_ABIST_CTRL0_START_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_CTRL0_Bits.START */
#define IFX_TLF35584_ABIST_CTRL0_START_OFF (0u)

/** \brief Length for Ifx_TLF35584_ABIST_CTRL0_Bits.PATH */
#define IFX_TLF35584_ABIST_CTRL0_PATH_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_CTRL0_Bits.PATH */
#define IFX_TLF35584_ABIST_CTRL0_PATH_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_CTRL0_Bits.PATH */
#define IFX_TLF35584_ABIST_CTRL0_PATH_OFF (1u)

/** \brief Length for Ifx_TLF35584_ABIST_CTRL0_Bits.SINGLE */
#define IFX_TLF35584_ABIST_CTRL0_SINGLE_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_CTRL0_Bits.SINGLE */
#define IFX_TLF35584_ABIST_CTRL0_SINGLE_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_CTRL0_Bits.SINGLE */
#define IFX_TLF35584_ABIST_CTRL0_SINGLE_OFF (2u)

/** \brief Length for Ifx_TLF35584_ABIST_CTRL0_Bits.INT */
#define IFX_TLF35584_ABIST_CTRL0_INT_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_CTRL0_Bits.INT */
#define IFX_TLF35584_ABIST_CTRL0_INT_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_CTRL0_Bits.INT */
#define IFX_TLF35584_ABIST_CTRL0_INT_OFF (3u)

/** \brief Length for Ifx_TLF35584_ABIST_CTRL0_Bits.STATUS */
#define IFX_TLF35584_ABIST_CTRL0_STATUS_LEN (4u)

/** \brief Mask for Ifx_TLF35584_ABIST_CTRL0_Bits.STATUS */
#define IFX_TLF35584_ABIST_CTRL0_STATUS_MSK (0xfu)

/** \brief Offset for Ifx_TLF35584_ABIST_CTRL0_Bits.STATUS */
#define IFX_TLF35584_ABIST_CTRL0_STATUS_OFF (4u)

/** \brief Length for Ifx_TLF35584_ABIST_CTRL1_Bits.OV_TRIG */
#define IFX_TLF35584_ABIST_CTRL1_OV_TRIG_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_CTRL1_Bits.OV_TRIG */
#define IFX_TLF35584_ABIST_CTRL1_OV_TRIG_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_CTRL1_Bits.OV_TRIG */
#define IFX_TLF35584_ABIST_CTRL1_OV_TRIG_OFF (0u)

/** \brief Length for Ifx_TLF35584_ABIST_CTRL1_Bits.ABIST_CLK_EN */
#define IFX_TLF35584_ABIST_CTRL1_ABIST_CLK_EN_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_CTRL1_Bits.ABIST_CLK_EN */
#define IFX_TLF35584_ABIST_CTRL1_ABIST_CLK_EN_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_CTRL1_Bits.ABIST_CLK_EN */
#define IFX_TLF35584_ABIST_CTRL1_ABIST_CLK_EN_OFF (1u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT0_Bits.PREGOV */
#define IFX_TLF35584_ABIST_SELECT0_PREGOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT0_Bits.PREGOV */
#define IFX_TLF35584_ABIST_SELECT0_PREGOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT0_Bits.PREGOV */
#define IFX_TLF35584_ABIST_SELECT0_PREGOV_OFF (0u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT0_Bits.UCOV */
#define IFX_TLF35584_ABIST_SELECT0_UCOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT0_Bits.UCOV */
#define IFX_TLF35584_ABIST_SELECT0_UCOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT0_Bits.UCOV */
#define IFX_TLF35584_ABIST_SELECT0_UCOV_OFF (1u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT0_Bits.STBYOV */
#define IFX_TLF35584_ABIST_SELECT0_STBYOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT0_Bits.STBYOV */
#define IFX_TLF35584_ABIST_SELECT0_STBYOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT0_Bits.STBYOV */
#define IFX_TLF35584_ABIST_SELECT0_STBYOV_OFF (2u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT0_Bits.VCOREOV */
#define IFX_TLF35584_ABIST_SELECT0_VCOREOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT0_Bits.VCOREOV */
#define IFX_TLF35584_ABIST_SELECT0_VCOREOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT0_Bits.VCOREOV */
#define IFX_TLF35584_ABIST_SELECT0_VCOREOV_OFF (3u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT0_Bits.COMOV */
#define IFX_TLF35584_ABIST_SELECT0_COMOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT0_Bits.COMOV */
#define IFX_TLF35584_ABIST_SELECT0_COMOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT0_Bits.COMOV */
#define IFX_TLF35584_ABIST_SELECT0_COMOV_OFF (4u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT0_Bits.VREFOV */
#define IFX_TLF35584_ABIST_SELECT0_VREFOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT0_Bits.VREFOV */
#define IFX_TLF35584_ABIST_SELECT0_VREFOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT0_Bits.VREFOV */
#define IFX_TLF35584_ABIST_SELECT0_VREFOV_OFF (5u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT0_Bits.TRK1OV */
#define IFX_TLF35584_ABIST_SELECT0_TRK1OV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT0_Bits.TRK1OV */
#define IFX_TLF35584_ABIST_SELECT0_TRK1OV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT0_Bits.TRK1OV */
#define IFX_TLF35584_ABIST_SELECT0_TRK1OV_OFF (6u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT0_Bits.TRK2OV */
#define IFX_TLF35584_ABIST_SELECT0_TRK2OV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT0_Bits.TRK2OV */
#define IFX_TLF35584_ABIST_SELECT0_TRK2OV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT0_Bits.TRK2OV */
#define IFX_TLF35584_ABIST_SELECT0_TRK2OV_OFF (7u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT1_Bits.PREGUV */
#define IFX_TLF35584_ABIST_SELECT1_PREGUV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT1_Bits.PREGUV */
#define IFX_TLF35584_ABIST_SELECT1_PREGUV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT1_Bits.PREGUV */
#define IFX_TLF35584_ABIST_SELECT1_PREGUV_OFF (0u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT1_Bits.UCUV */
#define IFX_TLF35584_ABIST_SELECT1_UCUV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT1_Bits.UCUV */
#define IFX_TLF35584_ABIST_SELECT1_UCUV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT1_Bits.UCUV */
#define IFX_TLF35584_ABIST_SELECT1_UCUV_OFF (1u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT1_Bits.STBYUV */
#define IFX_TLF35584_ABIST_SELECT1_STBYUV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT1_Bits.STBYUV */
#define IFX_TLF35584_ABIST_SELECT1_STBYUV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT1_Bits.STBYUV */
#define IFX_TLF35584_ABIST_SELECT1_STBYUV_OFF (2u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT1_Bits.VCOREUV */
#define IFX_TLF35584_ABIST_SELECT1_VCOREUV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT1_Bits.VCOREUV */
#define IFX_TLF35584_ABIST_SELECT1_VCOREUV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT1_Bits.VCOREUV */
#define IFX_TLF35584_ABIST_SELECT1_VCOREUV_OFF (3u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT1_Bits.COMUV */
#define IFX_TLF35584_ABIST_SELECT1_COMUV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT1_Bits.COMUV */
#define IFX_TLF35584_ABIST_SELECT1_COMUV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT1_Bits.COMUV */
#define IFX_TLF35584_ABIST_SELECT1_COMUV_OFF (4u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT1_Bits.VREFUV */
#define IFX_TLF35584_ABIST_SELECT1_VREFUV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT1_Bits.VREFUV */
#define IFX_TLF35584_ABIST_SELECT1_VREFUV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT1_Bits.VREFUV */
#define IFX_TLF35584_ABIST_SELECT1_VREFUV_OFF (5u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT1_Bits.TRK1UV */
#define IFX_TLF35584_ABIST_SELECT1_TRK1UV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT1_Bits.TRK1UV */
#define IFX_TLF35584_ABIST_SELECT1_TRK1UV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT1_Bits.TRK1UV */
#define IFX_TLF35584_ABIST_SELECT1_TRK1UV_OFF (6u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT1_Bits.TRK2UV */
#define IFX_TLF35584_ABIST_SELECT1_TRK2UV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT1_Bits.TRK2UV */
#define IFX_TLF35584_ABIST_SELECT1_TRK2UV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT1_Bits.TRK2UV */
#define IFX_TLF35584_ABIST_SELECT1_TRK2UV_OFF (7u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT2_Bits.VBATOV */
#define IFX_TLF35584_ABIST_SELECT2_VBATOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT2_Bits.VBATOV */
#define IFX_TLF35584_ABIST_SELECT2_VBATOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT2_Bits.VBATOV */
#define IFX_TLF35584_ABIST_SELECT2_VBATOV_OFF (0u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT2_Bits.INTOV */
#define IFX_TLF35584_ABIST_SELECT2_INTOV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT2_Bits.INTOV */
#define IFX_TLF35584_ABIST_SELECT2_INTOV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT2_Bits.INTOV */
#define IFX_TLF35584_ABIST_SELECT2_INTOV_OFF (3u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT2_Bits.BG12UV */
#define IFX_TLF35584_ABIST_SELECT2_BG12UV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT2_Bits.BG12UV */
#define IFX_TLF35584_ABIST_SELECT2_BG12UV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT2_Bits.BG12UV */
#define IFX_TLF35584_ABIST_SELECT2_BG12UV_OFF (4u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT2_Bits.BG12OV */
#define IFX_TLF35584_ABIST_SELECT2_BG12OV_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT2_Bits.BG12OV */
#define IFX_TLF35584_ABIST_SELECT2_BG12OV_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT2_Bits.BG12OV */
#define IFX_TLF35584_ABIST_SELECT2_BG12OV_OFF (5u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT2_Bits.BIASLOW */
#define IFX_TLF35584_ABIST_SELECT2_BIASLOW_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT2_Bits.BIASLOW */
#define IFX_TLF35584_ABIST_SELECT2_BIASLOW_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT2_Bits.BIASLOW */
#define IFX_TLF35584_ABIST_SELECT2_BIASLOW_OFF (6u)

/** \brief Length for Ifx_TLF35584_ABIST_SELECT2_Bits.BIASHI */
#define IFX_TLF35584_ABIST_SELECT2_BIASHI_LEN (1u)

/** \brief Mask for Ifx_TLF35584_ABIST_SELECT2_Bits.BIASHI */
#define IFX_TLF35584_ABIST_SELECT2_BIASHI_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_ABIST_SELECT2_Bits.BIASHI */
#define IFX_TLF35584_ABIST_SELECT2_BIASHI_OFF (7u)

/** \brief Length for Ifx_TLF35584_BCK_FREQ_CHANGE_Bits.BCK_FREQ_SEL */
#define IFX_TLF35584_BCK_FREQ_CHANGE_BCK_FREQ_SEL_LEN (3u)

/** \brief Mask for Ifx_TLF35584_BCK_FREQ_CHANGE_Bits.BCK_FREQ_SEL */
#define IFX_TLF35584_BCK_FREQ_CHANGE_BCK_FREQ_SEL_MSK (0x7u)

/** \brief Offset for Ifx_TLF35584_BCK_FREQ_CHANGE_Bits.BCK_FREQ_SEL */
#define IFX_TLF35584_BCK_FREQ_CHANGE_BCK_FREQ_SEL_OFF (0u)

/** \brief Length for Ifx_TLF35584_BCK_FRE_SPREAD_Bits.FRE_SP_THR */
#define IFX_TLF35584_BCK_FRE_SPREAD_FRE_SP_THR_LEN (8u)

/** \brief Mask for Ifx_TLF35584_BCK_FRE_SPREAD_Bits.FRE_SP_THR */
#define IFX_TLF35584_BCK_FRE_SPREAD_FRE_SP_THR_MSK (0xffu)

/** \brief Offset for Ifx_TLF35584_BCK_FRE_SPREAD_Bits.FRE_SP_THR */
#define IFX_TLF35584_BCK_FRE_SPREAD_FRE_SP_THR_OFF (0u)

/** \brief Length for Ifx_TLF35584_BCK_MAIN_CTRL_Bits.DATA_VALID */
#define IFX_TLF35584_BCK_MAIN_CTRL_DATA_VALID_LEN (1u)

/** \brief Mask for Ifx_TLF35584_BCK_MAIN_CTRL_Bits.DATA_VALID */
#define IFX_TLF35584_BCK_MAIN_CTRL_DATA_VALID_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_BCK_MAIN_CTRL_Bits.DATA_VALID */
#define IFX_TLF35584_BCK_MAIN_CTRL_DATA_VALID_OFF (6u)

/** \brief Length for Ifx_TLF35584_BCK_MAIN_CTRL_Bits.BUSY */
#define IFX_TLF35584_BCK_MAIN_CTRL_BUSY_LEN (1u)

/** \brief Mask for Ifx_TLF35584_BCK_MAIN_CTRL_Bits.BUSY */
#define IFX_TLF35584_BCK_MAIN_CTRL_BUSY_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_BCK_MAIN_CTRL_Bits.BUSY */
#define IFX_TLF35584_BCK_MAIN_CTRL_BUSY_OFF (7u)

/** \brief Length for Ifx_TLF35584_GTM_Bits.TM */
#define IFX_TLF35584_GTM_TM_LEN (1u)

/** \brief Mask for Ifx_TLF35584_GTM_Bits.TM */
#define IFX_TLF35584_GTM_TM_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_GTM_Bits.TM */
#define IFX_TLF35584_GTM_TM_OFF (0u)

/** \brief Length for Ifx_TLF35584_GTM_Bits.NTM */
#define IFX_TLF35584_GTM_NTM_LEN (1u)

/** \brief Mask for Ifx_TLF35584_GTM_Bits.NTM */
#define IFX_TLF35584_GTM_NTM_MSK (0x1u)

/** \brief Offset for Ifx_TLF35584_GTM_Bits.NTM */
#define IFX_TLF35584_GTM_NTM_OFF (1u)

/** \}  */
/******************************************************************************/
/******************************************************************************/
#endif /* IFXTLF35584_BF_H */
