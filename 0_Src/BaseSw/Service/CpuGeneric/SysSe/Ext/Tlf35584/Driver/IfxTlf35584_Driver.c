/**
 * \file IfxTlf35584_Driver.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 */

#include "SysSe/Ext/Tlf35584/Driver/IfxTlf35584_Driver.h"
#include "SysSe/Ext/Tlf35584/If/IfxTlf35584_If.h"
#include "SysSe/Ext/Tlf35584/Std/IfxTlf35584.h"
#include "SysSe/Ext/Tlf35584/_Reg/IfxTlf35584_bf.h"
#include "string.h"

#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
#pragma message  "WARNING: TLF35584 Cache feature is experimental"
#endif

#if IFX_CFG_TLF35584_DEBUG_SPI
boolean IfxTlf35584_g_showSpiFrame = FALSE;
#endif

/*--------------------------------------------*/
boolean IfxTlf35584_Driver_disableCache(IfxTlf35584_Driver *driver)
{
    boolean result;
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
    /* Ensure all cached data are written to the device before disabling the cache */
    result = Ifx_DeviceCache_writeBack8(&driver->cache.engine);
    Ifx_DeviceCache_disable(&driver->cache.engine);
#else
    (void)driver;
    result = TRUE;
#endif
    return result;
}


void IfxTlf35584_Driver_enableCache(IfxTlf35584_Driver *driver)
{
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
    Ifx_DeviceCache_enable(&driver->cache.engine);
#else
    (void)driver;
#endif
}

boolean IfxTlf35584_Driver_isCacheEnabled(IfxTlf35584_Driver *driver)
{
    boolean enabled;
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
    enabled = Ifx_DeviceCache_isCacheEnabled(&driver->cache.engine);
#else
    (void)driver;
    enabled = FALSE;
#endif
    return enabled;
}


boolean IfxTlf35584_Driver_restoreCache(IfxTlf35584_Driver *driver, boolean enabled)
{
    boolean result;
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE

    if (enabled)
    {
        Ifx_DeviceCache_enable(&driver->cache.engine);
        result = TRUE;
    }
    else
    {
        result = Ifx_DeviceCache_disable(&driver->cache.engine);
    }

#else
    (void)driver;
    (void)enabled;
    result = TRUE;
#endif
	return result;
}

/** Check that the protected watchdog configuration request match the written values
 *
 * \param driver Pointer to the driver object, will be initialized by the function
 *
 * \note This function should be called right after the lock operation as it uses the
 * cached register values for comparison. Any read cache invalidate operation between
 * the protected register setting and the call to this function will corrupt the check
 * operation FIXME [New feature] Should a flag be implemented to check this condition?
 *
 *
 * \note Check feature is only available if cache is enabled
 */
static boolean IfxTlf35584_Driver_checkResuestConfig(IfxTlf35584_Driver *driver)
{
    boolean               result = TRUE;
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("IfxTlf35584_Driver_checkResuestConfig()"TLF35584_ENDL);
    IfxTlf35584_Hal_print("{"TLF35584_ENDL);
#endif
    Ifx_TLF35584_SYSPCFG0 ref_syspcfg0;
    Ifx_TLF35584_SYSPCFG1 ref_syspcfg1;
    Ifx_TLF35584_WDCFG0   ref_wdcfg0;
    Ifx_TLF35584_WDCFG1   ref_wdcfg1;
    Ifx_TLF35584_FWDCFG   ref_fwdcfg;
    Ifx_TLF35584_WWDCFG0  ref_wwdcfg0;
    Ifx_TLF35584_WWDCFG1  ref_wwdcfg1;

    Ifx_TLF35584_SYSPCFG0 dev_syspcfg0;
    Ifx_TLF35584_SYSPCFG1 dev_syspcfg1;
    Ifx_TLF35584_WDCFG0   dev_wdcfg0;
    Ifx_TLF35584_WDCFG1   dev_wdcfg1;
    Ifx_TLF35584_FWDCFG   dev_fwdcfg;
    Ifx_TLF35584_WWDCFG0  dev_wwdcfg0;
    Ifx_TLF35584_WWDCFG1  dev_wwdcfg1;

    // This feature assumes that the registers are not volatile or action write registers
    //IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, driver->cache.enabled);
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, Ifx_DeviceCache_isCachable(&driver->cache.engine, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG0) >> IFXTLF35584_ADDRESS_SHIFT)));
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, Ifx_DeviceCache_isCachable(&driver->cache.engine, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT)));
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, Ifx_DeviceCache_isCachable(&driver->cache.engine, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT)));
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, Ifx_DeviceCache_isCachable(&driver->cache.engine, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG1) >> IFXTLF35584_ADDRESS_SHIFT)));
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, Ifx_DeviceCache_isCachable(&driver->cache.engine, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDCFG) >> IFXTLF35584_ADDRESS_SHIFT)));
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, Ifx_DeviceCache_isCachable(&driver->cache.engine, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT)));
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, Ifx_DeviceCache_isCachable(&driver->cache.engine, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG1) >> IFXTLF35584_ADDRESS_SHIFT)));

    // Read values from cache
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &ref_syspcfg0.U);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &ref_syspcfg1.U);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &ref_wdcfg0.U);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &ref_wdcfg1.U);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDCFG) >> IFXTLF35584_ADDRESS_SHIFT), &ref_fwdcfg.U);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &ref_wwdcfg0.U);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &ref_wwdcfg1.U);

    // Read values from device
    IfxTlf35584_Driver_invalidateCache(driver);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &dev_syspcfg0.U);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.SYSPCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &dev_syspcfg1.U);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &dev_wdcfg0.U);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &dev_wdcfg1.U);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.FWDCFG) >> IFXTLF35584_ADDRESS_SHIFT), &dev_fwdcfg.U);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG0) >> IFXTLF35584_ADDRESS_SHIFT), &dev_wwdcfg0.U);
    IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.WWDCFG1) >> IFXTLF35584_ADDRESS_SHIFT), &dev_wwdcfg1.U);

    result &= dev_syspcfg0.U == ref_syspcfg0.U;
    result &= dev_syspcfg1.U == ref_syspcfg1.U;
    result &= dev_wdcfg0.U == ref_wdcfg0.U;
    result &= dev_wdcfg1.U == ref_wdcfg1.U;
    result &= dev_fwdcfg.U == ref_fwdcfg.U;
    result &= dev_wwdcfg0.U == ref_wwdcfg0.U;
    result &= dev_wwdcfg1.U == ref_wwdcfg1.U;
#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("}"TLF35584_ENDL);
#endif
#else
    (void)driver;
#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("IfxTlf35584_Driver_checkResuestConfig() FEATURE NOT AVAILABLE WHEN CACHE DISBALED"TLF35584_ENDL);
#endif
#pragma message  "WARNING: TLF35584 Cache feature is disabled, value written to protected configuration registers won't be checked after lock"
#endif

    return result;
}

boolean IfxTlf35584_Driver_init(IfxTlf35584_Driver *driver, IfxTlf35584_Driver_Config *config)
{
    boolean          result   = TRUE;
    IfxTlf35584_Time deadline = TIME_NULL;
#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("IfxTlf35584_Driver_init()"TLF35584_ENDL);
    IfxTlf35584_Hal_print("{"TLF35584_ENDL);
#endif

    IFX_ASSERT(IFX_VERBOSE_LEVEL_FAILURE, (((sizeof(Ifx_TLF35584) >> IFXTLF35584_ADDRESS_SHIFT)-1)/32+1) == (sizeof(IfxTlf35584_g_validAddress) / 4));
    driver->status = IfxTlf35584_status_notInitialized;
    IfxTlf35584_Hal_init(&config->hal);

    driver->channel       = config->hal.channel;
    driver->pins.wdi      = config->hal.wdi;
    driver->useSpiTrigger = config->windowWatchdog.useSpiTrigger;

#if IFX_CFG_TLF35584_DEBUG_SPI
    IfxTlf35584_g_showSpiFrame = config->showSpiFrame;
#endif

#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
    Ifx_DeviceCache_Config cacheConfig;
    Ifx_DeviceCache_initConfig(&cacheConfig);

    cacheConfig.ItemCount            = sizeof(Ifx_TLF35584) / IFXTLF35584_ADDRESS_UNIT;
    cacheConfig.buffer               = (void*)&driver->cache.cache;
    cacheConfig.bufferNoOpMaskActual = (void*)&driver->cache.bufferNoOpMaskActual;
    cacheConfig.bufferNoOpMaskDefault= (void*)&IfxTlf35584_g_noOpMask;
    cacheConfig.bufferNoOpValue      = (void*)&IfxTlf35584_g_noOpValue;
    cacheConfig.modificationMask     = &driver->cache.modificationMask[0];
    cacheConfig.validAddress         = (uint32 *)&IfxTlf35584_g_validAddress[0];
    cacheConfig.volatileRegisters    = (uint32 *)&IfxTlf35584_g_volatileRegisters[0];
    cacheConfig.actionReadRegisters  = (uint32 *)&IfxTlf35584_g_actionReadRegisters[0];
    cacheConfig.actionWriteRegisters = (uint32 *)&IfxTlf35584_g_actionWriteRegisters[0];
    cacheConfig.valid                = &driver->cache.cacheValid[0];
    cacheConfig.writeBackErrorFlags  = &driver->cache.writeBackErrorFlags[0];
    cacheConfig.flagArraySize        = sizeof(IfxTlf35584_g_validAddress) / 4;
    cacheConfig.read                 = (Ifx_DeviceCache_ReadFromDevice)IfxTlf35584_If_readRegister;
    cacheConfig.write                = (Ifx_DeviceCache_WriteToDevice)IfxTlf35584_If_writeRegister;
    cacheConfig.writeBack			 = &Ifx_DeviceCache_writeBack8;
    cacheConfig.deviceDriver         = driver;
    cacheConfig.registerWidth        = IFXTLF35584_ADDRESS_UNIT;
    cacheConfig.previousCache        = NULL_PTR;
    cacheConfig.tempCacheLine        = driver->cache.tempCacheLine;

    result                          &= Ifx_DeviceCache_init(&driver->cache.engine, &cacheConfig);

    if (!config->cacheEnabled)
    {
    	IfxTlf35584_Driver_disableCache(driver);
    }

    if (result)
#endif
    {   /* WARNING: part of the previous if (result) statement */
        driver->status = IfxTlf35584_status_ready;
    }

    // A delay of 3xTsam (3*213us=639) has must be respected between IfxTlf35584_Driver_serviceWindowWatchdogPin() and
    // IfxTlf35584_Driver_releaseWindowWatchdogPin() calls, either ways.

    driver->windowWatchdogMinDelay = IFXTLF35584_TIME_1S * 639e-6;

    if (!driver->useSpiTrigger)
    {                                                                           /* Initialize WDI to high */
        result  &= IfxTlf35584_Driver_releaseWindowWatchdogTrigger(driver);
        deadline = IfxTlf35584_Hal_getDeadline(driver->windowWatchdogMinDelay); /* Ensure minimal high time of the WDI signal */
    }

    /* Device configuration */

    result &= IfxTlf35584_Driver_unlock(driver);
    IfxTlf35584_setRequestWatchdogCycleTime(driver, config->watchdogCycleTime, &result);
    IfxTlf35584_setRequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep(driver, config->watchdogEnabledWhileInSleep, &result);

    IfxTlf35584_setRequestStandbyRegulatorQstEnable(driver, config->standByQstEnabled, &result);

    IfxTlf35584_setRequestSafeState2Delay(driver, config->safeState2Delay, &result);

    if (config->windowWatchdog.enabled)
    {
        result &= IfxTlf35584_Driver_enableWindowWatchdog(driver, config->windowWatchdog.openWindowTime, config->windowWatchdog.closedWindowTime, config->windowWatchdog.useSpiTrigger, config->watchdogCycleTime);
        IfxTlf35584_setRequestWindowWatchdogErrorThreshold(driver, config->windowWatchdog.errorThreshold, &result);
    }
    else
    {
        result &= IfxTlf35584_Driver_disableWindowWatchdog(driver);
    }

    if (config->functionalWatchdog.enabled)
    {
        result &= IfxTlf35584_Driver_enableFunctionalWatchdog(driver, config->functionalWatchdog.heartbeatTimerPeriod, config->functionalWatchdog.servicePeriod, config->watchdogCycleTime);
        IfxTlf35584_setRequestFunctionalWatchdogErrorThreshold(driver, config->functionalWatchdog.errorThreshold, &result);
    }
    else
    {
        result &= IfxTlf35584_Driver_disableFunctionalWatchdog(driver);
    }

    if (config->errPinMonitoring.enabled)
    {
        IfxTlf35584_enableRequestErrPinMonitor(driver, &result);
        IfxTlf35584_setRequestErrPinMonitorRecoveryEnable(driver, config->errPinMonitoring.recoveryEnabled, &result);
        IfxTlf35584_setRequestErrPinMonitorRecoveryTime(driver, config->errPinMonitoring.recoveryTime, &result);
        IfxTlf35584_setRequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep(driver, config->errPinMonitoring.enabledWhileInSleep, &result);
    }
    else
    {
        IfxTlf35584_disableRequestErrPinMonitor(driver, &result);
    }

    result &= IfxTlf35584_Driver_lock(driver);
    result &= IfxTlf35584_Driver_checkResuestConfig(driver);

    if (config->windowWatchdog.enabled)
    {
        IfxTlf35584_Hal_waitForDeadline(deadline);                                                                 /* Ensure minimal high time of the WDI signal */
        /* Service the window watchdog */
        result                                     &= IfxTlf35584_Driver_serviceWindowWatchdog(driver);
        deadline                                    = IfxTlf35584_Hal_getDeadline(driver->windowWatchdogMinDelay); /* Ensure minimal high time of the WDI signal */

        driver->windowWatchdogServiceRequest        = IfxTlf35584_Hal_getDeadline(driver->windowWatchdogClosedWindowTime + driver->windowWatchdogOpenedWindowTime / 2);
        driver->windowWatchdogServiceRequestRelease = TIME_INFINITE;
    }

    if (config->functionalWatchdog.enabled)
    {
        result                                  &= IfxTlf35584_Driver_serviceFunctionalWatchdog(driver);
        driver->functionalWatchdogServiceRequest = IfxTlf35584_Hal_getDeadline(driver->functionalWatchdogPeriod);
    }

    if (config->windowWatchdog.enabled)
    {
        IfxTlf35584_Hal_waitForDeadline(deadline); /* Ensure minimal high time of the WDI signal */
        /* Release of the WDi signal is required for a complete service */
        IfxTlf35584_Driver_releaseWindowWatchdogTrigger(driver);
    }
#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("}"TLF35584_ENDL);
#endif

    return result;
}


void IfxTlf35584_Driver_initConfig(IfxTlf35584_Driver_Config *config)
{
    IfxTlf35584_Hal_initConfig(&config->hal);
    config->watchdogCycleTime                       = IfxTlf35584_RequestWatchdogCycleTime_1ms;
    config->watchdogEnabledWhileInSleep             = IfxTlf35584_RequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep_disabled;

    config->standByQstEnabled                       = IfxTlf35584_RequestStandbyRegulatorQstEnable_enabled;

    config->safeState2Delay                         = IfxTlf35584_RequestSafeState2Delay_noDelay;

    config->windowWatchdog.useSpiTrigger            = TRUE;
    config->windowWatchdog.enabled                  = TRUE;
    config->windowWatchdog.errorThreshold           = 9;
    config->windowWatchdog.openWindowTime           = (0xB + 1) * 50 * 1e-3;
    config->windowWatchdog.closedWindowTime         = (6 + 1) * 50 * 1e-3;

    config->functionalWatchdog.errorThreshold       = 9;
    config->functionalWatchdog.enabled              = FALSE;
    config->functionalWatchdog.heartbeatTimerPeriod = (0xB + 1) * 50 * 1e-3;

    config->errPinMonitoring.recoveryTime           = IfxTlf35584_RequestErrPinMonitorRecoveryTime_1ms;
    config->errPinMonitoring.recoveryEnabled        = IfxTlf35584_RequestErrPinMonitorRecoveryEnable_disabled;
    config->errPinMonitoring.enabled                = TRUE;
    config->errPinMonitoring.enabledWhileInSleep    = IfxTlf35584_RequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep_disabled;
    config->showSpiFrame    = FALSE;
    config->cacheEnabled    = TRUE;
}


boolean IfxTlf35584_Driver_readRegister(IfxTlf35584_Driver *driver, IfxTlf35584_Address address, IfxTlf35584_UData *data)
{
    boolean result = TRUE;
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
	result = Ifx_DeviceCache_getValue8(&driver->cache.engine, address, data);
#else
	result = IfxTlf35584_If_readRegister(driver, address, data);
#endif

    return result;
}


boolean IfxTlf35584_Driver_readSync(IfxTlf35584_Driver *driver)
{
    boolean result = TRUE;
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE

	result = Ifx_DeviceCache_synchronize8(&driver->cache.engine);
#else
    (void)driver;
#endif

    return result;
}


boolean IfxTlf35584_Driver_writeRegister(IfxTlf35584_Driver *driver, IfxTlf35584_Address address, IfxTlf35584_UData data)
{
    boolean result;
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
	result = Ifx_DeviceCache_setValue8(&driver->cache.engine, address, data);
#else
	result = IfxTlf35584_If_writeRegister(driver, 1, address, &data);
#endif

    return result;
}

boolean IfxTlf35584_Driver_writeRegisterStar(IfxTlf35584_Driver *driver, IfxTlf35584_Address address, IfxTlf35584_UData data, IfxTlf35584_UData noOpMask)
{
    boolean result;
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
	result = Ifx_DeviceCache_setValue8Star(&driver->cache.engine, address, data, noOpMask);
#else
	/* Set the required bitfields to no action */
	IfxTlf35584_UData mask;
	mask = ((IfxTlf35584_UData*)&IfxTlf35584_g_noOpMask)[address] & ~noOpMask;
	data = (data & ~mask) | ((((IfxTlf35584_UData*)&IfxTlf35584_g_noOpValue)[address]) & mask);
	result = IfxTlf35584_If_writeRegister(driver, 1, address, &data);
#endif

    return result;
}


boolean IfxTlf35584_Driver_writeSync(IfxTlf35584_Driver *driver)
{
    boolean result = TRUE;
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("IfxTlf35584_Driver_writeSync()"TLF35584_ENDL);
    IfxTlf35584_Hal_print("{"TLF35584_ENDL);
#endif
	result = Ifx_DeviceCache_writeBack8(&driver->cache.engine);
#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("}"TLF35584_ENDL);
#endif
#else
    (void)driver;
#endif

    return result;
}


boolean IfxTlf35584_Driver_unlock(IfxTlf35584_Driver *driver)
{
    boolean status = TRUE;
    boolean locked;
#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("IfxTlf35584_Driver_unlock()"TLF35584_ENDL);
    IfxTlf35584_Hal_print("{"TLF35584_ENDL);
#endif
    IfxTlf35584_setProtectionKey(driver, IfxTlf35584_ProtectionKey_unlockKey1, &status);
    IfxTlf35584_setProtectionKey(driver, IfxTlf35584_ProtectionKey_unlockKey2, &status);
    IfxTlf35584_setProtectionKey(driver, IfxTlf35584_ProtectionKey_unlockKey3, &status);
    IfxTlf35584_setProtectionKey(driver, IfxTlf35584_ProtectionKey_unlockKey4, &status);
    locked  = IfxTlf35584_isProtectedRegisterLocked(driver, &status);
#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("}"TLF35584_ENDL);
#endif
    status &= locked == 0;
    return status;
}


boolean IfxTlf35584_Driver_lock(IfxTlf35584_Driver *driver)
{
    boolean status = TRUE;
    boolean locked;
#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("IfxTlf35584_Driver_lock()"TLF35584_ENDL);
    IfxTlf35584_Hal_print("{"TLF35584_ENDL);
#endif
    /* PROTCFG is an action write register and will automatically trigger as cache write synchronization before protection sequence */
    IfxTlf35584_setProtectionKey(driver, IfxTlf35584_ProtectionKey_lockKey1, &status);
    IfxTlf35584_setProtectionKey(driver, IfxTlf35584_ProtectionKey_lockKey2, &status);
    IfxTlf35584_setProtectionKey(driver, IfxTlf35584_ProtectionKey_lockKey3, &status);
    IfxTlf35584_setProtectionKey(driver, IfxTlf35584_ProtectionKey_lockKey4, &status);
    locked  = IfxTlf35584_isProtectedRegisterLocked(driver, &status);
#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("}"TLF35584_ENDL);
#endif
    status &= locked == 1;
    return status;
}


boolean IfxTlf35584_Driver_waitForStateRequest(IfxTlf35584_Driver *driver, IfxTlf35584_Time timeout)
{
    boolean                                      result   = TRUE;
    IfxTlf35584_RequestForDeviceStateTransition state;
    IfxTlf35584_Time                             deadline = IfxTlf35584_Hal_getDeadline(timeout);

    do
    {
        state = IfxTlf35584_getRequestForDeviceStateTransition(driver, &result);
    } while (result && (state != IfxTlf35584_RequestForDeviceStateTransition_noStateChange) && (IfxTlf35584_Hal_now() < deadline));

    result &= state == IfxTlf35584_RequestForDeviceStateTransition_noStateChange;

    return result;
}


boolean IfxTlf35584_Driver_goToNormal(IfxTlf35584_Driver *driver, boolean tracker1Enabled, boolean tracker2Enabled, boolean CommunicationLdoEnabled, boolean referenceVoltageEnabled)
{
    boolean          result  = TRUE;
    IfxTlf35584_Time timeout = IFXTLF35584_TIME_1S * 100e-6; // Spec recommends min 60us

#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("IfxTlf35584_Driver_goToNormal()"TLF35584_ENDL);
    IfxTlf35584_Hal_print("{"TLF35584_ENDL);
#endif

    result &= IfxTlf35584_Driver_waitForStateRequest(driver, timeout);

    if (result)
    {
        IfxTlf35584_DeviceState state;

        result &= IfxTlf35584_Driver_setStateRequest(driver, IfxTlf35584_RequestForDeviceStateTransition_normal, tracker1Enabled, tracker2Enabled, CommunicationLdoEnabled, referenceVoltageEnabled);

        result &= IfxTlf35584_Driver_waitForStateRequest(driver, timeout);

        state   = IfxTlf35584_getDeviceState(driver, &result);
        result &= state == IfxTlf35584_DeviceState_normal;
    }

#if IFX_CFG_TLF35584_DEBUG
    IfxTlf35584_Hal_print("}"TLF35584_ENDL);
#endif
    return result;
}


boolean IfxTlf35584_Driver_serviceWindowWatchdog(IfxTlf35584_Driver *driver)
{
    boolean result = TRUE;
#if IFX_CFG_TLF35584_DEBUG && 0
    IfxTlf35584_Hal_print("IfxTlf35584_Driver_serviceWindowWatchdog()"TLF35584_ENDL);
    IfxTlf35584_Hal_print("{"TLF35584_ENDL);
#endif

    if (!driver->useSpiTrigger)
    {
        IfxTlf35584_Hal_setPortLow(&driver->pins.wdi);
    }
    else
    {
        boolean lastTrigger;

        lastTrigger = IfxTlf35584_getLastSpiTriggerReceived(driver, &result);
        lastTrigger = !lastTrigger;
        IfxTlf35584_setWindowWatchdogSpiTriggerCommand(driver, lastTrigger, &result);
    }
#if IFX_CFG_TLF35584_DEBUG && 0
    IfxTlf35584_Hal_print("}"TLF35584_ENDL);
#endif

    return result;
}


boolean IfxTlf35584_Driver_releaseWindowWatchdogTrigger(IfxTlf35584_Driver *driver)
{
    boolean result = TRUE;

    if (!driver->useSpiTrigger)
    {
        IfxTlf35584_Hal_setPortHigh(&driver->pins.wdi);
    }
    else
    {}

    return result;
}


typedef struct
{
    uint8 question;
    uint8 response[4];
}IfxTlf35584_Driver_FunctionalWatchdogResponse;

IfxTlf35584_Driver_FunctionalWatchdogResponse IfxTlf35584_Driver_g_FunctionalWatchdogResponse[16] = {
    {.question = 0x0, .response = {0xFF, 0x0F, 0xF0, 0x00}},
    {.question = 0x1, .response = {0xB0, 0x40, 0xBF, 0x4F}},
    {.question = 0x2, .response = {0xE9, 0x19, 0xE6, 0x16}},
    {.question = 0x3, .response = {0xA6, 0x56, 0xA9, 0x59}},
    {.question = 0x4, .response = {0x75, 0x85, 0x7A, 0x8A}},
    {.question = 0x5, .response = {0x3A, 0xCA, 0x35, 0xC5}},
    {.question = 0x6, .response = {0x63, 0x93, 0x6C, 0x9C}},
    {.question = 0x7, .response = {0x2C, 0xDC, 0x23, 0xD3}},
    {.question = 0x8, .response = {0xD2, 0x22, 0xDD, 0x2D}},
    {.question = 0x9, .response = {0x9D, 0x6D, 0x92, 0x62}},
    {.question = 0xA, .response = {0xC4, 0x34, 0xCB, 0x3B}},
    {.question = 0xB, .response = {0x8B, 0x7B, 0x84, 0x74}},
    {.question = 0xC, .response = {0x58, 0xA8, 0x57, 0xA7}},
    {.question = 0xD, .response = {0x17, 0xE7, 0x18, 0xE8}},
    {.question = 0xE, .response = {0x4E, 0xBE, 0x41, 0xB1}},
    {.question = 0xF, .response = {0x01, 0xF1, 0x0E, 0xFE}}
};

boolean IfxTlf35584_Driver_serviceFunctionalWatchdog(IfxTlf35584_Driver *driver)
{
    boolean result = TRUE;
    uint8   question;

#if IFX_CFG_TLF35584_DEBUG && 0
    IfxTlf35584_Hal_print("IfxTlf35584_Driver_serviceFunctionalWatchdog()"TLF35584_ENDL);
    IfxTlf35584_Hal_print("{"TLF35584_ENDL);
#endif

    question = IfxTlf35584_getFunctionalWatchdogQuestion(driver, &result);

    /* The response is written without checking the response counter FWDSTAT0.FWDRSPC. It is assumed that the synchronization is correct
     * Incorrect synchronization shows a bad system  behavior
     */

    IfxTlf35584_setFunctionalWatchdogResponse(driver, IfxTlf35584_Driver_g_FunctionalWatchdogResponse[question].response[0], &result);
    IfxTlf35584_setFunctionalWatchdogResponse(driver, IfxTlf35584_Driver_g_FunctionalWatchdogResponse[question].response[1], &result);
    IfxTlf35584_setFunctionalWatchdogResponse(driver, IfxTlf35584_Driver_g_FunctionalWatchdogResponse[question].response[2], &result);
    IfxTlf35584_setFunctionalWatchdogHeartbeatSynchronizationResponse(driver, IfxTlf35584_Driver_g_FunctionalWatchdogResponse[question].response[3], &result);

#if IFX_CFG_TLF35584_DEBUG && 0
    IfxTlf35584_Hal_print("}"TLF35584_ENDL);
#endif
    return result;
}


boolean IfxTlf35584_Driver_process(IfxTlf35584_Driver *driver)
{
    boolean result = TRUE;

    if (driver->windowWatchdogClosedWindowTime != 0)
    {
        if (isDeadLine(driver->windowWatchdogServiceRequestRelease))
        {
            result                                     &= IfxTlf35584_Driver_releaseWindowWatchdogTrigger(driver);
            driver->windowWatchdogServiceRequestRelease = TIME_INFINITE;
        }
        else if (isDeadLine(driver->windowWatchdogServiceRequest))
        {
            result                                     &= IfxTlf35584_Driver_serviceWindowWatchdog(driver);
            driver->windowWatchdogServiceRequestRelease = IfxTlf35584_Hal_getDeadline(driver->windowWatchdogMinDelay);
            driver->windowWatchdogServiceRequest        = IfxTlf35584_Hal_getDeadline(driver->windowWatchdogClosedWindowTime + driver->windowWatchdogOpenedWindowTime / 2);
        }
    }

    if ((driver->functionalWatchdogPeriod != 0) && isDeadLine(driver->functionalWatchdogServiceRequest))
    {
        driver->functionalWatchdogServiceRequest = IfxTlf35584_Hal_getDeadline(driver->functionalWatchdogPeriod);
        result                                  &= IfxTlf35584_Driver_serviceFunctionalWatchdog(driver);
    }

    return result;
}


boolean IfxTlf35584_Driver_enableWindowWatchdog(IfxTlf35584_Driver *driver, float32 openWindowTime, float32 closedWindowTime, boolean useSpiTrigger, IfxTlf35584_RequestWatchdogCycleTime cycleTime)
{
    boolean result = TRUE;
    uint8   valueCw;
    uint8   valueOw;
    float32 divider;

    driver->useSpiTrigger = useSpiTrigger;

    if (cycleTime == IfxTlf35584_RequestWatchdogCycleTime_1ms)
    {
        divider = 1e-3 * 50;
    }
    else//if (cycleTime == IfxTlf35584_RequestWatchdogCycleTime_100us)
    {
        divider = 100e-6 * 50;
    }

    valueCw = closedWindowTime / divider - 1;
    result &= valueCw <= IFX_TLF35584_WWDCFG0_CW_MSK;

    valueOw = openWindowTime / divider - 1;
    result &= valueOw <= IFX_TLF35584_WWDCFG1_OW_MSK;

    IfxTlf35584_setRequestWindowWatchdogClosedWindowTime(driver, valueCw, &result);
    IfxTlf35584_setRequestWindowWatchdogOpenWindowTime(driver, valueOw, &result);

    IfxTlf35584_setRequestWindowWatchdogTriggerSelection(driver,
        useSpiTrigger ? IfxTlf35584_RequestWindowWatchdogTriggerSelection_spi : IfxTlf35584_RequestWindowWatchdogTriggerSelection_wdiPin,
        &result);

    IfxTlf35584_enableRequestWindowWatchdog(driver, &result);/* Note: the Window watchdog is enabled by default */

    if (result)
    {
        driver->windowWatchdogClosedWindowTime = ((valueCw + 1) * divider) * IFXTLF35584_TIME_1S;
        driver->windowWatchdogOpenedWindowTime = ((valueOw + 1) * divider) * IFXTLF35584_TIME_1S;
        result                                &= driver->windowWatchdogClosedWindowTime >= driver->windowWatchdogMinDelay;
    }

    if (!result)
    {
        driver->windowWatchdogClosedWindowTime = 0;
        driver->windowWatchdogOpenedWindowTime = 0;
    }

    return result;
}


boolean IfxTlf35584_Driver_disableWindowWatchdog(IfxTlf35584_Driver *driver)
{
    boolean result = TRUE;

    IfxTlf35584_disableRequestWindowWatchdog(driver, &result);
    driver->windowWatchdogClosedWindowTime = 0;

    return result;
}


boolean IfxTlf35584_Driver_enableFunctionalWatchdog(IfxTlf35584_Driver *driver, float32 heartbeartTimerPeriod, float32 servicePeriod, IfxTlf35584_RequestWatchdogCycleTime cycleTime)
{
    boolean result = TRUE;
    uint8   value;
    float32 divider;

    if (cycleTime == IfxTlf35584_RequestWatchdogCycleTime_1ms)
    {
        divider = 1e-3 * 50;
    }
    else//if (cycleTime == IfxTlf35584_RequestWatchdogCycleTime_100us)
    {
        divider = 100e-6 * 50;
    }

    value   = heartbeartTimerPeriod / divider - 1;
    result &= value <= IFX_TLF35584_FWDCFG_WDHBTP_MSK;

    IfxTlf35584_setRequestFunctionalWatchdogHeartbeatTimerPeriod(driver, value, &result);

    IfxTlf35584_enableRequestFunctionalWatchdog(driver, &result);

    if (result)
    {
        driver->functionalWatchdogPeriod = servicePeriod * IFXTLF35584_TIME_1S;
    }

    if (!result)
    {
        driver->functionalWatchdogPeriod = 0;
    }

    return result;
}


float32 IfxTlf35584_Driver_getFunctionalWatchdogPeriod(IfxTlf35584_Driver *driver)
{
    float32 period;

    if (driver->functionalWatchdogPeriod > 0.0)
    {
        period = (float32)driver->functionalWatchdogPeriod / IFXTLF35584_TIME_1S;
    }
    else
    {
        period = 0;
    }

    return period;
}


float32 IfxTlf35584_Driver_getWindowWatchdogOpenedWindow(IfxTlf35584_Driver *driver)
{
    float32 value;

    if (driver->windowWatchdogClosedWindowTime > 0.0)
    {
        value = (float32)driver->windowWatchdogOpenedWindowTime / IFXTLF35584_TIME_1S;
    }
    else
    {
        value = 0;
    }

    return value;
}


float32 IfxTlf35584_Driver_getWindowWatchdogClosedWindow(IfxTlf35584_Driver *driver)
{
    float32 value;

    if (driver->windowWatchdogClosedWindowTime > 0.0)
    {
        value = (float32)driver->windowWatchdogClosedWindowTime / IFXTLF35584_TIME_1S;
    }
    else
    {
        value = 0;
    }

    return value;
}


boolean IfxTlf35584_Driver_disableFunctionalWatchdog(IfxTlf35584_Driver *driver)
{
    boolean result = TRUE;
    IfxTlf35584_disableRequestFunctionalWatchdog(driver, &result);
    driver->functionalWatchdogPeriod = 0;
    return result;
}


boolean IfxTlf35584_Driver_setStateRequest(IfxTlf35584_Driver *driver, IfxTlf35584_RequestForDeviceStateTransition state, boolean tracker1Enabled, boolean tracker2Enabled, boolean CommunicationLdoEnabled, boolean referenceVoltageEnabled)
{
    boolean               status;
    Ifx_TLF35584_DEVCTRL  devctrl;
    Ifx_TLF35584_DEVCTRLN devctrln;

    status = IfxTlf35584_Driver_readRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), &devctrl.U);

    if (status)
    {
        devctrl.B.STATEREQ = state;
        devctrl.B.TRK1EN   = tracker1Enabled ? 1 : 0;
        devctrl.B.TRK2EN   = tracker2Enabled ? 1 : 0;
        devctrl.B.COMEN    = CommunicationLdoEnabled ? 1 : 0;
        devctrl.B.VREFEN   = referenceVoltageEnabled ? 1 : 0;
        status            &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRL) >> IFXTLF35584_ADDRESS_SHIFT), devctrl.U);

        devctrln.U         = ~devctrl.U;
        status            &= IfxTlf35584_Driver_writeRegister(driver, (IfxTlf35584_Address)(((uint32)&MODULE_TLF35584.DEVCTRLN) >> IFXTLF35584_ADDRESS_SHIFT), devctrln.U);
    }

    return status;
}


void IfxTlf35584_Driver_dumpRegisters(IfxTlf35584_Driver *driver)
{
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
    IfxTlf35584_Address address;

    IfxTlf35584_Hal_print("TLF35584 register dump:"TLF35584_ENDL);

    IfxTlf35584_Driver_invalidateCache(driver);

    for (address = 0; address < driver->cache.engine.ItemCount; address++)
    {
        if (Ifx_DeviceCache_isAddressValid(&driver->cache.engine, address))
        {
            char              text[255];
            IfxTlf35584_UData data;

            if (IfxTlf35584_Driver_readRegister(driver, address, &data))
            {
                IfxTlf35584_registerDump(address, data, text, 255);
                IfxTlf35584_Hal_print(text, address);
                IfxTlf35584_Hal_print(TLF35584_ENDL);
            }
            else
            {
                IfxTlf35584_Hal_print("Read error at 0x%2H"TLF35584_ENDL, address);
            }
        }
    }
#else
    (void) driver;
#pragma message  "WARNING: TLF35584 register dump not available when cache is disabled"
    IfxTlf35584_Hal_print("INFO: TLF35584 register dump not available when cache is disabled"TLF35584_ENDL);
#endif
}
