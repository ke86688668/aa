/**
 * \file IfxTlf35584_Driver.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup IfxLld_Tlf35584_Driver TLF35584 Driver
 * \ingroup IfxLldExt
 *
 * \image html "GcG_Logo_small.png" "GcG"
 *
 * The IfxTlf35584 driver provides an interface for handling the TLF35584 device.
 *
 * It is based on the \subpage doc_gcgBasedDriver "GcG device driver", and requires and
 * adaption of the HAL part (\ref IfxTlf35584_Hal.c & \ref IfxTlf35584_Hal.h) to the microcontroller
 * and framework used. By default the HAL is provided for the AURIX iLLD.
 *
 * The driver optionally supports the \ref library_srvsw_sysse_general_devicecache "experimental register caching feature" which
 * optimizes the SPI interface usage, by caching device registers to local RAM. The feature can be enabled / disabled with the \ref IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE switch.
 *
 * The driver is initialized with \ref IfxTlf35584_Driver_init() to which a configuration
 * structure is passed. This configuration structure must be set to default with
 * \ref IfxTlf35584_Driver_initConfig() before use.
 * Note that the SPI channel used must be initialized explicitly before calling \ref IfxTlf35584_Driver_init().
 *
 * Below an example of driver initialization:
 * \code
 * boolean result = TRUE;
 * IfxTlf35584_Driver_Config config;
 * IfxTlf35584_Driver driver;
 *
 * // Set default configuration values
 * IfxTlf35584_Driver_initConfig(&config);
 *
 * // HAL configuration
 * config.hal.channel = &g_Tlf35584.drivers.sscDeviceChannel.base;
 * config.hal.wdi = (IfxTlf35584_Pin) {&MODULE_P14, 3};
 *
 * // Global configuration
 * config.watchdogCycleTime = IfxTlf35584_RequestWatchdogCycleTime_100us;
 *
 * // Window watchdog configuration
 * config.windowWatchdog.enabled = TRUE;
 * config.windowWatchdog.useSpiTrigger = FALSE;
 * config.windowWatchdog.openWindowTime = 10e-3;
 * config.windowWatchdog.closedWindowTime = 35e-3;
 *
 * // Functional watchdog configuration
 * config.functionalWatchdog.enabled = TRUE;
 * config.functionalWatchdog.heartbeatTimerPeriod = 35e-3;
 * config.functionalWatchdog.servicePeriod = 25e-3;
 *
 * // FSP configuration
 * config.errPinMonitoring.enabled = TRUE;
 *
 * // Driver initialization
 * result &= IfxTlf35584_Driver_init(&driver, &config);
 *
 * // Go to NORMAL state
 * if (!IfxTlf35584_Driver_goToNormal(&driver, TRUE, TRUE, TRUE, TRUE))
 * { // Error while entering the NORMAL state
 *  result = FALSE;
 * }
 *
 * return result;
 * \endcode
 *
 * Once initialized, the watchdogs must be serviced by periodically by calling \ref IfxTlf35584_Driver_process().
 * \code
 * IfxTlf35584_Driver_process(&driver);
 * \endcode
 *
 *
 * All device features can be accessed through getter and setter API available in \ref IfxLld_Tlf35584_Std_Std.
 * The example below show how to test and clear the window watchdog error:
 * \code
 * if(IfxTlf35584_isWindowWatchdogErrorCounterOverflowFailureFlagSet(&driver, &result ))
 * {
 *     IfxTlf35584_clearWindowWatchdogErrorCounterOverflowFailureFlag(&driver, &result );
 * }
 * \endcode
 *
 * Dump of register can be done with \ref IfxTlf35584_Driver_dumpRegisters(), this feature is only available if the cache is enabled.
 * Below an example of register dump:
 * \code
 * TLF35584 register dump:
 * DEVCFG0 @ 0x0 = 0x8 = [TRDEL:0x8, WKTIMCYC:0x0, WKTIMEN:0x0]
 * DEVCFG1 @ 0x1 = 0x6 = [RESDEL:0x6]
 * DEVCFG2 @ 0x2 = 0x60 = [ESYNEN:0x0, ESYNPHA:0x0, CTHR:0x0, CMONEN:0x0, FRE:0x1, STU:0x1, EVCEN:0x0]
 * PROTCFG @ 0x3 = 0xCA = [KEY:0xCA]
 * SYSPCFG0 @ 0x4 = 0x1 = [STBYEN:0x1]
 * SYSPCFG1 @ 0x5 = 0x8 = [ERRREC:0x0, ERRRECEN:0x0, ERREN:0x1, ERRSLPEN:0x0, SS2DEL:0x0]
 * WDCFG0 @ 0x6 = 0x9C = [WDCYC:0x0, WWDTSEL:0x0, FWDEN:0x1, WWDEN:0x1, WWDETHR:0x9]
 * WDCFG1 @ 0x7 = 0x9 = [FWDETHR:0x9, WDSLPEN:0x0]
 * FWDCFG @ 0x8 = 0x6 = [WDHBTP:0x6]
 * WWDCFG0 @ 0x9 = 0x6 = [CW:0x6]
 * WWDCFG1 @ 0xA = 0x1 = [OW:0x1]
 * RSYSPCFG0 @ 0xB = 0x1 = [STBYEN:0x1]
 * RSYSPCFG1 @ 0xC = 0x8 = [ERRREC:0x0, ERRRECEN:0x0, ERREN:0x1, ERRSLPEN:0x0, SS2DEL:0x0]
 * RWDCFG0 @ 0xD = 0x9C = [WDCYC:0x0, WWDTSEL:0x0, FWDEN:0x1, WWDEN:0x1, WWDETHR:0x9]
 * RWDCFG1 @ 0xE = 0x9 = [FWDETHR:0x9, WDSLPEN:0x0]
 * RFWDCFG @ 0xF = 0x6 = [WDHBTP:0x6]
 * RWWDCFG0 @ 0x10 = 0x6 = [CW:0x6]
 * RWWDCFG1 @ 0x11 = 0x1 = [OW:0x1]
 * WKTIMCFG0 @ 0x12 = 0x0 = [TIMVALL:0x0]
 * WKTIMCFG1 @ 0x13 = 0x0 = [TIMVALM:0x0]
 * WKTIMCFG2 @ 0x14 = 0x0 = [TIMVALH:0x0]
 * DEVCTRL @ 0x15 = 0xE8 = [STATEREQ:0x0, VREFEN:0x1, COMEN:0x1, TRK1EN:0x1, TRK2EN:0x1]
 * DEVCTRLN @ 0x16 = 0x0 = [STATEREQ:0x0, VREFEN:0x0, COMEN:0x0, TRK1EN:0x0, TRK2EN:0x0]
 * WWDSCMD @ 0x17 = 0x0 = [TRIG:0x0, TRIG_STATUS:0x0]
 * FWDRSP @ 0x18 = 0x35 = [FWDRSP:0x35]
 * FWDRSPSYNC @ 0x19 = 0xC5 = [FWDRSPS:0xC5]
 * SYSFAIL @ 0x1A = 0x0 = [VOLTSELERR:0x0, OTF:0x0, VMONF:0x0, ABISTERR:0x0, INITF:0x0]
 * INITERR @ 0x1B = 0x78 = [VMONF:0x0, WWDF:0x1, FWDF:0x1, ERRF:0x1, SOFTRES:0x1, HARDRES:0x0]
 * IF @ 0x1C = 0x81 = [SYS:0x1, WK:0x0, SPI:0x0, MON:0x0, OTW:0x0, OTF:0x0, ABIST:0x0, INTMISS:0x1]
 * SYSSF @ 0x1D = 0x6 = [CFGE:0x0, WWDE:0x1, FWDE:0x1, ERRMISS:0x0, TRFAIL:0x0, NO_OP:0x0]
 * WKSF @ 0x1E = 0x0 = [WAK:0x0, ENA:0x0, CMON:0x0, WKTIM:0x0, WKSPI:0x0]
 * SPISF @ 0x1F = 0x0 = [PARE:0x0, LENE:0x0, ADDRE:0x0, DURE:0x0, LOCK:0x0]
 * MONSF0 @ 0x20 = 0x0 = [PREGSG:0x0, UCSG:0x0, STBYSG:0x0, VCORESG:0x0, COMSG:0x0, VREFSG:0x0, TRK1SG:0x0, TRK2SG:0x0]
 * MONSF1 @ 0x21 = 0x0 = [PREGOV:0x0, UCOV:0x0, STBYOV:0x0, VCOREOV:0x0, COMOV:0x0, VREFOV:0x0, TRK1OV:0x0, TRK2OV:0x0]
 * MONSF2 @ 0x22 = 0x0 = [PREGUV:0x0, UCUV:0x0, STBYUV:0x0, VCOREUV:0x0, COMUV:0x0, VREFUV:0x0, TRK1UV:0x0, TRK2UV:0x0]
 * MONSF3 @ 0x23 = 0x0 = [VBATOV:0x0, BG12UV:0x0, BG12OV:0x0, BIASLOW:0x0, BIASHI:0x0]
 * OTFAIL @ 0x24 = 0x0 = [PREG:0x0, UC:0x0, COM:0x0, MON:0x0]
 * OTWRNSF @ 0x25 = 0x0 = [PREG:0x0, UC:0x0, STDBY:0x0, COM:0x0, VREF:0x0]
 * VMONSTAT @ 0x26 = 0xF4 = [STBYST:0x1, VCOREST:0x0, COMST:0x1, VREFST:0x1, TRK1ST:0x1, TRK2ST:0x1]
 * DEVSTAT @ 0x27 = 0xFA = [STATE:0x2, VREFEN:0x1, STBYEN:0x1, COMEN:0x1, TRK1EN:0x1, TRK2EN:0x1]
 * PROTSTAT @ 0x28 = 0xF1 = [LOCK:0x1, KEY1OK:0x1, KEY2OK:0x1, KEY3OK:0x1, KEY4OK:0x1]
 * WWDSTAT @ 0x29 = 0x0 = [WWDECNT:0x0]
 * FWDSTAT0 @ 0x2A = 0x7B = [FWDQUEST:0xB, FWDRSPC:0x3, FWDRSPOK:0x1]
 * FWDSTAT1 @ 0x2B = 0x0 = [FWDECNT:0x0]
 * ABIST_CTRL0 @ 0x2C = 0x0 = [START:0x0, PATH:0x0, SINGLE:0x0, INT:0x0, STATUS:0x0]
 * ABIST_CTRL1 @ 0x2D = 0x0 = [OV_TRIG:0x0, ABIST_CLK_EN:0x0]
 * ABIST_SELECT0 @ 0x2E = 0x0 = [PREGOV:0x0, UCOV:0x0, STBYOV:0x0, VCOREOV:0x0, COMOV:0x0, VREFOV:0x0, TRK1OV:0x0, TRK2OV:0x0]
 * ABIST_SELECT1 @ 0x2F = 0x0 = [PREGUV:0x0, UCUV:0x0, STBYUV:0x0, VCOREUV:0x0, COMUV:0x0, VREFUV:0x0, TRK1UV:0x0, TRK2UV:0x0]
 * ABIST_SELECT2 @ 0x30 = 0x0 = [VBATOV:0x0, INTOV:0x0, BG12UV:0x0, BG12OV:0x0, BIASLOW:0x0, BIASHI:0x0]
 * BCK_FREQ_CHANGE @ 0x31 = 0x0 = [BCK_FREQ_SEL:0x0]
 * BCK_FRE_SPREAD @ 0x32 = 0x0 = [FRE_SP_THR:0x0]
 * BCK_MAIN_CTRL @ 0x33 = 0x0 = [DATA_VALID:0x0, BUSY:0x0]
 * GTM @ 0x3F = 0x2 = [TM:0x0, NTM:0x1]
 * \endcode
 *
 *
 * For debugging purpose, SPI frame exchanged with the device can be monitored using the \ref IFX_CFG_TLF35584_DEBUG_SPI switch.
 * The IFX_CFG_TLF35584_DEBUG switch can be used to monitor the program flow.
 *
 *
 * Below is an SPI log with <b>cache enabled</b> for the above initialization example:
 * \code
 * Initializing TLF35584
 * IfxTlf35584_Driver_init()
 * {
 * 	IfxTlf35584_Driver_unlock()
 * 	{
 * 		TLF35584 write 0xAB @ 0x03 (0x8756)->(0x8756)
 * 		TLF35584 write 0xEF @ 0x03 (0x87DE)->(0x87DE)
 * 		TLF35584 write 0x56 @ 0x03 (0x86AD)->(0x86AD)
 * 		TLF35584 write 0x12 @ 0x03 (0x8625)->(0x8625)
 * 		(0x5000)->(0x81E1) TLF35584 read 0xF0 @ 0x28
 * 	}
 *  (0x0C00)->(0x80C8) TLF35584 read 0x9B @ 0x06
 * 	(0x0E01)->(0x81ED) TLF35584 read 0x09 @ 0x07
 * 	(0x0A00)->(0x81EE) TLF35584 read 0x08 @ 0x05
 *
 * 	IfxTlf35584_Driver_lock()
 * 	{
 * 		TLF35584 write 0x01 @ 0x0A (0x9402)->(0x9402)
 * 		TLF35584 write 0x06 @ 0x09 (0x920D)->(0x920D)
 * 		TLF35584 write 0x06 @ 0x08 (0x900C)->(0x900C)
 *      TLF35584 write 0x09 @ 0x07 (0x8E12)->(0x8E12)
 *      TLF35584 write 0x9C @ 0x06 (0x8D39)->(0x8D39)
 *      TLF35584 write 0x08 @ 0x05 (0x8A10)->(0x8A10)
 * 		TLF35584 write 0x01 @ 0x04 (0x8803)->(0x8803)
 * 		TLF35584 write 0xDF @ 0x03 (0x87BE)->(0x87BE)
 * 		TLF35584 write 0x34 @ 0x03 (0x8668)->(0x8668)
 * 		TLF35584 write 0xBE @ 0x03 (0x877D)->(0x877D)
 * 		TLF35584 write 0xCA @ 0x03 (0x8795)->(0x8795)
 * 		(0x5000)->(0x81E2) TLF35584 read 0xF1 @ 0x28
 * 	}
 *
 * 	IfxTlf35584_Driver_checkResuestConfig()
 * 	{
 * 		(0x0801)->(0x81FC) TLF35584 read 0x01 @ 0x04
 * 		(0x0A00)->(0x81EE) TLF35584 read 0x08 @ 0x05
 * 		(0x0C00)->(0x80C7) TLF35584 read 0x9C @ 0x06
 * 		(0x0E01)->(0x81ED) TLF35584 read 0x09 @ 0x07
 * 		(0x1001)->(0x81F3) TLF35584 read 0x06 @ 0x08
 * 		(0x1200)->(0x81F3) TLF35584 read 0x06 @ 0x09
 * 		(0x1400)->(0x81FC) TLF35584 read 0x01 @ 0x0A
 * 	}
 *
 * 	IfxTlf35584_Driver_serviceWindowWatchdog()
 * 	{
 * 	}
 *
 * 	IfxTlf35584_Driver_serviceFunctionalWatchdog()
 * 	{
 * 		(0x5401)->(0x8061) TLF35584 read 0x30 @ 0x2A
 * 		TLF35584 write 0xFF @ 0x18 (0xB1FF)->(0xB1FF)
 * 		TLF35584 write 0x0F @ 0x18 (0xB01F)->(0xB01F)
 * 		TLF35584 write 0xF0 @ 0x18 (0xB1E1)->(0xB1E1)
 * 		TLF35584 write 0x00 @ 0x19 (0xB200)->(0xB200)
 * 	}
 * }
 *
 * IfxTlf35584_Driver_goToNormal()
 * {
 *  (0x2A01)->(0x8001) TLF35584 read 0x00 @ 0x15
 *  (0x2A01)->(0x8001) TLF35584 read 0x00 @ 0x15
 * 	TLF35584 write 0xEA @ 0x15 (0xABD5)->(0xABD5)
 * 	TLF35584 write 0x15 @ 0x16 (0xAC2B)->(0xAC2B)
 * 	(0x2A01)->(0x81D1) TLF35584 read 0xE8 @ 0x15
 * 	(0x4E00)->(0x81F5) TLF35584 read 0xFA @ 0x27
 * }
 *
 * \endcode
 * Below is an SPI log with <b>cache disabled</b> for the above initialization example:
 * \code
 * Initializing TLF35584
 * IfxTlf35584_Driver_init()
 * {
 * 	IfxTlf35584_Driver_unlock()
 * 	{
 * 		TLF35584 write 0xAB @ 0x03 (0x8756)->(0x8756)
 * 		TLF35584 write 0xEF @ 0x03 (0x87DE)->(0x87DE)
 * 		TLF35584 write 0x56 @ 0x03 (0x86AD)->(0x86AD)
 * 		TLF35584 write 0x12 @ 0x03 (0x8625)->(0x8625)
 * 		(0x5000)->(0x81E1) TLF35584 read 0xF0 @ 0x28
 * 	}
 *
 * 	(0x0C00)->(0x80C7) TLF35584 read 0x9C @ 0x06
 * 	TLF35584 write 0x9C @ 0x06 (0x8D39)->(0x8D39)
 * 	(0x0E01)->(0x81ED) TLF35584 read 0x09 @ 0x07
 * 	TLF35584 write 0x09 @ 0x07 (0x8E12)->(0x8E12)
 * 	TLF35584 write 0x01 @ 0x04 (0x8803)->(0x8803)
 * 	(0x0A00)->(0x81EE) TLF35584 read 0x08 @ 0x05
 * 	TLF35584 write 0x08 @ 0x05 (0x8A10)->(0x8A10)
 * 	TLF35584 write 0x06 @ 0x09 (0x920D)->(0x920D)
 * 	TLF35584 write 0x01 @ 0x0A (0x9402)->(0x9402)
 * 	(0x0C00)->(0x80C7) TLF35584 read 0x9C @ 0x06
 * 	TLF35584 write 0x9C @ 0x06 (0x8D39)->(0x8D39)
 * 	(0x0C00)->(0x80C7) TLF35584 read 0x9C @ 0x06
 * 	TLF35584 write 0x9C @ 0x06 (0x8D39)->(0x8D39)
 * 	(0x0C00)->(0x80C7) TLF35584 read 0x9C @ 0x06
 * 	TLF35584 write 0x9C @ 0x06 (0x8D39)->(0x8D39)
 * 	TLF35584 write 0x06 @ 0x08 (0x900C)->(0x900C)
 * 	(0x0C00)->(0x80C7) TLF35584 read 0x9C @ 0x06
 * 	TLF35584 write 0x9C @ 0x06 (0x8D39)->(0x8D39)
 * 	(0x0E01)->(0x81ED) TLF35584 read 0x09 @ 0x07
 * 	TLF35584 write 0x09 @ 0x07 (0x8E12)->(0x8E12)
 * 	(0x0A00)->(0x81EE) TLF35584 read 0x08 @ 0x05
 * 	TLF35584 write 0x08 @ 0x05 (0x8A10)->(0x8A10)
 * 	(0x0A00)->(0x81EE) TLF35584 read 0x08 @ 0x05
 * 	TLF35584 write 0x08 @ 0x05 (0x8A10)->(0x8A10)
 * 	(0x0A00)->(0x81EE) TLF35584 read 0x08 @ 0x05
 * 	TLF35584 write 0x08 @ 0x05 (0x8A10)->(0x8A10)
 * 	(0x0A00)->(0x81EE) TLF35584 read 0x08 @ 0x05
 * 	TLF35584 write 0x08 @ 0x05 (0x8A10)->(0x8A10)
 *
 * 	IfxTlf35584_Driver_lock()
 * 	{
 * 		TLF35584 write 0xDF @ 0x03 (0x87BE)->(0x87BE)
 * 		TLF35584 write 0x34 @ 0x03 (0x8668)->(0x8668)
 * 		TLF35584 write 0xBE @ 0x03 (0x877D)->(0x877D)
 * 		TLF35584 write 0xCA @ 0x03 (0x8795)->(0x8795)
 * 		(0x5000)->(0x81E2) TLF35584 read 0xF1 @ 0x28
 * 	}
 *
 * 	IfxTlf35584_Driver_checkResuestConfig() FEATURE NOT AVAILABLE WHEN CACHE DISBALED
 *
 * 	IfxTlf35584_Driver_serviceWindowWatchdog()
 * 	{
 * 	}
 *
 * 	IfxTlf35584_Driver_serviceFunctionalWatchdog()
 * 	{
 * 		(0x5401)->(0x8061) TLF35584 read 0x30 @ 0x2A
 * 		TLF35584 write 0xFF @ 0x18 (0xB1FF)->(0xB1FF)
 * 		TLF35584 write 0x0F @ 0x18 (0xB01F)->(0xB01F)
 * 		TLF35584 write 0xF0 @ 0x18 (0xB1E1)->(0xB1E1)
 * 		TLF35584 write 0x00 @ 0x19 (0xB200)->(0xB200)
 * 	}
 * }
 * IfxTlf35584_Driver_goToNormal()
 * {
 * 	(0x2A01)->(0x81D1) TLF35584 read 0xE8 @ 0x15
 * 	(0x2A01)->(0x81D1) TLF35584 read 0xE8 @ 0x15
 * 	TLF35584 write 0xEA @ 0x15 (0xABD5)->(0xABD5)
 * 	TLF35584 write 0x15 @ 0x16 (0xAC2B)->(0xAC2B)
 * 	(0x2A01)->(0x81D1) TLF35584 read 0xE8 @ 0x15
 * 	(0x4E00)->(0x81F5) TLF35584 read 0xFA @ 0x27
 * }
 *
 * \endcode
 *
 */

#ifndef  IFXTLF35584_DRIVER_H
#define  IFXTLF35584_DRIVER_H 1

#include "Ifx_Cfg.h"
#ifndef IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
#define IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE (FALSE)
#endif

#include "SysSe/Ext/Tlf35584/_Impl/IfxTlf35584_cfg.h"
#include "SysSe/Ext/Tlf35584/_Reg/IfxTlf35584_reg.h"
#include "SysSe/Ext/Tlf35584/Hal/IfxTlf35584_Hal.h"

#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
#include "SysSe/General/Ifx_DeviceCache.h"
#endif

typedef enum
{
    IfxTlf35584_status_notInitialized,  /**< \brief device driver is not initialized */
    IfxTlf35584_status_ready            /**< \brief device driver is ready */
}IfxTlf35584_status;
typedef struct
{
    IfxTlf35584_Spi *channel;
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
    struct
    {
        /* Device specific part */
        Ifx_TLF35584    cache;                                                       /**< \brief Cache of the device registers */
        Ifx_TLF35584    bufferNoOpMaskActual; /**< \brief Device no operation mask (actual mask). This buffer is modified. */
        uint32          modificationMask[((sizeof(Ifx_TLF35584) >> IFXTLF35584_ADDRESS_SHIFT)-1)/32+1];   /**< \brief register modification mask. If bit with offset x is 1, corresponding register address x has been modified and need to be written to the device */
        uint32          cacheValid[((sizeof(Ifx_TLF35584) >> IFXTLF35584_ADDRESS_SHIFT)-1)/32+1];         /**< \brief register cache valid. If bit with offset x is 1, corresponding register cache x has is valid */
        uint32          writeBackErrorFlags[((sizeof(Ifx_TLF35584) >> IFXTLF35584_ADDRESS_SHIFT)-1)/32+1];       /**< \brief Register write back error flag array. If bit with offset x is 1, corresponding register had an error during write cache back. Only valid if IfxTlf35584_Driver_isWriteSyncError() returns true */
        Ifx_DeviceCache engine;
        uint8           tempCacheLine[1];                                            /** Buffer for the temporary cache line */
    }cache;
#endif

    struct
    {
        IfxTlf35584_Pin wdi;                                /**< \brief WDI pin */
    }                  pins;
    IfxTlf35584_status status;
    boolean            useSpiTrigger;                       /**< \brief If TRUE, use the SPI to service the watchdog, else use the WDI Pin */
    IfxTlf35584_Time   windowWatchdogServiceRequest;        /**< \brief Window watchdog service request timeout */
    IfxTlf35584_Time   windowWatchdogServiceRequestRelease; /**< \brief Window watchdog service request timeout to release the WDI signal */
    IfxTlf35584_Time   windowWatchdogClosedWindowTime;      /**< \brief Window watchdog closed window time  */
    IfxTlf35584_Time   windowWatchdogOpenedWindowTime;      /**< \brief Window watchdog opened window time  */
    IfxTlf35584_Time   windowWatchdogMinDelay;              /**< \brief Minimal delay between WDI signal change  */

    IfxTlf35584_Time   functionalWatchdogServiceRequest;    /**< \brief Window watchdog service request timeout */
    IfxTlf35584_Time   functionalWatchdogPeriod;            /**< \brief Functional watchdog period */
}IfxTlf35584_Driver;

typedef struct
{
    IfxTlf35584_Hal_Config                                                 hal;                         /**< \brief HAL configuration */
    IfxTlf35584_RequestWatchdogCycleTime                                  watchdogCycleTime;           /**< \brief \ref WDCFG0::WDCYC Watchdog cycle time */
    IfxTlf35584_RequestWatchdogFunctionalityEnableWhileTheDeviceIsInSleep watchdogEnabledWhileInSleep; /**< \brief \ref WDCFG1::WDSLPEN Watchdog enable while the device is in SLEEP */

    /** \brief Window watchdog */
    struct
    {
        boolean enabled;                                           /**< \brief \ref WDCFG0.WWDEN      Window watchdog enable. If TRUE, the watchdog is enabled */
        float32 openWindowTime;                                    /**< \brief \ref WWDCFG1.OW        Window watchdog open window time in s */
        float32 closedWindowTime;                                  /**< \brief \ref WWDCFG1.CW        Window watchdog closed window time in s */
        boolean useSpiTrigger;                                     /**< \brief \ref WDCFG0.WWDTSEL    Window watchdog trigger selection. If TRUE, use the SPI to service the watchdog, else use the WDI pin */
        uint8   errorThreshold;                                    /**< \brief \ref WDCFG0.WWDETHR     Window watchdog error threshold */
    }windowWatchdog;
    struct
    {
        boolean enabled;                                           /**< \brief \ref WDCFG0.FWDEN      Functional watchdog enable. If TRUE, the watchdog is enabled */
        float32 heartbeatTimerPeriod;                              /**< \brief \ref FWDCFG.WDHBTP     Functional watchdog heartbeat timer period in s */
        float32 servicePeriod;                                     /**< \brief Functional watchdog service period in s */
        uint8   errorThreshold;                                    /**< \brief WDCFG1.FWDETHR         Functional watchdog error threshold */
    }functionalWatchdog;
    struct
    {
        IfxTlf35584_RequestErrPinMonitorRecoveryTime                                recoveryTime;        /**< \brief \ref SYSPCFG1.ERRREC   ERR pin monitor recovery time */
        IfxTlf35584_RequestErrPinMonitorRecoveryEnable                              recoveryEnabled;     /**< \brief \ref SYSPCFG1.ERRRECEN ERR pin monitor recovery enable */
        boolean                                                                      enabled;             /**< \brief \ref SYSPCFG0.ERREN    ERR pin monitor enabled. If true, the ERR pin monitoring is enabled  */
        IfxTlf35584_RequestErrPinMonitorFunctionailityEnableWhileTheSystemIsInSleep enabledWhileInSleep; /**< \brief \ref SYSPCFG0.ERRSLPEN ERR pin monitor enabled while the system is in SLEEP*/
    }                                             errPinMonitoring;
    IfxTlf35584_RequestStandbyRegulatorQstEnable standByQstEnabled;                                      /**< \brief \ref SYSPCFG0.STBYEN   Standby regulator QST enable */
    IfxTlf35584_RequestSafeState2Delay           safeState2Delay;                                        /**< \brief \ref SYSPCFG0.SS2DEL   Safe state 2 delay */
    boolean 		   showSpiFrame;						/**< \brief Debug feature available id IFX_CFG_TLF35584_DEBUG_SPI is set */
    boolean cacheEnabled;									/**< \brief Enable / disable the cache feature */
}IfxTlf35584_Driver_Config;

#if IFX_CFG_TLF35584_DEBUG_SPI
IFX_EXTERN boolean IfxTlf35584_g_showSpiFrame;
#endif

/** \addtogroup IfxLld_Tlf35584_Driver
 * \{ */

/** \name Main API
 * \{ */

/** Initialize the driver
 *
 * This API initialise the TLF35584 device driver.
 *
 * It also do the following:
 * - initialize the window watchdog
 * - initialize the functional watchdog
 * - initialize the ERR pin monitoring feature
 * - set the stand by regulator QST behaviour
 * - set the Safe state 2 delay
 * - Check the applied config (Check protected register content after lock)
 * - service the window and functional watchdog if enabled
 *
 * \note The SPI communication must be enable prior to the call to the IfxTlf35584_Driver_init() function
 *
 * \param driver Pointer to the driver object, will be initialized by the function
 * \param config Driver configuration
 * \return Returns TRUE in case of success, else FALSE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_init(IfxTlf35584_Driver *driver, IfxTlf35584_Driver_Config *config);

/** Initialize the configuration to default
 *
 * This function must be called prior to \ref IfxTlf35584_Driver_init()
 * \param config Driver configuration. Will be initialized by the function
 */
IFX_EXTERN void IfxTlf35584_Driver_initConfig(IfxTlf35584_Driver_Config *config);

/** read a register value either from cache or the device if the cache is not valid
 *
 * \param driver Pointer to the driver object
 * \param address Register address
 * \param data Returned register value
 *
 * \return Returns TRUE in case of success else false
 */
IFX_EXTERN boolean IfxTlf35584_Driver_readRegister(IfxTlf35584_Driver *driver, IfxTlf35584_Address address, uint8 *data);

/** Write to a register
 *
 * \param driver Pointer to the driver object
 * \param address Register address
 * \param data Register value to be written
 *
 * \return TRUE in case of successful else FALSE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_writeRegister(IfxTlf35584_Driver *driver, IfxTlf35584_Address address, uint8 data);

/** Write to a register with action write bitfield ('t' flag)
 *
 * \param driver Pointer to the driver object
 * \param address Register address
 * \param data Register value to be written
 * \param noOpMask Mask for the bifield set by this function
 *
 * \return TRUE in case of successful else FALSE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_writeRegisterStar(IfxTlf35584_Driver *driver, IfxTlf35584_Address address, IfxTlf35584_UData data, IfxTlf35584_UData noOpMask);


/** Return TRUE is a write syncronisation error occured
 *
 * \return TRUE is a write syncronisation error occured
 */
IFX_INLINE boolean IfxTlf35584_Driver_isWriteSyncError(IfxTlf35584_Driver *driver)
{
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
	return Ifx_DeviceCache_isWriteBackError(&driver->cache.engine);
#else
	return FALSE;
#endif
}

/** Unlock the protected registers
 *
 * \param driver Pointer to the driver object
 *
 * \return TRUE in case of successful else FALSE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_unlock(IfxTlf35584_Driver *driver);

/** Lock the protected registers
 *
 * \param driver Pointer to the driver object
 *
 * \return TRUE in case of successful else FALSE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_lock(IfxTlf35584_Driver *driver);

/** Set the device to NORMAL state
 *
 * \param driver Pointer to the driver object
 * \param tracker1Enabled If TRUE, the tracker 1 is enabled, else disabled
 * \param tracker2Enabled If TRUE, the tracker 2 is enabled, else disabled
 * \param CommunicationLdoEnabled If TRUE, the communication LDO is enabled, else disabled
 * \param referenceVoltageEnabled If TRUE, the reference voltage is enabled, else disabled
 *
 * \return Returns TRUE in case of success else FALSE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_goToNormal(IfxTlf35584_Driver *driver, boolean tracker1Enabled, boolean tracker2Enabled, boolean CommunicationLdoEnabled, boolean referenceVoltageEnabled);

/** Service the window watchdog
 * This function has to be called within the open window
 *
 * IfxTlf35584_Driver_releaseWindowWatchdogPin() must be called every time after the IfxTlf35584_Driver_serviceWindowWatchdogPin().
 * A delay of 3xTsam (3*213us=639) has must be respected between IfxTlf35584_Driver_serviceWindowWatchdogPin() and
 * IfxTlf35584_Driver_releaseWindowWatchdogPin() calls, either ways.
 *
 * \param driver Pointer to the driver object
 *
 * \return Returns TRUE in case of success else FALSE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_serviceWindowWatchdog(IfxTlf35584_Driver *driver);

/** Release the window watchdog trigger
 *
 * This function has to be called every time after the IfxTlf35584_Driver_serviceWindowWatchdogPin().
 * A delay of 3xTsam (3*213us=639us) has must be respected between IfxTlf35584_Driver_serviceWindowWatchdogPin() and
 * IfxTlf35584_Driver_releaseWindowWatchdogPin() calls, either ways.
 *
 * \param driver Pointer to the driver object
 *
 * \return Returns TRUE in case of success else FALSE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_releaseWindowWatchdogTrigger(IfxTlf35584_Driver *driver);

/** Service the functional watchdog
 *
 * \param driver Pointer to the driver object
 *
 * \return Returns TRUE in case of success else FALSE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_serviceFunctionalWatchdog(IfxTlf35584_Driver *driver);

/** Enable the window watchdog
 *
 * \param driver Pointer to the driver object
 * \param openWindowTime Open window time in second
 * \param closedWindowTime Closed window time in second
 * \param useSpiTrigger If TRUE, the SPI is used else the WDI Pin is used for watchdog triggering
 * \param cycleTime Window watchdog cycle time configuration
 *
 * \return Returns TRUE in case of success else FALSE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_enableWindowWatchdog(IfxTlf35584_Driver *driver, float32 openWindowTime, float32 closedWindowTime, boolean useSpiTrigger, IfxTlf35584_RequestWatchdogCycleTime cycleTime);

/** Disable the window watchdog
 *
 * \param driver Pointer to the driver object
 */
IFX_EXTERN boolean IfxTlf35584_Driver_disableWindowWatchdog(IfxTlf35584_Driver *driver);

/** Enable the functional watchdog and set the heartbeat period
 *
 * \param driver Pointer to the driver object
 * \param heartbeartTimerPeriod Heart beat timer period in s
 * \param servicePeriod service period in second
 * \param cycleTime Functional watchdog cycle time configuration
 */
IFX_EXTERN boolean IfxTlf35584_Driver_enableFunctionalWatchdog(IfxTlf35584_Driver *driver, float32 heartbeartTimerPeriod, float32 servicePeriod, IfxTlf35584_RequestWatchdogCycleTime cycleTime);

/** Return the functional watchdog service period
 *
 * \param driver Pointer to the driver object
 */
IFX_EXTERN float32 IfxTlf35584_Driver_getFunctionalWatchdogPeriod(IfxTlf35584_Driver *driver);

/** Return the window watchdog open window time
 *
 * Note the service is done in the middle of teh opened window time
 *
 * \param driver Pointer to the driver object
 */
IFX_EXTERN float32 IfxTlf35584_Driver_getWindowWatchdogOpenedWindow(IfxTlf35584_Driver *driver);

/** Return the window watchdog closed window time
 *
 * Note the service is done in the middle of teh opened window time
 *
 * \param driver Pointer to the driver object
 */
IFX_EXTERN float32 IfxTlf35584_Driver_getWindowWatchdogClosedWindow(IfxTlf35584_Driver *driver);

/** Disable the functional watchdog
 *
 * \param driver Pointer to the driver object
 */
IFX_EXTERN boolean IfxTlf35584_Driver_disableFunctionalWatchdog(IfxTlf35584_Driver *driver);

/** Handles the windows and functional watchdogs
 *
 * This function checks if the window or functional watchdog must be serviced,
 * based on deadlines, and service the watchdog if required. Therefore, is must
 * be called periodically to enable correct servicing.
 *
 */
IFX_EXTERN boolean IfxTlf35584_Driver_process(IfxTlf35584_Driver *driver);

/** Request state transition
 *
 * \param driver Pointer to the driver object
 * \param state Requested state
 * \param tracker1Enabled If TRUE, the tracker 1 is enabled, else disabled
 * \param tracker2Enabled If TRUE, the tracker 2 is enabled, else disabled
 * \param CommunicationLdoEnabled If TRUE, the communication LDO is enabled, else disabled
 * \param referenceVoltageEnabled If TRUE, the reference voltage is enabled, else disabled
 *
 * \return Returns TRUE in case of success else FALSE
 *
 */
IFX_EXTERN boolean IfxTlf35584_Driver_setStateRequest(IfxTlf35584_Driver *driver, IfxTlf35584_RequestForDeviceStateTransition state, boolean tracker1Enabled, boolean tracker2Enabled, boolean CommunicationLdoEnabled, boolean referenceVoltageEnabled);
/** \} */

/** \name Cache API
 * \{ */

/** Disable the cache
 *
 * write back the modified values to the device before disabling the cache
 *
 * \param driver Pointer to the driver object
 *
 * \return Returns FALSE if an error occurred during write back operation else TRUE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_disableCache(IfxTlf35584_Driver *driver);

/** Enable the cache
 *
 * \param driver Pointer to the driver object
 */
IFX_EXTERN void IfxTlf35584_Driver_enableCache(IfxTlf35584_Driver *driver);

/** Return the cache enable status
 *
 * \param driver Pointer to the driver object
 */
IFX_EXTERN boolean IfxTlf35584_Driver_isCacheEnabled(IfxTlf35584_Driver *driver);

/** Restore the device cache enable state
 *
 * \param driver Pointer to the driver object
 * \param enabled If true, the cache is enabled, else disabled
 *
 * \return Returns FALSE if an error occurred during write back operation else TRUE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_restoreCache(IfxTlf35584_Driver *driver, boolean enabled);

/** Invalidate the cache
 *
 * \param driver Pointer to the driver object
 */
IFX_INLINE void IfxTlf35584_Driver_invalidateCache(IfxTlf35584_Driver *driver);

/** Invalidate the cache (Cache for volatile registers only)
 *
 *
 * \param driver Pointer to the driver object. 
 */
IFX_INLINE void IfxTlf35584_Driver_invalidateCacheVolatile(IfxTlf35584_Driver *driver);

/** Synchronize the cache with the device (read)
 *
 * \param driver Pointer to the driver object
 *
 * \return Returns TRUE in case of success else FALSE
 */
IFX_EXTERN boolean IfxTlf35584_Driver_readSync(IfxTlf35584_Driver *driver);

/** Synchonize the cache with the device (write)
 *
 * \param driver Pointer to the driver object
 */
IFX_EXTERN boolean IfxTlf35584_Driver_writeSync(IfxTlf35584_Driver *driver);

/** \} */

/** \name Helper API
 * \{ */
/** Dump the Device registers using \ref IfxTlf35584_Hal_print()
 *
 * \param driver Pointer to the driver object
 */
IFX_EXTERN void IfxTlf35584_Driver_dumpRegisters(IfxTlf35584_Driver *driver);
/** \} */

/** \} */

IFX_INLINE void IfxTlf35584_Driver_invalidateCache(IfxTlf35584_Driver *driver)
{
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
    Ifx_DeviceCache_invalidate(&driver->cache.engine);
#else
    (void)driver;
#endif
}


IFX_INLINE void IfxTlf35584_Driver_invalidateCacheVolatile(IfxTlf35584_Driver *driver)
{
#if IFX_CFG_TLF35584_DRIVER_CACHE_ENABLE
    Ifx_DeviceCache_invalidateVolatile(&driver->cache.engine);
#else
    (void)driver;
#endif
}

/** Show the SPI frames (Debug feature)
 *
 * Only available if IFX_CFG_TLF35584_DEBUG_SPI is set
 */
IFX_INLINE void IfxTlf35584_Driver_showSpiFrame(IfxTlf35584_Driver *driver)
{
#if IFX_CFG_TLF35584_DEBUG_SPI
	IfxTlf35584_g_showSpiFrame = TRUE;
#endif
}

/** Show the SPI frames (Debug feature)
 *
 * Only available if IFX_CFG_TLF35584_DEBUG_SPI is set
 */
IFX_INLINE void IfxTlf35584_Driver_hideSpiFrame(IfxTlf35584_Driver *driver)
{
#if IFX_CFG_TLF35584_DEBUG_SPI
	IfxTlf35584_g_showSpiFrame = FALSE;
#endif
}

#endif /*  IFXTLF35584_DRIVER_H */
