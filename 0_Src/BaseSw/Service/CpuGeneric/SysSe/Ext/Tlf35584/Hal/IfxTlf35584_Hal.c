/**
 * \file IfxTlf35584_Hal.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 */
#include "SysSe/Ext/Tlf35584/Hal/IfxTlf35584_Hal.h"
#include "SysSe/Ext/Tlf35584/Driver/IfxTlf35584_Driver.h"
#include "SysSe/Bsp/Bsp.h"

#include "SysSe/Comm/Ifx_Console.h"
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

boolean IfxTlf35584_Hal_exchangeSpi(IfxTlf35584_Spi *spiChannel, const void *src, void *dest, uint8 length)
{
    boolean result = TRUE;
#if IFX_CFG_TLF35584_DEBUG_SPI
    if (IfxTlf35584_g_showSpiFrame)
    {
        uint16 d = *(uint16 *)src;
        IfxTlf35584_Hal_print(" (0x%04X)", d);
    }
#endif

    result &= SpiIf_exchange(spiChannel, src, dest, length) == SpiIf_Status_ok;
    if (result)
    {
        result &= SpiIf_waitWithTimeout(spiChannel, IFXTLF35584_TIME_10MS);
    }
#if IFX_CFG_TLF35584_DEBUG_SPI
    if (IfxTlf35584_g_showSpiFrame)
    {
        uint16 d = *(uint16 *)dest;
        IfxTlf35584_Hal_print("->(0x%04X)", d);
    }
#endif
    return result;
}


void IfxTlf35584_Hal_initPinOut(IfxTlf35584_Pin *pin, IfxPort_Mode mode, IfxPort_PadDriver driver, IfxTlf35584_PinState state)
{
    IfxPort_setPinMode(pin->port, pin->pinIndex, mode);
    IfxPort_setPinPadDriver(pin->port, pin->pinIndex, driver);
    IfxTlf35584_Hal_setPortState(pin, state);
}


void IfxTlf35584_Hal_initPinInp(IfxTlf35584_Pin *pin, IfxPort_Mode mode)
{
    IfxPort_setPinMode(pin->port, pin->pinIndex, mode);
}


boolean IfxTlf35584_Hal_init(IfxTlf35584_Hal_Config *config)
{
    boolean result = TRUE;
    if (config->wdi.port != NULL_PTR)
    {
    	IfxTlf35584_Hal_initPinOut(&config->wdi, IfxPort_Mode_outputPushPullGeneral, IfxPort_PadDriver_cmosAutomotiveSpeed1, IfxTlf35584_PinState_low);
    }

    return result;
}


void IfxTlf35584_Hal_initConfig(IfxTlf35584_Hal_Config *config)
{
    config->channel = NULL_PTR;
    config->wdi     = (IfxTlf35584_Pin) {NULL_PTR, 0};
}


boolean IfxTlf35584_Hal_print(pchar format, ...)
{
    if (!Ifx_g_console.standardIo->txDisabled)
    {
        char      message[STDIF_DPIPE_MAX_PRINT_SIZE + 1];
        Ifx_SizeT count;
        va_list   args;
        va_start(args, format);
        vsprintf((char *)message, format, args);
        va_end(args);
        count = (Ifx_SizeT)strlen(message);
        IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, count < STDIF_DPIPE_MAX_PRINT_SIZE);

        return IfxStdIf_DPipe_write(Ifx_g_console.standardIo, (void *)message, &count, TIME_INFINITE);
    }
    else
    {
        return TRUE;
    }
}
