/*
 * @file TLE5012.h
 * @brief TLE5012 iGMR sensor interface.
 * \version disabled
 * \copyright Copyright (c) 2016 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.

 * \defgroup tle5012 TLE5012 iGMR sensor interface
 *
 * @ingroup library
 *
 */

#ifndef IFXTLE5012_DRIVER_H
#define IFXTLE5012_DRIVER_H 1

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "TLE5012_regs.h"
#include "If/SpiIf.h"
#include "StdIf/IfxStdIf_Pos.h"
#include "SysSe/Bsp/Bsp.h"
#include "SysSe/Math/Ifx_LowPassPt1F32.h"


/** @brief Angle range, from 0x4000 (-180deg) .. 0 (0deg) .. 0x3FFF(179.99deg) */
#define IFX_TLE5012_DRIVER_ANGLE_RANGE             (32768)


/******************************************************************************/
/*------------------------------Type Definitions------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*-----------------------------Data Structures--------------------------------*/
/******************************************************************************/



/** \brief TLE5012 object
 */
typedef struct
{
    sint32                  rawPosition;                  /**< \brief raw position in ticks. \note: the value already contains the offset */
    float32                 speed;                        /**< \brief mechanical speed in rad/s */
    sint32                  turn;                         /**< \brief number of mechanical turns */
    IfxStdIf_Pos_Dir        direction;                    /**< \brief rotation direction */
    IfxStdIf_Pos_Status     status;                       /**< \brief error code (0 = no error) */
    sint32                  offset;                       /**< \brief raw position offset */
    boolean                    reversed;
    sint32                  resolution;                   /**< \brief resolution of this position sensor interface */
    float32                 updatePeriod;                 /**< \brief update period in seconds */
    float32                 speedConstPulseCount;         /**< \brief constant for calculating mechanical speed (in rad/s) from raw speed in pulse count mode */
    float32                 positionConst;                /**< \brief constant for calculating mechanical position (in rad) from raw position */
    Ifx_TLE5012           registers;          /**< Copy of TLE5012 register values read through SPI */

    SpiIf_Ch         *channel;
    IfxPort_Pin                chipSelect;         /**< Pin to drive chip select signal */
    IfxPort_Pin                slaveReceive;       /**< Pin to drive slave receive signal */
    uint16              buffer[16];
    uint16 				dummyBuffer;		 /**< \brief Dummy buffer used for SPI data transfer */
    sint32               errorCrc;           /**< Count of detected CRC error */
    sint32               errorTx;            /**< Count of detected transmission error */
    uint32 				 updateCounter;		 /**< Update counter use to average the comm error per second */
    uint32 				 updateCounterLimit; /**< Update counter use to average the comm error per second */
    uint32 				 updateErrorCounter; /**< Update counter use to average the comm error per second */
    uint32 				 updateErrorCounterThreshold; /**< Update counter use to average the comm error per second */

} IfxTle5012_Driver;

/** \brief Configuration structure for TLE5012
 */
typedef struct
{
    sint32                   offset;
    boolean                    reversed;
    IfxPort_Pin   chipSelect;         /**< @brief Resolver chip select */
    IfxPort_Pin                    slaveReceive;       /**< Pin to drive slave receive signal */
    float32 		updatePeriod;	/* iGMR update period in s (Period at which IfxTle5012_Driver_update() is called)*/
    SpiIf_Ch   *channel;
}IfxTle5012_Driver_Config;


/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \brief Initialises the TLE5012 interface
 * \param driver TLE5012 interface Handle
 * \param config Configuration structure for TLE5012
 * \return TRUE on success else FALSE
 *
 *
 */
IFX_EXTERN boolean IfxTle5012_Driver_init(IfxTle5012_Driver *driver, const IfxTle5012_Driver_Config* config);

/** \brief Initializes the configuration structure to default
 * \param config Configuration structure for the TLE5012
 * \return None
 *
 */
IFX_EXTERN void IfxTle5012_Driver_initConfig(IfxTle5012_Driver_Config* config);


IFX_EXTERN void IfxTle5012_Driver_reset(IfxTle5012_Driver* driver);

IFX_EXTERN void IfxTle5012_Driver_sscUpdatePosition(IfxTle5012_Driver* driver);
IFX_EXTERN void IfxTle5012_Driver_sscUpdateSpeed(IfxTle5012_Driver* driver, sint32 newPosition);

IFX_EXTERN boolean IfxTle5012_Driver_sscReadConfigRegisters(IfxTle5012_Driver* driver);
IFX_EXTERN boolean IfxTle5012_Driver_sscReadStatusRegisters(IfxTle5012_Driver* driver);
IFX_EXTERN boolean IfxTle5012_Driver_sscReadAngularRegisters(IfxTle5012_Driver* driver);
/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \brief \see IfxStdIf_Pos_GetAbsolutePosition
 * \param driver driver handle
 * \return absolute position
 */
IFX_EXTERN float32 IfxTle5012_Driver_getAbsolutePosition(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_GetDirection
 * \param driver driver handle
 * \return direction
 */
IFX_EXTERN IfxStdIf_Pos_Dir IfxTle5012_Driver_getDirection(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_GetFault
 * \param driver driver handle
 * \return Fault
 */
IFX_EXTERN IfxStdIf_Pos_Status IfxTle5012_Driver_getFault(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_GetOffset
 * \param driver driver handle
 * \return offset
 */
IFX_EXTERN sint32 IfxTle5012_Driver_getOffset(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_GetPeriodPerRotation
 * \param driver driver handle
 * \return Period per rotation
 */
IFX_EXTERN uint16 IfxTle5012_Driver_getPeriodPerRotation(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_GetPosition
 * \param driver driver handle
 * \return position
 */
IFX_EXTERN float32 IfxTle5012_Driver_getPosition(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_GetRawPosition
 * \param driver driver handle
 * \return position in ticks
 */
IFX_EXTERN sint32 IfxTle5012_Driver_getRawPosition(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_GetRefreshPeriod
 * \param driver driver handle
 * \return update period
 */
IFX_EXTERN float32 IfxTle5012_Driver_getRefreshPeriod(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_GetResolution
 * \param driver driver handle
 * \return resolution
 */
IFX_EXTERN sint32 IfxTle5012_Driver_getResolution(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_GetSensorType
 * \param driver driver handle
 * \return sensor type
 */
IFX_EXTERN IfxStdIf_Pos_SensorType IfxTle5012_Driver_getSensorType(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_GetSpeed
 * \param driver driver handle
 * \return speed
 */
IFX_EXTERN float32 IfxTle5012_Driver_getSpeed(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_GetTurn
 * \param driver driver handle
 * \return the number of turns
 */
IFX_EXTERN sint32 IfxTle5012_Driver_getTurn(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_Reset
 * \param driver driver handle
 * \return None
 */
IFX_EXTERN void IfxTle5012_Driver_reset(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_ResetFaults
 * \param driver driver handle
 * \return None
 */
IFX_EXTERN void IfxTle5012_Driver_resetFaults(IfxTle5012_Driver *driver);

/** \brief \see IfxStdIf_Pos_SetOffset
 * \param driver driver handle
 * \param offset offset
 * \return None
 */
IFX_EXTERN void IfxTle5012_Driver_setOffset(IfxTle5012_Driver *driver, sint32 offset);

/** \brief \see IfxStdIf_Pos_SetRefreshPeriod
 * \param driver driver handle
 * \param updatePeriod update period
 * \return None
 */
IFX_EXTERN void IfxTle5012_Driver_setRefreshPeriod(IfxTle5012_Driver *driver, float32 updatePeriod);

/** \brief \see IfxStdIf_Pos_Update
 * \param driver driver handle
 * \return None
 */
IFX_EXTERN void IfxTle5012_Driver_update(IfxTle5012_Driver *driver);

/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \brief Initializes the standard interface "Pos"
 * \param stdif Standard interface position object
 * \param driver TLE5012 interface Handle
 * \return TRUE on success else FALSE
 */
IFX_EXTERN boolean IfxTle5012_Driver_stdIfPosInit(IfxStdIf_Pos *stdif, IfxTle5012_Driver *driver);

#endif /* IFXTLE5012_DRIVER_H */

