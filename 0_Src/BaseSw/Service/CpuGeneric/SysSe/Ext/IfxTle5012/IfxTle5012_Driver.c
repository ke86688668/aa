/**
 * @file TLE5012.c
 * @brief TLE5012 iGMR sensor interface.
 *
 * \version disabled
 * \copyright Copyright (c) 2016 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "IfxTle5012_Driver.h"
#include "string.h"

/******************************************************************************/
/*-----------------------Private Function Prototypes--------------------------*/
/******************************************************************************/

static uint8 IfxTle5012_Driver_Crc8Calculate(const void* data, uint16 count);


/******************************************************************************/
/*-------------------------Function Implementations---------------------------*/
/******************************************************************************/
float32 IfxTle5012_Driver_getAbsolutePosition(IfxTle5012_Driver *driver)
{
    return ((float32)driver->turn + (float32)driver->rawPosition / (float32)driver->resolution) * 2.0 * IFX_PI;
}


IfxStdIf_Pos_Dir IfxTle5012_Driver_getDirection(IfxTle5012_Driver *driver)
{
    return driver->direction;
}


IfxStdIf_Pos_Status IfxTle5012_Driver_getFault(IfxTle5012_Driver *driver)
{
    return driver->status;
}


sint32 IfxTle5012_Driver_getOffset(IfxTle5012_Driver *driver)
{
    return driver->offset;
}


uint16 IfxTle5012_Driver_getPeriodPerRotation(IfxTle5012_Driver *driver)
{
    return 1; /* Period per rotation is 1*/
}


float32 IfxTle5012_Driver_getPosition(IfxTle5012_Driver *driver)
{
    return (float32)driver->rawPosition * driver->positionConst;
}


sint32 IfxTle5012_Driver_getRawPosition(IfxTle5012_Driver *driver)
{
    return driver->rawPosition;
}


float32 IfxTle5012_Driver_getRefreshPeriod(IfxTle5012_Driver *driver)
{
    return driver->updatePeriod;
}


sint32 IfxTle5012_Driver_getResolution(IfxTle5012_Driver *driver)
{
    return driver->resolution;
}


IfxStdIf_Pos_SensorType IfxTle5012_Driver_getSensorType(IfxTle5012_Driver *driver)
{
    return IfxStdIf_Pos_SensorType_igmr;
}


float32 IfxTle5012_Driver_getSpeed(IfxTle5012_Driver *driver)
{
    return driver->speed;
}


sint32 IfxTle5012_Driver_getTurn(IfxTle5012_Driver *driver)
{
    return driver->turn;
}


boolean IfxTle5012_Driver_init(IfxTle5012_Driver* driver, const IfxTle5012_Driver_Config* config)
{
    boolean Result = TRUE;


    /*< HARDWARE INITIALISATION > --------------------------------------------*/
    /* Override CS pin, this is required to enable the "slave receive" update */
    driver->chipSelect = config->chipSelect;

    driver->offset               = config->offset;
    driver->resolution                 = IFX_TLE5012_DRIVER_ANGLE_RANGE;
    driver->positionConst      = 1.0 / (float32)driver->resolution * 2.0 * IFX_PI;
    driver->reversed                 = config->reversed;

    IfxTle5012_Driver_setRefreshPeriod(driver, config->updatePeriod);

    driver->status.status            = 0;
    driver->rawPosition              = 0;
    driver->speed                    = 0;
    driver->direction                = IfxStdIf_Pos_Dir_unknown;
    driver->turn                     = 0;

    driver->errorCrc             = 0;
    driver->errorTx              = 0;

    driver->channel = config->channel;

    IfxPort_setPinState(driver->chipSelect.port, driver->chipSelect.pinIndex , IfxPort_State_low);
    IfxPort_setPinMode(driver->chipSelect.port, driver->chipSelect.pinIndex, IfxPort_Mode_outputPushPullGeneral);
    IfxPort_setPinPadDriver(driver->chipSelect.port, driver->chipSelect.pinIndex, IfxPort_PadDriver_cmosAutomotiveSpeed1);


    driver->slaveReceive         = config->slaveReceive;
    IfxPort_setPinState(driver->slaveReceive.port, driver->slaveReceive.pinIndex , IfxPort_State_low);
    IfxPort_setPinMode(driver->slaveReceive.port, driver->slaveReceive.pinIndex, IfxPort_Mode_outputPushPullGeneral);
    IfxPort_setPinPadDriver(driver->slaveReceive.port, driver->slaveReceive.pinIndex, IfxPort_PadDriver_cmosAutomotiveSpeed1);

    driver->updateCounterLimit = 1.0 / driver->updatePeriod; /* Assumes the update period is <= 1 second*/
    driver->updateCounter = driver->updateCounterLimit;
    driver->updateErrorCounter = 0;
    driver->updateErrorCounterThreshold = 5; /* Allow up to 5 error per second FIXME make it configurable */


    return Result;
}

void IfxTle5012_Driver_initConfig(IfxTle5012_Driver_Config* config)
{
	config->chipSelect = (IfxPort_Pin){NULL_PTR, 0};
	config->offset = 0;
	config->reversed = FALSE;
	config->slaveReceive = (IfxPort_Pin){NULL_PTR, 0};
	config->channel = NULL_PTR;
	config->updatePeriod = 10e-3;
}



void IfxTle5012_Driver_reset(IfxTle5012_Driver *driver)
{
    driver->rawPosition              = 0;
    driver->turn                     = 0;
    driver->speed                    = 0;
    driver->status.status            = 0;
	driver->errorCrc             = 0;
	driver->errorTx              = 0;
}


void IfxTle5012_Driver_resetFaults(IfxTle5012_Driver *driver)
{
    driver->status.status            = 0;
}


void IfxTle5012_Driver_setOffset(IfxTle5012_Driver *driver, sint32 offset)
{
	while (offset <0)
	{ /* Make sure the offset is positive */
		offset += driver->resolution;
	}
    driver->rawPosition = driver->rawPosition - driver->offset + offset;

    driver->offset = offset;
}


void IfxTle5012_Driver_setRefreshPeriod(IfxTle5012_Driver *driver, float32 updatePeriod)
{
    driver->updatePeriod           = updatePeriod;
    driver->speedConstPulseCount   = (2.0 * IFX_PI) / driver->resolution / updatePeriod;
}


boolean IfxTle5012_Driver_stdIfPosInit(IfxStdIf_Pos *stdif, IfxTle5012_Driver *driver)
{
    /* Ensure the stdif is reset to zeros */
    memset(stdif, 0, sizeof(IfxStdIf_Pos));

    /* Set the driver */
    stdif->driver = driver;

    /* *INDENT-OFF* Note: this file was indented manually by the author. */
    /* Set the API link */
	stdif->onZeroIrq          =(IfxStdIf_Pos_OnZeroIrq               )NULL_PTR;
	stdif->getAbsolutePosition=(IfxStdIf_Pos_GetAbsolutePosition     )&IfxTle5012_Driver_getAbsolutePosition;
	stdif->getDirection		  =(IfxStdIf_Pos_GetDirection            )&IfxTle5012_Driver_getDirection;
	stdif->getFault           =(IfxStdIf_Pos_GetFault                )&IfxTle5012_Driver_getFault;
	stdif->getOffset		  =(IfxStdIf_Pos_GetOffset			     )&IfxTle5012_Driver_getOffset;
	stdif->getPeriodPerRotation  =(IfxStdIf_Pos_GetPeriodPerRotation )&IfxTle5012_Driver_getPeriodPerRotation;
	stdif->getPosition		  =(IfxStdIf_Pos_GetPosition			 )&IfxTle5012_Driver_getPosition;
	stdif->getRawPosition	  =(IfxStdIf_Pos_GetRawPosition          )&IfxTle5012_Driver_getRawPosition;
	stdif->getRefreshPeriod   =(IfxStdIf_Pos_GetRefreshPeriod        )&IfxTle5012_Driver_getRefreshPeriod;
	stdif->getResolution      =(IfxStdIf_Pos_GetResolution           )&IfxTle5012_Driver_getResolution;
    stdif->getSensorType      =(IfxStdIf_Pos_GetSensorType           )&IfxTle5012_Driver_getSensorType;
	stdif->reset			  =(IfxStdIf_Pos_Reset				     )&IfxTle5012_Driver_reset;
	stdif->resetFaults		  =(IfxStdIf_Pos_ResetFaults			 )&IfxTle5012_Driver_resetFaults;
	stdif->getSpeed           =(IfxStdIf_Pos_GetSpeed                )&IfxTle5012_Driver_getSpeed;
	stdif->update			  =(IfxStdIf_Pos_Update				     )&IfxTle5012_Driver_update;
	stdif->setOffset		  =(IfxStdIf_Pos_SetOffset			     )&IfxTle5012_Driver_setOffset;
	stdif->setRefreshPeriod   =(IfxStdIf_Pos_SetRefreshPeriod        )&IfxTle5012_Driver_setRefreshPeriod;
	stdif->getTurn            =(IfxStdIf_Pos_GetTurn                 )&IfxTle5012_Driver_getTurn;
    /* *INDENT-ON* */

    return TRUE;
}

void IfxTle5012_Driver_update(IfxTle5012_Driver *driver)
{
	boolean result;

	if (driver->updateCounter == 0)
	{
		driver->updateCounter = driver->updateCounterLimit;
		driver->updateErrorCounter = 0;
	}
	driver->updateCounter--;

	result = IfxTle5012_Driver_sscReadStatusRegisters(driver);
	if (result)
	{
		driver->status.B.signalLoss |= driver->registers.I.STAT.B.S_MAGOL == 1;
		driver->status.B.signalDegradation |= driver->registers.I.STAT.B.S_XYOL == 1;
	    IfxTle5012_Driver_sscUpdatePosition(driver);
	}
	else
	{
		driver->updateErrorCounter++;
		if (driver->updateErrorCounter > driver->updateErrorCounterThreshold)
		{
			driver->status.B.commErrorThresholdReached = 1;
		}
	}
}

/* <OPERATIONAL FUNCTIONS> ================================================== */

#define IFX_TLE5012_DRIVER_ANGULAR_OFFSET (0x02U)
#define IFX_TLE5012_DRIVER_CONFIG_OFFSET  (0x05U)
#define IFX_TLE5012_DRIVER_STATUS_OFFSET  (0x00U)

#define IFX_TLE5012_DRIVER_ACCESS_KEY      0x0U
#define IFX_TLE5012_DRIVER_CONFIG_KEY      0xAU



typedef union
{
    struct
    {
        uint16 NumDat   :4;
        uint16 Address  :6;
        uint16 Update   :1;
        uint16 Lock     :4;
        uint16 Read     :1;
    } B;
    uint16 U;
} IfxTle5012_Driver_CommandWord;


IFX_INLINE void IfxTle5012_Driver_sscActivateChipSelect(IfxTle5012_Driver* driver)
{
    IfxPort_setPinState(driver->chipSelect.port, driver->chipSelect.pinIndex , IfxPort_State_high); /* Inverter gate on PCB */
}

IFX_INLINE void IfxTle5012_Driver_sscActivateSlaveReceive(IfxTle5012_Driver* driver)
{
    IfxPort_setPinState(driver->slaveReceive.port, driver->slaveReceive.pinIndex , IfxPort_State_high);
}

IFX_INLINE void IfxTle5012_Driver_sscDeactivateChipSelect(IfxTle5012_Driver* driver)
{
    IfxPort_setPinState(driver->chipSelect.port, driver->chipSelect.pinIndex , IfxPort_State_low);
}

IFX_INLINE void IfxTle5012_Driver_sscDeactivateSlaveReceive(IfxTle5012_Driver* driver)
{
    IfxPort_setPinState(driver->slaveReceive.port, driver->slaveReceive.pinIndex , IfxPort_State_low);
}


static boolean IfxTle5012_Driver_sscSendReadCommand(IfxTle5012_Driver* driver, uint16 accessCmd, uint16 offset, uint16 numData)
{
    boolean result;

	uint16* mbuffer = driver->buffer;
	driver->errorCrc = 0;
	driver->errorTx = 0;

	IfxTle5012_Driver_CommandWord* commandWord = (IfxTle5012_Driver_CommandWord*)&mbuffer[0];

	commandWord->B.Read = 1;
	commandWord->B.Lock = accessCmd;
	commandWord->B.Address = offset;
	commandWord->B.NumDat = numData;


	/* send command word */
	IfxTle5012_Driver_sscActivateChipSelect(driver);
	IfxTle5012_Driver_sscActivateSlaveReceive(driver);
	result = SpiIf_exchange(driver->channel, &mbuffer[0], &driver->dummyBuffer, 1) == SpiIf_Status_ok;


	if (result == FALSE)
	{   /* can't request transmission */
	    driver->errorTx++;
	}

    return result;
}

static void IfxTle5012_Driver_sscReceiveDataAndCrc(IfxTle5012_Driver* driver)
{
    boolean result;
    uint16* mbuffer = driver->buffer;

    {   /* continue with data exchange */
        uint16  numData = ((IfxTle5012_Driver_CommandWord*)mbuffer)->B.NumDat;

#if 1
        {
            /* Set the send data to 0, not required but clean signal output  */
        	uint32 i;
        	for (i=0;i<numData;i++)
        	{
        		driver->buffer[1+i+1] = 0;
        	}

        }

#endif
        /* receive data + CRC from driver */
        IfxTle5012_Driver_sscDeactivateSlaveReceive(driver);
    	result = SpiIf_exchange(driver->channel, &mbuffer[1], &mbuffer[1], numData + 1) == SpiIf_Status_ok;

        if (result == FALSE)
        {
            driver->errorTx++;
        }
    }
}

static void IfxTle5012_Driver_sscProcessChecksum(IfxTle5012_Driver* driver)
{
    uint16* mbuffer = driver->buffer;
    uint16  numData = ((IfxTle5012_Driver_CommandWord*)mbuffer)->B.NumDat;
    uint16  address = ((IfxTle5012_Driver_CommandWord*)mbuffer)->B.Address;

    {   /* continue process checksum */
        uint8 crc = IfxTle5012_Driver_Crc8Calculate(&mbuffer[0], 2 * (numData + 1));

        if (crc != (mbuffer[numData + 1] & 0xFF))
        {
            driver->errorCrc++;
        }
        else
        {
            uint16 i;
            /* copy register values */
            for (i = 0; i < numData; i++)
            {
                driver->registers.A[i + address] = mbuffer[i + 1];
            }
        }
    }

    IfxTle5012_Driver_sscDeactivateChipSelect(driver);
}

boolean IfxTle5012_Driver_sscReadConfigRegisters(IfxTle5012_Driver* driver)
{
    boolean Result;
    Result = IfxTle5012_Driver_sscSendReadCommand(driver, IFX_TLE5012_DRIVER_CONFIG_KEY, IFX_TLE5012_DRIVER_CONFIG_OFFSET, 13);

    SpiIf_wait(driver->channel);
    IfxTle5012_Driver_sscReceiveDataAndCrc(driver);
    SpiIf_wait(driver->channel);
    IfxTle5012_Driver_sscProcessChecksum(driver);

    Result &= ((driver->errorCrc + driver->errorTx) == 0);
    driver->status.B.commError |= Result == 0;
    return Result;
}


boolean IfxTle5012_Driver_sscReadStatusRegisters(IfxTle5012_Driver* driver)
{
    boolean Result;
    Result = IfxTle5012_Driver_sscSendReadCommand(driver, IFX_TLE5012_DRIVER_ACCESS_KEY, IFX_TLE5012_DRIVER_STATUS_OFFSET, 5);

    SpiIf_wait(driver->channel);
    IfxTle5012_Driver_sscReceiveDataAndCrc(driver);
    SpiIf_wait(driver->channel);
    IfxTle5012_Driver_sscProcessChecksum(driver);

    Result &= ((driver->errorCrc + driver->errorTx) == 0);
    driver->status.B.commError |= Result == 0;
    return Result;
}


boolean IfxTle5012_Driver_sscReadAngularRegisters(IfxTle5012_Driver* driver)
{
    boolean Result;
    Result = IfxTle5012_Driver_sscSendReadCommand(driver, IFX_TLE5012_DRIVER_ACCESS_KEY, IFX_TLE5012_DRIVER_ANGULAR_OFFSET, 3);

    SpiIf_wait(driver->channel);
    IfxTle5012_Driver_sscReceiveDataAndCrc(driver);
    SpiIf_wait(driver->channel);
    IfxTle5012_Driver_sscProcessChecksum(driver);

    Result &= ((driver->errorCrc + driver->errorTx) == 0);
    driver->status.B.commError |= Result == 0;
    return Result;
}


void IfxTle5012_Driver_sscUpdatePosition(IfxTle5012_Driver* driver)
{
    sint32 newPosition = (driver->registers.I.AVAL.B.ANG_VAL);
    newPosition         = driver->reversed ? driver->resolution - newPosition : newPosition;
    newPosition         += driver->offset;
    newPosition         &= driver->resolution - 1; /* Assumes sum is positive */


    {
        sint32 R2 = driver->resolution << 1;
        if (newPosition > driver->rawPosition)
        {
            if (((newPosition - driver->rawPosition) < R2))
            {
            	driver->direction = IfxStdIf_Pos_Dir_forward;
            }
            else
            {
            	driver->direction = IfxStdIf_Pos_Dir_backward;
            }
        }
        else if (newPosition < driver->rawPosition)
        {
            if ((driver->rawPosition - newPosition) < R2)
            {
            	driver->direction = IfxStdIf_Pos_Dir_backward;
            }
            else
            {
            	driver->direction = IfxStdIf_Pos_Dir_forward;
            }
        }
        else {}
    }

    driver->turn = driver->registers.I.AREV.B.REVOL;

    IfxTle5012_Driver_sscUpdateSpeed(driver, newPosition);
    driver->rawPosition = newPosition;
}


void IfxTle5012_Driver_sscUpdateSpeed(IfxTle5012_Driver* driver, sint32 newPosition)
{
    float32 speed;
    uint32  diff;

    diff = __minX(
    		__minX(
    				Ifx__absX(newPosition - driver->rawPosition),
    				Ifx__absX(newPosition + driver->resolution - driver->rawPosition)
    			),
    		Ifx__absX(newPosition - driver->resolution - driver->rawPosition)
    		);

#if 1
	speed = diff * driver->speedConstPulseCount;
    speed = driver->direction == IfxStdIf_Pos_Dir_forward ? speed : -speed;
    driver->speed = speed;
#else
    sint16 RawSpeed;

    /* extend sign */
    RawSpeed = driver->registers.I.ASPD.B.ANG_SPD << 1;

    if (driver->reversed != FALSE) { RawSpeed = -RawSpeed; }

    /* Note the angle range is multiplied by 2 because we extend the sign before,
     * so the RawSpeed doubles its original value */
    driver->speed = RawSpeed * ((2.0 * IFX_PI) / (driver->resolution * 2)) * (1.0e6 /
                    (driver->updatePeriod * 2));
#endif

}






/* <CRC8  CALCULATIONS> ===================================================== */


const uint8 IfxTle5012_Driver_Crc8Table[256] = {
    0x00U, 0x1DU, 0x3AU, 0x27U, 0x74U, 0x69U, 0x4EU, 0x53U,
    0xE8U, 0xF5U, 0xD2U, 0xCFU, 0x9CU, 0x81U, 0xA6U, 0xBBU,
    0xCDU, 0xD0U, 0xF7U, 0xEAU, 0xB9U, 0xA4U, 0x83U, 0x9EU,
    0x25U, 0x38U, 0x1FU, 0x02U, 0x51U, 0x4CU, 0x6BU, 0x76U,
    0x87U, 0x9AU, 0xBDU, 0xA0U, 0xF3U, 0xEEU, 0xC9U, 0xD4U,
    0x6FU, 0x72U, 0x55U, 0x48U, 0x1BU, 0x06U, 0x21U, 0x3CU,
    0x4AU, 0x57U, 0x70U, 0x6DU, 0x3EU, 0x23U, 0x04U, 0x19U,
    0xA2U, 0xBFU, 0x98U, 0x85U, 0xD6U, 0xCBU, 0xECU, 0xF1U,
    0x13U, 0x0EU, 0x29U, 0x34U, 0x67U, 0x7AU, 0x5DU, 0x40U,
    0xFBU, 0xE6U, 0xC1U, 0xDCU, 0x8FU, 0x92U, 0xB5U, 0xA8U,
    0xDEU, 0xC3U, 0xE4U, 0xF9U, 0xAAU, 0xB7U, 0x90U, 0x8DU,
    0x36U, 0x2BU, 0x0CU, 0x11U, 0x42U, 0x5FU, 0x78U, 0x65U,
    0x94U, 0x89U, 0xAEU, 0xB3U, 0xE0U, 0xFDU, 0xDAU, 0xC7U,
    0x7CU, 0x61U, 0x46U, 0x5BU, 0x08U, 0x15U, 0x32U, 0x2FU,
    0x59U, 0x44U, 0x63U, 0x7EU, 0x2DU, 0x30U, 0x17U, 0x0AU,
    0xB1U, 0xACU, 0x8BU, 0x96U, 0xC5U, 0xD8U, 0xFFU, 0xE2U,
    0x26U, 0x3BU, 0x1CU, 0x01U, 0x52U, 0x4FU, 0x68U, 0x75U,
    0xCEU, 0xD3U, 0xF4U, 0xE9U, 0xBAU, 0xA7U, 0x80U, 0x9DU,
    0xEBU, 0xF6U, 0xD1U, 0xCCU, 0x9FU, 0x82U, 0xA5U, 0xB8U,
    0x03U, 0x1EU, 0x39U, 0x24U, 0x77U, 0x6AU, 0x4DU, 0x50U,
    0xA1U, 0xBCU, 0x9BU, 0x86U, 0xD5U, 0xC8U, 0xEFU, 0xF2U,
    0x49U, 0x54U, 0x73U, 0x6EU, 0x3DU, 0x20U, 0x07U, 0x1AU,
    0x6CU, 0x71U, 0x56U, 0x4BU, 0x18U, 0x05U, 0x22U, 0x3FU,
    0x84U, 0x99U, 0xBEU, 0xA3U, 0xF0U, 0xEDU, 0xCAU, 0xD7U,
    0x35U, 0x28U, 0x0FU, 0x12U, 0x41U, 0x5CU, 0x7BU, 0x66U,
    0xDDU, 0xC0U, 0xE7U, 0xFAU, 0xA9U, 0xB4U, 0x93U, 0x8EU,
    0xF8U, 0xE5U, 0xC2U, 0xDFU, 0x8CU, 0x91U, 0xB6U, 0xABU,
    0x10U, 0x0DU, 0x2AU, 0x37U, 0x64U, 0x79U, 0x5EU, 0x43U,
    0xB2U, 0xAFU, 0x88U, 0x95U, 0xC6U, 0xDBU, 0xFCU, 0xE1U,
    0x5AU, 0x47U, 0x60U, 0x7DU, 0x2EU, 0x33U, 0x14U, 0x09U,
    0x7FU, 0x62U, 0x45U, 0x58U, 0x0BU, 0x16U, 0x31U, 0x2CU,
    0x97U, 0x8AU, 0xADU, 0xB0U, 0xE3U, 0xFEU, 0xD9U, 0xC4U,
};




#define SIZE16   sizeof(uint16)

#define IsAligned(offset, alignment)    \
    (((uint32)(offset) & ((alignment)-1)) == 0)




/*#pragma section=CRC_RAMFUNCS*/
IFX_INLINE uint8 IfxTle5012_Driver_Crc8Update8(uint8 crc, uint8 value)
{
    return IfxTle5012_Driver_Crc8Table[(crc ^ value) & 0xFF];
}



IFX_INLINE uint8 IfxTle5012_Driver_Crc8Update16(uint8 crc, uint16 value)
{
    crc = IfxTle5012_Driver_Crc8Update8(crc, value >> 8);
    return IfxTle5012_Driver_Crc8Update8(crc, value & 0xff);
}




static uint8 IfxTle5012_Driver_Crc8Update(uint8 crc, const uint8* data, uint16 count)
{
    uint16 i;

    if (IsAligned(data, SIZE16) && (count >= SIZE16))
    {
        for (i = 0; i < (count / SIZE16); i++)
        {
            crc = IfxTle5012_Driver_Crc8Update16(crc, *(const uint16*)data);
            data = &data[SIZE16];
        }
        count &= (SIZE16 - 1);
    }

    for (i = 0; i < count; i++)
    {
        crc = IfxTle5012_Driver_Crc8Update8(crc, ((const uint8*)data)[i]);
    }

    return crc;
}


static uint8 IfxTle5012_Driver_Crc8Calculate(const void* data, uint16 count)
{
    return IfxTle5012_Driver_Crc8Update(0xff, (const uint8*) data, count) ^ 0xff;
}





