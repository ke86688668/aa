/**
 * \file Ifx1edi2010as.h
 * \brief 1EDI2010AS  basic functionality 
 * \ingroup IfxLld_1edi2010as 
 *
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved.
 *
 * $Date: 2017-06-30 15:39:05
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_1edi2010as_Std_Std Std  
 * \ingroup IfxLld_1edi2010as_Std
 */

#ifndef IFX1EDI2010AS_H
#define IFX1EDI2010AS_H 1

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "SysSe/Ext/1edi2010as/_Impl/Ifx1edi2010as_cfg.h"
#include "SysSe/Ext/1edi2010as/Driver/Ifx1edi2010as_Driver.h"
#include "SysSe/Ext/1edi2010as/_Reg/Ifx1edi2010as_bf.h"

/** \addtogroup IfxLld_1edi2010as_Std_Std
 * \{ */

/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \brief Get Primary Chip Identification
 * Bitfield: PID.PVERS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Primary Chip Identification  
 */
IFX_EXTERN Ifx1edi2010as_PrimaryChipIdentification Ifx1edi2010as_getPrimaryChipIdentification(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Gate Regular TTOFF Plateau Level Configuration Status
 * Bitfield: PSTAT.GPOFP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Gate Regular TTOFF Plateau Level Configuration Status  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getGateRegularTtoffPlateauLevelConfigurationStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Secondary Ready Status
 * Bitfield: PSTAT.SRDY 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Secondary Ready Status  
 */
IFX_EXTERN Ifx1edi2010as_SecondaryReadyStatus Ifx1edi2010as_getSecondaryReadyStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Secondary Ready Status
 * Bitfield: PSTAT.SRDY 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isSecondaryReady(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Active State Status
 * Bitfield: PSTAT.ACT 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Active State Status  
 */
IFX_EXTERN Ifx1edi2010as_ActiveStateStatus Ifx1edi2010as_getActiveStateStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Gate TTON Plateau Level Configuration Status
 * Bitfield: PSTAT.GPONP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Gate TTON Plateau Level Configuration Status  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getGateTtonPlateauLevelConfigurationStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Error Status
 * Bitfield: PSTAT.ERR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Error Status  
 */
IFX_EXTERN Ifx1edi2010as_ErrorStatus Ifx1edi2010as_getErrorStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Error Status
 * Bitfield: PSTAT.ERR 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isError(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get EN Valid Status
 * Bitfield: PSTAT2.ENVAL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return EN Valid Status  
 */
IFX_EXTERN Ifx1edi2010as_EnValidStatus Ifx1edi2010as_getEnValidStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get NFLTA Pin Driver Request
 * Bitfield: PSTAT2.FLTA 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return NFLTA Pin Driver Request  
 */
IFX_EXTERN Ifx1edi2010as_NfltaPinDriverRequest Ifx1edi2010as_getNfltaPinDriverRequest(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get NFLTB Pin Driver Request
 * Bitfield: PSTAT2.FLTB 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return NFLTB Pin Driver Request  
 */
IFX_EXTERN Ifx1edi2010as_NfltbPinDriverRequest Ifx1edi2010as_getNfltbPinDriverRequest(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Operating Mode
 * Bitfield: PSTAT2.OPMP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Operating Mode  
 */
IFX_EXTERN Ifx1edi2010as_OperatingMode Ifx1edi2010as_getOperatingMode(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Event Class A Status
 * Bitfield: PSTAT2.FLTAP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Event Class A Status  
 */
IFX_EXTERN Ifx1edi2010as_EventClassAStatus Ifx1edi2010as_getEventClassAStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Event Class B Status
 * Bitfield: PSTAT2.FLTBP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Event Class B Status  
 */
IFX_EXTERN Ifx1edi2010as_EventClassBStatus Ifx1edi2010as_getEventClassBStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Shoot Through Protection Status
 * Bitfield: PSTAT2.STP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Shoot Through Protection Status  
 */
IFX_EXTERN Ifx1edi2010as_ShootThroughProtectionStatus Ifx1edi2010as_getShootThroughProtectionStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Under Or Overvoltage Error Status
 * Bitfield: PSTAT2.AXVP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Under Or Overvoltage Error Status  
 */
IFX_EXTERN Ifx1edi2010as_AdcUnderOrOvervoltageErrorStatus Ifx1edi2010as_getAdcUnderOrOvervoltageErrorStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Under Or Overvoltage Error Status
 * Bitfield: PSTAT2.AXVP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isAdcUnderOrOvervoltageError(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Primary Communication Error Flag
 * Bitfield: PER.CERP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Primary Communication Error Flag  
 */
IFX_EXTERN Ifx1edi2010as_PrimaryCommunicationErrorFlag Ifx1edi2010as_getPrimaryCommunicationErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Primary Communication Error Flag
 * Bitfield: PER.CERP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isPrimaryCommunicationErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Error Flag
 * Bitfield: PER.ADER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Error Flag  
 */
IFX_EXTERN Ifx1edi2010as_AdcErrorFlag Ifx1edi2010as_getAdcErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Error Flag
 * Bitfield: PER.ADER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isAdcErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get SPI Error Flag
 * Bitfield: PER.SPIER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return SPI Error Flag  
 */
IFX_EXTERN Ifx1edi2010as_SpiErrorFlag Ifx1edi2010as_getSpiErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get SPI Error Flag
 * Bitfield: PER.SPIER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isSpiErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Shoot Through Protection Error Flag
 * Bitfield: PER.STPER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Shoot Through Protection Error Flag  
 */
IFX_EXTERN Ifx1edi2010as_ShootThroughProtectionErrorFlag Ifx1edi2010as_getShootThroughProtectionErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Shoot Through Protection Error Flag
 * Bitfield: PER.STPER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isShootThroughProtectionErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get EN Signal Invalid Flag
 * Bitfield: PER.ENER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return EN Signal Invalid Flag  
 */
IFX_EXTERN Ifx1edi2010as_EnSignalInvalidFlag Ifx1edi2010as_getEnSignalInvalidFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get EN Signal Invalid Flag
 * Bitfield: PER.ENER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isEnSignalInvalidFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Primary Reset Flag
 * Bitfield: PER.RSTP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Primary Reset Flag  
 */
IFX_EXTERN Ifx1edi2010as_PrimaryResetFlag Ifx1edi2010as_getPrimaryResetFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Primary Reset Flag
 * Bitfield: PER.RSTP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isPrimaryResetFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Primary External Hard Reset flag
 * Bitfield: PER.RSTEP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Primary External Hard Reset flag  
 */
IFX_EXTERN Ifx1edi2010as_PrimaryExternalHardResetFlag Ifx1edi2010as_getPrimaryExternalHardResetFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Primary External Hard Reset flag
 * Bitfield: PER.RSTEP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isPrimaryExternalHardResetFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get SPI Parity Enable
 * Bitfield: PCFG.PAREN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return SPI Parity Enable  
 */
IFX_EXTERN Ifx1edi2010as_SpiParityEnable Ifx1edi2010as_getSpiParityEnable(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get SPI Parity Enable
 * Bitfield: PCFG.PAREN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isSpiParityEnabled(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set SPI Parity Enable
 * Bitfield: PCFG.PAREN 
 * \param driver Device driver object  
 * \param value SPI Parity Enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setSpiParityEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_SpiParityEnable value, boolean* status);

/** \brief Set SPI Parity Enable [Enable]
 * Bitfield: PCFG.PAREN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_enableSpiParity(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set SPI Parity Enable [Disable]
 * Bitfield: PCFG.PAREN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_disableSpiParity(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get NFLTA Pin Activation on Boundary Check Event Enable
 * Bitfield: PCFG.ADAEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return NFLTA Pin Activation on Boundary Check Event Enable  
 */
IFX_EXTERN Ifx1edi2010as_NfltaPinActivationOnBoundaryCheckEventEnable Ifx1edi2010as_getNfltaPinActivationOnBoundaryCheckEventEnable(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get NFLTA Pin Activation on Boundary Check Event Enable
 * Bitfield: PCFG.ADAEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isNfltaPinActivationOnBoundaryCheckEventEnabled(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set NFLTA Pin Activation on Boundary Check Event Enable
 * Bitfield: PCFG.ADAEN 
 * \param driver Device driver object  
 * \param value NFLTA Pin Activation on Boundary Check Event Enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setNfltaPinActivationOnBoundaryCheckEventEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_NfltaPinActivationOnBoundaryCheckEventEnable value, boolean* status);

/** \brief Set NFLTA Pin Activation on Boundary Check Event Enable [Enable]
 * Bitfield: PCFG.ADAEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_enableNfltaPinActivationOnBoundaryCheckEvent(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set NFLTA Pin Activation on Boundary Check Event Enable [Disable]
 * Bitfield: PCFG.ADAEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_disableNfltaPinActivationOnBoundaryCheckEvent(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Trigger Input Enable
 * Bitfield: PCFG.ADTEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Trigger Input Enable  
 */
IFX_EXTERN Ifx1edi2010as_AdcTriggerInputEnable Ifx1edi2010as_getAdcTriggerInputEnable(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Trigger Input Enable
 * Bitfield: PCFG.ADTEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isAdcTriggerInputEnabled(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Trigger Input Enable
 * Bitfield: PCFG.ADTEN 
 * \param driver Device driver object  
 * \param value ADC Trigger Input Enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setAdcTriggerInputEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcTriggerInputEnable value, boolean* status);

/** \brief Set ADC Trigger Input Enable [Enable]
 * Bitfield: PCFG.ADTEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_enableAdcTriggerInput(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Trigger Input Enable [Disable]
 * Bitfield: PCFG.ADTEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_disableAdcTriggerInput(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Shoot Through Protection Delay
 * Bitfield: PCFG2.STPDEL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Shoot Through Protection Delay  
 */
IFX_EXTERN Ifx1edi2010as_ShootThroughProtectionDelay Ifx1edi2010as_getShootThroughProtectionDelay(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Shoot Through Protection Delay
 * Bitfield: PCFG2.STPDEL 
 * \param driver Device driver object  
 * \param value Shoot Through Protection Delay  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setShootThroughProtectionDelay(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_ShootThroughProtectionDelay value, boolean* status);

/** \brief Get DIO1 Pin Mode
 * Bitfield: PCFG2.DIO1 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return DIO1 Pin Mode  
 */
IFX_EXTERN Ifx1edi2010as_Dio1PinMode Ifx1edi2010as_getDio1PinMode(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set DIO1 Pin Mode
 * Bitfield: PCFG2.DIO1 
 * \param driver Device driver object  
 * \param value DIO1 Pin Mode  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setDio1PinMode(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_Dio1PinMode value, boolean* status);

/** \brief Get Gate TTON Plateau Level
 * Bitfield: PCTRL.GPON 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Gate TTON Plateau Level  
 */
IFX_EXTERN Ifx1edi2010as_GateTtonPlateauLevel Ifx1edi2010as_getGateTtonPlateauLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Gate TTON Plateau Level
 * Bitfield: PCTRL.GPON 
 * \param driver Device driver object  
 * \param value Gate TTON Plateau Level  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setGateTtonPlateauLevel(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_GateTtonPlateauLevel value, boolean* status);

/** \brief Get Clear Primary Sitcky Bits
 * Bitfield: PCTRL.CLRP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Clear Primary Sitcky Bits  
 */
IFX_EXTERN Ifx1edi2010as_ClearPrimarySitckyBits Ifx1edi2010as_getClearPrimarySitckyBits(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Clear Primary Sitcky Bits
 * Bitfield: PCTRL.CLRP 
 * \param driver Device driver object  
 * \param value Clear Primary Sitcky Bits  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setClearPrimarySitckyBits(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_ClearPrimarySitckyBits value, boolean* status);

/** \brief Set Clear Primary Sitcky Bits
 * Bitfield: PCTRL.CLRP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_clearPrimarySitckyBits(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Clear Secondary Sitcky Bits
 * Bitfield: PCTRL.CLRS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Clear Secondary Sitcky Bits  
 */
IFX_EXTERN Ifx1edi2010as_ClearSecondarySitckyBits Ifx1edi2010as_getClearSecondarySitckyBits(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Clear Secondary Sitcky Bits
 * Bitfield: PCTRL.CLRS 
 * \param driver Device driver object  
 * \param value Clear Secondary Sitcky Bits  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setClearSecondarySitckyBits(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_ClearSecondarySitckyBits value, boolean* status);

/** \brief Set Clear Secondary Sitcky Bits
 * Bitfield: PCTRL.CLRS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_clearSecondarySitckyBits(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Gate Regular TTOFF Plateau Level
 * Bitfield: PCTRL2.GPOF 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Gate Regular TTOFF Plateau Level  
 */
IFX_EXTERN Ifx1edi2010as_GateRegularTtoffPlateauLevel Ifx1edi2010as_getGateRegularTtoffPlateauLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Gate Regular TTOFF Plateau Level
 * Bitfield: PCTRL2.GPOF 
 * \param driver Device driver object  
 * \param value Gate Regular TTOFF Plateau Level  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setGateRegularTtoffPlateauLevel(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_GateRegularTtoffPlateauLevel value, boolean* status);

/** \brief Get ADC Conversion Request
 * Bitfield: PCTRL2.ACRP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Conversion Request  
 */
IFX_EXTERN Ifx1edi2010as_AdcConversionRequest Ifx1edi2010as_getAdcConversionRequest(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Conversion Request
 * Bitfield: PCTRL2.ACRP 
 * \param driver Device driver object  
 * \param value ADC Conversion Request  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setAdcConversionRequest(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcConversionRequest value, boolean* status);

/** \brief Get Primary Verification Function
 * Bitfield: PSCR.VFSP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Primary Verification Function  
 */
IFX_EXTERN Ifx1edi2010as_PrimaryVerificationFunction Ifx1edi2010as_getPrimaryVerificationFunction(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Primary Verification Function
 * Bitfield: PSCR.VFSP 
 * \param driver Device driver object  
 * \param value Primary Verification Function  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setPrimaryVerificationFunction(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_PrimaryVerificationFunction value, boolean* status);

/** \brief Get Data Integrity Test Register
 * Bitfield: PRW.RWVAL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Data Integrity Test Register  
 */
IFX_EXTERN uint16 Ifx1edi2010as_getDataIntegrityTestRegister(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Data Integrity Test Register
 * Bitfield: PRW.RWVAL 
 * \param driver Device driver object  
 * \param value Data Integrity Test Register  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setDataIntegrityTestRegister(Ifx1edi2010as_Driver* driver, uint16 value, boolean* status);

/** \brief Get INP Pin Level
 * Bitfield: PPIN.INPL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return INP Pin Level  
 */
IFX_EXTERN Ifx1edi2010as_InpPinLevel Ifx1edi2010as_getInpPinLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get INSTP Pin Level
 * Bitfield: PPIN.INSTPL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return INSTP Pin Level  
 */
IFX_EXTERN Ifx1edi2010as_InstpPinLevel Ifx1edi2010as_getInstpPinLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get EN Pin Level
 * Bitfield: PPIN.ENL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return EN Pin Level  
 */
IFX_EXTERN Ifx1edi2010as_EnPinLevel Ifx1edi2010as_getEnPinLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get NFLTA pin Level
 * Bitfield: PPIN.NFLTAL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return NFLTA pin Level  
 */
IFX_EXTERN Ifx1edi2010as_NfltaPinLevel Ifx1edi2010as_getNfltaPinLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get NFLTB pin level
 * Bitfield: PPIN.NFLTBL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return NFLTB pin level  
 */
IFX_EXTERN Ifx1edi2010as_NfltbPinLevel Ifx1edi2010as_getNfltbPinLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Trigger Input Level
 * Bitfield: PPIN.ADCTL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Trigger Input Level  
 */
IFX_EXTERN Ifx1edi2010as_AdcTriggerInputLevel Ifx1edi2010as_getAdcTriggerInputLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get DIO1 Pin Level
 * Bitfield: PPIN.DIO1L 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return DIO1 Pin Level  
 */
IFX_EXTERN Ifx1edi2010as_Dio1PinLevel Ifx1edi2010as_getDio1PinLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Primary Clock Supervision
 * Bitfield: PCS.CSP 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Primary Clock Supervision  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getPrimaryClockSupervision(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Secondary Chip Identification
 * Bitfield: SID.SVERS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Secondary Chip Identification  
 */
IFX_EXTERN uint16 Ifx1edi2010as_getSecondaryChipIdentification(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get PWM Command Status
 * Bitfield: SSTAT.PWM 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return PWM Command Status  
 */
IFX_EXTERN Ifx1edi2010as_PwmCommandStatus Ifx1edi2010as_getPwmCommandStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Debug Mode Active Flag
 * Bitfield: SSTAT.DBG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Debug Mode Active Flag  
 */
IFX_EXTERN Ifx1edi2010as_DebugModeActiveFlag Ifx1edi2010as_getDebugModeActiveFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Debug Mode Active Flag
 * Bitfield: SSTAT.DBG 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isDebugModeActiveFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get DESAT Comparator Result
 * Bitfield: SSTAT2.DSATC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return DESAT Comparator Result  
 */
IFX_EXTERN Ifx1edi2010as_DesatComparatorResult Ifx1edi2010as_getDesatComparatorResult(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get OCP Comparator Result
 * Bitfield: SSTAT2.OCPC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return OCP Comparator Result  
 */
IFX_EXTERN Ifx1edi2010as_OcpComparatorResult Ifx1edi2010as_getOcpComparatorResult(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get UVLO2 Event
 * Bitfield: SSTAT2.UVLO2M 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return UVLO2 Event  
 */
IFX_EXTERN Ifx1edi2010as_Uvlo2Event Ifx1edi2010as_getUvlo2Event(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get DIO2 Pin Level
 * Bitfield: SSTAT2.DIO2L 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return DIO2 Pin Level  
 */
IFX_EXTERN Ifx1edi2010as_Dio2PinLevel Ifx1edi2010as_getDio2PinLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get DACLP Pin outpout level
 * Bitfield: SSTAT2.DACL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return DACLP Pin outpout level  
 */
IFX_EXTERN Ifx1edi2010as_DaclpPinOutpoutLevel Ifx1edi2010as_getDaclpPinOutpoutLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Communication Error Secondary Flag
 * Bitfield: SER.CERS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Communication Error Secondary Flag  
 */
IFX_EXTERN Ifx1edi2010as_CommunicationErrorSecondaryFlag Ifx1edi2010as_getCommunicationErrorSecondaryFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Communication Error Secondary Flag
 * Bitfield: SER.CERS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isCommunicationErrorSecondaryFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Undervoltage Error Flag
 * Bitfield: SER.AUVER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Undervoltage Error Flag  
 */
IFX_EXTERN Ifx1edi2010as_AdcUndervoltageErrorFlag Ifx1edi2010as_getAdcUndervoltageErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Undervoltage Error Flag
 * Bitfield: SER.AUVER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isAdcUndervoltageErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Overvoltage Error Flag
 * Bitfield: SER.AOVER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Overvoltage Error Flag  
 */
IFX_EXTERN Ifx1edi2010as_AdcOvervoltageErrorFlag Ifx1edi2010as_getAdcOvervoltageErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Overvoltage Error Flag
 * Bitfield: SER.AOVER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isAdcOvervoltageErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Verification Mode Time-Out Event Flag
 * Bitfield: SER.VMTO 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Verification Mode Time-Out Event Flag  
 */
IFX_EXTERN Ifx1edi2010as_VerificationModeTimeOutEventFlag Ifx1edi2010as_getVerificationModeTimeOutEventFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Verification Mode Time-Out Event Flag
 * Bitfield: SER.VMTO 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isVerificationModeTimeOutEventFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get UVLO2 Error Flag
 * Bitfield: SER.UVLO2ER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return UVLO2 Error Flag  
 */
IFX_EXTERN Ifx1edi2010as_Uvlo2ErrorFlag Ifx1edi2010as_getUvlo2ErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get UVLO2 Error Flag
 * Bitfield: SER.UVLO2ER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isUvlo2ErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get DESAT Error Flag
 * Bitfield: SER.DESATER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return DESAT Error Flag  
 */
IFX_EXTERN Ifx1edi2010as_DesatErrorFlag Ifx1edi2010as_getDesatErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get DESAT Error Flag
 * Bitfield: SER.DESATER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isDesatErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get OCP Error Flag
 * Bitfield: SER.OCPER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return OCP Error Flag  
 */
IFX_EXTERN Ifx1edi2010as_OcpErrorFlag Ifx1edi2010as_getOcpErrorFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get OCP Error Flag
 * Bitfield: SER.OCPER 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isOcpErrorFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Secondary Hard Reset Flag
 * Bitfield: SER.RSTS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Secondary Hard Reset Flag  
 */
IFX_EXTERN Ifx1edi2010as_SecondaryHardResetFlag Ifx1edi2010as_getSecondaryHardResetFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Secondary Hard Reset Flag
 * Bitfield: SER.RSTS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isSecondaryHardResetFlagSet(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get VBE Compensation Enable
 * Bitfield: SCFG.VBEC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return VBE Compensation Enable  
 */
IFX_EXTERN Ifx1edi2010as_VbeCompensationEnable Ifx1edi2010as_getVbeCompensationEnable(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get VBE Compensation Enable
 * Bitfield: SCFG.VBEC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isVbeCompensationEnabled(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set VBE Compensation Enable
 * Bitfield: SCFG.VBEC 
 * \param driver Device driver object  
 * \param value VBE Compensation Enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setVbeCompensationEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_VbeCompensationEnable value, boolean* status);

/** \brief Set VBE Compensation Enable [Enable]
 * Bitfield: SCFG.VBEC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_enableVbeCompensation(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set VBE Compensation Enable [Disable]
 * Bitfield: SCFG.VBEC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_disableVbeCompensation(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Secondary Advanced Configuration Enable
 * Bitfield: SCFG.CFG2 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Secondary Advanced Configuration Enable  
 */
IFX_EXTERN Ifx1edi2010as_SecondaryAdvancedConfigurationEnable Ifx1edi2010as_getSecondaryAdvancedConfigurationEnable(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Secondary Advanced Configuration Enable
 * Bitfield: SCFG.CFG2 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isSecondaryAdvancedConfigurationEnabled(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Secondary Advanced Configuration Enable
 * Bitfield: SCFG.CFG2 
 * \param driver Device driver object  
 * \param value Secondary Advanced Configuration Enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setSecondaryAdvancedConfigurationEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_SecondaryAdvancedConfigurationEnable value, boolean* status);

/** \brief Set Secondary Advanced Configuration Enable [Enable]
 * Bitfield: SCFG.CFG2 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_enableSecondaryAdvancedConfiguration(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Secondary Advanced Configuration Enable [Disable]
 * Bitfield: SCFG.CFG2 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_disableSecondaryAdvancedConfiguration(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get DIO2 Pin Mode
 * Bitfield: SCFG.DIO2C 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return DIO2 Pin Mode  
 */
IFX_EXTERN Ifx1edi2010as_Dio2PinMode Ifx1edi2010as_getDio2PinMode(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set DIO2 Pin Mode
 * Bitfield: SCFG.DIO2C 
 * \param driver Device driver object  
 * \param value DIO2 Pin Mode  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setDio2PinMode(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_Dio2PinMode value, boolean* status);

/** \brief Get DESAT Clamping Enable
 * Bitfield: SCFG.DSTCEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return DESAT Clamping Enable  
 */
IFX_EXTERN Ifx1edi2010as_DesatClampingEnable Ifx1edi2010as_getDesatClampingEnable(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get DESAT Clamping Enable
 * Bitfield: SCFG.DSTCEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isDesatClampingEnabled(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set DESAT Clamping Enable
 * Bitfield: SCFG.DSTCEN 
 * \param driver Device driver object  
 * \param value DESAT Clamping Enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setDesatClampingEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_DesatClampingEnable value, boolean* status);

/** \brief Set DESAT Clamping Enable [Enable]
 * Bitfield: SCFG.DSTCEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_enableDesatClamping(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set DESAT Clamping Enable [Disable]
 * Bitfield: SCFG.DSTCEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_disableDesatClamping(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Pulse Suppressor Enable
 * Bitfield: SCFG.PSEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Pulse Suppressor Enable  
 */
IFX_EXTERN Ifx1edi2010as_PulseSuppressorEnable Ifx1edi2010as_getPulseSuppressorEnable(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Pulse Suppressor Enable
 * Bitfield: SCFG.PSEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isPulseSuppressorEnabled(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Pulse Suppressor Enable
 * Bitfield: SCFG.PSEN 
 * \param driver Device driver object  
 * \param value Pulse Suppressor Enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setPulseSuppressorEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_PulseSuppressorEnable value, boolean* status);

/** \brief Set Pulse Suppressor Enable [Enable]
 * Bitfield: SCFG.PSEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_enablePulseSuppressor(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Pulse Suppressor Enable [Disable]
 * Bitfield: SCFG.PSEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_disablePulseSuppressor(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Verification Mode Time Out Duration
 * Bitfield: SCFG.TOSEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Verification Mode Time Out Duration  
 */
IFX_EXTERN Ifx1edi2010as_VerificationModeTimeOutDuration Ifx1edi2010as_getVerificationModeTimeOutDuration(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Verification Mode Time Out Duration
 * Bitfield: SCFG.TOSEN 
 * \param driver Device driver object  
 * \param value Verification Mode Time Out Duration  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setVerificationModeTimeOutDuration(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_VerificationModeTimeOutDuration value, boolean* status);

/** \brief Get DESAT Threshold Level
 * Bitfield: SCFG.DSATLS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return DESAT Threshold Level  
 */
IFX_EXTERN Ifx1edi2010as_DesatThresholdLevel Ifx1edi2010as_getDesatThresholdLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set DESAT Threshold Level
 * Bitfield: SCFG.DSATLS 
 * \param driver Device driver object  
 * \param value DESAT Threshold Level  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setDesatThresholdLevel(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_DesatThresholdLevel value, boolean* status);

/** \brief Get UVLO2 Threshold Level
 * Bitfield: SCFG.UVLO2S 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return UVLO2 Threshold Level  
 */
IFX_EXTERN Ifx1edi2010as_Uvlo2ThresholdLevel Ifx1edi2010as_getUvlo2ThresholdLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set UVLO2 Threshold Level
 * Bitfield: SCFG.UVLO2S 
 * \param driver Device driver object  
 * \param value UVLO2 Threshold Level  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setUvlo2ThresholdLevel(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_Uvlo2ThresholdLevel value, boolean* status);

/** \brief Get OCP Threshold Level
 * Bitfield: SCFG.OCPLS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return OCP Threshold Level  
 */
IFX_EXTERN Ifx1edi2010as_OcpThresholdLevel Ifx1edi2010as_getOcpThresholdLevel(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set OCP Threshold Level
 * Bitfield: SCFG.OCPLS 
 * \param driver Device driver object  
 * \param value OCP Threshold Level  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setOcpThresholdLevel(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_OcpThresholdLevel value, boolean* status);

/** \brief Get DACLP Pin clamping outpout
 * Bitfield: SCFG.DACLC 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return DACLP Pin clamping outpout  
 */
IFX_EXTERN Ifx1edi2010as_DaclpPinClampingOutpout Ifx1edi2010as_getDaclpPinClampingOutpout(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set DACLP Pin clamping outpout
 * Bitfield: SCFG.DACLC 
 * \param driver Device driver object  
 * \param value DACLP Pin clamping outpout  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setDaclpPinClampingOutpout(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_DaclpPinClampingOutpout value, boolean* status);

/** \brief Get ADC PWM Trigger Delay
 * Bitfield: SCFG2.PWMD 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC PWM Trigger Delay  
 */
IFX_EXTERN Ifx1edi2010as_AdcPwmTriggerDelay Ifx1edi2010as_getAdcPwmTriggerDelay(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC PWM Trigger Delay
 * Bitfield: SCFG2.PWMD 
 * \param driver Device driver object  
 * \param value ADC PWM Trigger Delay  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setAdcPwmTriggerDelay(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcPwmTriggerDelay value, boolean* status);

/** \brief Get ADC Secondary Trigger Mode
 * Bitfield: SCFG2.ATS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Secondary Trigger Mode  
 */
IFX_EXTERN Ifx1edi2010as_AdcSecondaryTriggerMode Ifx1edi2010as_getAdcSecondaryTriggerMode(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Secondary Trigger Mode
 * Bitfield: SCFG2.ATS 
 * \param driver Device driver object  
 * \param value ADC Secondary Trigger Mode  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setAdcSecondaryTriggerMode(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcSecondaryTriggerMode value, boolean* status);

/** \brief Get ADC Gain
 * Bitfield: SCFG2.AGS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Gain  
 */
IFX_EXTERN Ifx1edi2010as_AdcGain Ifx1edi2010as_getAdcGain(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Gain
 * Bitfield: SCFG2.AGS 
 * \param driver Device driver object  
 * \param value ADC Gain  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setAdcGain(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcGain value, boolean* status);

/** \brief Get ADC Offset
 * Bitfield: SCFG2.AOS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Offset  
 */
IFX_EXTERN Ifx1edi2010as_AdcOffset Ifx1edi2010as_getAdcOffset(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Offset
 * Bitfield: SCFG2.AOS 
 * \param driver Device driver object  
 * \param value ADC Offset  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setAdcOffset(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcOffset value, boolean* status);

/** \brief Get ADC Current Source
 * Bitfield: SCFG2.ACSS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Current Source  
 */
IFX_EXTERN Ifx1edi2010as_AdcCurrentSource Ifx1edi2010as_getAdcCurrentSource(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Current Source
 * Bitfield: SCFG2.ACSS 
 * \param driver Device driver object  
 * \param value ADC Current Source  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setAdcCurrentSource(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcCurrentSource value, boolean* status);

/** \brief Get ADC Event Class A Enable
 * Bitfield: SCFG2.ACAEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Event Class A Enable  
 */
IFX_EXTERN Ifx1edi2010as_AdcEventClassAEnable Ifx1edi2010as_getAdcEventClassAEnable(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Event Class A Enable
 * Bitfield: SCFG2.ACAEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isAdcEventClassAEnabled(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Event Class A Enable
 * Bitfield: SCFG2.ACAEN 
 * \param driver Device driver object  
 * \param value ADC Event Class A Enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setAdcEventClassAEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcEventClassAEnable value, boolean* status);

/** \brief Set ADC Event Class A Enable [Enable]
 * Bitfield: SCFG2.ACAEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_enableAdcEventClassA(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Event Class A Enable [Disable]
 * Bitfield: SCFG2.ACAEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_disableAdcEventClassA(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Enable
 * Bitfield: SCFG2.ADCEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Enable  
 */
IFX_EXTERN Ifx1edi2010as_AdcEnable Ifx1edi2010as_getAdcEnable(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Enable
 * Bitfield: SCFG2.ADCEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isAdcEnabled(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Enable
 * Bitfield: SCFG2.ADCEN 
 * \param driver Device driver object  
 * \param value ADC Enable  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setAdcEnable(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_AdcEnable value, boolean* status);

/** \brief Set ADC Enable [Enable]
 * Bitfield: SCFG2.ADCEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_enableAdc(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Enable [Disable]
 * Bitfield: SCFG2.ADCEN 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_disableAdc(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get Secondary Verification Function
 * Bitfield: SSCR.VFS2 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Secondary Verification Function  
 */
IFX_EXTERN Ifx1edi2010as_SecondaryVerificationFunction Ifx1edi2010as_getSecondaryVerificationFunction(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Secondary Verification Function
 * Bitfield: SSCR.VFS2 
 * \param driver Device driver object  
 * \param value Secondary Verification Function  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setSecondaryVerificationFunction(Ifx1edi2010as_Driver* driver, Ifx1edi2010as_SecondaryVerificationFunction value, boolean* status);

/** \brief Get DESAT Blanking Time
 * Bitfield: SDESAT.DSATBT 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return DESAT Blanking Time  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getDesatBlankingTime(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set DESAT Blanking Time
 * Bitfield: SDESAT.DSATBT 
 * \param driver Device driver object  
 * \param value DESAT Blanking Time  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setDesatBlankingTime(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status);

/** \brief Get OCP Blanking Time
 * Bitfield: SOCP.OCPBT 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return OCP Blanking Time  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getOcpBlankingTime(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set OCP Blanking Time
 * Bitfield: SOCP.OCPBT 
 * \param driver Device driver object  
 * \param value OCP Blanking Time  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setOcpBlankingTime(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status);

/** \brief Get Gate Regular TTOFF delay
 * Bitfield: SRTTOF.RTVAL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Gate Regular TTOFF delay  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getGateRegularTtoffDelay(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Gate Regular TTOFF delay
 * Bitfield: SRTTOF.RTVAL 
 * \param driver Device driver object  
 * \param value Gate Regular TTOFF delay  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setGateRegularTtoffDelay(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status);

/** \brief Get Gate Safe TTOFF Plateau Voltage
 * Bitfield: SSTTOF.GPS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Gate Safe TTOFF Plateau Voltage  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getGateSafeTtoffPlateauVoltage(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Gate Safe TTOFF Plateau Voltage
 * Bitfield: SSTTOF.GPS 
 * \param driver Device driver object  
 * \param value Gate Safe TTOFF Plateau Voltage  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setGateSafeTtoffPlateauVoltage(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status);

/** \brief Get Gate Safe TTOFF delay
 * Bitfield: SSTTOF.STVAL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Gate Safe TTOFF delay  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getGateSafeTtoffDelay(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Gate Safe TTOFF delay
 * Bitfield: SSTTOF.STVAL 
 * \param driver Device driver object  
 * \param value Gate Safe TTOFF delay  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setGateSafeTtoffDelay(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status);

/** \brief Get Gate TTON Delay
 * Bitfield: STTON.TTONVAL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Gate TTON Delay  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getGateTtonDelay(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set Gate TTON Delay
 * Bitfield: STTON.TTONVAL 
 * \param driver Device driver object  
 * \param value Gate TTON Delay  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setGateTtonDelay(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status);

/** \brief Get ADC Result Valid Flag
 * Bitfield: SADC.AVFS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Result Valid Flag  
 */
IFX_EXTERN Ifx1edi2010as_AdcResultValidFlag Ifx1edi2010as_getAdcResultValidFlag(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Undervoltage Error Status
 * Bitfield: SADC.AUVS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Undervoltage Error Status  
 */
IFX_EXTERN Ifx1edi2010as_AdcUndervoltageErrorStatus Ifx1edi2010as_getAdcUndervoltageErrorStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Undervoltage Error Status
 * Bitfield: SADC.AUVS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isAdcUndervoltageError(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Overvoltage Error Status
 * Bitfield: SADC.AOVS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Overvoltage Error Status  
 */
IFX_EXTERN Ifx1edi2010as_AdcOvervoltageErrorStatus Ifx1edi2010as_getAdcOvervoltageErrorStatus(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Overvoltage Error Status
 * Bitfield: SADC.AOVS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return TRUE if test is successful else false  
 */
IFX_EXTERN boolean Ifx1edi2010as_isAdcOvervoltageError(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Result
 * Bitfield: SADC.ADCVAL 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Result  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getAdcResult(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Get ADC Limit Checking Boundary A
 * Bitfield: SBC.LCB1A 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Limit Checking Boundary A  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getAdcLimitCheckingBoundaryA(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Limit Checking Boundary A
 * Bitfield: SBC.LCB1A 
 * \param driver Device driver object  
 * \param value ADC Limit Checking Boundary A  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setAdcLimitCheckingBoundaryA(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status);

/** \brief Get ADC Limit Checking Boundary B
 * Bitfield: SBC.LCB1B 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return ADC Limit Checking Boundary B  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getAdcLimitCheckingBoundaryB(Ifx1edi2010as_Driver* driver, boolean* status);

/** \brief Set ADC Limit Checking Boundary B
 * Bitfield: SBC.LCB1B 
 * \param driver Device driver object  
 * \param value ADC Limit Checking Boundary B  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return None
 */
IFX_EXTERN void Ifx1edi2010as_setAdcLimitCheckingBoundaryB(Ifx1edi2010as_Driver* driver, uint8 value, boolean* status);

/** \brief Get Secondary Supervision Oscillator Clock Cycles
 * Bitfield: SCS.SCSS 
 * \param driver Device driver object  
 * \param status The pointed value is set to FALSE if an communication error occurred, else unchanged. The pointed value must be initialized before the call to the function  
 * \return Secondary Supervision Oscillator Clock Cycles  
 */
IFX_EXTERN uint8 Ifx1edi2010as_getSecondarySupervisionOscillatorClockCycles(Ifx1edi2010as_Driver* driver, boolean* status);

/** \} */  


/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \brief Register information dump 
 * \param address Register address  
 * \param value Register value  
 * \param text Returned information text  
 * \param maxLength Text max length  
 * \return TRUE in case of success else FALSE  
 */
IFX_EXTERN boolean Ifx1edi2010as_registerDump(Ifx1edi2010as_Address address, Ifx1edi2010as_UData value, char* text, size_t maxLength);

/******************************************************************************/
/*-------------------Global Exported Variables/Constants----------------------*/
/******************************************************************************/

/** \brief Action read register flags
 *  Bitfield list:  
 */
IFX_EXTERN IFX_CONST uint32 Ifx1edi2010as_g_actionReadRegisters[1];

/** \brief Action write register flags
 *  Bitfield list:
 * - PCTRL: CLRP, CLRS
 * - PCTRL2: ACRP
 * - SCFG: CFG2  
 */
IFX_EXTERN IFX_CONST uint32 Ifx1edi2010as_g_actionWriteRegisters[1];

/** \brief No operation masks  
 */
IFX_EXTERN IFX_CONST Ifx_1EDI2010AS Ifx1edi2010as_g_noOpMask;

/** \brief No operation values  
 */
IFX_EXTERN IFX_CONST Ifx_1EDI2010AS Ifx1edi2010as_g_noOpValue;

/** \brief Valid address flags  
 */
IFX_EXTERN IFX_CONST uint32 Ifx1edi2010as_g_validAddress[1];

/** \brief Volatile register flags
 *  Bitfield list:
 * - PSTAT: GPOFP, AVFP, SRDY, ACT, GPONP, ERR
 * - PSTAT2: ENVAL, FLTA, FLTB, OPMP, FLTAP, FLTBP, STP, AXVP
 * - PER: CERP, ADER, RES, SPIER, STPER, ENER, RSTP, RSTEP
 * - PCTRL: CLRP, CLRS
 * - PCTRL2: ACRP
 * - PSCR: VFSP
 * - PPIN: INPL, INSTPL, ENL, NFLTAL, NFLTBL, ADCTL, DIO1L
 * - PCS: CSP
 * - SSTAT: PWM, RES, DBG, RES
 * - SSTAT2: DSATC, OCPC, UVLO2M, DIO2L, DACL
 * - SER: CERS, AUVER, AOVER, VMTO, UVLO2ER, DESATER, OCPER, RSTS
 * - SCFG: CFG2
 * - SSCR: VFS2
 * - SADC: AVFS, AUVS, AOVS, ADCVAL
 * - SCS: SCSS  
 */
IFX_EXTERN IFX_CONST uint32 Ifx1edi2010as_g_volatileRegisters[1];


#endif /* IFX1EDI2010AS_H */
