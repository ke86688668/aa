/**
 * \file Ifx1edi2010as_Hal.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 */
#include "SysSe/Ext/1edi2010as/Hal/Ifx1edi2010as_Hal.h"
#include "SysSe/Ext/1edi2010as/Driver/Ifx1edi2010as_Driver.h"
#include "SysSe/Bsp/Bsp.h"

#include "SysSe/Comm/Ifx_Console.h"
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

boolean Ifx1edi2010as_Hal_exchangeSpi(Ifx1edi2010as_Spi *spiChannel, const void *src, void *dest, uint8 length)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
    if (Ifx1edi2010as_g_showSpiFrame)
    {
        uint8   i;
        uint16 *p = (uint16 *)src;

        Ifx1edi2010as_Hal_print("{S:");
        for (i = 0; i < length; i++)
        {
            Ifx1edi2010as_Hal_print(" 0x%04X", p[i]);
        }
    }
#endif

    result &= SpiIf_exchange(spiChannel, src, dest, length) == SpiIf_Status_ok;

    if (result)
    {
        result &= SpiIf_waitWithTimeout(spiChannel, IFX1EDI2010AS_TIME_10MS);
    }

#if IFX_CFG_1EDI2010AS_DEBUG_SPI
    if (Ifx1edi2010as_g_showSpiFrame)
    {
        uint8   i;
        uint16 *p = (uint16 *)dest;

        Ifx1edi2010as_Hal_print(" R:");
        for (i = 0; i < length; i++)
        {
            Ifx1edi2010as_Hal_print(" 0x%04X", p[i]);
        }
        Ifx1edi2010as_Hal_print("}");
    }
#endif
    return result;
}


void Ifx1edi2010as_Hal_initPinOut(Ifx1edi2010as_Pin *pin, IfxPort_Mode mode, IfxPort_PadDriver driver, Ifx1edi2010as_PinState state)
{
    IfxPort_setPinMode(pin->port, pin->pinIndex, mode);
    IfxPort_setPinPadDriver(pin->port, pin->pinIndex, driver);
    Ifx1edi2010as_Hal_setPortState(pin, state);
}


void Ifx1edi2010as_Hal_initPinInp(Ifx1edi2010as_Pin *pin, IfxPort_Mode mode)
{
    IfxPort_setPinMode(pin->port, pin->pinIndex, mode);
}


boolean Ifx1edi2010as_Hal_init(Ifx1edi2010as_Hal_Config *config)
{
    boolean result = TRUE;

    if (config->rstN.port != NULL_PTR)
    {
        if ((config->rstN.port == config->rdy.port) && (config->rstN.pinIndex == config->rdy.pinIndex))
        {
            Ifx1edi2010as_Hal_initPinOut(&config->rstN, IfxPort_Mode_outputOpenDrainGeneral, IfxPort_PadDriver_cmosAutomotiveSpeed1,
                config->rstNActiveState == Ifx_ActiveState_low ? Ifx1edi2010as_PinState_low : Ifx1edi2010as_PinState_high);
        }
        else
        {
            Ifx1edi2010as_Hal_initPinOut(&config->rstN, IfxPort_Mode_outputPushPullGeneral, IfxPort_PadDriver_cmosAutomotiveSpeed1,
                config->rstNActiveState == Ifx_ActiveState_low ? Ifx1edi2010as_PinState_low : Ifx1edi2010as_PinState_high);
        }
    }

    if ((config->rdy.port != NULL_PTR) && !((config->rstN.port == config->rdy.port) && (config->rstN.pinIndex == config->rdy.pinIndex)))
    {
        Ifx1edi2010as_Hal_initPinInp(&config->rdy, IfxPort_Mode_inputNoPullDevice);
    }

    if (config->en.port != NULL_PTR)
    {
        Ifx1edi2010as_Hal_initPinOut(&config->en, IfxPort_Mode_outputPushPullGeneral, IfxPort_PadDriver_cmosAutomotiveSpeed1, Ifx1edi2010as_PinState_low);
    }

    if (config->fltAN.port != NULL_PTR)
    {
        Ifx1edi2010as_Hal_initPinInp(&config->fltAN, IfxPort_Mode_inputNoPullDevice);
    }

    if (config->fltBN.port != NULL_PTR)
    {
        Ifx1edi2010as_Hal_initPinInp(&config->fltBN, IfxPort_Mode_inputNoPullDevice);
    }

    return result;
}


void Ifx1edi2010as_Hal_initConfig(Ifx1edi2010as_Hal_Config *config)
{
    config->channel         = NULL_PTR;
    config->en.port         = NULL_PTR;
    config->en.pinIndex     = 0;
    config->fltAN.port      = NULL_PTR;
    config->fltAN.pinIndex  = 0;
    config->fltBN.port      = NULL_PTR;
    config->fltBN.pinIndex  = 0;
    config->rdy.port        = NULL_PTR;
    config->rdy.pinIndex    = 0;
    config->rstN.port       = NULL_PTR;
    config->rstN.pinIndex   = 0;
    config->rstNActiveState = Ifx_ActiveState_low;
}


boolean Ifx1edi2010as_Hal_print(pchar format, ...)
{
    if (!Ifx_g_console.standardIo->txDisabled)
    {
        char      message[STDIF_DPIPE_MAX_PRINT_SIZE + 1];
        Ifx_SizeT count;
        va_list   args;
        va_start(args, format);
        vsprintf((char *)message, format, args);
        va_end(args);
        count = (Ifx_SizeT)strlen(message);
        IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, count < STDIF_DPIPE_MAX_PRINT_SIZE);

        return IfxStdIf_DPipe_write(Ifx_g_console.standardIo, (void *)message, &count, TIME_INFINITE);
    }
    else
    {
        return TRUE;
    }
}
