/**
 * \file Ifx1edi2010as_Hal.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup IfxLld_1edi2010as_Hal Hardware abstraction layer
 * \ingroup IfxLld_1edi2010as
 *
 * This layer abstracts the SPI and port modules used to interact with the device.
 * An example is provided here using the AURIX iLLD, those files (Ifx1edi2010as_Hal.*) must be adapteed to the user environment.
 */

#ifndef IFX1EDI2010AS_HAL_H
#define IFX1EDI2010AS_HAL_H 1

#include "Ifx_Cfg.h"
#include "Platform_Types.h"
#include "Compilers.h"
#include "If/SpiIf.h"
#include "IfxPort.h"

#ifndef IFX_CFG_1EDI2010AS_DEBUG
#define IFX_CFG_1EDI2010AS_DEBUG (0)
#endif

#ifndef IFX_CFG_1EDI2010AS_DEBUG_SPI
#define IFX_CFG_1EDI2010AS_DEBUG_SPI (0)
#endif

#ifndef _1EDI2010AS_ENDL
#define _1EDI2010AS_ENDL         "\r\n"
#endif

/** SPI object definition
 * Ifx1edi2010as_Spi is an handle used to identify the SPI used for communcation with the device
 */
typedef SpiIf_Ch Ifx1edi2010as_Spi;

/** Pin object definition
 * Ifx1edi2010as_Pin is a handle used to identify a digital input / output used for controlling the device
 */
typedef IfxPort_Pin Ifx1edi2010as_Pin;

/** Pin action definition
 *
 */
typedef enum
{
    Ifx1edi2010as_PinState_high = IfxPort_State_high,                /**< \brief Set the pin high */
    Ifx1edi2010as_PinState_low  = IfxPort_State_low,                 /**< \brief Set the pin low */
} Ifx1edi2010as_PinState;

typedef Ifx_TickTime Ifx1edi2010as_Time;                             /**< \brief time type, this type should allows time definition below 1s */

#define IFX1EDI2010AS_TIME_1S   ((Ifx1edi2010as_Time)TimeConst_1s)   /**< \brief value that represent a time of 1s */
#define IFX1EDI2010AS_TIME_10MS ((Ifx1edi2010as_Time)TimeConst_10ms) /**< \brief value that represent a time of 10ms, used by \ref SpiIf_waitWithTimeout() */
#define IFX1EDI2010AS_TIME_10US ((Ifx1edi2010as_Time)TimeConst_10us) /**< \brief value that represent a time of 10us */

/** Hal configuration
 *
 */
typedef struct
{
    Ifx1edi2010as_Spi *channel;
    Ifx1edi2010as_Pin  fltAN;           /**< \brief FLTA\\*/
    Ifx1edi2010as_Pin  fltBN;           /**< \brief FLTB\\*/
    Ifx1edi2010as_Pin  rstN;            /**< \brief RSTN / RDY => output. if only one signal is used for reset and  ready, rstN and rdy should have the same value */
    Ifx_ActiveState    rstNActiveState; /**< \brief RSTN active state */
    Ifx1edi2010as_Pin  rdy;             /**< \brief RSTN / RDY  => input.  if only one signal is used for reset and  ready, rstN and rdy should have the same value */
    Ifx1edi2010as_Pin  en;              /**< \brief EN */
    Ifx1edi2010as_Pin  PBen;			/* Jimmy Create for Power Board Enable pin */

}Ifx1edi2010as_Hal_Config;

/* Pure virtual function. to be implemented outside of the driver */

/** \addtogroup IfxLld_1edi2010as_Hal
 * Those API must be adapted to the mircocontroller / software framework
 * \{ */

/** Perform an SPI data exchange operation (send/receive).
 *
 * This function send the data pointer by src and store the data received to dest.
 * The number of data element to be send/received is specified by length.
 *
 * \param spiChannel SPI channel handle used for data transmission
 * \param src Pointer to the start of transmit data buffer
 * \param dest Pointer to the start of received data buffer
 * \param length Number of SPI data element to be transfered
 *
 * \return TRUE in case of success else FALSE
 *
 * \note the src and dest may be the same data location.
 * \note The function body has to be filled in depending on the microcontroller and SPI driver used. An example is provided for the iLLD only.
 */
boolean Ifx1edi2010as_Hal_exchangeSpi(Ifx1edi2010as_Spi *spiChannel, const void *src, void *dest, uint8 length);

/** Microcontroller hardware initialization
 *
 * This function initializes the following:
 * - RSTN port as output, active state
 * - RDY port as input (only if port is different from RSTN)
 * - EN port as output, incative state
 * - FLTAN port as input
 * - FLTBN port as input
 *
 * \param config Configuration data
 * \return Returns TRUE in case of success, else FALSE
 */
boolean Ifx1edi2010as_Hal_init(Ifx1edi2010as_Hal_Config *config);

/** Initialize the configuration structure to default values
 * \param config HAL configuration
 */
void Ifx1edi2010as_Hal_initConfig(Ifx1edi2010as_Hal_Config *config);

/** Set the port output to high level
 *
 * \param port Port handle
 *
 */
IFX_INLINE void Ifx1edi2010as_Hal_setPortHigh(Ifx1edi2010as_Pin *port)
{
    IfxPort_setPinState(port->port, port->pinIndex, IfxPort_State_high);
}


/** Set the port output to low level
 *
 * \param pin Port handle
 *
 */
IFX_INLINE void Ifx1edi2010as_Hal_setPortLow(Ifx1edi2010as_Pin *port)
{
    IfxPort_setPinState(port->port, port->pinIndex, IfxPort_State_low);
}


/** Set the port output to a defined level
 *
 * \param port Port handle
 * \param action Action to be done on the port
 *
 */
IFX_INLINE void Ifx1edi2010as_Hal_setPortState(Ifx1edi2010as_Pin *port, Ifx1edi2010as_PinState state)
{
    IfxPort_setPinState(port->port, port->pinIndex, (IfxPort_State)state);
}


/** Get the port input level
 *
 * \param port Port handle
 *
 * \return Return TRUE for level high and FALSE for level LOW
 *
 */
IFX_INLINE boolean Ifx1edi2010as_Hal_getPortState(Ifx1edi2010as_Pin *port)
{
    return IfxPort_getPinState(port->port, port->pinIndex);
}


/** Wait for the given timeout
 * The function returns once the timeout elapsed.
 *
 * \param Timeout Time value before the function returns
 *
 * \return None
 *
 */
IFX_INLINE void Ifx1edi2010as_Hal_wait(Ifx1edi2010as_Time timeout)
{
//	  20181212 disbale by Jimmy
//    wait(timeout);
}


/**
 * \brief Print formatted string
 * \param format printf-compatible formatted string.
 * \retval TRUE if the string is printed successfully
 * \retval FALSE if the function failed.
 */
boolean Ifx1edi2010as_Hal_print(pchar format, ...);
/** \} */

#endif /* IFX1EDI2010AS_HAL_H */
