/**
 * \file Ifx1edi2010as_Driver.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup IfxLld_1edi2010as_Driver 1EDI2010AS Driver
 * \ingroup IfxLldExt
 *
 * \image html "GcG_Logo_small.png" "GcG"
 *
 * The Ifx1edi2010as driver provides an interface for handling the 1EDI2010AS device.
 *
 * It is based on the \subpage doc_gcgBasedDriver "GcG device driver", and requires and
 * adaption of the HAL part (\ref Ifx1edi2010as_Hal.c & \ref Ifx1edi2010as_Hal.h) to the microcontroller
 * and framework used. By default the HAL is provided for the AURIX iLLD.
 *
 * The driver optionally supports the \ref library_srvsw_sysse_general_devicecache "experimental register caching feature" which
 * optimizes the SPI interface usage, by caching device registers to local RAM. The feature can be enabled / disabled with the \ref IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE switch.
 *
 * The driver is initialized with \ref Ifx1edi2010as_Driver_init() to which a configuration
 * structure is passed. This configuration structure must be set to default with
 * \ref Ifx1edi2010as_Driver_initConfig() before use.
 *
 * The driver supports daisy chained device.
 * To initialize the daisy chain, each of the devices must be initialize using the \ref Ifx1edi2010as_Driver_init() API, starting with the 1st device in the chain.
 *
 * Below an example of driver initialization:
 * \code
 * boolean result = TRUE;
 *
 * {
 *     // 1EDI2010AS device driver initialization
 *     Ifx1edi2010as_Driver_Config config;
 *     Ifx1edi2010as_Driver_initConfig(&config);
 *     config.hal.channel            = &g_1edi2010as.drivers.sscDeviceChannel.base;
 *     config.resetDuration          = IFX1EDI2010AS_TIME_10MS *10;
 *
 *     // Top
 *     config.hal.en                 = (Ifx1edi2010as_Pin) {&MODULE_P15, 8};
 *     config.hal.rstN               = (Ifx1edi2010as_Pin) {&MODULE_P00, 4};
 *     config.hal.rstNActiveState    = Ifx_ActiveState_high;
 *     config.hal.rdy                = (Ifx1edi2010as_Pin) {&MODULE_P10, 8};
 *     config.hal.fltAN              = (Ifx1edi2010as_Pin) {&MODULE_P00, 5};
 *     config.hal.fltBN              = (Ifx1edi2010as_Pin) {&MODULE_P00, 12};
 *     config.previousDevice = NULL_PTR;
 *
 *     Ifx_Console_print("Device 0:"ENDL);
 *     Ifx_Console_print(" - EN=P%d.%d"ENDL, IfxPort_getIndex(config.hal.en.port), config.hal.en.pinIndex);
 *     Ifx_Console_print(" - RSTN/RDY (output)=P%d.%d"ENDL, IfxPort_getIndex(config.hal.rstN.port), config.hal.rstN.pinIndex);
 *     Ifx_Console_print(" - RSTN/RDY (input)=P%d.%d"ENDL, IfxPort_getIndex(config.hal.rdy.port), config.hal.rdy.pinIndex);
 *     Ifx_Console_print(" - NFLTA=P%d.%d"ENDL, IfxPort_getIndex(config.hal.fltAN.port), config.hal.fltAN.pinIndex);
 *     Ifx_Console_print(" - NFLTB=P%d.%d"ENDL, IfxPort_getIndex(config.hal.fltBN.port), config.hal.fltBN.pinIndex);
 *
 *     if (!Ifx1edi2010as_Driver_init(&g_1edi2010as.drivers.device[0], &config))
 *     {
 *         Ifx_Console_print(" - Error during 1EDI2010AS driver initialization device 0"ENDL);
 *         result = FALSE;
 *     }
 *
 *     // Bottom
 *     config.hal.en              = (Ifx1edi2010as_Pin) {&MODULE_P14, 0};
 *     config.hal.rstN            = (Ifx1edi2010as_Pin) {&MODULE_P15, 5};
 *     config.hal.rstNActiveState = Ifx_ActiveState_high;
 *     config.hal.rdy             = (Ifx1edi2010as_Pin) {&MODULE_P15, 1};
 *     config.hal.fltAN           = (Ifx1edi2010as_Pin) {&MODULE_P13, 0};
 *     config.hal.fltBN           = (Ifx1edi2010as_Pin) {&MODULE_P13, 1};
 *     config.previousDevice = &g_1edi2010as.drivers.device[0];
 *
 *     Ifx_Console_print("Device 1:"ENDL);
 *     Ifx_Console_print(" - EN=P%d.%d"ENDL, IfxPort_getIndex(config.hal.en.port), config.hal.en.pinIndex);
 *     Ifx_Console_print(" - RSTN/RDY (output)=P%d.%d"ENDL, IfxPort_getIndex(config.hal.rstN.port), config.hal.rstN.pinIndex);
 *     Ifx_Console_print(" - RSTN/RDY (input)=P%d.%d"ENDL, IfxPort_getIndex(config.hal.rdy.port), config.hal.rdy.pinIndex);
 *     Ifx_Console_print(" - NFLTA=P%d.%d"ENDL, IfxPort_getIndex(config.hal.fltAN.port), config.hal.fltAN.pinIndex);
 *     Ifx_Console_print(" - NFLTB=P%d.%d"ENDL, IfxPort_getIndex(config.hal.fltBN.port), config.hal.fltBN.pinIndex);
 *     if (!Ifx1edi2010as_Driver_init(&g_1edi2010as.drivers.device[1], &config))
 *     {
 *         Ifx_Console_print(" - Error during 1EDI2010AS driver initialization device 1"ENDL);
 *         result = FALSE;
 *     }
 *
 * }
 *
 * {
 *     // 1EDI2010AS device configuration
 *     Ifx1edi2010as_Driver_reset(&g_1edi2010as.drivers.device[0], 0x3);
 *
 *     wait(TimeConst_1ms);
 *
 *     if (Ifx1edi2010as_Driver_isAnyDeviceNotReady(&g_1edi2010as.drivers.device[0]) == 0)
 *     {
 *         uint8                       index;
 *         Ifx1edi2010as_OperatingMode mode;
 *
 *         // Read Primary and secondary IDs
 *         for (index = 0; index < g_1edi2010as.drivers.device[0].daisyChain.length; index++)
 *         {
 *             g_1edi2010as.info.device[index].primaryId   = Ifx1edi2010as_getPrimaryChipIdentification(&g_1edi2010as.drivers.device[0], &result);
 *             g_1edi2010as.info.device[index].secondaryId = Ifx1edi2010as_getSecondaryChipIdentification(&g_1edi2010as.drivers.device[0], &result);
 *         }
 *
 *         // Check secondary ready flag and clear primary and secondary sticky bits
 *         for (index = 0; index < g_1edi2010as.drivers.device[0].daisyChain.length; index++)
 *         {
 *             boolean ready;
 *             ready                                       = Ifx1edi2010as_isSecondaryReady(&g_1edi2010as.drivers.device[index], &result);
 *             result                                     &= ready;
 *
 *             mode                                        = Ifx1edi2010as_getOperatingMode(&g_1edi2010as.drivers.device[index], &result);
 *             result                                     &= mode == Ifx1edi2010as_OperatingMode_opm0;
 *
 *             // Clear PCTRL.CLRP and CLRS
 *             Ifx1edi2010as_clearPrimarySitckyBits(&g_1edi2010as.drivers.device[index], &result);
 *             Ifx1edi2010as_clearSecondarySitckyBits(&g_1edi2010as.drivers.device[index], &result);
 *         }
 *
 *         // Write register modification to devices, this is required if cache of volatile register is enabled
 *         result &= Ifx1edi2010as_Driver_writeSync(&g_1edi2010as.drivers.device[0]);
 *
 *         if (result)
 *         {
 *             result &= Ifx1edi2010as_Driver_enterConfigurationMode(&g_1edi2010as.drivers.device[0], 0x3);
 *
 *             if (result)
 *             {
 *                 // Write custom configuration
 *                 for (index = 0; index < g_1edi2010as.drivers.device[0].daisyChain.length; index++)
 *                 {
 *                     mode                               = Ifx1edi2010as_getOperatingMode(&g_1edi2010as.drivers.device[index], &result);
 *
 *                     result                            &= mode == Ifx1edi2010as_OperatingMode_opm2;
 *
 *                     Ifx1edi2010as_enableSecondaryAdvancedConfiguration(&g_1edi2010as.drivers.device[index], &result);
 *                 }
 *
 *                 result &= Ifx1edi2010as_Driver_writeSync(&g_1edi2010as.drivers.device[0]);
 *
 *                 {
 *                     // Make Advanced configuration here
 *                     result &= Ifx1edi2010as_Driver_writeSync(&g_1edi2010as.drivers.device[0]);
 *                 }
 *
 *                 for (index = 0; index < g_1edi2010as.drivers.device[0].daisyChain.length; index++)
 *                 {
 *                     Ifx1edi2010as_disableSecondaryAdvancedConfiguration(&g_1edi2010as.drivers.device[index], &result);
 *                 }
 *
 *                 // No need for Ifx1edi2010as_Driver_writeSync() as this is automatically done by Ifx1edi2010as_Driver_exitConfigurationMode()
 *
 *                 // Exit Configuration mode
 *                 result &= Ifx1edi2010as_Driver_exitConfigurationMode(&g_1edi2010as.drivers.device[0], 0x3);
 *
 *                 if (result)
 *                 {
 *                     for (index = 0; index < g_1edi2010as.drivers.device[0].daisyChain.length; index++)
 *                     {
 *                         mode                               = Ifx1edi2010as_getOperatingMode(&g_1edi2010as.drivers.device[index], &result);
 *                         result                            &= mode == Ifx1edi2010as_OperatingMode_opm3;
 *                     }
 *
 *                     if (result)
 *                     {
 *                     	Ifx1edi2010as_Driver_invalidateCacheVolatile(&g_1edi2010as.drivers.device[0]);
 *
 *                         // Enable the driver
 *                         Ifx1edi2010as_Driver_enable(&g_1edi2010as.drivers.device[0], 0x3);
 *                         for (index = 0; index < g_1edi2010as.drivers.device[0].daisyChain.length; index++)
 *                         {
 *                             mode                               = Ifx1edi2010as_getOperatingMode(&g_1edi2010as.drivers.device[index], &result);
 *                             result                            &= mode == Ifx1edi2010as_OperatingMode_opm4;
 *                         }
 *                     }
 *                 }
 *             }
 *         }
 *     }
 * }
 * return result;
 *
 * \endcode
 *
 * All device features can be accessed through getter and setter API available in \ref IfxLld_1edi2010as_Std_Std.
 *
 * Dump of register can be done with \ref Ifx1edi2010as_Driver_dumpRegisters(), this feature is only available if the cache is enabled.
 * Below an example of register dump:
 * \code
 * 1EDI2010 (0) register dump:
 * PID @ 0x0 = 0x4911 = [P:0x1, LMI:0x0, PVERS:0x491]
 * PSTAT @ 0x1 = 0x7D4 = [P:0x0, LMI:0x0, GPOFP:0x5, AVFP:0x0, RES:0x0, SRDY:0x1, ACT:0x1, GPONP:0x7, ERR:0x0]
 * PSTAT2 @ 0x2 = 0x85 = [P:0x1, LMI:0x0, ENVAL:0x1, FLTA:0x0, FLTB:0x0, OPMP:0x4, FLTAP:0x0, FLTBP:0x0, STP:0x0, AXVP:0x0, RES:0x0]
 * PER @ 0x3 = 0x1 = [P:0x1, LMI:0x0, CERP:0x0, ADER:0x0, RES:0x0, VMTOP:0x0, SPIER:0x0, STPER:0x0, ENER:0x0, RSTP:0x0, RSTEP:0x0]
 * PCFG @ 0x4 = 0x4 = [P:0x0, LMI:0x0, PAREN:0x1, CLFAM:0x0, CLFBM:0x0, ADAEN:0x0, RES:0x0, ADTEN:0x0, RES:0x0]
 * PCFG2 @ 0x5 = 0x45 = [P:0x1, LMI:0x0, STPDEL:0x11, DIO1:0x0]
 * PCTRL @ 0x6 = 0x1C = [P:0x0, LMI:0x0, GPON:0x7, CLRP:0x0, CLRS:0x0]
 * PCTRL2 @ 0x7 = 0x15 = [P:0x1, LMI:0x0, GPOF:0x5, ACRP:0x0, RES:0x0]
 * PSCR @ 0x8 = 0x1 = [P:0x1, LMI:0x0, VFSP:0x0]
 * PRW @ 0x9 = 0x1 = [P:0x1, LMI:0x0, RWVAL:0x0]
 * PPIN @ 0xA = 0x75 = [P:0x1, LMI:0x0, INPL:0x1, INSTPL:0x0, ENL:0x1, NFLTAL:0x1, NFLTBL:0x1, ADCTL:0x0, DIO1L:0x0]
 * PCS @ 0xB = 0x1 = [P:0x1, LMI:0x0, CSP:0x0]
 * SID @ 0x10 = 0x8911 = [P:0x1, LMI:0x0, SVERS:0x891]
 * SSTAT @ 0x11 = 0x211 = [P:0x1, LMI:0x0, PWM:0x1, FLTAS:0x0, FLTBS:0x0, OPMS:0x4, DBG:0x0, VMTOA:0x0, RES:0x0]
 * SSTAT2 @ 0x12 = 0x100 = [P:0x0, LMI:0x0, DSATC:0x0, OCPC:0x0, UVLO2M:0x0, DIO2L:0x0, DACL:0x1]
 * SER @ 0x13 = 0x1 = [P:0x1, LMI:0x0, CERS:0x0, AUVER:0x0, RES:0x0, AOVER:0x0, VMTO:0x0, UVLO2ER:0x0, DESATER:0x0, OCPER:0x0, RSTS:0x0]
 * SCFG @ 0x14 = 0xC111 = [P:0x1, LMI:0x0, VBEC:0x1, CFG2:0x0, RES:0x0, DIO2C:0x0, DSTCEN:0x1, PSEN:0x0, TOSEN:0x0, DSATLS:0x0, RES:0x0, UVLO2S:0x0, OCPLS:0x0, RES:0x0, DACLC:0x3]
 * SCFG2 @ 0x15 = 0x800 = [P:0x0, LMI:0x0, PWMD:0x0, ATS:0x0, AGS:0x0, AOS:0x2, ACSS:0x0, ACAEN:0x0, ADCEN:0x0]
 * SCTRL @ 0x16 = 0x3D0 = [P:0x0, LMI:0x0, GPOFS:0x5, GPONS:0x7, ARS:0x0, RES:0x0, CLRS:0x0]
 * SSCR @ 0x17 = 0x1 = [P:0x1, LMI:0x0, VFS2:0x0]
 * SDESAT @ 0x18 = 0x2000 = [P:0x0, LMI:0x0, DSATBT:0x20]
 * SOCP @ 0x19 = 0x1 = [P:0x1, LMI:0x0, OCPBT:0x0]
 * SRTTOF @ 0x1A = 0x1 = [P:0x1, LMI:0x0, RTVAL:0x0]
 * SSTTOF @ 0x1B = 0x2081 = [P:0x1, LMI:0x0, GPS:0x4, STVAL:0x20]
 * STTON @ 0x1C = 0x1 = [P:0x1, LMI:0x0, TTONVAL:0x0]
 * SADC @ 0x1D = 0x1 = [P:0x1, LMI:0x0, AVFS:0x0, AUVS:0x0, AOVS:0x0, ADCVAL:0x0]
 * SBC @ 0x1E = 0xFC01 = [P:0x1, LMI:0x0, LCB1A:0x0, LCB1B:0x3F]
 * SCS @ 0x1F = 0x1 = [P:0x1, LMI:0x0, SCSS:0x0]
 * \endcode
 *
 *
 * For debugging purpose, SPI frame exchanged with the device can be monitored using the \ref IFX_CFG_1EDI2010AS_DEBUG switch.
 *
 *
 * Below is an SPI log with <b>cache enabled</b> for the above initialization example:
 * \code
 * Ifx1edi2010as_Driver_init()
 * {}
 *
 * Ifx1edi2010as_Driver_reset()
 * {
 * Ifx1edi2010as_Driver_switchOff()
 * {}
 * Ifx1edi2010as_Driver_switchOn()
 * {}
 * }
 *
 * 1EDI2010AS Read @ 0x00: [{S: 0x002A 0x002A R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x4911 0x4911}] => 0x4910; 0x4910;
 * 1EDI2010AS Read @ 0x10: [{S: 0x082B 0x082B R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x8911 0x8911}] => 0x8910; 0x8910;
 * 1EDI2010AS Read @ 0x01: [{S: 0x00AB 0x00AB R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x0F54 0x0F54}] => 0xF54; 0xF54;
 * 1EDI2010AS Read @ 0x02: [{S: 0x012B 0x012B R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x0010 0x0010}] => 0x10; 0x10;
 * 1EDI2010AS Read @ 0x06: [{S: 0x032A 0x032A R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x001C 0x001C}] => 0x1C; 0x1C;
 * 
 * Ifx1edi2010as_Driver_enterConfigurationMode()
 * {
 *     1EDI2010AS Write @ 0x06: 0x7C; 0x7C; [{S: 0x4401 0x4401 R: 0x0F54 0x0F54}{S: 0xA33E 0xA33E R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 *
 *     1EDI2010AS enter C MODE: YES; YES; [{S: 0x1880 0x1880 R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 * }
 *
 * 1EDI2010AS Read @ 0x02: [{S: 0x012B 0x012B R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0040 0x0040}] => 0x40; 0x40;
 * 1EDI2010AS Read @ 0x14: [{S: 0x0A2A 0x0A2A R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0xC111 0xC111}] => 0xC110; 0xC110;
 *
 * Ifx1edi2010as_Driver_writeSync()
 * {
 *     1EDI2010AS Write @ 0x14: 0xC130; 0xC130; [{S: 0x4582 0x4582 R: 0x0755 0x0755}{S: 0xAA19 0xAA19 R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 * }
 *
 * Ifx1edi2010as_Driver_writeSync()
 * {}
 *
 * Ifx1edi2010as_Driver_exitConfigurationMode()
 * {
 *
 *     1EDI2010AS Write @ 0x14: 0xC110; 0xC110; [{S: 0x4582 0x4582 R: 0x0755 0x0755}{S: 0xAA08 0xAA08 R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 *     1EDI2010AS exit C MODE: YES; YES; [{S: 0x1220 0x1220 R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 * }
 *
 * 1EDI2010AS Read @ 0x02: [{S: 0x012B 0x012B R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0061 0x0061}] => 0x60; 0x60;
 * Ifx1edi2010as_Driver_enable()
 * {}
 *
 * 1EDI2010AS Read @ 0x02: [{S: 0x012B 0x012B R: 0x07D4 0x07D4}{S: 0x1410 0x1410 R: 0x0085 0x0085}] => 0x84; 0x84;
 *
 * \endcode
 * Below is an SPI log with <b>cache disabled</b> for the above initialization example:
 * \code
 * Ifx1edi2010as_Driver_init()
 * {}
 *
 * Ifx1edi2010as_Driver_reset()
 * {
 *     Ifx1edi2010as_Driver_switchOff()
 *     {}
 *     Ifx1edi2010as_Driver_switchOn()
 *     {}
 * }
 *
 * 1EDI2010AS Read @ 0x00: [{S: 0x002A 0x002A R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x4911 0x4911}] => 0x4910; 0x4910;
 * 1EDI2010AS Read @ 0x10: [{S: 0x082B 0x082B R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x8911 0x8911}] => 0x8910; 0x8910;
 * 1EDI2010AS Read @ 0x00: [{S: 0x002A 0x002A R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x4911 0x4911}] => 0x4910; 0x4910;
 * 1EDI2010AS Read @ 0x10: [{S: 0x082B 0x082B R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x8911 0x8911}] => 0x8910; 0x8910;
 * 1EDI2010AS Read @ 0x01: [{S: 0x00AB 0x00AB R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x0F54 0x0F54}] => 0xF54; 0xF54;
 * 1EDI2010AS Read @ 0x02: [{S: 0x012B 0x012B R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x0010 0x0010}] => 0x10; 0x10;
 * 1EDI2010AS Read @ 0x06: [{S: 0x032A 0x032A R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x001C 0x001C}] => 0x1C; 0x1C;
 * 1EDI2010AS Write @ 0x06: 0x3C; NOP; [{S: 0x1410 0x4401 R: 0x0F54 0x0F54}{S: 0x1410 0xA31F R: 0x0F54 0x0F54}{S: 0x1410 0x1410 R: 0x0F54 0x0755}]
 * 1EDI2010AS Read @ 0x06: [{S: 0x032A 0x032A R: 0x0F54 0x0755}{S: 0x1410 0x1410 R: 0x001C 0x001C}] => 0x1C; 0x1C;
 * 1EDI2010AS Write @ 0x06: 0x5C; NOP; [{S: 0x1410 0x4401 R: 0x0F54 0x0755}{S: 0x1410 0xA32F R: 0x0F54 0x0755}{S: 0x1410 0x1410 R: 0x0F54 0x0755}]
 * 1EDI2010AS Read @ 0x01: [{S: 0x00AB 0x00AB R: 0x0F54 0x0755}{S: 0x1410 0x1410 R: 0x0F54 0x0755}] => 0x754; 0xF54;
 * 1EDI2010AS Read @ 0x02: [{S: 0x012B 0x012B R: 0x0F54 0x0755}{S: 0x1410 0x1410 R: 0x0010 0x0001}] => 0x00; 0x10;
 * 1EDI2010AS Read @ 0x06: [{S: 0x032A 0x032A R: 0x0F54 0x0755}{S: 0x1410 0x1410 R: 0x001C 0x001C}] => 0x1C; 0x1C;
 * 1EDI2010AS Write @ 0x06: NOP; 0x3C; [{S: 0x4401 0x1410 R: 0x0F54 0x0755}{S: 0xA31F 0x1410 R: 0x0F54 0x0755}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 * 1EDI2010AS Read @ 0x06: [{S: 0x032A 0x032A R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x001C 0x001C}] => 0x1C; 0x1C;
 * 1EDI2010AS Write @ 0x06: NOP; 0x5C; [{S: 0x4401 0x1410 R: 0x0755 0x0755}{S: 0xA32F 0x1410 R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 *
 *
 * Ifx1edi2010as_Driver_enterConfigurationMode()
 * {
 *
 *     1EDI2010AS enter C MODE: YES; YES; [{S: 0x1880 0x1880 R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 * }
 *
 * 1EDI2010AS Read @ 0x02: [{S: 0x012B 0x012B R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0040 0x0040}] => 0x40; 0x40;
 * 1EDI2010AS Read @ 0x14: [{S: 0x0A2A 0x0A2A R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0xC111 0xC111}] => 0xC110; 0xC110;
 * 1EDI2010AS Write @ 0x14: 0xC130; NOP; [{S: 0x1410 0x4582 R: 0x0755 0x0755}{S: 0x1410 0xAA19 R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 * 1EDI2010AS Read @ 0x02: [{S: 0x012B 0x012B R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0040 0x0040}] => 0x40; 0x40;
 * 1EDI2010AS Read @ 0x14: [{S: 0x0A2A 0x0A2A R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0xC111 0xC130}] => 0xC130; 0xC110;
 * 1EDI2010AS Write @ 0x14: NOP; 0xC130; [{S: 0x4582 0x1410 R: 0x0755 0x0755}{S: 0xAA19 0x1410 R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 * 1EDI2010AS Read @ 0x14: [{S: 0x0A2A 0x0A2A R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0xC130 0xC130}] => 0xC130; 0xC130;
 * 1EDI2010AS Write @ 0x14: 0xC110; NOP; [{S: 0x1410 0x4582 R: 0x0755 0x0755}{S: 0x1410 0xAA08 R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 * 1EDI2010AS Read @ 0x14: [{S: 0x0A2A 0x0A2A R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0xC130 0xC111}] => 0xC110; 0xC130;
 * 1EDI2010AS Write @ 0x14: NOP; 0xC110; [{S: 0x4582 0x1410 R: 0x0755 0x0755}{S: 0xAA08 0x1410 R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 *
 *
 * Ifx1edi2010as_Driver_exitConfigurationMode()
 * {
 * 1EDI2010AS exit C MODE: YES; YES; [{S: 0x1220 0x1220 R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0755 0x0755}]
 * }
 * 1EDI2010AS Read @ 0x02: [{S: 0x012B 0x012B R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0061 0x0061}] => 0x60; 0x60;
 * 1EDI2010AS Read @ 0x02: [{S: 0x012B 0x012B R: 0x0755 0x0755}{S: 0x1410 0x1410 R: 0x0061 0x0061}] => 0x60; 0x60;
 *
 *
 * Ifx1edi2010as_Driver_enable()
 * {}
 *
 * 1EDI2010AS Read @ 0x02: [{S: 0x012B 0x012B R: 0x07D4 0x07D4}{S: 0x1410 0x1410 R: 0x0085 0x0085}] => 0x84; 0x84;
 * 1EDI2010AS Read @ 0x02: [{S: 0x012B 0x012B R: 0x07D4 0x07D4}{S: 0x1410 0x1410 R: 0x0085 0x0085}] => 0x84; 0x84;
 *
 * \endcode
 *
 *
 */


#ifndef IFX1EDI2010AS_DRIVER_H
#define IFX1EDI2010AS_DRIVER_H                 1

#include "Ifx_Cfg.h"
#ifndef IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
#define IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE (FALSE)
#endif

#include "SysSe/Ext/1edi2010as/_Impl/Ifx1edi2010as_cfg.h"
#include "SysSe/Ext/1edi2010as/_Reg/Ifx1edi2010as_reg.h"
#include "SysSe/Ext/1edi2010as/Hal/Ifx1edi2010as_Hal.h"

#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
#include "SysSe/General/Ifx_DeviceCache.h"
#endif

#ifndef IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH
#define IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH (6) /**<  Maximal length of the daisy chain.*/
#endif

typedef struct Ifx1edi2010as_Driver_ Ifx1edi2010as_Driver;
struct Ifx1edi2010as_Driver_
{
    Ifx1edi2010as_Spi *channel;

#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
    struct
    {
        /* Device specific part */
        Ifx_1EDI2010AS  cache;                                                         /**< \brief Cache of the device registers */
        Ifx_1EDI2010AS  bufferNoOpMaskActual; /**< \brief Device no operation mask (actual mask). This buffer is modified. */
        uint32          modificationMask[((sizeof(Ifx_1EDI2010AS) >> IFX1EDI2010AS_ADDRESS_SHIFT)-1)/32+1];   /**< \brief register modification mask. If bit with offset x is 1, corresponding register address x has been modified and need to be written to the device */
        uint32          cacheValid[((sizeof(Ifx_1EDI2010AS) >> IFX1EDI2010AS_ADDRESS_SHIFT)-1)/32+1];         /**< \brief register cache valid. If bit with offset x is 1, corresponding register cache x has is valid */
        uint32          writeBackErrorFlags[((sizeof(Ifx_1EDI2010AS) >> IFX1EDI2010AS_ADDRESS_SHIFT)-1)/32+1];       /**< \brief Register write back error flag array. If bit with offset x is 1, corresponding register had an error during write cache back. Only valid if Ifx1edi2010as_Driver_isWriteSyncError() returns true */
        Ifx_DeviceCache engine;
        uint16          tempCacheLine[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];            /** Buffer for the temporary cache line */
    } cache;
#endif

    struct
    {
        Ifx1edi2010as_Pin fltAN;           /**< \brief FLTA\\*/
        Ifx1edi2010as_Pin fltBN;           /**< \brief FLTB\\*/
        Ifx1edi2010as_Pin rstN;            /**< \brief RSTN / RDY  => output.  if only one signal is used for reset and  ready, rstN and rdy should have the same value */
        Ifx_ActiveState   rstNActiveState; /**< \brief RSTN active state FIXME have active state configurable for all signals ? */
        Ifx1edi2010as_Pin rdy;             /**< \brief RSTN / RDY  => input.  if only one signal is used for reset and  ready, rstN and rdy should have the same value */
        Ifx1edi2010as_Pin en;              /**< \brief EN */
        Ifx1edi2010as_Pin PBen;			   /* Jimmy Created for Power Board Enable pin */
    } pins;
    struct
    {
        Ifx1edi2010as_Driver *first;  /**< First daisy chain device. */
        Ifx1edi2010as_Driver *next;   /**< Next device in the chain */
        uint8                 length; /**< Number of device in the daisy chain */
        uint8                 index;  /**< \brief index of the device in the daisy chain. First device has index 0 */
    } daisyChain;
    Ifx1edi2010as_Time        resetDuration; /**< \brief Duration of the reset active signal */
};

typedef struct
{
    Ifx1edi2010as_Hal_Config hal;            /**< \brief HAL configuration */
    Ifx1edi2010as_Driver    *previousDevice; /**< Previous devices in the daisy chain. The first device in the daisy chain must be NULL. */
    Ifx1edi2010as_Time        resetDuration; /**< \brief Duration of the reset active signal */
    boolean 		   showSpiFrame;						/**< \brief Debug feature available id IFX_CFG_1EDI2010AS_DEBUG_SPI is set */
    boolean cacheEnabled;									/**< \brief Enable / disable the cache feature */
}Ifx1edi2010as_Driver_Config;

#if IFX_CFG_1EDI2010AS_DEBUG_SPI
IFX_EXTERN boolean Ifx1edi2010as_g_showSpiFrame;
#endif

/** \addtogroup IfxLld_1edi2010as_Driver
 * \{ */

/** \name Main API
 * \{ */

/** Initialize the driver
 *
 * This API initialise the 1EDI2010AS device driver.
 *
 * \param driver Pointer to the driver object, will be initialized by the function
 * \param config Driver configuration
 * \return Returns TRUE in case of success, else FALSE
 */
IFX_EXTERN boolean Ifx1edi2010as_Driver_init(Ifx1edi2010as_Driver *driver, Ifx1edi2010as_Driver_Config *config);

/** Initialize the configuration to default
 *
 * This function must be called prior to \ref Ifx1edi2010as_Driver_init()
 * \param config Driver configuration. Will be initialized by the function
 */
IFX_EXTERN void    Ifx1edi2010as_Driver_initConfig(Ifx1edi2010as_Driver_Config *config);

/** read a register value either from cache or the device if the cache is not valid
 *
 * If the cache is enabled, the full cache line is read. If the cache is disabled only the requested device register is read.
 *
 * \param driver Pointer to the driver object
 * \param address Register address. Unit is the device address unit.
 * \param data Returned register value
 *
 * \return Returns TRUE in case of success else false
 */
IFX_EXTERN boolean Ifx1edi2010as_Driver_readRegister(Ifx1edi2010as_Driver *driver, Ifx1edi2010as_Address address, uint16 *data);

/** Write to a register
 *
 * \param driver Pointer to the driver object
 * \param address Register address. Unit is the device address unit.
 * \param data Register value to be written
 *
 * \return TRUE in case of successful else FALSE
 */
IFX_EXTERN boolean Ifx1edi2010as_Driver_writeRegister(Ifx1edi2010as_Driver *driver, Ifx1edi2010as_Address address, uint16 data);

/** Write to a register with action write bitfield ('t' flag)
 *
 * \param driver Pointer to the driver object
 * \param address Register address. Unit is the device address unit.
 * \param data Register value to be written
 * \param noOpMask Mask for the bifield set by this function
 *
 * \return TRUE in case of successful else FALSE
 */
IFX_EXTERN boolean Ifx1edi2010as_Driver_writeRegisterStar(Ifx1edi2010as_Driver *driver, Ifx1edi2010as_Address address, Ifx1edi2010as_UData data, Ifx1edi2010as_UData noOpMask);


/** Return TRUE is a write syncronisation error occured
 *
 * \return TRUE is a write syncronisation error occured
 */
IFX_INLINE boolean Ifx1edi2010as_Driver_isWriteSyncError(Ifx1edi2010as_Driver *driver)
{
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
	return Ifx_DeviceCache_isWriteBackError(&driver->cache.engine);
#else
	return FALSE;
#endif
}

/** Disable the PWM signal (Set EN pin to inactive)
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 * \param mask Specifies which device must be disabled. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 */
IFX_EXTERN void Ifx1edi2010as_Driver_disable(Ifx1edi2010as_Driver *driver, uint32 mask);

/** Enable the PWM signal (Set EN pin to active)
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 * \param mask Specifies which device must be disabled. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 */
IFX_EXTERN void Ifx1edi2010as_Driver_enable(Ifx1edi2010as_Driver *driver, uint32 mask);

/** Return the enable status (EN pin state)
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 * \param mask Specifies which device must be disabled. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 *
 * \return TRUE if EN is active, else FALSE
 */
IFX_EXTERN boolean Ifx1edi2010as_Driver_isEnabled(Ifx1edi2010as_Driver *driver, uint32 mask);

/** Execute a reset of the driver (Set the RSTN/RDY pin to active for the reset time)
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 * \param mask Specifies which device must be disabled. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 */
IFX_EXTERN void Ifx1edi2010as_Driver_reset(Ifx1edi2010as_Driver *driver, uint32 mask);

/** Switch the driver off (Set the RSTN/RDY pin to active)
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 * \param mask Specifies which device must be disabled. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 */
IFX_EXTERN void Ifx1edi2010as_Driver_switchOff(Ifx1edi2010as_Driver *driver, uint32 mask);

/** Switch the driver on  (Set the RSTN/RDY pin to not active)
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 * \param mask Specifies which device must be disabled. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 */
IFX_EXTERN void Ifx1edi2010as_Driver_switchOn(Ifx1edi2010as_Driver *driver, uint32 mask);

/** Indicates if the driver is not ready (Status of RSTN/RDY pin )
 *
 * In case of daisy chain, the return value has the following meaning
 * - bit 0: 1st device in the chain. 1 means not ready, 0 means ready or no pin ready assigned
 * - bit 1: 2nd device in the chain
 * - ...
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 */

IFX_EXTERN uint32 Ifx1edi2010as_Driver_isAnyDeviceNotReady(Ifx1edi2010as_Driver *driver);

/** Return the fault status (Status of NFLTA /NFLTB pin)
 *
 * The returned value has the following meaning:
 * [15-0] : FLTA. Bit 0 being the 1st device in the chain. A bit set indicates a fault.
 * [31-16]: FLTB. Bit 16 being the 1st device in the chain. A bit set indicates a fault.
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 */
IFX_EXTERN uint32 Ifx1edi2010as_Driver_isAnyDeviceFault(Ifx1edi2010as_Driver *driver);

/** Enter the configuration mode
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 * \param mask Specifies which device must be switched on. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 *
 * \return Returns TRUE in case of success else FALSE
 *
 * \note In case the cache is enabled, a \ref Ifx1edi2010as_Driver_writeSync "cache write back" is executed prior to the mode change request
 */
IFX_EXTERN boolean Ifx1edi2010as_Driver_enterConfigurationMode(Ifx1edi2010as_Driver *driver, uint32 mask);

/** Exit the configuration mode
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 * \param mask Specifies which device must be switched on. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 *
 * \return Returns TRUE in case of success else FALSE
 *
 * \note In case the cache is enabled, a \ref Ifx1edi2010as_Driver_writeSync "cache write back" is executed prior to the mode change request
 */
IFX_EXTERN boolean Ifx1edi2010as_Driver_exitConfigurationMode(Ifx1edi2010as_Driver *driver, uint32 mask);

/** Enter the verification mode
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 * \param mask Specifies which device must be switched on. bit 0 is 1st device in the daisy chain, bit 1 is the 2nd device in the chain, ...
 *
 * \return Returns TRUE in case of success else FALSE
 *
 * \note In case the cache is enabled, a \ref Ifx1edi2010as_Driver_writeSync "cache write back" is executed prior to the mode change request
 */
IFX_EXTERN boolean Ifx1edi2010as_Driver_enterVerificationMode(Ifx1edi2010as_Driver *driver, uint32 mask);

/** Return the gain value from gain code
 *
 * \param code ADC gain code, as defined by the register
 *
 * \return Returns the gain value as float
 */
IFX_EXTERN float32 Ifx1edi2010as_Driver_getScaledGain(Ifx1edi2010as_AdcGain code);

/** Return the offset value from offset code
 *
 * \param code ADC offset code, as defined by the register
 *
 * \return Returns the offset value as float
 */
IFX_EXTERN float32 Ifx1edi2010as_Driver_getScaledOffset(Ifx1edi2010as_AdcOffset code);
/** \} */

/** \name Cache API
 * \{ */

/** Disable the cache
 * If the device are in daisy chain, the cache for all device in the chain are disabled.
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 *
 * \return Returns the previous cache enable status
 */
IFX_EXTERN boolean Ifx1edi2010as_Driver_disableCache(Ifx1edi2010as_Driver *driver);

/** Enable the cache
 * Enabling the cache will automatically invalidate the cache
 * If the device are in daisy chain, cache for all device in the chain is disabled
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 */
IFX_EXTERN void Ifx1edi2010as_Driver_enableCache(Ifx1edi2010as_Driver *driver);

/** Return the cache enable status
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 */
IFX_EXTERN boolean Ifx1edi2010as_Driver_isCacheEnabled(Ifx1edi2010as_Driver *driver);

/** Restore the device cache
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 * \param enabled If true, the cache is enabled, else disabled
 *
 */
IFX_EXTERN boolean Ifx1edi2010as_Driver_restoreCache(Ifx1edi2010as_Driver *driver, boolean enabled);

/** Invalidate the cache (all cached registers)
 *
 * If the device are in daisy chain, cache for all device in the chain is invalidated
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 */
IFX_INLINE void Ifx1edi2010as_Driver_invalidateCache(Ifx1edi2010as_Driver *driver);

/** Invalidate the cache (Cache for volatile registers only)
 *
 * If the device are in daisy chain, cache for all device in the chain is invalidated
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 */
IFX_INLINE void Ifx1edi2010as_Driver_invalidateCacheVolatile(Ifx1edi2010as_Driver *driver);


/** Synchonize the cache with the device (read)
 * If the devices are in daisy chain, all devices are synchronized.
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 *
 * \return Returns TRUE in case of success else FALSE
 */
boolean Ifx1edi2010as_Driver_readSync(Ifx1edi2010as_Driver *driver);

/** Synchonize the cache with the device (write)
 * If the devices are in daisy chain, all devices are written
 *
 * \param driver Pointer to the driver object. if the device is in daisy chain, any device in the daisy chain can be used
 *
 * \return TRUE in case of successful else FALSE
 */
IFX_EXTERN boolean Ifx1edi2010as_Driver_writeSync(Ifx1edi2010as_Driver *driver);

/** \} */

/** \name Helper API
 * \{ */
/** Dump the Device registers using \ref Ifx1edi2010as_Hal_print()
 *
 * \param driver Pointer to the driver object
 */
IFX_EXTERN void Ifx1edi2010as_Driver_dumpRegisters(Ifx1edi2010as_Driver *driver);
/** \} */

/** \} */

IFX_INLINE void Ifx1edi2010as_Driver_invalidateCache(Ifx1edi2010as_Driver *driver)
{
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_invalidateCache()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif
    Ifx_DeviceCache_invalidate(&driver->cache.engine);
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
#else
    (void)driver;
#endif
}


IFX_INLINE void Ifx1edi2010as_Driver_invalidateCacheVolatile(Ifx1edi2010as_Driver *driver)
{
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
#if IFX_CFG_1EDI2010AS_DEBUG && 0
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_invalidateCacheVolatile()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif
    Ifx_DeviceCache_invalidateVolatile(&driver->cache.engine);
#if IFX_CFG_1EDI2010AS_DEBUG && 0
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
#else
    (void)driver;
#endif
}

/** Show the SPI frames (Debug feature)
 *
 * Only available if IFX_CFG_1EDI2010AS_DEBUG_SPI is set
 */
IFX_INLINE void Ifx1edi2010as_Driver_showSpiFrame(Ifx1edi2010as_Driver *driver)
{
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
	Ifx1edi2010as_g_showSpiFrame = TRUE;
#endif
}

/** Show the SPI frames (Debug feature)
 *
 * Only available if IFX_CFG_1EDI2010AS_DEBUG_SPI is set
 */
IFX_INLINE void Ifx1edi2010as_Driver_hideSpiFrame(Ifx1edi2010as_Driver *driver)
{
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
	Ifx1edi2010as_g_showSpiFrame = FALSE;
#endif
}

#endif /* IFX1EDI2010AS_DRIVER_H_ */
