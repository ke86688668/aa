/**
 * \file Ifx1edi2010as_Driver.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 */

#include "SysSe/Ext/1edi2010as/Driver/Ifx1edi2010as_Driver.h"
#include "SysSe/Ext/1edi2010as/If/Ifx1edi2010as_If.h"
#include "SysSe/Ext/1edi2010as/Std/Ifx1edi2010as.h"
#include "SysSe/Ext/1edi2010as/Hal/Ifx1edi2010as_Hal.h"
#include "SysSe/Bsp/Bsp.h"
#include "string.h"

/* Jimmy Create the Global values for GD_EN status */
boolean Dio_GD_EN_Status = FALSE;


#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
#pragma message  "WARNING: 1EDI2010AS Cache feature is experimental"
#endif

#if IFX_CFG_1EDI2010AS_DEBUG_SPI
boolean Ifx1edi2010as_g_showSpiFrame = FALSE;
#endif
boolean Ifx1edi2010as_Driver_disableCache(Ifx1edi2010as_Driver *driver)
{
    boolean result;
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
    /* Ensure all cached data are written to the device before disabling the cache */
    result = Ifx_DeviceCache_writeBack16(&driver->cache.engine);
    Ifx_DeviceCache_disable(&driver->cache.engine);
#else
    (void)driver;
    result = TRUE;
#endif
    return result;
}


void Ifx1edi2010as_Driver_enableCache(Ifx1edi2010as_Driver *driver)
{
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
    Ifx_DeviceCache_enable(&driver->cache.engine);
#else
    (void)driver;
#endif
}

boolean Ifx1edi2010as_Driver_isCacheEnabled(Ifx1edi2010as_Driver *driver)
{
    boolean enabled;
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
    enabled = Ifx_DeviceCache_isCacheEnabled(&driver->cache.engine);
#else
    (void)driver;
    enabled = FALSE;
#endif
    return enabled;
}


boolean Ifx1edi2010as_Driver_restoreCache(Ifx1edi2010as_Driver *driver, boolean enabled)
{
    boolean result;
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE

    if (enabled)
    {
        Ifx_DeviceCache_enable(&driver->cache.engine);
        result = TRUE;
    }
    else
    {
        result = Ifx_DeviceCache_disable(&driver->cache.engine);
    }

#else
    (void)driver;
    (void)enabled;
    result = TRUE;
#endif
	return result;
}




boolean Ifx1edi2010as_Driver_init(Ifx1edi2010as_Driver *driver, Ifx1edi2010as_Driver_Config *config)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_init()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif

    IFX_ASSERT(IFX_VERBOSE_LEVEL_FAILURE, (((sizeof(Ifx_1EDI2010AS) >> IFX1EDI2010AS_ADDRESS_SHIFT)-1)/32+1) == (sizeof(Ifx1edi2010as_g_validAddress) / 4));

    Ifx1edi2010as_Hal_init(&config->hal);

    driver->channel              = config->hal.channel;
    driver->pins.en              = config->hal.en;
    driver->pins.fltAN           = config->hal.fltAN;
    driver->pins.fltBN           = config->hal.fltBN;
    driver->pins.rstN            = config->hal.rstN;
    driver->pins.rstNActiveState = config->hal.rstNActiveState;
    driver->pins.rdy             = config->hal.rdy;
    driver->daisyChain.next      = NULL_PTR;
    driver->resetDuration        = config->resetDuration;

    if (config->previousDevice)
    {
        driver->daisyChain.first                = config->previousDevice->daisyChain.first;
        config->previousDevice->daisyChain.next = driver;
        uint8                 length, index;
        Ifx1edi2010as_Driver *device;
        device = driver->daisyChain.first;
        length = 0;
        index  = 0;

        do
        {
            length++;
            device = device->daisyChain.next;
        } while (device);

        device = driver->daisyChain.first;

        do
        {
            device->daisyChain.length = length;
            device->daisyChain.index  = index++;
            device                    = device->daisyChain.next;
        } while (device);
    }
    else
    {
        driver->daisyChain.first  = driver;
        driver->daisyChain.length = 1;
    }
#if IFX_CFG_1EDI2010AS_DEBUG_SPI
    Ifx1edi2010as_g_showSpiFrame = config->showSpiFrame;
#endif

#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
    Ifx_DeviceCache_Config cacheConfig;
    Ifx_DeviceCache_initConfig(&cacheConfig);

    cacheConfig.ItemCount            = sizeof(Ifx_1EDI2010AS) / IFX1EDI2010AS_ADDRESS_UNIT;
    cacheConfig.buffer               = (void*)&driver->cache.cache;
    cacheConfig.bufferNoOpMaskActual = (void*)&driver->cache.bufferNoOpMaskActual;
    cacheConfig.bufferNoOpMaskDefault= (void*)&Ifx1edi2010as_g_noOpMask;
    cacheConfig.bufferNoOpValue      = (void*)&Ifx1edi2010as_g_noOpValue;
    cacheConfig.modificationMask     = &driver->cache.modificationMask[0];
    cacheConfig.validAddress         = (uint32 *)&Ifx1edi2010as_g_validAddress[0];
    cacheConfig.volatileRegisters    = (uint32 *)&Ifx1edi2010as_g_volatileRegisters[0];
    cacheConfig.actionReadRegisters  = (uint32 *)&Ifx1edi2010as_g_actionReadRegisters[0];
    cacheConfig.actionWriteRegisters = (uint32 *)&Ifx1edi2010as_g_actionWriteRegisters[0];
    cacheConfig.valid                = &driver->cache.cacheValid[0];
    cacheConfig.writeBackErrorFlags  = &driver->cache.writeBackErrorFlags[0];
    cacheConfig.flagArraySize        = sizeof(Ifx1edi2010as_g_validAddress) / 4;
    cacheConfig.read                 = (Ifx_DeviceCache_ReadFromDevice)Ifx1edi2010as_If_readRegister;
    cacheConfig.write                = (Ifx_DeviceCache_WriteToDevice)Ifx1edi2010as_If_writeRegister;
    cacheConfig.writeBack			 = &Ifx_DeviceCache_writeBack16;
    cacheConfig.deviceDriver         = driver;
    cacheConfig.registerWidth        = IFX1EDI2010AS_ADDRESS_UNIT;

    if (config->previousDevice)
    {
        cacheConfig.previousCache = &config->previousDevice->cache.engine;
    }
    else
    {
        cacheConfig.previousCache = NULL_PTR;
    }

    cacheConfig.tempCacheLine = driver->cache.tempCacheLine;

    result                   &= Ifx_DeviceCache_init(&driver->cache.engine, &cacheConfig);
    Ifx_DeviceCache_setVolatileCache(&driver->cache.engine, TRUE);

    if (!config->cacheEnabled)
    {
    	Ifx1edi2010as_Driver_disableCache(driver);
    }

#endif
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif

    return result;
}


void Ifx1edi2010as_Driver_initConfig(Ifx1edi2010as_Driver_Config *config)
{
    Ifx1edi2010as_Hal_initConfig(&config->hal);
    config->previousDevice = NULL_PTR;
    config->resetDuration = IFX1EDI2010AS_TIME_1S;
}


boolean Ifx1edi2010as_Driver_readRegister(Ifx1edi2010as_Driver *driver, Ifx1edi2010as_Address address, uint16 *data)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
    result = Ifx_DeviceCache_getValue16(&driver->cache.engine, address, data);
#else
    uint16 temp[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];

    result = Ifx1edi2010as_If_readRegister(driver, address, temp);
    *data  = temp[driver->daisyChain.index];
#endif

    return result;
}


boolean Ifx1edi2010as_Driver_readSync(Ifx1edi2010as_Driver *driver)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_readSync()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif

    result = Ifx_DeviceCache_synchronize16(&driver->cache.engine);
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
#else
    (void)driver;
#endif

    return result;
}


boolean Ifx1edi2010as_Driver_writeRegister(Ifx1edi2010as_Driver *driver, Ifx1edi2010as_Address address, uint16 data)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
    result = Ifx_DeviceCache_setValue16(&driver->cache.engine, address, data);
#else
    uint16 temp[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];
    temp[driver->daisyChain.index] = data;
    result                         = Ifx1edi2010as_If_writeRegister(driver, 1 << driver->daisyChain.index, address, temp);
#endif

    return result;
}

boolean Ifx1edi2010as_Driver_writeRegisterStar(Ifx1edi2010as_Driver *driver, Ifx1edi2010as_Address address, Ifx1edi2010as_UData data, Ifx1edi2010as_UData noOpMask)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
    result = Ifx_DeviceCache_setValue16(&driver->cache.engine, address, data);
#else
	uint16 temp[IFX1EDI2010AS_MAX_DAISYCHAIN_LENGTH];
	/* Set the required bitfields to no action */
	Ifx1edi2010as_UData mask;
	mask = ((Ifx1edi2010as_UData*)&Ifx1edi2010as_g_noOpMask)[address] & ~noOpMask;
	data = (data & ~mask) | ((((Ifx1edi2010as_UData*)&Ifx1edi2010as_g_noOpValue)[address]) & mask);
	temp[driver->daisyChain.index] = data;
    result                         = Ifx1edi2010as_If_writeRegister(driver, 1 << driver->daisyChain.index, address, temp);
#endif

    return result;
}


boolean Ifx1edi2010as_Driver_writeSync(Ifx1edi2010as_Driver *driver)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_writeSync()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif
    result = Ifx_DeviceCache_writeBack16(&driver->cache.engine);
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
#else
    (void)driver;
#endif

    return result;
}


void Ifx1edi2010as_Driver_disable(Ifx1edi2010as_Driver *driver, uint32 mask)
{
    uint32 i = 1;
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_disable()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif
    driver = driver->daisyChain.first;

    do
    {
        if ((i & mask) && (driver->pins.en.port != NULL_PTR))
        {
            Ifx1edi2010as_Hal_setPortLow(&driver->pins.en);
            Dio_GD_EN_Status = FALSE;	/* Jimmy Create for show the GD_EN_Status setting */
        }

        i    <<= 1;
        driver = driver->daisyChain.next;
    } while (driver);
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
}


void Ifx1edi2010as_Driver_enable(Ifx1edi2010as_Driver *driver, uint32 mask)
{
    uint32 i = 1;
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_enable()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif
    driver = driver->daisyChain.first;

    do
    {
        if ((i & mask) && (driver->pins.en.port != NULL_PTR))
        {
            Ifx1edi2010as_Hal_setPortHigh(&driver->pins.en);
            Dio_GD_EN_Status = TRUE;	/* Jimmy Create for show the GD_EN_Status setting */
        }

        i    <<= 1;
        driver = driver->daisyChain.next;
    } while (driver);
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
}

boolean Ifx1edi2010as_Driver_isEnabled(Ifx1edi2010as_Driver *driver, uint32 mask)
{
    uint32 i = 1;
#if IFX_CFG_1EDI2010AS_DEBUG && 0
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_isEnabled()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif
    driver = driver->daisyChain.first;

    do
    {
        if ((i & mask) && (driver->pins.en.port != NULL_PTR))
        {
            return Ifx1edi2010as_Hal_getPortState(&driver->pins.en);
        }

        i    <<= 1;
        driver = driver->daisyChain.next;
    } while (driver);
#if IFX_CFG_1EDI2010AS_DEBUG && 0
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
    return FALSE;
}



void Ifx1edi2010as_Driver_reset(Ifx1edi2010as_Driver *driver, uint32 mask)
{
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_reset()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif
    Ifx1edi2010as_Driver_switchOff(driver, mask);
    Ifx1edi2010as_Hal_wait(driver->resetDuration);/* FIXME make it configurable */
    Ifx1edi2010as_Driver_switchOn(driver, mask);
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
}


void Ifx1edi2010as_Driver_switchOff(Ifx1edi2010as_Driver *driver, uint32 mask)
{
    uint32 i = 1;
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_switchOff()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif
    driver = driver->daisyChain.first;

    do
    {
        if ((i & mask) && (driver->pins.rstN.port != NULL_PTR))
        {
            Ifx1edi2010as_Hal_setPortState(&driver->pins.rstN, driver->pins.rstNActiveState == Ifx_ActiveState_low ? Ifx1edi2010as_PinState_low : Ifx1edi2010as_PinState_high);
        }

        i    <<= 1;
        driver = driver->daisyChain.next;
    } while (driver);
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
}


void Ifx1edi2010as_Driver_switchOn(Ifx1edi2010as_Driver *driver, uint32 mask)
{
    uint32 i = 1;
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_switchOn()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif
    driver = driver->daisyChain.first;

    do
    {
        if ((i & mask) && (driver->pins.rstN.port != NULL_PTR))
        {
            Ifx1edi2010as_Hal_setPortState(&driver->pins.rstN, driver->pins.rstNActiveState == Ifx_ActiveState_low ? Ifx1edi2010as_PinState_high : Ifx1edi2010as_PinState_low);
        }

        i    <<= 1;
        driver = driver->daisyChain.next;
    } while (driver);
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
}


uint32 Ifx1edi2010as_Driver_isAnyDeviceNotReady(Ifx1edi2010as_Driver *driver)
{
    uint32 result = 0;
    uint32 i      = 1;
    driver = driver->daisyChain.first;

    do
    {
        if (driver->pins.rdy.port != NULL_PTR)
        {
            result |= Ifx1edi2010as_Hal_getPortState(&driver->pins.rdy) == FALSE ? i : 0;
        }

        i    <<= 1;
        driver = driver->daisyChain.next;
    } while (driver);

    return result;
}


uint32 Ifx1edi2010as_Driver_isAnyDeviceFault(Ifx1edi2010as_Driver *driver)
{
    uint32 result = 0;
    uint32 i      = 1;
    driver = driver->daisyChain.first;

    do
    {
        if (driver->pins.fltAN.port != NULL_PTR)
        {
            result |= Ifx1edi2010as_Hal_getPortState(&driver->pins.fltAN) == FALSE ? i : 0;
        }

        if (driver->pins.fltBN.port != NULL_PTR)
        {
            result |= Ifx1edi2010as_Hal_getPortState(&driver->pins.fltBN) == FALSE ? i << 16 : 0;
        }

        i    <<= 1;
        /* Jimmy note "driver->daisyChain.next = NULL_PTR;" in "Ifx1edi2010as_Driver_init" loop */
        driver = driver->daisyChain.next;
    } while (driver);
#if 1//DEBUG_3
    /* Jimmy Create for detection Gate Driver Enable pin status, using "bit 7" */
    if (Dio_GD_EN_Status == TRUE)
    {
    	if(Ifx1edi2010as_Hal_getPortState(&driver->pins.en) == FALSE)
    		result |= 0x80;
    	else
    		result &= 0x7F;
    }
    /* end created */
#endif
    return result;
}


boolean Ifx1edi2010as_Driver_enterConfigurationMode(Ifx1edi2010as_Driver *driver, uint32 mask)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_enterConfigurationMode()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
	// Ensure register values are transfered before mode change
	result &= Ifx_DeviceCache_writeBack16(&driver->cache.engine);
#endif
    result &= Ifx1edi2010as_If_modeTransition(driver, mask, Ifx1edi2010as_modeTransition_enterCmode);
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
	Ifx1edi2010as_Driver_invalidateCacheVolatile(driver);
#endif
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
    return result;
}


boolean Ifx1edi2010as_Driver_exitConfigurationMode(Ifx1edi2010as_Driver *driver, uint32 mask)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_exitConfigurationMode()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
	// Ensure register values are transfered before mode change
	result &= Ifx_DeviceCache_writeBack16(&driver->cache.engine);
#endif
    result &= Ifx1edi2010as_If_modeTransition(driver, mask, Ifx1edi2010as_modeTransition_exitCmode);
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
	Ifx1edi2010as_Driver_invalidateCacheVolatile(driver);
#endif
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
    return result;
}


boolean Ifx1edi2010as_Driver_enterVerificationMode(Ifx1edi2010as_Driver *driver, uint32 mask)
{
    boolean result = TRUE;
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("Ifx1edi2010as_Driver_enterVerificationMode()"_1EDI2010AS_ENDL);
    Ifx1edi2010as_Hal_print("{"_1EDI2010AS_ENDL);
#endif
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
	// Ensure register values are transfered before mode change
	result &= Ifx_DeviceCache_writeBack16(&driver->cache.engine);
#endif
    result &= Ifx1edi2010as_If_modeTransition(driver, mask, Ifx1edi2010as_modeTransition_enterVmode);
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
	Ifx1edi2010as_Driver_invalidateCacheVolatile(driver);
#endif
#if IFX_CFG_1EDI2010AS_DEBUG
    Ifx1edi2010as_Hal_print("}"_1EDI2010AS_ENDL);
#endif
    return result;
}


float32 Ifx1edi2010as_Driver_getScaledGain(Ifx1edi2010as_AdcGain code)
{
    float32 value;

    switch (code)
    {
    case Ifx1edi2010as_AdcGain_gain0:
    case Ifx1edi2010as_AdcGain_gain1:
        value = 2.0 / (3.0 * 2.75);
        break;
    case Ifx1edi2010as_AdcGain_gain2:
        value = 1.0 / (1.0 * 2.75);
        break;
    case Ifx1edi2010as_AdcGain_gain3:
		value = 2.0/(1.0*2.75);
        break;
    default:
        value = 0.0;
        break;
    }

    return value;
}


float32 Ifx1edi2010as_Driver_getScaledOffset(Ifx1edi2010as_AdcOffset code)
{
    return (float32)code / 2.0;
}


void Ifx1edi2010as_Driver_dumpRegisters(Ifx1edi2010as_Driver *driver)
{
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
    Ifx1edi2010as_Address address;

	Ifx1edi2010as_Driver_invalidateCache(driver);

    {
        Ifx1edi2010as_Driver *d;
        d = driver->daisyChain.first;

        // Loop over all devices
        do
        {
            Ifx1edi2010as_Hal_print(_1EDI2010AS_ENDL "1EDI2010 (%d) register dump:"_1EDI2010AS_ENDL, d->daisyChain.index);

            for (address = 0; address < d->cache.engine.ItemCount; address++)
            {
                if (Ifx_DeviceCache_isAddressValid(&d->cache.engine, address))
                {
                    char                text[255];
                    Ifx1edi2010as_UData data;

                    if (Ifx1edi2010as_Driver_readRegister(d, address, &data))
                    {
                        Ifx1edi2010as_registerDump(address, data, text, 255);
                        Ifx1edi2010as_Hal_print(text, address);
                        Ifx1edi2010as_Hal_print(_1EDI2010AS_ENDL);
                    }
                    else
                    {
                        Ifx1edi2010as_Hal_print("Read error at 0x%2H"_1EDI2010AS_ENDL, address);
                    }
                }
            }

            d = d->daisyChain.next;
        } while (d);
    }

#else
    (void)driver;
#pragma message  "WARNING: 1EDI2010AS register dump not available when cache is disabled"
    Ifx1edi2010as_Hal_print("INFO: 1EDI2010AS register dump not available when cache is disabled"_1EDI2010AS_ENDL);
#endif
}
