/**
 * \file Ifx1edi2010as_bf.h
 * \brief
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved.
 *
 *
 * Date: 2017-06-30 15:39:22 GMT
 * Version: TBD
 * Specification: TBD
 * MAY BE CHANGED BY USER [yes/no]: Yes
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_1edi2010as_Registers_BitfieldsMask Bitfields mask and offset
 * \ingroup IfxLld_1edi2010as_Registers
 * 
 */
#ifndef IFX1EDI2010AS_BF_H
#define IFX1EDI2010AS_BF_H 1
/******************************************************************************/
/******************************************************************************/
/** \addtogroup IfxLld_1edi2010as_Registers_BitfieldsMask
 * \{  */
/** \brief Length for Ifx_1EDI2010AS_PID_Bits.P */
#define IFX_1EDI2010AS_PID_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PID_Bits.P */
#define IFX_1EDI2010AS_PID_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PID_Bits.P */
#define IFX_1EDI2010AS_PID_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_PID_Bits.LMI */
#define IFX_1EDI2010AS_PID_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PID_Bits.LMI */
#define IFX_1EDI2010AS_PID_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PID_Bits.LMI */
#define IFX_1EDI2010AS_PID_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_PID_Bits.PVERS */
#define IFX_1EDI2010AS_PID_PVERS_LEN (12u)

/** \brief Mask for Ifx_1EDI2010AS_PID_Bits.PVERS */
#define IFX_1EDI2010AS_PID_PVERS_MSK (0xfffu)

/** \brief Offset for Ifx_1EDI2010AS_PID_Bits.PVERS */
#define IFX_1EDI2010AS_PID_PVERS_OFF (4u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT_Bits.P */
#define IFX_1EDI2010AS_PSTAT_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT_Bits.P */
#define IFX_1EDI2010AS_PSTAT_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT_Bits.P */
#define IFX_1EDI2010AS_PSTAT_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT_Bits.LMI */
#define IFX_1EDI2010AS_PSTAT_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT_Bits.LMI */
#define IFX_1EDI2010AS_PSTAT_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT_Bits.LMI */
#define IFX_1EDI2010AS_PSTAT_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT_Bits.GPOFP */
#define IFX_1EDI2010AS_PSTAT_GPOFP_LEN (3u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT_Bits.GPOFP */
#define IFX_1EDI2010AS_PSTAT_GPOFP_MSK (0x7u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT_Bits.GPOFP */
#define IFX_1EDI2010AS_PSTAT_GPOFP_OFF (2u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT_Bits.AVFP */
#define IFX_1EDI2010AS_PSTAT_AVFP_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT_Bits.AVFP */
#define IFX_1EDI2010AS_PSTAT_AVFP_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT_Bits.AVFP */
#define IFX_1EDI2010AS_PSTAT_AVFP_OFF (5u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT_Bits.SRDY */
#define IFX_1EDI2010AS_PSTAT_SRDY_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT_Bits.SRDY */
#define IFX_1EDI2010AS_PSTAT_SRDY_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT_Bits.SRDY */
#define IFX_1EDI2010AS_PSTAT_SRDY_OFF (6u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT_Bits.ACT */
#define IFX_1EDI2010AS_PSTAT_ACT_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT_Bits.ACT */
#define IFX_1EDI2010AS_PSTAT_ACT_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT_Bits.ACT */
#define IFX_1EDI2010AS_PSTAT_ACT_OFF (7u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT_Bits.GPONP */
#define IFX_1EDI2010AS_PSTAT_GPONP_LEN (3u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT_Bits.GPONP */
#define IFX_1EDI2010AS_PSTAT_GPONP_MSK (0x7u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT_Bits.GPONP */
#define IFX_1EDI2010AS_PSTAT_GPONP_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT_Bits.ERR */
#define IFX_1EDI2010AS_PSTAT_ERR_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT_Bits.ERR */
#define IFX_1EDI2010AS_PSTAT_ERR_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT_Bits.ERR */
#define IFX_1EDI2010AS_PSTAT_ERR_OFF (11u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT2_Bits.P */
#define IFX_1EDI2010AS_PSTAT2_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT2_Bits.P */
#define IFX_1EDI2010AS_PSTAT2_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT2_Bits.P */
#define IFX_1EDI2010AS_PSTAT2_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT2_Bits.LMI */
#define IFX_1EDI2010AS_PSTAT2_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT2_Bits.LMI */
#define IFX_1EDI2010AS_PSTAT2_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT2_Bits.LMI */
#define IFX_1EDI2010AS_PSTAT2_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT2_Bits.ENVAL */
#define IFX_1EDI2010AS_PSTAT2_ENVAL_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT2_Bits.ENVAL */
#define IFX_1EDI2010AS_PSTAT2_ENVAL_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT2_Bits.ENVAL */
#define IFX_1EDI2010AS_PSTAT2_ENVAL_OFF (2u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT2_Bits.FLTA */
#define IFX_1EDI2010AS_PSTAT2_FLTA_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT2_Bits.FLTA */
#define IFX_1EDI2010AS_PSTAT2_FLTA_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT2_Bits.FLTA */
#define IFX_1EDI2010AS_PSTAT2_FLTA_OFF (3u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT2_Bits.FLTB */
#define IFX_1EDI2010AS_PSTAT2_FLTB_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT2_Bits.FLTB */
#define IFX_1EDI2010AS_PSTAT2_FLTB_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT2_Bits.FLTB */
#define IFX_1EDI2010AS_PSTAT2_FLTB_OFF (4u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT2_Bits.OPMP */
#define IFX_1EDI2010AS_PSTAT2_OPMP_LEN (3u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT2_Bits.OPMP */
#define IFX_1EDI2010AS_PSTAT2_OPMP_MSK (0x7u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT2_Bits.OPMP */
#define IFX_1EDI2010AS_PSTAT2_OPMP_OFF (5u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT2_Bits.FLTAP */
#define IFX_1EDI2010AS_PSTAT2_FLTAP_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT2_Bits.FLTAP */
#define IFX_1EDI2010AS_PSTAT2_FLTAP_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT2_Bits.FLTAP */
#define IFX_1EDI2010AS_PSTAT2_FLTAP_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT2_Bits.FLTBP */
#define IFX_1EDI2010AS_PSTAT2_FLTBP_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT2_Bits.FLTBP */
#define IFX_1EDI2010AS_PSTAT2_FLTBP_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT2_Bits.FLTBP */
#define IFX_1EDI2010AS_PSTAT2_FLTBP_OFF (9u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT2_Bits.STP */
#define IFX_1EDI2010AS_PSTAT2_STP_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT2_Bits.STP */
#define IFX_1EDI2010AS_PSTAT2_STP_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT2_Bits.STP */
#define IFX_1EDI2010AS_PSTAT2_STP_OFF (10u)

/** \brief Length for Ifx_1EDI2010AS_PSTAT2_Bits.AXVP */
#define IFX_1EDI2010AS_PSTAT2_AXVP_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSTAT2_Bits.AXVP */
#define IFX_1EDI2010AS_PSTAT2_AXVP_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSTAT2_Bits.AXVP */
#define IFX_1EDI2010AS_PSTAT2_AXVP_OFF (11u)

/** \brief Length for Ifx_1EDI2010AS_PER_Bits.P */
#define IFX_1EDI2010AS_PER_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PER_Bits.P */
#define IFX_1EDI2010AS_PER_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PER_Bits.P */
#define IFX_1EDI2010AS_PER_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_PER_Bits.LMI */
#define IFX_1EDI2010AS_PER_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PER_Bits.LMI */
#define IFX_1EDI2010AS_PER_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PER_Bits.LMI */
#define IFX_1EDI2010AS_PER_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_PER_Bits.CERP */
#define IFX_1EDI2010AS_PER_CERP_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PER_Bits.CERP */
#define IFX_1EDI2010AS_PER_CERP_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PER_Bits.CERP */
#define IFX_1EDI2010AS_PER_CERP_OFF (2u)

/** \brief Length for Ifx_1EDI2010AS_PER_Bits.ADER */
#define IFX_1EDI2010AS_PER_ADER_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PER_Bits.ADER */
#define IFX_1EDI2010AS_PER_ADER_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PER_Bits.ADER */
#define IFX_1EDI2010AS_PER_ADER_OFF (6u)

/** \brief Length for Ifx_1EDI2010AS_PER_Bits.SPIER */
#define IFX_1EDI2010AS_PER_SPIER_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PER_Bits.SPIER */
#define IFX_1EDI2010AS_PER_SPIER_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PER_Bits.SPIER */
#define IFX_1EDI2010AS_PER_SPIER_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_PER_Bits.STPER */
#define IFX_1EDI2010AS_PER_STPER_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PER_Bits.STPER */
#define IFX_1EDI2010AS_PER_STPER_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PER_Bits.STPER */
#define IFX_1EDI2010AS_PER_STPER_OFF (9u)

/** \brief Length for Ifx_1EDI2010AS_PER_Bits.ENER */
#define IFX_1EDI2010AS_PER_ENER_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PER_Bits.ENER */
#define IFX_1EDI2010AS_PER_ENER_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PER_Bits.ENER */
#define IFX_1EDI2010AS_PER_ENER_OFF (10u)

/** \brief Length for Ifx_1EDI2010AS_PER_Bits.RSTP */
#define IFX_1EDI2010AS_PER_RSTP_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PER_Bits.RSTP */
#define IFX_1EDI2010AS_PER_RSTP_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PER_Bits.RSTP */
#define IFX_1EDI2010AS_PER_RSTP_OFF (11u)

/** \brief Length for Ifx_1EDI2010AS_PER_Bits.RSTEP */
#define IFX_1EDI2010AS_PER_RSTEP_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PER_Bits.RSTEP */
#define IFX_1EDI2010AS_PER_RSTEP_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PER_Bits.RSTEP */
#define IFX_1EDI2010AS_PER_RSTEP_OFF (12u)

/** \brief Length for Ifx_1EDI2010AS_PCFG_Bits.P */
#define IFX_1EDI2010AS_PCFG_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCFG_Bits.P */
#define IFX_1EDI2010AS_PCFG_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCFG_Bits.P */
#define IFX_1EDI2010AS_PCFG_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_PCFG_Bits.LMI */
#define IFX_1EDI2010AS_PCFG_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCFG_Bits.LMI */
#define IFX_1EDI2010AS_PCFG_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCFG_Bits.LMI */
#define IFX_1EDI2010AS_PCFG_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_PCFG_Bits.PAREN */
#define IFX_1EDI2010AS_PCFG_PAREN_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCFG_Bits.PAREN */
#define IFX_1EDI2010AS_PCFG_PAREN_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCFG_Bits.PAREN */
#define IFX_1EDI2010AS_PCFG_PAREN_OFF (2u)

/** \brief Length for Ifx_1EDI2010AS_PCFG_Bits.ADAEN */
#define IFX_1EDI2010AS_PCFG_ADAEN_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCFG_Bits.ADAEN */
#define IFX_1EDI2010AS_PCFG_ADAEN_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCFG_Bits.ADAEN */
#define IFX_1EDI2010AS_PCFG_ADAEN_OFF (5u)

/** \brief Length for Ifx_1EDI2010AS_PCFG_Bits.ADTEN */
#define IFX_1EDI2010AS_PCFG_ADTEN_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCFG_Bits.ADTEN */
#define IFX_1EDI2010AS_PCFG_ADTEN_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCFG_Bits.ADTEN */
#define IFX_1EDI2010AS_PCFG_ADTEN_OFF (6u)

/** \brief Length for Ifx_1EDI2010AS_PCFG2_Bits.P */
#define IFX_1EDI2010AS_PCFG2_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCFG2_Bits.P */
#define IFX_1EDI2010AS_PCFG2_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCFG2_Bits.P */
#define IFX_1EDI2010AS_PCFG2_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_PCFG2_Bits.LMI */
#define IFX_1EDI2010AS_PCFG2_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCFG2_Bits.LMI */
#define IFX_1EDI2010AS_PCFG2_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCFG2_Bits.LMI */
#define IFX_1EDI2010AS_PCFG2_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_PCFG2_Bits.STPDEL */
#define IFX_1EDI2010AS_PCFG2_STPDEL_LEN (6u)

/** \brief Mask for Ifx_1EDI2010AS_PCFG2_Bits.STPDEL */
#define IFX_1EDI2010AS_PCFG2_STPDEL_MSK (0x3fu)

/** \brief Offset for Ifx_1EDI2010AS_PCFG2_Bits.STPDEL */
#define IFX_1EDI2010AS_PCFG2_STPDEL_OFF (2u)

/** \brief Length for Ifx_1EDI2010AS_PCFG2_Bits.DIO1 */
#define IFX_1EDI2010AS_PCFG2_DIO1_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCFG2_Bits.DIO1 */
#define IFX_1EDI2010AS_PCFG2_DIO1_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCFG2_Bits.DIO1 */
#define IFX_1EDI2010AS_PCFG2_DIO1_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_PCTRL_Bits.P */
#define IFX_1EDI2010AS_PCTRL_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCTRL_Bits.P */
#define IFX_1EDI2010AS_PCTRL_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCTRL_Bits.P */
#define IFX_1EDI2010AS_PCTRL_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_PCTRL_Bits.LMI */
#define IFX_1EDI2010AS_PCTRL_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCTRL_Bits.LMI */
#define IFX_1EDI2010AS_PCTRL_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCTRL_Bits.LMI */
#define IFX_1EDI2010AS_PCTRL_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_PCTRL_Bits.GPON */
#define IFX_1EDI2010AS_PCTRL_GPON_LEN (3u)

/** \brief Mask for Ifx_1EDI2010AS_PCTRL_Bits.GPON */
#define IFX_1EDI2010AS_PCTRL_GPON_MSK (0x7u)

/** \brief Offset for Ifx_1EDI2010AS_PCTRL_Bits.GPON */
#define IFX_1EDI2010AS_PCTRL_GPON_OFF (2u)

/** \brief Length for Ifx_1EDI2010AS_PCTRL_Bits.CLRP */
#define IFX_1EDI2010AS_PCTRL_CLRP_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCTRL_Bits.CLRP */
#define IFX_1EDI2010AS_PCTRL_CLRP_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCTRL_Bits.CLRP */
#define IFX_1EDI2010AS_PCTRL_CLRP_OFF (5u)

/** \brief Length for Ifx_1EDI2010AS_PCTRL_Bits.CLRS */
#define IFX_1EDI2010AS_PCTRL_CLRS_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCTRL_Bits.CLRS */
#define IFX_1EDI2010AS_PCTRL_CLRS_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCTRL_Bits.CLRS */
#define IFX_1EDI2010AS_PCTRL_CLRS_OFF (6u)

/** \brief Length for Ifx_1EDI2010AS_PCTRL2_Bits.P */
#define IFX_1EDI2010AS_PCTRL2_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCTRL2_Bits.P */
#define IFX_1EDI2010AS_PCTRL2_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCTRL2_Bits.P */
#define IFX_1EDI2010AS_PCTRL2_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_PCTRL2_Bits.LMI */
#define IFX_1EDI2010AS_PCTRL2_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCTRL2_Bits.LMI */
#define IFX_1EDI2010AS_PCTRL2_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCTRL2_Bits.LMI */
#define IFX_1EDI2010AS_PCTRL2_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_PCTRL2_Bits.GPOF */
#define IFX_1EDI2010AS_PCTRL2_GPOF_LEN (3u)

/** \brief Mask for Ifx_1EDI2010AS_PCTRL2_Bits.GPOF */
#define IFX_1EDI2010AS_PCTRL2_GPOF_MSK (0x7u)

/** \brief Offset for Ifx_1EDI2010AS_PCTRL2_Bits.GPOF */
#define IFX_1EDI2010AS_PCTRL2_GPOF_OFF (2u)

/** \brief Length for Ifx_1EDI2010AS_PCTRL2_Bits.ACRP */
#define IFX_1EDI2010AS_PCTRL2_ACRP_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCTRL2_Bits.ACRP */
#define IFX_1EDI2010AS_PCTRL2_ACRP_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCTRL2_Bits.ACRP */
#define IFX_1EDI2010AS_PCTRL2_ACRP_OFF (5u)

/** \brief Length for Ifx_1EDI2010AS_PSCR_Bits.P */
#define IFX_1EDI2010AS_PSCR_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSCR_Bits.P */
#define IFX_1EDI2010AS_PSCR_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSCR_Bits.P */
#define IFX_1EDI2010AS_PSCR_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_PSCR_Bits.LMI */
#define IFX_1EDI2010AS_PSCR_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PSCR_Bits.LMI */
#define IFX_1EDI2010AS_PSCR_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PSCR_Bits.LMI */
#define IFX_1EDI2010AS_PSCR_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_PSCR_Bits.VFSP */
#define IFX_1EDI2010AS_PSCR_VFSP_LEN (2u)

/** \brief Mask for Ifx_1EDI2010AS_PSCR_Bits.VFSP */
#define IFX_1EDI2010AS_PSCR_VFSP_MSK (0x3u)

/** \brief Offset for Ifx_1EDI2010AS_PSCR_Bits.VFSP */
#define IFX_1EDI2010AS_PSCR_VFSP_OFF (2u)

/** \brief Length for Ifx_1EDI2010AS_PRW_Bits.P */
#define IFX_1EDI2010AS_PRW_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PRW_Bits.P */
#define IFX_1EDI2010AS_PRW_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PRW_Bits.P */
#define IFX_1EDI2010AS_PRW_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_PRW_Bits.LMI */
#define IFX_1EDI2010AS_PRW_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PRW_Bits.LMI */
#define IFX_1EDI2010AS_PRW_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PRW_Bits.LMI */
#define IFX_1EDI2010AS_PRW_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_PRW_Bits.RWVAL */
#define IFX_1EDI2010AS_PRW_RWVAL_LEN (14u)

/** \brief Mask for Ifx_1EDI2010AS_PRW_Bits.RWVAL */
#define IFX_1EDI2010AS_PRW_RWVAL_MSK (0x3fffu)

/** \brief Offset for Ifx_1EDI2010AS_PRW_Bits.RWVAL */
#define IFX_1EDI2010AS_PRW_RWVAL_OFF (2u)

/** \brief Length for Ifx_1EDI2010AS_PPIN_Bits.P */
#define IFX_1EDI2010AS_PPIN_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PPIN_Bits.P */
#define IFX_1EDI2010AS_PPIN_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PPIN_Bits.P */
#define IFX_1EDI2010AS_PPIN_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_PPIN_Bits.LMI */
#define IFX_1EDI2010AS_PPIN_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PPIN_Bits.LMI */
#define IFX_1EDI2010AS_PPIN_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PPIN_Bits.LMI */
#define IFX_1EDI2010AS_PPIN_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_PPIN_Bits.INPL */
#define IFX_1EDI2010AS_PPIN_INPL_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PPIN_Bits.INPL */
#define IFX_1EDI2010AS_PPIN_INPL_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PPIN_Bits.INPL */
#define IFX_1EDI2010AS_PPIN_INPL_OFF (2u)

/** \brief Length for Ifx_1EDI2010AS_PPIN_Bits.INSTPL */
#define IFX_1EDI2010AS_PPIN_INSTPL_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PPIN_Bits.INSTPL */
#define IFX_1EDI2010AS_PPIN_INSTPL_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PPIN_Bits.INSTPL */
#define IFX_1EDI2010AS_PPIN_INSTPL_OFF (3u)

/** \brief Length for Ifx_1EDI2010AS_PPIN_Bits.ENL */
#define IFX_1EDI2010AS_PPIN_ENL_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PPIN_Bits.ENL */
#define IFX_1EDI2010AS_PPIN_ENL_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PPIN_Bits.ENL */
#define IFX_1EDI2010AS_PPIN_ENL_OFF (4u)

/** \brief Length for Ifx_1EDI2010AS_PPIN_Bits.NFLTAL */
#define IFX_1EDI2010AS_PPIN_NFLTAL_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PPIN_Bits.NFLTAL */
#define IFX_1EDI2010AS_PPIN_NFLTAL_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PPIN_Bits.NFLTAL */
#define IFX_1EDI2010AS_PPIN_NFLTAL_OFF (5u)

/** \brief Length for Ifx_1EDI2010AS_PPIN_Bits.NFLTBL */
#define IFX_1EDI2010AS_PPIN_NFLTBL_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PPIN_Bits.NFLTBL */
#define IFX_1EDI2010AS_PPIN_NFLTBL_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PPIN_Bits.NFLTBL */
#define IFX_1EDI2010AS_PPIN_NFLTBL_OFF (6u)

/** \brief Length for Ifx_1EDI2010AS_PPIN_Bits.ADCTL */
#define IFX_1EDI2010AS_PPIN_ADCTL_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PPIN_Bits.ADCTL */
#define IFX_1EDI2010AS_PPIN_ADCTL_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PPIN_Bits.ADCTL */
#define IFX_1EDI2010AS_PPIN_ADCTL_OFF (7u)

/** \brief Length for Ifx_1EDI2010AS_PPIN_Bits.DIO1L */
#define IFX_1EDI2010AS_PPIN_DIO1L_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PPIN_Bits.DIO1L */
#define IFX_1EDI2010AS_PPIN_DIO1L_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PPIN_Bits.DIO1L */
#define IFX_1EDI2010AS_PPIN_DIO1L_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_PCS_Bits.P */
#define IFX_1EDI2010AS_PCS_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCS_Bits.P */
#define IFX_1EDI2010AS_PCS_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCS_Bits.P */
#define IFX_1EDI2010AS_PCS_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_PCS_Bits.LMI */
#define IFX_1EDI2010AS_PCS_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_PCS_Bits.LMI */
#define IFX_1EDI2010AS_PCS_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_PCS_Bits.LMI */
#define IFX_1EDI2010AS_PCS_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_PCS_Bits.CSP */
#define IFX_1EDI2010AS_PCS_CSP_LEN (8u)

/** \brief Mask for Ifx_1EDI2010AS_PCS_Bits.CSP */
#define IFX_1EDI2010AS_PCS_CSP_MSK (0xffu)

/** \brief Offset for Ifx_1EDI2010AS_PCS_Bits.CSP */
#define IFX_1EDI2010AS_PCS_CSP_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_SID_Bits.P */
#define IFX_1EDI2010AS_SID_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SID_Bits.P */
#define IFX_1EDI2010AS_SID_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SID_Bits.P */
#define IFX_1EDI2010AS_SID_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SID_Bits.LMI */
#define IFX_1EDI2010AS_SID_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SID_Bits.LMI */
#define IFX_1EDI2010AS_SID_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SID_Bits.LMI */
#define IFX_1EDI2010AS_SID_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SID_Bits.SVERS */
#define IFX_1EDI2010AS_SID_SVERS_LEN (12u)

/** \brief Mask for Ifx_1EDI2010AS_SID_Bits.SVERS */
#define IFX_1EDI2010AS_SID_SVERS_MSK (0xfffu)

/** \brief Offset for Ifx_1EDI2010AS_SID_Bits.SVERS */
#define IFX_1EDI2010AS_SID_SVERS_OFF (4u)

/** \brief Length for Ifx_1EDI2010AS_SSTAT_Bits.P */
#define IFX_1EDI2010AS_SSTAT_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTAT_Bits.P */
#define IFX_1EDI2010AS_SSTAT_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTAT_Bits.P */
#define IFX_1EDI2010AS_SSTAT_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SSTAT_Bits.LMI */
#define IFX_1EDI2010AS_SSTAT_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTAT_Bits.LMI */
#define IFX_1EDI2010AS_SSTAT_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTAT_Bits.LMI */
#define IFX_1EDI2010AS_SSTAT_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SSTAT_Bits.PWM */
#define IFX_1EDI2010AS_SSTAT_PWM_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTAT_Bits.PWM */
#define IFX_1EDI2010AS_SSTAT_PWM_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTAT_Bits.PWM */
#define IFX_1EDI2010AS_SSTAT_PWM_OFF (4u)

/** \brief Length for Ifx_1EDI2010AS_SSTAT_Bits.DBG */
#define IFX_1EDI2010AS_SSTAT_DBG_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTAT_Bits.DBG */
#define IFX_1EDI2010AS_SSTAT_DBG_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTAT_Bits.DBG */
#define IFX_1EDI2010AS_SSTAT_DBG_OFF (10u)

/** \brief Length for Ifx_1EDI2010AS_SSTAT2_Bits.P */
#define IFX_1EDI2010AS_SSTAT2_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTAT2_Bits.P */
#define IFX_1EDI2010AS_SSTAT2_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTAT2_Bits.P */
#define IFX_1EDI2010AS_SSTAT2_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SSTAT2_Bits.LMI */
#define IFX_1EDI2010AS_SSTAT2_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTAT2_Bits.LMI */
#define IFX_1EDI2010AS_SSTAT2_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTAT2_Bits.LMI */
#define IFX_1EDI2010AS_SSTAT2_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SSTAT2_Bits.DSATC */
#define IFX_1EDI2010AS_SSTAT2_DSATC_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTAT2_Bits.DSATC */
#define IFX_1EDI2010AS_SSTAT2_DSATC_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTAT2_Bits.DSATC */
#define IFX_1EDI2010AS_SSTAT2_DSATC_OFF (4u)

/** \brief Length for Ifx_1EDI2010AS_SSTAT2_Bits.OCPC */
#define IFX_1EDI2010AS_SSTAT2_OCPC_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTAT2_Bits.OCPC */
#define IFX_1EDI2010AS_SSTAT2_OCPC_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTAT2_Bits.OCPC */
#define IFX_1EDI2010AS_SSTAT2_OCPC_OFF (5u)

/** \brief Length for Ifx_1EDI2010AS_SSTAT2_Bits.UVLO2M */
#define IFX_1EDI2010AS_SSTAT2_UVLO2M_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTAT2_Bits.UVLO2M */
#define IFX_1EDI2010AS_SSTAT2_UVLO2M_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTAT2_Bits.UVLO2M */
#define IFX_1EDI2010AS_SSTAT2_UVLO2M_OFF (6u)

/** \brief Length for Ifx_1EDI2010AS_SSTAT2_Bits.DIO2L */
#define IFX_1EDI2010AS_SSTAT2_DIO2L_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTAT2_Bits.DIO2L */
#define IFX_1EDI2010AS_SSTAT2_DIO2L_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTAT2_Bits.DIO2L */
#define IFX_1EDI2010AS_SSTAT2_DIO2L_OFF (7u)

/** \brief Length for Ifx_1EDI2010AS_SSTAT2_Bits.DACL */
#define IFX_1EDI2010AS_SSTAT2_DACL_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTAT2_Bits.DACL */
#define IFX_1EDI2010AS_SSTAT2_DACL_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTAT2_Bits.DACL */
#define IFX_1EDI2010AS_SSTAT2_DACL_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_SER_Bits.P */
#define IFX_1EDI2010AS_SER_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SER_Bits.P */
#define IFX_1EDI2010AS_SER_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SER_Bits.P */
#define IFX_1EDI2010AS_SER_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SER_Bits.LMI */
#define IFX_1EDI2010AS_SER_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SER_Bits.LMI */
#define IFX_1EDI2010AS_SER_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SER_Bits.LMI */
#define IFX_1EDI2010AS_SER_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SER_Bits.CERS */
#define IFX_1EDI2010AS_SER_CERS_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SER_Bits.CERS */
#define IFX_1EDI2010AS_SER_CERS_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SER_Bits.CERS */
#define IFX_1EDI2010AS_SER_CERS_OFF (4u)

/** \brief Length for Ifx_1EDI2010AS_SER_Bits.AUVER */
#define IFX_1EDI2010AS_SER_AUVER_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SER_Bits.AUVER */
#define IFX_1EDI2010AS_SER_AUVER_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SER_Bits.AUVER */
#define IFX_1EDI2010AS_SER_AUVER_OFF (5u)

/** \brief Length for Ifx_1EDI2010AS_SER_Bits.AOVER */
#define IFX_1EDI2010AS_SER_AOVER_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SER_Bits.AOVER */
#define IFX_1EDI2010AS_SER_AOVER_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SER_Bits.AOVER */
#define IFX_1EDI2010AS_SER_AOVER_OFF (6u)

/** \brief Length for Ifx_1EDI2010AS_SER_Bits.VMTO */
#define IFX_1EDI2010AS_SER_VMTO_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SER_Bits.VMTO */
#define IFX_1EDI2010AS_SER_VMTO_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SER_Bits.VMTO */
#define IFX_1EDI2010AS_SER_VMTO_OFF (9u)

/** \brief Length for Ifx_1EDI2010AS_SER_Bits.UVLO2ER */
#define IFX_1EDI2010AS_SER_UVLO2ER_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SER_Bits.UVLO2ER */
#define IFX_1EDI2010AS_SER_UVLO2ER_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SER_Bits.UVLO2ER */
#define IFX_1EDI2010AS_SER_UVLO2ER_OFF (12u)

/** \brief Length for Ifx_1EDI2010AS_SER_Bits.DESATER */
#define IFX_1EDI2010AS_SER_DESATER_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SER_Bits.DESATER */
#define IFX_1EDI2010AS_SER_DESATER_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SER_Bits.DESATER */
#define IFX_1EDI2010AS_SER_DESATER_OFF (13u)

/** \brief Length for Ifx_1EDI2010AS_SER_Bits.OCPER */
#define IFX_1EDI2010AS_SER_OCPER_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SER_Bits.OCPER */
#define IFX_1EDI2010AS_SER_OCPER_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SER_Bits.OCPER */
#define IFX_1EDI2010AS_SER_OCPER_OFF (14u)

/** \brief Length for Ifx_1EDI2010AS_SER_Bits.RSTS */
#define IFX_1EDI2010AS_SER_RSTS_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SER_Bits.RSTS */
#define IFX_1EDI2010AS_SER_RSTS_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SER_Bits.RSTS */
#define IFX_1EDI2010AS_SER_RSTS_OFF (15u)

/** \brief Length for Ifx_1EDI2010AS_SCFG_Bits.P */
#define IFX_1EDI2010AS_SCFG_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG_Bits.P */
#define IFX_1EDI2010AS_SCFG_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG_Bits.P */
#define IFX_1EDI2010AS_SCFG_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SCFG_Bits.LMI */
#define IFX_1EDI2010AS_SCFG_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG_Bits.LMI */
#define IFX_1EDI2010AS_SCFG_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG_Bits.LMI */
#define IFX_1EDI2010AS_SCFG_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SCFG_Bits.VBEC */
#define IFX_1EDI2010AS_SCFG_VBEC_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG_Bits.VBEC */
#define IFX_1EDI2010AS_SCFG_VBEC_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG_Bits.VBEC */
#define IFX_1EDI2010AS_SCFG_VBEC_OFF (4u)

/** \brief Length for Ifx_1EDI2010AS_SCFG_Bits.CFG2 */
#define IFX_1EDI2010AS_SCFG_CFG2_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG_Bits.CFG2 */
#define IFX_1EDI2010AS_SCFG_CFG2_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG_Bits.CFG2 */
#define IFX_1EDI2010AS_SCFG_CFG2_OFF (5u)

/** \brief Length for Ifx_1EDI2010AS_SCFG_Bits.DIO2C */
#define IFX_1EDI2010AS_SCFG_DIO2C_LEN (2u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG_Bits.DIO2C */
#define IFX_1EDI2010AS_SCFG_DIO2C_MSK (0x3u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG_Bits.DIO2C */
#define IFX_1EDI2010AS_SCFG_DIO2C_OFF (6u)

/** \brief Length for Ifx_1EDI2010AS_SCFG_Bits.DSTCEN */
#define IFX_1EDI2010AS_SCFG_DSTCEN_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG_Bits.DSTCEN */
#define IFX_1EDI2010AS_SCFG_DSTCEN_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG_Bits.DSTCEN */
#define IFX_1EDI2010AS_SCFG_DSTCEN_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_SCFG_Bits.PSEN */
#define IFX_1EDI2010AS_SCFG_PSEN_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG_Bits.PSEN */
#define IFX_1EDI2010AS_SCFG_PSEN_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG_Bits.PSEN */
#define IFX_1EDI2010AS_SCFG_PSEN_OFF (9u)

/** \brief Length for Ifx_1EDI2010AS_SCFG_Bits.TOSEN */
#define IFX_1EDI2010AS_SCFG_TOSEN_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG_Bits.TOSEN */
#define IFX_1EDI2010AS_SCFG_TOSEN_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG_Bits.TOSEN */
#define IFX_1EDI2010AS_SCFG_TOSEN_OFF (10u)

/** \brief Length for Ifx_1EDI2010AS_SCFG_Bits.DSATLS */
#define IFX_1EDI2010AS_SCFG_DSATLS_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG_Bits.DSATLS */
#define IFX_1EDI2010AS_SCFG_DSATLS_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG_Bits.DSATLS */
#define IFX_1EDI2010AS_SCFG_DSATLS_OFF (11u)

/** \brief Length for Ifx_1EDI2010AS_SCFG_Bits.UVLO2S */
#define IFX_1EDI2010AS_SCFG_UVLO2S_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG_Bits.UVLO2S */
#define IFX_1EDI2010AS_SCFG_UVLO2S_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG_Bits.UVLO2S */
#define IFX_1EDI2010AS_SCFG_UVLO2S_OFF (12u)

/** \brief Length for Ifx_1EDI2010AS_SCFG_Bits.OCPLS */
#define IFX_1EDI2010AS_SCFG_OCPLS_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG_Bits.OCPLS */
#define IFX_1EDI2010AS_SCFG_OCPLS_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG_Bits.OCPLS */
#define IFX_1EDI2010AS_SCFG_OCPLS_OFF (13u)

/** \brief Length for Ifx_1EDI2010AS_SCFG_Bits.DACLC */
#define IFX_1EDI2010AS_SCFG_DACLC_LEN (2u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG_Bits.DACLC */
#define IFX_1EDI2010AS_SCFG_DACLC_MSK (0x3u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG_Bits.DACLC */
#define IFX_1EDI2010AS_SCFG_DACLC_OFF (14u)

/** \brief Length for Ifx_1EDI2010AS_SCFG2_Bits.P */
#define IFX_1EDI2010AS_SCFG2_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG2_Bits.P */
#define IFX_1EDI2010AS_SCFG2_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG2_Bits.P */
#define IFX_1EDI2010AS_SCFG2_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SCFG2_Bits.LMI */
#define IFX_1EDI2010AS_SCFG2_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG2_Bits.LMI */
#define IFX_1EDI2010AS_SCFG2_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG2_Bits.LMI */
#define IFX_1EDI2010AS_SCFG2_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SCFG2_Bits.PWMD */
#define IFX_1EDI2010AS_SCFG2_PWMD_LEN (2u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG2_Bits.PWMD */
#define IFX_1EDI2010AS_SCFG2_PWMD_MSK (0x3u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG2_Bits.PWMD */
#define IFX_1EDI2010AS_SCFG2_PWMD_OFF (4u)

/** \brief Length for Ifx_1EDI2010AS_SCFG2_Bits.ATS */
#define IFX_1EDI2010AS_SCFG2_ATS_LEN (2u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG2_Bits.ATS */
#define IFX_1EDI2010AS_SCFG2_ATS_MSK (0x3u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG2_Bits.ATS */
#define IFX_1EDI2010AS_SCFG2_ATS_OFF (6u)

/** \brief Length for Ifx_1EDI2010AS_SCFG2_Bits.AGS */
#define IFX_1EDI2010AS_SCFG2_AGS_LEN (2u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG2_Bits.AGS */
#define IFX_1EDI2010AS_SCFG2_AGS_MSK (0x3u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG2_Bits.AGS */
#define IFX_1EDI2010AS_SCFG2_AGS_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_SCFG2_Bits.AOS */
#define IFX_1EDI2010AS_SCFG2_AOS_LEN (3u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG2_Bits.AOS */
#define IFX_1EDI2010AS_SCFG2_AOS_MSK (0x7u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG2_Bits.AOS */
#define IFX_1EDI2010AS_SCFG2_AOS_OFF (10u)

/** \brief Length for Ifx_1EDI2010AS_SCFG2_Bits.ACSS */
#define IFX_1EDI2010AS_SCFG2_ACSS_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG2_Bits.ACSS */
#define IFX_1EDI2010AS_SCFG2_ACSS_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG2_Bits.ACSS */
#define IFX_1EDI2010AS_SCFG2_ACSS_OFF (13u)

/** \brief Length for Ifx_1EDI2010AS_SCFG2_Bits.ACAEN */
#define IFX_1EDI2010AS_SCFG2_ACAEN_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG2_Bits.ACAEN */
#define IFX_1EDI2010AS_SCFG2_ACAEN_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG2_Bits.ACAEN */
#define IFX_1EDI2010AS_SCFG2_ACAEN_OFF (14u)

/** \brief Length for Ifx_1EDI2010AS_SCFG2_Bits.ADCEN */
#define IFX_1EDI2010AS_SCFG2_ADCEN_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCFG2_Bits.ADCEN */
#define IFX_1EDI2010AS_SCFG2_ADCEN_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCFG2_Bits.ADCEN */
#define IFX_1EDI2010AS_SCFG2_ADCEN_OFF (15u)

/** \brief Length for Ifx_1EDI2010AS_SSCR_Bits.P */
#define IFX_1EDI2010AS_SSCR_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSCR_Bits.P */
#define IFX_1EDI2010AS_SSCR_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSCR_Bits.P */
#define IFX_1EDI2010AS_SSCR_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SSCR_Bits.LMI */
#define IFX_1EDI2010AS_SSCR_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSCR_Bits.LMI */
#define IFX_1EDI2010AS_SSCR_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSCR_Bits.LMI */
#define IFX_1EDI2010AS_SSCR_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SSCR_Bits.VFS2 */
#define IFX_1EDI2010AS_SSCR_VFS2_LEN (2u)

/** \brief Mask for Ifx_1EDI2010AS_SSCR_Bits.VFS2 */
#define IFX_1EDI2010AS_SSCR_VFS2_MSK (0x3u)

/** \brief Offset for Ifx_1EDI2010AS_SSCR_Bits.VFS2 */
#define IFX_1EDI2010AS_SSCR_VFS2_OFF (4u)

/** \brief Length for Ifx_1EDI2010AS_SDESAT_Bits.P */
#define IFX_1EDI2010AS_SDESAT_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SDESAT_Bits.P */
#define IFX_1EDI2010AS_SDESAT_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SDESAT_Bits.P */
#define IFX_1EDI2010AS_SDESAT_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SDESAT_Bits.LMI */
#define IFX_1EDI2010AS_SDESAT_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SDESAT_Bits.LMI */
#define IFX_1EDI2010AS_SDESAT_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SDESAT_Bits.LMI */
#define IFX_1EDI2010AS_SDESAT_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SDESAT_Bits.DSATBT */
#define IFX_1EDI2010AS_SDESAT_DSATBT_LEN (8u)

/** \brief Mask for Ifx_1EDI2010AS_SDESAT_Bits.DSATBT */
#define IFX_1EDI2010AS_SDESAT_DSATBT_MSK (0xffu)

/** \brief Offset for Ifx_1EDI2010AS_SDESAT_Bits.DSATBT */
#define IFX_1EDI2010AS_SDESAT_DSATBT_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_SOCP_Bits.P */
#define IFX_1EDI2010AS_SOCP_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SOCP_Bits.P */
#define IFX_1EDI2010AS_SOCP_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SOCP_Bits.P */
#define IFX_1EDI2010AS_SOCP_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SOCP_Bits.LMI */
#define IFX_1EDI2010AS_SOCP_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SOCP_Bits.LMI */
#define IFX_1EDI2010AS_SOCP_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SOCP_Bits.LMI */
#define IFX_1EDI2010AS_SOCP_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SOCP_Bits.OCPBT */
#define IFX_1EDI2010AS_SOCP_OCPBT_LEN (8u)

/** \brief Mask for Ifx_1EDI2010AS_SOCP_Bits.OCPBT */
#define IFX_1EDI2010AS_SOCP_OCPBT_MSK (0xffu)

/** \brief Offset for Ifx_1EDI2010AS_SOCP_Bits.OCPBT */
#define IFX_1EDI2010AS_SOCP_OCPBT_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_SRTTOF_Bits.P */
#define IFX_1EDI2010AS_SRTTOF_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SRTTOF_Bits.P */
#define IFX_1EDI2010AS_SRTTOF_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SRTTOF_Bits.P */
#define IFX_1EDI2010AS_SRTTOF_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SRTTOF_Bits.LMI */
#define IFX_1EDI2010AS_SRTTOF_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SRTTOF_Bits.LMI */
#define IFX_1EDI2010AS_SRTTOF_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SRTTOF_Bits.LMI */
#define IFX_1EDI2010AS_SRTTOF_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SRTTOF_Bits.RTVAL */
#define IFX_1EDI2010AS_SRTTOF_RTVAL_LEN (8u)

/** \brief Mask for Ifx_1EDI2010AS_SRTTOF_Bits.RTVAL */
#define IFX_1EDI2010AS_SRTTOF_RTVAL_MSK (0xffu)

/** \brief Offset for Ifx_1EDI2010AS_SRTTOF_Bits.RTVAL */
#define IFX_1EDI2010AS_SRTTOF_RTVAL_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_SSTTOF_Bits.P */
#define IFX_1EDI2010AS_SSTTOF_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTTOF_Bits.P */
#define IFX_1EDI2010AS_SSTTOF_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTTOF_Bits.P */
#define IFX_1EDI2010AS_SSTTOF_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SSTTOF_Bits.LMI */
#define IFX_1EDI2010AS_SSTTOF_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SSTTOF_Bits.LMI */
#define IFX_1EDI2010AS_SSTTOF_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SSTTOF_Bits.LMI */
#define IFX_1EDI2010AS_SSTTOF_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SSTTOF_Bits.GPS */
#define IFX_1EDI2010AS_SSTTOF_GPS_LEN (3u)

/** \brief Mask for Ifx_1EDI2010AS_SSTTOF_Bits.GPS */
#define IFX_1EDI2010AS_SSTTOF_GPS_MSK (0x7u)

/** \brief Offset for Ifx_1EDI2010AS_SSTTOF_Bits.GPS */
#define IFX_1EDI2010AS_SSTTOF_GPS_OFF (5u)

/** \brief Length for Ifx_1EDI2010AS_SSTTOF_Bits.STVAL */
#define IFX_1EDI2010AS_SSTTOF_STVAL_LEN (8u)

/** \brief Mask for Ifx_1EDI2010AS_SSTTOF_Bits.STVAL */
#define IFX_1EDI2010AS_SSTTOF_STVAL_MSK (0xffu)

/** \brief Offset for Ifx_1EDI2010AS_SSTTOF_Bits.STVAL */
#define IFX_1EDI2010AS_SSTTOF_STVAL_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_STTON_Bits.P */
#define IFX_1EDI2010AS_STTON_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_STTON_Bits.P */
#define IFX_1EDI2010AS_STTON_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_STTON_Bits.P */
#define IFX_1EDI2010AS_STTON_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_STTON_Bits.LMI */
#define IFX_1EDI2010AS_STTON_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_STTON_Bits.LMI */
#define IFX_1EDI2010AS_STTON_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_STTON_Bits.LMI */
#define IFX_1EDI2010AS_STTON_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_STTON_Bits.TTONVAL */
#define IFX_1EDI2010AS_STTON_TTONVAL_LEN (8u)

/** \brief Mask for Ifx_1EDI2010AS_STTON_Bits.TTONVAL */
#define IFX_1EDI2010AS_STTON_TTONVAL_MSK (0xffu)

/** \brief Offset for Ifx_1EDI2010AS_STTON_Bits.TTONVAL */
#define IFX_1EDI2010AS_STTON_TTONVAL_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_SADC_Bits.P */
#define IFX_1EDI2010AS_SADC_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SADC_Bits.P */
#define IFX_1EDI2010AS_SADC_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SADC_Bits.P */
#define IFX_1EDI2010AS_SADC_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SADC_Bits.LMI */
#define IFX_1EDI2010AS_SADC_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SADC_Bits.LMI */
#define IFX_1EDI2010AS_SADC_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SADC_Bits.LMI */
#define IFX_1EDI2010AS_SADC_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SADC_Bits.AVFS */
#define IFX_1EDI2010AS_SADC_AVFS_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SADC_Bits.AVFS */
#define IFX_1EDI2010AS_SADC_AVFS_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SADC_Bits.AVFS */
#define IFX_1EDI2010AS_SADC_AVFS_OFF (4u)

/** \brief Length for Ifx_1EDI2010AS_SADC_Bits.AUVS */
#define IFX_1EDI2010AS_SADC_AUVS_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SADC_Bits.AUVS */
#define IFX_1EDI2010AS_SADC_AUVS_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SADC_Bits.AUVS */
#define IFX_1EDI2010AS_SADC_AUVS_OFF (5u)

/** \brief Length for Ifx_1EDI2010AS_SADC_Bits.AOVS */
#define IFX_1EDI2010AS_SADC_AOVS_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SADC_Bits.AOVS */
#define IFX_1EDI2010AS_SADC_AOVS_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SADC_Bits.AOVS */
#define IFX_1EDI2010AS_SADC_AOVS_OFF (6u)

/** \brief Length for Ifx_1EDI2010AS_SADC_Bits.ADCVAL */
#define IFX_1EDI2010AS_SADC_ADCVAL_LEN (8u)

/** \brief Mask for Ifx_1EDI2010AS_SADC_Bits.ADCVAL */
#define IFX_1EDI2010AS_SADC_ADCVAL_MSK (0xffu)

/** \brief Offset for Ifx_1EDI2010AS_SADC_Bits.ADCVAL */
#define IFX_1EDI2010AS_SADC_ADCVAL_OFF (8u)

/** \brief Length for Ifx_1EDI2010AS_SBC_Bits.P */
#define IFX_1EDI2010AS_SBC_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SBC_Bits.P */
#define IFX_1EDI2010AS_SBC_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SBC_Bits.P */
#define IFX_1EDI2010AS_SBC_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SBC_Bits.LMI */
#define IFX_1EDI2010AS_SBC_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SBC_Bits.LMI */
#define IFX_1EDI2010AS_SBC_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SBC_Bits.LMI */
#define IFX_1EDI2010AS_SBC_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SBC_Bits.LCB1A */
#define IFX_1EDI2010AS_SBC_LCB1A_LEN (6u)

/** \brief Mask for Ifx_1EDI2010AS_SBC_Bits.LCB1A */
#define IFX_1EDI2010AS_SBC_LCB1A_MSK (0x3fu)

/** \brief Offset for Ifx_1EDI2010AS_SBC_Bits.LCB1A */
#define IFX_1EDI2010AS_SBC_LCB1A_OFF (4u)

/** \brief Length for Ifx_1EDI2010AS_SBC_Bits.LCB1B */
#define IFX_1EDI2010AS_SBC_LCB1B_LEN (6u)

/** \brief Mask for Ifx_1EDI2010AS_SBC_Bits.LCB1B */
#define IFX_1EDI2010AS_SBC_LCB1B_MSK (0x3fu)

/** \brief Offset for Ifx_1EDI2010AS_SBC_Bits.LCB1B */
#define IFX_1EDI2010AS_SBC_LCB1B_OFF (10u)

/** \brief Length for Ifx_1EDI2010AS_SCS_Bits.P */
#define IFX_1EDI2010AS_SCS_P_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCS_Bits.P */
#define IFX_1EDI2010AS_SCS_P_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCS_Bits.P */
#define IFX_1EDI2010AS_SCS_P_OFF (0u)

/** \brief Length for Ifx_1EDI2010AS_SCS_Bits.LMI */
#define IFX_1EDI2010AS_SCS_LMI_LEN (1u)

/** \brief Mask for Ifx_1EDI2010AS_SCS_Bits.LMI */
#define IFX_1EDI2010AS_SCS_LMI_MSK (0x1u)

/** \brief Offset for Ifx_1EDI2010AS_SCS_Bits.LMI */
#define IFX_1EDI2010AS_SCS_LMI_OFF (1u)

/** \brief Length for Ifx_1EDI2010AS_SCS_Bits.SCSS */
#define IFX_1EDI2010AS_SCS_SCSS_LEN (8u)

/** \brief Mask for Ifx_1EDI2010AS_SCS_Bits.SCSS */
#define IFX_1EDI2010AS_SCS_SCSS_MSK (0xffu)

/** \brief Offset for Ifx_1EDI2010AS_SCS_Bits.SCSS */
#define IFX_1EDI2010AS_SCS_SCSS_OFF (8u)

/** \}  */
/******************************************************************************/
/******************************************************************************/
#endif /* IFX1EDI2010AS_BF_H */
