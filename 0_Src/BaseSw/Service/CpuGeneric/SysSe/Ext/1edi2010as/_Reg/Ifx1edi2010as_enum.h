/**
 * \file Ifx1edi2010as_enum.h 
 * \brief Enumeration for 1EDI2010AS register bitfields 
 *  
 * Date: 2017-06-30 15:39:22 GMT
 * Version: TBD
 * Specification: TBD
 * MAY BE CHANGED BY USER [yes/no]: Yes
 *
 * \copyright Copyright (c) 2017 Infineon Technologies AG. All rights reserved. 
 *  
 *  
 *                                 IMPORTANT NOTICE 
 *  
 * Infineon Technologies AG (Infineon) is supplying this file for use 
 * exclusively with Infineon's microcontroller products. This file can be freely 
 * distributed within development tools that are supporting such microcontroller 
 * products. 
 *  
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED 
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. 
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, 
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER. 
 *  
 * \defgroup IfxLld_1edi2010as_Registers_Enum Enumeration 
 * \ingroup IfxLld_1edi2010as_Registers
 */

#ifndef IFX1EDI2010AS_ENUM_H
#define IFX1EDI2010AS_ENUM_H 1
/******************************************************************************/
/** \addtogroup IfxLld_1edi2010as_Registers_Enum
 * \{  */

/** \brief Primary Chip Identification
 * Bitfield(s): Ifx1edi2010as_PID.PVERS
 */
typedef enum
{
    Ifx1edi2010as_PrimaryChipIdentification_a11 = 1169,/**< \brief A11 Step. */
}Ifx1edi2010as_PrimaryChipIdentification;

/** \brief Secondary Ready Status
 * Bitfield(s): Ifx1edi2010as_PSTAT.SRDY
 */
typedef enum
{
    Ifx1edi2010as_SecondaryReadyStatus_notReady = 0,  /**< \brief Secondary chip is not ready. */
    Ifx1edi2010as_SecondaryReadyStatus_ready = 1,     /**< \brief Secondary chip is ready. */
}Ifx1edi2010as_SecondaryReadyStatus;

/** \brief Active State Status
 * Bitfield(s): Ifx1edi2010as_PSTAT.ACT
 */
typedef enum
{
    Ifx1edi2010as_ActiveStateStatus_notActive = 0,    /**< \brief The device is not in Active State. */
    Ifx1edi2010as_ActiveStateStatus_active = 1,       /**< \brief The device is in Active State. */
}Ifx1edi2010as_ActiveStateStatus;

/** \brief Error Status
 * Bitfield(s): Ifx1edi2010as_PSTAT.ERR
 */
typedef enum
{
    Ifx1edi2010as_ErrorStatus_noError = 0,            /**< \brief No error is detected. */
    Ifx1edi2010as_ErrorStatus_error = 1,              /**< \brief An error is detected. */
}Ifx1edi2010as_ErrorStatus;

/** \brief EN Valid Status
 * Bitfield(s): Ifx1edi2010as_PSTAT2.ENVAL
 */
typedef enum
{
    Ifx1edi2010as_EnValidStatus_notValid = 0,         /**< \brief A non-valid signal is detected. */
    Ifx1edi2010as_EnValidStatus_valid = 1,            /**< \brief A valid signal is detected. */
}Ifx1edi2010as_EnValidStatus;

/** \brief NFLTA Pin Driver Request
 * Bitfield(s): Ifx1edi2010as_PSTAT2.FLTA
 */
typedef enum
{
    Ifx1edi2010as_NfltaPinDriverRequest_tristate = 0, /**< \brief  */
    Ifx1edi2010as_NfltaPinDriverRequest_lowLevel = 1, /**< \brief A Low Level issued at */
}Ifx1edi2010as_NfltaPinDriverRequest;

/** \brief NFLTB Pin Driver Request
 * Bitfield(s): Ifx1edi2010as_PSTAT2.FLTB
 */
typedef enum
{
    Ifx1edi2010as_NfltbPinDriverRequest_tristate = 0, /**< \brief  */
    Ifx1edi2010as_NfltbPinDriverRequest_lowLevel = 1, /**< \brief A Low Level is issued at */
}Ifx1edi2010as_NfltbPinDriverRequest;

/** \brief Operating Mode
 * Bitfield(s): Ifx1edi2010as_PSTAT2.OPMP
 */
typedef enum
{
    Ifx1edi2010as_OperatingMode_opm0 = 0,             /**< \brief Mode OPM0 is active (reset). */
    Ifx1edi2010as_OperatingMode_opm1 = 1,             /**< \brief Mode OPM1 is active (Event Class B) . */
    Ifx1edi2010as_OperatingMode_opm2 = 2,             /**< \brief Mode OPM2 is active. */
    Ifx1edi2010as_OperatingMode_opm3 = 3,             /**< \brief Mode OPM3 is active. */
    Ifx1edi2010as_OperatingMode_opm4 = 4,             /**< \brief Mode OPM4 is active. */
    Ifx1edi2010as_OperatingMode_opm5 = 5,             /**< \brief Mode OPM5 is active. */
    Ifx1edi2010as_OperatingMode_opm6 = 6,             /**< \brief Mode OPM6 is active. */
}Ifx1edi2010as_OperatingMode;

/** \brief Event Class A Status
 * Bitfield(s): Ifx1edi2010as_PSTAT2.FLTAP
 */
typedef enum
{
    Ifx1edi2010as_EventClassAStatus_noError = 0,      /**< \brief No error condition is detected. */
    Ifx1edi2010as_EventClassAStatus_error = 1,        /**< \brief An error condition is detected. */
}Ifx1edi2010as_EventClassAStatus;

/** \brief Event Class B Status
 * Bitfield(s): Ifx1edi2010as_PSTAT2.FLTBP
 */
typedef enum
{
    Ifx1edi2010as_EventClassBStatus_noError = 0,      /**< \brief No error condition detected. */
    Ifx1edi2010as_EventClassBStatus_error = 1,        /**< \brief An error condition is detected. */
}Ifx1edi2010as_EventClassBStatus;

/** \brief Shoot Through Protection Status
 * Bitfield(s): Ifx1edi2010as_PSTAT2.STP
 */
typedef enum
{
    Ifx1edi2010as_ShootThroughProtectionStatus_inhibitionNotActive = 0,/**< \brief STP inhibition is not active. */
    Ifx1edi2010as_ShootThroughProtectionStatus_inhibitionActive = 1,/**< \brief STP inhibition is active. */
}Ifx1edi2010as_ShootThroughProtectionStatus;

/** \brief ADC Under Or Overvoltage Error Status
 * Bitfield(s): Ifx1edi2010as_PSTAT2.AXVP
 */
typedef enum
{
    Ifx1edi2010as_AdcUnderOrOvervoltageErrorStatus_noError = 0,/**< \brief An error condition is not detected. */
    Ifx1edi2010as_AdcUnderOrOvervoltageErrorStatus_error = 1,/**< \brief An error condition is being detected */
}Ifx1edi2010as_AdcUnderOrOvervoltageErrorStatus;

/** \brief Primary Communication Error Flag
 * Bitfield(s): Ifx1edi2010as_PER.CERP
 */
typedef enum
{
    Ifx1edi2010as_PrimaryCommunicationErrorFlag_notSet = 0,/**< \brief No event has been detected. */
    Ifx1edi2010as_PrimaryCommunicationErrorFlag_set = 1,/**< \brief An event has been detected. */
}Ifx1edi2010as_PrimaryCommunicationErrorFlag;

/** \brief ADC Error Flag
 * Bitfield(s): Ifx1edi2010as_PER.ADER
 */
typedef enum
{
    Ifx1edi2010as_AdcErrorFlag_notSet = 0,            /**< \brief No error condition has been detected. */
    Ifx1edi2010as_AdcErrorFlag_set = 1,               /**< \brief An error condition has been detected has been detected. */
}Ifx1edi2010as_AdcErrorFlag;

/** \brief SPI Error Flag
 * Bitfield(s): Ifx1edi2010as_PER.SPIER
 */
typedef enum
{
    Ifx1edi2010as_SpiErrorFlag_notSet = 0,            /**< \brief No error event has been detected. */
    Ifx1edi2010as_SpiErrorFlag_set = 1,               /**< \brief An error event has been detected. */
}Ifx1edi2010as_SpiErrorFlag;

/** \brief Shoot Through Protection Error Flag
 * Bitfield(s): Ifx1edi2010as_PER.STPER
 */
typedef enum
{
    Ifx1edi2010as_ShootThroughProtectionErrorFlag_notSet = 0,/**< \brief No event has been detected. */
    Ifx1edi2010as_ShootThroughProtectionErrorFlag_set = 1,/**< \brief An event has been detected. */
}Ifx1edi2010as_ShootThroughProtectionErrorFlag;

/** \brief EN Signal Invalid Flag
 * Bitfield(s): Ifx1edi2010as_PER.ENER
 */
typedef enum
{
    Ifx1edi2010as_EnSignalInvalidFlag_notSet = 0,     /**< \brief No event has been detected. */
    Ifx1edi2010as_EnSignalInvalidFlag_set = 1,        /**< \brief An event has been detected. */
}Ifx1edi2010as_EnSignalInvalidFlag;

/** \brief Primary Reset Flag
 * Bitfield(s): Ifx1edi2010as_PER.RSTP
 */
typedef enum
{
    Ifx1edi2010as_PrimaryResetFlag_notSet = 0,        /**< \brief No reset event has been detected. */
    Ifx1edi2010as_PrimaryResetFlag_set = 1,           /**< \brief A reset event has been detected. */
}Ifx1edi2010as_PrimaryResetFlag;

/** \brief Primary External Hard Reset flag
 * Bitfield(s): Ifx1edi2010as_PER.RSTEP
 */
typedef enum
{
    Ifx1edi2010as_PrimaryExternalHardResetFlag_notSet = 0,/**< \brief No external hard reset event has been detected. */
    Ifx1edi2010as_PrimaryExternalHardResetFlag_set = 1,/**< \brief An externally hard reset event has been detected. */
}Ifx1edi2010as_PrimaryExternalHardResetFlag;

/** \brief SPI Parity Enable
 * Bitfield(s): Ifx1edi2010as_PCFG.PAREN
 */
typedef enum
{
    Ifx1edi2010as_SpiParityEnable_disabled = 0,       /**< \brief Parity Check is disabled. */
    Ifx1edi2010as_SpiParityEnable_enabled = 1,        /**< \brief Parity Check is enabled. */
}Ifx1edi2010as_SpiParityEnable;

/** \brief NFLTA Pin Activation on Boundary Check Event Enable
 * Bitfield(s): Ifx1edi2010as_PCFG.ADAEN
 */
typedef enum
{
    Ifx1edi2010as_NfltaPinActivationOnBoundaryCheckEventEnable_disabled = 0,/**< \brief  */
    Ifx1edi2010as_NfltaPinActivationOnBoundaryCheckEventEnable_enabled = 1,/**< \brief  */
}Ifx1edi2010as_NfltaPinActivationOnBoundaryCheckEventEnable;

/** \brief ADC Trigger Input Enable
 * Bitfield(s): Ifx1edi2010as_PCFG.ADTEN
 */
typedef enum
{
    Ifx1edi2010as_AdcTriggerInputEnable_disabled = 0, /**< \brief  */
    Ifx1edi2010as_AdcTriggerInputEnable_enabled = 1,  /**< \brief  */
}Ifx1edi2010as_AdcTriggerInputEnable;

/** \brief Shoot Through Protection Delay
 * Bitfield(s): Ifx1edi2010as_PCFG2.STPDEL
 */
typedef enum
{
    Ifx1edi2010as_ShootThroughProtectionDelay_1 = 1,  /**< \brief 1 clock cycle. */
    Ifx1edi2010as_ShootThroughProtectionDelay_3F = 63,/**< \brief 63 clock cycles. */
}Ifx1edi2010as_ShootThroughProtectionDelay;

/** \brief DIO1 Pin Mode
 * Bitfield(s): Ifx1edi2010as_PCFG2.DIO1
 */
typedef enum
{
    Ifx1edi2010as_Dio1PinMode_input = 0,              /**< \brief  */
    Ifx1edi2010as_Dio1PinMode_output = 1,             /**< \brief  */
}Ifx1edi2010as_Dio1PinMode;

/** \brief Gate TTON Plateau Level
 * Bitfield(s): Ifx1edi2010as_PCTRL.GPON
 */
typedef enum
{
    Ifx1edi2010as_GateTtonPlateauLevel_gpon0 = 0,     /**< \brief V */
    Ifx1edi2010as_GateTtonPlateauLevel_gpon1 = 1,     /**< \brief V */
    Ifx1edi2010as_GateTtonPlateauLevel_gpon2 = 2,     /**< \brief V */
    Ifx1edi2010as_GateTtonPlateauLevel_gpon3 = 3,     /**< \brief V */
    Ifx1edi2010as_GateTtonPlateauLevel_gpon4 = 4,     /**< \brief V */
    Ifx1edi2010as_GateTtonPlateauLevel_gpon5 = 5,     /**< \brief V */
    Ifx1edi2010as_GateTtonPlateauLevel_gpon6 = 6,     /**< \brief V */
    Ifx1edi2010as_GateTtonPlateauLevel_gpon6WtoOrHardSwitching = 7,/**< \brief V */
}Ifx1edi2010as_GateTtonPlateauLevel;

/** \brief Clear Primary Sitcky Bits
 * Bitfield(s): Ifx1edi2010as_PCTRL.CLRP
 */
typedef enum
{
    Ifx1edi2010as_ClearPrimarySitckyBits_noAction = 0,/**< \brief No action. */
    Ifx1edi2010as_ClearPrimarySitckyBits_clear = 1,   /**< \brief Clear sticky bits and deassert signals */
}Ifx1edi2010as_ClearPrimarySitckyBits;

/** \brief Clear Secondary Sitcky Bits
 * Bitfield(s): Ifx1edi2010as_PCTRL.CLRS
 */
typedef enum
{
    Ifx1edi2010as_ClearSecondarySitckyBits_noAction = 0,/**< \brief No action. */
    Ifx1edi2010as_ClearSecondarySitckyBits_clear = 1, /**< \brief Clear sticky bits. */
}Ifx1edi2010as_ClearSecondarySitckyBits;

/** \brief Gate Regular TTOFF Plateau Level
 * Bitfield(s): Ifx1edi2010as_PCTRL2.GPOF
 */
typedef enum
{
    Ifx1edi2010as_GateRegularTtoffPlateauLevel_gpof0 = 0,/**< \brief V */
    Ifx1edi2010as_GateRegularTtoffPlateauLevel_gpof1 = 1,/**< \brief V */
    Ifx1edi2010as_GateRegularTtoffPlateauLevel_gpof2 = 2,/**< \brief V */
    Ifx1edi2010as_GateRegularTtoffPlateauLevel_gpof3 = 3,/**< \brief V */
    Ifx1edi2010as_GateRegularTtoffPlateauLevel_gpof4 = 4,/**< \brief V */
    Ifx1edi2010as_GateRegularTtoffPlateauLevel_gpof5 = 5,/**< \brief V */
    Ifx1edi2010as_GateRegularTtoffPlateauLevel_gpof6 = 6,/**< \brief V */
    Ifx1edi2010as_GateRegularTtoffPlateauLevel_gpof7 = 7,/**< \brief V */
}Ifx1edi2010as_GateRegularTtoffPlateauLevel;

/** \brief ADC Conversion Request
 * Bitfield(s): Ifx1edi2010as_PCTRL2.ACRP
 */
typedef enum
{
    Ifx1edi2010as_AdcConversionRequest_none = 0,      /**< \brief No conversion request pending. */
    Ifx1edi2010as_AdcConversionRequest_pending = 1,   /**< \brief A conversion request is pending.This bit is automatically cleared by hardware */
}Ifx1edi2010as_AdcConversionRequest;

/** \brief Primary Verification Function
 * Bitfield(s): Ifx1edi2010as_PSCR.VFSP
 */
typedef enum
{
    Ifx1edi2010as_PrimaryVerificationFunction_disabled = 0,/**< \brief No function activated. */
    Ifx1edi2010as_PrimaryVerificationFunction_primaryClockSupervision = 2,/**< \brief Primary Clock Supervision active. */
}Ifx1edi2010as_PrimaryVerificationFunction;

/** \brief INP Pin Level
 * Bitfield(s): Ifx1edi2010as_PPIN.INPL
 */
typedef enum
{
    Ifx1edi2010as_InpPinLevel_low = 0,                /**< \brief Low-level is detected. */
    Ifx1edi2010as_InpPinLevel_high = 1,               /**< \brief High-level is detected. */
}Ifx1edi2010as_InpPinLevel;

/** \brief INSTP Pin Level
 * Bitfield(s): Ifx1edi2010as_PPIN.INSTPL
 */
typedef enum
{
    Ifx1edi2010as_InstpPinLevel_low = 0,              /**< \brief Low-level is detected. */
    Ifx1edi2010as_InstpPinLevel_high = 1,             /**< \brief High-level is detected. */
}Ifx1edi2010as_InstpPinLevel;

/** \brief EN Pin Level
 * Bitfield(s): Ifx1edi2010as_PPIN.ENL
 */
typedef enum
{
    Ifx1edi2010as_EnPinLevel_low = 0,                 /**< \brief Low-level is detected. */
    Ifx1edi2010as_EnPinLevel_high = 1,                /**< \brief High-level is detected. */
}Ifx1edi2010as_EnPinLevel;

/** \brief NFLTA pin Level
 * Bitfield(s): Ifx1edi2010as_PPIN.NFLTAL
 */
typedef enum
{
    Ifx1edi2010as_NfltaPinLevel_low = 0,              /**< \brief Low-level is detected. */
    Ifx1edi2010as_NfltaPinLevel_high = 1,             /**< \brief High-level is detected. */
}Ifx1edi2010as_NfltaPinLevel;

/** \brief NFLTB pin level
 * Bitfield(s): Ifx1edi2010as_PPIN.NFLTBL
 */
typedef enum
{
    Ifx1edi2010as_NfltbPinLevel_low = 0,              /**< \brief Low-level is detected. */
    Ifx1edi2010as_NfltbPinLevel_high = 1,             /**< \brief High-level is detected. */
}Ifx1edi2010as_NfltbPinLevel;

/** \brief ADC Trigger Input Level
 * Bitfield(s): Ifx1edi2010as_PPIN.ADCTL
 */
typedef enum
{
    Ifx1edi2010as_AdcTriggerInputLevel_low = 0,       /**< \brief Low-level is detected. */
    Ifx1edi2010as_AdcTriggerInputLevel_high = 1,      /**< \brief High-level is detected. */
}Ifx1edi2010as_AdcTriggerInputLevel;

/** \brief DIO1 Pin Level
 * Bitfield(s): Ifx1edi2010as_PPIN.DIO1L
 */
typedef enum
{
    Ifx1edi2010as_Dio1PinLevel_low = 0,               /**< \brief Low-level is detected. */
    Ifx1edi2010as_Dio1PinLevel_high = 1,              /**< \brief High-level is detected. */
}Ifx1edi2010as_Dio1PinLevel;

/** \brief PWM Command Status
 * Bitfield(s): Ifx1edi2010as_SSTAT.PWM
 */
typedef enum
{
    Ifx1edi2010as_PwmCommandStatus_off = 0,           /**< \brief PWM OFF command is detected. */
    Ifx1edi2010as_PwmCommandStatus_on = 1,            /**< \brief PWM ON command is detected. */
}Ifx1edi2010as_PwmCommandStatus;

/** \brief Debug Mode Active Flag
 * Bitfield(s): Ifx1edi2010as_SSTAT.DBG
 */
typedef enum
{
    Ifx1edi2010as_DebugModeActiveFlag_notSet = 0,     /**< \brief Debug Mode is not active. */
    Ifx1edi2010as_DebugModeActiveFlag_set = 1,        /**< \brief Debug Mode is active. */
}Ifx1edi2010as_DebugModeActiveFlag;

/** \brief DESAT Comparator Result
 * Bitfield(s): Ifx1edi2010as_SSTAT2.DSATC
 */
typedef enum
{
    Ifx1edi2010as_DesatComparatorResult_belowThreshold = 0,/**< \brief  */
    Ifx1edi2010as_DesatComparatorResult_aboveThreshold = 1,/**< \brief  */
}Ifx1edi2010as_DesatComparatorResult;

/** \brief OCP Comparator Result
 * Bitfield(s): Ifx1edi2010as_SSTAT2.OCPC
 */
typedef enum
{
    Ifx1edi2010as_OcpComparatorResult_belowThreshold = 0,/**< \brief  */
    Ifx1edi2010as_OcpComparatorResult_aboveThreshold = 1,/**< \brief  */
}Ifx1edi2010as_OcpComparatorResult;

/** \brief UVLO2 Event
 * Bitfield(s): Ifx1edi2010as_SSTAT2.UVLO2M
 */
typedef enum
{
    Ifx1edi2010as_Uvlo2Event_noError = 0,             /**< \brief No failure condition is detected. */
    Ifx1edi2010as_Uvlo2Event_error = 1,               /**< \brief One failure condition is detected. */
}Ifx1edi2010as_Uvlo2Event;

/** \brief DIO2 Pin Level
 * Bitfield(s): Ifx1edi2010as_SSTAT2.DIO2L
 */
typedef enum
{
    Ifx1edi2010as_Dio2PinLevel_low = 0,               /**< \brief  */
    Ifx1edi2010as_Dio2PinLevel_high = 1,              /**< \brief  */
}Ifx1edi2010as_Dio2PinLevel;

/** \brief DACLP Pin outpout level
 * Bitfield(s): Ifx1edi2010as_SSTAT2.DACL
 */
typedef enum
{
    Ifx1edi2010as_DaclpPinOutpoutLevel_low = 0,       /**< \brief  */
    Ifx1edi2010as_DaclpPinOutpoutLevel_high = 1,      /**< \brief  */
}Ifx1edi2010as_DaclpPinOutpoutLevel;

/** \brief Communication Error Secondary Flag
 * Bitfield(s): Ifx1edi2010as_SER.CERS
 */
typedef enum
{
    Ifx1edi2010as_CommunicationErrorSecondaryFlag_notSet = 0,/**< \brief No event has been detected. */
    Ifx1edi2010as_CommunicationErrorSecondaryFlag_set = 1,/**< \brief An event has been detected. */
}Ifx1edi2010as_CommunicationErrorSecondaryFlag;

/** \brief ADC Undervoltage Error Flag
 * Bitfield(s): Ifx1edi2010as_SER.AUVER
 */
typedef enum
{
    Ifx1edi2010as_AdcUndervoltageErrorFlag_notSet = 0,/**< \brief An error condition has not been detected. */
    Ifx1edi2010as_AdcUndervoltageErrorFlag_set = 1,   /**< \brief An error condition has been detected. */
}Ifx1edi2010as_AdcUndervoltageErrorFlag;

/** \brief ADC Overvoltage Error Flag
 * Bitfield(s): Ifx1edi2010as_SER.AOVER
 */
typedef enum
{
    Ifx1edi2010as_AdcOvervoltageErrorFlag_notSet = 0, /**< \brief An error condition has not been detected. */
    Ifx1edi2010as_AdcOvervoltageErrorFlag_set = 1,    /**< \brief An error condition has been detected. */
}Ifx1edi2010as_AdcOvervoltageErrorFlag;

/** \brief Verification Mode Time-Out Event Flag
 * Bitfield(s): Ifx1edi2010as_SER.VMTO
 */
typedef enum
{
    Ifx1edi2010as_VerificationModeTimeOutEventFlag_notSet = 0,/**< \brief No event has been detected. */
    Ifx1edi2010as_VerificationModeTimeOutEventFlag_set = 1,/**< \brief An event has been detected. */
}Ifx1edi2010as_VerificationModeTimeOutEventFlag;

/** \brief UVLO2 Error Flag
 * Bitfield(s): Ifx1edi2010as_SER.UVLO2ER
 */
typedef enum
{
    Ifx1edi2010as_Uvlo2ErrorFlag_notSet = 0,          /**< \brief No event has been detected. */
    Ifx1edi2010as_Uvlo2ErrorFlag_set = 1,             /**< \brief An event has been detected. */
}Ifx1edi2010as_Uvlo2ErrorFlag;

/** \brief DESAT Error Flag
 * Bitfield(s): Ifx1edi2010as_SER.DESATER
 */
typedef enum
{
    Ifx1edi2010as_DesatErrorFlag_notSet = 0,          /**< \brief No event has been detected. */
    Ifx1edi2010as_DesatErrorFlag_set = 1,             /**< \brief An event has been detected. */
}Ifx1edi2010as_DesatErrorFlag;

/** \brief OCP Error Flag
 * Bitfield(s): Ifx1edi2010as_SER.OCPER
 */
typedef enum
{
    Ifx1edi2010as_OcpErrorFlag_notSet = 0,            /**< \brief No event has been detected. */
    Ifx1edi2010as_OcpErrorFlag_set = 1,               /**< \brief An event has been detected. */
}Ifx1edi2010as_OcpErrorFlag;

/** \brief Secondary Hard Reset Flag
 * Bitfield(s): Ifx1edi2010as_SER.RSTS
 */
typedef enum
{
    Ifx1edi2010as_SecondaryHardResetFlag_notSet = 0,  /**< \brief No hard reset event has been detected. */
    Ifx1edi2010as_SecondaryHardResetFlag_set = 1,     /**< \brief A hard reset event has been detected. */
}Ifx1edi2010as_SecondaryHardResetFlag;

/** \brief VBE Compensation Enable
 * Bitfield(s): Ifx1edi2010as_SCFG.VBEC
 */
typedef enum
{
    Ifx1edi2010as_VbeCompensationEnable_disabled = 0, /**< \brief V */
    Ifx1edi2010as_VbeCompensationEnable_enabled = 1,  /**< \brief V */
}Ifx1edi2010as_VbeCompensationEnable;

/** \brief Secondary Advanced Configuration Enable
 * Bitfield(s): Ifx1edi2010as_SCFG.CFG2
 */
typedef enum
{
    Ifx1edi2010as_SecondaryAdvancedConfigurationEnable_disabled = 0,/**< \brief Write access to */
    Ifx1edi2010as_SecondaryAdvancedConfigurationEnable_enabled = 1,/**< \brief Write access to */
}Ifx1edi2010as_SecondaryAdvancedConfigurationEnable;

/** \brief DIO2 Pin Mode
 * Bitfield(s): Ifx1edi2010as_SCFG.DIO2C
 */
typedef enum
{
    Ifx1edi2010as_Dio2PinMode_input = 0,              /**< \brief  */
    Ifx1edi2010as_Dio2PinMode_output = 1,             /**< \brief  */
}Ifx1edi2010as_Dio2PinMode;

/** \brief DESAT Clamping Enable
 * Bitfield(s): Ifx1edi2010as_SCFG.DSTCEN
 */
typedef enum
{
    Ifx1edi2010as_DesatClampingEnable_disabled = 0,   /**< \brief DESAT clamping is disabled. */
    Ifx1edi2010as_DesatClampingEnable_enabled = 1,    /**< \brief DESAT clamping is enabled. */
}Ifx1edi2010as_DesatClampingEnable;

/** \brief Pulse Suppressor Enable
 * Bitfield(s): Ifx1edi2010as_SCFG.PSEN
 */
typedef enum
{
    Ifx1edi2010as_PulseSuppressorEnable_disabled = 0, /**< \brief Pulse suppressor is disabled. */
    Ifx1edi2010as_PulseSuppressorEnable_enabled = 1,  /**< \brief Pulse suppressor is enabled. */
}Ifx1edi2010as_PulseSuppressorEnable;

/** \brief Verification Mode Time Out Duration
 * Bitfield(s): Ifx1edi2010as_SCFG.TOSEN
 */
typedef enum
{
    Ifx1edi2010as_VerificationModeTimeOutDuration_regular = 0,/**< \brief Regular time-out value (typ. 15 ms). */
    Ifx1edi2010as_VerificationModeTimeOutDuration_slow = 1,/**< \brief Slow time-out value (typ. 60 ms). */
}Ifx1edi2010as_VerificationModeTimeOutDuration;

/** \brief DESAT Threshold Level
 * Bitfield(s): Ifx1edi2010as_SCFG.DSATLS
 */
typedef enum
{
    Ifx1edi2010as_DesatThresholdLevel_vdesat0 = 0,    /**< \brief Threshold V */
    Ifx1edi2010as_DesatThresholdLevel_vdesat1 = 1,    /**< \brief Threshold V */
}Ifx1edi2010as_DesatThresholdLevel;

/** \brief UVLO2 Threshold Level
 * Bitfield(s): Ifx1edi2010as_SCFG.UVLO2S
 */
typedef enum
{
    Ifx1edi2010as_Uvlo2ThresholdLevel_uvlo2l0 = 0,    /**< \brief Threshold V */
    Ifx1edi2010as_Uvlo2ThresholdLevel_uvlo2l1 = 1,    /**< \brief Threshold V */
}Ifx1edi2010as_Uvlo2ThresholdLevel;

/** \brief OCP Threshold Level
 * Bitfield(s): Ifx1edi2010as_SCFG.OCPLS
 */
typedef enum
{
    Ifx1edi2010as_OcpThresholdLevel_vocp0 = 0,        /**< \brief Threshold V */
    Ifx1edi2010as_OcpThresholdLevel_vocp1 = 1,        /**< \brief Threshold V */
}Ifx1edi2010as_OcpThresholdLevel;

/** \brief DACLP Pin clamping outpout
 * Bitfield(s): Ifx1edi2010as_SCFG.DACLC
 */
typedef enum
{
    Ifx1edi2010as_DaclpPinClampingOutpout_low = 0,    /**< \brief The pin delivers a constant Low level. */
    Ifx1edi2010as_DaclpPinClampingOutpout_high = 1,   /**< \brief The pin delivers a constant High level. */
    Ifx1edi2010as_DaclpPinClampingOutpout_daclpSafe = 2,/**< \brief : */
    Ifx1edi2010as_DaclpPinClampingOutpout_daclpRegular = 3,/**< \brief : */
}Ifx1edi2010as_DaclpPinClampingOutpout;

/** \brief ADC PWM Trigger Delay
 * Bitfield(s): Ifx1edi2010as_SCFG2.PWMD
 */
typedef enum
{
    Ifx1edi2010as_AdcPwmTriggerDelay_delay0 = 0,      /**< \brief 16 OSC2 cycles selected. */
    Ifx1edi2010as_AdcPwmTriggerDelay_delay1 = 1,      /**< \brief 32 OSC2 cycles selected. */
    Ifx1edi2010as_AdcPwmTriggerDelay_delay2 = 2,      /**< \brief 48 OSC2 cycles selected. */
    Ifx1edi2010as_AdcPwmTriggerDelay_delay3 = 3,      /**< \brief 64 OSC2 cycles selected. */
}Ifx1edi2010as_AdcPwmTriggerDelay;

/** \brief ADC Secondary Trigger Mode
 * Bitfield(s): Ifx1edi2010as_SCFG2.ATS
 */
typedef enum
{
    Ifx1edi2010as_AdcSecondaryTriggerMode_disabled = 0,/**< \brief No secondary trigger source active. */
    Ifx1edi2010as_AdcSecondaryTriggerMode_periodic = 1,/**< \brief Periodic trigger selected. */
    Ifx1edi2010as_AdcSecondaryTriggerMode_risingPwm = 2,/**< \brief PWM trigger selected (rising edge). */
    Ifx1edi2010as_AdcSecondaryTriggerMode_fallingPwm = 3,/**< \brief PWM trigger selected (falling edge). */
}Ifx1edi2010as_AdcSecondaryTriggerMode;

/** \brief ADC Gain
 * Bitfield(s): Ifx1edi2010as_SCFG2.AGS
 */
typedef enum
{
    Ifx1edi2010as_AdcGain_gain0 = 0,                  /**< \brief GAIN */
    Ifx1edi2010as_AdcGain_gain1 = 1,                  /**< \brief GAIN */
    Ifx1edi2010as_AdcGain_gain2 = 2,                  /**< \brief GAIN */
    Ifx1edi2010as_AdcGain_gain3 = 3,                  /**< \brief GAIN */
}Ifx1edi2010as_AdcGain;

/** \brief ADC Offset
 * Bitfield(s): Ifx1edi2010as_SCFG2.AOS
 */
typedef enum
{
    Ifx1edi2010as_AdcOffset_ofst0 = 0,                /**< \brief V */
    Ifx1edi2010as_AdcOffset_ofst1 = 1,                /**< \brief V */
    Ifx1edi2010as_AdcOffset_ofst2 = 2,                /**< \brief V */
    Ifx1edi2010as_AdcOffset_ofst3 = 3,                /**< \brief V */
    Ifx1edi2010as_AdcOffset_ofst4 = 4,                /**< \brief V */
    Ifx1edi2010as_AdcOffset_ofst5 = 5,                /**< \brief V */
    Ifx1edi2010as_AdcOffset_ofst6 = 6,                /**< \brief V */
}Ifx1edi2010as_AdcOffset;

/** \brief ADC Current Source
 * Bitfield(s): Ifx1edi2010as_SCFG2.ACSS
 */
typedef enum
{
    Ifx1edi2010as_AdcCurrentSource_disabled = 0,      /**< \brief Current source disabled. */
    Ifx1edi2010as_AdcCurrentSource_enabled = 1,       /**< \brief Current source I */
}Ifx1edi2010as_AdcCurrentSource;

/** \brief ADC Event Class A Enable
 * Bitfield(s): Ifx1edi2010as_SCFG2.ACAEN
 */
typedef enum
{
    Ifx1edi2010as_AdcEventClassAEnable_disabled = 0,  /**< \brief No Event Class A is generated. */
    Ifx1edi2010as_AdcEventClassAEnable_enabled = 1,   /**< \brief An Event Class A is generated. */
}Ifx1edi2010as_AdcEventClassAEnable;

/** \brief ADC Enable
 * Bitfield(s): Ifx1edi2010as_SCFG2.ADCEN
 */
typedef enum
{
    Ifx1edi2010as_AdcEnable_disabled = 0,             /**< \brief ADC Disabled. */
    Ifx1edi2010as_AdcEnable_enabled = 1,              /**< \brief ADC Enabled. */
}Ifx1edi2010as_AdcEnable;

/** \brief Secondary Verification Function
 * Bitfield(s): Ifx1edi2010as_SSCR.VFS2
 */
typedef enum
{
    Ifx1edi2010as_SecondaryVerificationFunction_disabled = 0,/**< \brief No function activated. */
    Ifx1edi2010as_SecondaryVerificationFunction_tcf = 1,/**< \brief TCF function active. */
}Ifx1edi2010as_SecondaryVerificationFunction;

/** \brief ADC Result Valid Flag
 * Bitfield(s): Ifx1edi2010as_SADC.AVFS
 */
typedef enum
{
    Ifx1edi2010as_AdcResultValidFlag_notValid = 0,    /**< \brief ADC result is not valid. */
    Ifx1edi2010as_AdcResultValidFlag_valid = 1,       /**< \brief ADC result is valid. */
}Ifx1edi2010as_AdcResultValidFlag;

/** \brief ADC Undervoltage Error Status
 * Bitfield(s): Ifx1edi2010as_SADC.AUVS
 */
typedef enum
{
    Ifx1edi2010as_AdcUndervoltageErrorStatus_noError = 0,/**< \brief An error condition is not detected. */
    Ifx1edi2010as_AdcUndervoltageErrorStatus_error = 1,/**< \brief An error condition is being detected */
}Ifx1edi2010as_AdcUndervoltageErrorStatus;

/** \brief ADC Overvoltage Error Status
 * Bitfield(s): Ifx1edi2010as_SADC.AOVS
 */
typedef enum
{
    Ifx1edi2010as_AdcOvervoltageErrorStatus_noError = 0,/**< \brief An error condition is not detected. */
    Ifx1edi2010as_AdcOvervoltageErrorStatus_error = 1,/**< \brief An error condition is being detected */
}Ifx1edi2010as_AdcOvervoltageErrorStatus;

/** \}  */
#endif /* IFX1EDI2010AS_ENUM_H */
