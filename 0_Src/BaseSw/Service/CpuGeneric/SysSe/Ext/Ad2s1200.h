/**
 * \file Ad2s1200.h
 * \brief Ad2s1200 resolver interface.
 * \ingroup library_srvsw_sysse_ext_ad2s1200
 *
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_ext_ad2s1200 AD2S1200 resolver interface
 * Serial configuration:
 *	- spiChannelConfig.base.mode.autoCS         = 1;
 *	- spiChannelConfig.base.mode.dataWidth      = 16;
 *	- spiChannelConfig.base.mode.csActiveLevel  = Ifx_ActiveState_low;
 *
 * \ingroup library_srvsw_sysse_ext
 *
 */
#ifndef AD2S1200_H
#define AD2S1200_H

//________________________________________________________________________________________
// INCLUDES

#include "If/SpiIf.h"
#include "StdIf/IfxStdIf_Pos.h"
#include "SysSe/Bsp/Bsp.h"
#include "SysSe/Math/Ifx_LowPassPt1F32.h"

//________________________________________________________________________________________
// CONFIGURATION MACROS
//#define AD2S1200_DEBUG (1)

#ifndef AD2S1200_EXT_BUS_AUTO_SELECT
/** \brief Define whether configuration select signal is set directly through EBU: 1=yes, 0=no */
#define AD2S1200_EXT_BUS_AUTO_SELECT (0)
#endif

#ifndef AD2S1200_RD_TO_DATA_WAIT
/** \brief Specifies the wait cycles inserted between the RD\\and the Data read.
 * Series of __nop() should be defined */
#define AD2S1200_RD_TO_DATA_WAIT     wait(TimeConst_10ns)
#endif

//________________________________________________________________________________________
// HELPER MACROS

/** \brief full scale speed in rad/s */
#define AD2S1200_SPEED_FULL_SCALE_12BITS (1000.0 * 2 * IFX_PI)

//________________________________________________________________________________________
// ENUMERATIONS

/** \brief Resolver state flags
 */
typedef enum
{
    Ad2s1200_State_ready,           /**< \brief The resolver is ready for use */
    Ad2s1200_State_startUp,         /**< \brief The resolver is starting up */
    Ad2s1200_State_reset            /**< \brief The resolver is being reset */
} Ad2s1200_State;

/** \brief Interfacing modes */
typedef enum
{
    Ad2s1200_Mode_serial,
    Ad2s1200_Mode_parallel,
    Ad2s1200_Mode_extbus
} Ad2s1200_Mode;

/** \brief Chip operation modes */
typedef enum
{
    Ad2s1200_OperationMode_normalPosition = 0,
    Ad2s1200_OperationMode_normalVelocity = 2
} Ad2s1200_OperationMode;

/** \brief Supported operations */
typedef enum
{
    Ad2s1200_Resolution_12Bits = 0
} Ad2s1200_Resolution;

/** \brief Resolver status
 */
typedef enum
{
    Ad2s1200_Status_signalLoss        = 0,      /**< \brief Loss of signal. In case of "loss of signal" the resolver should be reset */
    Ad2s1200_Status_signalDegradation = 1,      /**< \brief Degradation of signal */
    Ad2s1200_Status_trackingLoss      = 2,      /**< \brief Loss of tracking */
    Ad2s1200_Status_noError           = 3       /**< \brief No error */
} Ad2s1200_Status;

//________________________________________________________________________________________
// DATA STRUCTURES

/** \brief Common structure */
typedef struct
{
    IfxPort_Pin RDVEL;      /**< \brief RDVEL pin */
    IfxPort_Pin RESET;      /**< \brief Reset pin */
    IfxPort_Pin FS1;        /**< \brief FS1 pin */
    IfxPort_Pin FS2;        /**< \brief FS2 pin */
} Ad2s1200_Common;

/** \brief Serial interface runtime structure */
typedef struct
{
    SpiIf_Ch   *channel;
    IfxPort_Pin CSn;        /**< \brief CS\\pin */
    IfxPort_Pin RDn;        /**< \brief RDn pin */
} Ad2s1200_Serial;

/** \brief Parallel port interface runtime structure */
typedef struct
{
    IfxPort_Pin CSn;        /**< \brief CS\\pin */
    IfxPort_Pin RDn;        /**< \brief RDn pin */
    IfxPort_Pin DB0;        /**< \brief DB0 pin. DB0 to DB15 must be adjacent pins, with DB0 having the lowest pin index */
    IfxPort_Pin LOT;        /**< \brief LOT pin */
    IfxPort_Pin DOS;        /**< \brief DOS pin */
} Ad2s1200_Par;

/** \brief External bus interface runtime structure */
typedef struct
{
    void       *data;       /**< \brief Resolver data register address for position (and speed when AD2S1200_EXT_BUS_AUTO_SELECT == 1)*/
    void       *dataVel;    /**< \brief Resolver data register address for speed (optional, only when AD2S1200_EXT_BUS_AUTO_SELECT == 0) */
    IfxPort_Pin LOT;        /**< \brief LOT pin */
    IfxPort_Pin DOS;        /**< \brief DOS pin */
} Ad2s1200_Ebu;

/** \brief Position sensor diagnostic/statistic */
typedef union
{
    uint32 byIndex[5];
    struct
    {
        uint32 signalLoss;
        uint32 signalDegradation;
        uint32 trackingLoss;
        uint32 noError;
        uint32 commError;
    } byType;
} Ad2s1200_Diag;

/** Mode specific run-time data */
typedef struct
{
    Ad2s1200_Mode   selected;   /**< \brief Resolver chip selected mode*/
    Ad2s1200_Common common;
    union
    {
        Ad2s1200_Serial serial;
        /* TODO add configuration SSC channel */
        Ad2s1200_Par    parallel;
        Ad2s1200_Ebu    extbus;
    } i;
} Ad2s1200_ModeRT;

/** \brief Alias to resolver object definition */
typedef struct Ad2s1200_s Ad2s1200;

/** \brief Resolver object definition */
struct Ad2s1200_s
{
    /* status */
    sint32              position;            /**< \brief raw position in ticks. \note: the value already contains the offset */
    float32             speed;               /**< \brief mechanical speed in rad/s */
    sint32              turn;                /**< \brief number of mechanical turns */
    IfxStdIf_Pos_Dir    direction;           /**< \brief rotation direction */
    IfxStdIf_Pos_Status stdifStatus;         /**< \brief error code (0 = no error) */
#if AD2S1200_DEBUG
    Ad2s1200_Diag       diag;                /**< \brief diagnostic and statistic */
#endif
    /* configuration */
    sint32             offset;               /**< \brief raw position offset */
    boolean            reversed;             /**< \brief reverse direction */
    uint16             periodPerRotation;    /**< \brief 'electrical' periods per mechanical rotation */
    sint32             resolution;           /**< \brief resolution of this position sensor interface */
    float32            speedConstPulseCount; /**< \brief constant for calculating mechanical speed (in rad/s) from raw speed */

    sint16             speedCounter;         /**< \brief Temporary counter for internal speed calculation */
    sint16             speedReload;          /**< \brief Refresh factor for the speed. each call to Ad2s1200_update() increase the counter SpeedCounter */
    Ad2s1200_State     state;                /**< \brief Resolver State */
    Ad2s1200_ModeRT    modes;                /**< \brief */
    Ad2s1200_Status    status;               /**< \brief internal status */
    uint8              dataBits;             /**< \brief number of data bits */
    Ifx_LowPassPt1F32 speedLpf;             /**< \brief PT1 low-pass filter of this input */
    boolean            speedFilterEnabled;   /**< \brief Specifies whether the filter is enabled */
};

/** \brief Mode configuration */
typedef struct
{
    Ad2s1200_Mode       selected;      /**< \brief Resolver selected mode*/
    Ad2s1200_Common     common;
    const Ad2s1200_Par *parallel;
    const Ad2s1200_Ebu *extbus;
    Ad2s1200_Serial     serial; /**< \brief Driver dependent channel structure. E.g. if IfxQspi_Spi, then put IfxQspi_Spi_Ch* as value to this field */
} Ad2s1200_ModeConfig;

/** \brief Resolver configuration definition */
typedef struct
{
    sint32              offset;                    /**< \brief Specified the resolver offset. */
    uint16              periodPerRotation;         /**< \brief Number of period per mechanical rotation */
    boolean             reversed;                  /**< \brief if TRUE, the direction is reversed */
    sint32              frequency;                 /**< \brief initial resolver excitation frequency */
    sint32              updateFrequency;           /**< \brief the update frequency */
    uint16              speedUpdateFactor;         /**< \brief Update factor for the speed relative to the position update  */
    Ad2s1200_ModeConfig modes;
    float32             speedFilerCutOffFrequency; /**< \brief speed cut off frequency in Hz. a value of <= 0 disable the filter;*/
} Ad2s1200_Config;

//________________________________________________________________________________________
// FUNCTION PROTOTYPES

/** \addtogroup library_srvsw_sysse_ext_ad2s1200
 * \{ */

/** \name Initialisation functions
 * Initialisation shall be done by calling Ad2s1200_init(), e.g.:
 * \code
 * extern Ad2s1200 driverData;
 * extern const Ad2s1200_Config driverConfig;
 * Ad2s1200_init(&driverData, &driverConfig);
 * Ad2s1200_reset(&driverData);
 * \endcode
 *
 * Application (e.g. running inside motor PWM interrupt context) should use \ref library_srvsw_stdif_posif
 * functions for updating and accessing the actual results, e.g:
 * \code
 * extern Ad2s1200 driverData;
 * IfxStdIf_Pos* handle = &driverData.base;
 * PosIf_update(handle);
 * elAngle = PosIf_getElAngle(handle);
 * elSpeed = PosIf_getElSpeed(handle);
 * \endcode
 * Prototypes:
 * \{ */
IFX_EXTERN boolean        Ad2s1200_init(Ad2s1200 *driver, const Ad2s1200_Config *config);
void                      Ad2s1200_initConfig(Ad2s1200_Config *config);
IFX_EXTERN Ad2s1200_State Ad2s1200_enable(Ad2s1200 *driver, WaitTimeFunction waitFunction);
IFX_EXTERN void           Ad2s1200_reset(Ad2s1200 *driver);
IFX_EXTERN void           Ad2s1200_resetFaults(Ad2s1200 *driver);
IFX_EXTERN sint32         Ad2s1200_setFrequency(Ad2s1200 *driver, sint32 frequency);
boolean                   Ad2s1200_stdIfPosInit(IfxStdIf_Pos *stdif, Ad2s1200 *driver);

float32                 Ad2s1200_getAbsolutePosition(Ad2s1200 *driver);
sint32                  Ad2s1200_getOffset(Ad2s1200 *driver);
float32                 Ad2s1200_getRefreshPeriod(Ad2s1200 *driver);
IfxStdIf_Pos_SensorType Ad2s1200_getSensorType(Ad2s1200 *driver);
float32                 Ad2s1200_getSpeed(Ad2s1200 *driver);
void                    Ad2s1200_setRefreshPeriod(Ad2s1200 *driver, float32 updatePeriod);

/** \} */
/** \name Status functions
 * Additional functions not covered by \ref library_srvsw_stdif_posif are provided.
 *
 * Prototypes:
 * \{ */
IFX_INLINE boolean Ad2s1200_isLossOfSignal(Ad2s1200 *driver);
IFX_INLINE boolean Ad2s1200_isDegradationOfSignal(Ad2s1200 *driver);
IFX_INLINE boolean Ad2s1200_isLossOfTracking(Ad2s1200 *driver);
IFX_INLINE sint32  Ad2s1200_getRawPosition(Ad2s1200 *driver);
IFX_INLINE float32 Ad2s1200_getPosition(Ad2s1200 *driver);
IFX_INLINE sint32  Ad2s1200_getTurn(Ad2s1200 *driver);
IFX_INLINE boolean Ad2s1200_reverse(Ad2s1200 *driver, boolean reverse);

/** \brief \see IfxStdIf_Pos_GetDirection
 * \param driver driver handle
 * \return direction
 */
IFX_EXTERN IfxStdIf_Pos_Dir Ad2s1200_getDirection(Ad2s1200 *driver);

/** \brief \see IfxStdIf_Pos_GetFault
 * \param driver driver handle
 * \return Fault
 */
IFX_EXTERN IfxStdIf_Pos_Status Ad2s1200_getFault(Ad2s1200 *driver);

IFX_EXTERN sint32 Ad2s1200_getResolution(Ad2s1200 *driver);
IFX_EXTERN uint16 Ad2s1200_readPosition(Ad2s1200 *driver);
IFX_EXTERN sint16 Ad2s1200_readVelocity(Ad2s1200 *driver);
/** \} */

/** \} */

//________________________________________________________________________________________
// INLINE FUNCTION IMPLEMENTATIONS

/** \brief Return the resolver position is in Loss of Signal (LOS).
 * \param driver Specifies resolver object.
 * \return if TRUE, the resolver is in Loss of Signal (LOS)..
 */
IFX_INLINE boolean Ad2s1200_isLossOfSignal(Ad2s1200 *driver)
{
    return (driver->stdifStatus.B.signalLoss != 0) ? TRUE : FALSE;
}


/** \brief Return the resolver position is in Degradation of Signal (DOS).
 * \param driver Specifies resolver object. *
 * \return if TRUE, the resolver is in Degradation of Signal (DOS).
 */
IFX_INLINE boolean Ad2s1200_isDegradationOfSignal(Ad2s1200 *driver)
{
    return (driver->stdifStatus.B.signalDegradation != 0) ? TRUE : FALSE;
}


/** \brief Return the resolver position is  in loss of tracking (LOT)
 * \param driver Specifies resolver object.
 * \return if TRUE, the resolver is in loss of tracking (LOT).
 */
IFX_INLINE boolean Ad2s1200_isLossOfTracking(Ad2s1200 *driver)
{
    return (driver->stdifStatus.B.trackingLoss != 0) ? TRUE : FALSE;
}


/** \brief Return the resolver position in resolver ticks
 *
 * This function returns the resolver position in resolver ticks.
 * In order to get the actual position, the function Ad2s1200_update() must have been
 * called just before.
 *
 * \param driver Specifies resolver object.
 *
 * \return returns the resolver position in resolver ticks.
 */
IFX_INLINE sint32 Ad2s1200_getRawPosition(Ad2s1200 *driver)
{
    return driver->position;
}


/** \brief \see IfxStdIf_Pos_GetPeriodPerRotation
 * \param driver driver handle
 * \return Period per rotation
 */
IFX_EXTERN uint16 Ad2s1200_getPeriodPerRotation(Ad2s1200 *driver);

/** \brief Return the resolver position in rad
 *
 * This function returns the resolver position in rad.
 * In order to get the actual position, the function Ad2s1200_update() must have been
 * called just before.
 *
 * \param driver Specifies resolver object.
 * \return returns the resolver position in rad.
 */
IFX_INLINE float32 Ad2s1200_getPosition(Ad2s1200 *driver)
{
    return (float32)(driver->position * ((2 * IFX_PI) / driver->resolution));
}


/** \brief Return the resolver sensor turn count
 * \param driver Specifies resolver object.
 * \return returns the resolver sensor turn count.
 * \todo Implements Turn feature
 */
IFX_INLINE sint32 Ad2s1200_getTurn(Ad2s1200 *driver)
{
    return driver->turn;
}


/** \brief Set the resolver positive direction
 * \param driver Specifies resolver object.
 * \param reversed Specifies if the resolver direction should be reversed (TRUE) or not (FALSE).
 * \return None.
 */
IFX_INLINE boolean Ad2s1200_reverse(Ad2s1200 *driver, boolean reversed)
{
    driver->reversed = reversed;
    return reversed;
}


//------------------------------------------------------------------------------
#endif
