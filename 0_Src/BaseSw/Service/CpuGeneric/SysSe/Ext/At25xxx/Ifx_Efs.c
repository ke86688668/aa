/**
 * \file Ifx_Efs.c
 * \brief EEPROM file system.
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

//---------------------------------------------------------------------------
#include <string.h>
#include <stddef.h>
#include "Ifx_Efs.h"
#include "_Utilities/Ifx_Assert.h"
#include "SysSe/Math/Ifx_Crc.h"
//---------------------------------------------------------------------------
static boolean         Ifx_Efs_fileLoad(Ifx_Efs *efs, Ifx_Efs_File *file, Ifx_Efs_FileIndex fileIndex);
static Ifx_Efs_Address Ifx_Efs_fileOffsetItemAddressGet(Ifx_Efs *efs, Ifx_Efs_FileIndex fileIndex);

const Ifc_Crc_Table16 Ifx_g_Efs_crcTable = {
    .data.order      = 16,
    .data.polynom    = 0xA001,
    .data.refin      = 1,
    .data.crchighbit = 32768,
    .data.crcmask    = 0xFFFF,
    .crctab          = {
        0x0000, 0x9705, 0x2E01, 0xB904, 0x5C02, 0xCB07, 0x7203, 0xE506, 0xB804, 0x2F01, 0x9605, 0x0100, 0xE406, 0x7303, 0xCA07, 0x5D02,
        0x7003, 0xE706, 0x5E02, 0xC907, 0x2C01, 0xBB04, 0x0200, 0x9505, 0xC807, 0x5F02, 0xE606, 0x7103, 0x9405, 0x0300, 0xBA04, 0x2D01,
        0xE006, 0x7703, 0xCE07, 0x5902, 0xBC04, 0x2B01, 0x9205, 0x0500, 0x5802, 0xCF07, 0x7603, 0xE106, 0x0400, 0x9305, 0x2A01, 0xBD04,
        0x9005, 0x0700, 0xBE04, 0x2901, 0xCC07, 0x5B02, 0xE206, 0x7503, 0x2801, 0xBF04, 0x0600, 0x9105, 0x7403, 0xE306, 0x5A02, 0xCD07,
        0xC007, 0x5702, 0xEE06, 0x7903, 0x9C05, 0x0B00, 0xB204, 0x2501, 0x7803, 0xEF06, 0x5602, 0xC107, 0x2401, 0xB304, 0x0A00, 0x9D05,
        0xB004, 0x2701, 0x9E05, 0x0900, 0xEC06, 0x7B03, 0xC207, 0x5502, 0x0800, 0x9F05, 0x2601, 0xB104, 0x5402, 0xC307, 0x7A03, 0xED06,
        0x2001, 0xB704, 0x0E00, 0x9905, 0x7C03, 0xEB06, 0x5202, 0xC507, 0x9805, 0x0F00, 0xB604, 0x2101, 0xC407, 0x5302, 0xEA06, 0x7D03,
        0x5002, 0xC707, 0x7E03, 0xE906, 0x0C00, 0x9B05, 0x2201, 0xB504, 0xE806, 0x7F03, 0xC607, 0x5102, 0xB404, 0x2301, 0x9A05, 0x0D00,
        0x8005, 0x1700, 0xAE04, 0x3901, 0xDC07, 0x4B02, 0xF206, 0x6503, 0x3801, 0xAF04, 0x1600, 0x8105, 0x6403, 0xF306, 0x4A02, 0xDD07,
        0xF006, 0x6703, 0xDE07, 0x4902, 0xAC04, 0x3B01, 0x8205, 0x1500, 0x4802, 0xDF07, 0x6603, 0xF106, 0x1400, 0x8305, 0x3A01, 0xAD04,
        0x6003, 0xF706, 0x4E02, 0xD907, 0x3C01, 0xAB04, 0x1200, 0x8505, 0xD807, 0x4F02, 0xF606, 0x6103, 0x8405, 0x1300, 0xAA04, 0x3D01,
        0x1000, 0x8705, 0x3E01, 0xA904, 0x4C02, 0xDB07, 0x6203, 0xF506, 0xA804, 0x3F01, 0x8605, 0x1100, 0xF406, 0x6303, 0xDA07, 0x4D02,
        0x4002, 0xD707, 0x6E03, 0xF906, 0x1C00, 0x8B05, 0x3201, 0xA504, 0xF806, 0x6F03, 0xD607, 0x4102, 0xA404, 0x3301, 0x8A05, 0x1D00,
        0x3001, 0xA704, 0x1E00, 0x8905, 0x6C03, 0xFB06, 0x4202, 0xD507, 0x8805, 0x1F00, 0xA604, 0x3101, 0xD407, 0x4302, 0xFA06, 0x6D03,
        0xA004, 0x3701, 0x8E05, 0x1900, 0xFC06, 0x6B03, 0xD207, 0x4502, 0x1800, 0x8F05, 0x3601, 0xA104, 0x4402, 0xD307, 0x6A03, 0xFD06,
        0xD007, 0x4702, 0xFE06, 0x6903, 0x8C05, 0x1B00, 0xA204, 0x3501, 0x6803, 0xFF06, 0x4602, 0xD107, 0x3401, 0xA304, 0x1A00, 0x8D05,
    }
};

/**
 * String identifying the eeproim file system.
 * The value shall not be changed in future version of the driver
 */
#define IFX_EFS_ID                       ("EepromFileSystem");
/** EFS version
 * format x.y with x the upper 8 bits and y the lower 8 bits
 */
#define IFX_EFS_VERSION                  (0x0101)

#define IFX_CFG_EFS_DEFAULT_FILENAME_ZES (0)

/** Offset of the Ifx_Efs_Table object
 */
#define IFX_CFG_EFS_DEFAULT_TABLE_OFFSET (0)

/** Dynamic file memory allocation support (not implemented)
 */
#define IFX_EFS_DYNAMIC_SIZE_SUPPORT     (0)

const uint8 efs_id[IFX_EFS_ID_SIZE] = IFX_EFS_ID;

static void Ifx_Efs_errorFlagClear(Ifx_Efs *efs)
{
    efs->error.U = 0;
}


/** Write data to the eeprom
 *
 * \param efs Ifx_At25xxx file system object, initialised by Ifx_Efs_init()
 * \param address Address in the eeprom
 * \param data data to be written
 * \param length length of the data to be written in byte
 *
 * \return Returns TRUE in case of success, else FALSE. In case of error, see the efs.error flags
 */
static boolean Ifx_Efs_eepromWrite(Ifx_Efs *efs, Ifx_At25xxx_Address address, const uint8 *data, Ifx_At25xxx_Address length)
{
    boolean result = TRUE;

    /* EEPROM driver only supports 16 bit address */
    if ((address < efs->tableOffset) || (address >= efs->tableOffset + efs->table.header.partitionSize))
    {
        efs->error.B.writeOutOfMemory = 1;
        result                        = FALSE;
    }
    else
    {
        efs->error.B.communication = (Ifx_At25xxx_write(efs->eeprom, address, data, length) == FALSE) ? 1 : 0;
        result                     = (efs->error.B.communication == 0);
    }

    return result;
}


/** Read data from the eeprom
 *
 * \param efs Ifx_At25xxx file system object, Initialised by Ifx_Efs_init()
 * \param address Address in the eeprom
 * \param data data buffer where the read data are copied
 * \param length length of the data to be read in byte
 *
 * \return Returns TRUE in case of success, else FALSE. In case of error, see the efs.error flags
 */
static boolean Ifx_Efs_eepromRead(Ifx_Efs *efs, Ifx_At25xxx_Address address, uint8 *data, Ifx_At25xxx_Address length)
{
    boolean result = TRUE;

    /* EEPROM driver only supports 16 bit address */
    if ((address < efs->tableOffset) || (address >= efs->tableOffset + efs->table.header.partitionSize))
    {
        efs->error.B.readOutOfMemory = 1;
        result                       = FALSE;
    }
    else
    {
        efs->error.B.communication = (Ifx_At25xxx_read(efs->eeprom, address, data, length) == 1) ? 0 : 1;
        result                     = efs->error.B.communication == 0;
    }

    return result;
}


/** Erase the eeprom content corresponding to the efs configuration
 *
 */
static boolean Ifx_Efs_eepromErase(Ifx_Efs *efs)
{
    /* EEPROM driver only supports 16 bit address */
    efs->error.B.communication = (Ifx_At25xxx_erase(efs->eeprom, efs->tableOffset, efs->table.header.partitionSize,0x00) == 1) ? 0 : 1;
    return efs->error.B.communication == 0;
}


/** Return the maximal data size that can be written to the file in byte
 *
 */
Ifx_Efs_FileSize Ifx_Efs_fileMaxDataSizeGet(Ifx_Efs_File *file)
{
    Ifx_Efs_FileSize size;

    if (file->efs->table.header.flags.dynamicFileSize == 1)
    {
        /* FIXME implement feature */
        size = 0;
    }
    else
    {
        size = file->header.allocatedFileSize - sizeof(file->header);
    }

    return size;
}


boolean Ifx_Efs_defaultFileSizeSet(Ifx_Efs *efs, Ifx_Efs_FileSize size)
{
    return Ifx_Efs_defaultAllocatedFileSizeSet(efs, size + sizeof(Ifx_Efs_FileHeader));
}


Ifx_Efs_FileSize Ifx_Efs_defaultFileSizeGet(Ifx_Efs *efs)
{
    Ifx_Efs_FileSize size;

    if (efs->table.header.flags.dynamicFileSize == 1)
    {
        /* FIXME implement feature */
        size = 0;
    }
    else
    {
        size = Ifx_Efs_defaultAllocatedFileSizeGet(efs) - sizeof(Ifx_Efs_FileHeader);
    }

    return size;
}


/** Initialize the Ifx_Efs object
 * The default is:
 * - partition size is the eeprom size
 * - file size is IFX_CFG_EFS_DEFAULT_ALLOCATED_FILE_SIZE
 * - EFS table offset is 0
 *
 */
void Ifx_Efs_Init(Ifx_Efs *efs, Ifx_At25xxx *eeprom)
{
    sint16 i;

    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, sizeof(efs_id) <= IFX_EFS_ID_SIZE);
    efs->eeprom = eeprom;

    //Ifx_Crc_createTable(&Ifx_g_Efs_crcTable.data, 16, 0xA001, 1);
    if (Ifx_Crc_init(&efs->crc, &Ifx_g_Efs_crcTable.data, 1, 1, 0xFFFF, 0xFFFF))
    {
        memcpy(&efs->table.header.fileTableId, efs_id, IFX_EFS_ID_SIZE);
    }

    efs->table.header.headerSize               = sizeof(efs->table.header);
    efs->table.header.crc                      = 0;
    efs->table.header.efsVersion               = IFX_EFS_VERSION;

    efs->table.header.fileOffsetItemsCrc       = 0;
    efs->table.header.defaultAllocatedFileSize = Ifx_AlignOn128(IFX_CFG_EFS_DEFAULT_ALLOCATED_FILE_SIZE);
    efs->table.header.flags.dynamicFileSize    = IFX_EFS_DYNAMIC_SIZE_SUPPORT;
    efs->table.header.flags.filenameZes        = IFX_CFG_EFS_DEFAULT_FILENAME_ZES;
    efs->tableOffset                           = IFX_CFG_EFS_DEFAULT_TABLE_OFFSET;
    efs->table.header.partitionSize            = (Ifx_Efs_PartitionSize)(eeprom->device->size - efs->tableOffset);

    if (Ifx_Efs_getMinPartitionSize(efs, 0) < efs->table.header.partitionSize)
    {
        if (Ifx_Efs_getMinPartitionSize(efs, IFX_CFG_EFS_MAX_FILE_COUNT) > efs->table.header.partitionSize)
        {
            efs->table.header.maxFileCount =
                (efs->table.header.partitionSize - sizeof(Ifx_Efs_TableHeader))
                / (sizeof(Ifx_Efs_FileOffsetItem) + (efs->table.header.flags.dynamicFileSize == 1 ? sizeof(Ifx_Efs_FileHeader) : efs->table.header.defaultAllocatedFileSize));

            if (efs->table.header.maxFileCount > IFX_CFG_EFS_MAX_FILE_COUNT)
            {
                efs->table.header.maxFileCount = IFX_CFG_EFS_MAX_FILE_COUNT;
            }
        }
        else
        {
            efs->table.header.maxFileCount = IFX_CFG_EFS_MAX_FILE_COUNT;
        }

        for (i = 0; i < IFX_CFG_EFS_MAX_FILE_COUNT; i++)
        {
            efs->table.fileOffsetItems[i] = IFX_EFS_FILE_OFFSET_ITEM_INVALID;
        }

        efs->dirIndex = -1;

        Ifx_Efs_errorFlagClear(efs);

        efs->status.loaded = 0;
        efs->status.ready  = 1;
    }
    else
    {
        efs->status.ready = 0;
    }
}


/** Set the EFS partition size
 *
 */
boolean Ifx_Efs_partitionSizeSet(Ifx_Efs *efs, Ifx_Efs_PartitionSize partitionSize)
{
    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.loaded == 1)
    {
        efs->error.B.invalidAction = 1;
    }
    else
    {
        efs->table.header.partitionSize = partitionSize;
    }

    return efs->table.header.partitionSize == partitionSize;
}


/** Return the partition size
 *
 */
Ifx_Efs_PartitionSize Ifx_Efs_partitionSizeGet(Ifx_Efs *efs)
{
    return efs->table.header.partitionSize;
}


boolean Ifx_Efs_maxFileCountSet(Ifx_Efs *efs, Ifx_Efs_FileIndex maxFileCount)
{
    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.loaded == 1)
    {
        efs->error.B.invalidAction = 1;
    }
    else
    {
        efs->table.header.maxFileCount = maxFileCount;
    }

    return efs->table.header.maxFileCount == maxFileCount;
}


Ifx_Efs_FileIndex Ifx_Efs_maxFileCountGet(Ifx_Efs *efs)
{
    return efs->table.header.maxFileCount;
}


boolean Ifx_Efs_tableOffsetSet(Ifx_Efs *efs, Ifx_Efs_Address offset)
{
    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.loaded == 1)
    {
        efs->error.B.invalidAction = 1;
    }
    else
    {
        efs->tableOffset = offset;
    }

    return offset == efs->tableOffset;
}


Ifx_Efs_Address Ifx_Efs_tableOffsetGet(Ifx_Efs *efs)
{
    return efs->tableOffset;
}


/** Return the file index of the corresponding filename
 * \param filename file name to be looked for. The complete filename data
 *                 is compared (IFX_CFG_EFS_FILENAME_SIZE bytes compared or up to
 *                 the zero char if efs->table.header.flags.filenameZes is set).
 */
static Ifx_Efs_FileIndex Ifx_Efs_fileIndexGet(Ifx_Efs *efs, pchar filename)
{
    boolean           found = FALSE;
    Ifx_Efs_FileIndex fileIndex;

    for (fileIndex = 0; fileIndex < efs->table.header.maxFileCount; fileIndex++)
    {
        Ifx_Efs_File file;
        boolean      exitLoop = FALSE;

        if (Ifx_Efs_fileLoad(efs, &file, fileIndex))
        {
            boolean match = TRUE;
            uint32  i     = 0;

            while (i < (sint8)sizeof(file.header.filename))
            {
                match &= file.header.filename[i] == filename[i];

                if (efs->table.header.flags.filenameZes == 1)
                {
                    if ((file.header.filename[i] == 0) && (filename[i] == 0))
                    {
                        break;
                    }
                }

                i++;
            }

            if (match)
            {
                found    = TRUE;
                exitLoop = TRUE;
            }
            else
            {      /* Filename does not match */
            }
        }
        else
        {   /* Communication error */
            exitLoop = TRUE;
        }

        if (exitLoop != FALSE)
        {
            break;
        }
    }

    if (!found)
    {
        fileIndex = IFX_EFS_FILE_INDEX_INVALID;
    }

    return fileIndex;
}


Ifx_Efs_Address Ifx_Efs_FirstFileOffsetGet(Ifx_Efs *efs)
{
    return Ifx_AlignOn128(sizeof(Ifx_Efs_TableHeader) + sizeof(Ifx_Efs_FileOffsetItem) * efs->table.header.maxFileCount);
}


Ifx_Efs_Address Ifx_Efs_getMinPartitionSize(Ifx_Efs *efs, Ifx_Efs_FileIndex maxFileCount)
{
    Ifx_Efs_FileSize size;

    /* Min size does include files with size of 0 for dynamicFileSize = 1*/
    size = efs->table.header.flags.dynamicFileSize == 1 ? sizeof(Ifx_Efs_FileHeader) : efs->table.header.defaultAllocatedFileSize;

    return sizeof(Ifx_Efs_TableHeader)
           + sizeof(Ifx_Efs_FileOffsetItem) * maxFileCount
           + size * maxFileCount;
}


Ifx_Efs_FileIndex Ifx_Efs_getBestMaxFileCount(Ifx_Efs *efs)
{
    Ifx_Efs_FileIndex count;

    if (efs->table.header.flags.dynamicFileSize == 1)
    {
        /* FIXME implement the feature */
        count = 0;
    }
    else
    {
        count =
            (efs->table.header.partitionSize - sizeof(Ifx_Efs_TableHeader))
            / (sizeof(Ifx_Efs_FileOffsetItem) + (efs->table.header.flags.dynamicFileSize == 1 ? sizeof(Ifx_Efs_FileHeader) : efs->table.header.defaultAllocatedFileSize));

        if (count > IFX_CFG_EFS_MAX_FILE_COUNT)
        {
            count = IFX_CFG_EFS_MAX_FILE_COUNT;
        }
    }

    return count;
}


/** Return the value of the Ifx_Efs_FileOffsetItem corresponding to the fileIndex
 *
 */
static Ifx_Efs_FileOffsetItem Ifx_Efs_fileOffsetItemGetNew(Ifx_Efs *efs, Ifx_Efs_FileIndex fileIndex, Ifx_Efs_FileSize size)
{
    Ifx_Efs_FileOffsetItem result;

    if (efs->table.header.flags.dynamicFileSize == 1)
    {
        /* FIXME implement the feature */
        result = IFX_EFS_FILE_OFFSET_ITEM_INVALID;
    }
    else if (size <= efs->table.header.defaultAllocatedFileSize)
    {
        result = (Ifx_Efs_FileOffsetItem)((Ifx_Efs_FirstFileOffsetGet(efs) + efs->table.header.defaultAllocatedFileSize * fileIndex) >> 4);
    }
    else
    {
        result = IFX_EFS_FILE_OFFSET_ITEM_INVALID; /* file size not supported */
    }

    return result;
}


/** Return the address offset of the file offset item in the eeprom memory space
 *
 */
static Ifx_Efs_Address Ifx_Efs_fileOffsetItemAddressGet(Ifx_Efs *efs, Ifx_Efs_FileIndex fileIndex)
{
    return efs->tableOffset + sizeof(Ifx_Efs_TableHeader) + fileIndex * sizeof(Ifx_Efs_FileOffsetItem);
}


/** Return the file address in the eeprom memory space
 *
 */
static Ifx_Efs_Address Ifx_Efs_fileAddressGet(Ifx_Efs *efs, Ifx_Efs_FileIndex fileIndex)
{
    return efs->tableOffset + (efs->table.fileOffsetItems[fileIndex] << 4);
}


/** Return the address of the file data in the eeprom memory space
 *
 */
static Ifx_Efs_Address Ifx_Efs_fileDataAddressGet(Ifx_Efs_File *file, Ifx_Efs_Address offset)
{
    return Ifx_Efs_fileAddressGet(file->efs, file->fileIndex) + sizeof(Ifx_Efs_FileHeader) + offset;
}


static boolean Ifx_Efs_fileLoad(Ifx_Efs *efs, Ifx_Efs_File *file, Ifx_Efs_FileIndex fileIndex)
{
    Ifx_Efs_Address address = Ifx_Efs_fileAddressGet(efs, fileIndex);
    file->efs             = efs;
    file->fileIndex       = fileIndex;
    file->status.modified = 0;

    if (Ifx_Efs_eepromRead(efs, address, (uint8 *)&file->header, sizeof(file->header)))
    {
        file->status.opened = 1;
    }
    else
    {
        file->status.opened = 0;
    }

    return file->status.opened == 1;
}


static void Ifx_Efs_crcUpdate(Ifx_Efs *efs)
{
    efs->table.header.fileOffsetItemsCrc = (Ifx_Efs_Crc)Ifx_Crc_tableFast(&efs->crc, (uint8 *)efs->table.fileOffsetItems, efs->table.header.maxFileCount * sizeof(Ifx_Efs_FileOffsetItem));
    efs->table.header.crc                = 0;
    efs->table.header.crc                = (Ifx_Efs_Crc)Ifx_Crc_tableFast(&efs->crc, (uint8 *)&efs->table.header, sizeof(efs->table.header));
}


/** Check the EFS table header crc
 *
 */
static boolean Ifx_Efs_crcHeaderCheck(Ifx_Efs *efs)
{
    boolean     result;
    Ifx_Efs_Crc crc = efs->table.header.crc;
    efs->table.header.crc = 0;
    result                = crc == Ifx_Crc_tableFast(&efs->crc, (uint8 *)&efs->table.header, sizeof(efs->table.header));
    efs->table.header.crc = crc;

    return result;
}


/** Check the EFS table data CRC
 *
 */
static boolean Ifx_Efs_crcDataCheck(Ifx_Efs *efs)
{
    return efs->table.header.fileOffsetItemsCrc == Ifx_Crc_tableFast(&efs->crc, (uint8 *)efs->table.fileOffsetItems, efs->table.header.maxFileCount * sizeof(Ifx_Efs_FileOffsetItem));
}


/** Mount the eeprom file system
 * the eeprom EFS version is returned in the version parameter
 */
boolean Ifx_Efs_mount(Ifx_Efs *efs)
{
    Ifx_Efs_Table table;
    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.ready == 0)
    {
        efs->error.B.invalidAction = 1;
        return FALSE;
    }

    table              = efs->table;
    efs->status.loaded = 0;

    /* Read the existing table */
    if (!Ifx_Efs_eepromRead(efs, efs->tableOffset, (uint8 *)&efs->table.header, sizeof(efs->table.header)))
    {
        return FALSE;
    }
    else
    {}

    /* Check that the EFS ID exists */
    if (memcmp(efs->table.header.fileTableId, efs_id, sizeof(efs->table.header.fileTableId)) == 0)
    {   /* The EFS ID is present */
        /* check that the table header is consistent */
        efs->error.B.crcHeader = (Ifx_Efs_crcHeaderCheck(efs)) ? 0 : 1;

        if (efs->error.B.crcHeader == 0)
        {
            efs->error.B.version = (efs->table.header.efsVersion != IFX_EFS_VERSION) ? 1 : 0;

            if (efs->error.B.version == 0)
            {
                efs->error.B.maxFileCount = (efs->table.header.maxFileCount <= IFX_CFG_EFS_MAX_FILE_COUNT) ? 0 : 1;

                if (efs->error.B.maxFileCount == 0)
                {
                    if (!Ifx_Efs_eepromRead(efs, Ifx_Efs_fileOffsetItemAddressGet(efs, 0), (uint8 *)efs->table.fileOffsetItems, efs->table.header.maxFileCount * sizeof(Ifx_Efs_FileOffsetItem)))
                    {
                        return FALSE;
                    }
                    else
                    {}

                    /* check that the table data is consistent */
                    efs->error.B.crcTable = (Ifx_Efs_crcDataCheck(efs)) ? 0 : 1;

                    if ((efs->error.B.crcTable == 0))
                    {
                        efs->status.loaded = 1;
                    }
                    else
                    {        /* The data CRC is not correct */
                    }
                }
                else
                {        /* the maxFileCount is not supported by the driver */
                }
            }
            else
            {        /* The EFS version is not compatible with the softwer driver */
            }
        }
        else
        {        /* The header CRC in not correct */
        }
    }
    else
    {   /* The EFS ID is not present */
        efs->error.B.efsIdNotPresent = 1;
    }

    if (efs->status.loaded == 0)
    {
        efs->table = table;
    }

    return efs->status.loaded == 1;
}


boolean Ifx_Efs_isLoaded(Ifx_Efs *efs)
{
    return efs->status.loaded == 1;
}


/**
 * \return If TRUE, the efs table header has been properly updated. if FALSE the efs header might be corrupted
 */
static boolean Ifx_Efs_writeHeader(Ifx_Efs *efs)
{
    Ifx_Efs_crcUpdate(efs);
    return Ifx_Efs_eepromWrite(efs, efs->tableOffset, (uint8 *)&efs->table.header, sizeof(efs->table.header));
}


boolean Ifx_Efs_checkConfig(Ifx_Efs *efs)
{

    if (efs->table.header.partitionSize < Ifx_Efs_getMinPartitionSize(efs, 0))
    {
        efs->error.B.partitionSizeMin = 1;
    }

    if (efs->tableOffset + efs->table.header.partitionSize > efs->eeprom->device->size)
    {
        efs->error.B.partitionSizeMax = 1;
    }

    if (efs->table.header.maxFileCount > IFX_CFG_EFS_MAX_FILE_COUNT)
    {
        efs->error.B.maxFileCount = 1;
    }

    return efs->error.U == 0;
}


/** Create the file table only (eeprom)
 */
boolean Ifx_Efs_formatFast(Ifx_Efs *efs)
{
    uint16 i;
    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.ready == 0)
    {
        efs->error.B.invalidAction = 1;
        return FALSE;
    }

    if (!Ifx_Efs_checkConfig(efs))
    {
        return FALSE;
    }

    for (i = 0; i < efs->table.header.maxFileCount; i++)
    {
        efs->table.fileOffsetItems[i] = IFX_EFS_FILE_OFFSET_ITEM_INVALID;
    }

    if (!Ifx_Efs_eepromWrite(efs, Ifx_Efs_fileOffsetItemAddressGet(efs, 0), (uint8 *)efs->table.fileOffsetItems, efs->table.header.maxFileCount * sizeof(Ifx_Efs_FileOffsetItem)))
    {
        return FALSE;
    }

    if (!Ifx_Efs_writeHeader(efs))
    {
        return FALSE;
    }

    return Ifx_Efs_mount(efs);
}


/** Format the eeprom (write the eeprom with zeros), create the file table (eeprom)
 */
boolean Ifx_Efs_format(Ifx_Efs *efs)
{
    boolean result;

    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.ready == 0)
    {
        efs->error.B.invalidAction = 1;
        return FALSE;
    }

    if (!Ifx_Efs_checkConfig(efs))
    {
        return FALSE;
    }

    result = Ifx_Efs_eepromErase(efs);

    if (result)
    {
        result = Ifx_Efs_formatFast(efs);
    }

    return result;
}


/** Initialize the directory listing
 */
void Ifx_Efs_dirInit(Ifx_Efs *efs)
{
    efs->dirIndex = -1;
}


/** Get the next file.
 * return false if no file found
 */
boolean Ifx_Efs_dir(Ifx_Efs *efs, Ifx_Efs_File *file)
{
    boolean result;
    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.loaded == 0)
    {
        efs->error.B.invalidAction = 1;
        return FALSE;
    }

    do
    {
        efs->dirIndex++;
    } while ((efs->dirIndex < efs->table.header.maxFileCount) && (efs->table.fileOffsetItems[efs->dirIndex] == IFX_EFS_FILE_OFFSET_ITEM_INVALID));

    if (efs->dirIndex < efs->table.header.maxFileCount)
    {
        result = Ifx_Efs_fileLoad(efs, file, efs->dirIndex);
    }
    else
    {
        result = FALSE;
    }

    return result;
}


/**
 * \return If TRUE, the efs table has been properly updated. if FALSE the efs header might be corrupted
 */
static boolean Ifx_Efs_writefileOffsetItem(Ifx_Efs *efs, Ifx_Efs_FileIndex fileIndex, Ifx_Efs_FileOffsetItem fileOffsetItem)
{
    boolean result = FALSE;

    if (Ifx_Efs_eepromWrite(efs, Ifx_Efs_fileOffsetItemAddressGet(efs, fileIndex), (uint8 *)&fileOffsetItem, sizeof(fileOffsetItem)))
    {
        efs->table.fileOffsetItems[fileIndex] = fileOffsetItem;
        result                                = Ifx_Efs_writeHeader(efs);
    }
    else
    {   /* EFS might be corrupted */
    }

    return result;
}


/** delete a file (eeprom)
 */
boolean Ifx_Efs_fileDelete(Ifx_Efs *efs, pchar filename)
{
    boolean           result = TRUE;
    Ifx_Efs_FileIndex fileIndex;
    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.loaded == 0)
    {
        efs->error.B.invalidAction = 1;
        return FALSE;
    }

    fileIndex = Ifx_Efs_fileIndexGet(efs, filename);

    if (fileIndex != IFX_EFS_FILE_INDEX_INVALID)
    {
        /* Write the EFS table data to the eeprom */
        result = Ifx_Efs_writefileOffsetItem(efs, fileIndex, IFX_EFS_FILE_OFFSET_ITEM_INVALID);
    }
    else
    {   /* File not found */
        result = FALSE;
    }

    return result;
}


/** open a file for read / write. If the file exists, the file header is loaded (eeprom).
 */
boolean Ifx_Efs_fileOpen(Ifx_Efs *efs, pchar filename, Ifx_Efs_File *file)
{
    Ifx_Efs_FileIndex fileIndex;
    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.loaded == 0)
    {
        efs->error.B.invalidAction = 1;
        return FALSE;
    }

    file->status.opened       = 0;
    file->status.fileNotFound = 0;

    fileIndex                 = Ifx_Efs_fileIndexGet(efs, filename);

    if (fileIndex != IFX_EFS_FILE_INDEX_INVALID)
    {
        Ifx_Efs_fileLoad(efs, file, fileIndex);
    }
    else
    {   /* File not found */
        file->status.fileNotFound = 1;
    }

    return file->status.opened == 1;
}


static boolean Ifx_Efs_fileHeaderSave(Ifx_Efs_File *file)
{
    boolean         saved;
    Ifx_Efs_Address address = Ifx_Efs_fileAddressGet(file->efs, file->fileIndex);

    /* Update header crc */
    file->header.crc = 0;
    file->header.crc = (Ifx_Efs_Crc)Ifx_Crc_tableFast(&file->efs->crc, (uint8 *)&file->header, sizeof(file->header));

    if (Ifx_Efs_eepromWrite(file->efs, address, (uint8 *)&file->header, sizeof(file->header)))
    {
        file->status.modified = 0;
        saved                 = TRUE;
    }
    else
    {
        saved = FALSE;
    }

    return saved;
}


/** Save the file header (eemprom) and close it.
 */
boolean Ifx_Efs_fileClose(Ifx_Efs_File *file)
{
    boolean result = TRUE;

    Ifx_Efs_errorFlagClear(file->efs);

    if (file->status.opened == 0)
    {
        file->efs->error.B.invalidAction = 1;
        return FALSE;
    }

    if (file->status.opened == 1)
    {
        boolean closed = FALSE;

        if (file->status.modified == 1)
        {
            if (Ifx_Efs_fileHeaderSave(file))
            {
                closed = TRUE;
            }
            else
            {
                result = FALSE;
            }
        }
        else
        {   /* No need to save the file header */
            closed = TRUE;
        }

        if (closed)
        {
            file->status.opened = 0;
            file->fileIndex     = IFX_EFS_FILE_INDEX_INVALID;
        }
    }
    else
    {   /* File  not opened */
        file->efs->error.B.invalidAction = 1;
    }

    return result;
}


/** Create a new file and save the header to the eeprom. If the file already exists, the file is opened
 *
 */
boolean Ifx_Efs_fileCreate(Ifx_Efs *efs, pchar filename, Ifx_Efs_File *file)
{
    boolean result = FALSE;

    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.loaded == 0)
    {
        efs->error.B.invalidAction = 1;
        return FALSE;
    }

    file->efs                 = efs;
    file->fileIndex           = Ifx_Efs_fileIndexGet(efs, filename);
    file->status.opened       = 0;
    file->status.fileNotFound = 0;
    file->status.modified     = 0;

    if (file->fileIndex != IFX_EFS_FILE_INDEX_INVALID)
    {
        result = Ifx_Efs_fileOpen(efs, filename, file);
    }
    else
    {   /* File not found, create the file */
        /* Find a free file index slot */
        Ifx_Efs_FileIndex fileIndex;

        for (fileIndex = 0; fileIndex < efs->table.header.maxFileCount; fileIndex++)
        {
            if (efs->table.fileOffsetItems[fileIndex] == IFX_EFS_FILE_OFFSET_ITEM_INVALID)
            {
                Ifx_Efs_FileOffsetItem fileOffsetItem;
                Ifx_Efs_FileSize       size = 0;
                fileOffsetItem = Ifx_Efs_fileOffsetItemGetNew(efs, fileIndex, size);

                if (fileOffsetItem != IFX_EFS_FILE_OFFSET_ITEM_INVALID)
                {
                    sint8 i;
                    file->status.reserved          = 0;
                    file->header.dataSize          = size;
                    file->fileIndex                = fileIndex;
                    file->header.allocatedFileSize = efs->table.header.defaultAllocatedFileSize;
                    /* Copy the filename */
                    memset(file->header.filename, 0, sizeof(file->header.filename));
                    i = 0;

                    while ((i < (sint8)sizeof(file->header.filename)) && ((efs->table.header.flags.filenameZes == 0) || (filename[i] != 0)))
                    {
                        file->header.filename[i] = filename[i];
                        i++;
                    }

                    file->header.flags.hasCrc             = IFX_CFG_EFS_DEFAULT_FILE_DATA_USE_CRC;
                    file->header.flags.isDirectory        = 0;
                    file->header.flags.reserved           = 0;
                    file->header.crc                      = 0;
                    file->header.dataCrc                  = 0;

                    file->header.headerSize               = sizeof(file->header);

                    efs->table.fileOffsetItems[fileIndex] = fileOffsetItem;                       /* Ifx_Efs_fileHeaderSave() requires this variable to be set */

                    if (!Ifx_Efs_fileHeaderSave(file))
                    {                                                                             /* Header could not be saved */
                        efs->table.fileOffsetItems[fileIndex] = IFX_EFS_FILE_OFFSET_ITEM_INVALID; /* Roll back the efs table */
                    }
                    else
                    {                                                                             /* Save the file pointer to the EFS table */
                        if (Ifx_Efs_writefileOffsetItem(efs, fileIndex, fileOffsetItem))
                        {
                            file->status.opened = 1;
                            result              = TRUE;
                        }
                        else
                        {                                                                             /*Error while writing the efs table */
                            efs->table.fileOffsetItems[fileIndex] = IFX_EFS_FILE_OFFSET_ITEM_INVALID; /* Roll back the efs table */
                        }
                    }
                }
                else
                {   /* Not enough memory space */
                    efs->error.B.maxFileCount = 1;
                }

                break;
            }
        }
    }

    if (file->status.opened == 0)
    {
        file->fileIndex = IFX_EFS_FILE_INDEX_INVALID;
    }

    return result;
}


/** Set the data CRC
 *
 * The CRC is calculated on the data buffer. the data length is specified by the file data size (file->header.dataSize)
 */
boolean Ifx_Efs_fileDataCrcSet(Ifx_Efs_File *file, uint8 *data)
{
    boolean result;

    if (file->header.flags.hasCrc == 1)
    {
        file->header.dataCrc  = (Ifx_Efs_Crc)Ifx_Crc_tableFast(&file->efs->crc, data, file->header.dataSize);
        file->status.modified = 1;
        result                = TRUE;
    }
    else
    {
        result = FALSE;
    }

    return result;
}


/** Check the data CRC
 *
 *  \return Returns TRUE if the CRC is correct or if the CRC is disabled, else FALSE
 */
boolean Ifx_Efs_fileDataCrcCheck(Ifx_Efs_File *file, uint8 *data)
{
    boolean result;

    if (file->header.flags.hasCrc == 1)
    {
        result = file->header.dataCrc == Ifx_Crc_tableFast(&file->efs->crc, data, file->header.dataSize);
    }
    else
    {
        result = TRUE;
    }

    return result;
}


/** Enable the data CRC
 */
void Ifx_Efs_fileDataCrcEnable(Ifx_Efs_File *file)
{
    if (file->header.flags.hasCrc == 0)
    {
        file->header.flags.hasCrc = 1;
        file->status.modified     = 1;
    }
    else
    {}
}


/** Disable the data CRC
 */
void Ifx_Efs_fileDataCrcDisable(Ifx_Efs_File *file)
{
    if (file->header.flags.hasCrc == 1)
    {
        file->header.flags.hasCrc = 0;
        file->status.modified     = 1;
    }
    else
    {}
}


/** Return the data crc configuration
 *
 * \return returns TRUE if the data CRC is enabled
 */
boolean Ifx_Efs_fileDataIsCrc(Ifx_Efs_File *file)
{
    return file->header.flags.hasCrc;
}


/** Get the data CRC
 */
Ifx_Efs_Crc Ifx_Efs_fileDataCrcGet(Ifx_Efs_File *file)
{
    return file->header.dataCrc;
}


/** Set the file data size
 * return false if the size is out of range
 */
boolean Ifx_Efs_fileSizeSet(Ifx_Efs_File *file, Ifx_Efs_FileSize size)
{
    boolean result;

    if (file->efs->table.header.flags.dynamicFileSize)
    {
        result = FALSE; /* FIXME implement this feature */
    }
    else if (size <= Ifx_Efs_fileMaxDataSizeGet(file))
    {
        file->header.dataSize = size;
        file->status.modified = 1;
        result                = TRUE;
    }
    else
    {   /* size is bigger than the allocated memory */
        file->efs->error.B.fileSizeOverflow = 1;
        result                              = FALSE;
    }

    return result;
}


/** Return  the file data size
 */
Ifx_Efs_FileSize Ifx_Efs_fileSizeGet(Ifx_Efs_File *file)
{
    return file->header.dataSize;
}


/** Read data from the file (eeprom)
 * Return the difference between requested length and actual read size (bytes);
 */
Ifx_Efs_FileOffset Ifx_Efs_fileRead(Ifx_Efs_File *file, Ifx_Efs_FileOffset offset, Ifx_Efs_FileOffset length, uint8 *buffer)
{
    Ifx_Efs_FileOffset size;
    Ifx_Efs_errorFlagClear(file->efs);

    if (file->status.opened == 0)
    {
        file->efs->error.B.invalidAction = 1;
        return FALSE;
    }

    size = length;

    if ((offset + length) > file->header.dataSize)
    {
        size = __max(file->header.dataSize - offset, 0);
    }

    if (size > 0)
    {
        if (!Ifx_Efs_eepromRead(file->efs, Ifx_Efs_fileDataAddressGet(file, offset), buffer, size))
        {
            size = 0;
        }
        else
        {}
    }

    return length - size;
}


/** Write data to the file (eeprom)
 * \return bytes of remaining data which hasn't been written
 */
Ifx_Efs_FileOffset Ifx_Efs_fileWrite(Ifx_Efs_File *file, Ifx_Efs_FileOffset offset, Ifx_Efs_FileOffset length, const uint8 *buffer)
{
    Ifx_Efs_FileOffset size;
    Ifx_Efs_errorFlagClear(file->efs);

    if (file->status.opened == 0)
    {
        file->efs->error.B.invalidAction = 1;
        return FALSE;
    }

    size = length;

    if ((offset + length) > file->header.dataSize)
    {
        size                  = __min(Ifx_Efs_fileMaxDataSizeGet(file) - offset, size);
        size                  = __max(size, 0);
        file->header.dataSize = offset + size;
        file->status.modified = 1;
    }

    if (size > 0)
    {
        if (!Ifx_Efs_eepromWrite(file->efs, Ifx_Efs_fileDataAddressGet(file, offset), buffer, size))
        {
            size = 0;
        }
        else
        {}
    }

    return length - size;
}


boolean Ifx_Efs_fileExist(Ifx_Efs *efs, pchar filename)
{
    return Ifx_Efs_fileIndexGet(efs, filename) != IFX_EFS_FILE_INDEX_INVALID;
}


/** Set the filename zero ending string flag
 *
 */
boolean Ifx_Efs_filenameZesSet(Ifx_Efs *efs, boolean value)
{
    boolean result;

    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.ready == 0)
    {
        efs->error.B.invalidAction = 1;
        result                     = FALSE;
    }
    else
    {
        efs->table.header.flags.filenameZes = value ? 1 : 0;

        if (efs->status.loaded == 1)
        {
            result = Ifx_Efs_writeHeader(efs);
        }
        else
        {
            result = TRUE;
        }
    }

    return result;
}


/** Return the filename zero ending string flag
 *
 */
boolean Ifx_Efs_filenameZesGet(Ifx_Efs *efs)
{
    return efs->table.header.flags.filenameZes == 1;
}


/** Set the default allocated file size
 *
 */
boolean Ifx_Efs_defaultAllocatedFileSizeSet(Ifx_Efs *efs, Ifx_Efs_FileSize size)
{
    boolean result = TRUE;

    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.loaded == 1)
    {
        efs->error.B.invalidAction = 1;
        result                     = FALSE;
    }
    else
    {
        efs->table.header.defaultAllocatedFileSize = Ifx_AlignOn128(size);
    }

    return result;
}


/** Return the default allocated file size
 *
 */
Ifx_Efs_FileSize Ifx_Efs_defaultAllocatedFileSizeGet(Ifx_Efs *efs)
{
    return efs->table.header.defaultAllocatedFileSize;
}


/** Erase the complete file system */
boolean Ifx_Efs_erase(Ifx_Efs *efs)
{
    boolean result;
    Ifx_Efs_errorFlagClear(efs);

    if (efs->status.loaded == 0)
    {
        efs->error.B.invalidAction = 1;
        return FALSE;
    }

    result = Ifx_At25xxx_erase(efs->eeprom, efs->tableOffset, efs->table.header.partitionSize, 0x00);

    Ifx_Efs_Init(efs, efs->eeprom);

    return result;
}


/** Unmount the file system  */
boolean Ifx_Efs_unmount(Ifx_Efs *efs)
{
    Ifx_Efs_errorFlagClear(efs);
    efs->status.loaded = 0;

    return TRUE;
}
