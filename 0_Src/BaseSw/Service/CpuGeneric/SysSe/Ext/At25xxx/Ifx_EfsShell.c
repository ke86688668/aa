/**
 * \file Ifx_EfsShell.c
 * \brief Ifx_EfsShell.c
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */
#include "SysSe/Comm/Ifx_Shell.h"
#include "Ifx_EfsShell.h"

#include <string.h>
#include <stdio.h>

#define BOLD         "\033[1m"
#define BOLD_INVERSE "\033[1;7m"
#define BLUE         "\033[34m"
#define GREEN        "\033[32m"
#define MAGENTA      "\033[35m"
#define BLACK        "\033[30m"
#define RED          "\033[31m"

void Ifx_EfsShell_printInfo(Ifx_Efs *efs, IfxStdIf_DPipe *io)
{
    if (Ifx_Efs_isLoaded(efs))
    {
        Ifx_Efs_Address address;
        IfxStdIf_DPipe_print(io, "File system"ENDL);
        IfxStdIf_DPipe_print(io, "  Version:%d.%d"ENDL, (efs->table.header.efsVersion >> 8), (efs->table.header.efsVersion & 0xFF));
        IfxStdIf_DPipe_print(io, "  Table offset:%d"ENDL, efs->tableOffset);
        IfxStdIf_DPipe_print(io, "  Partition size:%d"ENDL, efs->table.header.partitionSize);
        IfxStdIf_DPipe_print(io, "  Default file size (raw):%d"ENDL, efs->table.header.defaultAllocatedFileSize);
        IfxStdIf_DPipe_print(io, "  Default file size (available):%d"ENDL, (efs->table.header.defaultAllocatedFileSize - sizeof(Ifx_Efs_FileHeader)));
        IfxStdIf_DPipe_print(io, "  Max files:%d"ENDL, efs->table.header.maxFileCount);
        IfxStdIf_DPipe_print(io, "  Address offset of 1st file:%d"ENDL, Ifx_Efs_FirstFileOffsetGet(efs));
        address = Ifx_Efs_getMinPartitionSize(efs, Ifx_Efs_maxFileCountGet(efs));

        if (address)
        {
            IfxStdIf_DPipe_print(io, "  Suggested partition size:%d"ENDL, address);
        }
    }
    else
    {
        IfxStdIf_DPipe_print(io, "File system not mounted"ENDL);
    }
}


static void Ifx_EfsShell_printStatus(Ifx_Efs *efs, IfxStdIf_DPipe *io)
{
    if (efs->error.B.communication != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: communication");
    }

    if (efs->error.B.crcHeader != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: crcHeader"ENDL);
    }

    if (efs->error.B.crcTable != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: crcTable"ENDL);
    }

    if (efs->error.B.efsIdNotPresent != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: efsIdNotPresent"ENDL);
    }

    if (efs->error.B.invalidAction != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: invalidAction"ENDL);
    }

    if (efs->error.B.maxFileCount != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: maxFileCount"ENDL);
    }

    if (efs->error.B.writeOutOfMemory != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: writeOutOfMemory"ENDL);
    }

    if (efs->error.B.readOutOfMemory != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: readOutOfMemory"ENDL);
    }

    if (efs->error.B.partitionSizeMax != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: partitionSizeMax"ENDL);
    }

    if (efs->error.B.offsetOutOfMemory != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: offsetOutOfMemory"ENDL);
    }

    if (efs->error.B.maxFileCount != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: maxFileCount"ENDL);
    }

    if (efs->error.B.fileSizeOverflow != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: fileSizeOverflow"ENDL);
    }

    if (efs->error.B.version != 0)
    {
        IfxStdIf_DPipe_print(io, "ERR: version"ENDL);
    }
}


static boolean Ifx_EfsShell_format(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_EfsShell       *shell;
    shell = (Ifx_EfsShell *)data;
    Ifx_Efs            *efs    = shell->efs;

    Ifx_Efs_Address     offset = 0;
    Ifx_At25xxx_Address size;
    boolean             result = TRUE;

    Ifx_Efs_unmount(efs);

    if (Ifx_Shell_parseUInt32(&arguments, &offset, FALSE) != FALSE)
    {
        result = Ifx_Efs_tableOffsetSet(efs, offset);

        if (result)
        {
            if (Ifx_Shell_parseUInt32(&arguments, &size, FALSE) != FALSE)
            {
                result = Ifx_Efs_partitionSizeSet(efs, size);

                if (!result)
                {
                    IfxStdIf_DPipe_print(io, "Error: invalid size"ENDL);
                    result = FALSE;
                }
            }
        }
        else
        {
            IfxStdIf_DPipe_print(io, "Error: invalid offset"ENDL);
            result = FALSE;
        }
    }

    if (result)
    {
        IfxStdIf_DPipe_print(io, "Formating... "ENDL);
        efs->status.loaded = 0; // WORKAROUND: force efs non-loaded.
        Ifx_Efs_defaultAllocatedFileSizeSet(efs, IFX_CFG_EFS_DEFAULT_ALLOCATED_FILE_SIZE);

        if (Ifx_Efs_formatFast(efs))
        {
            IfxStdIf_DPipe_print(io, "Success."ENDL);
            Ifx_EfsShell_printInfo(efs, io);
        }
        else
        {
            IfxStdIf_DPipe_print(io, "Error."ENDL);
        }
    }

    Ifx_EfsShell_printStatus(efs, io);

    return TRUE;
}


static boolean Ifx_EfsShell_mount(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_EfsShell *shell;
    shell = (Ifx_EfsShell *)data;
    Ifx_Efs      *efs    = shell->efs;

    sint32        offset = 0;
    boolean       result = TRUE;

    if (Ifx_Shell_parseSInt32(&arguments, &offset) != FALSE)
    {
        result = Ifx_Efs_tableOffsetSet(efs, offset);

        if (!result)
        {
            IfxStdIf_DPipe_print(io, "Error: invalid offset"ENDL);
            result = FALSE;
        }
    }

    if (result)
    {
        IfxStdIf_DPipe_print(io, "Mounting... "ENDL);

        if (Ifx_Efs_mount(efs))
        {
            IfxStdIf_DPipe_print(io, "Success."ENDL);
            Ifx_EfsShell_printInfo(efs, io);
        }
        else
        {
            IfxStdIf_DPipe_print(io, "Error."ENDL);
        }
    }

    Ifx_EfsShell_printStatus(efs, io);

    return TRUE;
}


static boolean Ifx_EfsShell_erase(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_EfsShell *shell;
    shell = (Ifx_EfsShell *)data;
    Ifx_Efs      *efs = shell->efs;


    if (Ifx_Shell_matchToken(&arguments, "eeprom") != FALSE)
    {
    	uint32 pattern = 0;
        Ifx_Shell_parseUInt32(&arguments, &pattern, TRUE);

        IfxStdIf_DPipe_print(io, "Erasing entire EEPROM... using pattern 0x%02X"ENDL, pattern);
        if (Ifx_At25xxx_erase(efs->eeprom, 0, efs->eeprom->device->size, (uint8)pattern))
        {
            Ifx_Efs_unmount(efs);
            IfxStdIf_DPipe_print(io, "Success."ENDL);
            Ifx_EfsShell_printInfo(efs, io);
        }
        else
        {
            Ifx_Efs_unmount(efs);
            IfxStdIf_DPipe_print(io, "Error."ENDL);
        }
    }
    else
    {
        IfxStdIf_DPipe_print(io, "Erasing... "ENDL);
        if (Ifx_Efs_erase(efs))
        {
            IfxStdIf_DPipe_print(io, "Success."ENDL);
            Ifx_EfsShell_printInfo(efs, io);
        }
        else
        {
            IfxStdIf_DPipe_print(io, "Error."ENDL);
        }
    }

    Ifx_EfsShell_printStatus(efs, io);

    return TRUE;
}


static boolean Ifx_EfsShell_unmount(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_EfsShell *shell;
    shell = (Ifx_EfsShell *)data;
    Ifx_Efs      *efs = shell->efs;

    IfxStdIf_DPipe_print(io, "Unmounting... "ENDL);

    if (Ifx_Efs_unmount(efs))
    {
        IfxStdIf_DPipe_print(io, "Success."ENDL);
        Ifx_EfsShell_printInfo(efs, io);
    }
    else
    {
        IfxStdIf_DPipe_print(io, "Error."ENDL);
    }

    Ifx_EfsShell_printStatus(efs, io);

    return TRUE;
}


static boolean Ifx_EfsShell_reset(pchar arguments, void *data, IfxStdIf_DPipe *io)
{

    return FALSE; /* FIXME to be implmented*/
}


static boolean Ifx_EfsShell_info(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_EfsShell *shell;
    shell = (Ifx_EfsShell *)data;
    Ifx_Efs      *efs = shell->efs;

    Ifx_EfsShell_printInfo(efs, io);
    Ifx_EfsShell_printStatus(efs, io);
    return TRUE;
}


static boolean Ifx_EfsShell_dir(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_EfsShell     *shell;
    shell = (Ifx_EfsShell *)data;
    Ifx_Efs          *efs = shell->efs;

    Ifx_Efs_File      file;
    Ifx_Efs_FileIndex count = 0;
    Ifx_Efs_dirInit(efs);

    while (Ifx_Efs_dir(efs, &file))
    {
        count++;
        IfxStdIf_DPipe_print(io, "%s Size:%d"ENDL, file.header.filename, file.header.dataSize);
    }

    if (count == 0)
    {
        IfxStdIf_DPipe_print(io, "Empty file system"ENDL);
    }
    else
    {
        IfxStdIf_DPipe_print(io, "%d file(s)"ENDL, count);
    }

    Ifx_EfsShell_printStatus(efs, io);

    return TRUE;
}

static boolean Ifx_EfsShell_dump(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_EfsShell *shell;
    shell = (Ifx_EfsShell *)data;
    Ifx_Efs      *efs    = shell->efs;

    sint32        offset = 0;
    sint32        size = efs->eeprom->device->size;
    boolean       result = TRUE;

    if (Ifx_Shell_parseSInt32(&arguments, &offset) != FALSE)
    {

        if (offset >= size)
        {
            IfxStdIf_DPipe_print(io, "Error: invalid offset. value range=[0x0,0x%X]"ENDL, (size-1));
            result = FALSE;
        }
        if (Ifx_Shell_parseSInt32(&arguments, &size) != FALSE)
        {

            if (offset + size > efs->eeprom->device->size)
            {
            	size = efs->eeprom->device->size - offset;
                IfxStdIf_DPipe_print(io, "Warning: invalid size. Value truncated to 0x%X", size);
            }
        }
    }

    if (result)
    {
    	while (size)
    	{
    		uint8 buffer[64];
        	sint32 chunkSize = __minX(size, 64);
        	size -= chunkSize;
        	if (Ifx_At25xxx_read(efs->eeprom, offset, buffer, chunkSize))
        	{
        		uint32 i;
            	IfxStdIf_DPipe_print(io, "0x%04X ", offset);
        		for (i=0;i<chunkSize;i++)
        		{
                	IfxStdIf_DPipe_print(io, "%02X", buffer[i]);
        		}
            	IfxStdIf_DPipe_print(io, ENDL, buffer[i]);

        	}
        	else
        	{
            	IfxStdIf_DPipe_print(io, "Read Error at offset 0x%X."ENDL, offset);
        	}
        	offset+=chunkSize;

    	}
    }


    return TRUE;
}

static boolean Ifx_EfsShell_print(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_EfsShell *shell;
    shell = (Ifx_EfsShell *)data;
    Ifx_Efs      *efs = shell->efs;

    char          filename[IFX_CFG_EFS_FILENAME_SIZE];
    Ifx_Efs_File  file;
    memset(filename, 0, IFX_CFG_EFS_FILENAME_SIZE);

    if (Ifx_Shell_parseToken(&arguments, (char *)filename, IFX_CFG_EFS_FILENAME_SIZE) != FALSE)
    {
        if (Ifx_Efs_fileOpen(efs, filename, &file) != FALSE)
        {
            uint16             i;
            uint8              content[256];
            uint8             *pcontent  = content;
            Ifx_Efs_FileOffset length    = 255;
            Ifx_Efs_FileOffset remaining = Ifx_Efs_fileRead(&file, 0, length, content);
            Ifx_Efs_FileOffset readLen   = length - remaining;

            while (readLen > 0)
            {
                for (i = 0; i < 32; i++)
                {
                    IfxStdIf_DPipe_print(io, "%02X ", *pcontent);
                    pcontent = &pcontent[1];
                    readLen  = readLen - 1;

                    if (readLen == 0)
                    {
                        break;
                    }
                }

                IfxStdIf_DPipe_print(io, ENDL);
            }

            IfxStdIf_DPipe_print(io, ENDL);
        }
        else
        {
            IfxStdIf_DPipe_print(io, "Error opening file"ENDL);
        }
    }

    Ifx_EfsShell_printStatus(efs, io);

    return TRUE;
}


static boolean Ifx_EfsShell_delete(pchar arguments, void *data, IfxStdIf_DPipe *io)
{
    Ifx_EfsShell *shell;
    shell = (Ifx_EfsShell *)data;
    Ifx_Efs      *efs = shell->efs;

    char          filename[IFX_CFG_EFS_FILENAME_SIZE];
    memset(filename, 0, IFX_CFG_EFS_FILENAME_SIZE);

    if (Ifx_Shell_parseToken(&arguments, (char *)filename, IFX_CFG_EFS_FILENAME_SIZE) != FALSE)
    {
        if (Ifx_Efs_fileDelete(efs, filename) != FALSE)
        {
            IfxStdIf_DPipe_print(io, "File deleted"ENDL);
        }
        else
        {
            IfxStdIf_DPipe_print(io, "Error deleting file"ENDL);
        }
    }

    Ifx_EfsShell_printStatus(efs, io);

    return TRUE;}


/** \addtogroup library_srvsw_sysse_ext_at25xxx_efs_efsshell
 * \{ */
/** EFS commands / and syntax */
/* *INDENT-OFF* */
Ifx_Shell_Command Ifx_g_EfsShell_commands[] =
{
    {IFX_EFS_SHELL_PREFIX, " : EFS (Ifx_At25xxx File System)functions"
            , NULL_PTR, NULL_PTR               },
    {"format", " : Format the EFS"
            "/s format <offset> <size> : Create the file system, and format the EFS"ENDL
                "/p <offset> : Offset of the file system in the EEPROM. Default=0"ENDL
                "/p <size>   : Size of the file system. Default=EEPROM size - <offset>"
            , NULL_PTR, &Ifx_EfsShell_format     },
    {"mount", " : Mount the EFS"
            "/s mount <offset> : Mount the file system"ENDL
                "/p <offset> : Offset of the file system in the EEPROM. Default=0"
            , NULL_PTR, &Ifx_EfsShell_mount     },
    {"unmount", " : Unmount the EFS"
            , NULL_PTR, &Ifx_EfsShell_unmount     },
    {"erase", " : Erase the EFS"
            "/s erase [eeprom] [<pattern>] : Erase the EFS or the complete EEPROM"ENDL
                "/p eeprom : If eeprom is specified, the complete eeprom is erased"
                "/p pattern : Pattern byte to be written during the eerase process, default 0x00 "
            , NULL_PTR, &Ifx_EfsShell_erase     },
    {"reset", " : reset/restart the event logger"
            , NULL_PTR, &Ifx_EfsShell_reset    },
    {"info", " : show the EFS information"
            , NULL_PTR, &Ifx_EfsShell_info     },
    {"dir", " : show the directory of the EFS"
            , NULL_PTR, &Ifx_EfsShell_dir     },
    {"print", " : display file content"ENDL
            "/s print <file> : Display the file content"ENDL
                "/p <file>   : file name"
            , NULL_PTR, &Ifx_EfsShell_print     },
    {"dump", " : dump the EEPROM memory content"ENDL
            "/s dump [<offset>] [<size>]: Display the file content"ENDL
                "/p <offset>   : Dump start offset. default is 0"
                "/p <size>   : Dump size in byte. Default is up to the end of the device"
            , NULL_PTR, &Ifx_EfsShell_dump     },
    {"delete", " : delete a file"
            "/s delete <file> : Delete the file"ENDL
                "/p <file>   : file name"
            , NULL_PTR, &Ifx_EfsShell_delete     },
    IFX_SHELL_COMMAND_LIST_END,
};
/* *INDENT-ON* */

/** \} */

boolean Ifx_EfsShell_init(Ifx_EfsShell *shell, Ifx_EfsShell_Config *config)
{
    uint32 index;

    index      = 0;
    shell->efs = config->efs;

    while (Ifx_g_EfsShell_commands[index].commandLine != NULL_PTR)
    {
        Ifx_g_EfsShell_commands[index].data = shell;
        index++;
    }

    return TRUE;
}


void Ifx_EfsShell_initConfig(Ifx_EfsShell_Config *config, Ifx_Efs *efs)
{
    config->efs = efs;
}
