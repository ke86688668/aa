/**
 * \file Ifx_EfsShell.h
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_ext_at25xxx_efs_efsshell Shell for Ifx_Efs file system
 * \ingroup library_srvsw_sysse_ext_at25xxx_efs
 */

#ifndef IFX_EFSSHELL_H
#define IFX_EFSSHELL_H

#include "Ifx_Efs.h"
#include "Ifx_Cfg.h"

#include "SysSe/Comm/Ifx_Shell.h"
#include "SysSe/Debug/Ifx_Osci.h"

#define IFX_EFS_SHELL_PREFIX "efs"

typedef struct
{
    Ifx_Efs *efs;
}Ifx_EfsShell;

typedef struct
{
    Ifx_Efs *efs;
}Ifx_EfsShell_Config;

IFX_EXTERN Ifx_Shell_Command Ifx_g_EfsShell_commands[]; /* FIXME rename using global var convention */

/** \addtogroup library_srvsw_sysse_ext_at25xxx_efs_efsshell
 * \{ */
boolean Ifx_EfsShell_init(Ifx_EfsShell *shell, Ifx_EfsShell_Config *config);
void    Ifx_EfsShell_initConfig(Ifx_EfsShell_Config *config, Ifx_Efs *efs);
void    Ifx_EfsShell_printInfo(Ifx_Efs *efs, IfxStdIf_DPipe *io);

/** \} */

#endif /* IFX_EFSSHELL_H */
