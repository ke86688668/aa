/**
 * \file Ifx_At25xxx.c
 * \brief EEPROM interface (for AT128N on board EEPROM)
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

#include "Ifx_At25xxx.h"
#include "_Utilities/Ifx_Assert.h"
#include <string.h>
#include "SysSe/Bsp/Bsp.h"

#define IFX_AT25XXX_ASSERT(level, expr) IFX_ASSERT(level, expr)

/* Device-specific definitions */
#define IFX_AT25XXX_WRITE_TIMEOUT       (15)     /**< Maximum time to allow EEPROM to write */

/* Commands */
#define IFX_AT25XXX_CMD_WRITE_STATUS    (0x01)   /**< Write to status register */
#define IFX_AT25XXX_CMD_WRITE_DATA      (0x02)   /**< Write data */
#define IFX_AT25XXX_CMD_READ_DATA       (0x03)   /**< Read data */
#define IFX_AT25XXX_CMD_WRITE_DISABLE   (0x04)   /**< Disable write-enable */
#define IFX_AT25XXX_CMD_READ_STATUS     (0x05)   /**< Read status register */
#define IFX_AT25XXX_CMD_WRITE_ENABLE    (0x06)   /**< Enable write-enable */

/* Lengths of commands to access EEPROM */
#define IFX_AT25XXX_CMD_LEN_READ        (3)      /**< length of command/address to read data */
#define IFX_AT25XXX_CMD_LEN_WRITE       (3)      /**< length of command/address to write data */

/* Register masks and bit definitions */
#define IFX_AT25XXX_STATUS_BUSY         (0x01)   /**< Ready bit, but ACTIVE LOW (i.e. if HIGH then device is busy) - therefore renamed BUSY */
#define IFX_AT25XXX_STATUS_WRITE_ENABLE (0x02)   /**< Write Enable bit - HIGH means device is write-enabled */

/* Driver-specific definitions */
#define IFX_AT25XXX_READ_BLOCK_SIZE     (256)    /**< Block size to use for reading */
#define IFX_AT25XXX_PAGE_SIZE           (64)     /**< Sector size internally in EEPROM that limits number of consecutive writes */

void Ifx_At25xxx_onDataEvent(void *data);
void Ifx_At25xxx_onErrorEvent(void *data);

const Ifx_At25xxx_Device Ifx_g_At25xxx_at25256 = {
    .type                = "AT25256",
    .size                = 32768,
    .protectionZoneCount = 4,
    .protectionZones     = {
        {
            .protection = 0,
            .offset     = 0,
            .size       = 0x0,
        },
        {
            .protection = 1,
            .offset     = 0x6000,
            .size       = 0x2000,
        },
        {
            .protection = 2,
            .offset     = 0x4000,
            .size       = 0x4000,
        },
        {
            .protection = 3,
            .offset     = 0x0,
            .size       = 0x8000,
        }
    },
};

const Ifx_At25xxx_Device Ifx_g_At25xxx_at25128 = {
    .type                = "AT25128",
    .size                = 16384,
    .protectionZoneCount = 4,
    .protectionZones     = {
        {
            .protection = 0,
            .offset     = 0,
            .size       = 0x0,
        },
        {
            .protection = 1,
            .offset     = 0x3000,
            .size       = 0x1000,
        },
        {
            .protection = 2,
            .offset     = 0x2000,
            .size       = 0x2000,
        },
        {
            .protection = 3,
            .offset     = 0x0,
            .size       = 0x4000,
        }
    },
};

/**
 * \brief Initialize the EEPROM driver/interface.
 *
 * \param eeprom specifies pointer to the \ref Ifx_At25xxx object. Values will be populated by this function
 * \param config specifies pointer to the configuration structure
 *
 * \ingroup library_srvsw_sysse_ext_at25xxx
 * FIXME missing initConfig()
 */
boolean Ifx_At25xxx_init(Ifx_At25xxx *eeprom, const Ifx_At25xxx_Config *config)
{
    boolean result = TRUE;

    eeprom->device      = config->device;
    eeprom->sscChannel  = config->sscChannel;
    eeprom->byteTimeout = (Ifx_TickTime)((TimeConst_1s * (1.0 / eeprom->sscChannel->baudrate)) * 10) + TimeConst_10ms;
    eeprom->wpn         = TRUE;
    return result;
}


static boolean Ifx_At25xxx_sendCommand(Ifx_At25xxx *eeprom, uint8 command)
{
    boolean result = TRUE;
    uint8   sscBuffer[2];

    sscBuffer[0] = command;
    result       = (SpiIf_exchange(eeprom->sscChannel, sscBuffer, sscBuffer, 1) == SpiIf_Status_ok);

    if (result)
    {
        /* Wait transfer */
        result = SpiIf_waitOrTimeout(eeprom->sscChannel, TimeConst_100us + eeprom->byteTimeout * 1);
    }

    return result;
}


static boolean Ifx_At25xxx_waitReady(Ifx_At25xxx *eeprom)
{
    Ifx_TickTime deadline = getDeadLine(IFX_CFG_AT25XXX_TIMEOUT_WR);
    boolean      result   = TRUE;
    uint8        sscBuffer[3];
    boolean      Busy     = TRUE;

    do
    {
        sscBuffer[0] = IFX_AT25XXX_CMD_READ_STATUS;
        sscBuffer[1] = 0;
        result       = (SpiIf_exchange(eeprom->sscChannel, sscBuffer, sscBuffer, 2) == SpiIf_Status_ok);

        if (result)
        {
            /* Wait transfer */
            result = SpiIf_waitOrTimeout(eeprom->sscChannel, TimeConst_100us + eeprom->byteTimeout * 2);

            if (result)
            {
                Busy                       = (sscBuffer[1] & IFX_AT25XXX_STATUS_BUSY) == 1;
                eeprom->statusRegister.reg = sscBuffer[1];
            }
        }
    } while (result && Busy && (!isDeadLine(deadline)));

    return !Busy;
}


static boolean Ifx_At25xxx_writeStatusRegister(Ifx_At25xxx *eeprom, Ifx_At25xxx_StatusRegister value)
{
    boolean result = TRUE;
    uint8   sscBuffer[2];

    sscBuffer[0] = IFX_AT25XXX_CMD_WRITE_STATUS;
    sscBuffer[1] = value.reg;
    result       = (SpiIf_exchange(eeprom->sscChannel, sscBuffer, sscBuffer, 2) == SpiIf_Status_ok);

    if (result)
    {
        /* Wait transfer */
        result = SpiIf_waitOrTimeout(eeprom->sscChannel, TimeConst_100us + eeprom->byteTimeout * 2);

        if (result)
        {
            result = Ifx_At25xxx_waitReady(eeprom);
        }
    }

    return result;
}


/**
 * \brief Read from the EEPROM
 *
 * \param eeprom specifies pointer to the \ref Ifx_At25xxx object.
 * \param addr is the internal address of the data to read from the EEPROM
 * \param data is the pointer to the destination buffer. Must be enough to store length
 * bytes of data
 * \param length is the read length.
 *
 * \return TRUE if success, else see the \ref Ifx_At25xxx_Status values
 *
 * \ingroup library_srvsw_sysse_ext_at25xxx
 */
boolean Ifx_At25xxx_read(Ifx_At25xxx *eeprom, Ifx_At25xxx_Address addr, uint8 *data, Ifx_At25xxx_Address length)
{
    boolean             result;
    uint8               sscBuffer[IFX_AT25XXX_CMD_LEN_READ + IFX_AT25XXX_READ_BLOCK_SIZE];
    uint16              bufferSize = 0;
    Ifx_At25xxx_Address remaining  = length;
    Ifx_At25xxx_Address nextAddr   = addr;

    bufferSize = __min(length, IFX_AT25XXX_READ_BLOCK_SIZE);

    result     = Ifx_At25xxx_waitReady(eeprom);

    /* Transfer data in blocks */
    while ((result != FALSE) && (remaining > 0))
    {
        /* Set up buffer with command and address */
        sscBuffer[0] = IFX_AT25XXX_CMD_READ_DATA;
        sscBuffer[1] = (uint8)((nextAddr & 0xFF00) >> 8);
        sscBuffer[2] = (uint8)(nextAddr & 0x00FF);

        /* Read in block */
        result = (SpiIf_exchange(eeprom->sscChannel, sscBuffer, sscBuffer,
                      IFX_AT25XXX_CMD_LEN_READ + bufferSize) == SpiIf_Status_ok);

        if (result == FALSE)
        {
            IFX_AT25XXX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, FALSE);
        }
        else
        {
            /* Wait transfer */
            result     = SpiIf_waitOrTimeout(eeprom->sscChannel, TimeConst_100us + eeprom->byteTimeout * (IFX_AT25XXX_CMD_LEN_READ + bufferSize));
            memcpy(&data[length - remaining], &sscBuffer[3], bufferSize);
            remaining -= bufferSize;
            nextAddr  += bufferSize;
            bufferSize = __min(remaining, IFX_AT25XXX_READ_BLOCK_SIZE);
        }
    }

    return result;
}


/**
 * \brief Write data to the EEPROM
 *
 * \param eeprom specifies pointer to the \ref Ifx_At25xxx object.
 * \param addr is the internal address of the EEPROM to write
 * \param data is the pointer to the data source buffer.
 * \param length is the write length.
 *
 * \return TRUE if success
 *
 * \ingroup library_srvsw_sysse_ext_at25xxx
 */
boolean Ifx_At25xxx_write(Ifx_At25xxx *eeprom, Ifx_At25xxx_Address addr, const uint8 *data, Ifx_At25xxx_Address length)
{
    boolean             result;                /* Return code */
    uint8               sscBuffer[IFX_AT25XXX_CMD_LEN_WRITE + IFX_AT25XXX_READ_BLOCK_SIZE];
    //uint16  bufferSize = 0;             /* Size of buffer used for transfers */
    Ifx_At25xxx_Address remaining    = length; /* Number of bytes remaining to be transferred */
    uint16              transferSize = 0;      /* Number of bytes being transferred in this block */
    Ifx_At25xxx_Address nextAddr     = addr;   /* Place to read data from next */

    /* Writing is optimised to write as many bytes as possible in a "page" in the EEPROM.  */
    /* For instance, the AT25128 supports up to 64 bytes being written as it automatically */
    /* increments the address register after each write.  However, only 6 bits are updated */
    /* and so care must be taken when using this!                                          */

    /* Calculate size of buffer needed */
    //bufferSize = __min(length, IFX_AT25XXX_PAGE_SIZE);

    /* First phase is to transfer all possible data in this "page" */
    transferSize = __min(remaining, IFX_AT25XXX_PAGE_SIZE - (nextAddr & (IFX_AT25XXX_PAGE_SIZE - 1)));

    /* Transfer data in blocks */
    do
    {
        result = Ifx_At25xxx_waitReady(eeprom);

        if (result != FALSE)
        {
            /* Send write enable command */
            sscBuffer[0] = IFX_AT25XXX_CMD_WRITE_ENABLE;
            result       = (SpiIf_exchange(eeprom->sscChannel, sscBuffer, sscBuffer, 1) == SpiIf_Status_ok);

            if (result == FALSE)
            {
                IFX_AT25XXX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, FALSE);
            }
            else
            {
                result = SpiIf_waitOrTimeout(eeprom->sscChannel, TimeConst_100us + eeprom->byteTimeout * (1));

                if (result == FALSE)
                {
                    IFX_AT25XXX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, FALSE);
                }
                else
                {
                    /* Set up buffer with command and address */
                    sscBuffer[0] = IFX_AT25XXX_CMD_WRITE_DATA;
                    sscBuffer[1] = (uint8)((nextAddr & 0xFF00) >> 8);
                    sscBuffer[2] = (uint8)(nextAddr & 0x00FF);

                    /* Copy data into buffer and write it to EEPROM */
                    memcpy(&sscBuffer[3], &data[length - remaining], transferSize);
                    result = (SpiIf_exchange(eeprom->sscChannel, sscBuffer, sscBuffer,
                                  IFX_AT25XXX_CMD_LEN_WRITE + transferSize) == SpiIf_Status_ok);

                    if (result == FALSE)
                    {
                        IFX_AT25XXX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, FALSE);
                    }
                    else
                    {
                        /* Wait transfer */
                        result = SpiIf_waitOrTimeout(eeprom->sscChannel, TimeConst_100us + eeprom->byteTimeout * (IFX_AT25XXX_CMD_LEN_WRITE + transferSize));

                        /* data checked (if requested) - move to next block */
                        remaining   -= transferSize;
                        nextAddr    += transferSize;
                        transferSize = __min(remaining, IFX_AT25XXX_PAGE_SIZE);
                    }
                }
            }
        }
    } while ((result != FALSE) && (transferSize > 0));

    return result;
}


/**
 * \brief Compare memory data with data inside the EEPROM
 *
 * \param eeprom specifies pointer to the \ref Ifx_At25xxx object.
 * \param addr is the internal address of the data inside the EEPROM
 * \param data is the pointer to reference data.
 * \param length is the compare length.
 *
 * \return TRUE if success
 *
 * \ingroup library_srvsw_sysse_ext_at25xxx
 */
boolean Ifx_At25xxx_compare(Ifx_At25xxx *eeprom, Ifx_At25xxx_Address addr, const uint8 *data, Ifx_At25xxx_Address length)
{
    boolean             result;
    uint8               sscBuffer[IFX_AT25XXX_CMD_LEN_READ + IFX_AT25XXX_READ_BLOCK_SIZE];
    uint16              bufferSize = 0;
    Ifx_At25xxx_Address remaining  = length;
    Ifx_At25xxx_Address nextAddr   = addr;

    bufferSize = __min(length, IFX_AT25XXX_READ_BLOCK_SIZE);

    result     = Ifx_At25xxx_waitReady(eeprom);

    /* Transfer data in blocks */
    while ((result != FALSE) && (remaining > 0))
    {
        /* Set up buffer with command and address */
        sscBuffer[0] = IFX_AT25XXX_CMD_READ_DATA;
        sscBuffer[1] = (uint8)((nextAddr & 0xFF00) >> 8);
        sscBuffer[2] = (uint8)(nextAddr & 0x00FF);

        /* Read in block */
        result = (SpiIf_exchange(eeprom->sscChannel, sscBuffer, sscBuffer, IFX_AT25XXX_CMD_LEN_READ + bufferSize) == SpiIf_Status_ok);

        if (result == FALSE)
        {
            IFX_AT25XXX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, FALSE);
        }
        else
        {
            /* Wait transfer */
            result = SpiIf_waitOrTimeout(eeprom->sscChannel, TimeConst_100us + eeprom->byteTimeout * (IFX_AT25XXX_CMD_LEN_READ + bufferSize));

            if (0 != memcmp(&data[length - remaining], &sscBuffer[3], bufferSize))
            {
                /* differences detected */
                result = FALSE;
            }
            else
            {
                remaining -= bufferSize;
                nextAddr  += bufferSize;
                bufferSize = __min(remaining, IFX_AT25XXX_READ_BLOCK_SIZE);
            }
        }
    }

    return result;
}


void Ifx_At25xxx_onDataEvent(void *data)
{
    Ifx_At25xxx *eeprom = (Ifx_At25xxx *)data;
    eeprom->onSscTransfer = FALSE;
}


void Ifx_At25xxx_onErrorEvent(void *data)
{
    Ifx_At25xxx *eeprom = (Ifx_At25xxx *)data;
    eeprom->onSscTransfer = FALSE;
}


/** Fill in the eeprom with zero value
 *
 * \param eeprom specifies pointer to the \ref Ifx_At25xxx object.
 * \param addr is the internal address of the EEPROM to erase
 * \param length is the erase length.
 * \param pattern Pattern byte to be written to the memory location during erase process
 *
 * \return TRUE if success
 *
 */
boolean Ifx_At25xxx_erase(Ifx_At25xxx *eeprom, Ifx_At25xxx_Address addr, Ifx_At25xxx_Address length, uint8 pattern)
{
    uint8               data[64];
    Ifx_At25xxx_Address size;
    memset(data, pattern, sizeof(data));

    while (length > 0)
    {
        size = length > sizeof(data) ? sizeof(data) : length;

        if (!Ifx_At25xxx_write(eeprom, addr, data, size))
        {
            break;
        }
        else
        {}

        addr   += size;
        length -= size;
    }

    return length == 0;
}


void Ifx_At25xxx_setWpn(Ifx_At25xxx *eeprom, boolean state)
{
    eeprom->wpn = state;
    /* FIXME add support for port pin */
}


boolean Ifx_At25xxx_getProtection(Ifx_At25xxx *eeprom, uint8 *protectionIndex, Ifx_At25xxx_ProtectionState *protectedBlocks, Ifx_At25xxx_ProtectionState *unprotectedBlocks, Ifx_At25xxx_ProtectionState *statusRegister)
{
    boolean result;
    result = Ifx_At25xxx_waitReady(eeprom);

    if (result)
    {
        *protectionIndex = eeprom->statusRegister.bits.bp;

        if (eeprom->statusRegister.bits.wpen)
        {   /* WPn pin enabled */
            if (!eeprom->wpn)
            {
                if (eeprom->statusRegister.bits.wen)
                {
                    *protectedBlocks   = Ifx_At25xxx_ProtectionState_protected;
                    *unprotectedBlocks = Ifx_At25xxx_ProtectionState_unprotected;
                    *statusRegister    = Ifx_At25xxx_ProtectionState_protected;
                }
                else
                {
                    *protectedBlocks   = Ifx_At25xxx_ProtectionState_protected;
                    *unprotectedBlocks = Ifx_At25xxx_ProtectionState_protected;
                    *statusRegister    = Ifx_At25xxx_ProtectionState_protected;
                }
            }
            else
            {
                if (eeprom->statusRegister.bits.wen)
                {
                    *protectedBlocks   = Ifx_At25xxx_ProtectionState_protected;
                    *unprotectedBlocks = Ifx_At25xxx_ProtectionState_unprotected;
                    *statusRegister    = Ifx_At25xxx_ProtectionState_unprotected;
                }
                else
                {
                    *protectedBlocks   = Ifx_At25xxx_ProtectionState_protected;
                    *unprotectedBlocks = Ifx_At25xxx_ProtectionState_protected;
                    *statusRegister    = Ifx_At25xxx_ProtectionState_protected;
                }
            }
        }
        else
        {   /* WPn pin disabled */
            if (eeprom->statusRegister.bits.wen)
            {
                *protectedBlocks   = Ifx_At25xxx_ProtectionState_protected;
                *unprotectedBlocks = Ifx_At25xxx_ProtectionState_unprotected;
                *statusRegister    = Ifx_At25xxx_ProtectionState_unprotected;
            }
            else
            {
                *protectedBlocks   = Ifx_At25xxx_ProtectionState_protected;
                *unprotectedBlocks = Ifx_At25xxx_ProtectionState_protected;
                *statusRegister    = Ifx_At25xxx_ProtectionState_protected;
            }
        }
    }

    return result;
}


boolean Ifx_At25xxx_unprotectStatus(Ifx_At25xxx *eeprom)
{
    boolean                     result = TRUE;
    Ifx_At25xxx_StatusRegister  status;
    uint8                       protectionIndex;
    Ifx_At25xxx_ProtectionState protectedBlocks;
    Ifx_At25xxx_ProtectionState unprotectedBlocks;
    Ifx_At25xxx_ProtectionState statusRegister;

    result &= Ifx_At25xxx_sendCommand(eeprom, IFX_AT25XXX_CMD_WRITE_ENABLE);
    result &= Ifx_At25xxx_getProtection(eeprom, &protectionIndex, &protectedBlocks, &unprotectedBlocks, &statusRegister);

    if (statusRegister == Ifx_At25xxx_ProtectionState_protected)
    {
        if (!eeprom->wpn)
        {
            result = FALSE; /* Can't un-protect the EEPROM, WPn pin protection*/
        }
        else
        {
            status           = eeprom->statusRegister;
            status.bits.wpen = 0;
            result          &= Ifx_At25xxx_writeStatusRegister(eeprom, status);
            result          &= Ifx_At25xxx_getProtection(eeprom, &protectionIndex, &protectedBlocks, &unprotectedBlocks, &statusRegister);
        }
    }

    result &= statusRegister == Ifx_At25xxx_ProtectionState_unprotected;
    return result;
}


/**
 *  \param enableWpn if TRUE, the WPn pin protection is enabled else disabled
 */
boolean Ifx_At25xxx_setProtection(Ifx_At25xxx *eeprom, boolean enableWpn, uint8 protectionIndex, Ifx_At25xxx_ProtectionState protectedBlocks, Ifx_At25xxx_ProtectionState unprotectedBlocks, Ifx_At25xxx_ProtectionState statusRegister)
{
    boolean                     result = TRUE;
    uint8                       pIndex;
    Ifx_At25xxx_StatusRegister  status;
    Ifx_At25xxx_ProtectionState pBlocks;
    Ifx_At25xxx_ProtectionState uBlocks;
    Ifx_At25xxx_ProtectionState sRegister;

    result &= Ifx_At25xxx_getProtection(eeprom, &pIndex, &pBlocks, &uBlocks, &sRegister);

    if (result)
    {
        status = eeprom->statusRegister;

        if ((pIndex == protectionIndex) && (pBlocks == protectedBlocks) && (uBlocks == unprotectedBlocks) && (sRegister == statusRegister) && (status.bits.wpen == enableWpn))
        {
            /* Setting are already as requested */
        }
        else
        {
            result &= Ifx_At25xxx_unprotectStatus(eeprom);

            if (result)
            {
                status           = eeprom->statusRegister;
                status.bits.wpen = enableWpn ? 1 : 0;
                status.bits.bp   = protectedBlocks == Ifx_At25xxx_ProtectionState_protected ? protectionIndex : 0;
                protectionIndex  = status.bits.bp;
                //eeprom->wpn = statusRegister == Ifx_At25xxx_ProtectionState_protected ? 0 : 1; ==> FIXME this is to be added if pin can be controlled
                result          &= Ifx_At25xxx_writeStatusRegister(eeprom, status);
            }

            if ((unprotectedBlocks == Ifx_At25xxx_ProtectionState_protected) && (statusRegister == Ifx_At25xxx_ProtectionState_protected))
            {
                result &= Ifx_At25xxx_sendCommand(eeprom, IFX_AT25XXX_CMD_WRITE_DISABLE);
            }
        }
    }

    result &= Ifx_At25xxx_getProtection(eeprom, &pIndex, &pBlocks, &uBlocks, &sRegister);
    status  = eeprom->statusRegister;
    result &= (pIndex == protectionIndex) && (pBlocks == protectedBlocks) && (uBlocks == unprotectedBlocks) && (sRegister == statusRegister) && (status.bits.wpen == enableWpn);
    return result;
}


boolean Ifx_At25xxx_printStatus(Ifx_At25xxx *eeprom, IfxStdIf_DPipe *io)
{
    boolean                     result = TRUE;
    uint8                       pIndex;
    Ifx_At25xxx_ProtectionState pBlocks;
    Ifx_At25xxx_ProtectionState uBlocks;
    Ifx_At25xxx_ProtectionState sRegister;

    result &= Ifx_At25xxx_getProtection(eeprom, &pIndex, &pBlocks, &uBlocks, &sRegister);

    IfxStdIf_DPipe_print(io, ENDL);
    IfxStdIf_DPipe_print(io, "EEPROM status"ENDL);
    IfxStdIf_DPipe_print(io, "- Device type: %s"ENDL, eeprom->device->type);
    IfxStdIf_DPipe_print(io, "- WPn pin protection: %s"ENDL, eeprom->wpn ? "Disabled" : "Enabled");

    if (result)
    {
        if (eeprom->statusRegister.bits.bp != 0)
        {
            IfxStdIf_DPipe_print(io, "- Protected range   : %X-%X"ENDL,
                eeprom->device->protectionZones[eeprom->statusRegister.bits.bp].offset,
                eeprom->device->protectionZones[eeprom->statusRegister.bits.bp].offset + eeprom->device->protectionZones[eeprom->statusRegister.bits.bp].size - 1);
            IfxStdIf_DPipe_print(io, "- Protected blocks  : %s"ENDL, pBlocks == Ifx_At25xxx_ProtectionState_protected ? "protected" : "unprotected");
            IfxStdIf_DPipe_print(io, "- Unprotected blocks: %s"ENDL, uBlocks == Ifx_At25xxx_ProtectionState_protected ? "protected" : "unprotected");
            IfxStdIf_DPipe_print(io, "- Status register   : %s"ENDL, sRegister == Ifx_At25xxx_ProtectionState_protected ? "protected" : "unprotected");
        }
        else
        {
            IfxStdIf_DPipe_print(io, "- No protected blocks"ENDL);
        }

        IfxStdIf_DPipe_print(io, "- Status register value: %x"ENDL, eeprom->statusRegister.reg);
    }
    else
    {
        IfxStdIf_DPipe_print(io, "- ERROR: can't access EEPROM"ENDL);
    }

    return result;
}
