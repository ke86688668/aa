/**
 * \file Ifx_Efs.h
 * \brief EEPROM file system.
 *
 * This driver enables saving multiple files of variable size on the EEPROM.
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_ext_at25xxx_efs File System for Ifx_At25xxx
 * \ingroup library_srvsw_sysse_ext_at25xxx
 *
 *  FIXME make the EFS hardware independant
 */

//---------------------------------------------------------------------------
#ifndef IFX_EFS_H
#define IFX_EFS_H
//---------------------------------------------------------------------------
#include "Ifx_At25xxx.h"
#include "Ifx_Cfg.h"
#include "SysSe/Math/Ifx_Crc.h"
//---------------------------------------------------------------------------

#ifndef IFX_CFG_EFS_MAX_FILE_COUNT
/** Maximal number of file supported by the SW driver.
 * This value is limited by the type of Ifx_Efs_FileIndex */
#define IFX_CFG_EFS_MAX_FILE_COUNT              ((Ifx_Efs_FileIndex)255)
#endif

#ifndef IFX_CFG_EFS_DEFAULT_ALLOCATED_FILE_SIZE
/** \brief Default memory allocated for a file when created */
#define IFX_CFG_EFS_DEFAULT_ALLOCATED_FILE_SIZE (sizeof(Ifx_Efs_FileHeader) + 128)
#endif

#define IFX_CFG_EFS_FILENAME_SIZE               (16) /* Size of the filename field in byte */
#define IFX_EFS_ID_SIZE                         (16) /* Size of the EFS id field in byte */

#define IFX_EFS_FILE_OFFSET_ITEM_INVALID        ((Ifx_Efs_FileOffsetItem)0xFFFF)
#define IFX_EFS_FILE_INDEX_INVALID              ((Ifx_Efs_FileIndex)0xFFFF)

#ifndef IFX_CFG_EFS_DEFAULT_FILE_DATA_USE_CRC
/** Default setting for file data CRC */
#define IFX_CFG_EFS_DEFAULT_FILE_DATA_USE_CRC   (1)
#endif

typedef uint16             Ifx_Efs_FileOffsetItem; /* File offset item. Offset address is fileOffsets[n] << 4. offset is from Ifx_Efs.tableOffset. Max memory size 1MB */
typedef uint32             Ifx_Efs_Address;        /* Address in the eeprom */
typedef uint16             Ifx_Efs_FileIndex;      /* file index in the Ifx_Efs_FileOffsetItem[] array. IFX_EFS_FILE_INDEX_INVALID represent an invalid index */
typedef uint16             Ifx_Efs_FileOffset;     /* Address in the file */
typedef Ifx_Efs_FileOffset Ifx_Efs_FileSize;       /* File size */
typedef uint16             Ifx_Efs_PartitionSize;  /* Partition size */
typedef uint16             Ifx_Efs_Crc;            /* CRC data type. This type shall not be changed in future version of the driver */
typedef uint16             Ifx_Efs_Version;        /* EFS version data type. This type shall not be changed in future version of the driver */

/** The file table is located at the offset defined by the 32bit value at address 0.
 */
typedef struct
{
    uint16                headerSize;                   /* Size of the block Ifx_Efs_TableHeader. This member shall not be changed (size, offset) in future version of the driver */
    Ifx_Efs_Crc           crc;                          /* CRC of the block Ifx_Efs_TableHeader. This member shall not be changed (size, offset) in future version of the driver */
    Ifx_Efs_Version       efsVersion;                   /* File table version. This member shall not be changed (size, offset) in future version of the driver */
    uint8                 fileTableId[IFX_EFS_ID_SIZE]; /* data that identifies the eeprom file system to be valid */

    Ifx_Efs_FileIndex     maxFileCount;                 /* size of the array fileOffsets */
    Ifx_Efs_Crc           fileOffsetItemsCrc;           /* CRC of the block Ifx_Efs_TableHeader, with the Ifx_Efs_FileOffsetItem[] array */
    Ifx_Efs_PartitionSize partitionSize;                /* Size of the partition in bytes */
    Ifx_Efs_FileSize      defaultAllocatedFileSize;     /* Default allocated file size if Ifx_Efs_TableHeader.flags.dynamicFileSize=0 */
    struct
    {
        uint16 dynamicFileSize : 1;
        uint16 filenameZes     : 1;                     /* Zero ending file name. if true, a zero terminates the filename, else zero are part of the filename */
        uint16 reserved        : 14;
    }flags;
} Ifx_Efs_TableHeader;

typedef struct
{
    Ifx_Efs_TableHeader    header;
    Ifx_Efs_FileOffsetItem fileOffsetItems[IFX_CFG_EFS_MAX_FILE_COUNT];  /* File offset item */
} Ifx_Efs_Table;

/**
 * EFS memory map for static file size (dynamicFileSize=0)
 *
 * EFS_HEADER = 0
 * EFS_FILE_TABLE = sizeof(Ifx_Efs_TableHeader)
 * EFS_FILE_BASE = sizeof(Ifx_Efs_TableHeader) + sizeof(Ifx_Efs_FileOffsetItem) * efs->table.header.maxFileCount
 * offset        | Data
 * --------------|------
 * EFS_HEADER    | Ifx_Efs_TableHeader
 * EFS_FILE_TABLE| Ifx_Efs_FileOffsetItem[]
 * EFS_FILE_BASE | (Ifx_Efs_FileHeader + data)[]
 */
typedef struct
{
    Ifx_Efs_Table     table;
    Ifx_Efs_Address   tableOffset;
    Ifx_At25xxx      *eeprom;
    Ifc_Crc           crc;
    Ifx_Efs_FileIndex dirIndex;
    struct
    {
        uint8 ready                 : 1;         /* The Ifx_Efs is ready to be used */
        uint8 loaded                : 1;         /* The Ifx_Efs_Table has been loaded from the EEPROM */
        uint8 reserved              : 5;
    } status;
    union
    {
        struct
        {
            uint16 version          : 1;        /* The Ifx_Efs_Table exists but the table version does not match the driver version */
            uint16 communication    : 1;        /* Error during communication with the eeprom */
            uint16 crcHeader        : 1;        /* the Ifx_Efs_TableHeader crc is incorrect */
            uint16 crcTable         : 1;        /* the Ifx_Efs_TableHeader, with the Ifx_Efs_FileOffsetItem[] array crc is incorrect */
            uint16 maxFileCount     : 1;        /* The number of file is limited by The IFX_CFG_EFS_MAX_FILE_COUNT */
            uint16 writeOutOfMemory : 1;        /* Write access out of the available memory */
            uint16 readOutOfMemory  : 1;        /* Read access out of the available memory */
            uint16 partitionSizeMax : 1;        /* Partition size exceed the available memory */
            uint16 offsetOutOfMemory : 1;       /* EFS offset locate the EFS out of the available memory */
            uint16 fileSizeOverflow : 1;        /* The file size exceed the maximum allowed size */
            uint16 partitionSizeMin : 1;        /* The partition size is too small  */
            uint16 invalidAction    : 1;
            uint16 efsIdNotPresent  : 1;
        }      B;
        uint16 U;
    } error;
} Ifx_Efs;

typedef struct
{
    char             filename[IFX_CFG_EFS_FILENAME_SIZE];
    uint16           headerSize;                        /* Size of the block Ifx_Efs_FileHeader */
    Ifx_Efs_FileSize allocatedFileSize;                 /* Memory allocated for the file, all the memory might not be used by data */
    Ifx_Efs_Crc      crc;                               /* CRC of the block Ifx_Efs_FileHeader */
    struct
    {
        uint16 hasCrc          : 1;
        uint16 isDirectory     : 1;
        uint16 reserved        : 14;
    }                flags;
    Ifx_Efs_FileSize dataSize;                              /* Size of the file data in byte (effectively used memory) */
    Ifx_Efs_Crc      dataCrc;                               /* CRC for the data */
} Ifx_Efs_FileHeader;

typedef struct
{
    Ifx_Efs           *efs;
    Ifx_Efs_FileHeader header;
    Ifx_Efs_FileIndex  fileIndex;
    struct
    {
        uint8 fileNotFound    : 1;
        uint8 opened          : 1;
        uint8 modified        : 1;
        uint8 reserved        : 6;
    } status;
} Ifx_Efs_File;

/** \addtogroup library_srvsw_sysse_ext_at25xxx_efs
 * \{ */

IFX_EXTERN Ifx_Efs_FileSize      Ifx_Efs_fileMaxDataSizeGet(Ifx_Efs_File *file);
IFX_EXTERN void                  Ifx_Efs_Init(Ifx_Efs *efs, Ifx_At25xxx *eeprom);
IFX_EXTERN boolean               Ifx_Efs_partitionSizeSet(Ifx_Efs *efs, Ifx_Efs_PartitionSize partitionSize);
IFX_EXTERN Ifx_Efs_PartitionSize Ifx_Efs_partitionSizeGet(Ifx_Efs *efs);
IFX_EXTERN boolean               Ifx_Efs_maxFileCountSet(Ifx_Efs *efs, Ifx_Efs_FileIndex maxFileCount);
IFX_EXTERN Ifx_Efs_FileIndex     Ifx_Efs_maxFileCountGet(Ifx_Efs *efs);
IFX_EXTERN boolean               Ifx_Efs_tableOffsetSet(Ifx_Efs *efs, Ifx_Efs_Address offset);
IFX_EXTERN Ifx_Efs_Address       Ifx_Efs_tableOffsetGet(Ifx_Efs *efs);
IFX_EXTERN boolean               Ifx_Efs_mount(Ifx_Efs *efs);
IFX_EXTERN boolean               Ifx_Efs_isLoaded(Ifx_Efs *efs);
IFX_EXTERN boolean               Ifx_Efs_formatFast(Ifx_Efs *efs);
IFX_EXTERN boolean               Ifx_Efs_format(Ifx_Efs *efs);
IFX_EXTERN void                  Ifx_Efs_dirInit(Ifx_Efs *efs);
IFX_EXTERN boolean               Ifx_Efs_dir(Ifx_Efs *efs, Ifx_Efs_File *file);
IFX_EXTERN boolean               Ifx_Efs_fileDelete(Ifx_Efs *efs, pchar filename);
IFX_EXTERN boolean               Ifx_Efs_fileOpen(Ifx_Efs *efs, pchar filename, Ifx_Efs_File *file);
IFX_EXTERN boolean               Ifx_Efs_fileClose(Ifx_Efs_File *file);
IFX_EXTERN boolean               Ifx_Efs_fileCreate(Ifx_Efs *efs, pchar filename, Ifx_Efs_File *file);
IFX_EXTERN boolean               Ifx_Efs_fileDataCrcSet(Ifx_Efs_File *file, uint8 *data);
IFX_EXTERN boolean               Ifx_Efs_fileDataCrcCheck(Ifx_Efs_File *file, uint8 *data);
IFX_EXTERN void                  Ifx_Efs_fileDataCrcEnable(Ifx_Efs_File *file);
IFX_EXTERN void                  Ifx_Efs_fileDataCrcDisable(Ifx_Efs_File *file);
IFX_EXTERN boolean               Ifx_Efs_fileDataIsCrc(Ifx_Efs_File *file);
IFX_EXTERN Ifx_Efs_Crc           Ifx_Efs_fileDataCrcGet(Ifx_Efs_File *file);
IFX_EXTERN boolean               Ifx_Efs_fileSizeSet(Ifx_Efs_File *file, Ifx_Efs_FileSize size);
IFX_EXTERN Ifx_Efs_FileSize      Ifx_Efs_fileSizeGet(Ifx_Efs_File *file);
IFX_EXTERN Ifx_Efs_FileOffset    Ifx_Efs_fileRead(Ifx_Efs_File *file, Ifx_Efs_FileOffset offset, Ifx_Efs_FileOffset length, uint8 *buffer);
IFX_EXTERN Ifx_Efs_FileOffset    Ifx_Efs_fileWrite(Ifx_Efs_File *file, Ifx_Efs_FileOffset offset, Ifx_Efs_FileOffset length, const uint8 *buffer);
IFX_EXTERN boolean               Ifx_Efs_fileExist(Ifx_Efs *efs, pchar filename);
IFX_EXTERN boolean               Ifx_Efs_filenameZesSet(Ifx_Efs *efs, boolean value);
IFX_EXTERN boolean               Ifx_Efs_filenameZesGet(Ifx_Efs *efs);
IFX_EXTERN boolean               Ifx_Efs_defaultAllocatedFileSizeSet(Ifx_Efs *efs, Ifx_Efs_FileSize size);
IFX_EXTERN Ifx_Efs_FileSize      Ifx_Efs_defaultAllocatedFileSizeGet(Ifx_Efs *efs);
IFX_EXTERN boolean               Ifx_Efs_defaultFileSizeSet(Ifx_Efs *efs, Ifx_Efs_FileSize size);
IFX_EXTERN Ifx_Efs_FileSize      Ifx_Efs_defaultFileSizeGet(Ifx_Efs *efs);
IFX_EXTERN boolean               Ifx_Efs_erase(Ifx_Efs *efs);
IFX_EXTERN boolean               Ifx_Efs_unmount(Ifx_Efs *efs);
Ifx_Efs_Address                  Ifx_Efs_FirstFileOffsetGet(Ifx_Efs *efs);
Ifx_Efs_Address                  Ifx_Efs_getMinPartitionSize(Ifx_Efs *efs, Ifx_Efs_FileIndex maxFileCount);
Ifx_Efs_FileIndex                Ifx_Efs_getBestMaxFileCount(Ifx_Efs *efs);

/** \} */
//---------------------------------------------------------------------------
#endif
