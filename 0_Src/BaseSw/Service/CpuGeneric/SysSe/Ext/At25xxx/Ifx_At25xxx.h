/**
 * \file Ifx_At25xxx.h
 * FIXME To be renamed to AT128N
 * \brief EEPROM interface.
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_ext_at25xxx AT25xxx Serial EEPROM
 * \ingroup library_srvsw_sysse_ext
 */
#ifndef Ifx_AT25XXX_H
#define Ifx_AT25XXX_H

#include "If/SpiIf.h"
#include "Ifx_Cfg.h"
#include "StdIf/IfxStdIf_DPipe.h"

#ifndef IFX_CFG_AT25XXX_TIMEOUT_WR
#define IFX_CFG_AT25XXX_TIMEOUT_WR (TimeConst_10ms)
#endif

typedef uint32 Ifx_At25xxx_Address;         /* Address in the eeprom */
typedef union
{
    struct
    {
        uint8 rdyn : 1;                      /**< \brief RDYn */
        uint8 wen : 1;                       /**< \brief WEN */
        uint8 bp  : 2;                       /**< \brief BP0, BP1 */
        uint8 x   : 3;                       /**< \brief X */
        uint8 wpen : 1;                      /**< \brief WPEN */
    }     bits;
    uint8 reg;
}Ifx_At25xxx_StatusRegister;

typedef struct
{
    pchar  type;
    uint32 size;
    uint8  protectionZoneCount;
    struct
    {
        uint8  protection;
        uint32 offset;
        uint32 size;
    }protectionZones[4];
}Ifx_At25xxx_Device;

/**
 * \brief EEPROM configuration structure passed through Ifx_At25xxx_init()
 */
typedef struct
{
    SpiIf_Ch           *sscChannel;     /**< \brief Microcontroller's SPI channel used */
    const Ifx_At25xxx_Device *device;
} Ifx_At25xxx_Config;

/**
 * \brief EEPROM driver data (object) structure
 */
typedef struct
{
	const Ifx_At25xxx_Device  *device;
    SpiIf_Ch                  *sscChannel;  /**< \brief Microcontroller's SPI channel used */
    volatile boolean           onSscTransfer;
    Ifx_At25xxx_StatusRegister statusRegister;
    Ifx_TickTime               byteTimeout; /**< \brief Timeout value for a single byte transfer*/
    boolean                    wpn;         /**< \brief WPn pin state */
} Ifx_At25xxx;

typedef enum
{
    Ifx_At25xxx_Protection_unprotectBlockAndStatus,     /**< \brief protected blocks and status register are unprotected */
    Ifx_At25xxx_Protection_protectBlockAndStatus,       /**< \brief protected blocks and status register are unprotected */
    Ifx_At25xxx_Protection_protectBlocksOnly,           /**< \brief protected blocks are protected, status register is unprotected */
}Ifx_At25xxx_Protection;

typedef enum
{
    Ifx_At25xxx_ProtectionState_unprotected,    /**< \brief block is unprotected */
    Ifx_At25xxx_ProtectionState_protected       /**< \brief block is protected */
}Ifx_At25xxx_ProtectionState;

IFX_EXTERN const Ifx_At25xxx_Device Ifx_g_At25xxx_at25256;
IFX_EXTERN const Ifx_At25xxx_Device Ifx_g_At25xxx_at25128;

IFX_EXTERN boolean Ifx_At25xxx_init(Ifx_At25xxx *eeprom, const Ifx_At25xxx_Config *config);

IFX_EXTERN boolean Ifx_At25xxx_read(Ifx_At25xxx *eeprom, Ifx_At25xxx_Address addr, uint8 *data, Ifx_At25xxx_Address length);

IFX_EXTERN boolean Ifx_At25xxx_compare(Ifx_At25xxx *eeprom, Ifx_At25xxx_Address addr, const uint8 *data, Ifx_At25xxx_Address length);

IFX_EXTERN boolean Ifx_At25xxx_write(Ifx_At25xxx *eeprom, Ifx_At25xxx_Address addr, const uint8 *data, Ifx_At25xxx_Address length);

IFX_EXTERN boolean Ifx_At25xxx_erase(Ifx_At25xxx *eeprom, Ifx_At25xxx_Address addr, Ifx_At25xxx_Address length, uint8 pattern);
IFX_EXTERN void    Ifx_At25xxx_setWpn(Ifx_At25xxx *eeprom, boolean state);
IFX_EXTERN boolean Ifx_At25xxx_getProtection(Ifx_At25xxx *eeprom, uint8 *protectionIndex, Ifx_At25xxx_ProtectionState *protectedBlocks, Ifx_At25xxx_ProtectionState *unprotectedBlocks, Ifx_At25xxx_ProtectionState *statusRegister);
/**
 *  \param enableWpn if TRUE, the WPn pin protection is enabled else disabled
 */
boolean Ifx_At25xxx_setProtection(Ifx_At25xxx *eeprom, boolean enableWpn, uint8 protectionIndex, Ifx_At25xxx_ProtectionState protectedBlocks, Ifx_At25xxx_ProtectionState unprotectedBlocks, Ifx_At25xxx_ProtectionState statusRegister);

boolean Ifx_At25xxx_printStatus(Ifx_At25xxx *eeprom, IfxStdIf_DPipe *io);

#endif /*Ifx_AT25XXX_H*/
