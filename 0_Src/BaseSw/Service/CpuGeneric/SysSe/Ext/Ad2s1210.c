/*
 * \file Ad2s1210.c
 * \brief Ad2s1210 resolver interface.
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

//________________________________________________________________________________________
// INCLUDES

#include "Ad2s1210.h"
#include "_Utilities/Ifx_Assert.h"
#include "SysSe/Comm/Ifx_Console.h"
#include <stddef.h>
#include "string.h"

//________________________________________________________________________________________
// LOCAL FUNCTION PROTOTYPES

sint32      Ad2s1210_resetOffset(Ad2s1210 *driver);
float32     Ad2s1210_updateSpeed(Ad2s1210 *driver, boolean update);
void        Ad2s1210_Serial_update(Ad2s1210 *driver);
void        Ad2s1210_Parallel_update(Ad2s1210 *driver);
void        Ad2s1210_ExtBus_update(Ad2s1210 *driver);
static void Ad2s1210_setupSpeedConst(Ad2s1210 *driver);

static uint8 Ad2s1210_readDataSerial(Ad2s1210 *driver);
static uint8 Ad2s1210_readDataParallel(Ad2s1210 *driver);
static uint8 Ad2s1210_readDataExtBus(Ad2s1210 *driver);
static uint8 Ad2s1210_writeDataSerial(Ad2s1210 *driver, uint8 data);
static uint8 Ad2s1210_writeDataParallel(Ad2s1210 *driver, uint8 data);
static uint8 Ad2s1210_writeDataExtBus(Ad2s1210 *driver, uint8 data);
static void  Ad2s1210_setMode(Ad2s1210 *driver, Ad2s1210_OperationMode mode);
static void  Ad2s1210_setResolution(Ad2s1210 *driver, Ad2s1210_Resolution resolution);

static boolean Ad2s1210_read(Ad2s1210 *driver, uint8 address, uint8 *data);
static boolean Ad2s1210_write(Ad2s1210 *driver, uint8 address, uint8 data);

//________________________________________________________________________________________
// VARIABLES

//________________________________________________________________________________________
// CONSTANTS

/* Register addresses */
#define AD2S1210_REG_ADDR_POSITION_H              (0x80)
#define AD2S1210_REG_ADDR_POSITION_L              (0x81)
#define AD2S1210_REG_ADDR_VELOCITY_H              (0x82)
#define AD2S1210_REG_ADDR_VELOCITY_L              (0x83)
#define AD2S1210_REG_ADDR_LOS_THRESHOLD           (0x88)
#define AD2S1210_REG_ADDR_DOS_OVERRANGE_THRESHOLD (0x89)
#define AD2S1210_REG_ADDR_DOS_MISMATCH_THRESHOLD  (0x8A)
#define AD2S1210_REG_ADDR_DOS_RESET_MAX_THRESHOLD (0x8B)
#define AD2S1210_REG_ADDR_DOS_RESET_MIN_THRESHOLD (0x8C)
#define AD2S1210_REG_ADDR_LOT_HIGH_THRESHOLD      (0x8D)
#define AD2S1210_REG_ADDR_LOT_LOW_THRESHOLD       (0x8E)
#define AD2S1210_REG_ADDR_EXCITATION_FREQUENCY    (0x91)
#define AD2S1210_REG_ADDR_CONTROL                 (0x92)
#define AD2S1210_REG_ADDR_SOFT_RESET              (0xF0)
#define AD2S1210_REG_ADDR_FAULT                   (0xFF)

typedef struct
{
    Ad2s1200_RegIdx index;
    uint8           address;
} Ad2s1210_RegItem;

typedef union
{
    Ad2s1210_RegItem A[AD2S1210_REGS_COUNT];
    struct
    {
        Ad2s1210_RegItem positionH;
        Ad2s1210_RegItem positionL;
        Ad2s1210_RegItem velocityH;
        Ad2s1210_RegItem velocityL;
        Ad2s1210_RegItem losThreshold;
        Ad2s1210_RegItem dosOverrangeThreshold;
        Ad2s1210_RegItem dosMismatchThreshold;
        Ad2s1210_RegItem dosResetMaxThreshold;
        Ad2s1210_RegItem dosResetMinThreshold;
        Ad2s1210_RegItem lotMaxThreshold;
        Ad2s1210_RegItem lotMinThreshold;
        Ad2s1210_RegItem excitationFrequency;
        Ad2s1210_RegItem control;
        Ad2s1210_RegItem softReset;
        Ad2s1210_RegItem fault;
    } I;
} Ad2s1210_RegTable;

const Ad2s1210_RegTable Ad2s1210_regTable =
{    {
         {.index = Ad2s1200_RegIdx_positionH,             .address = 0x80},
         {.index = Ad2s1200_RegIdx_positionL,             .address = 0x81},
         {.index = Ad2s1200_RegIdx_velocityH,             .address = 0x82},
         {.index = Ad2s1200_RegIdx_velocityL,             .address = 0x83},
         {.index = Ad2s1200_RegIdx_losThreshold,          .address = 0x88},
         {.index = Ad2s1200_RegIdx_dosOverrangeThreshold, .address = 0x89},
         {.index = Ad2s1200_RegIdx_dosMismatchThreshold,  .address = 0x8A},
         {.index = Ad2s1200_RegIdx_dosResetMaxThreshold,  .address = 0x8B},
         {.index = Ad2s1200_RegIdx_dosResetMinThreshold,  .address = 0x8C},
         {.index = Ad2s1200_RegIdx_lotMaxThreshold,       .address = 0x8D},
         {.index = Ad2s1200_RegIdx_lotMinThreshold,       .address = 0x8E},
         {.index = Ad2s1200_RegIdx_excitationFrequency,   .address = 0x91},
         {.index = Ad2s1200_RegIdx_control,               .address = 0x92},
         {.index = Ad2s1200_RegIdx_softReset,             .address = 0xF0},
         {.index = Ad2s1200_RegIdx_fault,                 .address = 0xFF},
     }};

/******************************************************************************/
/*-------------------------Function Implementations---------------------------*/
/******************************************************************************/
sint32 Ad2s1210_getResolutionBit(Ad2s1210_Resolution resolution)
{
    sint32 result = 0;

    switch (resolution)
    {
    case Ad2s1210_Resolution_10Bits:
        result = 1024;
        break;
    case Ad2s1210_Resolution_12Bits:
        result = 4096;
        break;
    case Ad2s1210_Resolution_14Bits:
        result = 16384;
        break;
    case Ad2s1210_Resolution_16Bits:
        result = 65536;
        break;
    }

    return result;
}

Ad2s1210_Resolution Ad2s1210_getResolutionCode(sint32 resolution)
{
	Ad2s1210_Resolution result;
	if(resolution >= 65536)
	{
		result = Ad2s1210_Resolution_16Bits;
	}
	else if(resolution >= 16384)
	{
		result = Ad2s1210_Resolution_14Bits;
	}
	else if(resolution >= 4096)
	{
		result = Ad2s1210_Resolution_12Bits;
	}
	else
	{
		result = Ad2s1210_Resolution_10Bits;
	}

    return result;
}


uint8 Ad2s1210_getResolutionDataBits(Ad2s1210_Resolution resolution)
{
    uint8 result = 0;

    switch (resolution)
    {
    case Ad2s1210_Resolution_10Bits:
        result = 10;
        break;
    case Ad2s1210_Resolution_12Bits:
        result = 12;
        break;
    case Ad2s1210_Resolution_14Bits:
        result = 14;
        break;
    case Ad2s1210_Resolution_16Bits:
        result = 16;
        break;
    }

    return result;
}


float32 Ad2s1210_getAbsolutePosition(Ad2s1210 *driver)
{
    return ((float32)driver->turn + (float32)driver->position / (float32)driver->resolution) * 2.0 * IFX_PI;
}


IfxStdIf_Pos_Dir Ad2s1210_getDirection(Ad2s1210 *driver)
{
    return driver->direction;
}


IfxStdIf_Pos_Status Ad2s1210_getFault(Ad2s1210 *driver)
{
    return driver->stdifStatus;
}


sint32 Ad2s1210_getOffset(Ad2s1210 *driver)
{
    return driver->offset;
}


uint16 Ad2s1210_getPeriodPerRotation(Ad2s1210 *driver)
{
    return driver->periodPerRotation;
}


float32 Ad2s1210_getRefreshPeriod(Ad2s1210 *driver)
{
    return 0.0; /* FIXME */
}


IfxStdIf_Pos_SensorType Ad2s1210_getSensorType(Ad2s1210 *driver)
{
    return IfxStdIf_Pos_SensorType_resolver;
}


float32 Ad2s1210_getSpeed(Ad2s1210 *driver)
{
    return driver->speed;
}


IFX_INLINE void Ad2s1210_initInputPort(const IfxPort_Pin *pin)
{
    Pin_setMode(pin, IfxPort_Mode_inputNoPullDevice);
    //Pin_setDriver(pin, IfxPort_PadDriver_strongSharp);
}


IFX_INLINE void Ad2s1210_initOutputPort(const IfxPort_Pin *pin, IfxPort_State initiallevel)
{
    Pin_setState(pin, initiallevel);
    Pin_setMode(pin, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(pin, PIN_DRIVER_STRONG_SHARP);
}


IFX_INLINE void Port_print(const IfxPort_Pin *pin, pchar name)
{
    if (pin->port != NULL_PTR)
    {
        Ifx_Console_printAlign("- %8s  : P%d.%d"ENDL, name, IfxPort_getIndex(pin->port), pin->pinIndex);
    }
}


IFX_INLINE void Port_printGroup(const IfxPort_Pin *pin, pchar name, uint32 pins)
{
    if (pin->port != NULL_PTR)
    {
        Ifx_Console_printAlign("- %s[0..%d] : P%d.[%d..%d]"ENDL, name, (pins - 1), IfxPort_getIndex(pin->port), pin->pinIndex, (pin->pinIndex + pins - 1));
    }
}


static void Ad2s1210_initCommon(Ad2s1210 *driver, const Ad2s1210_Config *config)
{
    const Ad2s1210_Common *common = &config->modes.common;

    Ad2s1210_initOutputPort(&common->A0, IfxPort_State_low);
    Ad2s1210_initOutputPort(&common->A1, IfxPort_State_low);
    Ad2s1210_initOutputPort(&common->RESET, IfxPort_State_low);
#if PRINT_RESOURCE_USED
    Port_print(&common->A0, "A0");
    Port_print(&common->A1, "A1");
#endif

    driver->modes.selected = config->modes.selected;
    driver->modes.common   = config->modes.common;
    driver->speedReload    = config->speedUpdateFactor;
    driver->speedCounter   = driver->speedReload;
    driver->state          = Ad2s1210_State_reset;
    {
        driver->offset                        = config->offset;
        driver->periodPerRotation             = config->periodPerRotation;
        driver->resolution                    = Ad2s1210_getResolutionBit(config->resolution);
        driver->reversed                      = config->reversed;
        driver->position                      = 0;
        driver->speed                         = 0;
        driver->direction                     = IfxStdIf_Pos_Dir_unknown;
        driver->turn                          = 0;
        driver->stdifStatus.status            = 0;
#if AD2S1210_DEBUG
        driver->diag.byType.signalLoss        = 0;
        driver->diag.byType.signalDegradation = 0;
        driver->diag.byType.trackingLoss      = 0;
        driver->diag.byType.commError         = 0;
        driver->diag.byType.noError           = 0;
#endif

        Ad2s1210_setupSpeedConst(driver);
    }

    /* Set default resolution to 12 bits */
    if ((common->RES0.port != NULL_PTR) && (common->RES1.port != NULL_PTR))
    {
        Ad2s1210_initOutputPort(&common->RES0, IfxPort_State_low);
        Ad2s1210_initOutputPort(&common->RES1, IfxPort_State_high);
    }
}


static void Ad2s1210_initSerial(Ad2s1210 *driver, const Ad2s1210_Config *config)
{
    const Ad2s1210_Serial *interface = &config->modes.serial;
    driver->modes.i.serial = *interface;
    driver->readData       = &Ad2s1210_readDataSerial;
    driver->writeData      = &Ad2s1210_writeDataSerial;
    Ad2s1210_initOutputPort(&interface->WRn, IfxPort_State_high);
    Ad2s1210_initOutputPort(&interface->CSn, IfxPort_State_high);
}


static void Ad2s1210_initExtBus(Ad2s1210 *driver, const Ad2s1210_Config *config)
{
    const Ad2s1210_Ebu *interface = config->modes.extbus;
    Ad2s1210_initInputPort(&interface->DOS);
    Ad2s1210_initInputPort(&interface->LOT);
#if PRINT_RESOURCE_USED
    Port_print(&interface->LOT, "LOT");
    Port_print(&interface->DOS, "DOS");
#endif

    driver->modes.i.extbus = *interface;
    driver->readData       = &Ad2s1210_readDataExtBus;
    driver->writeData      = &Ad2s1210_writeDataExtBus;
}


static void Ad2s1210_initParallel(Ad2s1210 *driver, const Ad2s1210_Config *config)
{
    const Ad2s1210_Par *interface = config->modes.parallel;
    Pin_setGroupModeInput(&interface->DB0, 0xFFFFU, IfxPort_InputMode_noPullDevice);
    Ad2s1210_initOutputPort(&interface->CSn, IfxPort_State_high);
    Ad2s1210_initOutputPort(&interface->WRn, IfxPort_State_high);
    Ad2s1210_initOutputPort(&interface->RDn, IfxPort_State_high);
    Ad2s1210_initInputPort(&interface->DOS);
    Ad2s1210_initInputPort(&interface->LOT);
#if PRINT_RESOURCE_USED
    Port_print(&interface->LOT, "LOT");
    Port_print(&interface->DOS, "DOS");
    Port_print(&interface->CSn, "CSn");
    Port_printGroup(&interface->DB0, "DB", driver->dataBits);
#endif

    driver->modes.i.parallel = *interface;
    driver->readData         = &Ad2s1210_readDataParallel;
    driver->writeData        = &Ad2s1210_writeDataParallel;
}


void Ad2s1210_initConfig(Ad2s1210_Config *config)
{
    config->offset                    = 0;
    config->periodPerRotation         = 1;
    config->reversed                  = FALSE;
    config->frequency                 = 10000;
    config->updateFrequency           = 10000;
    memset(&config->modes, 0, sizeof(config->modes));
    config->modes.selected            = Ad2s1210_Mode_parallel;
    config->resolution                = Ad2s1210_Resolution_12Bits;
    config->speedFilerCutOffFrequency = 0.0;
    config->speedUpdateFactor         = 10;
}


/** \brief Initialize the resolver object.
 *
 * FIXME Ad2s1210_initConfig() required. the initConfig shall also initialize the required SSC parameters
 *
 * This function calls Ad2s1210_initHw() and Ad2s1210_initSw().
 *
 * \param driver Specifies resolver object. This parameter is filled in by the function.
 * \param config Specifies resolver object configuration.
 *
 * \retval TRUE Returns TRUE if the initialisation was successful
 * \retval FALSE Returns FALSE if the resolver could not be initialise
 *
 * \see Ad2s1210_initHw(), Ad2s1210_initSw()
 */
boolean Ad2s1210_init(Ad2s1210 *driver, const Ad2s1210_Config *config)
{
    boolean result = TRUE;

#if PRINT_RESOURCE_USED
    Ifx_Console_printAlign("- AD2S120x resolver interface:"ENDL);
    Ifx_Console_incrAlign(0);
#endif
    Ad2s1210_initCommon(driver, config);
    driver->dataBits = Ad2s1210_getResolutionDataBits(config->resolution);

    if (config->modes.selected == Ad2s1210_Mode_serial)
    {
        Ad2s1210_initSerial(driver, config);
    }
    else if (config->modes.selected == Ad2s1210_Mode_extbus)
    {
        Ad2s1210_initExtBus(driver, config);
    }
    else if (config->modes.selected == Ad2s1210_Mode_parallel)
    {
        Ad2s1210_initParallel(driver, config);
    }
    else
    {}

    /* Default frequency is 10kHz. Set new frequency as specified by configuration */
    Ad2s1210_setFrequency(driver, config->frequency);

    /* Set the resolution as specified by configuration */
    Ad2s1210_setResolution(driver, config->resolution);

    /* DEFAULT configuration, but not set here:
     * - LOS threshold: 2.2V
     * - DOS overrange threshold: 4.1V
     * - DOS mismatch threshold: 380mV
     * - DOS minimum threshold: 3.99V
     * - DOS maximum threshold: 2.28V
     *
     *               10-bit | 12-bit | 14-bit | 16-bit
     * - LOT high :  2.5      1.0      0.5      0.5
     * - LOT low  :  12.5     5        2.5      2.5
     * - Excitation: 10kHz
     */

#if PRINT_RESOURCE_USED
    Ifx_Console_decrAlign(0);
#endif

    Ad2s1210_softwareReset(driver);

    driver->speedFilterEnabled = config->speedFilerCutOffFrequency > 0.0;

    if (driver->speedFilterEnabled != FALSE)
    {
        Ifx_LowPassPt1F32_Config filterConfig;            /**< \brief PT1 filter configuration for speed  */
        filterConfig.cutOffFrequency = config->speedFilerCutOffFrequency;
        filterConfig.gain            = 1.0;
        filterConfig.samplingTime    = (1.0 / config->updateFrequency) * config->speedUpdateFactor;

        Ifx_LowPassPt1F32_init(&driver->speedLpf, &filterConfig);
    }

    return result;
}


/** \brief reset and enable the resolver.
 * FIXME there is a need to have a defined start when a NULL pointer is passed for waitFunction. the application can not ensure that the first call is done with Ad2s1210_State_startUp. additinal paramater required
 * \param driver Specifies resolver object.
 * \param waitFunction Specifies wait function to be used.
 *             If NULL_PTR, each call to the function executes the next reset step, 3 calls
 *             are required to finish the reset sequence.
 *
 * \retval Ad2s1210_State_reset if the resolver reset signal is active (Min time 10us)
 * \retval Ad2s1210_State_startUp if the resolver is powering up (Min time 20ms)
 * \retval Ad2s1210_State_ready if the reset sequence is finished.
 */
Ad2s1210_State Ad2s1210_enable(Ad2s1210 *driver, WaitTimeFunction waitFunction)
{
    if (waitFunction != NULL_PTR)
    {
        Pin_setStateLow(&driver->modes.common.RESET);
        driver->state = Ad2s1210_State_reset;
        waitFunction(TimeConst_10us);            // Wait 10us for the RESET\low signal

        Pin_setStateHigh(&driver->modes.common.RESET);
        driver->state = Ad2s1210_State_startUp;
        waitFunction(TimeConst_10ms * 2);        // Wait 20ms before resolver ready status

        driver->state = Ad2s1210_State_ready;
    }
    else
    {
        if (driver->state == Ad2s1210_State_startUp)
        {
            driver->state = Ad2s1210_State_ready;
        }
        else if (driver->state == Ad2s1210_State_reset)
        {
            Pin_setStateHigh(&driver->modes.common.RESET);
            driver->state = Ad2s1210_State_startUp;
        }
        else
        {
            Pin_setStateLow(&driver->modes.common.RESET);
            driver->state = Ad2s1210_State_reset;
        }
    }

    return driver->state;
}


/** \brief reset and enable the resolver.
 *
 * \param driver Specifies resolver object.
 *
 * \retval None. See Ad2s1210->state
 */
void Ad2s1210_reset(Ad2s1210 *driver)
{
    Ad2s1210_enable(driver, NULL_PTR);          /* activate reset pin*/
    Ad2s1210_enable(driver, NULL_PTR);          /* deactivate reset pin */
    driver->position                      = 0;
    driver->turn                          = 0;
    driver->speed                         = 0;
    driver->stdifStatus.status            = 0;
    driver->stdifStatus.B.notSynchronised = 1;
    Ad2s1210_enable(driver, NULL_PTR);   /* ready state */
}


void Ad2s1210_resetFaults(Ad2s1210 *driver)
{   /* FIXME Implement reset fault only */
    Ad2s1210_reset(driver);
}


void Ad2s1210_setRefreshPeriod(Ad2s1210 *driver, float32 updatePeriod)
{
    /* FIXME make use of updatePeriod */
    /* FIXME should also actualise the speed fileter and speedReload */
    Ad2s1210_setupSpeedConst(driver);
}


boolean Ad2s1210_stdIfPosInit(IfxStdIf_Pos *stdif, Ad2s1210 *driver)
{
    /* Ensure the stdif is reset to zeros */
    memset(stdif, 0, sizeof(IfxStdIf_Pos));

    /* Set the driver */
    stdif->driver = driver;

    /* *INDENT-OFF* Note: this file was indented manually by the author. */
    /* Set the API link */
	stdif->onZeroIrq          =(IfxStdIf_Pos_OnZeroIrq               )NULL_PTR; /* NO zero signal */
	stdif->getAbsolutePosition=(IfxStdIf_Pos_GetAbsolutePosition     )&Ad2s1210_getAbsolutePosition;
	stdif->getDirection		  =(IfxStdIf_Pos_GetDirection			 )&Ad2s1210_getDirection;
	stdif->getFault           =(IfxStdIf_Pos_GetFault			     )&Ad2s1210_getFault;
	stdif->getPeriodPerRotation =(IfxStdIf_Pos_GetPeriodPerRotation  )&Ad2s1210_getPeriodPerRotation;
	stdif->getOffset		  =(IfxStdIf_Pos_GetOffset			     )&Ad2s1210_getOffset;
	stdif->getPosition		  =(IfxStdIf_Pos_GetPosition			 )&Ad2s1210_getPosition;
	stdif->getRawPosition	  =(IfxStdIf_Pos_GetRawPosition          )&Ad2s1210_getRawPosition;
	stdif->getRefreshPeriod   =(IfxStdIf_Pos_GetRefreshPeriod        )&Ad2s1210_getRefreshPeriod;
	stdif->getResolution      =(IfxStdIf_Pos_GetResolution           )&Ad2s1210_getResolution;
	stdif->getSensorType      =(IfxStdIf_Pos_GetSensorType           )&Ad2s1210_getSensorType;
	stdif->reset			  =(IfxStdIf_Pos_Reset				     )&Ad2s1210_reset;
	stdif->resetFaults		  =(IfxStdIf_Pos_ResetFaults			 )&Ad2s1210_resetFaults;
	stdif->getSpeed           =(IfxStdIf_Pos_GetSpeed                )&Ad2s1210_getSpeed;
	stdif->getTurn            =(IfxStdIf_Pos_GetTurn                 )&Ad2s1210_getTurn;
	switch (driver->modes.selected)
	{
	case Ad2s1210_Mode_serial:
		stdif->update			  =(IfxStdIf_Pos_Update				     )&Ad2s1210_Serial_update;
		break;
	case Ad2s1210_Mode_parallel:
		stdif->update			  =(IfxStdIf_Pos_Update				     )&Ad2s1210_Parallel_update;
		break;
	case Ad2s1210_Mode_extbus:
		stdif->update			  =(IfxStdIf_Pos_Update				     )&Ad2s1210_ExtBus_update;
		break;
	}
	stdif->setOffset		  =(IfxStdIf_Pos_SetOffset			     )&Ad2s1210_setOffset;
	stdif->setRefreshPeriod   =(IfxStdIf_Pos_SetRefreshPeriod        )&Ad2s1210_setRefreshPeriod;
    /* *INDENT-ON* */

    return TRUE;
}


#if AD2S1210_DEBUG
#define Ad2s1210_countError(index) driver->diag.byIndex[index]++
#else
#define Ad2s1210_countError(index)
#endif

IFX_INLINE boolean Ad2s1210_updatePositionPrivate(Ad2s1210 *driver, sint32 rawPosition, boolean result)
{
    if (driver->reversed != FALSE)
    {
        rawPosition  = -rawPosition;
        rawPosition += (sint16)driver->resolution;
    }

    rawPosition += (sint16)driver->offset;
    rawPosition &= (sint16)driver->resolution - 1;

    //if (result != FALSE)
    {   // Although result is FALSE (error occurred), always assign the result..
        // because in some cases the value is still usable.
        // It's up to the user to decide based on the error status.
        driver->position = rawPosition;
    }

    if (driver->status.bitfields.dosOverrange)
    {
        driver->stdifStatus.B.signalDegradation = 1;
        Ad2s1210_countError(1);
    }

    if (driver->status.bitfields.los)
    {
        driver->stdifStatus.B.signalLoss = 1;
        Ad2s1210_countError(0);
    }

    if (driver->status.bitfields.lot)
    {
        driver->stdifStatus.B.trackingLoss = 1;
        Ad2s1210_countError(2);
    }

    /* FIXME Should other error flags be checked ? */
    return result;
}


static boolean Ad2s1210_updateSpeedPrivate(Ad2s1210 *driver, sint16 rawSpeed, boolean result)
{
    if (driver->reversed != FALSE)
    {
        rawSpeed = -rawSpeed;
    }

    if ((rawSpeed >= -1) && (rawSpeed <= 1))
    {   // Increase null speed stability
        rawSpeed = 0;
    }

    if (result != FALSE)
    {
        float32 speed = driver->speedConstPulseCount * rawSpeed;

        if (driver->speedFilterEnabled == TRUE)
        {
            speed = Ifx_LowPassPt1F32_do(&driver->speedLpf, speed);
        }

        driver->speed     = speed;
        driver->direction = (driver->speed < 0) ? IfxStdIf_Pos_Dir_backward : IfxStdIf_Pos_Dir_forward;
    }

    return result;
}


/* Raw position value is on DB(16-resolution) up to DB15 */
static boolean Ad2s1210_Parallel_updatePosition(Ad2s1210 *driver)
{
    boolean       result    = TRUE;
    sint32        rawPosition;
    Ad2s1210_Par *interface = &driver->modes.i.parallel;

    Pin_setStateLow(&interface->CSn);
    Pin_setStateLow(&interface->RDn);
    AD2S1210_RD_TO_DATA_WAIT;
    rawPosition = Pin_getGroupState(&interface->DB0, 0xFFFFU);
    Pin_setStateHigh(&interface->RDn);
    Pin_setStateHigh(&interface->CSn);

    /* Note that the DOS indicates either dosOverrange, dosMismatch or both, FIXME might be more accurate to call getFaults() */
    driver->status.bitfields.dosOverrange = Pin_getState(&interface->DOS) ? 0x1 : 0x0;
    driver->status.bitfields.dosMismatch  = Pin_getState(&interface->DOS) ? 0x1 : 0x0;
    driver->status.bitfields.lot          = Pin_getState(&interface->LOT) ? 0x1 : 0x0;

    rawPosition                           = rawPosition >> (16 - driver->dataBits);

    result                                = Ad2s1210_updatePositionPrivate(driver, rawPosition, result);

    return result;
}


/* Raw position value is on DB(16-resolution) up to DB15 */
static boolean Ad2s1210_Parallel_updateSpeed(Ad2s1210 *driver)
{
    sint16        rawSpeed;
    boolean       result    = TRUE;
    Ad2s1210_Par *interface = &driver->modes.i.parallel;

    Ad2s1210_setMode(driver, Ad2s1210_OperationMode_normalVelocity);
    Pin_setStateLow(&interface->CSn);
    Pin_setStateLow(&interface->RDn);
    // Ad2s1210: 12ns required between CSn & RDn to Data valid => this is 2 cycles \180Mhz
    AD2S1210_RD_TO_DATA_WAIT;
    rawSpeed = (sint16)Pin_getGroupState(&interface->DB0, 0xFFFFU);
    Pin_setStateHigh(&interface->RDn);
    Pin_setStateHigh(&interface->CSn);
    Ad2s1210_setMode(driver, Ad2s1210_OperationMode_normalPosition);

    // Extend sign to data size
    rawSpeed = rawSpeed >> (16 - driver->dataBits);

    result   = Ad2s1210_updateSpeedPrivate(driver, rawSpeed, result);
    return result;
}


static boolean Ad2s1210_Serial_updatePosition(Ad2s1210 *driver)
{
    boolean          result = TRUE;
    uint8            tempData[2];
    sint16           rawPosition;
    Ad2s1210_Serial *interface = &driver->modes.i.serial;

    Pin_setStateLow(&interface->CSn);
    Pin_setStateLow(&interface->WRn);
    // mode is default Ad2s1210_OperationMode_normalPosition
    SpiIf_exchange(interface->channel, &tempData, &tempData, 2);
    SpiIf_wait(interface->channel);
    Pin_setStateHigh(&interface->WRn);
    Pin_setStateHigh(&interface->CSn);

    driver->status = Ad2s1210_getFaults(driver);
    /* 20200312 Jimmy Note */
    /* Filter the rawPosition and rawSpeed data when AD2S already fault */
    rawPosition    = (((uint16)tempData[1]) << 0) | (((uint16)tempData[0]) << 8);
    rawPosition    = (rawPosition >> (16 - driver->dataBits));
    result         = Ad2s1210_updatePositionPrivate(driver, rawPosition, result);

    return result;
}


static boolean Ad2s1210_Serial_updateSpeed(Ad2s1210 *driver)
{
    boolean          result = TRUE;
    sint16           rawSpeed;
    uint8            tempData[2];
    Ad2s1210_Serial *interface = &driver->modes.i.serial;

    Ad2s1210_setMode(driver, Ad2s1210_OperationMode_normalVelocity);
    Pin_setStateLow(&interface->CSn);
    Pin_setStateLow(&interface->WRn);
    SpiIf_exchange(interface->channel, &tempData, &tempData, 2);
    SpiIf_wait(interface->channel);
    Pin_setStateHigh(&interface->WRn);
    Pin_setStateHigh(&interface->CSn);
    Ad2s1210_setMode(driver, Ad2s1210_OperationMode_normalPosition);

    // Extend sign to data size
    rawSpeed = (((uint16)tempData[1]) << 0) | (((uint16)tempData[0]) << 8);
    rawSpeed = rawSpeed >> (16 - driver->dataBits);

    result   = Ad2s1210_updateSpeedPrivate(driver, rawSpeed, result);
    return result;
}


static boolean Ad2s1210_ExtBus_updatePosition(Ad2s1210 *driver)
{
    sint32  rawPosition;
    boolean result = TRUE;

    rawPosition          = (sint16)__ld32(driver->modes.i.extbus.data);
#if AD2S1210_LOT_DOS_PIN_ENABLED == 0
    driver->status.value = (rawPosition >> 12 /* FIXME use configurable offset*/) & 0x3;
#endif

    result = Ad2s1210_updatePositionPrivate(driver, rawPosition, result);

    return result;
}


static boolean Ad2s1210_ExtBus_updateSpeed(Ad2s1210 *driver)
{
    sint16  rawSpeed;
    boolean result = TRUE;

#if AD2S1210_EXT_BUS_AUTO_SELECT != 0
    rawSpeed = (__ld32(driver->modes.i.extbus.dataVel) & (driver->resolution - 1));
#else
    Ad2s1210_setMode(driver, Ad2s1210_OperationMode_normalVelocity);
    rawSpeed = (sint16)(__ld32(driver->modes.i.extbus.data) & (driver->resolution - 1));
    Ad2s1210_setMode(driver, Ad2s1210_OperationMode_normalPosition);
#endif
    // Extend sign to data size
    rawSpeed = rawSpeed << (16 - driver->dataBits);

    result   = Ad2s1210_updateSpeedPrivate(driver, rawSpeed, result);
    return result;
}


/** \brief Update the resolver position, speed and direction.
 *
 * This function updates the resolver position and direction.
 * This function also updates the speed periodically.
 *
 * \param driver Specifies resolver object.
 *
 * \return None
 */
void Ad2s1210_Serial_update(Ad2s1210 *driver)
{
    Ad2s1210_Serial_updatePosition(driver);

    driver->speedCounter--;

    if (driver->speedCounter <= 0)
    {
        driver->speedCounter = driver->speedReload;
        Ad2s1210_Serial_updateSpeed(driver);
    }
}


/** \brief Update the resolver position, speed and direction.
 *
 * This function updates the resolver position and direction.
 * This function also updates the speed periodically.
 *
 * \param driver Specifies resolver object.
 *
 * \return None
 */
void Ad2s1210_Parallel_update(Ad2s1210 *driver)
{
    Ad2s1210_Parallel_updatePosition(driver);

    driver->speedCounter--;

    if (driver->speedCounter <= 0)
    {
        driver->speedCounter = driver->speedReload;
        Ad2s1210_Parallel_updateSpeed(driver);
    }
}


/** \brief Update the resolver position, speed and direction.
 *
 * This function updates the resolver position and direction.
 * This function also updates the speed periodically.
 *
 * \param driver Specifies resolver object.
 *
 * \return None
 */
void Ad2s1210_ExtBus_update(Ad2s1210 *driver)
{
    Ad2s1210_ExtBus_updatePosition(driver);

    driver->speedCounter--;

    if (driver->speedCounter <= 0)
    {
        driver->speedCounter = driver->speedReload;
        Ad2s1210_ExtBus_updateSpeed(driver);
    }
}


/** \brief Set the resolver oscillator frequency
 *
 * Range in Hz = [2000, 20000] See recommended excitation frequency in AD2S1210 datasheet.
 *
 * \param driver Specifies resolver object.
 * \param frequency Specifies resolver oscillator frequency in Hz.
 *
 * \return return the selected frequency in Hz.
 */
sint32 Ad2s1210_setFrequency(Ad2s1210 *driver, sint32 frequency)
{
    uint32 fcw = (frequency * 32768) / AD2S1210_CLKIN;
    Ad2s1210_write(driver, AD2S1210_REG_ADDR_EXCITATION_FREQUENCY, __maxu(__minu(fcw, 0x50), 0x4));
    return fcw * AD2S1210_CLKIN / 32768;
}


void Ad2s1210_setOffset(Ad2s1210 *driver, sint32 offset)
{
	while (offset <0)
	{ /* Make sure the offset is positive */
		offset += driver->resolution;
	}
    driver->position = driver->position - driver->offset + offset;
    driver->offset = offset;
}


float32 Ad2s1210_updateSpeed(Ad2s1210 *driver, boolean update)
{   /* FIXME shall also update the speed */
    return driver->speed;
}


sint32 Ad2s1210_resetOffset(Ad2s1210 *driver)
{   /* FIXME obsolete */
    sint32 offset;
    offset = (driver->position - driver->offset);
    offset = -(offset & (driver->resolution - 1));
    Ad2s1210_setOffset(driver, offset);
    offset = driver->offset;
    return offset;
}


static void Ad2s1210_setMode(Ad2s1210 *driver, Ad2s1210_OperationMode mode)
{
    Ad2s1210_Common *common = &driver->modes.common;
    Pin_setState(&common->A0, (mode & 0x1) ? IfxPort_State_high : IfxPort_State_low);
    Pin_setState(&common->A1, (mode & 0x2) ? IfxPort_State_high : IfxPort_State_low);
}


Ad2s1210_OperationMode Ad2s1210_getMode(Ad2s1210 *driver)
{
    Ad2s1210_OperationMode result;
    Ad2s1210_Common       *common = &driver->modes.common;
    result = (Pin_getState(&common->A1) << 1) | Pin_getState(&common->A0);
    return result;
}


void Ad2s1210_setResolution(Ad2s1210 *driver, Ad2s1210_Resolution resolution)
{
    Ad2s1210_controlReg data;

    Ad2s1210_Common    *common = &driver->modes.common;

    if ((common->RES0.port != NULL_PTR) && (common->RES1.port != NULL_PTR))
    {
        Pin_setState(&common->RES0, resolution & 0x1 ? IfxPort_State_high : IfxPort_State_low);
        Pin_setState(&common->RES1, resolution & 0x2 ? IfxPort_State_high : IfxPort_State_low);
    }

    Ad2s1210_read(driver, AD2S1210_REG_ADDR_CONTROL, &data.value);
    data.bitfields.controlBit = 0;
    data.bitfields.resolution = resolution;
    Ad2s1210_write(driver, AD2S1210_REG_ADDR_CONTROL, data.value);

    driver->dataBits   = Ad2s1210_getResolutionDataBits(resolution);
    driver->resolution = (sint32)(1U << driver->dataBits);
    Ad2s1210_setupSpeedConst(driver);
}


static void Ad2s1210_setupSpeedConst(Ad2s1210 *driver)
{
    float32 speedFullScale;

    switch (driver->resolution)
    {
    case 1024:
        speedFullScale = AD2S1210_SPEED_FULL_SCALE_10BITS;
        break;
    case 4096:
        speedFullScale = AD2S1210_SPEED_FULL_SCALE_12BITS;
        break;
    case 16384:
        speedFullScale = AD2S1210_SPEED_FULL_SCALE_14BITS;
        break;
    default:
        speedFullScale = AD2S1210_SPEED_FULL_SCALE_16BITS;
        break;
    }

    driver->speedConstPulseCount = speedFullScale / (driver->periodPerRotation * driver->resolution / 2);
}


sint32 Ad2s1210_getResolution(Ad2s1210 *driver)
{
    return driver->resolution;
}


/**
 * \param driver Specifies resolver object.
 * \retrun Return the selected resolution
 */
sint32 Ad2s1210_readResolution(Ad2s1210 *driver)
{
    Ad2s1210_Resolution resolution = 0;
    Ad2s1210_Common    *common     = &driver->modes.common;
    boolean             pin        = FALSE; // Always read from register

    if (pin)
    {
        if ((common->RES0.port != NULL_PTR) && (common->RES1.port != NULL_PTR))
        {
            resolution = (Pin_getState(&common->RES1) << 1) | Pin_getState(&common->RES0);
        }
    }
    else
    {
        Ad2s1210_controlReg data;
        Ad2s1210_read(driver, AD2S1210_REG_ADDR_CONTROL, &data.value);
        resolution = data.bitfields.resolution;
    }

    driver->resolution = Ad2s1210_getResolutionBit(resolution);
    return driver->resolution;
}


void Ad2s1210_setEncoderResolution(Ad2s1210 *driver, Ad2s1210_Resolution resolution)
{
    Ad2s1210_controlReg data;

    Ad2s1210_read(driver, AD2S1210_REG_ADDR_CONTROL, &data.value);
    data.bitfields.controlBit        = 0;
    data.bitfields.encoderResolution = resolution;
    Ad2s1210_write(driver, AD2S1210_REG_ADDR_CONTROL, data.value);
}


void Ad2s1210_setPhaseLockRange(Ad2s1210 *driver, Ad2s1210_PhaseLockRange phaseLockRange)
{
    Ad2s1210_controlReg data;

    Ad2s1210_read(driver, AD2S1210_REG_ADDR_CONTROL, &data.value);
    data.bitfields.controlBit     = 0;
    data.bitfields.phaseLockRange = phaseLockRange;
    Ad2s1210_write(driver, AD2S1210_REG_ADDR_CONTROL, data.value);
}


void Ad2s1210_setHysteresis(Ad2s1210 *driver, boolean enabled)
{
    Ad2s1210_controlReg data;

    Ad2s1210_read(driver, AD2S1210_REG_ADDR_CONTROL, &data.value);
    data.bitfields.controlBit = 0;
    data.bitfields.hysteresis = enabled ? 1 : 0;
    Ad2s1210_write(driver, AD2S1210_REG_ADDR_CONTROL, data.value);
}


uint16 Ad2s1210_readPosition(Ad2s1210 *driver)
{
    uint8 data[2];
    Ad2s1210_read(driver, AD2S1210_REG_ADDR_POSITION_L, &data[0]);
    Ad2s1210_read(driver, AD2S1210_REG_ADDR_POSITION_H, &data[1]);
    return (data[1] << 8) | data[0];
}


sint16 Ad2s1210_readVelocity(Ad2s1210 *driver)
{
    uint8 data[2];
    Ad2s1210_read(driver, AD2S1210_REG_ADDR_VELOCITY_L, &data[0]);
    Ad2s1210_read(driver, AD2S1210_REG_ADDR_VELOCITY_H, &data[1]);
    return (data[1] << 8) | data[0];
}


/**
 * \param driver Specifies resolver object.
 * \param threshold LOS threshold in V
 */
void Ad2s1210_setLosThreshold(Ad2s1210 *driver, float32 threshold)
{
    Ad2s1210_write(driver, AD2S1210_REG_ADDR_LOS_THRESHOLD, __minu((uint8)threshold / 0.038, 0x7F));
}


/**
 * \param driver Specifies resolver object.
 * \param threshold DOS overrange threshold in V
 */
void Ad2s1210_setDosOverrangeThreshold(Ad2s1210 *driver, float32 threshold)
{
    Ad2s1210_write(driver, AD2S1210_REG_ADDR_DOS_OVERRANGE_THRESHOLD, __minu((uint8)threshold / 0.038, 0x7F));
}


/**
 * \param driver Specifies resolver object.
 * \param threshold DOS mismatch threshold in V
 */
void Ad2s1210_setDosMismatchThreshold(Ad2s1210 *driver, float32 threshold)
{
    Ad2s1210_write(driver, AD2S1210_REG_ADDR_DOS_MISMATCH_THRESHOLD, __minu((uint8)threshold / 0.038, 0x7F));
}


/**
 * \param driver Specifies resolver object.
 * \param threshold DOS min and max reset threshold in V
 */
void Ad2s1210_setDosResetThreshold(Ad2s1210 *driver, float32 minThreshold, float32 maxThreshold)
{
    Ad2s1210_write(driver, AD2S1210_REG_ADDR_DOS_RESET_MIN_THRESHOLD, __minu((uint8)minThreshold / 0.038, 0x7F));
    Ad2s1210_write(driver, AD2S1210_REG_ADDR_DOS_RESET_MAX_THRESHOLD, __minu((uint8)maxThreshold / 0.038, 0x7F));
}


/** Software reset
 *
 * \param driver Specifies resolver object.
 */
void Ad2s1210_softwareReset(Ad2s1210 *driver)
{
    Ad2s1210_write(driver, AD2S1210_REG_ADDR_SOFT_RESET, 0x00);
}


/**
 * \param driver Specifies resolver object.
 * \return Returns the LOS threshold in V
 */
float32 Ad2s1210_getLosThreshold(Ad2s1210 *driver)
{
    uint8 data;
    Ad2s1210_read(driver, AD2S1210_REG_ADDR_LOS_THRESHOLD, &data);
    data = data & 0x7F;
    return data * 0.038;
}


/**
 * \param driver Specifies resolver object.
 * \return Returns the DOS overrange threshold in V
 */
float32 Ad2s1210_getDosOverrangeThreshold(Ad2s1210 *driver)
{
    uint8 data;
    Ad2s1210_read(driver, AD2S1210_REG_ADDR_DOS_OVERRANGE_THRESHOLD, &data);
    data = data & 0x7F;
    return data * 0.038;
}


/**
 * \param driver Specifies resolver object.
 * \return Returns the DOS mismatch threshold in V
 */
float32 Ad2s1210_getDosMismatchThreshold(Ad2s1210 *driver)
{
    uint8 data;
    Ad2s1210_read(driver, AD2S1210_REG_ADDR_DOS_MISMATCH_THRESHOLD, &data);
    data = data & 0x7F;
    return data * 0.038;
}


/**
 * \param driver Specifies resolver object.
 * \param minThreshold DOS min reset threshold in V
 * \param maxThreshold DOS max reset threshold in V
 */
void Ad2s1210_getDosResetThreshold(Ad2s1210 *driver, float32 *minThreshold, float32 *maxThreshold)
{
    uint8 data;
    Ad2s1210_read(driver, AD2S1210_REG_ADDR_DOS_RESET_MIN_THRESHOLD, &data);
    data          = data & 0x7F;
    *minThreshold = data * 0.038;
    Ad2s1210_read(driver, AD2S1210_REG_ADDR_DOS_RESET_MAX_THRESHOLD, &data);
    data          = data & 0x7F;
    *maxThreshold = data * 0.038;
}


/**
 * \param driver Specifies resolver object.
 */
Ad2s1210_FaultReg Ad2s1210_getFaults(Ad2s1210 *driver)
{
    Ad2s1210_FaultReg data;
    Ad2s1210_read(driver, AD2S1210_REG_ADDR_FAULT, &data.value);
    return data;
}


static uint8 Ad2s1210_writeDataSerial(Ad2s1210 *driver, uint8 data)
{
    Ad2s1210_Serial *interface = &driver->modes.i.serial;
    Pin_setStateLow(&interface->CSn);
    Pin_setStateLow(&interface->WRn);
    SpiIf_exchange(interface->channel, &data, &data, 1);
    SpiIf_wait(interface->channel);
    Pin_setStateHigh(&interface->WRn);
    Pin_setStateHigh(&interface->CSn);
    return data;
}


static uint8 Ad2s1210_writeDataParallel(Ad2s1210 *driver, uint8 data)
{
    Ad2s1210_Par *interface = &driver->modes.i.parallel;

    // Put data in the lines:

    Pin_setGroupModeOutput(&interface->DB0, 0xFFU, IfxPort_OutputMode_pushPull, IfxPort_OutputIdx_general);
    Pin_setGroupState(&interface->DB0, 0xFF, data);

    Pin_setStateLow(&interface->CSn);
    AD2S1210_RD_TO_DATA_WAIT;
    Pin_setStateLow(&interface->WRn);
    AD2S1210_RD_TO_DATA_WAIT;
    Pin_setStateHigh(&interface->WRn);
    Pin_setStateHigh(&interface->CSn);

    // set back to input mode after the WRn and CSn are inactive
    Pin_setGroupModeInput(&interface->DB0, 0xFFU, IfxPort_InputMode_noPullDevice);
    return data;
}


static uint8 Ad2s1210_writeDataExtBus(Ad2s1210 *driver, uint8 data)
{
    Ad2s1210_Ebu *interface = &driver->modes.i.extbus;
    __st32(interface->data, data);
    return data;
}


static uint8 Ad2s1210_readDataSerial(Ad2s1210 *driver)
{
    uint8            d         = AD2S1210_REG_ADDR_FAULT; /* Dummy address for read */
    Ad2s1210_Serial *interface = &driver->modes.i.serial;
    Pin_setStateLow(&interface->CSn);
    Pin_setStateLow(&interface->WRn);
    SpiIf_exchange(interface->channel, &d, &d, 1);
    SpiIf_wait(interface->channel);
    Pin_setStateHigh(&interface->WRn);
    Pin_setStateHigh(&interface->CSn);
    return d;
}


static uint8 Ad2s1210_readDataParallel(Ad2s1210 *driver)
{
    uint8         d;
    Ad2s1210_Par *interface = &driver->modes.i.parallel;

    Pin_setStateLow(&interface->CSn);
    Pin_setStateLow(&interface->RDn);
    AD2S1210_RD_TO_DATA_WAIT;
    d = (uint8)Pin_getGroupState(&interface->DB0, 0xFF);
    Pin_setStateHigh(&interface->RDn);
    Pin_setStateHigh(&interface->CSn);
    return d;
}


static uint8 Ad2s1210_readDataExtBus(Ad2s1210 *driver)
{
    uint8         d;
    Ad2s1210_Ebu *interface = &driver->modes.i.extbus;
    d = (uint8)__ld32(interface->data);
    return d;
}


static boolean Ad2s1210_read(Ad2s1210 *driver, uint8 address, uint8 *data)
{
    boolean                result   = TRUE;
    Ad2s1210_OperationMode lastMode = Ad2s1210_getMode(driver);

    Ad2s1210_setMode(driver, Ad2s1210_OperationMode_configuration);

    driver->writeData(driver, address);
    *data = driver->readData(driver);

    Ad2s1210_setMode(driver, lastMode);

    return result;
}


static boolean Ad2s1210_write(Ad2s1210 *driver, uint8 address, uint8 data)
{
    boolean                result   = TRUE;
    Ad2s1210_OperationMode lastMode = Ad2s1210_getMode(driver);

    Ad2s1210_setMode(driver, Ad2s1210_OperationMode_configuration);

    driver->writeData(driver, address);
    driver->writeData(driver, data);

    Ad2s1210_setMode(driver, lastMode);

    return result;
}


void Ad2s1210_readAllRegs(Ad2s1210 *driver, Ad2s1210_Regs *regs)
{
    int i;

    for (i = 0; i < AD2S1210_REGS_COUNT; i++)
    {
        uint8 data;
        Ad2s1210_read(driver, Ad2s1210_regTable.A[i].address, &data);
        regs->A[i] = data;
    }

    /* notes: default configuration will result the following register values:
     * Address  Name                    Value(hex)
     *  0x88    losThreshold;           39             2.166 V
     *  0x89    dosOverrangeThreshold;  6c             4.104 V
     *  0x8A    dosMismatchThreshold;   0a             0.38  V
     *  0x8B    dosResetMaxThreshold;   3d             2.318 V
     *  0x8C    dosResetMinThreshold;   69             3.99  V
     *  0x8D    lotMaxThreshold;        23
     *  0x8E    lotMinThreshold;        08
     *  0x91    excitationFrequency;    28
     *  0x92    control;                7E
     *  0xF0    softReset;              00
     *  0xFF    fault;                  40
     *
     */
}
