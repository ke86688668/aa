/**
 * \file Ifx_CanHandler.h
 * \brief CAN handler
 * \ingroup library_srvsw_sysse_comm_canHandler
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_comm_canHandler CAN handler
 * This module implements the CAN handler
 * \ingroup library_srvsw_sysse_comm
 *
 * Driver features:
 * - send / receive data over can in a stand alone way.
 * - register CAN message with arbitrary message content, ID, node
 * - encode and decode message
 * - support multiple CAN nodes
 * - process() to be called periodically
 */

#ifndef IFX_CANHANDLER_H
#define IFX_CANHANDLER_H (1)

#include "SysSe/Debug/Ifx_Osci.h"
#include "Can/Can/IfxCan_Can.h" /* FIXME including this make is dependent on the uC device ==> move it to the iLLD MULTICAN, or use standardIo */

#ifndef IFX_CANHANDLER_MESSAGE_MAX
/* Maximal number of message items that the CAN handler driver supports */
#define IFX_CANHANDLER_MESSAGE_MAX      (20)
#endif
#ifndef IFX_CANHANDLER_MESSAGE_ITEM_MAX
/* Maximal number of message that the CAN handler driver supports */
#define IFX_CANHANDLER_MESSAGE_ITEM_MAX (10)
#endif

typedef struct Ifx_CanHandler_MessageItem_ Ifx_CanHandler_MessageItem;
typedef struct Ifx_CanHandler_Message_     Ifx_CanHandler_Message;

typedef enum
{
    Ifx_CanHandler_MessageType_transmit,
    Ifx_CanHandler_MessageType_receive
} Ifx_CanHandler_MessageType;

typedef uint32                      Ifx_CanHandler_Id;
typedef Ifx_CanHandler_MessageItem *Ifx_CanHandler_MessageItemPointer;
typedef Ifx_CanHandler_Message     *Ifx_CanHandler_MessagePointer;
typedef uint32                      Ifx_CanHandler_Interval;
typedef uint32                      Ifx_CanHandler_EventTime;

#define IFX_CANHANDLER_NO_MESSAGE ((Ifx_CanHandler_MessagePointer)NULL_PTR)
#define IFX_CANHANDLER_NO_ITEM    ((Ifx_CanHandler_MessageItemPointer)NULL_PTR)

/** Data pack and unpack function prototype */
typedef void (*Ifx_CanHandler_Pack)(uint64 *buffer, Ifx_CanHandler_MessageItemPointer item);

struct Ifx_CanHandler_MessageItem_
{
    void                             *data;   		/**< \brief pointer to the data */
    Ifx_CanHandler_Pack               pack;
    uint8                             offset; 		/**< \brief field offset in the Ifx_CanHandler_Message */
    uint8                             size;   		/**< \brief data size in bits, use 0 for boolean type */
    Ifx_CanHandler_MessageItemPointer next;   		/**< \brief Next message item in the Ifx_CanHandler_Message. Last item is indicated by a NULL pointer */
};
typedef enum
{
    Ifx_CanHandler_Protocol_none,       			/**< \brief Normal CAN protocol used */
    Ifx_CanHandler_Protocol_streamBasic 			/**< \brief Basic streaming protocol used. When selected the Ifx_CanHandler_MessageItem.data must point to anIfx_Fifo */
} Ifx_CanHandler_Protocol;

struct Ifx_CanHandler_Message_
{
    Ifx_CanHandler_Id                 id;        	/**< \brief CAN message ID */
    uint8                             dlc;       	/**< \Data length code */
    Ifx_CanHandler_MessageItemPointer items;     	/**< \brief First message item in the Ifx_CanHandler_Message */
    Ifx_CanHandler_Interval           interval;  	/**< \brief Message period in ms */
    Ifx_CanHandler_MessagePointer     prev;      	/**< \brief Previous message of the TX or RX queue. Last message is indicated by a NULL pointer  */
    Ifx_CanHandler_MessagePointer     next;      	/**< \brief Next message of the TX or RX queue. Last message is indicated by a NULL pointer  */
    Ifx_CanHandler_EventTime          eventTime; 	/**< \brief Event time at wich the message is send / received */
    struct
    {
        uint8 type : 2;                          	/**< \brief Message type: 0: transmit, 1: receive, others: reserved */
        uint8 newData : 1;                       	/**< \brief if 1 new data is available for send, if 0 message will be ignored for send */
        uint8 single : 1;                        	/**< \brief if 1, newData is clear after sending. newData must be explicitly set to enable sending again  */
        uint8 reserved : 4;
    }                       flags;
    Ifx_CanHandler_Protocol protocol;            	/**< \brief Protocol used */
    uint8                   rxTxCount;				/**< \brief Indicated the number of Rx message that are properly received / transmit */
    uint8                   missingCount;        	/**< \brief Indicated the number of Rx message that where not received */
    IfxCan_Can_MsgObj *msgObj;              	/**< \brief Message object used to send / received CAN message */
};

#define IFX_CANHANDLER_MESSAGE_TYPE_TRANSMIT (0)
#define IFX_CANHANDLER_MESSAGE_TYPE_RECEIVE  (1)

typedef struct
{
    Ifx_CanHandler_MessagePointer     freeMessage;                               /**< \brief Next free message, points to a circular buffer */
    Ifx_CanHandler_MessageItemPointer freeItem;                                  /**< \brief Next free item */
    Ifx_CanHandler_MessagePointer     tx;                                        /**< \brief Next message to transmit, points to a circular buffer  */
    Ifx_CanHandler_MessagePointer     rx;                                        /**< \brief Next message to receive, points to a circular buffer  */
    Ifx_CanHandler_Message            messagePool[IFX_CANHANDLER_MESSAGE_MAX];   /* Message Pool */
    Ifx_CanHandler_MessageItem        itemPool[IFX_CANHANDLER_MESSAGE_ITEM_MAX]; /* Message Pool */
    Ifx_CanHandler_EventTime          inboxTime;                                 /**< \brief Current time used for receive  */
    Ifx_CanHandler_EventTime          outboxTime;                                /**< \brief Current time used for send  */
    Ifx_CanHandler_EventTime          processInterval;                           /**< \brief Process interval used for send / receive timing */
} Ifx_CanHandler;

typedef struct
{
    Ifx_CanHandler_EventTime processInterval; /**< \brief Process interval used for send / receive timing */
} Ifx_CanHandler_Config;

typedef struct
{
    Ifx_CanHandler_Id          id;        /**< \brief CAN message ID */
    Ifx_CanHandler_Interval    interval;  /**< \brief Message period in ms */
    Ifx_CanHandler_EventTime   eventTime; /**< \brief Event time at wich the message is send / received */
    Ifx_CanHandler_MessageType type;      /**< \brief message type */
    IfxCan_Can_MsgObj    *msgObj;    /**< \brief Message object used to send / received CAN message */
    Ifx_CanHandler_Protocol    protocol;  /**< \brief Protocol used */
} Ifx_CanHandler_MessageConfig;

typedef struct
{
    void *data;   /**< \brief pointer to the data */
    uint8 offset; /**< \brief field offset in the Ifx_CanHandler_Message */
    uint8 size;   /**< \brief data size in bits, use 0 for boolean type */
} Ifx_CanHandler_FieldConfig;

/** Initialize the CAN handler
 *
 */
boolean Ifx_CanHandler_init(Ifx_CanHandler *canHandler, Ifx_CanHandler_Config *config);
/** Initialize the CAN handler configuration
 *
 */
void Ifx_CanHandler_initConfig(Ifx_CanHandler_Config *config);

/** Process the CAN messages (receive)
 *
 */
boolean Ifx_CanHandler_processInbox(Ifx_CanHandler *canHandler);
/** Process the CAN messages (Send)
 *
 */
boolean Ifx_CanHandler_processOutbox(Ifx_CanHandler *canHandler);
/** Initialize the message configuration
 *
 */
void Ifx_CanHandler_initMessageConfig(Ifx_CanHandler_MessageConfig *config);
/** Initialize the field configuration
 *
 */
void Ifx_CanHandler_initFieldConfig(Ifx_CanHandler_FieldConfig *config);
/** Register a message
 *
 */
Ifx_CanHandler_MessagePointer Ifx_CanHandler_registerMessage(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessageConfig *config);

/** Set/clear the message new data flag
 *
 */
IFX_INLINE void Ifx_CanHandler_setNewData(Ifx_CanHandler_MessagePointer message, boolean newData)
{
    if (newData)
    {
        message->flags.newData = 1;
    }
    else
    {
        message->flags.newData = 0;
    }
}


/** Test the message new data flag
 *
 */
IFX_INLINE boolean Ifx_CanHandler_isNewData(Ifx_CanHandler_MessagePointer message)
{
    /* MCMETILLD-627 request to add IfxMultican_Can_MsgObj_isTransmitRequest         */
	//  20191212 disable by Jimmy
	//   return (message->flags.newData != 0) || (IfxCan_getNodePointer(message->msgObj->node->mcan, message->msgObj->msgObjId)->STAT.B.TXRQ != 0);
}


/** Enable / disable single send
 * Setting the single flag will automatically clear the newData flag
 *
 */
IFX_INLINE void Ifx_CanHandler_setSingle(Ifx_CanHandler_MessagePointer message, boolean single)
{
    if (single)
    {
        message->flags.single = 1;
        Ifx_CanHandler_setNewData(message, FALSE);
    }
    else
    {
        message->flags.single = 0;
    }
}


/** Add a message field
 *
 */
boolean Ifx_CanHandler_addMessageField(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessagePointer message, Ifx_CanHandler_FieldConfig *config);
/** Unregister a message
 *
 */
void Ifx_CanHandler_unregisterMessage(Ifx_CanHandler *canHandler, Ifx_CanHandler_MessagePointer message);
void Ifx_CanHandler_dumpQueues(Ifx_CanHandler *canHandler);

#endif
