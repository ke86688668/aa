/**
 * \file Ifx_Message.c
 * \brief Internal messaging functions
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 */
#include "Ifx_Message.h"

//---------------------------------------------------------------------------
void Ifx_Message_init(Ifx_Message *message, Ifx_Message_Config *config)
{
    Ifx_Message_clear(message);
    message->owner      = config->owner;
    message->ownerName  = config->ownerName;
    message->count      = config->count;
    message->entries    = config->entries;
    message->standardIo = config->standardIo;
}


void Ifx_Message_initConfig(Ifx_Message_Config *config)
{
    config->count      = 0;
    config->entries    = NULL_PTR;
    config->owner      = 0;
    config->ownerName  = "";
    config->standardIo = NULL_PTR;
}


boolean Ifx_Message_isSet(Ifx_Message *message, uint32 index, boolean single)
{
    uint32  Mask = 0x1 << index;
    uint32  result;

    boolean istate = disableInterrupts();
    result          = message->flags & Mask & (~message->displayed);
    message->flags &= ~result;

    if (single == TRUE)
    {
        message->displayed |= result;
    }

    restoreInterrupts(istate);
    return (result != 0) ? TRUE : FALSE;
}


void Ifx_Message_show(Ifx_Message *message)
{
    uint32 i;

    for (i = 0; i < message->count; i++)
    {
        if (Ifx_Message_isSet(message, i, message->entries[i].single) != FALSE)
        {
            IfxStdIf_DPipe_print(*message->standardIo, "\r\nSystem [0x%x: %s] %s"ENDL, message->owner, message->ownerName, message->entries[i].string);
        }
    }
}
