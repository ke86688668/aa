/**
 * \file Ifx_MotorControlF32.h
 * \brief Electrical motor control.
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_emotor_motorControlProcess Electrical motor control process.
 *
 * This module implements the motor control process that enables the motor to
 * - set control mode
 * - set reference values
 * - control the motor
 *
 * \ingroup library_srvsw_sysse_emotor
 *
 */
#ifndef IFX_MOTORCONTROLF32
#define IFX_MOTORCONTROLF32 1

#include "StdIf/IfxStdIf_MotorModelF32.h"
#include "StdIf/IfxStdIf_Inverter.h"
#include "StdIf/IfxStdIf_Pos.h"
#include "StdIf/IfxStdIf_MotorModelF32.h"
#include "SysSe/Math/Ifx_RampF32.h"
#include "SysSe/Comm/Ifx_Message.h"
#include "Ifx_VelocityCtrF32.h"
#include "SysSe/Math/Ifx_PicF32.h"
#include "SysSe/VirtualDevice/Ifx_VirtualPositionSensor.h"
#include "Ifx_MotorControl.h"

/** \brief Motor control flags */
typedef union
{
    struct
    {
        uint32 running :             1;  /**< \brief 1 = the motor is running. 0 = the motor is stopped. */
        uint32 start :               1;  /**< \brief 1 = request to start. 0 = no effect */
        uint32 stop :                1;  /**< \brief 1 = request to stop. 0 = no effect  */
        uint32 clearError :          1;  /**< \brief 1 = request to clear controller's error. 0 = no effect */
        uint32 emergencyEnabled :    1;  /**< \brief 1 = the emergency function is enabled, 0 = disabled */
        uint32 positionIsEstimated : 1;  /**< \brief Specifies if the sensorless control is enabled */        /* FIXME not implemented */
        uint32 generatorMode :       1;  /**< \brief 0 = motor mode, 1 = generator mode. */                   /* FIXME not used */
        uint32 paramsChanging :      1;  /**< \brief 1 = parameters changing is in process, 0 = otherwise */  /* FIXME not used */
        uint32 derating :            1;  /**< \brief 1 = in derating */                                       /* FIXME not used */
        uint32 speedLimit :          1;  /**< \brief 1 = in speed limit */                                    /* FIXME not used */
        uint32 backwardDrive :       1;  /**< \brief 1 = backward */                                          /* FIXME not used, not required ? */
        uint32 hadEmergency :        1;  /**< \brief 1 = an emergency event occured. 0 = nothing*/
        uint32 modeTest :            1;  /**< \brief 0 = Normal mode, 1 test mode */
    }      B;
    uint32 U;
} Ifx_MotorControlF32_Flags;

typedef struct
{
    IfxStdIf_MotorModelF32 *motorModel;
    struct
    {
        IfxStdIf_Pos_RawAngle	offset;
        Ifx_RampF32          	amplitude;							/**< \brief Modulation amplitude ramp */
    }                                  	openLoop;
    IfxStdIf_Inverter                  	*inverter;
    IfxStdIf_Pos                       	*positionSensor;
    IfxStdIf_Pos                       	*currentPositionSensor;
    cfloat32    					   	current;					/**< \brief Current reference in current control mode */
    Ifx_RampF32                        	torqueRamp; 				/**< \brief Torque ramp */
    Ifx_VelocityCtrF32                 	velocity;					/**< \brief Velocity control object */

    volatile Ifx_MotorControlF32_Flags	flags;
    Ifx_Message                         messages;					/**< \brief FIXME application flags To be globally handled */
    Ifx_F32_MotorModel_output           state;
    Ifx_MotorControl_Mode               mode;						/**< \brief Active control mode */
    Ifx_VirtualPositionSensor           virtualPosition;
    IfxStdIf_Pos                        stdifVirtualPosition;

    struct
    {
        uint8         	index;										/**< \brief State machine index, used by different modes */
        IfxStdIf_Pos	*sensor;									/**<\ brief temporary variable */
        Ifx_TickTime  	deadline;									/**<\ brief temporary variable */
    } stateMachine;
} Ifx_MotorControlF32;

typedef struct
{
    Ifx_PicF32_Config       piSpeed;               /**< \brief PI-controller configuration for speed control */
    float32                  speedControlPeriod;    /**< \brief Control period in s for speed controller  */
    float32                  speedMax;              /**< \brief Maximum allowed speed in Rpm */
    float32                  torqueMax;             /**< \brief Maximum allowed torque in N.m */

    float32                  controlPeriod;         /**< \brief Control period in s for torque and current controller */
    float32                  torqueRate;            /**< \brief Torque rate, in N.m/s */
    float32                  speedRate;             /**< \brief Speed rate, in rad/s/s */
    IfxStdIf_MotorModelF32 *motorModel;
    float32                  openLoopAmplitudeRate; /**< \brief Open loop amplitude rate, in s-1 */
    IfxStdIf_Inverter       *inverter;
    IfxStdIf_Pos            *positionSensor;
    Ifx_MotorControl_Mode    mode;                  /**< \brief Active control mode */
    IfxStdIf_DPipePointer   *standardIo;
    pchar                    name;                  /**< \brief named used to identify the object */
} Ifx_MotorControlF32_Config;

Ifx_MotorControl_Mode Ifx_MotorControlF32_getMode(Ifx_MotorControlF32 *motor);
boolean               Ifx_MotorControlF32_setMode(Ifx_MotorControlF32 *motor, Ifx_MotorControl_Mode mode);
boolean               Ifx_MotorControlF32_setSpeed(Ifx_MotorControlF32 *motor, float32 speed);
boolean Ifx_MotorControlF32_setPosition(Ifx_MotorControlF32 *motor, float32 position);
boolean               Ifx_MotorControlF32_setTorque(Ifx_MotorControlF32 *motor, float32 torque);
boolean Ifx_MotorControlF32_setCurrent(Ifx_MotorControlF32 *motor, cfloat32 *current);
void 				  Ifx_MotorControlF32_setCurrentControllers(Ifx_MotorControlF32 *motor, Ifx_FocF32_Pic_Config *piD, Ifx_FocF32_Pic_Config*piQ);
void                  Ifx_MotorControlF32_setTorqueLimit(Ifx_MotorControlF32 *motor, float32 limit);
void                  Ifx_MotorControlF32_speedStep(Ifx_MotorControlF32 *motor);
void                  Ifx_MotorControlF32_execute(Ifx_MotorControlF32 *motor);
void                  Ifx_MotorControlF32_init(Ifx_MotorControlF32 *motor, Ifx_MotorControlF32_Config *config);
void                  Ifx_MotorControlF32_initConfig(Ifx_MotorControlF32_Config *config);
void                  Ifx_MotorControlF32_resetFlags(Ifx_MotorControlF32 *motor);
void                  Ifx_MotorControlF32_start(Ifx_MotorControlF32 *motor);
void                  Ifx_MotorControlF32_stop(Ifx_MotorControlF32 *motor);
boolean               Ifx_MotorControlF32_setOpenLoopAmplitude(Ifx_MotorControlF32 *motor, float32 amplitude);
float32               Ifx_MotorControlF32_getOpenLoopAmplitude(Ifx_MotorControlF32 *motor);
boolean               Ifx_MotorControlF32_setOpenLoopPosition(Ifx_MotorControlF32 *motor, float32 position);
boolean               Ifx_MotorControlF32_setPositionSensor(Ifx_MotorControlF32 *motor, IfxStdIf_Pos *position);
float32               Ifx_MotorControlF32_getSpeed(Ifx_MotorControlF32 *motor);
float32               Ifx_MotorControlF32_getSensorSpeed(Ifx_MotorControlF32 *motor);
float32 			  Ifx_MotorControlF32_getPosition(Ifx_MotorControlF32 *motor);
float32 			  Ifx_MotorControlF32_getSensorPosition(Ifx_MotorControlF32 *motor);
void 				  Ifx_MotorControlF32_resetVirtualPositionSensor(Ifx_MotorControlF32 *motor);
sint32 Ifx_MotorControlF32_getTurn(Ifx_MotorControlF32 *motor);
float32               Ifx_MotorControlF32_getTorque(Ifx_MotorControlF32 *motor);

IFX_INLINE boolean Ifx_MotorControlF32_isRunning(Ifx_MotorControlF32 *motor)
{
    return (motor->flags.B.running != 0) ? TRUE : FALSE;
}


/** \brief Returns pointer to the position interface object */
IFX_INLINE IfxStdIf_Pos *Ifx_MotorControlF32_getPositionSensor(Ifx_MotorControlF32 *motor)
{
    return motor->currentPositionSensor;
}

void Ifx_MotorControlF32_backgroundTask(Ifx_MotorControlF32 *motor);

#endif
