/**
 * \file Ifx_MotorModelPmsmF32.h
 * \brief Electrical motor control (PMSM).
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_emotor_motorControl_pmsm Electrical motor control (PMSM).
 *
 * This module implements the \ref library_srvsw_sysse_ipmsmModel and \ref library_srvsw_sysse_spmsmModel.
 *
 * \ingroup library_srvsw_sysse_emotor
 *
 */
/**
 * \defgroup library_srvsw_sysse_ipmsmModel IPMSM Model
 * \ingroup library_srvsw_sysse_emotor_motorControl_pmsm
 *
 * This motor is characterized with the saliency (\f$L_d<L_q\f$) and its
 * inductance difference is defined as:
 *
 * \f$\Delta_L=L_q-L_d\f$   \eq{20, lib_sysse_emotor_motorModel}
 *
 * Electrical torque \f$\Gamma_e\f$ produced by the 3-phase motor is:
 *
 * \f$\Gamma_e=\frac{3}{2}p\,(\psi_m-\Delta_L.i_d ).i_q\f$  \eq{22,lib_sysse_emotor_motorModel_eq22}
 *
 * The voltage equation of this motor can be written as:
 *
 * \f$v_d = R_s.i_d - \omega_r.\Psi_q + \frac{\mathrm{d}\Psi_d}{\mathrm{d}t} = R_s.i_d - \omega_r.L_q.i_q + L_d.\frac{\mathrm{d}i_d}{\mathrm{d}t}\f$
 *
 * \f$v_q = R_s.i_q + \omega_r.\Psi_d + \frac{\mathrm{d}\Psi_q}{\mathrm{d}t} = R_s.i_d + \omega_r.\left(L_d.i_d + \Psi_{m}\right) + L_q.\frac{\mathrm{d}i_q}{\mathrm{d}t}\f$   \eq{21,lib_sysse_emotor_motorModel_eq21}
 *
 * To operate properly, the current and voltage limits shall be respected.
 *
 * \f$i_d^2+i_q^2 \le I_{max}^2\f$   \eq{23,lib_sysse_emotor_motorModel_eq23}
 *
 * \f$v_d^2+v_q^2 \le V_{max}^2\f$   \eq{24,lib_sysse_emotor_motorModel_eq24}
 *
 * Substituting \f$v_d\f$ and \f$v_q\f$ of \eqlink{24,lib_sysse_emotor_motorModel_eq24}
 * with steady-state of \eqlink{21,lib_sysse_emotor_motorModel_eq21} and
 * neglecting the resistive drop, the operation limit is bound within an ellipse.
 *
 * \f$\left(\psi_m+L_d\,i_d\right)^2+\left(L_q\,i_q\right)^2 \le \left(\frac{V_{max}}{\omega_r}\right)^2\f$  \eq{25,lib_sysse_emotor_motorModel_eq25}
 *
 * The ellipse boundary shrinks to its centre as the motor rotates toward higher speed. The
 * value of \f$V_{max}\f$ depends on the modulation scheme used.
 *
 * For \ref library_srvsw_sysse_emotor_svm, \f$V_{max}=s_f.\frac{V_{dc}}{\sqrt{3}}\f$ as in
 * \eqlink{17,lib_sysse_emotor_motorModel_eq17}, where \f$s_f\f$ is a factor to ensure
 * safe operation, e.g. 90%.
 *
 * \image html "SrvSw.SysSe.EMotor[ControlTrajectoryIPMSM].png" "Operating area of IPMSM"
 *
 * The above figure shows the operating area of IPMSM. There are several important edges:
 *
 * * A - B: Maximum-torque-per-ampere (MTPA).
 *
 * \f$i_{qmtpa}^4.\Delta_L^2+\Gamma_n.i_{qmtpa}.\psi_m-\Gamma_n^2=0\f$          with  \f$\Gamma_n=\frac{\Gamma_{ref}}{\frac{3}{2}.p}\f$   \eq{26,lib_sysse_emotor_motorModel_eq26}
 *
 * \f$i_{dmtpa}=\frac{\psi_m-\sqrt{\psi_m^2+(2\Delta_L.i_{qmtpa})^2 }}{2\Delta_L}\f$ \eq{27,lib_sysse_emotor_motorModel_eq27}
 *
 * * C: Field-weakening while maintaining requested torque.
 *
 * \f$i_{dfw}=\frac{-\psi_m}{L_d} +\frac{1}{L_d}.\sqrt{\frac{V_{max}}{\omega_r}^2+(L_q.i_{qmtpa} )^2}\f$,       \f$i_{dmax} \le i_{dfw} \le 0\f$      \eq{28,lib_sysse_emotor_motorModel_eq28}
 *
 * \f$i_{qfw}=i_{qmtpa}+\frac{\Delta_L.(i_{dfw}-i_{dmtpa}).i_{qmtpa}}{\psi_m-\Delta_L.i_{dfw}}\f$ \eq{29,lib_sysse_emotor_motorModel_eq29}
 *
 * * D: Field-weakening with limited torque, similar to edge C but with current limitation of \eqlink{23,lib_sysse_emotor_motorModel_eq23}.
 *
 * * E - F: Maximum-torque-per-flux (MTPF)
 *
 * \f$\frac{2\rho^2.L_d^3.\Gamma_e.(\rho-1)}{(\psi_m-(\rho-1).L_d.i_{dmtpf})^3} +2L_d.(\psi_m+L_d.i_{dmtpf})=0\f$ ,\f$\rho=\frac{L_q}{L_d}\f$ \eq{30,lib_sysse_emotor_motorModel_eq30}
 */
/**
 * \defgroup library_srvsw_sysse_spmsmModel SPMSM Model
 * \ingroup library_srvsw_sysse_emotor_motorControl_pmsm
 *
 * With this type of motor \f$L_d=L_q=L\f$, voltage equation from \eqlink{21,lib_sysse_emotor_motorModel_eq21} can be written as:
 *
 * \f$v_d=R_s.i_d-\omega_r.L.i_q+L\frac{di_d}{dt}\f$
 *
 * \f$v_q=R_s.i_q-\omega_r.(\psi_m+L.i_d )+L\frac{di_q}{dt}\f$   \eq{31,lib_sysse_emotor_motorModel_eq31}
 *
 * Electrical torque \f$T_e\f$ produced by the motor can be written as:
 *
 * \f$\Gamma_e=1,5.p.\psi_m.i_q\f$ \eq{32,lib_sysse_emotor_motorModel_eq32}
 *
 * Respecting the current and voltage limits (\eqlink{23,lib_sysse_emotor_motorModel_eq23} and \eqlink{24,lib_sysse_emotor_motorModel_eq24}) and
 * neglecting the resistive drop the operation is bound within circle which
 * shrink as the motor rotates toward higher speed
 *
 * \f$(\psi_m+L.i_d)^2+(L.i_q)^2 \le (\frac{V_{max}}{\omega_r})^2\f$  \eq{33,lib_sysse_emotor_motorModel_eq33}
 *
 *
 * \image html "SrvSw.SysSe.EMotor[ControlTrajectorySPMSM].png" "Operating area of SPMSM"
 *
 * The above figure shows the operating area of SPMSM. The path A-B-D-E-F is taken if
 * the \f$I_{max}\f$ is reached before field-weakening condition occurs. Otherwise,
 * path A-B-C-E-F is taken.
 *
 * The algorithm to calculate \f$i_{dref}\f$ and \f$i_{qref}\f$ is as follows:
 *
 * 1. Initially calculate \f$i_{qmtpa}\f$ as in \eqlink{32,lib_sysse_emotor_motorModel_eq32} limited by \f$I_{max}\f$:
 *
 * \f$i_{qmtpa}=min(\frac{\Gamma_e}{1,5.p.\psi_m},I_{max})\f$   \eq{34,lib_sysse_emotor_motorModel_eq34}
 *
 * 2. If  \f$(\frac{V_{max}}{L\omega_r})^2-(\frac{\psi_m}{L})^2<i_{qmtpa}^2\f$, then
 * continue to 3, else \f$i_{qref}=i_{qmtpa}\f$ and \f$i_{dref}=0\f$ and finish.
 *
 * 3. If  \f$i_{qmtpa}^2<(\frac{V_{max}}{L.\omega_r})^2\f$, then continue to 4, else
 * \f$i_{qref}=\frac{V_{max}}{L.\omega_r}\f$  and \f$i_{dref}=-\frac{\psi_m}{L}\f$ and finish.
 *
 * 4. Set \f$i_{dref}=i_{dfw}\f$, where:
 *
 * \f$i_{dfw}=-\frac{\psi_m}{L}+\sqrt{(\frac{V_{max}}{L.\omega_r})^2-i_{qmtpa}^2 }\f$ \eq{35,lib_sysse_emotor_motorModel_eq35}
 *
 * 5. Calculate:
 *
 * \f$i_{qfw}=\frac{1}{L}.\sqrt{(\frac{V_{max}}{\omega_r})^2-(\psi_m+L.i_{dfw})^2}\f$    \eq{36,lib_sysse_emotor_motorModel_eq36}
 *
 * and set \f$i_{qref}=min(i_{qfw},\sqrt{I_{max}^2-i_{dfw}^2})\f$
 */

#ifndef IFX_MOTORMODELPMSMF32_H
#define IFX_MOTORMODELPMSMF32_H
#include "StdIf/IfxStdIf_MotorModelF32.h"
#include "Ifx_FocF32.h"

typedef struct
{
    uint8   polePair;       /**< \brief Motor pole pair count */
    float32 rs;             /**< \brief Motor stator resistance in Ohm */

    float32 ld;             /**< \brief Motor phase D inductance in H */
    float32 lq;             /**< \brief Motor phase Q inductance in H */
    float32 fluxM;          /**< \brief Motor permanent magnet magnetic flux linkage in V.s */

    float32 iStall;         /**< \brief Motor Stall current in A */
}Ifx_MotorModelPmsmF32_Data;
typedef struct
{
    IfxStdIf_MotorModelF32_Config base;
    float32                        voltageGenMax;         /**< \brief Maximum allowed DC-link voltage in V during regenerative braking */
    boolean                        fieldWeakeningEnabled; /**< \brief Specifies if the field weakening is enabled FIXME duplicate with Ifx_FocF32_Config.fieldWeakeningEnabled */
    Ifx_FocF32_Config             foc;                   /**< \brief Configuration for FOC object */
    float32                        rs;                    /**< \brief Motor phase resistance in Ohm */
    float32                        ld;                    /**< \brief Motor phase D inductance in H */
    float32                        lq;                    /**< \brief Motor phase Q inductance in H */
    float32                        fluxM;                 /**< \brief Motor flux magnetic linkage in V.s */
    float32                        iStall;                /**< \brief Motor Stall current in A */

    boolean                        torqueControlEnabled;  /**< \brief Specifies if the torque control is enabled or disabled */
    boolean                        feedForwardEnabled;    /**< \brief Specifies if the feed forward is enabled */
}Ifx_MotorModelPmsmF32_Config;

typedef struct
{
    Ifx_F32_MotorModel_Type      type;
    float32                      electricalSpeed;       /**< \brief Rotor electrical speed (rad/s) */
    float32                      controlPeriod;
    Ifx_FocF32                  foc;
    Ifx_FocF32_Params           focParams;             /**< \brief FOC parameters */
    float32                      electricalAngleConst;
    IfxStdIf_Pos_RawAngle        electricalAngle;
    float32                      vdc;                   /**< \brief Nominal DC-link voltage in V */

    boolean                      torqueControlEnabled;  /**< \brief Specifies if the torque control is enabled or disabled */
    boolean                      feedForwardEnabled;    /**< \brief Specifies if the feed forward is enabled */
    boolean                      fieldWeakeningEnabled; /**< \brief Specifies if the field weakening is enabled */
    Ifx_MotorModelPmsmF32_Data params;
    cfloat32                     mtpa;
    float32                      fluxMax;
    cfloat32                     fw;
}Ifx_MotorModelPmsmF32;

/**
 * \brief Initialize the PMSM control structure.
 * \param model Pointer to the \ref Ifx_MotorModelPmsmF32 object
 * \param config Pointer to the configuration structure
 */
void    Ifx_MotorModelPmsmF32_init(Ifx_MotorModelPmsmF32 *model, Ifx_MotorModelPmsmF32_Config *config);
void    Ifx_MotorModelPmsmF32_initConfig(Ifx_MotorModelPmsmF32_Config *config);
boolean Ifx_MotorModelPmsmF32_stdIfMotorModelInit(IfxStdIf_MotorModelF32 *stdif, Ifx_MotorModelPmsmF32 *driver);
void    Ifx_MotorModelPmsmF32_currentStep(Ifx_MotorModelPmsmF32 *model, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output);
void Ifx_MotorModelPmsmF32_setCurrentControllers(Ifx_MotorModelPmsmF32 *model, Ifx_FocF32_Pic_Config *piD, Ifx_FocF32_Pic_Config*piQ);

#endif
