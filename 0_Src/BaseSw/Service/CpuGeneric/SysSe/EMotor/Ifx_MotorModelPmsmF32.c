#include "Ifx_MotorModelPmsmF32.h"
#include "Ifx_svmF32.h"
#include "string.h"

static void Ifx_MotorModelPmsmF32_executeSalient(Ifx_MotorModelPmsmF32 *model, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output);
static void Ifx_MotorModelPmsmF32_execute(Ifx_MotorModelPmsmF32 *model, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output);
static void Ifx_MotorModelPmsmF32_reset(Ifx_MotorModelPmsmF32 *model);

void Ifx_MotorModelPmsmF32_setupParams(Ifx_MotorModelPmsmF32 *model, const Ifx_MotorModelPmsmF32_Config *config)
{
    Ifx_MotorModelPmsmF32_Data *p = &model->params;
    p->rs     = config->rs;
    p->ld     = config->ld;
    p->lq     = config->lq;
    p->fluxM  = config->fluxM;
    p->iStall = config->iStall;
}


void Ifx_MotorModelPmsmF32_setControlPeriod(Ifx_MotorModelPmsmF32 *model, float32 controlPeriod)
{
    model->controlPeriod = controlPeriod;
}


void Ifx_MotorModelPmsmF32_initConfig(Ifx_MotorModelPmsmF32_Config *config)
{
    IfxStdIf_MotorModelF32_initConfig(&config->base);
    config->voltageGenMax             = 0.0;
    config->fieldWeakeningEnabled     = FALSE;
    config->foc.piD.kp                = 0.0;
    config->foc.piD.ki                = 0.0;
    config->foc.piQ.kp                = 0.0;
    config->foc.piQ.ki                = 0.0;
    config->foc.vdcNom                = 0.0;
    config->foc.kiFw                  = 2.5;
    config->foc.fwFbMax               = 1.0;
    config->foc.fieldWeakeningEnabled = FALSE;
    config->rs                        = 0.0;
    config->ld                        = 0.0;
    config->lq                        = 0.0;
    config->fluxM                     = 0.0;
    config->iStall                    = 0.0;
    config->torqueControlEnabled      = TRUE;
    config->feedForwardEnabled        = FALSE;
}


void Ifx_MotorModelPmsmF32_setPositionSensorResolution(Ifx_MotorModelPmsmF32 *model, IfxStdIf_Pos_RawAngle resolution)
{
    model->electricalAngleConst = (float32)(model->params.polePair * IFX_LUT_ANGLE_RESOLUTION)
                                  / (float32)resolution;
}


void Ifx_MotorModelPmsmF32_init(Ifx_MotorModelPmsmF32 *model, Ifx_MotorModelPmsmF32_Config *config)
{
    /* Ensure configuration consistency */
    config->foc.fieldWeakeningEnabled = config->fieldWeakeningEnabled;
    model->fieldWeakeningEnabled      = config->fieldWeakeningEnabled;
    /* internal variables --------------------------------------------------------------*/
    model->params.polePair            = config->base.polePair;
    Ifx_MotorModelPmsmF32_setPositionSensorResolution(model, config->base.positionSensorResolution);

    Ifx_MotorModelPmsmF32_setControlPeriod(model, config->base.controlPeriod);

    /* Initialise the field-oriented control -------------------------------------------*/
    Ifx_FocF32_calcParams(&model->focParams, &config->foc, model->controlPeriod);
    Ifx_FocF32_init(&model->foc, &model->focParams);
    Ifx_FocF32_setIdqLimit(&model->foc, config->base.currentMax);

    Ifx_MotorModelPmsmF32_setupParams(model, config);	// 20191224 Sync both eeprom or default parameters into control loop //

    /* Using MTPA when Ld doesn't equal Lq ---------------------------------------------*/
    if (model->params.ld != model->params.lq)
    {
        model->type = Ifx_F32_MotorModel_Type_pmsmSalient;
    }
    else
    {
        model->type = Ifx_F32_MotorModel_Type_pmsmStandard;
    }

    model->torqueControlEnabled = config->torqueControlEnabled;
    model->feedForwardEnabled   = config->feedForwardEnabled;
}


static void Ifx_MotorModelPmsmF32_reset(Ifx_MotorModelPmsmF32 *model)
{
    Ifx_FocF32_reset(&model->foc);
}


IFX_INLINE cfloat32 Ifx_MotorModelPmsmF32_decoupling(Ifx_MotorModelPmsmF32 *model)
{
    /*
     * PMSM equation:
     * Vd = (R.Id + Ld.dId) - w.(Lq.Iq)
     * Vq = (R.Iq + Lq.dIq) + w.(Ld.Id + FluxPM)
     */
    cfloat32                      vdq;
    Ifx_MotorModelPmsmF32_Data *p = &model->params;

    vdq.real = -model->electricalSpeed * p->lq * model->foc.idq.imag;
    vdq.imag = +model->electricalSpeed * ((p->ld * model->foc.idq.real) + p->fluxM);

#if IFX_FOCF32_VOLTAGE_IN_VOLTS
    return vdq;
#else
    /* FIXME only valid if SVM is used for generating the PWM, currently done out of this module */
    return Ifx_SvmF32_getModIndex(vdq, model->vdc);
#endif
}


// CURRENT CONTROL //

/** \brief Implementation of current control for PMSM.
 *
 * In this function, the followings are performed:
 * - Calling to Emotor_commandStage()
 * - Execute the FOC (Ifx_FocF32_stepOut() + Inverter_setVoltages()) or Emotor_openLoopStage()
 *   depends on the control mode.
 *
 * \param self specifies the pointer to the \ref Ifx_MotorControlF32 object
 *
 * \return None.
 *
 * \ingroup mod_PmsmVec
 */
/*static*/ void Ifx_MotorModelPmsmF32_currentStep(Ifx_MotorModelPmsmF32 *model, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output)
{
    cfloat32              mab;
    Ifx_FocF32          *foc = &model->foc;
    IfxStdIf_Pos_RawAngle electricalAngle;

    foc->ref = input->ref.current;

    /* Stator current inputs -----------------------------------------------------------*/

    electricalAngle = model->electricalAngle;
    Ifx_FocF32_stepIn(foc, electricalAngle, input->state.currents);

    if (model->feedForwardEnabled)
    {
        Ifx_FocF32_feedForward(foc, Ifx_MotorModelPmsmF32_decoupling(model));
    }

    electricalAngle         = model->electricalAngle + Ifx_F32_MotorModel_getElectricalAngleDelta(model->electricalSpeed, model->controlPeriod);
    mab                     = Ifx_FocF32_stepOut(foc, electricalAngle);
#if IFX_FOCF32_VOLTAGE_IN_VOLTS
    mab                     = Ifx_SvmF32_getModIndex(mab, model->vdc); // convert to mod index if needed.
#endif
    output->modulationIndex = mab;
}


/** \brief Torque control step for salient motor (IPMSM)
 * \ingroup mod_PmsmVec
 */
static void Ifx_MotorModelPmsmF32_torqueStepSalient(Ifx_MotorModelPmsmF32 *model, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output)
{
    Ifx_MotorModelPmsmF32_Data *p      = &model->params;
    float32                       deltaL = p->ld - p->lq;
    float32                       fluxM  = p->fluxM;
    sint16                        n;
    float32                       p_1_5  = 1.5 * p->polePair;
    float32                       iq_mtpa, id_mtpa;
    cfloat32                      idqRef;

    float32                       T_ref = input->ref.torque;

    /* Actual torque (estimated) -------------------------------------------------------*/
    output->torque = p_1_5 * model->foc.idq.imag * (fluxM + model->foc.idq.real * deltaL);

    {   /* MAXIMUM TORQUE PER AMPERE (MTPA) ------------------------------------------- */
        float32 Tn_ref, Tn_f;
        float32 deltaL_sq, fluxM_sq;
        float32 Iq_n, Iq_n_3;
        float32 f_n, df_n;

        Tn_ref    = (T_ref < 0) ? (-T_ref / p_1_5) : (T_ref / p_1_5);

        deltaL_sq = deltaL * deltaL;

        Tn_f      = Tn_ref * fluxM;
        Iq_n      = Tn_ref / (p_1_5 * fluxM);

        if (Iq_n > 0)
        {
            for (n = 0; n < 3; n++)
            {
                Iq_n_3 = Iq_n * Iq_n * Iq_n;

                f_n    = (Iq_n * (Iq_n_3 * deltaL_sq + Tn_f)) - (Tn_ref * Tn_ref);
                df_n   = (4 * Iq_n_3 * deltaL_sq) + Tn_f;
                Iq_n   = Iq_n - (f_n / df_n);
            }
        }

        iq_mtpa  = Iq_n;

        fluxM_sq = (fluxM * fluxM);
        id_mtpa  = (fluxM - __sqrtf(fluxM_sq + (4 * deltaL_sq * Iq_n * Iq_n))) / (-2 * deltaL);
    }

    if (model->fieldWeakeningEnabled)
    {   /* FIELD-WEAKENING ------------------------------------------------------------ */
        // Maximum flux from nominal DC voltage, rotation speed and correction-feedback */
        float32 absSpeed   = __absf(model->electricalSpeed) + 1.0e-9;
        float32 fluxMaxAbs = model->vdc / (IFX_SQRT_THREE / IFX_FOCF32_VDQ_THRESHOLD) / absSpeed;
        float32 fluxMax    = __maxf(fluxMaxAbs + model->foc.fw.fbValue, 0.0);
        model->fluxMax = fluxMax;
        float32 fluxMax_sq = fluxMax * fluxMax;

        // Q-axis flux
        float32 Lq       = p->lq;
        float32 Ld       = p->ld;
        float32 fluxQ    = Lq * iq_mtpa;
        float32 fluxQ_sq = fluxQ * fluxQ;

        // Calculate Id_fw from the following conditions:
        //     fluxQ_sq  + fluxD_sq          <= fluxMax_sq
        //     (Lq*Iq)^2 + (fluxM + Ld*Id)^2 <= fluxMax_sq
        //     Id <= -fluxPM /Ld + sqrtf(fluxMax_sq - fluxQ_sq)/Ld;

        float32 id_fw;
        float32 deltaFlux_sq = fluxMax_sq - fluxQ_sq;

        id_fw = (deltaFlux_sq > 0) ? ((-fluxM + __sqrtf(deltaFlux_sq)) / Ld) : (-fluxM / Ld);

        float32 deltaId = id_fw - id_mtpa;

        if (deltaId < 0)
        {
            // We are in field-weakening, check whether torque should be reduced
            float32 idqLimit = Ifx_FocF32_getIdqLimit(&model->foc);
            id_fw   = __maxf(id_fw, -idqLimit);
            iq_mtpa = (T_ref < 0) ? -iq_mtpa : iq_mtpa;
            float32 iq_fw    = iq_mtpa;
            float32 fluxD    = ((Ld * id_fw) + fluxM);
            float32 fluxD_sq = fluxD * fluxD;

            deltaFlux_sq = fluxMax_sq - fluxD_sq;
            iq_fw        = (deltaFlux_sq > 0) ? (__sqrtf(deltaFlux_sq) / Lq) : 0;

            float32 deltaIq       = (-iq_mtpa * deltaId * deltaL) / ((id_fw * deltaL) + fluxM);
            float32 iq_corr       = iq_mtpa + deltaIq;

            boolean reducedTorque = (iq_corr < iq_fw);
            iq_fw = reducedTorque ? iq_corr : iq_fw;
            //if (reducedTorque) __debug();

            iq_fw       = (iq_fw < iq_mtpa) ? iq_fw : iq_mtpa;
            iq_fw       = __minf(__sqrtf(__sqrf(idqLimit) - __sqrf(id_fw)), iq_fw);

            idqRef.real = id_fw;
            idqRef.imag = (T_ref < 0) ? -iq_fw : iq_fw;

            model->fw   = idqRef;
        }
        else
        {
            idqRef.real = id_mtpa;
            idqRef.imag = (T_ref < 0) ? -iq_mtpa : iq_mtpa;
        }
    }
    else
    {
        idqRef.real = id_mtpa;
        idqRef.imag = (T_ref < 0) ? -iq_mtpa : iq_mtpa;
    }

    /* Transfer the current references to current controller (FOC) block ---------------*/
    input->ref.current = idqRef;
}


/** \brief Torque control step using standard algorithm.
 *
 * Used for non-salient motor (with L = Lq = Ld).
 *
 * Below base speed: Id = 0.
 * Above base speed: FluxM/L < Id < 0.
 *
 * \ingroup mod_PmsmVec
 */
static void Ifx_MotorModelPmsmF32_torqueStepStandard(Ifx_MotorModelPmsmF32 *model, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output)
{
    Ifx_MotorModelPmsmF32_Data *p     = &model->params;
    cfloat32                      idqRef;
    float32                       fluxM = p->fluxM;
    float32                       Kt    = 1.5 * p->polePair * fluxM;
    float32                       Ld    = p->ld;
    float32                       Lq    = p->lq;
    float32                       T_ref, idqLimit;

    /* Actual torque (estimated) -------------------------------------------------------*/
    output->torque = Kt * model->foc.idq.imag;

    /* Ramp the torque reference. ----------------------------------------------------- */
    {
        T_ref            = input->ref.torque;
        idqLimit         = Ifx_FocF32_getIdqLimit(&model->foc);
        model->mtpa.imag = __saturatef(T_ref / Kt, -idqLimit, idqLimit);
        model->mtpa.real = 0;
    }

    if (model->fieldWeakeningEnabled)
    /* Field-weakening for standard motor (SPMSM) with numeric algorithm -------------- */
    {
        float32 iq_mtpa = model->mtpa.imag;
        float32 id_mtpa = model->mtpa.real;

        // Maximum flux from nominal DC voltage, rotation speed and correction-feedback */
        float32 absSpeed   = __absf(model->electricalSpeed) + 1.0e-9;
        float32 fluxMaxAbs = model->vdc / (IFX_SQRT_THREE / IFX_FOCF32_VDQ_THRESHOLD) / absSpeed;
        float32 fluxMax    = __maxf(fluxMaxAbs + model->foc.fw.fbValue, 0.0);
        model->fluxMax = fluxMax;
        float32 fluxMax_sq = fluxMax * fluxMax;

        // Q-axis flux
        float32 fluxQ    = Lq * iq_mtpa;
        float32 fluxQ_sq = fluxQ * fluxQ;

        // Calculate Id_fw from the following conditions:
        //     fluxQ_sq  + fluxD_sq          <= fluxMax_sq
        //     (Lq*Iq)^2 + (fluxM + Ld*Id)^2 <= fluxMax_sq
        //     Id <= -fluxPM /Ld + sqrtf(fluxMax_sq - fluxQ_sq)/Ld;

        float32 id_fw;
        float32 deltaFlux_sq = fluxMax_sq - fluxQ_sq;

        id_fw = (deltaFlux_sq > 0) ? ((-fluxM + __sqrtf(deltaFlux_sq)) / Ld) : (-fluxM / Ld);

        float32 deltaId = id_fw - id_mtpa;

        if (deltaId < 0)
        {
            float32 idqLimit = Ifx_FocF32_getIdqLimit(&model->foc);
            // We are in field-weakening, check whether torque should be reduced
            id_fw   = __maxf(id_fw, -idqLimit);
            iq_mtpa = (T_ref < 0) ? -iq_mtpa : iq_mtpa;
            float32 iq_fw    = iq_mtpa;
            float32 fluxD    = ((Ld * id_fw) + fluxM);
            float32 fluxD_sq = fluxD * fluxD;

            deltaFlux_sq = fluxMax_sq - fluxD_sq;
            iq_fw        = (deltaFlux_sq > 0) ? (__sqrtf(deltaFlux_sq) / Lq) : 0;
            iq_fw        = (iq_fw < iq_mtpa) ? iq_fw : iq_mtpa;
            iq_fw        = __minf(__sqrtf(__sqrf(idqLimit) - __sqrf(id_fw)), iq_fw);

            idqRef.real  = id_fw;
            idqRef.imag  = (T_ref < 0) ? -iq_fw : iq_fw;
        }
        else
        {
            idqRef.real = id_mtpa;
            idqRef.imag = iq_mtpa;
        }

        model->fw = idqRef;
    }
    else
    {
        idqRef = model->mtpa;
    }

    /* Transfer the current references to current controller (FOC) block ---------------*/
    input->ref.current = idqRef;
}


static IfxStdIf_MotorModelF32_Param Ifx_MotorModelPmsmF32_getParam(Ifx_MotorModelPmsmF32 *model)
{
    IfxStdIf_MotorModelF32_Param param;
    /* Ensure the param is reset to zeros */
    memset(&param, 0, sizeof(IfxStdIf_MotorModelF32_Param));
    param.fluxM    = model->params.fluxM;
    param.iStall   = model->params.iStall;
    param.ld       = model->params.ld;
    param.lq       = model->params.lq;
    param.polePair = model->params.polePair;
    param.rs       = model->params.rs;
    return param;
}


static void Ifx_MotorModelPmsmF32_setTorqueControl(Ifx_MotorModelPmsmF32 *model, boolean enabled)
{
    model->torqueControlEnabled = enabled;
}


static boolean Ifx_MotorModelPmsmF32_isTorqueControlEnabled(Ifx_MotorModelPmsmF32 *model)
{
    return model->torqueControlEnabled;
}


static void Ifx_MotorModelPmsmF32_execute(Ifx_MotorModelPmsmF32 *model, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output)
{
    if (model->torqueControlEnabled)
    {
        Ifx_MotorModelPmsmF32_torqueStepStandard(model, input, output);
    }

    model->electricalSpeed = Ifx_F32_MotorModel_getElectricalSpeed(model->params.polePair, input->state.mechanicalSpeed);
    model->electricalAngle = Ifx_F32_MotorModel_getElectricalAngle(model->electricalAngleConst, input->state.rawMechanicalAngle);
    model->vdc             = input->state.vdc;
    Ifx_MotorModelPmsmF32_currentStep(model, input, output);
}


static void Ifx_MotorModelPmsmF32_executeSalient(Ifx_MotorModelPmsmF32 *model, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output)
{
    if (model->torqueControlEnabled)
    {
        Ifx_MotorModelPmsmF32_torqueStepSalient(model, input, output);
    }

    model->electricalSpeed = Ifx_F32_MotorModel_getElectricalSpeed(model->params.polePair, input->state.mechanicalSpeed);
    model->electricalAngle = Ifx_F32_MotorModel_getElectricalAngle(model->electricalAngleConst, input->state.rawMechanicalAngle);
    model->vdc             = input->state.vdc;
    Ifx_MotorModelPmsmF32_currentStep(model, input, output);
}

void Ifx_MotorModelPmsmF32_setCurrentControllers(Ifx_MotorModelPmsmF32 *model, Ifx_FocF32_Pic_Config *piD, Ifx_FocF32_Pic_Config*piQ)
{
	Ifx_FocF32_Config focConfig;

	Ifx_FocF32_getConfig(&model->foc, &focConfig, model->controlPeriod);

	focConfig.piD = *piD;
	focConfig.piQ = *piQ;

    Ifx_FocF32_calcParams(&model->focParams, &focConfig, model->controlPeriod);

}


boolean Ifx_MotorModelPmsmF32_stdIfMotorModelInit(IfxStdIf_MotorModelF32 *stdif, Ifx_MotorModelPmsmF32 *driver)
{
    /* Ensure the stdif is reset to zeros */
    memset(stdif, 0, sizeof(IfxStdIf_MotorModelF32));

    /* Set the driver */
    stdif->driver = driver;

    /* *INDENT-OFF* Note: this file was indented manually by the author. */
    /* Set the API link */

	stdif->getParam          =(IfxStdIf_MotorModelF32_GetParam   )&Ifx_MotorModelPmsmF32_getParam;
	stdif->setTorqueControl          =(IfxStdIf_MotorModelF32_SetTorqueControl   )&Ifx_MotorModelPmsmF32_setTorqueControl;
	stdif->isTorqueControlEnable          =(IfxStdIf_MotorModelF32_IsTorqueControlEnabled   )&Ifx_MotorModelPmsmF32_isTorqueControlEnabled;
	if (driver->type == Ifx_F32_MotorModel_Type_pmsmSalient)
	{
		stdif->execute          =(IfxStdIf_MotorModelF32_Execute   )&Ifx_MotorModelPmsmF32_executeSalient;
	}
	else// if (driver->type == Ifx_F32_MotorModel_Type_pmsmStandard)
	{
		stdif->execute          =(IfxStdIf_MotorModelF32_Execute   )&Ifx_MotorModelPmsmF32_execute;
	}
	stdif->reset            =(IfxStdIf_MotorModelF32_Reset     )&Ifx_MotorModelPmsmF32_reset;
	stdif->setControlPeriod            =(IfxStdIf_MotorModelF32_SetControlPeriod     )&Ifx_MotorModelPmsmF32_setControlPeriod;
	stdif->setCurrentControllers            =(IfxStdIf_MotorModelF32_SetCurrentControllers     )&Ifx_MotorModelPmsmF32_setCurrentControllers;
	stdif->setPositionSensorResolution =(IfxStdIf_MotorModelF32_SetPositionSensorResolution     )&Ifx_MotorModelPmsmF32_setPositionSensorResolution;
    /* *INDENT-ON* */

    return TRUE;
}
