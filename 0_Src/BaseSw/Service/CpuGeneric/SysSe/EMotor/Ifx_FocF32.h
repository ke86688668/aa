/**
 * \file Ifx_FocF32.h
 * \brief Field Oriented Control.
 * \ingroup motor_control_foc
 *
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

/**
 * \defgroup motor_control_foc Field Oriented Control (FOC)
 *
 * This module implements the Field Oriented Control function.
 *
 * The F.O.C. consist of controlling the stator currents in the rotor reference
 * frame which simplify considerably the complexity of the mathematical machine model.
 *
 * \par Definition of the stationary reference frames
 * The stationary reference frames are frames fixed to the stator. Two stationary
 * reference frames are use in the FOC:
 *  - the three phases stationary reference frame \f$ (a, b, c) \f$ which
 *    vector a is aligned with the stator phase U,
 *    vector b is aligned with the stator phase V,
 *    and vector c is aligned with the stator phase W.
 *  - the two phases stationary reference frame \f$ (\alpha, \beta) \f$ which
 *    vector \f$ \alpha \f$ is aligned with the stator phase U,
 *    and vector \f$ \beta \f$ is in quadrature with the \f$ \alpha \f$ vector.
 * \par Definition of the rotor reference frame
 * The rotor reference frame \f$ (d, q) \f$ is a frame fixed to the rotor flux,
 * which vector d is aligned with the rotor flux vector,
 * and vector q is in quadrature with the d vector. The \f$ (d, q) \f$ reference frame is
 * rotating at the same speed as the rotor flux at speed \f$ \omega_s \f$, where
 * \f$ \omega_s \f$ is the synchronous speed.
 * The d current id is named direct current and is responsible for the rotor
 * magnetisation, and the q current iq is the quadrature current and is responsible
 * for the mechanical torque. The relation between the mechanical speed and the rotor flux
 * speed is \f$ \omega = \omega_s + \omega_{slip} \f$. Note that in case of synchronous
 * motor, the slip speed \f$ \omega_{slip} \f$ is null, and therefore
 * \f$ \omega = \omega_s \f$
 *
 * \image html "SrvSw.SysSe.EMotor[ReferenceFrames].svg" "Reference frames"
 *
 * \par FOC Description
 * The rotating stator current vector from the \f$ (a, b, c) \f$ reference frame
 * is transformed to a fix vector in the \f$ (d, q) \f$ frame by the
 * \ref library_srvsw_sysse_emotor_clarke "Clarke" and \ref library_srvsw_sysse_emotor_park "Park" transformations.
 * Then the current is controlled using PI controller in the \f$ (d, q) \f$ frame, the
 * results is transformed back to the \f$ (a, b, c) \f$ frame using the
 * \ref library_srvsw_sysse_emotor_svm "space vector modulation" to generate the PWM.
 *
 * \image html "SrvSw.SysSe.EMotor.Foc.svg" "Field Oriented Control"
 *
 * \ingroup library_srvsw_sysse_emotor
 */

#if !defined(IFX_FOCF32)
#define IFX_FOCF32 1

//________________________________________________________________________________________
// INCLUDES
#include "SysSe/Math/Ifx_LutLSincosF32.h"
#include "Ifx_Cfg.h"

//________________________________________________________________________________________
// DEFINES
#ifndef IFX_FOCF32_DEBUG
#define IFX_FOCF32_DEBUG            (0)
#endif

#ifndef IFX_FOCF32_DEBUG_2
#define IFX_FOCF32_DEBUG_2          (0)
#endif

#ifndef IFX_FOCF32_VDQ_THRESHOLD
#define IFX_FOCF32_VDQ_THRESHOLD    (0.9)          /**< \brief Default DQ voltage threshold for field-weakening control */
#endif

#ifndef IFX_FOCF32_VOLTAGE_IN_VOLTS
#define IFX_FOCF32_VOLTAGE_IN_VOLTS (0)            /**< \brief If not ZERO, the unit of voltages (mab, mdq, mdqFf) in FOC block are in Volts, else in modulation index */
#endif

//________________________________________________________________________________________
// HELPER MACRO
#if IFX_FOCF32_VOLTAGE_IN_VOLTS == 0
#define IFX_FOCF32_K_FACTOR(vdc)          (1.0 / ((vdc) * IFX_TWO_OVER_PI)) /** Factor used to convert Volt to modulation index */
#else
#define IFX_FOCF32_K_FACTOR(vdc)          (1.0)
#endif

/** \brief Calculate the integral gain (ki) for current controller.
 * \param bw Controller bandwidth in Hz. The current time-constant corresponds to T = 1/(2*pi*bw)
 * \param vdc Nominal DC link voltage in Volt
 * \param L phase inductance in Henry
 * \param R phase resistance in Ohm
 */
#define IFX_FOCF32_CALC_KI(bw, vdc, L, R) ((R) / (1.0 / (2.0 * IFX_PI * bw))) * IFX_FOCF32_K_FACTOR(vdc)

/** \brief Calculate the proportional gain (kp) for current controller.
 * \param bw Controller bandwidth in Hz. The current time-constant corresponds to T = 1/(2*pi*bw)
 * \param vdc Nominal DC link voltage in Volt
 * \param L phase inductance in Henry
 * \param R phase resistance in Ohm
 */
#define IFX_FOCF32_CALC_KP(bw, vdc, L, R) IFX_FOCF32_CALC_KI(bw, vdc, L, R) * IFX_FOCF32_CALC_TSTATOR(L, R)

/** \brief Stator time constant in seconds
 * \param L phase inductance in Henry
 * \param R phase resistance in Ohm
 */
#define IFX_FOCF32_CALC_TSTATOR(L, R)     ((L) / (R))

//________________________________________________________________________________________
// DATA STRUCTURES

typedef struct
{
    float32 mdqThres;       /**< \brief Magnitude threshold of DQ voltage, for field-weakening */
    float32 mdqMargin;      /**< \brief Margin of DQ voltage, difference from mdqThreshold */
    float32 fbValue;        /**< \brief Field-weakening feedback, accumulated from mdqFwMargin (zero or positive)*/
} Ifx_FocF32_FwSig;

/** \brief PI controller configuration */
typedef struct
{
    float32 kp;             /**< \brief Direct (Proportional) gain */
    float32 ki;             /**< \brief Integrator gain */
} Ifx_FocF32_Pic_Config;

/** \brief FOC configuration */
typedef struct
{
    Ifx_FocF32_Pic_Config piD;                   /**< \brief D-axis PI controller configuration */
    Ifx_FocF32_Pic_Config piQ;                   /**< \brief Q-axis PI controller configuration */
    float32                vdcNom;                /**< \brief Nominal DC-link voltage in V */
    float32                kiFw;                  /**< \brief Gain of field-weakening feedback */
    float32                fwFbMax;               /**< \brief Maximum value for field-weakening feedback */
    boolean                fieldWeakeningEnabled; /**< \brief Specifies if the field weakening is enabled */
} Ifx_FocF32_Config;

/** \brief PI controller run-time parameters */
typedef struct
{
    float32 kp;             /**< \brief Direct (Proportional) gain */
    float32 KiDt;           /**< \brief Integrator gain multiplied with Ts/2.0 */
} Ifx_FocF32_PicParams;

/** \brief PI controller signals
 * FIXME make Pic_Signals an own library
 **/
typedef struct
{
    float32 uk;             /**< \brief integrator value */
    float32 ik;             /**< \brief error value (previous) */
} Pic_Signals;

/** \brief FOC run-time parameters */
typedef struct
{
    Ifx_FocF32_PicParams piD;                   /**< \brief D-axis PI controller parameter */
    Ifx_FocF32_PicParams piQ;                   /**< \brief Q-axis PI controller parameter */
    float32               vdcNom;                /**< \brief Nominal DC-link voltage in V */
    float32               kiFwDt;                /**< \brief Gain of field-weakening feedback multipied with Ts */
    float32               fwFbMax;               /**< \brief Maximum value for field-weakening feedback */
    boolean               fieldWeakeningEnabled; /**< \brief Specifies if the field weakening is enabled */
} Ifx_FocF32_Params;

/** \brief FOC electrical angle */
typedef struct
{
    Ifx_Lut_FxpAngle input;         /**< \brief Used for input step */
    Ifx_Lut_FxpAngle output;        /**< \brief Used for output step */
    Ifx_Lut_FxpAngle offset;        /**< \brief Offset input and output */
} Ifx_FocF32_ElAngle;

/** \brief Ifx_FocF32 object definition. */
typedef struct
{
    cfloat32                                                ref;         /**< \brief FOC reference currents in the DQ frame [in Apeak] */
    float32                                                 idqLimit;    /**< \brief FOC reference current limit [in Apeak] */
    boolean                                                 vdqLimitHit; /**< \brief Indicator that voltage is saturated */
    Pic_Signals                                             piD;         /**< \brief PI controller for the D axis */
    Pic_Signals                                             piQ;         /**< \brief PI controller for the Q axis */
    cfloat32                                                idq;         /**< \brief Output of the Park transformation (debug information) */
    cfloat32                                                mdqFf;       /**< \brief Feed-forward DQ, e.g. for decoupling, active-damping, etc */
    cfloat32                                                mdq;         /**< \brief Input of the reverse park transformation */
    float32                                                 mdqMag;      /**< \brief Magnitude of DQ voltage */
/* FIXME made redundant of Ifx_F32_MotorModel.vdc*/ float32 vdcNom;      /**< \brief Nominal DC-link voltage in V */
    Ifx_FocF32_FwSig                                       fw;          /**< \brief Field-weakening support */
#if IFX_FOCF32_DEBUG
    cfloat32                                                mab;         /**< \brief Output of the reverse park transformation */
    float32                                                 iuvw[3];     /**< \brief Stator currents in the UVW frame (debug information) */
    cfloat32                                                iab;         /**< \brief Output of the Clarke transformation */
    cfloat32                                                vdq;         /**< \brief Input of the reverse park transformation in Volts */
#if IFX_FOCF32_DEBUG_2
    sint32                                                  marg;        /**< \brief Modulation angle */
#endif
#endif
    Ifx_FocF32_ElAngle       elAngle;                                   /**< \brief Electrical angle info (for debug) */

    const Ifx_FocF32_Params *params;                                    /**< \brief Runtime parameters */
} Ifx_FocF32;

//________________________________________________________________________________________
// FUNCTION PROTOTYPES

/** \addtogroup motor_control_foc
 * \{ */

/** \name Initialisation functions
 * \{ */
IFX_EXTERN void    Ifx_FocF32_calcParams(Ifx_FocF32_Params *params, const Ifx_FocF32_Config *config, float32 Ts);
IFX_EXTERN void    Ifx_FocF32_getConfig(Ifx_FocF32 *self, Ifx_FocF32_Config *config, float32 Ts);
IFX_EXTERN boolean Ifx_FocF32_init(Ifx_FocF32 *self, const Ifx_FocF32_Params *params);
/** \} */
/** \name Status functions
 * \{ */
IFX_EXTERN cfloat32 Ifx_FocF32_getIdq(Ifx_FocF32 *self);
IFX_EXTERN cfloat32 Ifx_FocF32_getIdqRef(Ifx_FocF32 *self);
IFX_EXTERN float32  Ifx_FocF32_getIdqLimit(Ifx_FocF32 *self);
/** \} */
/** \name Operational functions
 * \{ */
IFX_INLINE void     Ifx_FocF32_feedForward(Ifx_FocF32 *self, cfloat32 mdqFf);
IFX_EXTERN void     Ifx_FocF32_reset(Ifx_FocF32 *self);
IFX_EXTERN void     Ifx_FocF32_setParams(Ifx_FocF32 *self, const Ifx_FocF32_Params *params);
IFX_EXTERN void     Ifx_FocF32_setIdqLimit(Ifx_FocF32 *self, float32 limit);
IFX_EXTERN void     Ifx_FocF32_setIdqRef(Ifx_FocF32 *self, cfloat32 ref);
IFX_EXTERN void     Ifx_FocF32_stepIn(Ifx_FocF32 *self, Ifx_Lut_FxpAngle elAngle, const float32 *currents);
IFX_EXTERN cfloat32 Ifx_FocF32_stepOut(Ifx_FocF32 *self, Ifx_Lut_FxpAngle elAngle);
/** \} */

/** \} */

//________________________________________________________________________________________
// INLINE FUNCTIONS

/**
 * \param self Specifies the field oriented control object.
 * \param mdqFf Feed-forward voltage, e.g. for decoupling, active damping, etc.
 */
IFX_INLINE void Ifx_FocF32_feedForward(Ifx_FocF32 *self, cfloat32 mdqFf)
{
    self->mdqFf = mdqFf;
}


#endif
