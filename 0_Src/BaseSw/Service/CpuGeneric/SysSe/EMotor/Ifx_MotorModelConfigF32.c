/**
 * \file Ifx_MotorModelConfigF32.c
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */
#include "SysSe/EMotor/Ifx_MotorModelConfigF32.h"
#include "configuration.h"
#include <string.h>

#define MOTOR_SHELL_PREFIX "motor"

boolean Ifx_MotorModelConfigF32_cmdPrint(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdType(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdImax(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdIStall(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdCpi(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdPole(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdLdq(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdRs(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdStatorMax(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdRr(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdLx(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdFlux(pchar args, void *data, IfxStdIf_DPipe *io);

boolean Ifx_MotorModelConfigF32_cmdSpeedMax(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdSpi(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdTMax(pchar args, void *data, IfxStdIf_DPipe *io);
boolean Ifx_MotorModelConfigF32_cmdTRate(pchar args, void *data, IfxStdIf_DPipe *io);

/* *INDENT-OFF* */
Ifx_Shell_Command Ifx_g_MotorModelConfigF32[] =
{
    {MOTOR_SHELL_PREFIX, "     : Motor configuration"
            , NULL_PTR, NULL_PTR               },
	{"print", " : print the motor configuration"ENDL
			"/s motor print: print the motor configuration"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdPrint       },
	{"type", " : set the motor type"ENDL
		"/s motor type <type>: set the motor type"ENDL
			"/p <type>: Motor type. Range={0=>PMSM standard, 1=>PMSM salient, 2=>ACIM, 3=>open loop}"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdType       },
	{"imax", " : set the max phase current"ENDL
		"/s motor imax <value>: set the max phase current"ENDL
			"/p <value>: Max phase current in A"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdImax       },
	{"istall", " : set the stall current"ENDL
		"/s motor istall <value>: set the stall current"ENDL
			"/p <value>: Max phase current in A"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdImax       },
	{"cpi", " : set the current PI controller"ENDL
		"/s motor cpi <select> <kp> <ki>: set the current PI controller"ENDL
			"/p <select>: Select Id or Iq. range={id, iq}"ENDL
			"/p <kp>: proportional gain"ENDL
			"/p <ki>: integrator gain"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdCpi       },
	{"pole", " : set number of poles"ENDL
		"/s motor pole <value>: set the number of poles"ENDL
			"/p <value>: Number of poles"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdPole       },
	{"ldq", " : set the stator inductance (PMSM only)"ENDL
		"/s motor ldq <ld> [<lq>]: set the stator inductance"ENDL
			"/p <ld>: Stator inductance Ld in H"ENDL
			"/p <lq>: Stator inductance Lq in H. if ommited, the value is set to be equal to Ld"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdLdq       },
	{"rs", " : set the stator resistance"ENDL
		"/s motor rs <value>: set the stator resistance"ENDL
			"/p <value>: Stator resistance in Ohm"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdRs       },
	{"statormax", " : set the maximum stator electrical speed (ACIM only)"ENDL
		"/s motor statormax <value>: set the maximum stator electrical speed"ENDL
			"/p <value>: Mmaximum stator electrical speed in rad/s"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdStatorMax       },
	{"rr", " : set the rotor resistance (ACIM only)"ENDL
		"/s motor rr <value>: set the rotor resistance"ENDL
			"/p <value>: rotor resistance in Ohm"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdRr       },
	{"lx", " : set the ACIM inductance (ACIM only)"ENDL
		"/s motor lx <ls> <lr> <lm>: set the ACIM inductances"ENDL
			"/p <ls>: Motor stator inductance in H"ENDL
			"/p <lr>: Motor rotor inductance in H"ENDL
			"/p <lm>: Motor mutual inductance in H"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdLx       },
	{"flux", " : set the rotor flux in V.s (ACIM only)"ENDL
		"/s motor flux <value>: set the rotor flux in V.s"ENDL
			"/p <value>: rotor flux in V.s"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdFlux       },

	{"speedmax", " : set the motor max mechanical speed"ENDL
		"/s motor speedmax <value>: set the motor max mechanical speed"ENDL
			"/p <value>: max mechanical speed in rpm"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdSpeedMax       },
	{"spi", " : set the speed PI controller"ENDL
		"/s motor spi <select> <kp> <ki>: set the speed PI controller"ENDL
			"/p <kp>: proportional gain"ENDL
			"/p <ki>: integrator gain"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdSpi       },
	{"tmax", " : set the motor max torque"ENDL
		"/s motor tmax <value>: set the motor max torque"ENDL
			"/p <value>: Max torque in Nm"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdTMax       },
	{"trate", " : set the motor torque change rate"ENDL
		"/s motor trate <value>: set the motor torque change rate"ENDL
			"/p <value>: torque change rate in Nm/s"ENDL
			, NULL_PTR, &Ifx_MotorModelConfigF32_cmdTRate       },
    IFX_SHELL_COMMAND_LIST_END,
};
/* *INDENT-ON* */

boolean Ifx_MotorModelConfigF32_cmdPrint(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;

    Ifx_MotorModelConfigF32_printConfig(driver, io);
    result = TRUE;

    return result;
}


static void Ifx_MotorModelConfigF32_update(Ifx_MotorModelConfigF32 *driver, IfxStdIf_DPipe *io)
{
    if (!Ifx_MotorModelConfigF32_updateFile(driver))
    {
        IfxStdIf_DPipe_print(io, "Error while updating configuration file"ENDL);
    }
}


boolean Ifx_MotorModelConfigF32_cmdType(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    uint32                     value;

    if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
    {
        if (value == 0)
        {
            driver->data.type = Ifx_F32_MotorModel_Type_pmsmStandard;
        }
        else if (value == 1)
        {
            driver->data.type = Ifx_F32_MotorModel_Type_pmsmSalient;
        }
        else if (value == 2)
        {
            driver->data.type = Ifx_F32_MotorModel_Type_acim;
        }
        else if (value == 3)
        {
            driver->data.type = Ifx_F32_MotorModel_Type_openLoop;
        }

        Ifx_MotorModelConfigF32_update(driver, io);
        result = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdImax(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    value;

    if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
    {
        driver->data.iMax = value;
        Ifx_MotorModelConfigF32_update(driver, io);
        result            = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdIStall(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    value;

    if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
    {
        driver->data.iStall = value;
        Ifx_MotorModelConfigF32_update(driver, io);
        result              = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdCpi(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    kp;
    float32                    ki;

    if (Ifx_Shell_matchToken(&args, "id") != FALSE)
    {
        if ((Ifx_Shell_parseFloat32(&args, &kp) != FALSE)
            && (Ifx_Shell_parseFloat32(&args, &ki) != FALSE))
        {
            driver->data.idKp = kp;
            driver->data.idKi = ki;
            Ifx_MotorModelConfigF32_update(driver, io);
            result            = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "iq") != FALSE)
    {
        if ((Ifx_Shell_parseFloat32(&args, &kp) != FALSE)
            && (Ifx_Shell_parseFloat32(&args, &ki) != FALSE))
        {
            driver->data.iqKp = kp;
            driver->data.iqKi = ki;
            Ifx_MotorModelConfigF32_update(driver, io);
            result            = TRUE;
        }
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdPole(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    uint32                     value;

    if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
    {
        driver->data.polePair = (uint8)(value / 2);
        Ifx_MotorModelConfigF32_update(driver, io);
        result                = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdLdq(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    ld;
    float32                    lq;

    if ((Ifx_Shell_parseFloat32(&args, &ld) != FALSE))
    {
        lq = ld;
        Ifx_Shell_parseFloat32(&args, &lq);

        driver->data.ld = ld;
        driver->data.lq = lq;
        Ifx_MotorModelConfigF32_update(driver, io);
        result          = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdRs(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    value;

    if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
    {
        driver->data.rs = value;
        Ifx_MotorModelConfigF32_update(driver, io);
        result          = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdStatorMax(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    value;

    if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
    {
        driver->data.statorMax = value;
        Ifx_MotorModelConfigF32_update(driver, io);
        result                 = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdRr(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    value;

    if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
    {
        driver->data.rr = value;
        Ifx_MotorModelConfigF32_update(driver, io);
        result          = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdLx(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    ls;
    float32                    lr;
    float32                    lm;

    if ((Ifx_Shell_parseFloat32(&args, &ls) != FALSE)
        && (Ifx_Shell_parseFloat32(&args, &lr) != FALSE)
        && (Ifx_Shell_parseFloat32(&args, &lm) != FALSE)
        )
    {
        driver->data.ls = ls;
        driver->data.lr = lr;
        driver->data.lm = lm;
        Ifx_MotorModelConfigF32_update(driver, io);
        result          = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdFlux(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    value;

    if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
    {
        driver->data.fr = value;
        Ifx_MotorModelConfigF32_update(driver, io);
        result          = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdSpeedMax(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    value;

    if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
    {
        driver->data.speedMax = value;
        Ifx_MotorModelConfigF32_update(driver, io);
        result                = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdSpi(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    kp;
    float32                    ki;

    if ((Ifx_Shell_parseFloat32(&args, &kp) != FALSE)
        && (Ifx_Shell_parseFloat32(&args, &ki) != FALSE))
    {
        driver->data.speedKp = kp;
        driver->data.speedKi = ki;
        Ifx_MotorModelConfigF32_update(driver, io);
        result               = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdTMax(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    value;

    if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
    {
        driver->data.torqueMax = value;
        Ifx_MotorModelConfigF32_update(driver, io);
        result                 = TRUE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_cmdTRate(pchar args, void *data, IfxStdIf_DPipe *io)
{
    Ifx_MotorModelConfigF32 *driver = (Ifx_MotorModelConfigF32 *)data;
    boolean                    result = FALSE;
    float32                    value;

    if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
    {
        driver->data.torqueRate = value;
        Ifx_MotorModelConfigF32_update(driver, io);
        result                  = TRUE;
    }

    return result;
}


void Ifx_MotorModelConfigF32_initConfig(Ifx_MotorModelConfigF32_Config *config, Ifx_Efs *efs, pchar filename)
{
    config->efs      = efs;
    config->filename = filename;
}


boolean Ifx_MotorModelConfigF32_init(Ifx_MotorModelConfigF32 *driver, Ifx_MotorModelConfigF32_Config *config)
{
    uint32 i;
    driver->efs = config->efs;
    Ifx_MotorModelConfigF32_setupDefaultValue(driver);
    Ifx_MotorModelConfigF32_setFileName(driver, config->filename);

    i = 0;

    while (Ifx_g_MotorModelConfigF32[i].commandLine != NULL_PTR)
    {
        Ifx_g_MotorModelConfigF32[i].data = driver;
        i++;
    }

    return TRUE;
}


void Ifx_MotorModelConfigF32_setupDefaultValue(Ifx_MotorModelConfigF32 *driver)
{
    driver->data.valid         = TRUE;
    driver->data.fileVersion   = IFX_MOTORMODELCONFIGF32_FILE_VERSION;
    driver->data.type          = CFG_MOTOR_TYPE;
    driver->data.iMax          = CFG_MOTOR_I_MAX;
    driver->data.idKi          = CFG_MOTOR_ID_KI;
    driver->data.idKp          = CFG_MOTOR_ID_KP;
    driver->data.iqKi          = CFG_MOTOR_IQ_KI;
    driver->data.iqKp          = CFG_MOTOR_IQ_KP;
    driver->data.rs            = CFG_MOTOR_RS;
    driver->data.ld            = CFG_MOTOR_LD;
    driver->data.lq            = CFG_MOTOR_LQ;
    driver->data.kt            = CFG_MOTOR_KT;
    driver->data.iStall        = CFG_MOTOR_I_STALL;
    driver->data.polePair      = CFG_MOTOR_POLE_PAIR;

    driver->data.statorMax     = CFG_MOTOR_STATOR_MAX;
    driver->data.rr            = CFG_MOTOR_RR;
    driver->data.ls            = CFG_MOTOR_LS;
    driver->data.lr            = CFG_MOTOR_LR;
    driver->data.lm            = CFG_MOTOR_LM;
    driver->data.fr            = CFG_MOTOR_FR;

    driver->data.speedMax      = CFG_MOTOR_SPEED_MAX;
    driver->data.speedKp       = CFG_MOTOR_SPEED_KP;
    driver->data.speedKi       = CFG_MOTOR_SPEED_KI;
    driver->data.torqueMax     = CFG_MOTOR_TORQUE_MAX;
    driver->data.torqueRate    = CFG_MOTOR_TORQUE_RATE;

    driver->data.fieldWeakning = CFG_INVERTER_FIELD_WEAKENING;
}


boolean Ifx_MotorModelConfigF32_deleteFile(Ifx_MotorModelConfigF32 *driver)
{
	return Ifx_Efs_fileDelete(driver->efs, driver->filename);
}

boolean Ifx_MotorModelConfigF32_createFile(Ifx_MotorModelConfigF32 *driver)
{
    boolean      result = TRUE;
    Ifx_Efs_File file;

    if (Ifx_Efs_isLoaded(driver->efs))
    {
        /* Create the configuration file on the logic board */

        if (Ifx_Efs_fileCreate(driver->efs, driver->filename, &file))
        {
            driver->data.fileVersion = IFX_MOTORMODELCONFIGF32_FILE_VERSION;

            Ifx_Efs_fileDataCrcEnable(&file);

            if (Ifx_Efs_fileWrite(&file, 0, sizeof(driver->data), (uint8 *)&driver->data) != sizeof(driver->data))
            {
                Ifx_Efs_fileDataCrcSet(&file, (uint8 *)&driver->data);

                if (!Ifx_Efs_fileClose(&file))
                {
                    result = FALSE;
                }
            }
            else
            {
                result = FALSE;
            }
        }
        else
        {
            result = FALSE;
        }
    }
    else
    {
        result = FALSE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_updateFile(Ifx_MotorModelConfigF32 *driver)
{
    boolean      result = TRUE;
    Ifx_Efs_File file;

    if (Ifx_Efs_isLoaded(driver->efs))
    {
        /* Create the configuration file on the logic board */
        if (Ifx_Efs_fileOpen(driver->efs, driver->filename, &file))
        {
            if (Ifx_Efs_fileWrite(&file, 0, sizeof(driver->data), (uint8 *)&driver->data) != sizeof(driver->data))
            {
                Ifx_Efs_fileDataCrcSet(&file, (uint8 *)&driver->data);

                if (!Ifx_Efs_fileClose(&file))
                {
                    result = FALSE;
                }
            }
            else
            {
                result = FALSE;
            }
        }
        else
        {
            result = FALSE;
        }
    }
    else
    {
        result = FALSE;
    }

    return result;
}


boolean Ifx_MotorModelConfigF32_loadFile(Ifx_MotorModelConfigF32 *driver)
{
    boolean      result = TRUE;

    Ifx_Efs_File file;

    if (Ifx_Efs_fileExist(driver->efs, driver->filename))
    {
        if (Ifx_Efs_fileOpen(driver->efs, driver->filename, &file))
        {
            /* Get the file version */
            Ifx_MotorModelConfigF32_File_Version version;

            if (Ifx_Efs_fileRead(
                    &file,
                    offsetof(Ifx_MotorModelConfigF32_File, fileVersion),
                    sizeof(Ifx_MotorModelConfigF32_File_Version),
                    (uint8 *)&version)
                == 0)
            {
                if (version == IFX_MOTORMODELCONFIGF32_FILE_VERSION)
                {
                    if (sizeof(driver->data) == Ifx_Efs_fileSizeGet(&file))
                    {
                        if (Ifx_Efs_fileRead(
                                &file,
                                0,
                                sizeof(driver->data),
                                (uint8 *)&driver->data)
                            == 0)
                        {
                            result &= Ifx_Efs_fileDataCrcCheck(&file, (uint8 *)&driver->data);
                        }
                    }
                    else
                    {
                        return FALSE;
                    }
                }
                else
                {
                    result = FALSE; /* Unsupported version*/
                }
            }
        }
        else
        {
            result = FALSE; /* Error while opening the configuration file */
        }
    }
    else
    {
        result = FALSE;
    }

    return result;
}


void Ifx_MotorModelConfigF32_setFileName(Ifx_MotorModelConfigF32 *driver, pchar filename)
{
    memcpy(driver->filename, filename, strlen(filename));
}


void Ifx_MotorModelConfigF32_printConfig(Ifx_MotorModelConfigF32 *driver, IfxStdIf_DPipe *io)
{
    IfxStdIf_DPipe_print(io, ENDL "Motor configuration:"ENDL);

    /* Print configuration file version */
    IfxStdIf_DPipe_print(io, "- Configuration file version: %d.%d"ENDL, ((driver->data.fileVersion >> 8) & 0xFF), ((driver->data.fileVersion >> 0) & 0xFF));

    IfxStdIf_DPipe_print(io, "- Motor type              : ");
    Ifx_F32_MotorModel_printType(io, driver->data.type);
    IfxStdIf_DPipe_print(io, ENDL);
    IfxStdIf_DPipe_print(io, "- Phase current max       : %7.2f A"ENDL, (driver->data.iMax));
    IfxStdIf_DPipe_print(io, "- PI controller Id, Ki    : %7.6f"ENDL, driver->data.idKi);
    IfxStdIf_DPipe_print(io, "- PI controller Id, Kp    : %7.6f"ENDL, driver->data.idKp);
    IfxStdIf_DPipe_print(io, "- PI controller Iq, Ki    : %7.6f"ENDL, driver->data.iqKi);
    IfxStdIf_DPipe_print(io, "- PI controller Iq, Kp    : %7.6f"ENDL, driver->data.iqKp);
    IfxStdIf_DPipe_print(io, "- Phase resistance        : %7.3f Ohm"ENDL, driver->data.rs);

    if (driver->data.type == Ifx_F32_MotorModel_Type_acim)
    {
        IfxStdIf_DPipe_print(io, "- Max stator electrical speed : %7.3f rad/s"ENDL, driver->data.statorMax);
        IfxStdIf_DPipe_print(io, "- Rotor resisatnce    : %7.3f Ohm"ENDL, driver->data.rr);
        IfxStdIf_DPipe_print(io, "- Stator inductance   : %7.3f mH"ENDL, (driver->data.ls * 1000));
        IfxStdIf_DPipe_print(io, "- Rotor inductance    : %7.3f mH"ENDL, (driver->data.lr * 1000));
        IfxStdIf_DPipe_print(io, "- Mutual inductance   : %7.3f mH"ENDL, (driver->data.lm * 1000));
        IfxStdIf_DPipe_print(io, "- Flux                : %7.3f V/s"ENDL, driver->data.fr);
    }
    else
    {
        IfxStdIf_DPipe_print(io, "- Phase inductance Ld     : %7.5f mH"ENDL, (driver->data.ld * 1000));
        IfxStdIf_DPipe_print(io, "- Phase inductance Lq     : %7.5f mH"ENDL, (driver->data.lq * 1000));
        IfxStdIf_DPipe_print(io, "- Torque constant         : %7.5f Nm/Arms"ENDL, (driver->data.kt));
    }

    IfxStdIf_DPipe_print(io, "- Stall current           : %7.2f A"ENDL, driver->data.iStall);
    IfxStdIf_DPipe_print(io, "- Pole pair               : %d"ENDL, driver->data.polePair);

    IfxStdIf_DPipe_print(io, "- Speed Max               : %7.0f rpm"ENDL, driver->data.speedMax);
    IfxStdIf_DPipe_print(io, "- PI controller speed Kp  : %7.6f"ENDL, driver->data.speedKp);
    IfxStdIf_DPipe_print(io, "- PI controller speed Ki  : %7.6f"ENDL, driver->data.speedKi);
    IfxStdIf_DPipe_print(io, "- Torque max              : %7.2f Nm"ENDL, driver->data.torqueMax);
    IfxStdIf_DPipe_print(io, "- Torque rate             : %7.2f Nm/s"ENDL, driver->data.torqueRate);
    IfxStdIf_DPipe_print(io, "- Field weakening         : %s"ENDL, (driver->data.fieldWeakning ? "On" : "Off"));
}
