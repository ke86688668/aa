/**
 * \file Ifx_ParkF32.c
 * \brief Park transformation.
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */

//------------------------------------------------------------------------------
#include "Ifx_ParkF32.h"
//------------------------------------------------------------------------------
/** \brief Park transformation.
 *
 * This function converts the current from the stationary reference frame \f$ (\alpha, \beta) \f$
 * to the rotating reference frame \f$ (d, q) \f$ attached to the rotor flux.
 * \image html "SrvSw.SysSe.EMotor.Park[Transforms].png"
 *
 * \param mab Specifies the current in the stationary reference frame.
 * \param CosSin Specifies the cosinus and sinus coresponding to the angle \f$ \varphi \f$
 * between the phase U axis and the rotor flux axis.
 *
 * \return Returns the current in the rotating reference frame:
 * \f$
 *   \begin{bmatrix}
 *  i_{sd} \\
 *  i_{sq}
 *  \end{bmatrix}
 *  =
 *  \begin{bmatrix}
 *  \cos \varphi & \sin \varphi \\
 *  -\sin \varphi & \cos \varphi
 *  \end{bmatrix}
 *  *
 *  \begin{bmatrix}
 *  i_{s\alpha} \\
 *  i_{s\beta}
 *  \end{bmatrix}
 * \f$
 *
 * \ingroup library_srvsw_sysse_emotor_park
 */
cfloat32 Ifx_ParkF32_forward(const cfloat32 *mab, const cfloat32 *CosSin)
{
    cfloat32 Result;
    Result.real = (mab->real * CosSin->real) + (mab->imag * CosSin->imag);
    Result.imag = (mab->imag * CosSin->real) - (mab->real * CosSin->imag);
    return Result;
}


/** \brief Reverse Park transformation.
 *
 * This function converts the current from the rotating reference frame \f$ (d, q) \f$
 * to the stationary reference frame \f$ (\alpha, \beta) \f$.
 * \image html "SrvSw.SysSe.EMotor.Park[Transforms].png"
 *
 * \param mdq Specifies the current in the rotating reference frame.
 * \param CosSin Specifies the cosinus and sinus coresponding to the angle \f$ \varphi \f$
 * between the phase U axis and the rotor flux axis.
 *
 * \return Returns the current in the stationary reference frame:
 * \f$
 *  \begin{bmatrix}
 *  i_{s\alpha} \\
 *  i_{s\beta}
 *  \end{bmatrix}
 *  =
 *  \begin{bmatrix}
 *  \cos \varphi & -\sin \varphi \\
 *  \sin \varphi & \cos \varphi
 *  \end{bmatrix}
 *  *
 *  \begin{bmatrix}
 *  i_{sd} \\
 *  i_{sq}
 *  \end{bmatrix}
 * \f$
 * \ingroup library_srvsw_sysse_emotor_park
 */
cfloat32 Ifx_ParkF32_reverse(const cfloat32 *mdq, const cfloat32 *CosSin)
{
    cfloat32 Result;
    Result.real = (mdq->real * CosSin->real) - (mdq->imag * CosSin->imag);
    Result.imag = (mdq->real * CosSin->imag) + (mdq->imag * CosSin->real);
    return Result;
}
