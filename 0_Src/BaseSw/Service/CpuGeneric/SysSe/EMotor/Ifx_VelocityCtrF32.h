/**
 * \file Ifx_VelocityCtrF32.h
 * \brief Velocity control implementation
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 */
/** \defgroup library_srvsw_sysse_emotor_velocityctr Velocity control
 *
 * This module implements the velocity control object.
 *
 * The velocity control module is controlling the speed of the motor. This function
 * can be enabled / disabled.
 *
 * \anchor fig_VelocityCtr
 * \image html "VelocityControl.svg" "Velocity control"
 *
 * Signal definition:
 *     - Ref speed  : Motor mechanical reference speed in radian per second (rad/s)
 *     - Actual speed   : Measured motor speed in radian per second (rad/s)
 *     - Enable : speed control enable / disable input
 *     - Reset  : Reset the controller internal state
 *     - kp, ki : PI controller parameters for the velocity controller
 *     - Out min, max   : PI controller output limits
 *     - Torque Ref : Reference torque in Newton meter (N.m), input of the torque
 *       control module
 *
 *
 * \ingroup library_srvsw_sysse_emotor
 */

#if !defined(IFX_VELOCITYCTRF32_H)
#define IFX_VELOCITYCTRF32_H

//----------------------------------------------------------------------------------------
#include "SysSe/Math/Ifx_PicF32.h"
#include "Cpu/Std/IfxCpu_Intrinsics.h"
//----------------------------------------------------------------------------------------
/** \brief Velocity control object.
 */
typedef struct
{
    Ifx_PicF32 pic;      /**< \brief Speed PI controller */
    float32     ref;      /**< \brief Speed reference in rad/s */
    float32     value;    /**< \brief Actual speed in rad/s */
    float32     refLimit; /**< \brief Absolute value of the max allowed speed reference in rad/s. Range=[0, +INF] */
    boolean     enabled;  /**< \brief Speed control enable flag. TRUE: the velocity control is enabled. FALSE the velocity control is disabled */
    float32     period;   /**< \brief Speed controller sample period in seconds. */
} Ifx_VelocityCtrF32;

//----------------------------------------------------------------------------------------
/** \addtogroup library_srvsw_sysse_emotor_velocityctr
 * \{ */

void    Ifx_VelocityCtrF32_init(Ifx_VelocityCtrF32 *vc); /* FIXME add config */
float32 Ifx_VelocityCtrF32_step(Ifx_VelocityCtrF32 *vc, float32 speed);

/** \brief Reset the velocity controller.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 *
 * \return none
 */
IFX_INLINE void Ifx_VelocityCtrF32_reset(Ifx_VelocityCtrF32 *vc)
{
    vc->ref = 0;
    Ifx_PicF32_reset(&vc->pic);
}


/** \brief Enable the velocity controller.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 *
 * \return none
 */
IFX_INLINE void Ifx_VelocityCtrF32_enable(Ifx_VelocityCtrF32 *vc)
{
    vc->enabled = TRUE;
}


/** \brief Return the status of the velocity controller (Enabled / Disabled).
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 *
 * \retval TRUE Returns TRUE if the velocity controller is enabled
 * \retval FALSE Returns FALSE if the velocity controller is disabled
 */
IFX_INLINE boolean Ifx_VelocityCtrF32_isEnabled(Ifx_VelocityCtrF32 *vc)
{
    return vc->enabled;
}


/** \brief Disable the velocity controller.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 *
 * \return none
 */
IFX_INLINE void Ifx_VelocityCtrF32_disable(Ifx_VelocityCtrF32 *vc)
{
    vc->enabled = FALSE;
    Ifx_VelocityCtrF32_reset(vc);
}


/** \brief Reset the velocity controller limit flag.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 *
 * \return none
 */
IFX_INLINE void Ifx_VelocityCtrF32_resetLimitFlag(Ifx_VelocityCtrF32 *vc)
{
    Ifx_PicF32_resetLimitHit(&vc->pic);
}


/** \brief Set the velocity controller limits.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 * \param torqueLimit Specifies the PI controller output torque limit.
 *
 * \return none
 * \see Ifx_PicF32_setLimit()
 */
IFX_INLINE void Ifx_VelocityCtrF32_setTorqueLimit(Ifx_VelocityCtrF32 *vc, float32 torqueLimit)
{
    Ifx_PicF32_setLimit(&vc->pic, -torqueLimit, torqueLimit);
}


/** \brief Return the velocity controller maximum output reference limit in N.m.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 *
 * \return Return the max output limit
 */
IFX_INLINE float32 Ifx_VelocityCtrF32_getTorqueLimit(Ifx_VelocityCtrF32 *vc)
{
    return vc->pic.upper;
}


/** \brief Set the velocity controller maximum reference speed in rad/s.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 * \param limit Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) objectler maximum reference speed value.
 *
 * \return none
 */
IFX_INLINE void Ifx_VelocityCtrF32_setRefLimit(Ifx_VelocityCtrF32 *vc, float32 limit)
{
    vc->refLimit = limit;
}


/** \brief Set the velocity controller reference speed in rad/s.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 * \param speed Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) objectler reference speed value.
 *
 * \retval TRUE Returns TRUE if the reference speed could be set
 * \retval FALSE Returns FALSE if the reference speed is out of range
 */
IFX_INLINE boolean Ifx_VelocityCtrF32_setRef(Ifx_VelocityCtrF32 *vc, float32 speed)
{
    boolean result;

    if ((__absf(speed)) > (vc->refLimit))
    {
        result = FALSE;
    }
    else
    {
        vc->ref = speed;
        result  = TRUE;
    }

    return result;
}


/** \brief Return the velocity controller maximum reference speed in rad/s.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 *
 * \return Return the max allowed ref speed
 */
IFX_INLINE float32 Ifx_VelocityCtrF32_getRefLimit(Ifx_VelocityCtrF32 *vc)
{
    return vc->refLimit;
}


/** \brief Return the velocity controller output.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 *
 * \return Return the velocity controller output
 */
IFX_INLINE float32 Ifx_VelocityCtrF32_getOuput(Ifx_VelocityCtrF32 *vc)
{
    return vc->pic.uk;
}


/** \brief Return the current speed.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 *
 * \return Return the current speed in rad/s
 */
IFX_INLINE float32 Ifx_VelocityCtrF32_getSpeed(Ifx_VelocityCtrF32 *vc)
{
    return vc->value;
}


/** \brief Return the velocity controller ref speed in rad/s.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 *
 * \return Return the ref speed
 */
IFX_INLINE float32 Ifx_VelocityCtrF32_getRef(Ifx_VelocityCtrF32 *vc)
{
    return vc->ref;
}


/** \brief Return a copy of the velocity controller PI controller.
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 * \param pic Specifies the location where the copy must be saved.
 *
 * \return Return the max allowed ref speed
 */
IFX_INLINE void Ifx_VelocityCtrF32_getPic(Ifx_VelocityCtrF32 *vc, Ifx_PicF32 *pic)
{
    *pic = vc->pic;
}


/** \brief Get the velocity controller PI parameters
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 * \param kp Specifies the PI controller kp value.
 * \param ki Specifies the PI controller ki value.
 *
 * \return None.
 * \see Ifx_PicF32_getKpKi()
 */
IFX_INLINE void Ifx_VelocityCtrF32_getKpKi(Ifx_VelocityCtrF32 *vc, float32 *kp, float32 *ki)
{
    Ifx_PicF32_getKpKi(&vc->pic, kp, ki, vc->period);
}


/** \brief Set the velocity controller PI parameters
 *
 * \param vc Specifies pointer to the velocity control (\ref Ifx_VelocityCtrF32) object.
 * \param kp Specifies the PI controller kp value.
 * \param ki Specifies the PI controller ki value.
 *
 * \return None.
 * \see Ifx_PicF32_setKpKi()
 */
IFX_INLINE void Ifx_VelocityCtrF32_setKpKi(Ifx_VelocityCtrF32 *vc, float32 kp, float32 ki)
{
    Ifx_PicF32_setKpKi(&vc->pic, kp, ki, vc->period);
}


/** \} */
//----------------------------------------------------------------------------------------
#endif
