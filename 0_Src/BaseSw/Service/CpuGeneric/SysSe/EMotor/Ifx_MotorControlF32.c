#include "SysSe/EMotor/Ifx_MotorControlF32.h"
#include "SysSe/Comm/Ifx_Console.h"
#include "Common/main.h"

/** \brief Inverter application message flags */
typedef enum
{
    /* FIXME clean up unused messages */
    Ifx_MotorControlF32_Message_STARTED,             /**< \brief "Control started" message flag index. */
    Ifx_MotorControlF32_Message_STOPPED,             /**< \brief "Control stopped" message flag index. */
    Ifx_MotorControlF32_Message_RUNNING,             /**< \brief "Control running" message flag index. */
    Ifx_MotorControlF32_Message_CANT_START,          /**< \brief "Control application can't be started" message flag index. */
    Ifx_MotorControlF32_Message_CANT_STOP,           /**< \brief "Control application can't be stopped" message flag index. */
    Ifx_MotorControlF32_Message_ALREADY_STOPPED,     /**< \brief "Control application already stopped" message flag index. */
    Ifx_MotorControlF32_Message_BRAKE_ON,            /**< \brief "Brake on" message flag index. */
    Ifx_MotorControlF32_Message_EMERGENCY,           /**< \brief "Emergency" message flag index. */
    Ifx_MotorControlF32_Message_PHASE_CURRENT_ERROR, /**< \brief "Phase current out-of-limit" message flag index. */
    Ifx_MotorControlF32_Message_VDC_ERROR,           /**< \brief "DC voltage out-of-limit" message flag index. */
    Ifx_MotorControlF32_Message_BOARD_TEMP_ERROR,    /**< \brief "Board temp out-of-limit" message flag index. */
    Ifx_MotorControlF32_Message_STATOR_TEMP_ERROR,   /**< \brief "Stator temp out-of-limit" message flag index. */
    Ifx_MotorControlF32_Message_IGBT_TEMP_ERROR,     /**< \brief "IGBT temp out-of-limit" message flag index. */
    Ifx_MotorControlF32_Message_FAULT_U,             /**< \brief "IGBT driver phase U fault" message flag index. */
    Ifx_MotorControlF32_Message_FAULT_V,             /**< \brief "IGBT driver phase V fault" message flag index. */
    Ifx_MotorControlF32_Message_FAULT_W,             /**< \brief "IGBT driver phase W fault" message flag index. */
    Ifx_MotorControlF32_Message_FAULT_POSITION,      /**< \brief "Position sensor fault" message flag index. */
    Ifx_MotorControlF32_Message_FAULT_CURRENT,       /**< \brief "Current sensor fault" message flag index. */
    Ifx_MotorControlF32_Message_CAN_MISSING_MCU,     /**< \brief "CAN bus fault" message flag index. */
    Ifx_MotorControlF32_Message_VDC_GEN_ERROR,       /**< \brief "Regenerative DC-voltage error" message flag index. */
    Ifx_MotorControlF32_Message_FREQ_LIMIT,          /**< \brief "Frequency limit reached" message flag index. */

    Ifx_MotorControlF32_Message_COUNT                /**< \brief Runtime message count (must be less or equal than 32). */
} Ifx_MotorControlF32_Message;

/** \brief Inverter application messages */
const Ifx_Message_Entry Ifx_g_MotorControlF32_messages[Ifx_MotorControlF32_Message_COUNT] =
{
    {FALSE, "control application started"         },
    {FALSE, "control application stopped"         },
    {FALSE, "control application running"         },
    {FALSE, "control application can't be started"},
    {FALSE, "control application can't be stopped"},
    {FALSE, "control application already stopped" },
    {FALSE, "brake on"                            },
    {TRUE,  "emergency stop"                      },
    {TRUE,  "phase current out-of-limit"          },
    {TRUE,  "DC voltage out-of-limit"             },
    {TRUE,  "board temperature out-of-limit"      },
    {TRUE,  "motor temperature out-of-limit"      },
    {TRUE,  "IGBT temperature out-of-limit"       },
    {TRUE,  "IGBT driver fault on phase U"        },
    {TRUE,  "IGBT driver fault on phase V"        },
    {TRUE,  "IGBT driver fault on phase W"        },
    {TRUE,  "position sensor error"               },
    {TRUE,  "current sensor error"                },
    {FALSE, "CAN communication fault/missing"     },
    {TRUE,  "DC voltage regenerative error"       },
    {FALSE, "frequency limit reached"             },
};

IFX_INLINE void Ifx_MotorControlF32_raiseMessage(Ifx_MotorControlF32 *motor, uint32 index)
{
    Ifx_Message_set(&motor->messages, index);
}


IFX_INLINE void Ifx_MotorControlF32_showMessages(Ifx_MotorControlF32 *motor)
{
    Ifx_Message_show(&motor->messages);
}


void Ifx_MotorControlF32_initConfig(Ifx_MotorControlF32_Config *config)
{
    config->piSpeed.kp            = 0.0;
    config->piSpeed.ki            = 0.0;
    config->speedControlPeriod    = 1000;
    config->speedMax              = 1000;
    config->torqueMax             = 10;
    config->controlPeriod         = 10000;
    config->torqueRate            = 500;
    config->speedRate             = 2*IFX_PI*100; // 100 round/s/s
    config->motorModel            = NULL_PTR;
    config->openLoopAmplitudeRate = 0.1;
    config->inverter              = NULL_PTR;
    config->positionSensor        = NULL_PTR;
    config->mode                  = Ifx_MotorControl_Mode_torqueControl;
    config->standardIo            = &Ifx_g_console.standardIo;
    config->name                  = "\0";
}


void Ifx_MotorControlF32_init(Ifx_MotorControlF32 *motor, Ifx_MotorControlF32_Config *config)
{
    Ifx_Message_Config messageConfig;

    Ifx_Message_initConfig(&messageConfig);
    messageConfig.count      = Ifx_MotorControlF32_Message_COUNT;
    messageConfig.entries    = Ifx_g_MotorControlF32_messages;
    messageConfig.owner      = (uint32)motor;
    messageConfig.ownerName  = config->name;
    messageConfig.standardIo = config->standardIo;
    Ifx_Message_init(&motor->messages, &messageConfig);

    motor->flags.U                  = 0;

    motor->flags.B.emergencyEnabled = 1;
    motor->flags.B.modeTest         = 0;

    /* Initialise the velocity controller ----------------------------------------------*/
    {
        Ifx_VelocityCtrF32 *vc = &motor->velocity;
        Ifx_VelocityCtrF32_init(vc);
        vc->period = config->speedControlPeriod;
        Ifx_VelocityCtrF32_setKpKi(vc, config->piSpeed.kp, config->piSpeed.ki);
        Ifx_VelocityCtrF32_setRefLimit(vc, IfxStdIf_Pos_rpmToRads(config->speedMax));
    }
    /* Initialize the torque controller ------------------------------------------------*/
    Ifx_RampF32_init(&motor->torqueRamp, config->torqueRate, config->controlPeriod);
    Ifx_RampF32_init(&motor->openLoop.amplitude, config->openLoopAmplitudeRate, config->controlPeriod);
    motor->openLoop.offset = 0;
    Ifx_MotorControlF32_setTorqueLimit(motor, config->torqueMax);

    motor->motorModel            = config->motorModel;
    motor->inverter              = config->inverter;

    {
        /* Virtual position sensor for open loop ------------------------------------------*/
        Ifx_VirtualPositionSensor_Config configVirtualPosition;

        Ifx_VirtualPositionSensor_initConfig(&configVirtualPosition);
        configVirtualPosition.base.updatePeriod = config->controlPeriod;
        configVirtualPosition.frequencyRate = config->speedRate;
        Ifx_VirtualPositionSensor_init(&motor->virtualPosition, &configVirtualPosition);
        Ifx_VirtualPositionSensor_stdIfPosInit(&motor->stdifVirtualPosition, &motor->virtualPosition);
    }

    if (config->positionSensor != NULL_PTR)
    {
        motor->positionSensor        = config->positionSensor;
        motor->currentPositionSensor = config->positionSensor;
    }
    else
    {
        /* If no sensor are defined, use default virtual sensor */
        motor->positionSensor        = &motor->stdifVirtualPosition;
        motor->currentPositionSensor = &motor->stdifVirtualPosition;
    }

    IfxStdIf_MotorModelF32_setControlPeriod(motor->motorModel, config->controlPeriod);
    IfxStdIf_MotorModelF32_setPositionSensorResolution(motor->motorModel, (IfxStdIf_Pos_getPeriodPerRotation(motor->currentPositionSensor) * IfxStdIf_Pos_getResolution(motor->currentPositionSensor)));

    /* Set the default operation mode --------------------------------------------------*/
    Ifx_MotorControlF32_setMode(motor, config->mode);

}


/** \brief Set the active position sensor
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 * \param position Pointer to the new active position sensor
 *
 * \retval TRUE if the new position pointer is applied.
 * \retval FALSE if the motor control is running.
 */
boolean Ifx_MotorControlF32_setPositionSensor(Ifx_MotorControlF32 *motor, IfxStdIf_Pos *position)
{
    boolean result = FALSE;

    if (position != NULL_PTR)
    {
        motor->positionSensor        = position;
        motor->currentPositionSensor = position;
        IfxStdIf_MotorModelF32_setPositionSensorResolution(motor->motorModel, (IfxStdIf_Pos_getPeriodPerRotation(motor->currentPositionSensor) * IfxStdIf_Pos_getResolution(motor->currentPositionSensor)));

        result = TRUE;
    }

    return result;
}


/** \brief Evaluates the control commands and fault status
 *
 * This function is called by Emotor_currentStep() interface or functions which override it
 * such as PmsmVec_currentStep() and AcimVec_currentStep().
 *
 * This function evaluates:
 * - fault status from inverter low/high switches driver
 * - fault status from position interface
 * - start/stop command from the user
 *
 * When start flag and no fault was detected, the running flag will be set, indicates that
 * the motor control loop is started (Inverter/PWM output is enabled)
 *
 * When stop flag is detected the running flag will be cleared and the PWM output will be
 * disabled.
 *
 * When fault is detected, the stop will be requested.
 *
 * \param motor specifies the pointer to the \ref Ifx_MotorControlF32 object
 */
void Ifx_MotorControlF32_commandStage(Ifx_MotorControlF32 *motor)
{
    boolean emergency = FALSE;

#if BYPASSPROTECTION
    emergency = IfxStdIf_Inverter_getStatus(motor->inverter).error != 0;
#else if
    emergency = FALSE;
#endif

    boolean pFault = IfxStdIf_Pos_isFault(motor->currentPositionSensor);

    if (pFault != FALSE)
    {
        Ifx_MotorControlF32_raiseMessage(motor, Ifx_MotorControlF32_Message_FAULT_POSITION);
        emergency = TRUE;
    }

    if ((emergency != FALSE) && (motor->flags.B.hadEmergency == 0))
    {
        if (motor->flags.B.emergencyEnabled)
        {
            Ifx_MotorControlF32_raiseMessage(motor, Ifx_MotorControlF32_Message_EMERGENCY);
            motor->flags.B.hadEmergency = 1;
            motor->flags.B.stop         = 1; /* Request a PWM Off, self control Off */
        }
    }

    if (motor->flags.B.running && motor->flags.B.stop)
    {
        /* Request to stop */
        motor->flags.B.stop    = 0;
        motor->flags.B.start   = 0;
        motor->flags.B.running = 0;
        IfxStdIf_Inverter_setPwmOff(motor->inverter);
        Ifx_MotorControlF32_raiseMessage(motor, Ifx_MotorControlF32_Message_STOPPED);
    }
    else if ((!motor->flags.B.running) && motor->flags.B.start && !motor->flags.B.hadEmergency  && (motor->mode != Ifx_MotorControl_Mode_passive))
    {
        /* Request to start */
        motor->flags.B.start = 0;

        Ifx_RampF32_reset(&motor->torqueRamp);
        IfxStdIf_MotorModelF32_reset(motor->motorModel);
        Ifx_RampF32_reset(&motor->openLoop.amplitude);

        IFX_Cf32_reset(&motor->state.modulationIndex);
        motor->state.torque = 0.0;
        Ifx_VelocityCtrF32_reset(&motor->velocity);

        Ifx_Message_enableAll(&motor->messages);

        /* Enable PWM for next Period */
        Ifx_MotorControlF32_resetFlags(motor);
        IfxStdIf_Inverter_setPwmOn(motor->inverter, Ifx_Pwm_Mode_centerAligned);
        Ifx_MotorControlF32_raiseMessage(motor, Ifx_MotorControlF32_Message_STARTED);
        motor->flags.B.running = 1;
    }
    else
    {}

    motor->flags.B.generatorMode = 0 > (IfxStdIf_Pos_getSpeed(motor->currentPositionSensor) * motor->state.torque) ? 1 : 0;
}


void Ifx_MotorControlF32_execute(Ifx_MotorControlF32 *motor)
{
    Ifx_F32_MotorModel_input modelInput;

    /* Process user commands and emergency status */
    Ifx_MotorControlF32_commandStage(motor);

    if (motor->currentPositionSensor == &motor->stdifVirtualPosition)
    {
        IfxStdIf_Pos_update(&motor->stdifVirtualPosition);
    }

    modelInput.state.rawMechanicalAngle = IfxStdIf_Pos_getRawPosition(motor->currentPositionSensor);
    modelInput.state.mechanicalSpeed    = IfxStdIf_Pos_getSpeed(motor->currentPositionSensor);

    modelInput.state.vdc                = IfxStdIf_Inverter_getVdc(motor->inverter);

    IfxStdIf_Inverter_getPhaseCurrents(motor->inverter, modelInput.state.currents);

    if (motor->mode == Ifx_MotorControl_Mode_speedControl)
    {
        /* FIXME missing speed ramp */
        modelInput.ref.torque = Ifx_VelocityCtrF32_getOuput(&motor->velocity);
    }
    else if (motor->mode == Ifx_MotorControl_Mode_torqueControl)
    {
        modelInput.ref.torque = Ifx_RampF32_step(&motor->torqueRamp);
    }
    else if (motor->mode == Ifx_MotorControl_Mode_currentControl)
    {
        modelInput.ref.current = motor->current;
    }
    else if (motor->mode == Ifx_MotorControl_Mode_testPI)
    {
        modelInput.ref.current = motor->current;
    }
    /* 20200226 Jimmy Created */
    g_App.can.outbox.torqueCtrl_set = modelInput.ref.torque;
    /* end of created */

    if (motor->flags.B.running)
    {
        if (!motor->flags.B.modeTest)
        {
            IfxStdIf_MotorModelF32_execute(motor->motorModel, &modelInput, &motor->state);
            IfxStdIf_Inverter_setVoltage(motor->inverter, motor->state.modulationIndex);
        }
        else
        {
            /* Test modes */
            float32  m      = 0;
            cfloat32 cossin = (cfloat32)
            {
                0.0, 0.0
            };

            if (motor->mode == Ifx_MotorControl_Mode_openLoop)
            {
                IfxStdIf_MotorModelF32_Param param = IfxStdIf_MotorModelF32_getParam(motor->motorModel);
                float32                       electricalAngleConst;
                IfxStdIf_Pos_RawAngle         elAngle;

                m                    = Ifx_RampF32_step(&motor->openLoop.amplitude);

                electricalAngleConst = (float32)(param.polePair * IFX_LUT_ANGLE_RESOLUTION) / (float32)IfxStdIf_Pos_getResolution(motor->currentPositionSensor);
                elAngle              = ((IfxStdIf_Pos_RawAngle)(electricalAngleConst * modelInput.state.rawMechanicalAngle) & (IFX_LUT_ANGLE_RESOLUTION - 1));

                cossin               = Ifx_LutLSincosF32_cossin(elAngle);
                motor->state.modulationIndex.real = cossin.real * m;
                motor->state.modulationIndex.imag = cossin.imag * m;
            }
            else if (motor->mode == Ifx_MotorControl_Mode_openWithPosition)
            {
                IfxStdIf_MotorModelF32_Param param = IfxStdIf_MotorModelF32_getParam(motor->motorModel);
                float32                       electricalAngleConst;
                IfxStdIf_Pos_RawAngle         elAngle;

                m                    = Ifx_RampF32_step(&motor->openLoop.amplitude);

                electricalAngleConst = (float32)(param.polePair * IFX_LUT_ANGLE_RESOLUTION) / (float32)IfxStdIf_Pos_getResolution(motor->currentPositionSensor);
                elAngle              = ((IfxStdIf_Pos_RawAngle)(electricalAngleConst * modelInput.state.rawMechanicalAngle) & (IFX_LUT_ANGLE_RESOLUTION - 1));

                cossin               = Ifx_LutLSincosF32_cossin(elAngle + motor->openLoop.offset);
                motor->state.modulationIndex.real = cossin.real * m;
                motor->state.modulationIndex.imag = cossin.imag * m;
            }
            else if (motor->mode == Ifx_MotorControl_Mode_testPI)
            {
                IfxStdIf_MotorModelF32_execute(motor->motorModel, &modelInput, &motor->state);
            }

            IfxStdIf_Inverter_setVoltage(motor->inverter, motor->state.modulationIndex);
        }
    }
    else
    {
        if (!motor->flags.B.modeTest)
        {}
        else
        {
            if (motor->mode == Ifx_MotorControl_Mode_openLoop)
            {}
            else if (motor->mode == Ifx_MotorControl_Mode_openWithPosition)
            {}
            else if (motor->mode == Ifx_MotorControl_Mode_testPI)
            {}
        }
    }

#if IFX_FOCF32_DEBUG
    /*
     * // inverse SVM:
     * cfloat32 cossin = Ifx_LutLSincosF32_cossin(elAngle);
     * Ifx_TimerValue duty[3];
     * IfxStdIf_Inverter_getPwm(base->inverter, duty);
     * mab = Ifx_SvmF32_revertToVoltages(duty, foc->vdcNom, IfxStdIf_Inverter_getPeriodTicks(base->inverter));
     * foc->vdq = Ifx_ParkF32_forward(&mab, &cossin);
     */
#endif
}


/** \brief Speed controller step
 *
 * This function shall be called in periodic timer according to the speed control period
 * in the \ref Ifx_MotorControlF32_Config.
 *
 * \param motor specifies the pointer to the \ref Ifx_MotorControlF32 object
 */
void Ifx_MotorControlF32_speedStep(Ifx_MotorControlF32 *motor)
{
    Ifx_VelocityCtrF32 *vc = &motor->velocity;
    float32              speed;

    speed = IfxStdIf_Pos_getSpeed(motor->currentPositionSensor);

    if (motor->flags.B.running)
    {
        if (motor->mode == Ifx_MotorControl_Mode_speedControl)
        {
            Ifx_VelocityCtrF32_step(vc, speed);
        }
    }
    else
    {}
}


/** \brief Request to start the motor control.
 * \see Emotor_commandStage() */
void Ifx_MotorControlF32_start(Ifx_MotorControlF32 *motor)
{
    motor->flags.B.stop = 0;
    motor->flags.B.start = 1;
}


/** \brief Request to stop the motor control.
 * \see Emotor_commandStage() */
void Ifx_MotorControlF32_stop(Ifx_MotorControlF32 *motor)
{
    motor->flags.B.stop = 1;
}


/** \brief Request to clear the emergency flag. Only when control is not running */
void Ifx_MotorControlF32_resetFlags(Ifx_MotorControlF32 *motor)
{
    if (!motor->flags.B.running)
    {
        motor->flags.B.hadEmergency = 0;
        IfxStdIf_Inverter_clearFlags(motor->inverter);
    }
}


/** \brief Get the control mode
 * \param motor specifies the pointer to the \ref Ifx_MotorControlF32 object
 * \return Emotor_Mode.
 */
Ifx_MotorControl_Mode Ifx_MotorControlF32_getMode(Ifx_MotorControlF32 *motor)
{
    return motor->mode;
}


/** \brief Set control mode
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 * \param mode specifies the motor control mode
 *
 * \return TRUE if success. FALSE if failed e.g. when self is running.
 * \see Emotor_setMode(), Ifx_VelocityCtrF32_disable()
 */
boolean Ifx_MotorControlF32_setMode(Ifx_MotorControlF32 *motor, Ifx_MotorControl_Mode mode)
{
    boolean result = FALSE;

    if ((!motor->flags.B.running) && (motor->mode != mode))
    {
        motor->mode = mode;

        switch (mode)
        {
            case Ifx_MotorControl_Mode_passive:
                break;
            case Ifx_MotorControl_Mode_currentControl:
                motor->currentPositionSensor = motor->positionSensor;
                Ifx_VelocityCtrF32_disable(&motor->velocity);
                IfxStdIf_MotorModelF32_setTorqueControl(motor->motorModel, FALSE);
                motor->flags.B.modeTest      = 0;
                break;
            case Ifx_MotorControl_Mode_torqueControl:
                motor->currentPositionSensor = motor->positionSensor;
                Ifx_VelocityCtrF32_disable(&motor->velocity);
                IfxStdIf_MotorModelF32_setTorqueControl(motor->motorModel, TRUE);
                motor->flags.B.modeTest      = 0;
                break;
            case Ifx_MotorControl_Mode_speedControl:
                motor->currentPositionSensor = motor->positionSensor;
                Ifx_VelocityCtrF32_enable(&motor->velocity);
                IfxStdIf_MotorModelF32_setTorqueControl(motor->motorModel, TRUE);
                motor->flags.B.modeTest      = 0;
                break;
            case Ifx_MotorControl_Mode_openLoop:
                motor->currentPositionSensor = &motor->stdifVirtualPosition;
                Ifx_VirtualPositionSensor_reset(&motor->virtualPosition);
                motor->openLoop.offset       = 0;
                Ifx_VelocityCtrF32_enable(&motor->velocity);
                motor->flags.B.modeTest      = 1;
                break;
            case Ifx_MotorControl_Mode_openWithPosition:
                motor->currentPositionSensor = motor->positionSensor;
                motor->openLoop.offset       = IFX_LUT_ANGLE_PI / 4;
                Ifx_VelocityCtrF32_disable(&motor->velocity);
                motor->flags.B.modeTest      = 1;
                break;
            case Ifx_MotorControl_Mode_testPI:
                motor->currentPositionSensor = &motor->stdifVirtualPosition;
                Ifx_VirtualPositionSensor_reset(&motor->virtualPosition);
                Ifx_VelocityCtrF32_disable(&motor->velocity);
                IfxStdIf_MotorModelF32_setTorqueControl(motor->motorModel, FALSE);
                motor->flags.B.modeTest = 1;
                break;
            default:
            	break;
        }

        result = TRUE;
    }

    return result;
}


/** \brief Set the speed reference.
 *
 * This function operates only in \ref Ifx_MotorControl_Mode_speedControl.
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 * \param speed reference in rpm
 *
 * \retval TRUE if the new value is applied.
 * \retval FALSE if the new value can not be applied.
 *
 */
boolean Ifx_MotorControlF32_setSpeed(Ifx_MotorControlF32 *motor, float32 speed)
{
    boolean result = TRUE;

    if (motor->mode == Ifx_MotorControl_Mode_speedControl)
    {
        /* FIXME no need for the "if" test, should return void  */
        speed  = IfxStdIf_Pos_rpmToRads(speed);
        result = Ifx_VelocityCtrF32_setRef(&motor->velocity, speed);
    }
    else if (motor->mode == Ifx_MotorControl_Mode_openLoop)
    {
        /* FIXME move this to execute & corresponding test mode, use the motor->velocity ref as value  ? */
        speed = IfxStdIf_Pos_rpmToRads(speed);
        Ifx_VirtualPositionSensor_setSpeed(&motor->virtualPosition, speed);
    }
    else
    {
        result = FALSE;
    }

    return result;
}

/** \brief Set the position
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 * \param position position in rad
 *
 * \retval TRUE if the new value is applied.
 * \retval FALSE if the new value can not be applied.
 *
 */
boolean Ifx_MotorControlF32_setPosition(Ifx_MotorControlF32 *motor, float32 position)
{
    boolean result = TRUE;

    if (motor->mode == Ifx_MotorControl_Mode_openLoop)
    {
        Ifx_VirtualPositionSensor_setPosition(&motor->virtualPosition, position);
    }
    else
    {
        result = FALSE;
    }

    return result;
}


/** \brief Get the actual speed in rpm
 *
 * This function works in any control mode, either running or stopped.
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 *
 * \return Actual speed in rpm
 *
 */
float32 Ifx_MotorControlF32_getSpeed(Ifx_MotorControlF32 *motor)
{
    return IfxStdIf_Pos_radsToRpm(IfxStdIf_Pos_getSpeed(motor->currentPositionSensor));
}

float32 Ifx_MotorControlF32_getSensorSpeed(Ifx_MotorControlF32 *motor)
{
    return IfxStdIf_Pos_radsToRpm(IfxStdIf_Pos_getSpeed(motor->positionSensor));
}

/** \brief Get the actual position in rad
 *
 * This function works in any control mode, either running or stopped.
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 *
 * \return Actual position in rad
 *
 */
float32 Ifx_MotorControlF32_getPosition(Ifx_MotorControlF32 *motor)
{
    return IfxStdIf_Pos_getPosition(motor->currentPositionSensor);
}
float32 Ifx_MotorControlF32_getSensorPosition(Ifx_MotorControlF32 *motor)
{
    return IfxStdIf_Pos_getPosition(motor->positionSensor);
}
void Ifx_MotorControlF32_resetVirtualPositionSensor(Ifx_MotorControlF32 *motor)
{
    IfxStdIf_Pos_reset(&motor->stdifVirtualPosition);
}
/** \brief Get the actual turn count
 *
 * This function works in any control mode, either running or stopped.
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 *
 * \return Actual turn count
 *
 */
sint32 Ifx_MotorControlF32_getTurn(Ifx_MotorControlF32 *motor)
{
    return IfxStdIf_Pos_getTurn(motor->currentPositionSensor);
}


/** \brief Get the reference speed in rpm
 *
 * This function works in any control mode, either running or stopped.
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 *
 * \return Reference speed in rpm
 *
 */
float32 Ifx_MotorControlF32_getSpeedRef(Ifx_MotorControlF32 *motor)
{
    return IfxStdIf_Pos_radsToRpm(Ifx_VelocityCtrF32_getRef(&motor->velocity));
}


/** \brief Set the speed limit in rad/s
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 * \param limit specifies the speed limit in rpm
 *
 * \return Emotor_Mode.
 */
void Ifx_MotorControlF32_setSpeedLimit(Ifx_MotorControlF32 *motor, float32 limit)
{
    Ifx_VelocityCtrF32_setRefLimit(&motor->velocity, IfxStdIf_Pos_rpmToRads(limit));
}


/** \brief Get the speed limit in rpm
 *
 * This function works in any control mode, either running or stopped.
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 *
 * \return Speed limit in rpm
 *
 */
float32 Ifx_MotorControlF32_getSpeedLimit(Ifx_MotorControlF32 *motor)
{
    return IfxStdIf_Pos_radsToRpm(Ifx_VelocityCtrF32_getRefLimit(&motor->velocity));
}


/** \brief Set the reference torque
 *
 * This function applies new reference value if:
 * - the motor control is running, and
 * - in torque control mode, and
 * - the torque value is in range specified by Emotor_setTorqueLimit()
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 * \param torque specifies the torque reference in N.m
 *
 * \retval TRUE if the new reference value is applied.
 * \retval FALSE the new reference value is not applied
 *
 */
boolean Ifx_MotorControlF32_setTorque(Ifx_MotorControlF32 *motor, float32 torque)
{
    boolean result;

    if (motor->mode == Ifx_MotorControl_Mode_torqueControl)
    {
        Ifx_RampF32_setRef(&motor->torqueRamp, torque);
        result = TRUE;
    }
    else
    {
        result = FALSE;
    }

    return result;
}

/** \brief Set the reference current
 *
 * This function applies new reference value if:
 * - the motor control is running, and
 * - in current control mode, and
 * - the current value is in range specified by Emotor_setCurrentLimit()
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 * \param current specifies the current reference in A
 *
 * \retval TRUE if the new reference value is applied.
 * \retval FALSE the new reference value is not applied
 *
 */
boolean Ifx_MotorControlF32_setCurrent(Ifx_MotorControlF32 *motor, cfloat32 *current)
{
    boolean result;

    if (motor->mode == Ifx_MotorControl_Mode_currentControl)
    {
        motor->current  = *current; /* FIXME is ramp required ? */
        result = TRUE;
    }
    else if (motor->mode == Ifx_MotorControl_Mode_testPI)
    {
        motor->current  = *current;
        result = TRUE;
    }
    else
    {
        result = FALSE;
    }

    return result;
}


/** \brief Set the open loop amplitude
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 * \param amplitude specifies the amplitude reference in as modulation index [0,1]
 *
 * \retval TRUE if the new reference value is applied.
 * \retval FALSE the new reference value is not applied
 *
 */
boolean Ifx_MotorControlF32_setOpenLoopAmplitude(Ifx_MotorControlF32 *motor, float32 amplitude)
{
    boolean result;
    Ifx_RampF32_setRef(&motor->openLoop.amplitude, amplitude);
    result = TRUE;
    return result;
}


/** \brief Set the open loop sensor position
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 * \param position Position sensor rad
 * \return None
 */
boolean Ifx_MotorControlF32_setOpenLoopPosition(Ifx_MotorControlF32 *motor, float32 position)
{
    IfxStdIf_Pos_setPosition(&motor->stdifVirtualPosition, position);

    return TRUE;
}


/** \brief Get the open loop amplitude
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 *
 * \return value as modulation index [0,1]
 *
 */
float32 Ifx_MotorControlF32_getOpenLoopAmplitude(Ifx_MotorControlF32 *motor)
{
    return Ifx_RampF32_getValue(&motor->openLoop.amplitude);
}


/** \brief Get the actual estimated torque in N.m
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 *
 * \return Actual estimated torque in N.m
 *
 */
float32 Ifx_MotorControlF32_getTorque(Ifx_MotorControlF32 *motor)
{
    return motor->state.torque;
}


/** \brief Get the torque reference in N.m
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 *
 * \return Actual reference torque in N.m
 *
 */
float32 Ifx_MotorControlF32_getTorqueRef(Ifx_MotorControlF32 *motor)
{
    return Ifx_RampF32_getRef(&motor->torqueRamp);
}


void Ifx_MotorControlF32_setCurrentControllers(Ifx_MotorControlF32 *motor, Ifx_FocF32_Pic_Config *piD, Ifx_FocF32_Pic_Config*piQ)
{
    IfxStdIf_MotorModelF32_setCurrentControllers(motor->motorModel, piD, piQ);
}


/** \brief Set the torque limit
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 * \param limit specifies the torque limit in N.m
 *
 * \return None
 *
 */
void Ifx_MotorControlF32_setTorqueLimit(Ifx_MotorControlF32 *motor, float32 limit)
{
    Ifx_VelocityCtrF32_setTorqueLimit(&motor->velocity, limit);
}


/** \brief Get the torque limit in N.m
 *
 * \param motor specifies pointer to the \ref Ifx_MotorControlF32 object
 *
 * \return Actual torque limit in N.m
 *
 */
float32 Ifx_MotorControlF32_getTorqueLimit(Ifx_MotorControlF32 *motor)
{
    return Ifx_VelocityCtrF32_getTorqueLimit(&motor->velocity);
}


/** Limit the phase current in open loop mode
 *
 */
void Ifx_MotorControlF32_limitCurrent(Ifx_MotorControlF32 *motor)
{
    /* Check max current */
    IfxStdIf_MotorModelF32_Param param = IfxStdIf_MotorModelF32_getParam(motor->motorModel);
    float32                       currents[3];
    float32                       limit = param.iStall;
    IfxStdIf_Inverter_getPhaseCurrents(motor->inverter, currents);

    if ((__absf(currents[0]) > limit) || (__absf(currents[1]) > limit) || (__absf(currents[2]) > limit))
    {
        Ifx_MotorControlF32_setOpenLoopAmplitude(motor, Ifx_MotorControlF32_getOpenLoopAmplitude(motor) * 0.99);
    }
}



void Ifx_MotorControlF32_backgroundTask(Ifx_MotorControlF32 *motor)
{
    Ifx_MotorControlF32_showMessages(motor);
}


void Ifx_MotorControl_Mode_print(IfxStdIf_DPipe *io, Ifx_MotorControl_Mode mode)
{
    switch (mode)
    {
        case Ifx_MotorControl_Mode_currentControl:
            IfxStdIf_DPipe_print(io, "Motor control mode: Current control"ENDL);
            break;
        case Ifx_MotorControl_Mode_openLoop:
            IfxStdIf_DPipe_print(io, "Motor control mode: open loop control"ENDL);
            break;
        case Ifx_MotorControl_Mode_openWithPosition:
            IfxStdIf_DPipe_print(io, "Motor control mode: open loop with position control"ENDL);
            break;
        case Ifx_MotorControl_Mode_passive:
            IfxStdIf_DPipe_print(io, "Motor control mode: passive"ENDL);
            break;
        case Ifx_MotorControl_Mode_speedControl:
            IfxStdIf_DPipe_print(io, "Motor control mode: speed control"ENDL);
            break;
        case Ifx_MotorControl_Mode_testPI:
            IfxStdIf_DPipe_print(io, "Motor control mode: test PI"ENDL);
            break;
        case Ifx_MotorControl_Mode_torqueControl:
            IfxStdIf_DPipe_print(io, "Motor control mode: torque control"ENDL);
            break;
        default:
            IfxStdIf_DPipe_print(io, "Motor control mode:  Unknown"ENDL);
            break;
    }
}
