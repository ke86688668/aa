/**
 * \file Ifx_MotorModelAcimF32.h
 * \brief Electrical motor control (ACIM).
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup library_srvsw_sysse_emotor_motorControl_acim Electrical motor control (ACIM Model).
 * \ingroup library_srvsw_sysse_emotor
 *
 * The voltage, flux, and torque equation of this motor can be written as:
 *
 * a) \f$v_{sd}=R_s.i_{sd}+\frac{d}{dt}.\psi_{sd}-\omega_s.\psi_{sq}\f$
 *
 * b) \f$v_{sq}=R_s.i_{sq}+\frac{d}{dt}.\psi_{sq}-\omega_s.\psi_{sd}\f$
 *
 * c) \f$v_{rd}=0=R_r.i_{rd}+\frac{d}{dt}\psi_{rd}-(\omega_s-\omega_r).\psi_{rq}\f$
 *
 * d) \f$v_{rq}=0=R_r.i_{rq}+\frac{d}{dt}.\psi_{rq}+(\omega_s-\omega_r ).\psi_{rd}\f$ \eq{37,lib_sysse_emotor_motorModel_eq37}
 *
 * a) \f$\phi_{sd}=L_s.i_{sd}+L_m.i_{rd}\f$
 *
 * b) \f$\phi_{sq}=L_s.i_{sq}+L_m.i_{rq}\f$
 *
 * c) \f$\phi_{rd}=L_r.i_{rd}+L_m.i_{sd}\f$
 *
 * d) \f$\phi_{rq}=L_r.i_{rq}+L_m.i_{sq}\f$   \eq{38,lib_sysse_emotor_motorModel_eq38}
 *
 * \f$\Gamma_e=1,5.p.(\psi_{sd}.i_{sd}-\psi_{sq}.i_{sq})\f$  \eq{39,lib_sysse_emotor_motorModel_eq39}
 *
 * \f$\Gamma_e=1,5.p.(\psi_{rd}.i_{rd}-\psi_{rq}.i_{rq})\f$  \eq{40,lib_sysse_emotor_motorModel_eq40}
 *
 * Rotor time constant (\f$\tau_r\f$) and magnetizing current (\f$i_{mr}\f$) are defined:
 *
 * \f$\tau_r=\frac{L_m}{R_r}\f$     \eq{41,lib_sysse_emotor_motorModel_eq41}
 *
 * \f$i_{mr}=\frac{\phi_r}{L_m}\f$    \eq{42,lib_sysse_emotor_motorModel_eq42}
 *
 * For the rotor flux and angle estimation, in the vector control of ACIM it is
 * assumed that d-axis aligned with the rotor flux (\f$\psi_{rd}\f$), which means:
 *
 * \f$\psi_{rd}=|\overrightarrow{\psi_r}|\f$ and \f$\psi_{rq}=0\f$ \eq{43,lib_sysse_emotor_motorModel_eq43}
 *
 * Combining \eqlink{37,lib_sysse_emotor_motorModel_eq37}.c), \eqlink{38,lib_sysse_emotor_motorModel_eq38}.c), \eqlink{43,lib_sysse_emotor_motorModel_eq43}, and \eqlink{41,lib_sysse_emotor_motorModel_eq41} (also \eqlink{42,lib_sysse_emotor_motorModel_eq42}) we obtain:
 *
 * \f$\frac{d}{dt}.\psi_{rd}=\frac{L_m}{\tau_r}.i_{sd}-\frac{1}{\tau_r}.\psi_{rd}\f$    \eq{44,lib_sysse_emotor_motorModel_eq44}
 *
 * \f$\frac{d}{dt}.i_{mr}=\frac{L_m}{\tau_r}.(i_{sd}-i_{mr})\f$  \eq{45,lib_sysse_emotor_motorModel_eq45}
 *
 * The rotor flux angle is estimated by integration of the rotor and slip speed:
 *
 * \f$\varphi_\psi=\int_{0}^{t}(\omega_r+\omega_{sl})dt\f$ \eq{46,lib_sysse_emotor_motorModel_eq46}
 *
 * where \f$\omega_{sl}=\frac{1}{\tau_r}.\frac{i_{sq}}{i_{mr}}\f$    \eq{47,lib_sysse_emotor_motorModel_eq47}
 *
 *
 */
#ifndef IFX_MOTORMODELACIMF32_H
#define IFX_MOTORMODELACIMF32_H
#include "StdIf/IfxStdIf_MotorModelF32.h"
#include "SysSe/EMotor/Ifx_FocF32.h"
#include "SysSe/Math/Ifx_AngleGenF32.h"
#include "SysSe/Math/Ifx_IntegralF32.h"

typedef struct
{
    uint8 polePair;          /**< \brief Motor pole pair count */

    /* primary parameters, given by configuration */
    float32 rs;              /**< \brief Motor stator resistance in Ohm */
    float32 ls;              /**< \brief Motor stator inductance in H */
    float32 lm;              /**< \brief Motor mutual inductance in H */
    float32 fr;              /**< \brief Motor rotor flux reference in V.s */
    float32 rr;              /**< \brief Motor rotor resistance in Ohm */
    float32 lr;              /**< \brief Motor rotor inductance in H */

    /* secondary parameters, calculated from primary parameters */
    float32 kt;              /**< \brief Motor torque constant */
    float32 lls;             /**< \brief Motor stator leakage inductance in H */
    float32 llr;             /**< \brief Motor rotor leakage inductance in H */
#if 0
    float32 s;
    float32 sLs;
#else
    float32 tr;              /**< \brief Motor rotor resistance in Ohm */
    float32 lmr;             /**< \brief Calculated from (Lm*Lm) / Lr */
    float32 kr;              /**< \brief Calculated from Rs + Rr * (Lmr / Lr) */
    float32 kl;              /**< \brief Calculated from Ls - Lmr */
#endif
    float32 iStall;          /**< \brief Motor Stall current in A */
}Ifx_MotorModelAcimF32_Data;

typedef struct
{
    IfxStdIf_MotorModelF32_Config base;
    float32                        voltageGenMax;         /**< \brief Maximum allowed DC-link voltage in V during regenerative braking */
    boolean                        fieldWeakeningEnabled; /**< \brief Specifies if the field weakening is enabled FIXME duplicate with Ifx_FocF32_Config.fieldWeakeningEnabled */

    Ifx_FocF32_Config             foc;                   /**< \brief Configuration for FOC object */

    float32                        statorMax;             /**< \brief maximum stator electrical speed (e.g. used at start-up) */
    float32                        rs;                    /**< \brief Motor stator default resistance in Ohm */
    float32                        rr;                    /**< \brief Motor rotor default resistance in Ohm */
    float32                        ls;                    /**< \brief Motor stator default inductance in H */
    float32                        lr;                    /**< \brief Motor rotor default inductance in H */
    float32                        lm;                    /**< \brief Motor mutual default inductance in H */
    float32                        fr;                    /**< \brief Motor rotor default flux in V.s */
    float32                        iStall;                /**< \brief Motor Stall current in A */
    boolean                        torqueControlEnabled;  /**< \brief Specifies if the torque control is enabled or disabled */
    boolean                        feedForwardEnabled;    /**< \brief Specifies if the feed forward is enabled */
}Ifx_MotorModelAcimF32_Config;

typedef struct
{
    Ifx_AngleGenF32 elAngle; /**< \brief Generated output electrical angle for stator */
    struct
    {
        float32 slip;         /**< \brief Reference slip speed (difference of stator - rotor speed) */
        float32 stator;       /**< \brief Reference stator speed */
        float32 statorMax;    /**< \brief maximum stator speed (e.g. used at start-up) */
    }                elSpeed;
    Ifx_IntegralF32 imr;     /**< \brief Integrator for estimating magnetizing current */
    float32          fluxR;   /**< \brief Estimated rotor flux */
} Ifx_MotorModelAcimF32_Est;

typedef struct
{
    Ifx_F32_MotorModel_Type      type;                 /**< \brief Motor type */
    float32                      electricalSpeed;      /**< \brief Rotor electrical speed (rad/s) */
    float32                      controlPeriod;
    Ifx_FocF32                  foc;
    Ifx_FocF32_Params           focParams;            /**< \brief FOC parameters */
    float32                      electricalAngleConst; /**< \brief constant for calculating elAngle (in ticks) from raw position */
    IfxStdIf_Pos_RawAngle        electricalAngle;
    float32                      vdc;                  /**< \brief Nominal DC-link voltage in V */

    boolean                      torqueControlEnabled;
    boolean                      feedForwardEnabled;   /**< \brief Specifies if the feed forward is enabled */
    Ifx_MotorModelAcimF32_Data params;               /**< \brief default values of runtime parameters */
    float32                      slip;                 /**< \brief Reference slip */
    Ifx_MotorModelAcimF32_Est  est;                  /**< \brief Flux estimation data */
}Ifx_MotorModelAcimF32;

/**
 * \brief Initialize the ACIM control structure.
 * \param model Pointer to the \ref Ifx_MotorModelAcimF32 object
 * \param config Pointer to the configuration structure
 */
void    Ifx_MotorModelAcimF32_init(Ifx_MotorModelAcimF32 *model, Ifx_MotorModelAcimF32_Config *config);
void    Ifx_MotorModelAcimF32_initConfig(Ifx_MotorModelAcimF32_Config *config);
boolean Ifx_MotorModelAcimF32_stdIfMotorModelInit(IfxStdIf_MotorModelF32 *stdif, Ifx_MotorModelAcimF32 *driver);
void Ifx_MotorModelAcimF32_setCurrentControllers(Ifx_MotorModelAcimF32 *model, Ifx_FocF32_Pic_Config *piD, Ifx_FocF32_Pic_Config*piQ);

#endif
