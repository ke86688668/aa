#include "Ifx_MotorModelAcimF32.h"
#include "Ifx_SvmF32.h"
#include "string.h"

/* FIXME Where this value comes from ? */
#define EPS (1e-7)
static void Ifx_MotorModelAcimF32_execute(Ifx_MotorModelAcimF32 *model, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output);
static void Ifx_MotorModelAcimF32_reset(Ifx_MotorModelAcimF32 *model);

void Ifx_MotorModelAcimF32_updateParams(Ifx_MotorModelAcimF32 *model)
{
    Ifx_MotorModelAcimF32_Data *p = &model->params;
    p->tr = p->lr / p->rr;
    p->kt = 1.5 * p->polePair;

#if 0
    p->s   = 1.0 - ((p->lm * p->lm) / (p->ls * p->lr));
    p->lls = p->ls - p->lm;
    p->llr = p->lr - p->lm;
    p->sLs = ((p->ls * p->lr) - (p->lm * p->lm)) / p->lr;
#else
    p->lmr = (p->lm * p->lm) / p->lr;
    p->kr  = p->rs + (p->lmr / p->tr);
    p->kl  = p->ls - p->lmr;
#endif
}


void Ifx_MotorModelAcimF32_setupParams(Ifx_MotorModelAcimF32 *model, const Ifx_MotorModelAcimF32_Config *config)
{
    Ifx_MotorModelAcimF32_Data *p = &model->params;
    p->fr = config->fr;
    p->lm = config->lm;
    p->ls = config->ls;
    p->lr = config->lr;
    p->rr = config->rr;
    p->rs = config->rs;
    Ifx_MotorModelAcimF32_updateParams(model);
}


void Ifx_MotorModelAcimF32_setControlPeriod(Ifx_MotorModelAcimF32 *model, float32 controlPeriod)
{
    model->controlPeriod = controlPeriod;
    Ifx_IntegralF32_init(&model->est.imr, 1.0, controlPeriod);
    Ifx_AngleGenF32_init(&model->est.elAngle, Ifx_AngleGenF32_SpeedUnit_rad_s, controlPeriod);
}


void Ifx_MotorModelAcimF32_initConfig(Ifx_MotorModelAcimF32_Config *config)
{
    IfxStdIf_MotorModelF32_initConfig(&config->base);
    config->voltageGenMax             = 0.0;
    config->fieldWeakeningEnabled     = FALSE;
    config->foc.piD.kp                = 0.0;
    config->foc.piD.ki                = 0.0;
    config->foc.piD.kp                = 0.0;
    config->foc.piD.ki                = 0.0;
    config->foc.vdcNom                = 0.0;
    config->foc.kiFw                  = 0.0;
    config->foc.fwFbMax               = 0.0;
    config->foc.fieldWeakeningEnabled = FALSE;
    config->statorMax                 = 0.0;
    config->rs                        = 0.0;
    config->rr                        = 0.0;
    config->ls                        = 0.0;
    config->lr                        = 0.0;
    config->lm                        = 0.0;
    config->fr                        = 0.0;
    config->torqueControlEnabled      = TRUE;
    config->feedForwardEnabled        = FALSE;
}


void Ifx_MotorModelAcimF32_setPositionSensorResolution(Ifx_MotorModelAcimF32 *model, IfxStdIf_Pos_RawAngle resolution)
{
    model->electricalAngleConst = (float32)(model->params.polePair * IFX_LUT_ANGLE_RESOLUTION)
                                  / (float32)resolution;
}


void Ifx_MotorModelAcimF32_init(Ifx_MotorModelAcimF32 *model, Ifx_MotorModelAcimF32_Config *config)
{
    /* Ensure configuration consistency */
    config->foc.fieldWeakeningEnabled = FALSE;      /* Field weakning not supported */

    /* internal variables --------------------------------------------------------------*/
    model->params.polePair = config->base.polePair;
    Ifx_MotorModelAcimF32_setPositionSensorResolution(model, config->base.positionSensorResolution);

    Ifx_MotorModelAcimF32_setControlPeriod(model, config->base.controlPeriod);

    /* Initialise the field-oriented control -------------------------------------------*/
    Ifx_FocF32_calcParams(&model->focParams, &config->foc, model->controlPeriod);
    Ifx_FocF32_init(&model->foc, &model->focParams);
    Ifx_FocF32_setIdqLimit(&model->foc, config->base.currentMax);

    Ifx_MotorModelAcimF32_setupParams(model, config);

    model->slip                  = 0.0;
    model->est.elSpeed.statorMax = config->statorMax;
    model->est.elSpeed.stator    = 0.0;

    model->type                  = Ifx_F32_MotorModel_Type_acim;

    /* FIXME config->base.voltageGenMax not used */
    /* FIXME config fieldWeakeningEnabled not used */

    model->torqueControlEnabled = config->torqueControlEnabled;
    model->feedForwardEnabled   = config->feedForwardEnabled;
}


static void Ifx_MotorModelAcimF32_reset(Ifx_MotorModelAcimF32 *model)
{
    Ifx_FocF32_reset(&model->foc);
    Ifx_IntegralF32_reset(&model->est.imr);
    Ifx_AngleGenF32_reset(&model->est.elAngle);
}


Ifx_Lut_FxpAngle Ifx_MotorModelAcimF32_estimateRotorFluxAngle(Ifx_MotorModelAcimF32 *model)
{
    Ifx_MotorModelAcimF32_Data *p   = &model->params;
    cfloat32                      idq = model->foc.idq;

    /* Magnetizing current estimator ---------------------------------------------------*/
    float32                       dimr = (idq.real - model->est.imr.uk) / p->tr;
    float32                       imr  = Ifx_IntegralF32_step(&model->est.imr, dimr);

    /* Estimated flux ------------------------------------------------------------------*/
    model->est.fluxR = imr * p->lm;

    /* Slip speed ----------------------------------------------------------------------*/
#ifdef ACIMVEC_SLIP_INPUT

    if (model->base.mode != Emotor_Mode_currentControl)
    {
        model->est.elSpeed.slip = (idq.imag / ((imr * p->tr) + EPS));
    }
    else
    {
        model->est.elSpeed.slip = model->slip;
    }

#else
    model->est.elSpeed.slip = (idq.imag / ((imr * p->tr) + EPS));
#endif

    /* Stator flux speed ---------------------------------------------------------------*/
    {
        float32 elSpeedS;
        elSpeedS                  = model->electricalSpeed + model->est.elSpeed.slip;
        elSpeedS                  = __saturatef(elSpeedS, -model->est.elSpeed.statorMax, model->est.elSpeed.statorMax);
        model->est.elSpeed.stator = elSpeedS;

        /* DQ frame angle --------------------------------------------------------------*/
        Ifx_AngleGenF32_step(&model->est.elAngle, elSpeedS);
        return (Ifx_Lut_FxpAngle)Ifx_AngleGenF32_getValue(&model->est.elAngle);
    }
}


cfloat32 Ifx_MotorModelAcimF32_decoupling(Ifx_MotorModelAcimF32 *model)
{
    cfloat32                      vdq;
    cfloat32                      idq = model->foc.idq;
    Ifx_MotorModelAcimF32_Data *p   = &model->params;

#if 0
    float32                       lss = model->est.elSpeed.stator * (p->ls + p->lr);
    vdq.real = (p->rs * idq.real) - (lss * idq.imag);
    vdq.imag = (p->rs * idq.imag) + (lss * idq.real) + (self->est.elSpeed.stator * self->est.fluxR);
#else
    float32                       tmp = model->est.elSpeed.stator * p->kl;
    vdq.real = -((tmp * idq.imag) + (p->lmr * model->est.imr.uk / p->tr));
    vdq.imag = -((tmp * idq.real) + (p->lmr * model->est.imr.uk));
#endif
#if IFX_FOCF32_VOLTAGE_IN_VOLTS
    return vdq;
#else
    return Ifx_SvmF32_getModIndex(vdq, model->vdc);
#endif
}


void Ifx_MotorModelAcimF32_currentStep(Ifx_MotorModelAcimF32 *model, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output)
{
    cfloat32              mab;
    Ifx_FocF32          *foc = &model->foc;
    IfxStdIf_Pos_RawAngle electricalAngle;

    foc->ref = input->ref.current;

    /* Stator current inputs -----------------------------------------------------------*/

    electricalAngle = model->electricalAngle;
    Ifx_FocF32_stepIn(foc, electricalAngle, input->state.currents);

    if (model->feedForwardEnabled)
    {
        Ifx_FocF32_feedForward(foc, Ifx_MotorModelAcimF32_decoupling(model));
    }

    mab                     = Ifx_FocF32_stepOut(foc, electricalAngle);
#if IFX_FOCF32_VOLTAGE_IN_VOLTS
    mab                     = Ifx_SvmF32_getModIndex(mab, model->vdc); // convert to mod index if needed.
#endif
    output->modulationIndex = mab;
}


void Ifx_MotorModelAcimF32_torqueStep(Ifx_MotorModelAcimF32 *model, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output)
{
    Ifx_MotorModelAcimF32_Data *p = &model->params;
    float32                       iqLimit_sq, iqLimit, idRef, iqRef;
    float32                       torqueRef;

    torqueRef = input->ref.torque;

    /* DQ stator current reference -----------------------------------------------------*/
    idRef      = p->fr / p->lm;
    iqLimit_sq = __sqrf(Ifx_FocF32_getIdqLimit(&model->foc)) - __sqrf(idRef);

    if (iqLimit_sq < 0)
    {
        iqRef = 0;
    }
    else
    {
        iqLimit = __sqrtf(iqLimit_sq);
#if 0
        // Using referenced flux:
        iqRef = __saturatef(torqueRef / (p->fr * p->kt), -iqLimit, iqLimit);
#else
        // Using estimated flux:
        iqRef = __saturatef(torqueRef / (model->est.fluxR * p->kt), -iqLimit, iqLimit);
#endif
    }

    /* Transfer the current references to current controller (FOC) block ---------------*/
    input->ref.current.real = idRef;
    input->ref.current.imag = iqRef;

    /* Actual torque (estimated) -------------------------------------------------------*/
    output->torque = p->kt * model->est.fluxR * model->foc.idq.imag;
}


static IfxStdIf_MotorModelF32_Param Ifx_MotorModelAcimF32_getParam(Ifx_MotorModelAcimF32 *model)
{
    IfxStdIf_MotorModelF32_Param param;
    /* Ensure the param is reset to zeros */
    /* FIXME enalble all params*/
    memset(&param, 0, sizeof(IfxStdIf_MotorModelF32_Param));
    param.iStall   = model->params.iStall;
    param.polePair = model->params.polePair;
    param.rs       = model->params.rs;
    return param;
}


static void Ifx_MotorModelAcimF32_setTorqueControl(Ifx_MotorModelAcimF32 *model, boolean enabled)
{
    model->torqueControlEnabled = enabled;
}


static boolean Ifx_MotorModelAcimF32_isTorqueControlEnabled(Ifx_MotorModelAcimF32 *model)
{
    return model->torqueControlEnabled;
}


static void Ifx_MotorModelAcimF32_execute(Ifx_MotorModelAcimF32 *model, Ifx_F32_MotorModel_input *input, Ifx_F32_MotorModel_output *output)
{
    if (model->torqueControlEnabled)
    {
        Ifx_MotorModelAcimF32_torqueStep(model, input, output);
    }

    model->electricalSpeed = Ifx_F32_MotorModel_getElectricalSpeed(model->params.polePair, input->state.mechanicalSpeed);
    model->electricalAngle = Ifx_MotorModelAcimF32_estimateRotorFluxAngle(model);
    model->vdc             = input->state.vdc;
    Ifx_MotorModelAcimF32_currentStep(model, input, output);
}

void Ifx_MotorModelAcimF32_setCurrentControllers(Ifx_MotorModelAcimF32 *model, Ifx_FocF32_Pic_Config *piD, Ifx_FocF32_Pic_Config*piQ)
{
	Ifx_FocF32_Config focConfig;

	Ifx_FocF32_getConfig(&model->foc, &focConfig, model->controlPeriod);

	focConfig.piD = *piD;
	focConfig.piQ = *piQ;

    Ifx_FocF32_calcParams(&model->focParams, &focConfig, model->controlPeriod);

}


boolean Ifx_MotorModelAcimF32_stdIfMotorModelInit(IfxStdIf_MotorModelF32 *stdif, Ifx_MotorModelAcimF32 *driver)
{
    /* Ensure the stdif is reset to zeros */
    memset(stdif, 0, sizeof(IfxStdIf_MotorModelF32));

    /* Set the driver */
    stdif->driver = driver;

    /* *INDENT-OFF* Note: this file was indented manually by the author. */
    /* Set the API link */

	stdif->getParam          =(IfxStdIf_MotorModelF32_GetParam   )&Ifx_MotorModelAcimF32_getParam;
	stdif->setTorqueControl          =(IfxStdIf_MotorModelF32_SetTorqueControl   )&Ifx_MotorModelAcimF32_setTorqueControl;
	stdif->isTorqueControlEnable          =(IfxStdIf_MotorModelF32_IsTorqueControlEnabled   )&Ifx_MotorModelAcimF32_isTorqueControlEnabled;
	stdif->execute          =(IfxStdIf_MotorModelF32_Execute   )&Ifx_MotorModelAcimF32_execute;
	stdif->reset            =(IfxStdIf_MotorModelF32_Reset     )&Ifx_MotorModelAcimF32_reset;
	stdif->setControlPeriod =(IfxStdIf_MotorModelF32_SetControlPeriod     )&Ifx_MotorModelAcimF32_setControlPeriod;
	stdif->setCurrentControllers            =(IfxStdIf_MotorModelF32_SetCurrentControllers     )&Ifx_MotorModelAcimF32_setCurrentControllers;
	stdif->setPositionSensorResolution =(IfxStdIf_MotorModelF32_SetPositionSensorResolution     )&Ifx_MotorModelAcimF32_setPositionSensorResolution;
    /* *INDENT-ON* */

    return TRUE;
}
