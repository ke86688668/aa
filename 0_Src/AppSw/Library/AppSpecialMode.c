/**
 * \file AppSpecialMode.c
 * \brief Special modes
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include "Library/AppSpecialMode.h"
#include "Common/Main.h"
#include "Library/AppHwDebugMode.h"
#include "Library/AppHwPulseMode.h"

AppModeSpecial getSpecialMode(void)
{
	AppModeSpecial mode = AppModeSpecial_normal;

	if ((g_Lb.boardVersion.boardType == Lb_BoardType_HybridKit_LogicBoard) && !App_isBoardVersionFamillyFlag(g_Lb.boardVersion.boardVersion)
			&& (g_Lb.boardVersion.boardVersion >= LB_BoardVersion_HybridKit_LogicBoard_3_0))
	{

		uint8 patternRef[6][8] = {
		{0, 1, 2, 3, 4, 5, 6, 7}, // 1: AppModeSpecial_hardwareDebugMode
		{0, 1, 4, 5, 2, 3, 6, 7}, // 2: AppModeSpecial_hardwarePulseMode
		{0, 2, 1, 3, 4, 6, 5, 7}, // 3
		{0, 4, 1, 5, 2, 6, 3, 7}, // 4
		{0, 2, 4, 6, 1, 3, 5, 7}, // 5
		{0, 4, 2, 6, 1, 5, 3, 7}  // 6
		};

		uint8 pattern[8]                   = {0, 0, 0, 0, 0, 0, 0, 0};
		static uint8 in;
		static uint8 out;
		static uint8 m = 0;
		uint8 loop;
		uint8 ref;

		for(loop=0;loop<2;loop++)
		{
			for (out=0;out<8;out++)
			{
				AppLbStdIf_setDigitalOutput(&g_Lb.stdIf, 0, ((out >> 0) & 0x1) != 0);
				AppLbStdIf_setDigitalOutput(&g_Lb.stdIf, 1, ((out >> 1) & 0x1) != 0);
				AppLbStdIf_setDigitalOutput(&g_Lb.stdIf, 2, ((out >> 2) & 0x1) != 0);
				wait(TimeConst_1ms);
				in = 0;
				in =in | (AppLbStdIf_getDigitalInput(&g_Lb.stdIf, 0) ? (1 << 0) : 0);
				in =in | (AppLbStdIf_getDigitalInput(&g_Lb.stdIf, 1) ? (1 << 1) : 0);
				in =in | (AppLbStdIf_getDigitalInput(&g_Lb.stdIf, 2) ? (1 << 2) : 0);

				pattern[out] |= in;

			}
		}


		for(ref=0;ref<6;ref++)
		{
			loop = 0;
			for (out=0;out<8;out++)
			{
				if (pattern[out] == patternRef[ref][out])
				{
					loop++;
				}
			}

			if (loop == 8)
			{
				m |= 1 << (ref+1);
			}
		}

		switch (m)
		{
		case 1 << AppModeSpecial_hardwareDebugMode:
			mode = AppModeSpecial_hardwareDebugMode;
            Ifx_Console_print(ENDL"WARNING: Special mode entered -> Hardware debug mode"ENDL);
            g_Lb.stdIf.configuration->inverter[0].positionSensor[0] = ECU_PositionSensor_none; /* Overwrite position sensor configuration */
            g_Lb.stdIf.configuration->inverter[0].currentSenor.max = 0.0;

            g_Lb.stdIf.configuration->analogInput[0].cutOffFrequency = 1000.0;
            g_Lb.stdIf.configuration->analogInput[0].offset = 0.0;
            g_Lb.stdIf.configuration->analogInput[0].max = 0.0;
            g_Lb.stdIf.configuration->analogInput[0].min = 0.0;

            g_Lb.stdIf.configuration->analogInput[1].cutOffFrequency = 1000.0;
            g_Lb.stdIf.configuration->analogInput[1].offset = 0.0;
            g_Lb.stdIf.configuration->analogInput[1].max = 0.0;
            g_Lb.stdIf.configuration->analogInput[1].min = 0.0;
            *g_Lb.stdIf.disableCurrentSensorCheck = TRUE;
            if (g_Lb.boardVersion.boardVersion >= LB_BoardVersion_HybridKit_LogicBoard_3_3)
            {
                g_Lb.stdIf.configuration->analogInput[0].gain = CFG_ADC_GAIN/CFG_ADC_FULL_VOLTAGE;
                g_Lb.stdIf.configuration->analogInput[1].gain = CFG_ADC_GAIN/CFG_ADC_FULL_VOLTAGE*CFG_HW_DEBUG_MODE_FMAX*60;
            }
            else
            {
                g_Lb.stdIf.configuration->analogInput[0].gain = (9100+1000)/(1000) *CFG_ADC_GAIN/CFG_ADC_FULL_VOLTAGE;
                g_Lb.stdIf.configuration->analogInput[1].gain = (9100+1000)/(1000) *CFG_ADC_GAIN/CFG_ADC_FULL_VOLTAGE*CFG_HW_DEBUG_MODE_FMAX*60;
            }
            break;
		case 1 << AppModeSpecial_hardwarePulseMode:
				mode = AppModeSpecial_hardwarePulseMode;
	            Ifx_Console_print(ENDL"WARNING: Special mode entered -> Hardware pulse mode"ENDL);
	            if(!g_Lb.stdIf.configuration->specialMode.hwPulseMode.enabled)
	            {
	    			mode = AppModeSpecial_normal;
		            Ifx_Console_print(ENDL"ERROR: Special mode not enabled -> Normal mode selected"ENDL);
	            	break;
	            }
	            g_Lb.stdIf.configuration->inverter[0].positionSensor[0] = ECU_PositionSensor_none; /* Overwrite position sensor configuration */
	            g_Lb.stdIf.configuration->inverter[0].currentSenor.max = 0.0;

	            *g_Lb.stdIf.disableCurrentSensorCheck = TRUE;

	            {
	            	/* Initialize the driver board depending on the board version */
	            	LB_BoardVersion driverBoardVersion= AppDbStdIf_getBoardVersion(&g_Lb.driverBoardStdif)->boardVersion;
	            	Lb_BoardType driverBoardType= AppDbStdIf_getBoardVersion(&g_Lb.driverBoardStdif)->boardType;
	            	if (driverBoardType == Lb_BoardType_HybridKit_DriverBoardHp2)
	            	{
		            	switch (driverBoardVersion)
		            	{
		            	case LB_BoardVersion_HybridKit_DriverBoardHp2_3_1:
		            	case LB_BoardVersion_HybridKit_DriverBoardHp2_3_2:
		            		g_Lb.driverBoards.driverBoard_3x.configuration.inverter.frequency = 1.0 / g_Lb.stdIf.configuration->specialMode.hwPulseMode.pwmPeriod;
		            		break;
		            	default:
		        			mode = AppModeSpecial_unknown;
		            		break;

		            	}

	            	}
	            	else if (driverBoardType == Lb_BoardType_HybridKit_DriverBoardHpDrive)
	            	{
		            	switch (driverBoardVersion)
		            	{
		            	case LB_BoardVersion_HybridKit_DriverBoardHpDrive_1_3:
		            		g_Lb.driverBoards.driverBoard_3x.configuration.inverter.frequency = 1.0 / g_Lb.stdIf.configuration->specialMode.hwPulseMode.pwmPeriod;
		            		break;
		            	default:
		        			mode = AppModeSpecial_unknown;
		            		break;

		            	}

	            	}
	            	else if (driverBoardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
	            	{
		            	switch (driverBoardVersion)
		            	{
		            	case LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_0:
		            	case LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_1:
		            	case LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_2:
		            		g_Lb.driverBoards.driverBoard_HPDSense.configuration.inverter.frequency = 1.0 / g_Lb.stdIf.configuration->specialMode.hwPulseMode.pwmPeriod;
		            		break;
		            	default:
		        			mode = AppModeSpecial_unknown;
		            		break;

		            	}

	            	}
	            	else if (driverBoardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
	            	{
		            	switch (driverBoardVersion)
		            	{
		            	case LB_BoardVersion_HybridKit_DriverBoardHpDriveSenseDsc_1_0:
		            	case LB_BoardVersion_HybridKit_DriverBoardHpDriveSenseDsc_1_1:
		            		g_Lb.driverBoards.driverBoard_HPDSense.configuration.inverter.frequency = 1.0 / g_Lb.stdIf.configuration->specialMode.hwPulseMode.pwmPeriod;
		            		break;
		            	default:
		        			mode = AppModeSpecial_unknown;
		            		break;

		            	}
	            	}
	            	else
	            	{ /* Board unknown */
	        			mode = AppModeSpecial_unknown;
	            	}

	            	if (mode == AppModeSpecial_unknown)
	            	{
	                    Ifx_Console_print(ENDL"WARNING: Unknown driver board version %x"ENDL, driverBoardVersion);
	            	}

	            }
				break;
		case 1 << AppModeSpecial_hardwareReservedMode3:
		case 1 << AppModeSpecial_hardwareReservedMode4:
		case 1 << AppModeSpecial_hardwareReservedMode5:
		case 1 << AppModeSpecial_hardwareReservedMode6:
			mode = AppModeSpecial_unknown;
			Ifx_Console_print(ENDL"WARNING: Special mode entered -> Reserved mode %d"ENDL, m);

			break;
		default:
			mode = AppModeSpecial_normal;
			break;
		}
	}

	return mode;
}



void HandleSpecialModeBackground(void)
{
	if(g_App.hwState.specialMode == AppModeSpecial_hardwareDebugMode)
	{
		AppHwDebugMode_Background();
	}
	else if(g_App.hwState.specialMode == AppModeSpecial_hardwarePulseMode)
	{
		AppHwPulseMode_Background();
	}
}


void HandleSpecialModeOneMs(void)
{
	if(g_App.hwState.specialMode == AppModeSpecial_hardwareDebugMode)
	{
		AppHwDebugMode_OneMs();
	}
	else if(g_App.hwState.specialMode == AppModeSpecial_hardwarePulseMode)
	{
		AppHwPulseMode_OneMs();
	}

}

void HandleSpecialModeTenMs(void)
{
	if(g_App.hwState.specialMode == AppModeSpecial_hardwareDebugMode)
	{
		AppHwDebugMode_TenMs();
	}
	else if(g_App.hwState.specialMode == AppModeSpecial_hardwarePulseMode)
	{
		AppHwPulseMode_TenMs();
	}
}

void HandleSpecialModeEndOfPhaseCurrentConversion(void)
{
	if(g_App.hwState.specialMode == AppModeSpecial_hardwareDebugMode)
	{
		AppHwDebugMode_EndOfPhaseCurrentConversion();
	}
	else if(g_App.hwState.specialMode == AppModeSpecial_hardwarePulseMode)
	{
		AppHwPulseMode_EndOfPhaseCurrentConversion();
	}
}

boolean HandleSpecialModeInit(void)
{
	boolean result = FALSE;
	if(g_App.hwState.specialMode == AppModeSpecial_hardwareDebugMode)
	{
		result = AppHwDebugMode_Init();
	}
	else if(g_App.hwState.specialMode == AppModeSpecial_hardwarePulseMode)
	{
		result = AppHwPulseMode_Init();
	}

    return result;

}

