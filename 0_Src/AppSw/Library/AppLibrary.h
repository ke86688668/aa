/**
 * \file AppLibrary.h
 * \brief Motor control block
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 *  FIXME doxygen group name
 * \defgroup docsrc_application_motorControl 2 - Motor control block
 * \ingroup main_sourceCodeApplication
 *
 */

#ifndef APPLIBRARY_H
#define APPLIBRARY_H

#include "Common/AppInterface.h"
#include "SysSe/EMotor/Ifx_MotorControlF32.h"
#include "SysSe/EMotor/Ifx_MotorModelPmsmF32.h"
#include "SysSe/EMotor/Ifx_MotorModelAcimF32.h"

/** \addtogroup docsrc_application_motorControl
 * \{ */

/** Motor control data
 *
 */
typedef struct
{
    struct
    {
        float32            currents[ECU_PHASE_PER_MOTOR_COUNT];
        ECU_InverterStatus status;
        float32            vDc;
        float32            position;
        float32            speed;
    }                  AppState;

    IfxStdIf_Inverter *stdIfInverter;
    union
    {
        Ifx_MotorModelPmsmF32 pmsm;
        Ifx_MotorModelAcimF32 acim;
    }                       motorModel;
    IfxStdIf_MotorModelF32 stdifMotorModel;
    Ifx_MotorControlF32    motorControl;
} AppLibrary;
/** \} */

IFX_EXTERN AppLibrary g_appLibrary;

#endif /* APPLIBRARY_H */
