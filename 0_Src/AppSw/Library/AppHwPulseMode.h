/**
 * \file AppHwPulseMode.h
 * \brief Hardware pulse mode
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup docsrc_application_specialMode_HwPulseMode Hardware pulse mode
 * \ingroup docsrc_application_specialMode
 *
 */

#ifndef APPHWPULSEMODE_H
#define APPHWPULSEMODE_H

#include "Common/AppInterface.h"
#include "StdIf/IfxStdIf_Inverter.h"


/** \addtogroup docsrc_application_specialMode_HwPulseMode
 * \{ */



/** Special mode control data
 *
 */
typedef struct
{
    IfxStdIf_Inverter *stdIfInverter;
	struct
	{
		boolean running;				/**< \brief TRUE when pulse sequence is running */
		uint32 runRequestStateCount;	/**< \brief number of ms since the last runRequest state change. limited to 100 */
		boolean lastRunRequest;			/**< \brief last run request */
		uint32 error;					/**< \brief an error has been detected (3), is being handled (2), is being re-enabled (1), no error (0) */
	}control;

	struct
	{
		uint8 phaseIndex;			/**< \brief sequence index */
		sint32 pulseIndex;  			/**< \brief Index of the current pulse for the phase */
		boolean finished;				/**< \brief TRUE when the sequence is over */
		boolean started;				/**< \brief TRUE when the sequence is started */

		float32 pulseOn[6];     		/**< \brief Pulse width on U top, V top, W top, U bottom, V bottom, W bottom */
	    float32 pulseOffset[6];     	/**< \brief Pulse offset for U top, V top, W top, U bottom, V bottom, W bottom */
	    uint32 nextPulseCountDown;		/**< \brief Down counter for the next pulse, Next pulse if applied when value is 0 */
	    uint32 nextPulseCountDownReload; /**< \brief Down counter reload value for the next pulse */
	}sequenceState;

    struct
    {
    	float32 pulsePeriod;		/**< \brief Pulse period in s */
    	float32 startPulse;         /**< \brief Start pulse in s */
    	float32 pulseIncrement;     /**< \brief Pulse increment in s */
    	uint32 pulseCountPerPhase;  /**< \brief Number of pulse per phase per sequence */
    	float32 pwmPeriod;			/**< \brief PWM period */
		uint8 sequence[6][6];		/**< \brief Pulse sequence definition. 1 is ON, 0 is OFF, 2 is pulse */

    }sequenceParameter;
} AppHwPulseMode;

/** \} */

IFX_EXTERN AppHwPulseMode g_appHwPulseMode;

void AppHwPulseMode_Background(void);
void AppHwPulseMode_OneMs(void);
void AppHwPulseMode_TenMs(void);
void AppHwPulseMode_EndOfPhaseCurrentConversion(void);
boolean AppHwPulseMode_Init(void);


#endif // APPHWPULSEMODE_H
