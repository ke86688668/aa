/**
 * \file AppHwDebugMode.c
 * \brief Hardware debug mode
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include  "AppHwDebugMode.h"

#include "Common/AppInterface.h"
#include "Common/Main.h"
#include "Common/AppCan.h"
#include "SysSe/EMotor/Ifx_SvmF32.h"

AppHwDebugMode g_appHwDebugMode; /**< \brief General system information \ingroup main_sourceCodeGlobals */



void AppHwDebugMode_Background(void)
{
    Ifx_MotorControlF32_backgroundTask(&g_appHwDebugMode.motorControl);
}


void AppHwDebugMode_OneMs(void)
{
	boolean runRequest;
	float32 amplitudeRequest;
	float32 freqencyRequest;

	runRequest = !AppLbStdIf_getDigitalInput(&g_Lb.stdIf, 3);
	amplitudeRequest = AppLbStdIf_getAnalogInput(&g_Lb.stdIf, 0);
	freqencyRequest = AppLbStdIf_getAnalogInput(&g_Lb.stdIf, 1);
    ECU_InverterStatus status = ECU_getInverterStatus(0);


    {   /* Update according to UI */

        if (Ifx_MotorControlF32_isRunning(&g_appHwDebugMode.motorControl))
        {   /* Set command values */
            Ifx_MotorControl_Mode mode;
            mode = Ifx_MotorControlF32_getMode(&g_appHwDebugMode.motorControl);

            if (runRequest)
            {
                switch (mode)
                {
                case Ifx_MotorControl_Mode_passive:
                    break;
                case Ifx_MotorControl_Mode_openLoop:

					Ifx_MotorControlF32_setOpenLoopAmplitude(&g_appHwDebugMode.motorControl, amplitudeRequest);
					Ifx_MotorControlF32_setSpeed(&g_appHwDebugMode.motorControl, freqencyRequest);
                    break;
                default:
                    break;
                }
            }
            else
            {
                Ifx_MotorControlF32_stop(&g_appHwDebugMode.motorControl);
            }
        }
        else
        {

            if ((runRequest)
        			&& (amplitudeRequest < 0.1)
        			&& (freqencyRequest < 10*60)
        			&& (!status.faultMain)
        			&& (ECU_getInverterVdc(0) < 50.0))
            {
                Ifx_MotorControlF32_setMode(&g_appHwDebugMode.motorControl, Ifx_MotorControl_Mode_openLoop);
                Ifx_MotorControlF32_start(&g_appHwDebugMode.motorControl);
            }
            else
            {
				Ifx_MotorControlF32_setMode(&g_appHwDebugMode.motorControl, Ifx_MotorControl_Mode_passive);
            }
        }
    }

    Ifx_MotorControlF32_speedStep(&g_appHwDebugMode.motorControl);

    {   /* Update UI */

        if (status.faultMain)
        {   /* FIXME move this to ECU_slotEndOfPhaseCurrentConversion() for better reaction time, use Ifx_MotorControlF32_getMode() == Ifx_MotorControl_Mode_error (to be implemented)*/
            g_App.hwState.hwModeRequest = AppMode_error;
        }

		g_App.can.outbox.mode            = Ifx_MotorControlF32_getMode(&g_appHwDebugMode.motorControl);
		g_App.can.outbox.run             = Ifx_MotorControlF32_isRunning(&g_appHwDebugMode.motorControl);
		g_App.can.outbox.speedMeas       = Ifx_MotorControlF32_getSpeed(&g_appHwDebugMode.motorControl);
		g_App.can.outbox.torqueMeas      = Ifx_MotorControlF32_getTorque(&g_appHwDebugMode.motorControl);
		g_App.can.outbox.mechPos   		 = Ifx_MotorControlF32_getSensorPosition(&g_appHwDebugMode.motorControl);
		g_App.can.outbox.elecPos   	     = 0; /* FIXME todo*/


    }

}

void AppHwDebugMode_TenMs(void)
{

}

void AppHwDebugMode_EndOfPhaseCurrentConversion(void)
{
	Ifx_MotorControlF32_execute(&g_appHwDebugMode.motorControl);
}

boolean AppHwDebugMode_Init(void)
{
    Ifx_SvmF32_init();

    g_appHwDebugMode.stdIfInverter = g_Lb.stdIf.inverter;



    float32 controlPeriod = IfxStdIf_Inverter_getPeriod(g_appHwDebugMode.stdIfInverter);

    {
        Ifx_MotorModelPmsmF32_Config config;
        Ifx_MotorModelPmsmF32_initConfig(&config);
        config.base.controlPeriod        = controlPeriod;
        config.base.currentMax           = 0.0;
        config.base.polePair             = 1;

        config.voltageGenMax             = 0.0;
        config.fieldWeakeningEnabled     = FALSE;
        config.foc.piD.kp                = 0.0;
        config.foc.piD.ki                = 0.0;
        config.foc.piQ.kp                = 0.0;
        config.foc.piQ.ki                = 0.0;
        config.foc.vdcNom                = 0.0;
        config.foc.fieldWeakeningEnabled = FALSE;
        config.rs                        = 0.0;
        config.ld                        = 0.0;
        config.lq                        = 0.0;
        config.iStall                    = 0.0;
        config.fluxM                     = 0.0;
        config.torqueControlEnabled      = FALSE;
        config.feedForwardEnabled        = FALSE;

        Ifx_MotorModelPmsmF32_init(&g_appHwDebugMode.pmsm, &config);
        Ifx_MotorModelPmsmF32_stdIfMotorModelInit(&g_appHwDebugMode.stdifMotorModel, &g_appHwDebugMode.pmsm);
    }
    {
        Ifx_MotorControlF32_Config config;
        Ifx_MotorControlF32_initConfig(&config);
        config.piSpeed.kp            = 0.0;
        config.piSpeed.ki            = 0.0;
        config.speedControlPeriod    = 1e-3;
        config.speedMax              = 0.0;
        config.torqueMax             = 0.0;
        config.controlPeriod         = controlPeriod;
        config.torqueRate            = 0.0;
        config.speedRate             = 2*IFX_PI*CFG_HW_DEBUG_MODE_FREQ_RATE; // 10 Hz
        config.motorModel            = &g_appHwDebugMode.stdifMotorModel;
        config.openLoopAmplitudeRate = CFG_HW_DEBUG_MODE_AMPL_RATE;
        config.inverter              = g_appHwDebugMode.stdIfInverter;
        config.positionSensor        = NULL_PTR;
        config.mode                  = Ifx_MotorControl_Mode_passive;
        Ifx_MotorControlF32_init(&g_appHwDebugMode.motorControl, &config);
    }

    return TRUE;

}
