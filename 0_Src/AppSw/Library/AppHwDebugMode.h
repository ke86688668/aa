/**
 * \file AppHwDebugMode.h
 * \brief Hardware debug mode
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup docsrc_application_specialMode_HwDebugMode Hardware debug mode
 * \ingroup docsrc_application_specialMode
 *
 */

#ifndef APPHWDEBUGMODE_H
#define APPHWDEBUGMODE_H

#include "Common/AppInterface.h"
#include "SysSe/EMotor/Ifx_MotorControlF32.h"
#include "SysSe/EMotor/Ifx_MotorModelPmsmF32.h"


/** \addtogroup docsrc_application_specialMode_HwDebugMode
 * \{ */



/** Special mode control data
 *
 */
typedef struct
{
    IfxStdIf_Inverter *stdIfInverter;
	Ifx_MotorModelPmsmF32 pmsm;
    IfxStdIf_MotorModelF32 stdifMotorModel;
    Ifx_MotorControlF32    motorControl;
} AppHwDebugMode;

/** \} */

IFX_EXTERN AppHwDebugMode g_appHwDebugMode;

void AppHwDebugMode_Background(void);
void AppHwDebugMode_OneMs(void);
void AppHwDebugMode_TenMs(void);
void AppHwDebugMode_EndOfPhaseCurrentConversion(void);
boolean AppHwDebugMode_Init(void);



#endif // APPHWDEBUGMODE_H
