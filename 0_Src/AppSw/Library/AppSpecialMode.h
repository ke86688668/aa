/**
 * \file AppSpecialMode.c
 * \brief Special modes
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup docsrc_application_specialMode 7 - Special modes
 * \ingroup main_sourceCodeApplication
 *
 */

#ifndef APPSPECIALMODE_H
#define APPSPECIALMODE_H

#include "Common/AppGlobalDef.h"


/** \addtogroup docsrc_application_specialMode
 * \{ */


/** \} */

AppModeSpecial getSpecialMode(void);
void HandleSpecialModeBackground(void);
void HandleSpecialModeOneMs(void);
void HandleSpecialModeTenMs(void);
void HandleSpecialModeEndOfPhaseCurrentConversion(void);
boolean HandleSpecialModeInit(void);

#endif /* APPSPECIALMODE_H */
