/**
 * \file Configuration.h
 * \brief Global configuration
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup Configuration 4 - Configuration switches
 * \ingroup main_sourceCodeApplication
 * \defgroup Configuration_asc ASC configuration
 * \ingroup Configuration
 * \defgroup Configuration_inverter Inverter configuration
 * \ingroup Configuration
 * \defgroup Configuration_motor Motor configuration
 * \ingroup Configuration
 * \defgroup Configuration_posif Position sensor configuration
 * \ingroup Configuration
 * \defgroup Configuration_adc ADC configuration
 * \ingroup Configuration
 * \defgroup Configuration_limits Limits
 * \ingroup Configuration
 * \defgroup Configuration_dev_test Development configuration (Test purpose)
 * \ingroup Configuration
 * \defgroup Configuration_consoles Console configuration
 * \ingroup Configuration
 * \defgroup Configuration_an General purpose analog input configuration
 * \ingroup Configuration
 * \defgroup Configuration_hw_debug_mode Special mode: Hardware debug mode
 * \ingroup Configuration
 * \defgroup Configuration_safety Safety settings
 * \ingroup Configuration
 * \defgroup Configuration_wizard Wizard settings
 * \ingroup Configuration
 * \defgroup Configuration_hardwarePulseMode Hardware pulse mode
 * \ingroup Configuration
 *
 * FIXME check that All EEPROM configuration switch are configurable by #define
 * FIXME build a table of all configuration switches
 * FIXME add documentation of used hw modules in uC, ex. which ATOM, TOM, ... use dot graph?
 *
 *
 */


#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include "Cfg_Interrupts.h"

//------------------------------------------------------------------------------

/** \addtogroup Configuration_dev_test
 * \{ */
#define CFG_SETUP_TEST                          (1)                 /**< \brief Test setup */
#define CFG_SETUP_MATLAB                        (2)                 /**< \brief Matlab setup (Currently not implemented)*/
#define CFG_SETUP_LIBRARY                       (3)                 /**< \brief Motor library setup */
#define CFG_SETUP                               (CFG_SETUP_LIBRARY) /**< \brief select the software setup. range={CFG_SETUP_TEST, CFG_SETUP_MATLAB, CFG_SETUP_LIBRARY} */
#define CFG_AUTO_CONGIF_SETUP					(1)					/**< \brief if set to 1, the configuration in eeprom will be overwritten config file can't be load */

#define CFG_CURRENT_SENSOR_TEST 				(0)					/* Addon for current sensor test. To be replaced / merged with 2nd motor feature later */
/** \} */

/** \addtogroup Configuration_asc
 * \{ */

#define CFG_ASC0_RX_BUFFER_SIZE                 (128)      /**< \brief Define the Rx buffer size in byte. */
#define CFG_ASC0_TX_BUFFER_SIZE                 (512)      /**< \brief Define the Tx buffer size in byte. */
#define CFG_ASC0_BAUDRATE                       (115200.0) /**< \brief Define the Baudrate */
/** \} */

/** \addtogroup Configuration_consoles
 * \{ */
#define CFG_CONSOLE_TAB                         (4)
#define CFG_CONSOLE_PRIMARY                     (LB_PrimaryConsole_asc0) /**< \brief Select the primary console. This value is overwritten by the EEPROM settings */
#define CFG_CONSOLE_TX_BUFFER_SIZE              (10 * 1024)              /**< \brief Additional TX buffer size for the console  */

#define CFG_CAN_CONSOLE_RX_BUFFER_SIZE          (512)                    /**< \brief Define the Rx buffer size in byte. */
#define CFG_CAN_CONSOLE_TX_BUFFER_SIZE          (512)                    /**< \brief Define the Tx buffer size in byte. */
/** \} */

/** \addtogroup Configuration_asc
 * \{ */
#define CFG_ASC1_RX_BUFFER_SIZE                 (128)      /**< \brief Define the Rx buffer size in byte. */
#define CFG_ASC1_TX_BUFFER_SIZE                 (128)      /**< \brief Define the Tx buffer size in byte. */
#define CFG_ASC1_BAUDRATE                       (115200.0) /**< \brief Define the Baudrate */
#define CFG_ASC1_USE_RS232                      (TRUE)     /**< \brief if TRUE, USB interface  is used else RS485 interface  */
/** \} */

/** \addtogroup Configuration_inverter
 * \{ */
#define INVERTER_TRIGGER_RISING_EDGE_AT_PERIOD  (TRUE)


#define CFG_INVERTER_PWM                        (LB_PwmModule_gtmAtom)              /**< \brief Select the module that generates the PWM. Values={\ref LB_PwmModule_gtmAtom, \ref LB_PwmModule_gtmTom, \ref LB_PwmModule_ccu6}  FIXME only LB_PwmModule_gtmAtom tested. issue for ADC triggering in the other mode  */
#define CFG_INVERTER_FIELD_WEAKENING            (0)

/** \} */

/** \addtogroup Configuration_motor
 * \{ */
#define CFG_MOTOR_POLE_PAIR                     (5)                                    /**< \brief Set the default motor pole pair count */
#define CFG_MOTOR_TYPE                          (Ifx_F32_MotorModel_Type_pmsmStandard) /**<\brief Motor type FIXME only Ifx_F32_MotorModel_Type_pmsmStandard tested */
#define CFG_MOTOR_I_MAX                         ((33.7 * IFX_SQRT_TWO))                /**<\brief Max phase current */
#define CFG_MOTOR_ID_KI                         (25.0)                                 /**<\brief Id PI controller, Ki parameter */
#define CFG_MOTOR_ID_KP                         (0.015)                                /**<\brief Id PI controller, Kp parameter */
#define CFG_MOTOR_IQ_KI                         (25.0)                                 /**<\brief Iq PI controller, Ki parameter */
#define CFG_MOTOR_IQ_KP                         (0.015)                                /**<\brief Iq PI controller, Kp parameter */
#define CFG_MOTOR_RS                            ((0.80 / 2))                           /**<\brief Stator resistor in Ohm */
#define CFG_MOTOR_LD                            ((3.1e-3 / 2))                         /**<\brief (PMSM only) Stator inductance Ld in H */
#define CFG_MOTOR_LQ                            ((3.1e-3 / 2))                         /**<\brief (PMSM only) Stator inductance Lq in H */
#define CFG_MOTOR_KT                            (0.43)                                 /**<\brief (PMSM only) Torque constant in Nm/Arms */
#define CFG_MOTOR_I_STALL                       (8.4)                                  /**<\brief Stall current */

#define CFG_MOTOR_STATOR_MAX                    (0.0)                                  /**< \brief (ACIM only) maximum stator electrical speed (e.g. used at start-up) */
#define CFG_MOTOR_RR                            (0.0)                                  /**< \brief (ACIM only) Motor rotor default resistance in Ohm */
#define CFG_MOTOR_LS                            (0.0)                                  /**< \brief (ACIM only) Motor stator default inductance in H */
#define CFG_MOTOR_LR                            (0.0)                                  /**< \brief (ACIM only) Motor rotor default inductance in H */
#define CFG_MOTOR_LM                            (0.0)                                  /**< \brief (ACIM only) Motor mutual default inductance in H */
#define CFG_MOTOR_FR                            (0.0)                                  /**< \brief (ACIM only) Motor rotor default flux in V.s */

#define CFG_MOTOR_SPEED_MAX                     (6000)                                 /**< \brief Max speed in rpm */
#define CFG_MOTOR_SPEED_KP                      (0.005)                                /**< \brief Speed PI controller, Kp parameter */
#define CFG_MOTOR_SPEED_KI                      (0.30)                                 /**< \brief Speed PI controller, Ki parameter */
#define CFG_MOTOR_TORQUE_MAX                    (11.6)                                 /**< \brief Max torque in Nm */
#define CFG_MOTOR_TORQUE_RATE                   (500)                                  /**< \brief Torque change rate in Nm/s */

/** \} */

/** \addtogroup Configuration_posif
 * \{ */

#define CFG_POSITION_SENSOR                     (ECU_PositionSensor_none) /**< \brief Select the position sensor to be used. range=\ref ECU_PositionSensor. This value is overwritten by the EEPROM settings */

/* Encoder resolution 2048, offset -5605, reversed = TRUE */
/* AD2S1210 resolution 4096, offset -205, reversed= FALSE */
/* AURIX resolver, resolution 4096, offset -1434, reversed = TRUE */
/*
For >LB_BoardVersion_HybridKit_LogicBoard_3_1:
	CFG_POS_SENSOR_AURIX_RESOLVER_GAIN_CODE = LB31_ResolverGain_1_88
	CFG_POS_SENSOR_AD2S1210_GAIN_CODE = LB31_ResolverGain_1_88
For LB_BoardVersion_HybridKit_LogicBoard_3_0:
	CFG_POS_SENSOR_AURIX_RESOLVER_GAIN_CODE = LB30_ResolverGain_1_60
	CFG_POS_SENSOR_AD2S1210_GAIN_CODE = LB30_ResolverGain_1_60
*/

#define CFG_POS_SENSOR_ENCODER_RESOLUTION        	(2048)                        	/**< \brief Encoder resolution */
#define CFG_POS_SENSOR_ENCODER_OFFSET               (0)                      	  	/**<\brief Offset of the encoder sensor in ticks */
#define CFG_POS_SENSOR_ENCODER_REVERSED             (FALSE)                       	/**< \brief If TRUE, the position sensor direction is reversed */

#define CFG_POS_SENSOR_TLE5012_OFFSET               (0)								/**<\brief Offset of the TLE5012 sensor in ticks */
#define CFG_POS_SENSOR_TLE5012_REVERSED             (FALSE)                       	/**< \brief If TRUE, the position sensor direction is reversed */

#define CFG_POS_SENSOR_AD2S1210_OFFSET              (0)								/**<\brief Offset of the resolver AD2S1210 sensor in ticks */
#define CFG_POS_SENSOR_AURIX_RESOLVER_OFFSET        (0)								/**<\brief Offset of the resolver AURIX RDC sensor in ticks */

#define CFG_POS_SENSOR_AURIX_RESOLVER_RESOLUTION    (4096)							/**< \brief Aurix resolver resolution */
#define CFG_POS_SENSOR_AURIX_RESOLVER_REVERSED      (FALSE)                       	/**< \brief If TRUE, the position sensor direction is reversed */
#define CFG_POS_SENSOR_AURIX_RESOLVER_CARRIER_FREQUENCY   (10000)					/**< \brief Carrier frequency in Hz */
#define CFG_POS_SENSOR_AURIX_RESOLVER_PERIOD_PER_ROTATION (1)						/**< \brief Resolver signal period count per mechanical rotation */
#define CFG_POS_SENSOR_AURIX_RESOLVER_SIGNAL_AMPLITUDE_MAX (2700)					/**< \brief Max signal amplitude of cosine and sine in DSADC result unit */
#define CFG_POS_SENSOR_AURIX_RESOLVER_SIGNAL_AMPLITUDE_MIN (2100)					/**< \brief Min signal amplitude of cosine and sine in DSADC result unit */
#define CFG_POS_SENSOR_AURIX_RESOLVER_GAIN_CODE           (LB31_ResolverGain_1_88)                           /**< \brief Signal gain code See board specific values \ref LB30_ResolverGain \ref LB31_ResolverGain */

#define CFG_POS_SENSOR_AD2S1210_RESOLUTION          (4096)                       	/**< \brief AD2S1210 resolution */
#define CFG_POS_SENSOR_AD2S1210_REVERSED            (FALSE)                       	/**< \brief If TRUE, the position sensor direction is reversed */
#define CFG_POS_SENSOR_AD2S1210_CARRIER_FREQUENCY   (10000)                       	/**< \brief Carrier frequency in Hz */
#define CFG_POS_SENSOR_AD2S1210_PERIOD_PER_ROTATION (1)                           	/**< \brief Resolver signal period count per mechanical rotation */
#define CFG_POS_SENSOR_AD2S1210_GAIN_CODE           (LB31_ResolverGain_1_88)		/**< \brief Signal gain code See board specific values \ref LB30_ResolverGain \ref LB31_ResolverGain */

/** \} */

/** \addtogroup Configuration_adc
 * \{ */
#define CFG_ADC_RESOLUTION                      (4096)                                      /**< \brief Define the ADC resolution (12bit => 4096) */
#define CFG_ADC_FULL_VOLTAGE                    (5.0)                                       /**< \brief Define the ADC ref voltage in V. */
#define CFG_ADC_GAIN                            (CFG_ADC_FULL_VOLTAGE / CFG_ADC_RESOLUTION) /**< \brief ADC gain in V per bit */

#define CFG_LEM_LTS25NP_GAIN                    (25.0 - 0.0) / (3.125 - 2.5)                /**< \brief Gain for LEM LTS 25-NP */
#define CFG_LEM_GAIN                            CFG_LEM_LTS25NP_GAIN                        /**< \brief Default current sensor GAIN */

#define CFG_AN_I_GAIN                           (CFG_LEM_GAIN * CFG_ADC_GAIN)               /**< \brief Define the current sensor input gain. Equation y=a.(x+b). */
#define CFG_AN_I_OFFSET                         (-2.5 / CFG_ADC_GAIN)                       /**< \brief Define the current sensor input offset. Equation y=a.(x+b). */

/** \} */
/** \addtogroup Configuration_limits
 *  Runtime values outside the limits will put the application in emergency state
 * \{ */
#define CFG_LIMIT_MAX_MOTOR_PHASE_CURRENT       (5.0)   /**< \brief Maximal absolute motor phase current in A*/

#define CFG_LIMIT_MIN_BOARD_TEMP                (-40.0) /**< \brief Minimal board temperature in �C */
#define CFG_LIMIT_MAX_BOARD_TEMP                (125.0) /**< \brief Maximal board temperature in �C */

#define CFG_LIMIT_MIN_KL30                      (7.0)   /**< \brief Minimal KL30 voltage  */
#define CFG_LIMIT_MAX_KL30                      (18.0)  /**< \brief Maximal KL30 voltage */

/** \} */
/** \addtogroup Configuration_an
 * \{ */

#define CFG_AN0_CUTOFF_FREQUENCY   (0.0) 				/**< \brief Analog input 0 low pass cut off frequency */
#define CFG_AN0_GAIN               (CFG_ADC_GAIN) 		/**< \brief Analog input 0 gain */
#define CFG_AN0_OFFSET             (0.0) 				/**< \brief Analog input 0 offset */
#define CFG_AN0_MAX                (0.0) 				/**< \brief Analog input 0 limit max */
#define CFG_AN0_MIN                (0.0) 				/**< \brief Analog input 0 limit min */

#define CFG_AN1_CUTOFF_FREQUENCY   (0.0) 				/**< \brief Analog input 1 low pass cut off frequency */
#define CFG_AN1_GAIN               (CFG_ADC_GAIN) 		/**< \brief Analog input 1 gain */
#define CFG_AN1_OFFSET             (0.0) 				/**< \brief Analog input 1 offset */
#define CFG_AN1_MAX                (0.0) 				/**< \brief Analog input 1 limit max */
#define CFG_AN1_MIN                (0.0) 				/**< \brief Analog input 1 limit min */

#define CFG_AN2_CUTOFF_FREQUENCY   (0.0) 				/**< \brief Analog input 2 low pass cut off frequency */
#define CFG_AN2_GAIN               (CFG_ADC_GAIN) 		/**< \brief Analog input 2 gain */
#define CFG_AN2_OFFSET             (0.0) 				/**< \brief Analog input 2 offset */
#define CFG_AN2_MAX                (0.0) 				/**< \brief Analog input 2 limit max */
#define CFG_AN2_MIN                (0.0) 				/**< \brief Analog input 2 limit min */

#define CFG_AN3_CUTOFF_FREQUENCY   (0.0) 				/**< \brief Analog input 3 low pass cut off frequency */
#define CFG_AN3_GAIN               (CFG_ADC_GAIN) 		/**< \brief Analog input 3 gain */
#define CFG_AN3_OFFSET             (0.0) 				/**< \brief Analog input 3 offset */
#define CFG_AN3_MAX                (0.0) 				/**< \brief Analog input 3 limit max */
#define CFG_AN3_MIN                (0.0) 				/**< \brief Analog input 3 limit min */

/** \} */

/** \addtogroup Configuration_hw_debug_mode
 * \{ */
#define CFG_HW_DEBUG_MODE_FMAX                       (100.0)		/**< \brief Define the max frequency in Hz. */
#define CFG_HW_DEBUG_MODE_AMPL_RATE                  (0.25)			/**< \brief Define the modulation amplitude change rate in per s. */
#define CFG_HW_DEBUG_MODE_FREQ_RATE                  (10.0)			/**< \brief Define the frequency change rate in Hz/s. */
/** \} */

/** \addtogroup Configuration_safety
 * \{ */
#define CFG_SAFETY_FSP_ENABLED                        			(FALSE)			/**< \brief  Enables / disable the Fault signaling protocol */

#define CFG_SAFETY_FUNCIONAL_WATCHDOG_ENABLED         			(FALSE)			/**< \brief Enables / disable the functional watchdog  */
#define CFG_SAFETY_FUNCIONAL_WATCHDOG_HEARTBEAT_TIMER_PERIOD 	(35e-3)			/**< \brief Functional watchdog heartbeat period in s */
#define CFG_SAFETY_FUNCIONAL_WATCHDOG_SERVICE_PERIOD			(25e-3)			/**< \brief Functional watchdog service period in s */

#define CFG_SAFETY_WINDOW_WATCHDOG_CLOSED_WINDOW_TIME        	(35e-3)			/**< \brief Window watchdog close window time in s */
#define CFG_SAFETY_WINDOW_WATCHDOG_OPENED_WINDOW_TIME        	(10e-3)			/**< \brief Window watchdog open window time in s */
#define CFG_SAFETY_WINDOW_WATCHDOG_USE_WDI_PIN               	(TRUE)			/**< \brief Window watchdog If TRUE, use the WDI pin to service the watchdog, else use the SPI */
#define CFG_SAFETY_WINDOW_WATCHDOG_ENABLED            			(FALSE)			/**< \brief Window watchdog Enables / disable the window watchdog */

#define CFG_SAFETY_IOM_ENABLED                   				(FALSE)			/**< \brief  Enables / disable the IOM monitoring */
#define CFG_SAFETY_IOM_FSP_ON_FAULT              				(FALSE)			/**< \brief  Enables / disable the triggering of FSP on IOM fault */
#define CFG_SAFETY_IOM_NMI_ON_FAULT              				(FALSE)			/**< \brief  Enables / disable the triggering of NMI on IOM fault */
#define CFG_SAFETY_IOM_PIN_FILTER_TIME              			(0.3*1e-6)		/**< \brief  Pin input signal filter time for delay debounce filter  */
#define CFG_SAFETY_IOM_EVENT_WINDOW_THRESHOLD              		((9.6)/*Measured Delay AURIX PWM out -> IOM PWM in*/ + CFG_SAFETY_IOM_PIN_FILTER_TIME + 2 /* margin */)*1e-6		/**< \brief Event window threshold in second */

/** \} */

/** \addtogroup Configuration_wizard
 * \{ */
#define CFG_WIZARD_VDCNOM (35.0)	/**< \brief Define the nominal DC link voltage in V used during wizard operation */
#define CFG_WIZARD_VF 	  (0)		/**< \brief Define the V/F constant used for open loop during the wizard */
/** \} */

/** \addtogroup Configuration_hardwarePulseMode
 * \{ */
#define CFG_HW_PULSE_MODE1_PWM_PERIOD                        (500e-6)	/**< \brief  PWM period used as time base for the pulse generation. The generated pulse can't be bigger than this value */
#define CFG_HW_PULSE_MODE1_PULSE_PERIOD                      (1)		/**< \brief  Pulse period in s */
#define CFG_HW_PULSE_MODE1_START_PULSE                       (10e-6)	/**< \brief  Start pulse in s */
#define CFG_HW_PULSE_MODE1_PULSE_INCREMENT                   (10e-6)	/**< \brief  Pulse increment in s */
#define CFG_HW_PULSE_MODE1_COUNT_PER_PHASE                   (25)		/**< \brief  Number of pulse per phase per sequence */
/** \} */

//------------------------------------------------------------------------------
#endif
