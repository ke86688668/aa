/**
 * \file Cfg_Interrupts.h
 * \brief Interrupts configuration.
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup LB_AppIsr_interrupts Interrupts
 * \ingroup LB
 *
 * \defgroup LB_AppIsr_interruptsPrio Interrupts priorities
 * \ingroup LB_AppIsr_interrupts
 */

#ifndef CFG_INTERRUPTS_H
#define CFG_INTERRUPTS_H

#include "IfxSrc_cfg.h"
//------------------------------------------------------------------------------
/** \brief Build the ISR configuration object
 * \param no interrupt priority
 * \param cpu assign CPU number
 */
#define ISR_ASSIGN(no, cpu)  ((no << 8) + cpu)

/** \brief extract the priority out of the ISR object */
#define ISR_PRIORITY(no_cpu) (no_cpu >> 8)

/** \brief extract the service provider  out of the ISR object */
#define ISR_PROVIDER(no_cpu) (no_cpu % 8)
/**
 * \addtogroup LB_AppIsr_interruptsPrio
 * \{ */

/**
 * \name Interrupt priority configuration.
 * The interrupt priority range is [1,255]
 * \{
 */
#define ISR_PRIORITY_CPU0 200   /**< \brief Define the CPU0 interrupt priority.  */
#define ISR_PRIORITY_CPU1 201   /**< \brief Define the CPU1 interrupt priority.  */
#define ISR_PRIORITY_CPU2 202   /**< \brief Define the CPU1 interrupt priority.  */

#define ISR_PRIORITY_QSPI0_TX         (203) /**< \brief Define the QSPI0 transmit interrupt priority.  */
#define ISR_PRIORITY_QSPI0_RX         (204) /**< \brief Define the QSPI0 receive interrupt priority.  */
#define ISR_PRIORITY_QSPI0_ERR        (205) /**< \brief Define the QSPI0 error interrupt priority.  */

/** \} */

/**
 * \name Interrupt service provider configuration.
 * \{ */
#define ISR_PROVIDER_CPU0 IfxSrc_Tos_cpu0          /**< \brief Define the CPU0 interrupt provider.  */
#define ISR_PROVIDER_CPU1 IfxSrc_Tos_cpu1          /**< \brief Define the CPU1 interrupt provider.  */
#define ISR_PROVIDER_CPU2 IfxSrc_Tos_cpu2          /**< \brief Define the CPU2 interrupt provider.  */

#define ISR_PROVIDER_QSPI0            IfxSrc_Tos_cpu0 /**< \brief Define the QSPI0 interrupt provider.  */
/** \} */

/**
 * \name Interrupt configuration.
 * \{ */
#define INTERRUPT_CPU0    ISR_ASSIGN(ISR_PRIORITY_CPU0, ISR_PROVIDER_CPU0)         /**< \brief Define the CPU0 interrupt priority.  */
#define INTERRUPT_CPU1    ISR_ASSIGN(ISR_PRIORITY_CPU1, ISR_PROVIDER_CPU1)         /**< \brief Define the CPU1 interrupt priority.  */
#define INTERRUPT_CPU2    ISR_ASSIGN(ISR_PRIORITY_CPU2, ISR_PROVIDER_CPU2)         /**< \brief Define the CPU2 interrupt priority.  */

#define INTERRUPT_QSPI0_TX            ISR_ASSIGN(ISR_PRIORITY_QSPI0_TX, ISR_PROVIDER_QSPI0)              /**< \brief Define the QSPI0 transmit interrupt priority.  */
#define INTERRUPT_QSPI0_RX            ISR_ASSIGN(ISR_PRIORITY_QSPI0_RX, ISR_PROVIDER_QSPI0)              /**< \brief Define the QSPI0 receive interrupt priority.  */
#define INTERRUPT_QSPI0_ERR           ISR_ASSIGN(ISR_PRIORITY_QSPI0_ERR, ISR_PROVIDER_QSPI0)             /**< \brief Define the QSPI0 error interrupt priority.  */

/** \} */

/** \} */
//------------------------------------------------------------------------------

#endif
