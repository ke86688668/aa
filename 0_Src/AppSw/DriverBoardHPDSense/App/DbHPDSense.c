/**
 * \file DbHPDSense.c
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 */
#include "DbHPDSense.h"
#include "SysSe/Comm/Ifx_Shell.h"
#include "SysSe/Comm/Ifx_Console.h"
#include "DriverBoardHPDSense/Config/DbHPDSense_Configuration.h"
#include "Configuration.h"
#include "IfxPort_PinMap.h"
#include "string.h"
#include "Common/main.h"
#include "Common/AppSetupWizard.h"

boolean DbHPDSense_createConfigFile(DbHPDSense *board, boolean quiet);
boolean DbHPDSense_loadConfigFile(DbHPDSense *board);

boolean DbHPDSense_init(DbHPDSense *board, IfxQspi_SpiMaster *qspi, Ifx_At25xxx *eeprom)
{ // WARNING this function might be called multiple time depending on the board version
    boolean result = TRUE;
    board->boardVersion.boardType    = Lb_BoardType_undefined;
    board->boardVersion.boardVersion = LB_BoardVersion_undefined;
    board->boardVersion.fileVersion  = Lb_FileVersion_undefined;
    board->configuration.fileVersion = Lb_FileVersion_undefined;
    board->isBoardConfiguration      = FALSE;
    board->isBoardVersion            = FALSE;

    result                         &= DbHPDSense_AppPreInitDriverEeprom(board, qspi);

    return result;
}


void DbHPDSense_printRegDiff(Ifx1edi2010as_Address address, Ifx1edi2010as_UData value, Ifx1edi2010as_UData lastValue, boolean diff)
{
	char text[255];
    if (diff)
    {
        if (value != lastValue)
        {
            Ifx_Console_print("    Reg address 0x%x:", address);
            Ifx1edi2010as_registerDump(address, lastValue, text, sizeof(text));

            Ifx_Console_print("%s -> ", text);
            Ifx_Console_print(ENDL);
        }
    }
    else
    {
        Ifx_Console_print("    Reg address 0x%x:", address);
		Ifx1edi2010as_registerDump(address, value, text, sizeof(text));
        Ifx_Console_print(" %s"ENDL,text);
    }
}


void DbHPDSense_dumpDriverInfo(DbHPDSense *driver, IfxStdIf_DPipe *io, boolean diff)
{
#if IFX_CFG_1EDI2010AS_DRIVER_CACHE_ENABLE
	Ifx1edi2010as_Address                i;
    uint32                index;
    static Ifx_1EDI2010AS cache[6];

    Ifx_Console_print("IGBT driver info:"ENDL);

    Ifx1edi2010as_Driver_invalidateCache(&driver->driver._1edi2010as[0]);

    for (index = 0; index < driver->driver._1edi2010as[0].daisyChain.length; index++)
    {
        g_App.hwState.vdcMeasurementEnabled = FALSE; /* Avoid conflicting access on QSPI */
        Ifx1edi2010as_Driver_readSync(&driver->driver._1edi2010as[index]);
        g_App.hwState.vdcMeasurementEnabled = TRUE; /* Avoid conflicting access on QSPI */
        Ifx_Console_print(ENDL "    1EDI2010AS %d dump"ENDL, index);

        Ifx1edi2010as_UData *data      = driver->driver._1edi2010as[index].cache.engine.buffer;
        Ifx1edi2010as_UData *dataCache = (uint16 *)&cache[index];

        for (i = 0; i < driver->driver._1edi2010as[0].cache.engine.ItemCount; i++)
        {
            DbHPDSense_printRegDiff(i, data[i], dataCache[i], diff);

            dataCache[i] = data[i];
        }
    }
#else
#pragma message  "WARNING: DbHPDSense_dumpDriverInfo() disabled because 1EDI2010AS Cache feature is disabled"
    Ifx_Console_print("WARNING: DbHPDSense_dumpDriverInfo() disabled because 1EDI2010AS Cache feature is disabled"ENDL);
	(void)driver;
	(void)io;
	(void)diff;
#endif
}


void DbHPDSense_printRegByAddress(Ifx1edi2010as_Driver *driver, Ifx1edi2010as_Address address)
{
    uint16 data; /* FIXME type for data should be defined by the driver / GcG*/
    char text[255];
    Ifx_Console_printAlign("1EDI2010AS %d", driver->daisyChain.index);

    if (Ifx1edi2010as_Driver_readRegister(driver, address, &data))
    {
        Ifx1edi2010as_registerDump(address, data, text, sizeof(text));

        Ifx_Console_print(" %s"ENDL, text);
    }
    else
    {
        Ifx1edi2010as_registerDump(address, 0, text, sizeof(text));
        Ifx_Console_printAlign("Read Error. %s"ENDL, text);
    }
}


void DbHPDSense_setupDefaultValue(DbHPDSense_FileConfiguration *configuration, LB_FileBoardVersion *boardVersion)
{
    configuration->valid = TRUE;

    configuration->driverSpiFrequency = CFG_DBHPDSENSE_DRIVER_SPI_FREQUENCY; /* FIXME use #define default value */
    configuration->eepromSpiFrequency = CFG_DBHPDSENSE_DRIVER_SPI_FREQUENCY;
    configuration->vdc.fullScaleGain = CFG_DBHPDSENSE_DRIVER_VDC_FULL_SCALE_GAIN;
    configuration->vdc.fullScaleOffset = CFG_DBHPDSENSE_DRIVER_VDC_FULL_SCALE_OFFSET;
    configuration->vdc.zoomGain = CFG_DBHPDSENSE_DRIVER_VDC_ZOOM_GAIN;
    configuration->vdc.zoomOffset = CFG_DBHPDSENSE_DRIVER_VDC_ZOOM_OFFSET;
    configuration->igbtTemp.fullScaleGain = CFG_DBHPDSENSE_DRIVER_IGBT_TEMP_FULL_SCALE_GAIN;
    configuration->igbtTemp.fullScaleOffset = CFG_DBHPDSENSE_DRIVER_IGBT_TEMP_FULL_SCALE_OFFSET;
    configuration->activeClamping.SCFG_DACLC 				 = CFG_DBHPDSENSE_DRIVER_ACTIVE_CLAMPING;
    if (boardVersion->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
    {
    	configuration->blankingTime.SOCP_OCPBT   				 = CFG_DBHPDSENSE_DRIVER_BLANKING_TIME_DSC;
    }
    else
    {
    	configuration->blankingTime.SOCP_OCPBT   				 = CFG_DBHPDSENSE_DRIVER_BLANKING_TIME;
    }
    configuration->twoLevel.turnOnDelay = CFG_DBHPDSENSE_DRIVER_TWO_LEVEL_TURN_ON_DELAY;
    configuration->twoLevel.turnOnLevel = CFG_DBHPDSENSE_DRIVER_TWO_LEVEL_TURN_ON_LEVEL;
    configuration->twoLevel.turnOffDelay = CFG_DBHPDSENSE_DRIVER_TWO_LEVEL_TURN_OFF_DELAY;
    configuration->twoLevel.turnOffLevel = CFG_DBHPDSENSE_DRIVER_TWO_LEVEL_TURN_OFF_LEVEL;
    configuration->twoLevel.vbeCompensation = CFG_DBHPDSENSE_DRIVER_TWO_LEVEL_VBE_COMPENSATION;
    configuration->inverter.frequency                    = CFG_DBHPDRIVE_FREQUENCY;
    configuration->inverter.deadtime                     = CFG_DBHPDRIVE_DEAD_TIME;
    configuration->inverter.minPulse                     = CFG_DBHPDRIVE_MIN_PULSE;
    configuration->inverter.vdcNom                       = CFG_DBHPDRIVE_VDC_NOM;
    configuration->inverter.vdcMaxGen                    = CFG_DBHPDRIVE_VDC_MAX;
    configuration->inverter.vdcMin                       = CFG_DBHPDRIVE_VDC_MIN;
    configuration->inverter.igbtTempMin 				 = CFG_DBHPDRIVE_MIN_IGBT_TEMP;
    configuration->inverter.igbtTempMax 				 = CFG_DBHPDRIVE_MAX_IGBT_TEMP;
}

void DbHPDSense_setupDefaultConfig(DbHPDSense *board)
{
	DbHPDSense_setupDefaultValue(&board->configuration, &board->boardVersion);
}



boolean DbHPDSense_saveConfig(DbHPDSense *board)
{
    return App_updateConfigFile(&board->driver.efsConfig, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION, &board->configuration, sizeof(board->configuration));
}




void DbHPDSense_printBoardConfiguration(DbHPDSense *board, IfxStdIf_DPipe *io)
{
    DbHPDSense_FileConfiguration *configuration = &board->configuration;
    IfxStdIf_DPipe_print(io, ENDL "Driver board information:"ENDL);

    if (board->isBoardVersion)
    {
        App_printBoardVersion(&board->boardVersion, io);

		if (configuration->valid)
		{
			/* Print configuration file version */
			IfxStdIf_DPipe_print(io, "- Configuration file version: %d.%d"ENDL, ((configuration->fileVersion >> 8) & 0xFF), ((configuration->fileVersion >> 0) & 0xFF));

			IfxStdIf_DPipe_print(io, "- PWM: frequency=%f Hz, Deadtime=%f us, Min pulse=%f us"ENDL, configuration->inverter.frequency, (configuration->inverter.deadtime * 1e6), (configuration->inverter.minPulse * 1e6));
			IfxStdIf_DPipe_print(io, "- Vdc              : Nom=%f V, Min=%f V, Max=%f V"ENDL, configuration->inverter.vdcNom, configuration->inverter.vdcMin, configuration->inverter.vdcMaxGen);
			IfxStdIf_DPipe_print(io, "- IGBT Temperature limit: Min=%f �C, Max=%f �C"ENDL, configuration->inverter.igbtTempMin, configuration->inverter.igbtTempMax);

			IfxStdIf_DPipe_print(io, "- Driver SPI frequency: %7.0f"ENDL, configuration->driverSpiFrequency);
			IfxStdIf_DPipe_print(io, "- EEPROM SPI frequency: %7.0f"ENDL, configuration->eepromSpiFrequency);

			IfxStdIf_DPipe_print(io, "- Full scale Vdc scaling: gain(%d)=%4.3f, offset(%d)=%2.1f"ENDL,
					configuration->vdc.fullScaleGain,
					Ifx1edi2010as_Driver_getScaledGain(configuration->vdc.fullScaleGain),
					configuration->vdc.fullScaleOffset,
					Ifx1edi2010as_Driver_getScaledOffset(configuration->vdc.fullScaleOffset)
					);

			IfxStdIf_DPipe_print(io, "- Zoom Vdc scaling: gain(%d)=%4.3f, offset(%d)=%2.1f"ENDL,
					configuration->vdc.zoomGain,
					Ifx1edi2010as_Driver_getScaledGain(configuration->vdc.zoomGain),
					configuration->vdc.zoomOffset,
					Ifx1edi2010as_Driver_getScaledOffset(configuration->vdc.zoomOffset)
					);

			if (board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
			{
				IfxStdIf_DPipe_print(io, "- Full scale IGBT temp scaling: gain(%d)=%4.3f, offset(%d)=%2.1f"ENDL,
						configuration->igbtTemp.fullScaleGain,
						Ifx1edi2010as_Driver_getScaledGain(configuration->igbtTemp.fullScaleGain),
						configuration->igbtTemp.fullScaleOffset,
						Ifx1edi2010as_Driver_getScaledOffset(configuration->igbtTemp.fullScaleOffset)
						);
			}

			IfxStdIf_DPipe_print(io, "- Active clamping mode (SCFG.DACLC): %d"ENDL, configuration->activeClamping.SCFG_DACLC);
			IfxStdIf_DPipe_print(io, "- Blanking time (SOCP.OCPBT): %d"ENDL, configuration->blankingTime.SOCP_OCPBT);
			IfxStdIf_DPipe_print(io, "- Gate turn on plateau level: %d"ENDL, configuration->twoLevel.turnOnLevel);
			IfxStdIf_DPipe_print(io, "- Gate turn on delay %d"ENDL, configuration->twoLevel.turnOnDelay);
			IfxStdIf_DPipe_print(io, "- Gate turn off plateau level %d"ENDL, configuration->twoLevel.turnOffLevel);
			IfxStdIf_DPipe_print(io, "- Gate turn off delay %d"ENDL, configuration->twoLevel.turnOffDelay);
			IfxStdIf_DPipe_print(io, "- VBE compensation %s"ENDL, configuration->twoLevel.vbeCompensation == Ifx1edi2010as_VbeCompensationEnable_enabled ? "enabled" : "disabled");
		}
		else
		{
			IfxStdIf_DPipe_print(io, "- Configuration invalid"ENDL);
		}
    }
    else
    {
        IfxStdIf_DPipe_print(io, "- Board version invalid"ENDL);
    }
}


boolean DbHPDSense_shellSetup(DbHPDSense *board, pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean              result              = FALSE;

    if (Ifx_Shell_matchToken(&args, "spifrequency") != FALSE)
    {
        float32 frequency;

        if ((Ifx_Shell_parseFloat32(&args, &frequency) != FALSE))
        {
            board->configuration.driverSpiFrequency = frequency;
            board->configuration.eepromSpiFrequency = frequency;
            DbHPDSense_saveConfig(board);
            result                                  = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "vdcscale") != FALSE)
    {
        uint32 gain;
        uint32 offset;

        if ((Ifx_Shell_parseUInt32(&args, &gain, FALSE) != FALSE) && (Ifx_Shell_parseUInt32(&args, &offset, FALSE) != FALSE))
        {
            board->configuration.vdc.fullScaleGain = gain;
            board->configuration.vdc.fullScaleOffset = offset;
            DbHPDSense_saveConfig(board);
            result                                  = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "vdczoom") != FALSE)
    {
        uint32 gain;
        uint32 offset;

        if ((Ifx_Shell_parseUInt32(&args, &gain, FALSE) != FALSE) && (Ifx_Shell_parseUInt32(&args, &offset, FALSE) != FALSE))
        {
            board->configuration.vdc.zoomGain = gain;
            board->configuration.vdc.zoomOffset = offset;
            DbHPDSense_saveConfig(board);
            result                                  = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "igbttempscale") != FALSE)
    {
    	if (board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
    	{
			uint32 gain;
			uint32 offset;

			if ((Ifx_Shell_parseUInt32(&args, &gain, FALSE) != FALSE) && (Ifx_Shell_parseUInt32(&args, &offset, FALSE) != FALSE))
			{
				board->configuration.igbtTemp.fullScaleGain = gain;
				board->configuration.igbtTemp.fullScaleOffset = offset;
				DbHPDSense_saveConfig(board);
				result                                  = TRUE;
			}
    	}
    }
    else if (Ifx_Shell_matchToken(&args, "vdcnom") != FALSE)
    {
        float32 value;

        if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
        {
            board->configuration.inverter.vdcNom = value;
            DbHPDSense_saveConfig(board);
            result                                  = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "vdcmax") != FALSE)
    {
        float32 value;

        if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
        {
            board->configuration.inverter.vdcMaxGen = value;
            DbHPDSense_saveConfig(board);
            result                                     = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "vdcmin") != FALSE)
    {
        float32 value;

        if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
        {
            board->configuration.inverter.vdcMin = value;
            DbHPDSense_saveConfig(board);
            result                                  = TRUE;
        }
    }
	else if (Ifx_Shell_matchToken(&args, "frequency") != FALSE)
	{
		float32 frequency;

		if ((Ifx_Shell_parseFloat32(&args, &frequency) != FALSE))
		{
			board->configuration.inverter.frequency = frequency;
            DbHPDSense_saveConfig(board);
			result                                     = TRUE;
		}
	}
	else if (Ifx_Shell_matchToken(&args, "deadtime") != FALSE)
	{
		float32 value;

		if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
		{
			board->configuration.inverter.deadtime = value;
            DbHPDSense_saveConfig(board);
			result                                    = TRUE;
		}
	}
	else if (Ifx_Shell_matchToken(&args, "minpulse") != FALSE)
	{
		float32 value;

		if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
		{
			board->configuration.inverter.minPulse = value;
            DbHPDSense_saveConfig(board);
			result                                    = TRUE;
		}
	}
    else if (Ifx_Shell_matchToken(&args, "activeclamping") != FALSE)
    {
        uint32 mode;
        boolean error= FALSE;

        if ((Ifx_Shell_parseUInt32(&args, &mode, FALSE) != FALSE))
        {
        	if (mode > 3)
        	{
        		error = TRUE;
    			IfxStdIf_DPipe_print(io, "Mode of range [0,3]"ENDL);
        	}
        	if(!error)
        	{
				board->configuration.activeClamping.SCFG_DACLC = (uint8)mode;
				DbHPDSense_saveConfig(board);
        	}
            result                                  = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "blankingTime") != FALSE)
    {
        uint32 value;
        boolean error= FALSE;

        if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
        {
        	if ((value > 0) && (value < 0xA))
        	{
        		error = TRUE;
    			IfxStdIf_DPipe_print(io, "Mode of range [10,255]"ENDL);
        	}
        	if(!error)
        	{
				board->configuration.blankingTime.SOCP_OCPBT = (uint8)value;
				DbHPDSense_saveConfig(board);
        	}
            result                                  = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "twolevelon") != FALSE)
    {
        uint32 level;
        uint32 delay;
        boolean error= FALSE;

        if ((Ifx_Shell_parseUInt32(&args, &level, FALSE) != FALSE) && (Ifx_Shell_parseUInt32(&args, &delay, FALSE) != FALSE))
        {
        	if (level > Ifx1edi2010as_GateTtonPlateauLevel_gpon6WtoOrHardSwitching )
        	{
        		error = TRUE;
    			IfxStdIf_DPipe_print(io, "Level out of range [0,%d]"ENDL, Ifx1edi2010as_GateTtonPlateauLevel_gpon6WtoOrHardSwitching );
        	}
        	if (delay > 0xFF)
        	{
        		error = TRUE;
    			IfxStdIf_DPipe_print(io, "Delay out of range [0,%d]"ENDL, 0xFF);
        	}
        	if(!error)
        	{
				board->configuration.twoLevel.turnOnLevel = (Ifx1edi2010as_GateTtonPlateauLevel)level;
				board->configuration.twoLevel.turnOnDelay = (uint16)delay;
				DbHPDSense_saveConfig(board);
        	}
            result                                  = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "twoleveloff") != FALSE)
    {
        uint32 level;
        uint32 delay;
        boolean error= FALSE;

        if ((Ifx_Shell_parseUInt32(&args, &level, FALSE) != FALSE) && (Ifx_Shell_parseUInt32(&args, &delay, FALSE) != FALSE))
        {
        	if (level > Ifx1edi2010as_GateRegularTtoffPlateauLevel_gpof7)
        	{
        		error = TRUE;
    			IfxStdIf_DPipe_print(io, "Level out of range [0,%d]"ENDL, Ifx1edi2010as_GateRegularTtoffPlateauLevel_gpof7);
        	}
        	if (delay > 0xFF)
        	{
        		error = TRUE;
    			IfxStdIf_DPipe_print(io, "Delay out of range [0,%d]"ENDL, 0xFF);
        	}

        	if(!error)
        	{
                board->configuration.twoLevel.turnOffLevel = (Ifx1edi2010as_GateRegularTtoffPlateauLevel) level;
                board->configuration.twoLevel.turnOffDelay = (uint16)delay;
                DbHPDSense_saveConfig(board);
        	}
            result                                  = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "twolevelvbecomp") != FALSE)
    {
        uint32 enabled;

        if (Ifx_Shell_parseUInt32(&args, &enabled, FALSE) != FALSE)
        {
            board->configuration.twoLevel.vbeCompensation = enabled ? Ifx1edi2010as_VbeCompensationEnable_enabled : Ifx1edi2010as_VbeCompensationEnable_disabled;
            DbHPDSense_saveConfig(board);
            result                                  = TRUE;
        }
    }


    return result;
}


boolean DbHPDSense_AppPreInitDriverEeprom(DbHPDSense *board, IfxQspi_SpiMaster *qspi)
{
    boolean result = TRUE;

    Ifx_Console_printAlign("Initializing Driver board eeprom:");

    /*Use low frequency as default to support all board type*/
    result &= App_initEeprom(&board->driver.eeprom, qspi, &board->driver.sscEeprom, &IfxQspi0_SLSO1_P20_9_OUT, CFG_DBHPDSENSE_DRIVER_SPI_FREQUENCY);

    Ifx_Efs_Init(&board->driver.efsBoard, &board->driver.eeprom);
    result &= Ifx_Efs_tableOffsetSet(&board->driver.efsBoard, CFG_DB_WITH_EEPROM_BOARD_EFS_OFFSET);
    result &= Ifx_Efs_mount(&board->driver.efsBoard);

    Ifx_Efs_Init(&board->driver.efsConfig, &board->driver.eeprom);
    result &= Ifx_Efs_tableOffsetSet(&board->driver.efsConfig, CFG_DB_WITH_EEPROM_CONFIG_EFS_OFFSET);
    result &= Ifx_Efs_mount(&board->driver.efsConfig);

    board->isBoardVersion       = App_loadBoardVersionFile(&board->driver.efsBoard, &board->boardVersion);

    App_printStatus(result);

    return result;
}


void DbHPDSense_AppInit_print1edi2010asInitializationStatus(uint8 index, Ifx1edi2010as_Driver_Config *config)
{
    Ifx_Console_printAlign("Device %d (", index);

    if (config->hal.en.port != NULL_PTR)
    {
        Ifx_Console_print("EN=P%d.%d, ", index, IfxPort_getIndex(config->hal.en.port), config->hal.en.pinIndex);
        Ifx_Console_print("RST\\=P%d.%d, ", IfxPort_getIndex(config->hal.rstN.port), config->hal.rstN.pinIndex);
    }

    Ifx_Console_print("FLTA\\=P%d.%d, ", IfxPort_getIndex(config->hal.fltAN.port), config->hal.fltAN.pinIndex);
    Ifx_Console_print("FLTB\\=P%d.%d)"ENDL, IfxPort_getIndex(config->hal.fltBN.port), config->hal.fltBN.pinIndex);
}


boolean DbHPDSense_AppInit_1edi2010as(DbHPDSense *board, IfxQspi_SpiMaster *qspi, LB_FileBoardVersion *logicBoardVersion)
{
    boolean result = TRUE;
    App_printStatusTitle("Initializing 1EDI2010AS");
    Ifx_Console_print(" (");
    {   /* Initialze the serial interface */
        IfxQspi_SpiMaster_ChannelConfig spiChannelConfig;
        IfxQspi_SpiMaster_initChannelConfig(&spiChannelConfig, qspi);
        spiChannelConfig.base.baudrate             = board->configuration.driverSpiFrequency;
        spiChannelConfig.base.mode.enabled         = TRUE;
        spiChannelConfig.base.mode.autoCS          = 1;
        spiChannelConfig.base.mode.loopback        = FALSE;
        spiChannelConfig.base.mode.clockPolarity   = SpiIf_ClockPolarity_idleLow;
        spiChannelConfig.base.mode.shiftClock      = SpiIf_ShiftClock_shiftTransmitDataOnLeadingEdge;
        spiChannelConfig.base.mode.dataHeading     = SpiIf_DataHeading_msbFirst;
        spiChannelConfig.base.mode.dataWidth       = 16;
        spiChannelConfig.base.mode.csActiveLevel   = Ifx_ActiveState_low;
        spiChannelConfig.base.mode.csLeadDelay     = SpiIf_SlsoTiming_7;
        spiChannelConfig.base.mode.csTrailDelay    = SpiIf_SlsoTiming_7;
        spiChannelConfig.base.mode.csInactiveDelay = SpiIf_SlsoTiming_7; /* FIXME current QSPI driver 1.0.0.3 will produce wrong timing MCMETILLD-619		*/
        spiChannelConfig.base.mode.parityCheck     = FALSE;              /* FIXME use HW parity */
        spiChannelConfig.base.mode.parityMode      = Ifx_ParityMode_even;
        spiChannelConfig.base.errorChecks.baudrate = FALSE;
        spiChannelConfig.base.errorChecks.phase    = FALSE;
        spiChannelConfig.base.errorChecks.receive  = FALSE;
        spiChannelConfig.base.errorChecks.transmit = FALSE;

        if (logicBoardVersion->boardVersion >= LB_BoardVersion_HybridKit_LogicBoard_3_3)
        {
            spiChannelConfig.sls.output.pin            = &IfxQspi0_SLSO6_P20_10_OUT;
        }
        else
        {
            spiChannelConfig.sls.output.pin            = &IfxQspi0_SLSO0_P20_8_OUT;
        }

        spiChannelConfig.sls.output.mode           = IfxPort_OutputMode_pushPull;
        spiChannelConfig.sls.output.driver         = IfxPort_PadDriver_cmosAutomotiveSpeed1;

        result                                    &= IfxQspi_SpiMaster_initChannel(&board->driver.sscOneEdi2001as, &spiChannelConfig) == SpiIf_Status_ok;

        Ifx_Console_print("QSPI%d, CS%d, %9.0f baud)"ENDL, IfxQspi_getIndex(qspi->qspi), spiChannelConfig.sls.output.pin->slsoNr, spiChannelConfig.base.baudrate);
    }

    {
        Ifx1edi2010as_Driver_Config configTop;
        Ifx1edi2010as_Driver_Config configBottom;
        Ifx1edi2010as_Driver_initConfig(&configTop);
        configTop.hal.channel = &board->driver.sscOneEdi2001as.base;
        configTop.hal.en      = (IfxPort_Pin) {&MODULE_P14, 1};
        /* Jimmy Create for Power Board Enable */
        configTop.hal.PBen	  = (IfxPort_Pin) {&MODULE_P00, 11};

        if (logicBoardVersion->boardVersion >= LB_BoardVersion_HybridKit_LogicBoard_3_3)
        {
            configTop.hal.rstN = (IfxPort_Pin) {&MODULE_P20, 7};
        }
        else
        {   /* Missing dedicated signal connection, on logic board, use same as bottom devices */
            configTop.hal.rstN = (IfxPort_Pin) {&MODULE_P01, 4};
        }
        configTop.hal.rstNActiveState    = Ifx_ActiveState_low;
        configTop.hal.rdy = configTop.hal.rstN;

        configTop.hal.fltAN          = (IfxPort_Pin) {&MODULE_P20, 13};
        configTop.hal.fltBN          = (IfxPort_Pin) {&MODULE_P15, 1};
        configBottom             = configTop;
        configBottom.hal.en          = (IfxPort_Pin) {NULL_PTR, 0};
        /* Jimmy Create for Power Board Enable */
        configBottom.hal.PBen          = (IfxPort_Pin) {NULL_PTR, 0};
        /* end of created */
        configBottom.hal.rstN        = (IfxPort_Pin) {&MODULE_P20, 8};
        configBottom.hal.rdy 		 = configBottom.hal.rstN;
        configBottom.hal.fltAN       = (IfxPort_Pin) {&MODULE_P15, 0};
        configBottom.hal.fltBN       = (IfxPort_Pin) {&MODULE_P14, 0};

        /* FIXME fix driver assignment as defined by the daisy chain HW   */
        configBottom.previousDevice = NULL_PTR;
        Ifx1edi2010as_Driver_init(&board->driver._1edi2010as[0], &configBottom);
        DbHPDSense_AppInit_print1edi2010asInitializationStatus(0, &configBottom); 		/* W Bottom*/

        configTop.previousDevice = &board->driver._1edi2010as[0];
        Ifx1edi2010as_Driver_init(&board->driver._1edi2010as[1], &configTop);
        DbHPDSense_AppInit_print1edi2010asInitializationStatus(1, &configTop);			/* W Top*/

        configTop.hal.en             = (IfxPort_Pin) {NULL_PTR, 0};
        /* Jimmy Create for Power Board Enable */
        configTop.hal.PBen          = (IfxPort_Pin) {NULL_PTR, 0};
        /* end of created */
        configTop.hal.rstN           = (IfxPort_Pin) {NULL_PTR, 0};
        configTop.hal.rdy            = (IfxPort_Pin) {NULL_PTR, 0};
        configBottom.hal.rstN        = (IfxPort_Pin) {NULL_PTR, 0};
        configBottom.hal.rdy         = (IfxPort_Pin) {NULL_PTR, 0};

        configBottom.previousDevice = &board->driver._1edi2010as[1];
        Ifx1edi2010as_Driver_init(&board->driver._1edi2010as[2], &configBottom);
        DbHPDSense_AppInit_print1edi2010asInitializationStatus(2, &configBottom);		/* V Bottom*/

        configTop.previousDevice = &board->driver._1edi2010as[2];
        Ifx1edi2010as_Driver_init(&board->driver._1edi2010as[3], &configTop);
        DbHPDSense_AppInit_print1edi2010asInitializationStatus(3, &configTop);			/* V Top*/

        configBottom.previousDevice = &board->driver._1edi2010as[3];
        Ifx1edi2010as_Driver_init(&board->driver._1edi2010as[4], &configBottom);
        DbHPDSense_AppInit_print1edi2010asInitializationStatus(4, &configBottom);		/* U Bottom*/

        configTop.previousDevice = &board->driver._1edi2010as[4];
        Ifx1edi2010as_Driver_init(&board->driver._1edi2010as[5], &configTop);
        DbHPDSense_AppInit_print1edi2010asInitializationStatus(5, &configTop);			/* U Top*/
    }

    App_printStatus(result);
    return result;
}


boolean DbHPDSense_AppPostInit_1edi2010as(DbHPDSense *board)
{
    boolean result = TRUE;
    uint32 readyFlags;

    App_printStatusTitle("Post initialization of 1EDI2010AS");

    Ifx1edi2010as_Driver_reset(&board->driver._1edi2010as[0], 0x3F);

    wait(TimeConst_1ms);

    readyFlags = Ifx1edi2010as_Driver_isAnyDeviceNotReady(&board->driver._1edi2010as[0]);
    if (readyFlags == 0)
    {
        Ifx1edi2010as_Driver_invalidateCache(&board->driver._1edi2010as[0]);
//      Ifx1edi2010as_Driver_readSync(&board->driver._1edi2010as[0]);
        uint8                       index;
        Ifx1edi2010as_OperatingMode mode;

        for (index = 0; index < board->driver._1edi2010as[0].daisyChain.length; index++)
        {
            uint16 primaryId;
            uint16 secondaryId;
            primaryId = Ifx1edi2010as_getPrimaryChipIdentification(&board->driver._1edi2010as[index], &result);
            secondaryId = Ifx1edi2010as_getSecondaryChipIdentification(&board->driver._1edi2010as[index], &result);
            Ifx_Console_printAlign("Device %d: primary ID=", index);

            switch (primaryId)
            {
            /* FIXME need name for each step, not set in datasheet */
            default:
                Ifx_Console_print("Unknown step: %x", primaryId);
                break;
            }

            Ifx_Console_printAlign("; secondary ID=", index);

            switch (secondaryId)
            {
            /* FIXME need name for each step, not set in datasheet */
            default:
                Ifx_Console_print("Unknown step: %x", secondaryId);
                break;
            }

            Ifx_Console_printAlign(ENDL, index);
        }
        for (index = 0; index < board->driver._1edi2010as[0].daisyChain.length; index++)
        {
            boolean ready;
            ready = Ifx1edi2010as_isSecondaryReady(&board->driver._1edi2010as[index], &result);

            if (!ready)
            {
                Ifx_Console_printAlign("ERROR: Device %d PSTAT.SRDY set. Secondary not ready"ENDL, index);
                result = FALSE;
            }

            mode = Ifx1edi2010as_getOperatingMode(&board->driver._1edi2010as[index], &result);

            if (mode != Ifx1edi2010as_OperatingMode_opm0)
            {
                Ifx_Console_printAlign("ERROR: Device %d invalid operation mode: %d, 0 expected"ENDL, index, mode);
                result = FALSE;
            }

            // Clear PCTRL.CLRP and CLRS
            Ifx1edi2010as_clearPrimarySitckyBits(&board->driver._1edi2010as[index], &result);
            Ifx1edi2010as_clearSecondarySitckyBits(&board->driver._1edi2010as[index], &result);
        }
        result &= Ifx1edi2010as_Driver_writeSync(&board->driver._1edi2010as[0]);

        if (result)
        {
            result &= Ifx1edi2010as_Driver_enterConfigurationMode(&board->driver._1edi2010as[0], 0x3F);

            if (result)
            {
                /* FIXME write custom config. add additional parameters */
                /* Configure the device */
                // Enable DIO1 and DIO2 for temperature measurement,and ODS/ASC feedback
                for (index = 0; index < board->driver._1edi2010as[0].daisyChain.length; index++)
                {
                    mode = Ifx1edi2010as_getOperatingMode(&board->driver._1edi2010as[index], &result);

                    if (mode != Ifx1edi2010as_OperatingMode_opm2)
                    {
                        Ifx_Console_printAlign("ERROR: Device %d invalid operation mode: %d, 2 expected"ENDL, index, mode);
                        result = FALSE;
                    }

                    Ifx1edi2010as_enableSecondaryAdvancedConfiguration(&board->driver._1edi2010as[index], &result);
                }

                result &= Ifx1edi2010as_Driver_writeSync(&board->driver._1edi2010as[0]);
                if (!result)
                {
                    Ifx_Console_printAlign("ERROR: Enable advanced secondary configuration"ENDL, index);
                }

                /* FIXME currently all adc are configured the same, need to be updated for zoom*/
                for (index = 0; index < board->driver._1edi2010as[0].daisyChain.length; index++)
                {
                    Ifx1edi2010as_setDio1PinMode(&board->driver._1edi2010as[index], Ifx1edi2010as_Dio1PinMode_output, &result);
                    Ifx1edi2010as_setDio2PinMode(&board->driver._1edi2010as[index], Ifx1edi2010as_Dio2PinMode_input, &result);

                    /* Active clamping */
                    Ifx1edi2010as_setDaclpPinClampingOutpout(&board->driver._1edi2010as[index], board->configuration.activeClamping.SCFG_DACLC, &result);
                    /* Blanking time */
                    Ifx1edi2010as_setOcpBlankingTime(&board->driver._1edi2010as[index], board->configuration.blankingTime.SOCP_OCPBT, &result);
                    /* Two level trun on / off configuration */
                    Ifx1edi2010as_setGateRegularTtoffPlateauLevel(&board->driver._1edi2010as[index], board->configuration.twoLevel.turnOffLevel, &result);
                    Ifx1edi2010as_setGateRegularTtoffDelay(&board->driver._1edi2010as[index], board->configuration.twoLevel.turnOffDelay, &result);
                    Ifx1edi2010as_setGateTtonPlateauLevel(&board->driver._1edi2010as[index], board->configuration.twoLevel.turnOnLevel, &result);
                    Ifx1edi2010as_setGateTtonDelay(&board->driver._1edi2010as[index], board->configuration.twoLevel.turnOnDelay, &result);
                    Ifx1edi2010as_setVbeCompensationEnable(&board->driver._1edi2010as[index], board->configuration.twoLevel.vbeCompensation, &result);
                }

            	if (board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
            	{
                    index = 2; /* LS V - VDC measurement (full scale) */
                    Ifx1edi2010as_setAdcEnable(&board->driver._1edi2010as[index], 1, &result);
                    Ifx1edi2010as_setAdcOffset(&board->driver._1edi2010as[index], board->configuration.vdc.fullScaleOffset, &result);
                    Ifx1edi2010as_setAdcGain(&board->driver._1edi2010as[index], board->configuration.vdc.fullScaleGain, &result);
                    Ifx1edi2010as_setAdcSecondaryTriggerMode(&board->driver._1edi2010as[index], 0, &result);
                    Ifx1edi2010as_setAdcTriggerInputEnable(&board->driver._1edi2010as[index],  1, &result);

                    board->driver.vdc.fullScaleGain = Ifx1edi2010as_Driver_getScaledGain(board->configuration.vdc.fullScaleGain);
                    board->driver.vdc.fullScaleOffset = Ifx1edi2010as_Driver_getScaledOffset(board->configuration.vdc.fullScaleOffset);

                    index = 0; /* LS W - VDC measurement (zoom) */
                    Ifx1edi2010as_setAdcEnable(&board->driver._1edi2010as[index], 1, &result);
                    Ifx1edi2010as_setAdcOffset(&board->driver._1edi2010as[index], board->configuration.vdc.zoomOffset, &result);
                    Ifx1edi2010as_setAdcGain(&board->driver._1edi2010as[index], board->configuration.vdc.zoomGain, &result);
                    Ifx1edi2010as_setAdcSecondaryTriggerMode(&board->driver._1edi2010as[index], 0, &result);
                    Ifx1edi2010as_setAdcTriggerInputEnable(&board->driver._1edi2010as[index],  1, &result);
                    board->driver.vdc.zoomGain = Ifx1edi2010as_Driver_getScaledGain(board->configuration.vdc.zoomGain);
                    board->driver.vdc.zoomOffset = Ifx1edi2010as_Driver_getScaledOffset(board->configuration.vdc.zoomOffset);
            	}
            	else if (board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
            	{
                    index = 5; /* HS U - IGBT temp measurement  */
                    Ifx1edi2010as_setAdcCurrentSource(&board->driver._1edi2010as[index], Ifx1edi2010as_AdcCurrentSource_enabled, &result);
                    Ifx1edi2010as_setAdcEnable(&board->driver._1edi2010as[index], 1, &result);
                    Ifx1edi2010as_setAdcOffset(&board->driver._1edi2010as[index], board->configuration.igbtTemp.fullScaleOffset, &result);
                    Ifx1edi2010as_setAdcGain(&board->driver._1edi2010as[index], board->configuration.igbtTemp.fullScaleGain, &result);
                    Ifx1edi2010as_setAdcSecondaryTriggerMode(&board->driver._1edi2010as[index], 0, &result);
                    Ifx1edi2010as_setAdcTriggerInputEnable(&board->driver._1edi2010as[index],  1, &result);

                    board->driver.igbtTemp.fullScaleGain = Ifx1edi2010as_Driver_getScaledGain(board->configuration.igbtTemp.fullScaleGain);
                    board->driver.igbtTemp.fullScaleOffset = Ifx1edi2010as_Driver_getScaledOffset(board->configuration.igbtTemp.fullScaleOffset);

                    index = 4; /* LS U - VDC measurement (full scale) */
                    Ifx1edi2010as_setAdcEnable(&board->driver._1edi2010as[index], 1, &result);
                    Ifx1edi2010as_setAdcOffset(&board->driver._1edi2010as[index], board->configuration.vdc.fullScaleOffset, &result);
                    Ifx1edi2010as_setAdcGain(&board->driver._1edi2010as[index], board->configuration.vdc.fullScaleGain, &result);
                    Ifx1edi2010as_setAdcSecondaryTriggerMode(&board->driver._1edi2010as[index], 0, &result);
                    Ifx1edi2010as_setAdcTriggerInputEnable(&board->driver._1edi2010as[index],  1, &result);

                    board->driver.vdc.fullScaleGain = Ifx1edi2010as_Driver_getScaledGain(board->configuration.vdc.fullScaleGain);
                    board->driver.vdc.fullScaleOffset = Ifx1edi2010as_Driver_getScaledOffset(board->configuration.vdc.fullScaleOffset);

                    index = 3; /* HS V - IGBT temp measurement */
                    Ifx1edi2010as_setAdcCurrentSource(&board->driver._1edi2010as[index], Ifx1edi2010as_AdcCurrentSource_enabled, &result);
                    Ifx1edi2010as_setAdcEnable(&board->driver._1edi2010as[index], 1, &result);
                    Ifx1edi2010as_setAdcOffset(&board->driver._1edi2010as[index], board->configuration.igbtTemp.fullScaleOffset, &result);
                    Ifx1edi2010as_setAdcGain(&board->driver._1edi2010as[index], board->configuration.igbtTemp.fullScaleGain, &result);
                    Ifx1edi2010as_setAdcSecondaryTriggerMode(&board->driver._1edi2010as[index], 0, &result);
                    Ifx1edi2010as_setAdcTriggerInputEnable(&board->driver._1edi2010as[index],  1, &result);

                    index = 2; /* LS V - VDC measurement (zoom) */
                    Ifx1edi2010as_setAdcEnable(&board->driver._1edi2010as[index], 1, &result);
                    Ifx1edi2010as_setAdcOffset(&board->driver._1edi2010as[index], board->configuration.vdc.zoomOffset, &result);
                    Ifx1edi2010as_setAdcGain(&board->driver._1edi2010as[index], board->configuration.vdc.zoomGain, &result);
                    Ifx1edi2010as_setAdcSecondaryTriggerMode(&board->driver._1edi2010as[index], 0, &result);
                    Ifx1edi2010as_setAdcTriggerInputEnable(&board->driver._1edi2010as[index],  1, &result);
                    board->driver.vdc.zoomGain = Ifx1edi2010as_Driver_getScaledGain(board->configuration.vdc.zoomGain);
                    board->driver.vdc.zoomOffset = Ifx1edi2010as_Driver_getScaledOffset(board->configuration.vdc.zoomOffset);

                    index = 1; /* HS W - IGBT temp measurement */
                    Ifx1edi2010as_setAdcCurrentSource(&board->driver._1edi2010as[index], Ifx1edi2010as_AdcCurrentSource_enabled, &result);
                    Ifx1edi2010as_setAdcEnable(&board->driver._1edi2010as[index], 1, &result);
                    Ifx1edi2010as_setAdcOffset(&board->driver._1edi2010as[index], board->configuration.igbtTemp.fullScaleOffset, &result);
                    Ifx1edi2010as_setAdcGain(&board->driver._1edi2010as[index], board->configuration.igbtTemp.fullScaleGain, &result);
                    Ifx1edi2010as_setAdcSecondaryTriggerMode(&board->driver._1edi2010as[index], 0, &result);
                    Ifx1edi2010as_setAdcTriggerInputEnable(&board->driver._1edi2010as[index],  1, &result);
            	}



                result &= Ifx1edi2010as_Driver_writeSync(&board->driver._1edi2010as[0]);
                if (result)
                {
                    for (index = 0; index < board->driver._1edi2010as[0].daisyChain.length; index++)
                    {
                    	Ifx1edi2010as_disableSecondaryAdvancedConfiguration(&board->driver._1edi2010as[index], &result);
                    }

                    result &= Ifx1edi2010as_Driver_writeSync(&board->driver._1edi2010as[0]);
                    if (result)
                    {
                        /* Exit Configuration mode */
                        result &= Ifx1edi2010as_Driver_exitConfigurationMode(&board->driver._1edi2010as[0], 0x3F);

                        if (result)
                        {
                            for (index = 0; index < board->driver._1edi2010as[0].daisyChain.length; index++)
                            {
                                mode = Ifx1edi2010as_getOperatingMode(&board->driver._1edi2010as[index], &result);

                                if (mode != Ifx1edi2010as_OperatingMode_opm3)
                                {
                                    Ifx_Console_printAlign("ERROR: Device %d invalid operation mode: %d, 3 expected"ENDL, index, mode);
                                    result = FALSE;
                                }
                            }

                            if (result)
                            {
                                /* Enable the driver*/
                                Ifx1edi2010as_Driver_enable(&board->driver._1edi2010as[0], 0x3F);
                            }
                        }
                        else
                        {
                            Ifx_Console_printAlign("ERROR: Can't exit configuration mode"ENDL, index);
                        }
                    }
                    else
                    {
                        Ifx_Console_printAlign("ERROR: disable advanced secondary configuration"ENDL, index);
                    }
                }
                else
                {
                    Ifx_Console_printAlign("ERROR: Configuration"ENDL, index);
                }
            }
            else
            {
                Ifx_Console_printAlign("ERROR: Can't enter configuration mode"ENDL, index);
            }
        }
        else
        {
            Ifx_Console_printAlign("ERROR: Can't write config to device"ENDL, index);
        }

        if (!result)
        {
            uint32 index;
            Ifx1edi2010as_Driver_invalidateCache(&board->driver._1edi2010as[0]);
            Ifx_Console_printAlign("Additional driver information:"ENDL);

            for (index = 0; index < board->driver._1edi2010as[0].daisyChain.length; index++)
            {
                DbHPDSense_printRegByAddress(&board->driver._1edi2010as[index], ((uint32) & MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT);
                DbHPDSense_printRegByAddress(&board->driver._1edi2010as[index], ((uint32) & MODULE_1EDI2010AS.PSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT);
                DbHPDSense_printRegByAddress(&board->driver._1edi2010as[index], ((uint32) & MODULE_1EDI2010AS.PSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT);
                DbHPDSense_printRegByAddress(&board->driver._1edi2010as[index], ((uint32) & MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT);
                DbHPDSense_printRegByAddress(&board->driver._1edi2010as[index], ((uint32) & MODULE_1EDI2010AS.SSTAT) >> IFX1EDI2010AS_ADDRESS_SHIFT);
                DbHPDSense_printRegByAddress(&board->driver._1edi2010as[index], ((uint32) & MODULE_1EDI2010AS.SSTAT2) >> IFX1EDI2010AS_ADDRESS_SHIFT);
                DbHPDSense_printRegByAddress(&board->driver._1edi2010as[index], ((uint32) & MODULE_1EDI2010AS.SCFG) >> IFX1EDI2010AS_ADDRESS_SHIFT);
                DbHPDSense_printRegByAddress(&board->driver._1edi2010as[index], ((uint32) & MODULE_1EDI2010AS.SCFG2) >> IFX1EDI2010AS_ADDRESS_SHIFT);
                Ifx_Console_print(ENDL);
            }

            /* FIXME hint to power the secondary side in case the secondary register can not be read but the primary are ok*/
        }
    }
    else
    {
        Ifx_Console_printAlign("ERROR: Device not ready");
        result = FALSE;
    }

    App_printStatus(result);
    return result;
}


boolean DbHPDSense_AppSelfTest__1edi2010as(DbHPDSense *board)
{
    boolean success = TRUE;
    uint8   i;
    g_App.hwState.vdcMeasurementEnabled = FALSE; /* Avoid conflicting access on QSPI */
    for (i = 0; i < board->driver._1edi2010as[0].daisyChain.length; i++)
    {
        boolean status = TRUE;
        uint16  prwOrg;
        uint16  prw;
        App_printStatusTitle("Checking 1EDI2010AS %d", i);

        /* Invalidate the cache
         * Backup the original PWR value
         * Write PRW register with a dummy value and read the value back
         * Original value is written after the test
         */

        Ifx1edi2010as_Driver_invalidateCache(&board->driver._1edi2010as[i]);

        /* Backup the register value */
        prwOrg = Ifx1edi2010as_getDataIntegrityTestRegister(&board->driver._1edi2010as[i], &status);

        /* Write a dummy value */
        prw     = 0x2DEB;
        Ifx1edi2010as_setDataIntegrityTestRegister(&board->driver._1edi2010as[i], prw, &status);
        status &= Ifx1edi2010as_Driver_writeSync(&board->driver._1edi2010as[i]);

        /* Read back the dummy value */
        Ifx1edi2010as_Driver_invalidateCache(&board->driver._1edi2010as[i]);
        prw = Ifx1edi2010as_getDataIntegrityTestRegister(&board->driver._1edi2010as[i], &status);

        /* Write back the original value */
        Ifx1edi2010as_setDataIntegrityTestRegister(&board->driver._1edi2010as[i], prwOrg, &status);
        status &= Ifx1edi2010as_Driver_writeSync(&board->driver._1edi2010as[i]);

        if (!status)
        {
            Ifx_Console_printAlign("Frame check: failed"ENDL);
        }
        else if (prw != 0x2DEB)
        {
            Ifx_Console_printAlign("Register read / write: failed"ENDL);
            status = FALSE;
        }
        else
        {
            Ifx_Console_printAlign("Register read / write: passed"ENDL);
        }

        App_printStatus(status);
        success &= status;
    }
    g_App.hwState.vdcMeasurementEnabled = TRUE; /* Avoid conflicting access on QSPI */

    return success;
}


LB_FileBoardVersion* DbHPDSense_getBoardVersion(DbHPDSense *board)
{
	return &board->boardVersion;
}

AppDbStdIf_MainConfig* DbHPDSense_getMainConfig(DbHPDSense *board)
{
	return &board->configuration.inverter;
}

Ifx_Efs*DbHPDSense_getConfigEfs(DbHPDSense *board)
{
	return &board->driver.efsConfig;
}

Ifx_Efs*DbHPDSense_getBoardVersionEfs(DbHPDSense *board)
{
	return &board->driver.efsBoard;
}

boolean DbHPDSense_postInitialization(DbHPDSense *board)
{
	boolean result = TRUE;
    result                              &= DbHPDSense_AppPostInit_1edi2010as(board);

    return result;
}

boolean DbHPDSense_selfTest(DbHPDSense *board)
{
	boolean result = TRUE;
	result &= DbHPDSense_AppSelfTest__1edi2010as(board);
    return result;
}

void DbHPDSense_disableDriver(DbHPDSense *board)
{
    Ifx1edi2010as_Driver_disable(board->driver._1edi2010as, 0x3F);
}

void DbHPDSense_enableDriver(DbHPDSense *board)
{
    Ifx1edi2010as_Driver_enable(board->driver._1edi2010as, 0x3F);
}

uint32 DbHPDSense_getFault(DbHPDSense *board)
{/* FIXME make sure the fault are returned as per AppDbStdIf_GetFault */
    return Ifx1edi2010as_Driver_isAnyDeviceFault(&board->driver._1edi2010as[0]);
}

void DbHPDSense_getIgbtTemp(DbHPDSense *board, float32 *tempArray)
{
	if (board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
	{
		boolean result;
		uint8 fullScalevalue;
		// Scaling: -40�C => 3.32V; 175�C => 1.96V
		float32 a = (-40 -175)/(3.32-1.96);
		float32 b = 175-1.96*a;

		Ifx1edi2010as_Driver_invalidateCacheVolatile(&board->driver._1edi2010as[0]);

/* FIXME reduce cut off frequency to a few 10  ms*/
		result = TRUE;
		fullScalevalue = Ifx1edi2010as_getAdcResult(&board->driver._1edi2010as[5], &result); // HS U
		if (result)
		{
			tempArray[0] = (fullScalevalue/(255.0 * board->driver.igbtTemp.fullScaleGain) + board->driver.igbtTemp.fullScaleOffset);
			tempArray[0] = a*tempArray[0]+b;
		}
		else
		{ /* ERROR, EiceSense could not be read */
			/* FIXME raise error if communication fails, check valid flag, , over & under voltage bits */
		}

		result = TRUE;
		fullScalevalue = Ifx1edi2010as_getAdcResult(&board->driver._1edi2010as[3], &result); // HS V
		if (result)
		{
			tempArray[1] = (fullScalevalue/(255.0 * board->driver.igbtTemp.fullScaleGain) + board->driver.igbtTemp.fullScaleOffset);
			tempArray[1] = a*tempArray[1]+b;
		}
		else
		{ /* ERROR, EiceSense could not be read */
			/* FIXME raise error if communication fails, check valid flag, , over & under voltage bits */
		}

		result = TRUE;
		fullScalevalue = Ifx1edi2010as_getAdcResult(&board->driver._1edi2010as[1], &result); // HS W
		if (result)
		{
			tempArray[2] = (fullScalevalue/(255.0 * board->driver.igbtTemp.fullScaleGain) + board->driver.igbtTemp.fullScaleOffset);
			tempArray[2] = a*tempArray[2]+b;
		}
		else
		{ /* ERROR, EiceSense could not be read */
			/* FIXME raise error if communication fails, check valid flag, , over & under voltage bits */
		}
	}
	else
	{ /* Board does not provide the information */

	}


}

float32 DbHPDSense_getVdc(DbHPDSense *board)
{
	float32 vdc = 0.0;
	boolean result;
	uint8 zoomValue;
	uint8 fullScalevalue;
	float32 r1 = 3200;
	float32 r2 = 100000*5;

	uint32 indexFull = 0;
	uint32 indexZoom = 0;

	if (board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
	{
		indexZoom = 0;// LS W
		indexFull = 2;// LS V
	}
	else if (board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
	{
		indexZoom = 2;// LS V
		indexFull = 4;// LS U
	}


	Ifx1edi2010as_Driver_invalidateCacheVolatile(&board->driver._1edi2010as[indexFull]); /* FIXME add cache feature to invalidate single cached register */

	{ /* Try zoom value first */
		result = TRUE;
		zoomValue = Ifx1edi2010as_getAdcResult(&board->driver._1edi2010as[indexZoom], &result);
		if (result)
		{
			if ((zoomValue > 10) && (zoomValue < 0xF0))
			{ /* Filter out in case of clipping */
				vdc = (zoomValue/(255.0 * board->driver.vdc.zoomGain) + board->driver.vdc.zoomOffset) * ((r1+r2)/r1);
			}
			else
			{ /* Fall back to full range value */
			}
		}
		else
		{ /* ERROR, EiceSense could not be read */
			/* FIXME raise error if communication fails, check valid flag, , over & under voltage bits */
		}

	}

	if (vdc == 0.0)
	{ /* Full scale value first */
		result = TRUE;
		fullScalevalue = Ifx1edi2010as_getAdcResult(&board->driver._1edi2010as[indexFull], &result);
		if (result)
		{
			vdc = (fullScalevalue/(255.0 * board->driver.vdc.fullScaleGain) + board->driver.vdc.fullScaleOffset) * ((r1+r2)/r1);
		}
		else
		{ /* ERROR, EiceSense could not be read */
			/* FIXME raise error if communication fails, check valid flag, , over & under voltage bits */
		}

	}

	return vdc;

}

boolean DbHPDSense_clearFaults(DbHPDSense *board)
{/* Only DESAT error is handled, if other faults are presents, no action is taken */


	uint32 i;
    boolean              status = TRUE;
    Ifx_1EDI2010AS_SER ser;
    Ifx_1EDI2010AS_PER per;
    boolean errorPresent = FALSE;

    Ifx1edi2010as_Driver_invalidateCacheVolatile(&board->driver._1edi2010as[0]);
    for (i=0;i<6;i++)
    {
        status &= Ifx1edi2010as_Driver_readRegister(&board->driver._1edi2010as[i], ((uint32) & MODULE_1EDI2010AS.SER) >> IFX1EDI2010AS_ADDRESS_SHIFT, &ser.U);
        status &= Ifx1edi2010as_Driver_readRegister(&board->driver._1edi2010as[i], ((uint32) & MODULE_1EDI2010AS.PER) >> IFX1EDI2010AS_ADDRESS_SHIFT, &per.U);
        if (status && (ser.B.DESATER == 1))
        {

        	/* Check that only DESAT is set */
        	if ((ser.B.CERS == 0)
        	 && (ser.B.AUVER == 0)
        	 && (ser.B.AOVER == 0)
        	 && (ser.B.VMTO == 0)
        	 && (ser.B.UVLO2ER == 0)
        	 && (ser.B.OCPER == 0)
        	 && (ser.B.RSTS == 0)

        	 && (per.B.CERP == 0)
        	 && (per.B.SPIER == 0)
        	 && (per.B.STPER == 0)
        	 && (per.B.ENER == 0)
        	 && (per.B.RSTP == 0)
        	 && (per.B.RSTEP == 0)
        			)
        	{
        		Ifx1edi2010as_clearPrimarySitckyBits(&board->driver._1edi2010as[i], &status); /* De-assert NFLTA  */
        		Ifx1edi2010as_clearSecondarySitckyBits(&board->driver._1edi2010as[i], &status); /* CLEAR DESATER flag */
        	}
        	else
        	{
        		errorPresent = TRUE;
        	}
        }
    }

    status &= Ifx1edi2010as_Driver_writeSync(&board->driver._1edi2010as[0]);

    return status && !errorPresent;
}
boolean DbHPDSense_isBoardVersionFile(DbHPDSense *board)
{
	return board->isBoardVersion;
}

boolean DbHPDSense_isConfigFile(DbHPDSense *board)
{
	return board->isBoardConfiguration;
}

boolean DbHPDSense_createBoardVersionEfs(DbHPDSense *board, boolean quiet)
{
	boolean result = TRUE;
	result &= unprotectEeprom(&board->driver.eeprom);
    result &= App_createEfs(&board->driver.efsBoard, CFG_DB_WITH_EEPROM_BOARD_EFS_OFFSET, CFG_DB_WITH_EEPROM_BOARD_EFS_PARTITION_SIZE, CFG_DB_BOARD_EFS_FILE_SIZE, quiet);
	result &= protectEeprom(&board->driver.eeprom);

    return result;
}

boolean DbHPDSense_createBoardVersionFile(DbHPDSense *board, boolean quiet)
{
	boolean result = TRUE;
	result &= unprotectEeprom(&board->driver.eeprom);
    board->isBoardVersion = App_createBoardVersionFile(&board->driver.efsBoard, &board->boardVersion, Lb_BoardType_undefined, LB_BoardVersion_undefined, quiet);
	result &= protectEeprom(&board->driver.eeprom);
    return board->isBoardVersion && result;
}

boolean DbHPDSense_createConfigEfs(DbHPDSense *board, boolean quiet)
{
    return App_createEfs(&board->driver.efsConfig, CFG_DB_WITH_EEPROM_CONFIG_EFS_OFFSET, CFG_DB_WITH_EEPROM_CONFIG_EFS_PARTITION_SIZE, CFG_DB_CONFIG_EFS_FILE_SIZE, quiet);
}

boolean DbHPDSense_createConfigFile(DbHPDSense *board, boolean quiet)
{
    DbHPDSense_FileConfiguration *configuration = &board->configuration;
    DbHPDSense_setupDefaultValue(&board->configuration, &board->boardVersion);
    board->isBoardConfiguration = App_createConfigFile(&board->driver.efsConfig, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION, configuration, sizeof(*configuration), EFS_CFG_FILE_VERSION_DRIVER_BOARD_HPDSENSE_CONFIGURATION, quiet);
    return board->isBoardConfiguration;
}


boolean DbHPDSense_loadBoardVersionFile(DbHPDSense *board)
{
    board->isBoardVersion       = App_loadBoardVersionFile(&board->driver.efsBoard, &board->boardVersion);
    return board->isBoardVersion;
}

boolean DbHPDSense_loadConfigFile(DbHPDSense *board)
{
	board->isBoardConfiguration = App_loadConfigFile(&board->driver.efsConfig, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION, &board->configuration, sizeof(board->configuration), EFS_CFG_FILE_VERSION_DRIVER_BOARD_HPDSENSE_CONFIGURATION);

    if (board->isBoardConfiguration)
    { /* FIXME is this required? or can the EEPROM be setup with full speed from startup */
        IfxQspi_SpiMaster_setChannelBaudrate(&board->driver.sscEeprom, board->configuration.eepromSpiFrequency);
    }

    board->configuration.valid  = board->isBoardConfiguration;
    return board->isBoardConfiguration;
}

boolean DbHPDSense_saveBoardVersion(DbHPDSense *board)
{
	boolean result = TRUE;
	result &= unprotectEeprom(&board->driver.eeprom);
	result &= App_updateBoardVersionFile(&board->driver.efsBoard, &board->boardVersion);
	result &= protectEeprom(&board->driver.eeprom);
	return result;
}


boolean DbHPDSense_isBoardTypeSupported(DbHPDSense *board)
{
	return ((board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
    		|| (board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
    		);
}

boolean DbHPDSense_isBoardVersionSupported(DbHPDSense *board)
{
	return ((board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
    		&& (
				(board->boardVersion.boardVersion == LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_0)
				|| (board->boardVersion.boardVersion == LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_1)
				|| (board->boardVersion.boardVersion == LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_2)
				))
			||
			((board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
			    		&& (
							(board->boardVersion.boardVersion == LB_BoardVersion_HybridKit_DriverBoardHpDriveSenseDsc_1_0)
						 || (board->boardVersion.boardVersion == LB_BoardVersion_HybridKit_DriverBoardHpDriveSenseDsc_1_1)
							));
}

boolean DbHPDSense_isConfigFileVersionActual(DbHPDSense *board)
{
	return board->configuration.fileVersion == EFS_CFG_FILE_VERSION_DRIVER_BOARD_HPDSENSE_CONFIGURATION;
}


char DbHPDSense_setupWizard(DbHPDSense *board, Ifx_Shell *shell)
{
	DbHPDSense_FileConfiguration defaultConfig;
	DbHPDSense_setupDefaultValue(&defaultConfig, &board->boardVersion);

    /*************************************************************************/
    /* Inverter PWM frequency in Hz */
	if (AppSetupWizard_askFloat32(shell, "Inverter PWM frequency in Hz", defaultConfig.inverter.frequency, 0, 0, &board->configuration.inverter.frequency) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    /*************************************************************************/
    /* Inverter PWM deadtime in s */
	if (AppSetupWizard_askFloat32(shell, "Inverter PWM deadtime in s", defaultConfig.inverter.deadtime, 0, 0, &board->configuration.inverter.deadtime) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    /*************************************************************************/
    /* Inverter PWM min-pulse in s */
	if (AppSetupWizard_askFloat32(shell, "Inverter PWM min-pulse in s", defaultConfig.inverter.minPulse, 0, 0, &board->configuration.inverter.minPulse) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    /*************************************************************************/
    /* Nominal DC-link voltage in V */
	if (AppSetupWizard_askFloat32(shell, "Nominal DC-link voltage in V", defaultConfig.inverter.vdcNom, 0, 0, &board->configuration.inverter.vdcNom) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    /*************************************************************************/
    /* Maximal allowed DC-link voltage in V */
	if (AppSetupWizard_askFloat32(shell, "Maximal allowed DC-link voltage in V", defaultConfig.inverter.vdcMaxGen, 0, 0, &board->configuration.inverter.vdcMaxGen) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}

	if (AppSetupWizard_askUint8(shell, "Active clamping mode (SCFG.DACLC)", defaultConfig.activeClamping.SCFG_DACLC, 0, 0x3, &board->configuration.activeClamping.SCFG_DACLC) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
	if (AppSetupWizard_askUint8(shell, "Blanking time (SOCP.OCPBT)", defaultConfig.blankingTime.SOCP_OCPBT, 0, 0xFF, &board->configuration.blankingTime.SOCP_OCPBT) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
	if (AppSetupWizard_askUint8(shell, "Two-level turn off delay (SRTTOF.RTVAL)", defaultConfig.twoLevel.turnOffDelay, 0, 0xFF, &board->configuration.twoLevel.turnOffDelay) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    {
    	pchar answers[9] = {"vgpof0", "vgpof1", "vgpof2", "vgpof3", "vgpof4", "vgpof5", "vgpof6", "vgpof7", ""};
    	sint32 option;
    	sint32 defaultOption;
        option = board->configuration.twoLevel.turnOffLevel;
        defaultOption = defaultConfig.twoLevel.turnOffLevel;
    	option = AppSetupWizard_askOptions(shell, "Two-level turn off level (PCTRL2.GPOF)", defaultOption, option, answers);
    	if (option == -1)
    	{
    		return IFX_SHELL_KEY_CTRL_C;
    	}
    	else if (option == 0)
        {
    		board->configuration.twoLevel.turnOffLevel = option;
        }
    }

	if (AppSetupWizard_askUint8(shell, "Two-level turn on delay (STTON.TTONVAL)", defaultConfig.twoLevel.turnOnDelay, 0, 0xFF, &board->configuration.twoLevel.turnOnDelay) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    {
    	pchar answers[9] = {"vgpon0", "vgpon1", "vgpon2", "vgpon3", "vgpon4", "vgpon5", "vgpon6", "hard", ""};
    	sint32 option;
    	sint32 defaultOption;
        option = board->configuration.twoLevel.turnOnLevel;
        defaultOption = defaultConfig.twoLevel.turnOnLevel;
    	option = AppSetupWizard_askOptions(shell, "Two-level turn on level (PCTRL.GPON)", defaultOption, option, answers);
    	if (option == -1)
    	{
    		return IFX_SHELL_KEY_CTRL_C;
    	}
    	else if (option == 0)
        {
    		board->configuration.twoLevel.turnOnLevel = option;
        }
    }

    {
    	pchar answers[9] = {"disabled", "enabled", ""};
    	sint32 option;
    	sint32 defaultOption;
        option = board->configuration.twoLevel.vbeCompensation;
        defaultOption = defaultConfig.twoLevel.vbeCompensation;
    	option = AppSetupWizard_askOptions(shell, "Two-level Vbe compensation (SCFG.VBEC)", defaultOption, option, answers);
    	if (option == -1)
    	{
    		return IFX_SHELL_KEY_CTRL_C;
    	}
    	else if (option == 0)
        {
    		board->configuration.twoLevel.vbeCompensation = option;
        }
    }


	return IFX_SHELL_KEY_NULL;
}

boolean DbHPDSense_DbStdIfInit(AppDbStdIf *stdif, DbHPDSense *board)
{
    memset(stdif, 0, sizeof(*stdif));

    /* Set the API link */
    stdif->driver                        = board;

    /* *INDENT-OFF* */
    /* FIXME check API signature */
    stdif->disableDriver                 = (AppDbStdIf_DisableDriver            )&DbHPDSense_disableDriver;
    stdif->dumpDriverInfo                = (AppDbStdIf_DumpDriverInfo           )&DbHPDSense_dumpDriverInfo;
    stdif->enableDriver                  = (AppDbStdIf_EnableDriver             )&DbHPDSense_enableDriver;
    stdif->getBoardVersion               = (AppDbStdIf_GetBoardVersion          )&DbHPDSense_getBoardVersion;
    stdif->getmainConfig                 = (AppDbStdIf_GetMainConfig            )&DbHPDSense_getMainConfig;
    stdif->getConfigEfs                  = (AppDbStdIf_GetConfigEfs             )&DbHPDSense_getConfigEfs;
    stdif->getFault                      = (AppDbStdIf_GetFault                 )&DbHPDSense_getFault;
    stdif->getVdc                        = (AppDbStdIf_GetVdc                   )&DbHPDSense_getVdc;
    stdif->getIgbtTemp                   = (AppDbStdIf_GetIgbtTemp              )&DbHPDSense_getIgbtTemp;
    stdif->getBoardVersionEfs            = (AppDbStdIf_GetBoardVersionEfs       )&DbHPDSense_getBoardVersionEfs;
    stdif->init                          = (AppDbStdIf_Init                     )&DbHPDSense_init;
    stdif->initDriver                    = (AppDbStdIf_InitDriver               )&DbHPDSense_AppInit_1edi2010as;
    stdif->postInitialization            = (AppDbStdIf_PostInitialization       )&DbHPDSense_postInitialization;
    stdif->printBoardConfiguration       = (AppDbStdIf_PrintBoardConfiguration  )&DbHPDSense_printBoardConfiguration;
    stdif->saveConfig                    = (AppDbStdIf_SaveConfig               )&DbHPDSense_saveConfig;
    stdif->selfTest                      = (AppDbStdIf_SelfTest                 )&DbHPDSense_selfTest;
    stdif->setDefaultConfig              = (AppDbStdIf_SetDefaultConfig         )&DbHPDSense_setupDefaultConfig;
    stdif->shellSetup                    = (AppDbStdIf_ShellSetup               )&DbHPDSense_shellSetup;

    stdif->createBoardVersionEfs         = (AppDbStdIf_CreateBoardVersionEfs    )&DbHPDSense_createBoardVersionEfs    ;
    stdif->createBoardVersionFile        = (AppDbStdIf_CreateBoardVersionFile   )&DbHPDSense_createBoardVersionFile   ;
    stdif->createConfigEfs               = (AppDbStdIf_CreateConfigEfs          )&DbHPDSense_createConfigEfs          ;
    stdif->createConfigFile              = (AppDbStdIf_CreateConfigFile         )&DbHPDSense_createConfigFile         ;
    stdif->isBoardVersionFile            = (AppDbStdIf_IsBoardVersionFile       )&DbHPDSense_isBoardVersionFile       ;
    stdif->isConfigFile                  = (AppDbStdIf_IsConfigFile             )&DbHPDSense_isConfigFile             ;
    stdif->loadBoardVersionFile          = (AppDbStdIf_LoadBoardVersionFile     )&DbHPDSense_loadBoardVersionFile     ;
    stdif->loadConfigFile                = (AppDbStdIf_LoadConfigFile           )&DbHPDSense_loadConfigFile           ;
    stdif->saveBoardVersion              = (AppDbStdIf_SaveBoardVersion         )&DbHPDSense_saveBoardVersion         ;

    stdif->isBoardTypeSupported          = (AppDbStdIf_IsBoardTypeSupported     )&DbHPDSense_isBoardTypeSupported     ;
    stdif->isBoardVersionSupported       = (AppDbStdIf_IsBoardVersionSupported  )&DbHPDSense_isBoardVersionSupported  ;
    stdif->isConfigFileVersionActual     = (AppDbStdIf_IsConfigFileVersionActual)&DbHPDSense_isConfigFileVersionActual;
    stdif->setupWizard                   = (AppDbStdIf_SetupWizard              )&DbHPDSense_setupWizard              ;
    stdif->clearFaults                   = (AppDbStdIf_ClearFaults              )&DbHPDSense_clearFaults              ;

    /* *INDENT-ON* */


    return TRUE;
}

