/**
 * \file DbHPDSense.h
 * \brief Driver board HPD Sense
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup sourceCodeApplication_dbhpdsense Driver board HPD Sense
 * \ingroup LB30
 *
 * Support for boards:
 * - HPD Sense 1.0
 * - HPD Sense 1.1
 * - HPD Sense 1.2
 * - HPD Sense DSC 1.0
 *
 */
#ifndef DBHPDSENSE_H_
#define DBHPDSENSE_H_

#include "Common/AppGlobalDef.h"
#include "SysSe/Ext/1edi2010as/Std/Ifx1edi2010as.h"
#include "Common/AppDbStdIf.h"

/** Configuration file data structure
 *
 * If the data structure is modified, the \ref LB_FileVersion need to be enhanced, and the \ref EFS_CFG_FILE_VERSION_DRIVER_BOARD_HPDSENSE_CONFIGURATION
 * need to be updated.
 * Additionally DB*_setupDefaultValue() must be updated
 */
typedef struct
{
    LB_FileVersion fileVersion;         /**< \brief File version */
    boolean        valid;               /**< \brief TRUE if the configuration data are valid else FALSE */

    float32        driverSpiFrequency;
    float32        eepromSpiFrequency;

    AppDbStdIf_MainConfig inverter;
    struct
    {
    	Ifx1edi2010as_AdcOffset      fullScaleOffset;	   	/**< \brief EiceSence Vdc full scale offset */
    	Ifx1edi2010as_AdcGain        fullScaleGain;	   	/**< \brief EiceSence Vdc full scale gain */
        Ifx1edi2010as_AdcOffset      zoomOffset;	   		/**< \brief EiceSence Vdc zoom offset */
        Ifx1edi2010as_AdcGain        zoomGain;	   			/**< \brief EiceSence Vdc zoom gain */
    }vdc;
    struct
    {
    	Ifx1edi2010as_AdcOffset      fullScaleOffset;	   	/**< \brief EiceSence Igbt temp full scale offset */
    	Ifx1edi2010as_AdcGain      fullScaleGain;	   		/**< \brief EiceSence Igbt temp full scale gain */
    }igbtTemp;
    struct
    {
    	uint8 SCFG_DACLC;									    /**< \brief Active clamping configuration*/
    }activeClamping;
    struct
    {
    	uint8 SOCP_OCPBT;									    /**< \brief OCP Blanking Time */
    }blankingTime;
    struct
    {
    	Ifx1edi2010as_VbeCompensationEnable vbeCompensation;			/**< \brief Vbe compensation */
    	Ifx1edi2010as_GateTtonPlateauLevel turnOnLevel;		/**< \brief Gate turn on plateau level */
    	Ifx1edi2010as_GateRegularTtoffPlateauLevel turnOffLevel;		/**< \brief Gate turn off plateau level */
    	uint8 turnOnDelay;										/**< \brief Turn on delay. range =[0,0xFF] */
    	uint8 turnOffDelay;									    /**< \brief Turn off delay. range =[0,0xFF] */
    }twoLevel;
}DbHPDSense_FileConfiguration;

typedef struct
{
    boolean                isBoardVersion;       /**< \Brief TRUE if the driver board version file is loaded */
    boolean                isBoardConfiguration; /**< \Brief TRUE if the driver board configuration file is loaded */

    LB_FileBoardVersion    boardVersion;
    DbHPDSense_FileConfiguration configuration;

    struct
    {
        Ifx1edi2010as_Driver             _1edi2010as[6]; /**< 1EDI2010AS drivers ={U top, U Bottom, V Top,, V Bottom W Top, W Bottom}*/
        IfxQspi_SpiMaster_Channel sscOneEdi2001as; /**< \brief SSC channel for Edi2001AS */

        IfxQspi_SpiMaster_Channel sscEeprom;       /**< \brief SSC channel for EEPROM */
        Ifx_At25xxx               eeprom;          /**< Ifx_At25xxx driver */
        Ifx_Efs                   efsConfig;       /**< Ifx_At25xxx file system: Configuration */
        Ifx_Efs                   efsBoard;        /**< Ifx_At25xxx file system: Production data */
        struct
        {
            float32      fullScaleOffset;	   	/**< \brief EiceSence Vdc full scale offset */
            float32      fullScaleGain;	   		/**< \brief EiceSence Vdc full scale gain */
            float32      zoomOffset;	   		/**< \brief EiceSence Vdc zoom offset */
            float32      zoomGain;	   			/**< \brief EiceSence Vdc zoom gain */
        }vdc;
        struct
        {
            float32      fullScaleOffset;	   	/**< \brief EiceSence Igbt temp full scale offset */
            float32      fullScaleGain;	   		/**< \brief EiceSence Igbt temp full scale gain */
        }igbtTemp;
    }driver;
}DbHPDSense;
/* FIXME define a standard interface to the logic board to simplify the code */
boolean DbHPDSense_init(DbHPDSense *board, IfxQspi_SpiMaster *qspi, Ifx_At25xxx *eeprom);

boolean DbHPDSense_shellSetup(DbHPDSense *board, pchar args, void *data, IfxStdIf_DPipe *io);
boolean DbHPDSense_AppPostInit_1edi2010as(DbHPDSense *board);

void    DbHPDSense_dumpDriverInfo(DbHPDSense *driver, IfxStdIf_DPipe *io, boolean diff);
boolean DbHPDSense_AppPreInitDriverEeprom(DbHPDSense *board, IfxQspi_SpiMaster *qspi);
boolean DbHPDSense_AppInit_1edi2010as(DbHPDSense *board, IfxQspi_SpiMaster *qspi, LB_FileBoardVersion *logicBoardVersion);

boolean DbHPDSense_AppSelfTest__1edi2010as(DbHPDSense *board);
void    DbHPDSense_printBoardConfiguration(DbHPDSense *board, IfxStdIf_DPipe *io);
void    DbHPDSense_setupDefaultValue(DbHPDSense_FileConfiguration *configuration, LB_FileBoardVersion *boardVersion);
boolean DbHPDSense_saveConfig(DbHPDSense *board);
LB_FileBoardVersion* DbHPDSense_getBoardVersion(DbHPDSense *board);
Ifx_Efs*DbHPDSense_getConfigEfs(DbHPDSense *board);
Ifx_Efs*DbHPDSense_getBoardVersionEfs(DbHPDSense *board);
boolean DbHPDSense_postInitialization(DbHPDSense *board);
boolean DbHPDSense_selfTest(DbHPDSense *board);
void DbHPDSense_disableDriver(DbHPDSense *board);
void DbHPDSense_enableDriver(DbHPDSense *board);
uint32 DbHPDSense_getFault(DbHPDSense *board);
boolean DbHPDSense_DbStdIfInit(AppDbStdIf *stdif, DbHPDSense *board);

#endif /* DBHPDSENSE_H_ */
