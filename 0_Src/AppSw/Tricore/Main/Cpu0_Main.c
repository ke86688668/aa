/**
 * \file Main.c
 * \brief System initialisation and main program implementation.
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

//________________________________________________________________________________________
// INCLUDES

#include "Common/Main.h"

//________________________________________________________________________________________
// IMPORTED VARIABLES

//________________________________________________________________________________________
// GLOBAL VARIABLES
App g_App; /**< \brief General system information \ingroup main_sourceCodeGlobals */

//________________________________________________________________________________________
// LOCAL TYPE DEFINITIONS

//________________________________________________________________________________________
// IMPORTED FUNCTION PROTOTYPES

//________________________________________________________________________________________
// LOCAL FUNCTION PROTOTYPES

//________________________________________________________________________________________
// INITIALISATION FUNCTION IMPLEMENTATIONS

//________________________________________________________________________________________
// MAIN ROUTINE
/** \brief Main entry point after CPU boot-up. */
int core0_main(void)
{
    IfxScuWdt_disableCpuWatchdog(IfxScuWdt_getCpuWatchdogPassword());
    IfxScuWdt_disableSafetyWatchdog(IfxScuWdt_getSafetyWatchdogPassword());

    g_App.initialized           = FALSE;
    g_App.hwState.hwMode        = AppMode_off;
    g_App.hwState.hwModeRequest = AppMode_boot;

    /** Background loop. Handles the initialization, configuration and application execution */
    while (TRUE)
    {		
        AppStateMachine_process(&g_App.hwState);
    }

    return 0;
}
