/**
 * \file Db3x.h
 * \brief Driver board version 3.x
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup sourceCodeApplication_db3x Driver board version 3.x
 * \ingroup LB30
 *
 * Support for boards:
 * - HP2 3.2
 * - HP2 3.1
 * - HPD 1.3
 *
 */
#ifndef DB3X_H_
#define DB3X_H_

#include "Common/AppGlobalDef.h"
#include "SysSe/Ext/1edi2002as/Std/Ifx1edi2002as.h"
#include "Common/AppDbStdIf.h"

/** Configuration file data structure
 *
 * If the data structure is modified, the \ref LB_FileVersion need to be enhanced, and the \ref EFS_CFG_FILE_VERSION_DRIVER_BOARD_3X_CONFIGURATION
 * need to be updated.
 * Additionally DB*_setupDefaultValue() must be updated
 */
typedef struct
{
    LB_FileVersion fileVersion;         /**< \brief File version */
    boolean        valid;               /**< \brief TRUE if the configuration data are valid else FALSE */

    float32        driverSpiFrequency;
    float32        eepromSpiFrequency;

    AppDbStdIf_MainConfig inverter;
    struct
    {
    	uint8 SACLT_AT;											/**< \brief Active clamping activation time see SACLT.AT*/
    	uint8 SCFG2_ACLPM;										/**< \brief Active clamping mode see SCFG2.ACLPM*/
    }activeClamping;
    struct
    {
    	Ifx1edi2002as_VbeCompensation vbeCompensation;			/**< \brief Vbe compensation */
    	Ifx1edi2002as_GateTurnOnPlateauLevel turnOnLevel;		/**< \brief Gate turn on plateau level */
    	Ifx1edi2002as_GateTurnOffPlateauLevel turnOffLevel;		/**< \brief Gate turn off plateau level */
    	uint8 turnOnDelay;										/**< \brief Turn on delay. range =[0,0xF] */
    	uint8 turnOffDelay;								    	/**< \brief Turn off delay. range =[0,0xFF] */
    }twoLevel;
}Db3x_FileConfiguration;

typedef struct
{
    boolean                isBoardVersion;       /**< \Brief TRUE if the driver board version file is loaded */
    boolean                isBoardConfiguration; /**< \Brief TRUE if the driver board configuration file is loaded */

    LB_FileBoardVersion    boardVersion;
    Db3x_FileConfiguration configuration;

    struct
    {
        Ifx1edi2002as_Driver             oneEdi2001as[6]; /**< 1EDI2002AS drivers ={U top, U Bottom, V Top,, V Bottom W Top, W Bottom}*/
        IfxQspi_SpiMaster_Channel sscOneEdi2001as; /**< \brief SSC channel for Edi2001AS */

        IfxQspi_SpiMaster_Channel sscEeprom;       /**< \brief SSC channel for EEPROM */
        Ifx_At25xxx               eeprom;          /**< Ifx_At25xxx driver */
        Ifx_Efs                   efsConfig;       /**< Ifx_At25xxx file system: Configuration */
        Ifx_Efs                   efsBoard;        /**< Ifx_At25xxx file system: Production data */
    }driver;
}Db3x;
/* FIXME define a standard interface to the logic board to simplify the code */
boolean Db3x_init(Db3x *board, IfxQspi_SpiMaster *qspi, Ifx_At25xxx *eeprom);

boolean Db3x_shellSetup(Db3x *board, pchar args, void *data, IfxStdIf_DPipe *io);
boolean Db3x_AppPostInit_1edi2002as(Db3x *board);

void    Db3x_dumpDriverInfo(Db3x *driver, IfxStdIf_DPipe *io, boolean diff);
boolean Db3x_AppPreInitDriverEeprom(Db3x *board, IfxQspi_SpiMaster *qspi);
boolean Db3x_AppInit_1edi2002as(Db3x *board, IfxQspi_SpiMaster *qspi, LB_FileBoardVersion *logicBoardVersion);

boolean Db3x_AppSelfTest_oneEdi2001as(Db3x *board);
void    Db3x_printBoardConfiguration(Db3x *board, IfxStdIf_DPipe *io);
void    Db3x_setupDefaultValue(Db3x_FileConfiguration *configuration, LB_FileBoardVersion *boardVersion);
boolean Db3x_saveConfig(Db3x *board);
LB_FileBoardVersion* Db3x_getBoardVersion(Db3x *board);
Ifx_Efs*Db3x_getConfigEfs(Db3x *board);
Ifx_Efs*Db3x_getBoardVersionEfs(Db3x *board);
boolean Db3x_postInitialization(Db3x *board);
boolean Db3x_selfTest(Db3x *board);
void Db3x_disableDriver(Db3x *board);
void Db3x_enableDriver(Db3x *board);
uint32 Db3x_getFault(Db3x *board);
boolean Db3x_DbStdIfInit(AppDbStdIf *stdif, Db3x *board);

#endif /* DB3X_H_ */
