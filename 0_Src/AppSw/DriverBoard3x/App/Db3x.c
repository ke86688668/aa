/**
 * \file Db3x.c
 *
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 */
#include "Db3x.h"
#include "SysSe/Comm/Ifx_Shell.h"
#include "SysSe/Comm/Ifx_Console.h"
#include "DriverBoard3x/Config/DB3x_Configuration.h"
#include "Configuration.h"
#include "IfxPort_PinMap.h"
#include "string.h"
#include "Common/main.h"
#include "Common/AppSetupWizard.h"

boolean Db3x_createConfigFile(Db3x *board, boolean quiet);
boolean Db3x_loadConfigFile(Db3x *board);

boolean Db3x_init(Db3x *board, IfxQspi_SpiMaster *qspi, Ifx_At25xxx *eeprom)
{ // WARNING this function might be called multiple time depending on the board version
    boolean result = TRUE;
    board->boardVersion.boardType    = Lb_BoardType_undefined;
    board->boardVersion.boardVersion = LB_BoardVersion_undefined;
    board->boardVersion.fileVersion  = Lb_FileVersion_undefined;
    board->configuration.fileVersion = Lb_FileVersion_undefined;
    board->isBoardConfiguration      = FALSE;
    board->isBoardVersion            = FALSE;

    result                         &= Db3x_AppPreInitDriverEeprom(board, qspi);

    return result;
}


void Db3x_printReg(uint32 address, uint32 value)
{
//    Ifx_Console_print("0x%x ", value);

    /* FIXME auto generate this function with GcG */
    /* FIXME the below cast is wrong, Ifx1edi2002as_Address size might not be as big as the address in byte returned by '&', In the case of the MODULE_1EDI2002AS there is no issues*/
    if (address == ((uint32) & MODULE_1EDI2002AS.PSTAT) >> IFX1EDI2002AS_ADDRESS_SHIFT)
    {
//        Ifx_Console_print("[GPOFS:%d, SRDY:%d, ACT:%d, GPONS:%d, ERR:%d]",
//            ((value >> 2) & 0xF),
//            ((value >> 6) & 0x1),
//            ((value >> 7) & 0x1),
//            ((value >> 8) & 0x3),
//            ((value >> 11) & 0x1)
//            );
    }
    else if (address == ((uint32) & MODULE_1EDI2002AS.PSTAT2) >> IFX1EDI2002AS_ADDRESS_SHIFT)
    {
//        Ifx_Console_print("[ENVAL:%d, FLTA:%d, FLTB:%d, OPM:%d, HZ:%d, OT:%d, STP:%d, OSTC:%d]",
//            ((value >> 2) & 0x1),
//            ((value >> 3) & 0x1),
//            ((value >> 4) & 0x1),
//            ((value >> 5) & 0x7),
//            ((value >> 8) & 0x1),
//            ((value >> 9) & 0x1),
//            ((value >> 10) & 0x1),
//            ((value >> 11) & 0x1)
//            );
    }
    else if (address == ((uint32) & MODULE_1EDI2002AS.PER) >> IFX1EDI2002AS_ADDRESS_SHIFT)
    {
//        Ifx_Console_print("[CER1:%d, OSTER:%d, OTER:%d, OVLO3ER:%d, GER:%d, VMTO:%d, SPIER:%d, STPER:%d, ENER:%d, RST1:%d, RSTE1:%d]",
//            ((value >> 2) & 0x1),
//            ((value >> 3) & 0x1),
//            ((value >> 4) & 0x1),
//            ((value >> 5) & 0x1),
//            ((value >> 6) & 0x1),
//            ((value >> 7) & 0x1),
//            ((value >> 8) & 0x1),
//            ((value >> 9) & 0x1),
//            ((value >> 10) & 0x1),
//            ((value >> 11) & 0x1),
//            ((value >> 12) & 0x1)
//            );
    }
    else if (address == ((uint32) & MODULE_1EDI2002AS.SSTAT2) >> IFX1EDI2002AS_ADDRESS_SHIFT)
    {
//        Ifx_Console_print("[DSATC:%d, OSDL:%d, OCPC1:%d, OCPC2:%d, UVLO2M:%d, OVLO2M:%d, UVLO3M:%d, OVLO3M:%d, GC1:%d, GC2:%d, DACLPL:%d, DIO2L:%d]",
//            ((value >> 4) & 0x1),
//            ((value >> 5) & 0x1),
//            ((value >> 6) & 0x1),
//            ((value >> 7) & 0x1),
//            ((value >> 8) & 0x1),
//            ((value >> 9) & 0x1),
//            ((value >> 10) & 0x1),
//            ((value >> 11) & 0x1),
//            ((value >> 12) & 0x1),
//            ((value >> 13) & 0x1),
//            ((value >> 14) & 0x1),
//            ((value >> 15) & 0x1)
//            );
    }
    else if (address == ((uint32) & MODULE_1EDI2002AS.SSTAT) >> IFX1EDI2002AS_ADDRESS_SHIFT)
    {
//        Ifx_Console_print("[PWM:%d, FLTA:%d, FLTB:%d, OPM:%d, DBG:%d, OCPCD:%d, HZ:%d, OT:%d]",
//            ((value >> 4) & 0x1),
//            ((value >> 5) & 0x1),
//            ((value >> 6) & 0x1),
//            ((value >> 7) & 0x7),
//            ((value >> 10) & 0x1),
//            ((value >> 11) & 0x1),
//            ((value >> 12) & 0x1),
//            ((value >> 13) & 0x1)
//            );
    }
    else if (address == ((uint32) & MODULE_1EDI2002AS.SER) >> IFX1EDI2002AS_ADDRESS_SHIFT)
    {
//        Ifx_Console_print("[CER2:%d, OSTER:%d, OTER:%d, OVLO3ER:%d, GER:%d, VMTO:%d, UVLO3ER:%d, OVLO2ER:%d, UVLO2ER:%d, DESATER:%d, OCPER:%d, RST2:%d]",
//            ((value >> 4) & 0x1),
//            ((value >> 5) & 0x1),
//            ((value >> 6) & 0x1),
//            ((value >> 7) & 0x1),
//            ((value >> 8) & 0x1),
//            ((value >> 9) & 0x1),
//            ((value >> 10) & 0x1),
//            ((value >> 11) & 0x1),
//            ((value >> 12) & 0x1),
//            ((value >> 13) & 0x1),
//            ((value >> 14) & 0x1),
//            ((value >> 15) & 0x1)
//            );
    }
}


void Db3x_printRegDiff(uint32 address, uint32 value, uint32 lastValue, boolean diff)
{
    if (diff)
    {
        if (value != lastValue)
        {
//            Ifx_Console_print("    Reg address 0x%x:", address);
//            Db3x_printReg(address, lastValue);
//            Ifx_Console_print(" -> ");
//            Ifx_Console_print(ENDL);
        }
    }
    else
    {
//        Ifx_Console_print("    Reg address 0x%x:", address);
//        Db3x_printReg(address, value);
//        Ifx_Console_print(ENDL);
    }
}


void Db3x_dumpDriverInfo(Db3x *driver, IfxStdIf_DPipe *io, boolean diff)
{
#if IFX_CFG_1EDI2002AS_DRIVER_CACHE_ENABLE
    uint32                i;
    uint32                index;
    static Ifx_1EDI2002AS cache[6];

    Ifx_Console_print("IGBT driver info:"ENDL);

    Ifx1edi2002as_Driver_invalidateAllDevicesCache(&driver->driver.oneEdi2001as[0]);

    for (index = 0; index < driver->driver.oneEdi2001as[0].daisyChain.length; index++)
    {
        Ifx1edi2002as_Driver_readSync(&driver->driver.oneEdi2001as[index]);
        Ifx_Console_print(ENDL "    1EDI2002AS %d dump"ENDL, index);

        uint16 *data      = driver->driver.oneEdi2001as[index].cache.engine.buffer;
        uint16 *dataCache = (uint16 *)&cache[index];

        for (i = 0; i < driver->driver.oneEdi2001as[0].cache.engine.ItemCount; i++)
        {
            Db3x_printRegDiff(i, data[i], dataCache[i], diff);

            dataCache[i] = data[i];
        }
    }
#else
#pragma message  "WARNING: Db3x_dumpDriverInfo() disabled because 1EDI2002AS Cache feature is disabled"
//    Ifx_Console_print("WARNING: Db3x_dumpDriverInfo() disabled because 1EDI2002AS Cache feature is disabled"ENDL);
	(void)driver;
	(void)io;
	(void)diff;
#endif
}


void Db3x_printRegByAddress(Ifx1edi2002as_Driver *driver, pchar regName, Ifx1edi2002as_Address address)
{
    uint16 data; /* FIXME type for data should be defined by the driver / GcG*/
//    Ifx_Console_printAlign("1EDI2002AS %d.%s=", driver->daisyChain.index, regName);

    if (Ifx1edi2002as_Driver_readRegister(driver, address, &data))
    {
//        Db3x_printReg(address, data);
 //       Ifx_Console_print(ENDL);
    }
    else
    {
//        Ifx_Console_printAlign("Error reading device"ENDL);
    }
}


void Db3x_setupDefaultValue(Db3x_FileConfiguration *configuration, LB_FileBoardVersion *boardVersion)
{
    configuration->valid = TRUE;

    if ((boardVersion->boardType == Lb_BoardType_HybridKit_DriverBoardHp2) && (boardVersion->boardVersion == LB_BoardVersion_HybridKit_DriverBoardHp2_3_1))
    {
        configuration->driverSpiFrequency = CFG_DB3X_DRIVER_SPI_FREQUENCY;
        configuration->eepromSpiFrequency = 200e3;
    }
    else
    {
        configuration->driverSpiFrequency = 1.8e6; /* FIXME use #define default value */
        configuration->eepromSpiFrequency = 1.8e6;
    }

    if ((boardVersion->boardType == Lb_BoardType_HybridKit_DriverBoardHp2))
    {
        configuration->inverter.frequency                    = CFG_DBHP2_FREQUENCY;
        configuration->inverter.deadtime                     = CFG_DBHP2_DEAD_TIME;
        configuration->inverter.minPulse                     = CFG_DBHP2_MIN_PULSE;
        configuration->inverter.vdcNom                       = CFG_DBHP2_VDC_NOM;
        configuration->inverter.vdcMaxGen                    = CFG_DBHP2_VDC_MAX;
        configuration->inverter.vdcMin                       = CFG_DBHP2_VDC_MIN;
        configuration->inverter.igbtTempMin 				 = CFG_DBHP2_MIN_IGBT_TEMP;
        configuration->inverter.igbtTempMax 				 = CFG_DBHP2_MAX_IGBT_TEMP;
    }
    else // HP Drive
    {
        configuration->inverter.frequency                    = CFG_DBHPDRIVE_FREQUENCY;
        configuration->inverter.deadtime                     = CFG_DBHPDRIVE_DEAD_TIME;
        configuration->inverter.minPulse                     = CFG_DBHPDRIVE_MIN_PULSE;
        configuration->inverter.vdcNom                       = CFG_DBHPDRIVE_VDC_NOM;
        configuration->inverter.vdcMaxGen                    = CFG_DBHPDRIVE_VDC_MAX;
        configuration->inverter.vdcMin                       = CFG_DBHPDRIVE_VDC_MIN;
        configuration->inverter.igbtTempMin 				 = CFG_DBHPDRIVE_MIN_IGBT_TEMP;
        configuration->inverter.igbtTempMax 				 = CFG_DBHPDRIVE_MAX_IGBT_TEMP;
    }
    configuration->activeClamping.SCFG2_ACLPM 				 = CFG_DB3X_DRIVER_ACTIVE_CLAMPING_MODE;
    configuration->activeClamping.SACLT_AT 				 	 = CFG_DB3X_DRIVER_ACTIVE_CLAMPING_ACTIVATION_TIME;
    configuration->twoLevel.turnOnDelay = CFG_DB3X_DRIVER_TWO_LEVEL_TURN_ON_DELAY;
    configuration->twoLevel.turnOnLevel = CFG_DB3X_DRIVER_TWO_LEVEL_TURN_ON_LEVEL;
    configuration->twoLevel.turnOffDelay = CFG_DB3X_DRIVER_TWO_LEVEL_TURN_OFF_DELAY;
    configuration->twoLevel.turnOffLevel = CFG_DB3X_DRIVER_TWO_LEVEL_TURN_OFF_LEVEL;
    configuration->twoLevel.vbeCompensation = CFG_DB3X_DRIVER_TWO_LEVEL_VBE_COMPENSATION;
}


void Db3x_setupDefaultConfig(Db3x *board)
{
	Db3x_setupDefaultValue(&board->configuration, &board->boardVersion);
}


boolean Db3x_saveConfig(Db3x *board)
{
    return App_updateConfigFile(&board->driver.efsConfig, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION, &board->configuration, sizeof(board->configuration));
}



void Db3x_printBoardConfiguration(Db3x *board, IfxStdIf_DPipe *io)
{
    Db3x_FileConfiguration *configuration = &board->configuration;
//    IfxStdIf_DPipe_print(io, ENDL "Driver board information:"ENDL);

    if (board->isBoardVersion)
    {
//        App_printBoardVersion(&board->boardVersion, io);

		if (configuration->valid)
		{
			/* Print configuration file version */
//			IfxStdIf_DPipe_print(io, "- Configuration file version: %d.%d"ENDL, ((configuration->fileVersion >> 8) & 0xFF), ((configuration->fileVersion >> 0) & 0xFF));

//			IfxStdIf_DPipe_print(io, "- PWM: frequency=%f Hz, Deadtime=%f us, Min pulse=%f us"ENDL, configuration->inverter.frequency, (configuration->inverter.deadtime * 1e6), (configuration->inverter.minPulse * 1e6));
//			IfxStdIf_DPipe_print(io, "- Vdc              : Nom=%f V, Min=%f V, Max=%f V"ENDL, configuration->inverter.vdcNom, configuration->inverter.vdcMin, configuration->inverter.vdcMaxGen);
//			IfxStdIf_DPipe_print(io, "- IGBT Temperature limit: Min=%f �C, Max=%f �C"ENDL, configuration->inverter.igbtTempMin, configuration->inverter.igbtTempMax);

			switch (board->boardVersion.boardType)
			{
			case Lb_BoardType_HybridKit_DriverBoardHp2:
			case Lb_BoardType_HybridKit_DriverBoardHpDrive:
//				IfxStdIf_DPipe_print(io, "- Driver SPI frequency: %7.0f"ENDL, configuration->driverSpiFrequency);
//				IfxStdIf_DPipe_print(io, "- EEPROM SPI frequency: %7.0f"ENDL, configuration->eepromSpiFrequency);
				break;
			default:
				break;
			}

//			IfxStdIf_DPipe_print(io, "- Active clamping mode: %d"ENDL, configuration->activeClamping.SCFG2_ACLPM);
//			IfxStdIf_DPipe_print(io, "- Active clamping time: %d"ENDL, configuration->activeClamping.SACLT_AT);
//			IfxStdIf_DPipe_print(io, "- Gate turn on plateau level: %d"ENDL, configuration->twoLevel.turnOnLevel);
//			IfxStdIf_DPipe_print(io, "- Gate turn on delay %d"ENDL, configuration->twoLevel.turnOnDelay);
//			IfxStdIf_DPipe_print(io, "- Gate turn off plateau level %d"ENDL, configuration->twoLevel.turnOffLevel);
//			IfxStdIf_DPipe_print(io, "- Gate turn off delay %d"ENDL, configuration->twoLevel.turnOffDelay);
//			IfxStdIf_DPipe_print(io, "- VBE compensation %s"ENDL, configuration->twoLevel.vbeCompensation == Ifx1edi2002as_VbeCompensation_enabled ? "enabled" : "disabled");
		}
		else
		{
//			IfxStdIf_DPipe_print(io, "- Configuration invalid"ENDL);
		}
    }
    else
    {
//        IfxStdIf_DPipe_print(io, "- Board version invalid"ENDL);
    }
}


boolean Db3x_shellSetup(Db3x *board, pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean              result              = FALSE;

    if (Ifx_Shell_matchToken(&args, "spifrequency") != FALSE)
    {
        float32 frequency;

        if ((Ifx_Shell_parseFloat32(&args, &frequency) != FALSE))
        {
            board->configuration.driverSpiFrequency = frequency;
            board->configuration.eepromSpiFrequency = frequency;
            Db3x_saveConfig(board);
            result                                  = TRUE;
        }
    }


    else if (Ifx_Shell_matchToken(&args, "vdcnom") != FALSE)
    {
        float32 value;

        if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
        {
            board->configuration.inverter.vdcNom = value;
            Db3x_saveConfig(board);
            result                                  = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "vdcmax") != FALSE)
    {
        float32 value;

        if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
        {
            board->configuration.inverter.vdcMaxGen = value;
            Db3x_saveConfig(board);
            result                                     = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "vdcmin") != FALSE)
    {
        float32 value;

        if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
        {
            board->configuration.inverter.vdcMin = value;
            Db3x_saveConfig(board);
            result                                  = TRUE;
        }
    }
	else if (Ifx_Shell_matchToken(&args, "frequency") != FALSE)
	{
		float32 frequency;

		if ((Ifx_Shell_parseFloat32(&args, &frequency) != FALSE))
		{
			board->configuration.inverter.frequency = frequency;
            Db3x_saveConfig(board);
			result                                     = TRUE;
		}
	}
	else if (Ifx_Shell_matchToken(&args, "deadtime") != FALSE)
	{
		float32 value;

		if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
		{
			board->configuration.inverter.deadtime = value;
            Db3x_saveConfig(board);
			result                                    = TRUE;
		}
	}
	else if (Ifx_Shell_matchToken(&args, "minpulse") != FALSE)
	{
		float32 value;

		if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
		{
			board->configuration.inverter.minPulse = value;
            Db3x_saveConfig(board);
			result                                    = TRUE;
		}
	}
    else if (Ifx_Shell_matchToken(&args, "activeclamping") != FALSE)
    {
        uint32 mode;
        uint32 time;
        boolean error= FALSE;

        if ((Ifx_Shell_parseUInt32(&args, &mode, FALSE) != FALSE) && (Ifx_Shell_parseUInt32(&args, &time, FALSE) != FALSE))
        {
        	if (mode > 1)
        	{
        		error = TRUE;
//    			IfxStdIf_DPipe_print(io, "Mode of range [0,1]"ENDL);
        	}
        	if (time > 0xFF)
        	{
        		error = TRUE;
//    			IfxStdIf_DPipe_print(io, "Time of range [0,0xFF]"ENDL);
        	}
        	if(!error)
        	{
				board->configuration.activeClamping.SCFG2_ACLPM = (uint8)mode;
				board->configuration.activeClamping.SACLT_AT = (uint8)time;
				Db3x_saveConfig(board);
        	}
            result                                  = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "twolevelon") != FALSE)
    {
        uint32 level;
        uint32 delay;
        boolean error= FALSE;

        if ((Ifx_Shell_parseUInt32(&args, &level, FALSE) != FALSE) && (Ifx_Shell_parseUInt32(&args, &delay, FALSE) != FALSE))
        {
        	if (level > Ifx1edi2002as_GateTurnOnPlateauLevel_HardSwitching)
        	{
        		error = TRUE;
//    			IfxStdIf_DPipe_print(io, "Level out of range [0,%d]"ENDL, Ifx1edi2002as_GateTurnOnPlateauLevel_HardSwitching);
        	}
        	if (delay > 0xF)
        	{
        		error = TRUE;
//    			IfxStdIf_DPipe_print(io, "Delay out of range [0,%d]"ENDL, 0xF);
        	}
        	if(!error)
        	{
				board->configuration.twoLevel.turnOnLevel = (Ifx1edi2002as_GateTurnOnPlateauLevel)level;
				board->configuration.twoLevel.turnOnDelay = (uint16)delay;
				Db3x_saveConfig(board);
        	}
            result                                  = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "twoleveloff") != FALSE)
    {
        uint32 level;
        uint32 delay;
        boolean error= FALSE;

        if ((Ifx_Shell_parseUInt32(&args, &level, FALSE) != FALSE) && (Ifx_Shell_parseUInt32(&args, &delay, FALSE) != FALSE))
        {
        	if (level > Ifx1edi2002as_GateTurnOffPlateauLevel_15)
        	{
        		error = TRUE;
//    			IfxStdIf_DPipe_print(io, "Level out of range [0,%d]"ENDL, Ifx1edi2002as_GateTurnOffPlateauLevel_15);
        	}
        	if (delay > 0xFF)
        	{
        		error = TRUE;
//    			IfxStdIf_DPipe_print(io, "Delay out of range [0,%d]"ENDL, 0xFF);
        	}

        	if(!error)
        	{
                board->configuration.twoLevel.turnOffLevel = (Ifx1edi2002as_GateTurnOffPlateauLevel) level;
                board->configuration.twoLevel.turnOffDelay = (uint16)delay;
                Db3x_saveConfig(board);
        	}
            result                                  = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "twolevelvbecomp") != FALSE)
    {
        uint32 enabled;

        if (Ifx_Shell_parseUInt32(&args, &enabled, FALSE) != FALSE)
        {
            board->configuration.twoLevel.vbeCompensation = enabled ? Ifx1edi2002as_VbeCompensation_enabled : Ifx1edi2002as_VbeCompensation_disabled;
            Db3x_saveConfig(board);
            result                                  = TRUE;
        }
    }


    return result;
}


boolean Db3x_AppPreInitDriverEeprom(Db3x *board, IfxQspi_SpiMaster *qspi)
{
    boolean result = TRUE;

//    Ifx_Console_printAlign("Initializing Driver board eeprom:");

    /*Use low frequency as default to support all board type*/
    result &= App_initEeprom(&board->driver.eeprom, qspi, &board->driver.sscEeprom, &IfxQspi0_SLSO1_P20_9_OUT, 200e3);

    Ifx_Efs_Init(&board->driver.efsBoard, &board->driver.eeprom);
    result &= Ifx_Efs_tableOffsetSet(&board->driver.efsBoard, CFG_DB_WITH_EEPROM_BOARD_EFS_OFFSET);
    result &= Ifx_Efs_mount(&board->driver.efsBoard);

    Ifx_Efs_Init(&board->driver.efsConfig, &board->driver.eeprom);
    result &= Ifx_Efs_tableOffsetSet(&board->driver.efsConfig, CFG_DB_WITH_EEPROM_CONFIG_EFS_OFFSET);
    result &= Ifx_Efs_mount(&board->driver.efsConfig);

    board->isBoardVersion       = App_loadBoardVersionFile(&board->driver.efsBoard, &board->boardVersion);

//    App_printStatus(result);

    return result;
}


void DB3x_AppInit_print1edi2002asInitializationStatus(uint8 index, Ifx1edi2002as_Driver_Config *config)
{
//    Ifx_Console_printAlign("Device %d (", index);

    if (config->en.port != NULL_PTR)
    {
//        Ifx_Console_print("EN=P%d.%d, ", index, IfxPort_getIndex(config->en.port), config->en.pinIndex);
//        Ifx_Console_print("RST\\=P%d.%d, ", IfxPort_getIndex(config->rstN.port), config->rstN.pinIndex);
    }

//    Ifx_Console_print("FLTA\\=P%d.%d, ", IfxPort_getIndex(config->fltAN.port), config->fltAN.pinIndex);
//    Ifx_Console_print("FLTB\\=P%d.%d)"ENDL, IfxPort_getIndex(config->fltBN.port), config->fltBN.pinIndex);
}


boolean Db3x_AppInit_1edi2002as(Db3x *board, IfxQspi_SpiMaster *qspi, LB_FileBoardVersion *logicBoardVersion)
{
    boolean result = TRUE;
//    App_printStatusTitle("Initializing 1EDI2002AS");
//    Ifx_Console_print(" (");
    {   /* Initialze the serial interface */
        IfxQspi_SpiMaster_ChannelConfig spiChannelConfig;
        IfxQspi_SpiMaster_initChannelConfig(&spiChannelConfig, qspi);
        spiChannelConfig.base.baudrate             = board->configuration.driverSpiFrequency;
        spiChannelConfig.base.mode.enabled         = TRUE;
        spiChannelConfig.base.mode.autoCS          = 1;
        spiChannelConfig.base.mode.loopback        = FALSE;
        spiChannelConfig.base.mode.clockPolarity   = SpiIf_ClockPolarity_idleLow;
        spiChannelConfig.base.mode.shiftClock      = SpiIf_ShiftClock_shiftTransmitDataOnLeadingEdge;
        spiChannelConfig.base.mode.dataHeading     = SpiIf_DataHeading_msbFirst;
        spiChannelConfig.base.mode.dataWidth       = 16;
        spiChannelConfig.base.mode.csActiveLevel   = Ifx_ActiveState_low;
        spiChannelConfig.base.mode.csLeadDelay     = SpiIf_SlsoTiming_7;
        spiChannelConfig.base.mode.csTrailDelay    = SpiIf_SlsoTiming_7;
        spiChannelConfig.base.mode.csInactiveDelay = SpiIf_SlsoTiming_7; /* FIXME current QSPI driver 1.0.0.3 will produce wrong timing MCMETILLD-619		*/
        spiChannelConfig.base.mode.parityCheck     = FALSE;              /* FIXME use HW parity */
        spiChannelConfig.base.mode.parityMode      = Ifx_ParityMode_even;
        spiChannelConfig.base.errorChecks.baudrate = FALSE;
        spiChannelConfig.base.errorChecks.phase    = FALSE;
        spiChannelConfig.base.errorChecks.receive  = FALSE;
        spiChannelConfig.base.errorChecks.transmit = FALSE;

        if (logicBoardVersion->boardVersion >= LB_BoardVersion_HybridKit_LogicBoard_3_3)
        {
            spiChannelConfig.sls.output.pin            = &IfxQspi0_SLSO6_P20_10_OUT;
        }
        else
        {
            spiChannelConfig.sls.output.pin            = &IfxQspi0_SLSO0_P20_8_OUT;
        }

        spiChannelConfig.sls.output.mode           = IfxPort_OutputMode_pushPull;
        spiChannelConfig.sls.output.driver         = IfxPort_PadDriver_cmosAutomotiveSpeed1;

        result                                    &= IfxQspi_SpiMaster_initChannel(&board->driver.sscOneEdi2001as, &spiChannelConfig) == SpiIf_Status_ok;

//        Ifx_Console_print("QSPI%d, CS%d, %9.0f baud)"ENDL, IfxQspi_getIndex(qspi->qspi), spiChannelConfig.sls.output.pin->slsoNr, spiChannelConfig.base.baudrate);
    }

    {
        Ifx1edi2002as_Driver_Config configTop;
        Ifx1edi2002as_Driver_Config configBottom;
        Ifx1edi2002as_initConfig(&configTop);
        configTop.channel = &board->driver.sscOneEdi2001as.base;
        configTop.en      = (IfxPort_Pin) {&MODULE_P01, 6};

        if (logicBoardVersion->boardVersion >= LB_BoardVersion_HybridKit_LogicBoard_3_3)
        {
            configTop.rstN = (IfxPort_Pin) {&MODULE_P22, 7};
        }
        else
        {   /* Missing dedicated signal connection, on logic board, use same as bottom devices */
            configTop.rstN = (IfxPort_Pin) {&MODULE_P01, 4};
        }

        configTop.fltAN          = (IfxPort_Pin) {&MODULE_P15, 0};
        configTop.fltBN          = (IfxPort_Pin) {&MODULE_P15, 1};
        configBottom             = configTop;
        configBottom.en          = (IfxPort_Pin) {NULL_PTR, 0};
        configBottom.rstN        = (IfxPort_Pin) {&MODULE_P01, 4};
        configBottom.fltAN       = (IfxPort_Pin) {&MODULE_P01, 3};
        configBottom.fltBN       = (IfxPort_Pin) {&MODULE_P01, 5};

        /* FIXME fix driver assignment as defined by the daisy chain HW   */
        configTop.previousDevice = NULL_PTR;
        Ifx1edi2002as_init(&board->driver.oneEdi2001as[0], &configTop);/* U Top*/
//        DB3x_AppInit_print1edi2002asInitializationStatus(0, &configTop);

        configBottom.previousDevice = &board->driver.oneEdi2001as[0];
        Ifx1edi2002as_init(&board->driver.oneEdi2001as[1], &configBottom);
//        DB3x_AppInit_print1edi2002asInitializationStatus(1, &configBottom);/* U Bottom*/

        configTop.en             = (IfxPort_Pin) {NULL_PTR, 0};
        configTop.rstN           = (IfxPort_Pin) {NULL_PTR, 0};
        configBottom.rstN        = (IfxPort_Pin) {NULL_PTR, 0};

        configTop.previousDevice = &board->driver.oneEdi2001as[1];
        Ifx1edi2002as_init(&board->driver.oneEdi2001as[2], &configTop);
//        DB3x_AppInit_print1edi2002asInitializationStatus(2, &configTop);/* V Top*/

        configBottom.previousDevice = &board->driver.oneEdi2001as[2];
        Ifx1edi2002as_init(&board->driver.oneEdi2001as[3], &configBottom);
//        DB3x_AppInit_print1edi2002asInitializationStatus(3, &configBottom);/* V Bottom*/

        configTop.previousDevice = &board->driver.oneEdi2001as[3];
        Ifx1edi2002as_init(&board->driver.oneEdi2001as[4], &configTop);
//        DB3x_AppInit_print1edi2002asInitializationStatus(4, &configTop);/* W Top*/

        configBottom.previousDevice = &board->driver.oneEdi2001as[4];
        Ifx1edi2002as_init(&board->driver.oneEdi2001as[5], &configBottom);
//        DB3x_AppInit_print1edi2002asInitializationStatus(5, &configBottom);/* W Bottom*/
    }

//    App_printStatus(result);
    return result;
}


boolean Db3x_AppPostInit_1edi2002as(Db3x *board)
{
    boolean result = TRUE;

//    App_printStatusTitle("Post initialization of 1EDI2002AS");

    Ifx1edi2002as_Driver_reset(&board->driver.oneEdi2001as[0], 0x3F);

    wait(TimeConst_1ms);

    if (Ifx1edi2002as_Driver_isAnyDeviceNotReady(&board->driver.oneEdi2001as[0]) == 0)
    {
        Ifx1edi2002as_Driver_invalidateAllDevicesCache(&board->driver.oneEdi2001as[0]);
//      Ifx1edi2002as_Driver_readSync(&board->driver.oneEdi2001as[0]);
        uint8                       index;
        Ifx1edi2002as_OperationMode mode;

        for (index = 0; index < board->driver.oneEdi2001as[0].daisyChain.length; index++)
        {
            uint16 primaryId;
            uint16 secondaryId;
            result &= Ifx1edi2002as_getPrimaryChipIdentification(&board->driver.oneEdi2001as[0], &primaryId);
            result &= Ifx1edi2002as_getSecondaryChipIdentification(&board->driver.oneEdi2001as[0], &secondaryId);
//            Ifx_Console_printAlign("Device %d: primary ID=", index);

            switch (primaryId)
            {
            case 0x100:
//                Ifx_Console_print("A12 Step");
                break;
            case 0x4A1:
//                Ifx_Console_print("A21 or A22 step");
                break;
            case 0x4A2:
//                Ifx_Console_print("A31 step");
                break;
            case 0x4A3:
//                Ifx_Console_print("AD step");
                break;
            default:
//                Ifx_Console_print("Unknown step");
                break;
            }

//            Ifx_Console_printAlign("; secondary ID=", index);

            switch (secondaryId)
            {
            case 0x200:
//                Ifx_Console_print("A12 Step");
                break;
            case 0x8A1:
//                Ifx_Console_print("A21 step");
                break;
            case 0x8A2:
//                Ifx_Console_print("A22 step");
                break;
            case 0x8B1:
//                Ifx_Console_print("A31 step");
                break;
            case 0x8B2:
//                Ifx_Console_print("AD step");
                break;
            default:
//                Ifx_Console_print("Unknown step: %x", secondaryId);
                break;
            }

            Ifx_Console_printAlign(ENDL, index);
        }

        for (index = 0; index < board->driver.oneEdi2001as[0].daisyChain.length; index++)
        {
            boolean ready;
            result &= Ifx1edi2002as_isSecondaryReady(&board->driver.oneEdi2001as[index], &ready);

            if (!ready)
            {
//                Ifx_Console_printAlign("ERROR: Device %d PSTAT.SRDY set. Secondary not ready"ENDL, index);
                result = FALSE;
            }

            result &= Ifx1edi2002as_getOperationMode(&board->driver.oneEdi2001as[index], &mode);

            if (mode != Ifx1edi2002as_OperationMode_0)
            {
//                Ifx_Console_printAlign("ERROR: Device %d invalid operation mode: %d, 0 expected"ENDL, index, mode);
                result = FALSE;
            }

            // Clear PCTRL.CLRP and CLRS
            result &= Ifx1edi2002as_clearPrimaryRequestBit(&board->driver.oneEdi2001as[index]);
            result &= Ifx1edi2002as_clearSecondaryRequestBit(&board->driver.oneEdi2001as[index]);
        }

        result &= Ifx1edi2002as_Driver_writeSyncAllDevices(&board->driver.oneEdi2001as[0]);

        if (result)
        {
            result &= Ifx1edi2002as_Driver_enterConfigurationMode(&board->driver.oneEdi2001as[0], 0x3F);

            if (result)
            {
                /* FIXME write custom config. add additional parameters */
                /* Configure the device */
                // Enable DIO1 and DIO2 for temperature measurement,and ODS/ASC feedback
                for (index = 0; index < board->driver.oneEdi2001as[0].daisyChain.length; index++)
                {
                    result &= Ifx1edi2002as_getOperationMode(&board->driver.oneEdi2001as[index], &mode);

                    if (mode != Ifx1edi2002as_OperationMode_2)
                    {
//                        Ifx_Console_printAlign("ERROR: Device %d invalid operation mode: %d, 2 expected"ENDL, index, mode);
                        result = FALSE;
                    }

                    result &= Ifx1edi2002as_enableAdvancedPrimaryConfiguration(&board->driver.oneEdi2001as[index]);
                    result &= Ifx1edi2002as_enableAdvancedSecondaryConfiguration(&board->driver.oneEdi2001as[index]);
                }

                result &= Ifx1edi2002as_Driver_writeSyncAllDevices(&board->driver.oneEdi2001as[0]);

                for (index = 0; index < board->driver.oneEdi2001as[0].daisyChain.length; index++)
                {
                    result &= Ifx1edi2002as_setDio1Direction(&board->driver.oneEdi2001as[index], Ifx1edi2002as_Dio1Direction_output);
                    result &= Ifx1edi2002as_setDio2Direction(&board->driver.oneEdi2001as[index], Ifx1edi2002as_Dio2Direction_input);
                    result &= Ifx1edi2002as_enableDout(&board->driver.oneEdi2001as[index]);
                    result &= Ifx1edi2002as_enableIgbtStateMonitoring(&board->driver.oneEdi2001as[index]);
                    result &= Ifx1edi2002as_disableDesatClamping(&board->driver.oneEdi2001as[index]);

                    /* Active clamping */
                    result &= Ifx1edi2002as_setActiveClampingMode(&board->driver.oneEdi2001as[index], board->configuration.activeClamping.SCFG2_ACLPM);
                    result &= Ifx1edi2002as_setActiveClampingTime(&board->driver.oneEdi2001as[index], board->configuration.activeClamping.SACLT_AT);
                    /* Two level trun on / off configuration */
                    result &= Ifx1edi2002as_setTwoLevelTurnOffLevel(&board->driver.oneEdi2001as[index], board->configuration.twoLevel.turnOffLevel);
                    result &= Ifx1edi2002as_setTwoLevelTurnOffDelay(&board->driver.oneEdi2001as[index], board->configuration.twoLevel.turnOffDelay);
                    result &= Ifx1edi2002as_setTwoLevelTurnOnLevel(&board->driver.oneEdi2001as[index], board->configuration.twoLevel.turnOnLevel);
                    result &= Ifx1edi2002as_setTwoLevelTurnOnDelay(&board->driver.oneEdi2001as[index], board->configuration.twoLevel.turnOnDelay);
                    result &= Ifx1edi2002as_setVbeCompensation(&board->driver.oneEdi2001as[index], board->configuration.twoLevel.vbeCompensation);
                }

                result &= Ifx1edi2002as_Driver_writeSyncAllDevices(&board->driver.oneEdi2001as[0]);

                for (index = 0; index < board->driver.oneEdi2001as[0].daisyChain.length; index++)
                {
                    result &= Ifx1edi2002as_disableAdvancedPrimaryConfiguration(&board->driver.oneEdi2001as[index]);
                    result &= Ifx1edi2002as_disableAdvancedSecondaryConfiguration(&board->driver.oneEdi2001as[index]);
                }

                result &= Ifx1edi2002as_Driver_writeSyncAllDevices(&board->driver.oneEdi2001as[0]);

                /* Exit Configuration mode */
                result &= Ifx1edi2002as_Driver_exitConfigurationMode(&board->driver.oneEdi2001as[0], 0x3F);

                if (result)
                {
                    for (index = 0; index < board->driver.oneEdi2001as[0].daisyChain.length; index++)
                    {
                        result &= Ifx1edi2002as_getOperationMode(&board->driver.oneEdi2001as[index], &mode);

                        if (mode != Ifx1edi2002as_OperationMode_3)
                        {
//                            Ifx_Console_printAlign("ERROR: Device %d invalid operation mode: %d, 3 expected"ENDL, index, mode);
                            result = FALSE;
                        }
                    }

                    if (result)
                    {
                        /* Enable the driver*/
                        Ifx1edi2002as_Driver_enable(&board->driver.oneEdi2001as[0], 0x3F);
                    }
                }
                else
                {
//                    Ifx_Console_printAlign("ERROR: Can't exit configuration mode"ENDL, index);
                }
            }
            else
            {
//                Ifx_Console_printAlign("ERROR: Can't enter configuration mode"ENDL, index);
            }
        }
        else
        {
//            Ifx_Console_printAlign("ERROR: Can't write config to device"ENDL, index);
        }

        if (!result)
        {
            uint32 index;
            Ifx1edi2002as_Driver_invalidateAllDevicesCache(&board->driver.oneEdi2001as[0]);
//            Ifx_Console_printAlign("Additional driver information:"ENDL);

            for (index = 0; index < board->driver.oneEdi2001as[0].daisyChain.length; index++)
            {
//                Db3x_printRegByAddress(&board->driver.oneEdi2001as[index], "PER", ((uint32) & MODULE_1EDI2002AS.PER) >> IFX1EDI2002AS_ADDRESS_SHIFT);
//                Db3x_printRegByAddress(&board->driver.oneEdi2001as[index], "PSTAT", ((uint32) & MODULE_1EDI2002AS.PSTAT) >> IFX1EDI2002AS_ADDRESS_SHIFT);
//                Db3x_printRegByAddress(&board->driver.oneEdi2001as[index], "PSTAT2", ((uint32) & MODULE_1EDI2002AS.PSTAT2) >> IFX1EDI2002AS_ADDRESS_SHIFT);
//                Db3x_printRegByAddress(&board->driver.oneEdi2001as[index], "SER", ((uint32) & MODULE_1EDI2002AS.SER) >> IFX1EDI2002AS_ADDRESS_SHIFT);
//                Db3x_printRegByAddress(&board->driver.oneEdi2001as[index], "SSTAT", ((uint32) & MODULE_1EDI2002AS.SSTAT) >> IFX1EDI2002AS_ADDRESS_SHIFT);
//                Db3x_printRegByAddress(&board->driver.oneEdi2001as[index], "SSTAT2", ((uint32) & MODULE_1EDI2002AS.SSTAT2) >> IFX1EDI2002AS_ADDRESS_SHIFT);
//                Ifx_Console_print(ENDL);
            }
        }
    }
    else
    {
//        Ifx_Console_printAlign("ERROR: Device not ready");
        result = FALSE;
    }

//    App_printStatus(result);
    return result;
}


boolean Db3x_AppSelfTest_oneEdi2001as(Db3x *board)
{
    boolean success = TRUE;
    uint8   i;

    for (i = 0; i < board->driver.oneEdi2001as[0].daisyChain.length; i++)
    {
        boolean status = TRUE;
        uint16  prwOrg;
        uint16  prw;
//        App_printStatusTitle("Checking 1EDI2002AS %d", i);

        /* Invalidate the cache
         * Backup the original PWR value
         * Write PRW register with a dummy value and read the value back
         * Original value is written after the test
         */

        Ifx1edi2002as_Driver_invalidateAllDevicesCache(&board->driver.oneEdi2001as[i]);

        /* Backup the register value */
        status &= Ifx1edi2002as_getPrimariReadWriteRegister(&board->driver.oneEdi2001as[i], &prwOrg);

        /* Write a dummy value */
        prw     = 0x2DEB;
        status &= Ifx1edi2002as_setPrimariReadWriteRegister(&board->driver.oneEdi2001as[i], prw);
        status &= Ifx1edi2002as_Driver_writeSyncAllDevices(&board->driver.oneEdi2001as[i]);

        /* Read back the dummy value */
        Ifx1edi2002as_Driver_invalidateAllDevicesCache(&board->driver.oneEdi2001as[i]);
        status &= Ifx1edi2002as_getPrimariReadWriteRegister(&board->driver.oneEdi2001as[i], &prw);

        /* Write back the original value */
        status &= Ifx1edi2002as_setPrimariReadWriteRegister(&board->driver.oneEdi2001as[i], prwOrg);
        status &= Ifx1edi2002as_Driver_writeSyncAllDevices(&board->driver.oneEdi2001as[i]);

        if (!status)
        {
//            Ifx_Console_printAlign("Frame check: failed"ENDL);
        }
        else if (prw != 0x2DEB)
        {
//            Ifx_Console_printAlign("Register read / write: failed"ENDL);
            status = FALSE;
        }
        else
        {
//            Ifx_Console_printAlign("Register read / write: passed"ENDL);
        }

//        App_printStatus(status);
        success &= status;
    }

    return success;
}



LB_FileBoardVersion* Db3x_getBoardVersion(Db3x *board)
{
	return &board->boardVersion;
}

AppDbStdIf_MainConfig* Db3x_getMainConfig(Db3x *board)
{
	return &board->configuration.inverter;
}

Ifx_Efs*Db3x_getConfigEfs(Db3x *board)
{
	return &board->driver.efsConfig;
}

Ifx_Efs*Db3x_getBoardVersionEfs(Db3x *board)
{
	return &board->driver.efsBoard;
}

boolean Db3x_postInitialization(Db3x *board)
{
	boolean result = TRUE;
    result                              &= Db3x_AppPostInit_1edi2002as(board);

    return result;
}

boolean Db3x_selfTest(Db3x *board)
{
	boolean result = TRUE;
	result &= Db3x_AppSelfTest_oneEdi2001as(board);
    return result;
}

void Db3x_disableDriver(Db3x *board)
{
    Ifx1edi2002as_Driver_disable(board->driver.oneEdi2001as, 0x3F);
}

void Db3x_enableDriver(Db3x *board)
{
    Ifx1edi2002as_Driver_enable(board->driver.oneEdi2001as, 0x3F);
}

uint32 Db3x_getFault(Db3x *board)
{/* FIXME make sure the fault are returned as per AppDbStdIf_GetFault */
    return Ifx1edi2002as_Driver_isAnyDeviceFault(&board->driver.oneEdi2001as[0]);
}



boolean Db3x_clearFaults(Db3x *board)
{/* Only DESAT error is handled, if other faults are presents, no action is taken */


	uint32 i;
    boolean              status = TRUE;
    Ifx_1EDI2002AS_SER ser;
    Ifx_1EDI2002AS_PER per;
    boolean errorPresent = FALSE;

    Ifx1edi2002as_Driver_invalidateAllDevicesCacheVolatile(&board->driver.oneEdi2001as[0]);
    for (i=0;i<6;i++)
    {
        status &= Ifx1edi2002as_Driver_readRegister(&board->driver.oneEdi2001as[i], ((uint32) & MODULE_1EDI2002AS.SER) >> IFX1EDI2002AS_ADDRESS_SHIFT, &ser.U);
        status &= Ifx1edi2002as_Driver_readRegister(&board->driver.oneEdi2001as[i], ((uint32) & MODULE_1EDI2002AS.PER) >> IFX1EDI2002AS_ADDRESS_SHIFT, &per.U);
        if (status && (ser.B.DESATER == 1))
        {

        	/* Check that only DESAT is set */
        	if ((ser.B.RST2 == 0)
        	 && (ser.B.OCPER == 0)
        	 && (ser.B.UVLO2ER == 0)
        	 && (ser.B.OVLO2ER == 0)
        	 && (ser.B.UVLO3ER == 0)
        	 && (ser.B.VMTO == 0)
        	 && (ser.B.GER == 0)
        	 && (ser.B.OVLO3ER == 0)
        	 && (ser.B.OTER == 0)
        	 && (ser.B.OSTER == 0)
        	 && (ser.B.CER2 == 0)

        	 && (per.B.RSTE1 == 0)
        	 && (per.B.RST1 == 0)
        	 && (per.B.ENER == 0)
        	 && (per.B.STPER == 0)
        	 && (per.B.SPIER == 0)
        	 && (per.B.VMTO == 0)
        	 && (per.B.GER == 0)
        	 && (per.B.OVLO3ER == 0)
        	 && (per.B.OTER == 0)
        	 && (per.B.OSTER == 0)
        	 && (per.B.CER1 == 0)
        			)
        	{
        		status &= Ifx1edi2002as_clearPrimaryRequestBit(&board->driver.oneEdi2001as[i]); /* De-assert NFLTA  */
        		status &= Ifx1edi2002as_clearSecondaryRequestBit(&board->driver.oneEdi2001as[i]); /* CLEAR DESATER flag */
        	}
        	else
        	{
        		errorPresent = TRUE;
        	}
        }
    }

    status &= Ifx1edi2002as_Driver_writeSyncAllDevices(&board->driver.oneEdi2001as[0]);

    return status && !errorPresent;
}
boolean Db3x_isBoardVersionFile(Db3x *board)
{
	return board->isBoardVersion;
}

boolean Db3x_isConfigFile(Db3x *board)
{
	return board->isBoardConfiguration;
}

boolean Db3x_createBoardVersionEfs(Db3x *board, boolean quiet)
{
	boolean result = TRUE;
	result &= unprotectEeprom(&board->driver.eeprom);
    result &= App_createEfs(&board->driver.efsBoard, CFG_DB_WITH_EEPROM_BOARD_EFS_OFFSET, CFG_DB_WITH_EEPROM_BOARD_EFS_PARTITION_SIZE, CFG_DB_BOARD_EFS_FILE_SIZE, quiet);
	result &= protectEeprom(&board->driver.eeprom);

    return result;
}

boolean Db3x_createBoardVersionFile(Db3x *board, boolean quiet)
{
	boolean result = TRUE;
	result &= unprotectEeprom(&board->driver.eeprom);
    board->isBoardVersion = App_createBoardVersionFile(&board->driver.efsBoard, &board->boardVersion, Lb_BoardType_undefined, LB_BoardVersion_undefined, quiet);
	result &= protectEeprom(&board->driver.eeprom);
    return board->isBoardVersion && result;
}

boolean Db3x_createConfigEfs(Db3x *board, boolean quiet)
{
    return App_createEfs(&board->driver.efsConfig, CFG_DB_WITH_EEPROM_CONFIG_EFS_OFFSET, CFG_DB_WITH_EEPROM_CONFIG_EFS_PARTITION_SIZE, CFG_DB_CONFIG_EFS_FILE_SIZE, quiet);
}

boolean Db3x_createConfigFile(Db3x *board, boolean quiet)
{
    Db3x_FileConfiguration *configuration = &board->configuration;
    Db3x_setupDefaultValue(&board->configuration, &board->boardVersion);
    board->isBoardConfiguration = App_createConfigFile(&board->driver.efsConfig, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION, configuration, sizeof(*configuration), EFS_CFG_FILE_VERSION_DRIVER_BOARD_3X_CONFIGURATION, quiet);
    return board->isBoardConfiguration;
}


boolean Db3x_loadBoardVersionFile(Db3x *board)
{
    board->isBoardVersion       = App_loadBoardVersionFile(&board->driver.efsBoard, &board->boardVersion);
    return board->isBoardVersion;
}

boolean Db3x_loadConfigFile(Db3x *board)
{
	board->isBoardConfiguration = App_loadConfigFile(&board->driver.efsConfig, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION, &board->configuration, sizeof(board->configuration), EFS_CFG_FILE_VERSION_DRIVER_BOARD_3X_CONFIGURATION);

    if (board->isBoardConfiguration)
    {
        IfxQspi_SpiMaster_setChannelBaudrate(&board->driver.sscEeprom, board->configuration.eepromSpiFrequency);
    }

    board->configuration.valid  = board->isBoardConfiguration;
    return board->isBoardConfiguration;
}

boolean Db3x_saveBoardVersion(Db3x *board)
{
	boolean result = TRUE;
	result &= unprotectEeprom(&board->driver.eeprom);
	result &= App_updateBoardVersionFile(&board->driver.efsBoard, &board->boardVersion);
	result &= protectEeprom(&board->driver.eeprom);
	return result;
}

boolean Db3x_isBoardTypeSupported(Db3x *board)
{
	return (board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHp2)
    		|| (board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDrive);
}

boolean Db3x_isBoardVersionSupported(Db3x *board)
{
	return  ((board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHp2)
    		&& ((board->boardVersion.boardVersion == LB_BoardVersion_HybridKit_DriverBoardHp2_3_1)
    				|| (board->boardVersion.boardVersion == LB_BoardVersion_HybridKit_DriverBoardHp2_3_2)))
    ||
    ((board->boardVersion.boardType == Lb_BoardType_HybridKit_DriverBoardHpDrive)
    		&& ((board->boardVersion.boardVersion == LB_BoardVersion_HybridKit_DriverBoardHpDrive_1_3)));
}

boolean Db3x_isConfigFileVersionActual(Db3x *board)
{
	return board->configuration.fileVersion == EFS_CFG_FILE_VERSION_DRIVER_BOARD_3X_CONFIGURATION;
}


char Db3x_setupWizard(Db3x *board, Ifx_Shell *shell)
{
	Db3x_FileConfiguration defaultConfig;
	Db3x_setupDefaultValue(&defaultConfig, &board->boardVersion);

    /*************************************************************************/
    /* Inverter PWM frequency in Hz */
	if (AppSetupWizard_askFloat32(shell, "Inverter PWM frequency in Hz", defaultConfig.inverter.frequency, 0, 0, &board->configuration.inverter.frequency) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    /*************************************************************************/
    /* Inverter PWM deadtime in s */
	if (AppSetupWizard_askFloat32(shell, "Inverter PWM deadtime in s", defaultConfig.inverter.deadtime, 0, 0, &board->configuration.inverter.deadtime) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    /*************************************************************************/
    /* Inverter PWM min-pulse in s */
	if (AppSetupWizard_askFloat32(shell, "Inverter PWM min-pulse in s", defaultConfig.inverter.minPulse, 0, 0, &board->configuration.inverter.minPulse) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    /*************************************************************************/
    /* Nominal DC-link voltage in V */
	if (AppSetupWizard_askFloat32(shell, "Nominal DC-link voltage in V", defaultConfig.inverter.vdcNom, 0, 0, &board->configuration.inverter.vdcNom) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    /*************************************************************************/
    /* Maximal allowed DC-link voltage in V */
	if (AppSetupWizard_askFloat32(shell, "Maximal allowed DC-link voltage in V", defaultConfig.inverter.vdcMaxGen, 0, 0, &board->configuration.inverter.vdcMaxGen) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}

	if (AppSetupWizard_askUint8(shell, "Active clamping mode (SCFG2.ACPLM)", defaultConfig.activeClamping.SCFG2_ACLPM, 0, 0x1, &board->configuration.activeClamping.SCFG2_ACLPM) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
	if (AppSetupWizard_askUint8(shell, "Active clamping time (SACLT.AT)", defaultConfig.activeClamping.SACLT_AT, 0, 0xFF, &board->configuration.activeClamping.SACLT_AT) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
	if (AppSetupWizard_askUint8(shell, "Two-level turn off delay (SRTTOF.RTVAL)", defaultConfig.twoLevel.turnOffDelay, 0, 0xFF, &board->configuration.twoLevel.turnOffDelay) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    {
    	pchar answers[17] = {"vgpof0", "vgpof1", "vgpof2", "vgpof3", "vgpof4", "vgpof5", "vgpof6", "vgpof7", "vgpof8", "vgpof9",
    			"vgpof10", "vgpof11", "vgpof12", "vgpof13", "vgpof14", "vgpof15"
    			, ""};
    	sint32 option;
    	sint32 defaultOption;
        option = board->configuration.twoLevel.turnOffLevel;
        defaultOption = defaultConfig.twoLevel.turnOffLevel;
    	option = AppSetupWizard_askOptions(shell, "Two-level turn off level (PCTRL2.GPOF)", defaultOption, option, answers);
    	if (option == -1)
    	{
    		return IFX_SHELL_KEY_CTRL_C;
    	}
    	else if (option == 0)
        {
    		board->configuration.twoLevel.turnOffLevel = option;
        }
    }
	if (AppSetupWizard_askUint8(shell, "Two-level turn on delay (SCFG2.TTOND)", defaultConfig.twoLevel.turnOnDelay, 0, 0xF, &board->configuration.twoLevel.turnOnDelay) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    {
    	pchar answers[8] = {"vgpon0", "vgpon1", "vgpon2", "hard", ""};
    	sint32 option;
    	sint32 defaultOption;
        option = board->configuration.twoLevel.turnOnLevel;
        defaultOption = defaultConfig.twoLevel.turnOnLevel;
    	option = AppSetupWizard_askOptions(shell, "Two-level turn on level (PCTRL.GPON)", defaultOption, option, answers);
    	if (option == -1)
    	{
    		return IFX_SHELL_KEY_CTRL_C;
    	}
    	else if (option == 0)
        {
    		board->configuration.twoLevel.turnOnLevel = option;
        }
    }

    {
    	pchar answers[9] = {"disabled", "enabled", ""};
    	sint32 option;
    	sint32 defaultOption;
        option = board->configuration.twoLevel.vbeCompensation;
        defaultOption = defaultConfig.twoLevel.vbeCompensation;
    	option = AppSetupWizard_askOptions(shell, "Two-level Vbe compensation (SCFG.VBEC)", defaultOption, option, answers);
    	if (option == -1)
    	{
    		return IFX_SHELL_KEY_CTRL_C;
    	}
    	else if (option == 0)
        {
    		board->configuration.twoLevel.vbeCompensation = option;
        }
    }


	return IFX_SHELL_KEY_NULL;
}

boolean Db3x_DbStdIfInit(AppDbStdIf *stdif, Db3x *board)
{
    memset(stdif, 0, sizeof(*stdif));

    /* Set the API link */
    stdif->driver                        = board;

    /* *INDENT-OFF* */
    /* FIXME check API signature */
    stdif->disableDriver                 = (AppDbStdIf_DisableDriver            )&Db3x_disableDriver;
    stdif->dumpDriverInfo                = (AppDbStdIf_DumpDriverInfo           )&Db3x_dumpDriverInfo;
    stdif->enableDriver                  = (AppDbStdIf_EnableDriver             )&Db3x_enableDriver;
    stdif->getBoardVersion               = (AppDbStdIf_GetBoardVersion          )&Db3x_getBoardVersion;
    stdif->getmainConfig                 = (AppDbStdIf_GetMainConfig            )&Db3x_getMainConfig;
    stdif->getConfigEfs                  = (AppDbStdIf_GetConfigEfs             )&Db3x_getConfigEfs;
    stdif->getFault                      = (AppDbStdIf_GetFault                 )&Db3x_getFault;
    stdif->getVdc                        = (AppDbStdIf_GetVdc                   )NULL_PTR;
    stdif->getIgbtTemp                   = (AppDbStdIf_GetIgbtTemp              )NULL_PTR;
    stdif->getBoardVersionEfs            = (AppDbStdIf_GetBoardVersionEfs       )&Db3x_getBoardVersionEfs;
    stdif->init                          = (AppDbStdIf_Init                     )&Db3x_init;
    stdif->initDriver                    = (AppDbStdIf_InitDriver               )&Db3x_AppInit_1edi2002as;
    stdif->postInitialization            = (AppDbStdIf_PostInitialization       )&Db3x_postInitialization;
    stdif->printBoardConfiguration       = (AppDbStdIf_PrintBoardConfiguration  )&Db3x_printBoardConfiguration;
    stdif->saveConfig                    = (AppDbStdIf_SaveConfig               )&Db3x_saveConfig;
    stdif->selfTest                      = (AppDbStdIf_SelfTest                 )&Db3x_selfTest;
    stdif->setDefaultConfig              = (AppDbStdIf_SetDefaultConfig         )&Db3x_setupDefaultConfig;
    stdif->shellSetup                    = (AppDbStdIf_ShellSetup               )&Db3x_shellSetup;

    stdif->createBoardVersionEfs         = (AppDbStdIf_CreateBoardVersionEfs    )&Db3x_createBoardVersionEfs    ;
    stdif->createBoardVersionFile        = (AppDbStdIf_CreateBoardVersionFile   )&Db3x_createBoardVersionFile   ;
    stdif->createConfigEfs               = (AppDbStdIf_CreateConfigEfs          )&Db3x_createConfigEfs          ;
    stdif->createConfigFile              = (AppDbStdIf_CreateConfigFile         )&Db3x_createConfigFile         ;
    stdif->isBoardVersionFile            = (AppDbStdIf_IsBoardVersionFile       )&Db3x_isBoardVersionFile       ;
    stdif->isConfigFile                  = (AppDbStdIf_IsConfigFile             )&Db3x_isConfigFile             ;
    stdif->loadBoardVersionFile          = (AppDbStdIf_LoadBoardVersionFile     )&Db3x_loadBoardVersionFile     ;
    stdif->loadConfigFile                = (AppDbStdIf_LoadConfigFile           )&Db3x_loadConfigFile           ;
    stdif->saveBoardVersion              = (AppDbStdIf_SaveBoardVersion         )&Db3x_saveBoardVersion         ;

    stdif->isBoardTypeSupported          = (AppDbStdIf_IsBoardTypeSupported     )&Db3x_isBoardTypeSupported     ;
    stdif->isBoardVersionSupported       = (AppDbStdIf_IsBoardVersionSupported  )&Db3x_isBoardVersionSupported  ;
    stdif->isConfigFileVersionActual     = (AppDbStdIf_IsConfigFileVersionActual)&Db3x_isConfigFileVersionActual;
    stdif->setupWizard                   = (AppDbStdIf_SetupWizard              )&Db3x_setupWizard              ;
    stdif->clearFaults                   = (AppDbStdIf_ClearFaults              )&Db3x_clearFaults              ;
    /* *INDENT-ON* */


    return TRUE;
}

