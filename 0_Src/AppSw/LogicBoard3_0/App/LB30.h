/**
 * \file LB30.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 * *
 * \defgroup LB30 Logic board 3.0
 * \ingroup LB
 *
 * Support for boards:
 * - LB 3.0
 * - LB 3.1
 * - LB 3.3
 *
 * \defgroup LB30_Inverter_Datastructures Data structures
 * \ingroup LB30
 * \defgroup LB30_Inverter_Functions Functions
 * \ingroup LB30
 * \defgroup LB30_Inverter_StdIf_Functions StdIf Functions
 * \ingroup LB30
 */

#ifndef LB30_H
#define LB30_H

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include "StdIf/IfxStdIf_Inverter.h"
#include "SysSe/Comm/Ifx_Message.h"
#include "Configuration.h"
#include "Asclin/Asc/IfxAsclin_Asc.h"
#include "Can/Can/IfxCan_Can.h"
#include "Gtm/Atom/Timer/IfxGtm_Atom_Timer.h"
#include "Gtm/Atom/PwmHl/IfxGtm_Atom_PwmHl.h"
#include "Gtm/Tim/In/IfxGtm_Tim_In.h"
#include "Gtm/Tom/PwmHl/IfxGtm_Tom_PwmHl.h"
#include "Ccu6/PwmHl/IfxCcu6_PwmHl.h"
#include "Qspi/SpiMaster/IfxQspi_SpiMaster.h"
#include "Gpt12/IncrEnc/IfxGpt12_IncrEnc.h"
#include "SysSe/Ext/Ad2s1210.h"
#include "SysSe/Ext/IfxTle5012/IfxTle5012_Driver.h"
#include "Edsadc/Rdc/IfxDsadc_Rdc.h"
#include "SysSe/Ext/Tlf35584/Driver/IfxTlf35584_Driver.h"
#include "Common/AppGlobalDef.h"
#include "Common/AppInterface.h"
#include "SysSe/Ext/At25xxx/Ifx_Efs.h"
#include "SysSe/Ext/At25xxx/Ifx_EfsShell.h"
#include "Common/AppDbStdIf.h"
#include "Common/AppLbStdif.h"
#include "Iom/Driver/IfxIom_Driver.h"
/******************************************************************************/
/*------------------------------Type Definitions------------------------------*/
/******************************************************************************/

/** \brief Inverter application message flags */
typedef enum
{
    LB30_Message_errorCurrentU = 0,      /**< \brief Error: phase current U limit reached */
    LB30_Message_errorCurrentV,          /**< \brief Error: phase current V limit reached */
    LB30_Message_errorCurrentW,          /**< \brief Error: phase current W limit reached */
    LB30_Message_errorVdc,               /**< \brief Error: Vdc limit reached */
    LB30_Message_errorIgbtTempU,         /**< \brief Error: IGBT temp U limit reached */
    LB30_Message_errorIgbtTempV,         /**< \brief Error: IGBT temp V limit reached */
    LB30_Message_errorIgbtTempW,         /**< \brief Error: IGBT temp W limit reached */
    LB30_Message_errorCurrentInputU,     /**< \brief Error: Current input U failure */
    LB30_Message_errorCurrentInputV,     /**< \brief Error: Current input V failure */
    LB30_Message_errorCurrentInputW,     /**< \brief Error: Current input W failure */

    LB30_Message_errorBoardTemp,         /**< \brief Error: board temperature limit reached */
    LB30_Message_errorVana50,            /**< \brief Error: Vana 5.0V limit reached */
    LB30_Message_errorVref50,            /**< \brief Error: Vref 5.0V limit reached */
    LB30_Message_errorVdig33,            /**< \brief Error: Vdig 3.3V limit reached */
    LB30_Message_errorKl30,              /**< \brief Error: KL30 limit reached */
    LB30_Message_errorMotorTemp,         /**< \brief Error: Motor temperature limit reached */
    LB30_Message_errorAn0,         		 /**< \brief Error:Analog input 0 limit reached */
    LB30_Message_errorAn1,         		 /**< \brief Error:Analog input 1 limit reached */
    LB30_Message_errorAn2,         		 /**< \brief Error:Analog input 2 limit reached */
    LB30_Message_errorAn3,         		 /**< \brief Error:Analog input 3 limit reached */

    LB30_Message_errorIomUTop,         		 /**< \brief Error: IOM event phase U top */
    LB30_Message_errorIomVTop,         		 /**< \brief Error: IOM event phase V top */
    LB30_Message_errorIomWTop,         		 /**< \brief Error: IOM event phase W top */
    LB30_Message_errorIomUBottom,            /**< \brief Error: IOM event phase U bottom */
    LB30_Message_errorIomVBottom,         	 /**< \brief Error: IOM event phase V bottom */
    LB30_Message_errorIomWBottom,         	 /**< \brief Error: IOM event phase W bottom */

    LB30_Message_COUNT                   /**< \brief Runtime message count (must be less or equal than 32). */
} LB30_Message;

/******************************************************************************/
/*-----------------------------Data Structures--------------------------------*/
/******************************************************************************/
/** \addtogroup LB30_Inverter_Datastructures
 * \{ */

/** \brief Incremental encoder object
 */
typedef struct
{
    struct
    {
        IfxGtm_Atom_Timer timerDriver;       /**< \brief PWM timer driver */
        IfxGtm_Atom_PwmHl pwmDriver;         /**< \brief PWM interface driver */
    }atom;
    struct
    {
        IfxGtm_Tom_Timer timerDriver;       /**< \brief PWM timer driver */
        IfxGtm_Tom_PwmHl pwmDriver;         /**< \brief PWM interface driver */
    }iomRefTom;
    struct
    {
    	IfxIom_Driver driver;								/** \brief IOM object */
        IfxIom_Driver_Lam lam[ECU_PHASE_PER_MOTOR_COUNT*2+1]; /** \bref LAM objects, one for each phases and trigger */
        uint32 eventMask;									/** \brief temporary variable used to store the event enable mask */
    }iom;
    struct
    {
        IfxGtm_Tom_Timer timerDriver;       /**< \brief PWM timer driver */
        IfxGtm_Tom_PwmHl pwmDriver;         /**< \brief PWM interface driver */
    }tom;
    struct
    {
        IfxCcu6_TimerWithTrigger timerDriver;                     /**< \brief PWM timer driver */
        IfxCcu6_PwmHl            pwmDriver;                       /**< \brief PWM interface driver */
    }                        ccu6;
    IfxStdIf_PwmHl           pwmStdIf;                            /**< \brief PWM standard interface driver */
    IfxStdIf_PwmHl           iomRefStdIf;                            /**< \brief PWM standard interface driver */

    Ifx_TimerValue           duty[ECU_PHASE_PER_MOTOR_COUNT];     /**< \brief Last applied duty cycles */
    float32                  deadtimeComp;                        /**< \brief Deadtime compensation value */
    IfxStdIf_Inverter_Status status;
    boolean                  limitError;                          /**< \brief This flag is set when one of monitored limits is reached. see \ref Configuration_limits*/
    boolean                  iomError;                             /**< \brief This flag is set when one the IOM raised an event */
    boolean                  inputError;                          /**< \brief This flag is set when one of input shows an error (sensor error detection).*/
    Ifx_Message              messages;                            /**< \brief Message flags */
    IfxGtm_Tim_In            igbtTemp[ECU_PHASE_PER_MOTOR_COUNT]; /* IGBT temperature measurement. phase U, V, W */
} LB30_Inverter;

/** \} */

/* Resolver gain settings
 */
typedef enum
{
    LB30_ResolverGain_2_08 = 0x1,
    LB30_ResolverGain_1_92 = 0x2,
    LB30_ResolverGain_1_74 = 0x4,
    LB30_ResolverGain_1_60 = 0x8,
}LB30_ResolverGain;

/* Resolver gain settings
 * LB3.1 and next versions
 */
typedef enum
{
    LB31_ResolverGain_notConnected 	= 0 ,  /**< \brief Not connected  */
    LB31_ResolverGain_0_75 			= 1 ,  /**< \brief Gain 0.75 */
    LB31_ResolverGain_0_68          = 2 ,  /**< \brief Gain 0.68 */
    LB31_ResolverGain_1_43          = 3 ,  /**< \brief Gain 1.43 */
    LB31_ResolverGain_0_63          = 4 ,  /**< \brief Gain 0.63 */
    LB31_ResolverGain_1_38          = 5 ,  /**< \brief Gain 1.38 */
    LB31_ResolverGain_1_31          = 6 ,  /**< \brief Gain 1.31 */
    LB31_ResolverGain_2_06          = 7 ,  /**< \brief Gain 2.06 */
    LB31_ResolverGain_0_58          = 8 ,  /**< \brief Gain 0.58 */
    LB31_ResolverGain_1_33          = 9 ,  /**< \brief Gain 1.33 */
    LB31_ResolverGain_1_26          = 10,  /**< \brief Gain 1.26 */
    LB31_ResolverGain_2_01          = 11,  /**< \brief Gain 2.01 */
    LB31_ResolverGain_1_20          = 12,  /**< \brief Gain 1.20 */
    LB31_ResolverGain_1_95          = 13,  /**< \brief Gain 1.95 */
    LB31_ResolverGain_1_88          = 14,  /**< \brief Gain 1.88 */
    LB31_ResolverGain_2_63          = 15,  /**< \brief Gain 2.63 */
}LB31_ResolverGain;

typedef enum
{
    LB30_ResolverOutputSelect_dsadcRdc = 0,
    LB30_ResolverOutputSelect_ad2s1210 = 1
}LB30_ResolverOutputSelect;

// 20191226 Jimmy Created //
typedef struct
{
    uint8 tx[CFG_ASC0_TX_BUFFER_SIZE + CFG_CONSOLE_TX_BUFFER_SIZE + sizeof(Ifx_Fifo) + 8];
    uint8 rx[CFG_ASC0_RX_BUFFER_SIZE + sizeof(Ifx_Fifo) + 8];
} AppAsc0Buffer;

typedef struct
{
    uint8 tx[CFG_ASC1_TX_BUFFER_SIZE + sizeof(Ifx_Fifo) + 8];
    uint8 rx[CFG_ASC1_RX_BUFFER_SIZE + sizeof(Ifx_Fifo) + 8];
} AppAsc1Buffer;


typedef struct
{

    LB_FileBoardVersion  *boardVersion;
    LB_FileConfiguration configuration;

    struct
    {
        IfxQspi_SpiMaster         *qspi0;        /**< \brief QSPI0 driver data.  */
        Ifx_Efs                   *efsConfig;    /**< Ifx_At25xxx file system (configuration) */
        IfxAsclin_Asc             asc0;         /**< \brief ASC 0 interface */
        IfxAsclin_Asc             asc1;         /**< \brief ASC 1 interface */
        IfxPort_Pin               asc1Rs232Select;
        IfxCan_Can           	  can;          /**< \brief CAN driver */
        IfxCan_Can_Node           canNodes[2];  /**< \brief CAN nodes */
        IfxPort_Pin               can0ErrN;
        IfxPort_Pin               can0En;
        IfxPort_Pin               can0StbN;
        IfxPort_Pin               can0node3ErrN;		// 20191218 Jimmy DIY
        IfxPort_Pin               can0node3En;			// 20191218 Jimmy DIY
        IfxPort_Pin               can0node3StbN;		// 20191218 Jimmy DIY
        IfxGtm_Atom_Timer         timerOneMs;          /**< \brief 1ms interrupt */
        IfxGtm_Atom_Timer         timerTenMs;          /**< \brief PWM timer driver */
        IfxQspi_SpiMaster         qspi2;               /**< \brief QSPI2 driver data.  */
//        IfxQspi_SpiMaster         qspi3;               /**< \brief QSPI3 driver data.  */
        IfxQspi_SpiMaster		  qspi5;				// 20191218 Jimmy DIY
        IfxQspi_SpiMaster_Channel sscAd2s1210;         /**< \brief SSC channel for AD2S1210. */
        IfxQspi_SpiMaster_Channel sscTle5012;         /**< \brief SSC channel for TLE5012. */
        IfxGpt12_IncrEnc          encoder;             /**< \brief GPT12-based encoder object */
        Ad2s1210                  ad2s1210;            /**< \brief AD2S1210 based resolver object */
        IfxTle5012_Driver         tle5012;             /**< \brief TLE5012 iGMR object */
        IfxEdsadc_Edsadc          dsadc;
        IfxDsadc_Rdc              dsadcRdc[2];           /**< \brief DSADC-based resolver object */
        IfxPort_Pin               Resolver0GainSel[4]; /**< \brief Resolver 0 Gain settings */
        IfxPort_Pin               Resolver1GainSel[4]; /**< \brief Resolver 1 Gain settings */
        IfxPort_Pin               ResolverOutputSel;   /**< \brief Select between AD2S1210 and Resolver 0 */

        IfxTlf35584_Driver               tlf35584;            /**< TLF35584 driver */
        IfxQspi_SpiMaster_Channel sscTlf35584;         /**< \brief SSC channel for TLF35584 */

        IfxEvadc_Adc_Group        group[15];
        LB30_Inverter             inverter;
        IfxPort_Pin               digitalInputs[4];     /**< \brief General purpose digital inputs */
        IfxPort_Pin               digitalOutputs[4];    /**< \brief General purpose digital outputs */
        IfxPort_Pin				  LED_Indicator[2];		// 20191220 Jimmy Add. //
    }driver;

    struct
    {
        AppAnalogInput currents[ECU_MOTOR_COUNT][ECU_PHASE_PER_MOTOR_COUNT];
        AppAnalogInput vDc;			// vDc from PhaseV
		AppAnalogInput vDcPhaseU;	// vDc from PhaseU
        AppAnalogInput igbtTemp[ECU_PHASE_PER_MOTOR_COUNT];

        AppAnalogInput motorTemp;

        //AppAnalogInput in[4];	// Jovi
        AppAnalogInput tempBoard;
        AppAnalogInput vAna50;
        AppAnalogInput vRef50;
        AppAnalogInput vDig50;
        AppAnalogInput kl30;
        AppAnalogInput vRef25;
		AppAnalogInput vEncoder;
		AppAnalogInput vHVILIn;
		AppAnalogInput vHVILOut;
		AppAnalogInput iVdc;
		AppAnalogInput tempDRVBoard;
		AppAnalogInput tempWater;
    }analogInput;
    // 20191226 Jimmy Created //
    AppAsc0Buffer asc0Buffer;             /**< \brief ASC interface buffer */
    AppAsc1Buffer asc1Buffer;             /**< \brief ASC interface buffer */

    struct
    {
        IfxStdIf_DPipe    asc0;
        IfxStdIf_DPipe    asc1;
        IfxStdIf_Pos      encoder;
        IfxStdIf_Pos      tle5012;
        IfxStdIf_Pos      ad2s1210;
        IfxStdIf_Pos      dsadcRdc[2];
        IfxStdIf_Inverter inverter;
    }  stdIf;

    AppDbStdIf *driverBoardStdif;

    Ifx_MotorModelConfigF32_File *motorConfiguration;
    boolean disableCurrentSensorCheck;		/**< \brief If TRUE, the current sensors are disable */
    struct
    {
    	boolean pwmEnabled; /** If true, PWM duty cycles will be updated */
    	boolean running;		/** If true, selftest pwm is running */
    	Ifx_TimerValue duty[3]; /** PWM duty cycle values (reference)*/
    	Ifx_TimerValue dutyFaulty[3]; /** PWM duty cycle values (error injection) */
    }selftest;
}LB30;


//________________________________________________________________________________________
// GLOBAL VARIABLES

IFX_EXTERN const Ifx_Message_Entry g_LB30_Messages[LB30_Message_COUNT];

/** \addtogroup LB30_Inverter_Functions
 * \{ */

/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \see Limit_onOutOfRange
 */
void LB30_Inverter_onLimitError(LB30 *driver, Limits_Status status, sint32 info);

void LB30_Inverter_checkCurrentInputFault(LB30 *driver);

/** \} */

/** \addtogroup LB30_Inverter_StdIf_Functions
 * \{ */

/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \copydoc IfxStdIf_Inverter_ClearFlags */
boolean LB30_Inverter_clearFlags(LB30 *driver);
/** \copydoc IfxStdIf_Inverter_Disable
 *  Done by setting the enable pin of the gate driver
 */
void LB30_Inverter_disable(LB30 *driver);
/** \copydoc IfxStdIf_Inverter_Enable
 * Done by setting the enable pin of the gate driver
 */
void LB30_Inverter_enable(LB30 *driver);
/** \copydoc IfxStdIf_Inverter_GetDeadtime */
float32 LB30_Inverter_getDeadtime(LB30 *driver);
/** \copydoc IfxStdIf_Inverter_GetMinPulse */
float32 LB30_Inverter_getMinPulse(LB30 *driver);
/** \copydoc IfxStdIf_Inverter_GetPeriod */
float32 LB30_Inverter_getPeriod(LB30 *driver);
/** \copydoc IfxStdIf_Inverter_GetPeriodTicks */
Ifx_TimerValue LB30_Inverter_getPeriodTicks(LB30 *driver);
/** \copydoc IfxStdIf_Inverter_GetPhaseCurrents */
void LB30_Inverter_getPhaseCurrents(LB30 *driver, float32 *currents);
/** \copydoc IfxStdIf_Inverter_GetStatus */
IfxStdIf_Inverter_Status LB30_Inverter_getStatus(LB30 *driver);
/** \copydoc IfxStdIf_Inverter_GetSwitchTemps */
void LB30_Inverter_getSwitchTemps(LB30 *driver, float32 *temps);
/** \copydoc IfxStdIf_Inverter_GetVdc */
float32 LB30_Inverter_getVdc(LB30 *driver);
/** \copydoc IfxStdIf_Inverter_StartPwmTimer */
void LB30_Inverter_startPwmTimer(LB30 *driver);
/** \copydoc IfxStdIf_Inverter_SetDeadtime */
void LB30_Inverter_setDeadtime(LB30 *driver, float32 deadtime);
/** \copydoc IfxStdIf_Inverter_SetMinPulse */
void LB30_Inverter_setMinPulse(LB30 *driver, float32 minPulse);
/** \copydoc IfxStdIf_Inverter_SetPwm */
void LB30_Inverter_setPwm(LB30 *driver, Ifx_TimerValue *duty);
/** Enable fault injection in the PWM
 */
void LB30_Inverter_setPwmFaultInjection(LB30 *driver, Ifx_TimerValue *duty, Ifx_TimerValue *dutyFaulty);
/** \copydoc IfxStdIf_Inverter_SetPwmOff */
void LB30_Inverter_setPwmOff(LB30 *driver);
/** \copydoc IfxStdIf_Inverter_SetPwmOn
 *  PWM mode is set to center aligned mode
 */
void LB30_Inverter_setPwmOn(LB30 *driver, Ifx_Pwm_Mode mode);
/** \copydoc IfxStdIf_Inverter_SetPeriod */
void LB30_Inverter_setPeriod(LB30 *driver, float32 period);
/** \copydoc IfxStdIf_Inverter_SetPeriodTicks */
void LB30_Inverter_setPeriodTicks(LB30 *driver, Ifx_TimerValue period);
/** \copydoc IfxStdIf_Inverter_SetVoltages */
void LB30_Inverter_setVoltage(LB30 *driver, cfloat32 mab);
/** \copydoc IfxStdIf_Inverter_ShowMessage */
void LB30_Inverter_showMessage(LB30 *driver);
/** \} */

/******************************************************************************/
/*-------------------------Global Function Prototypes-------------------------*/
/******************************************************************************/

/** \brief Initializes the standard interface "Pos"
 * \param stdif Standard interface position object
 * \param driver incremental encoder interface Handle
 * \return TRUE on success else FALSE
 */
IFX_EXTERN boolean LB30_Inverter_stdIfInverterInit(IfxStdIf_Inverter *stdif, LB30 *driver);
void               LB30_setResolver0Gain(LB30 *driver, LB30_ResolverGain gain);
void               LB30_setResolver1Gain(LB30 *driver, LB30_ResolverGain gain);
void               LB31_setResolver0Gain(LB30 *driver, LB31_ResolverGain gain);
void               LB31_setResolver1Gain(LB30 *driver, LB31_ResolverGain gain);
void               LB30_setResolverOutputSelect(LB30 *driver, LB30_ResolverOutputSelect select);

boolean LB30_createLogicBoardConfigFile(LB30 *board, boolean quiet);
boolean LB30_loadLogicBoardConfiguration(LB30 *board);
boolean LB30_init(LB30 *board);

boolean LB30_LbStdIfInit(AppLbStdIf *stdif, LB30 *board);

#endif /* LB30_H */
