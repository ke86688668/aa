/**
 * \file LB30_Isr.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include "LogicBoard3_0/Config/LB30_Interrupts.h"
#include "LogicBoard3_0/App/LB30.h"
#include "Common/main.h"
#include "Common/AppTest.h"
#include "Library/AppSpecialMode.h"
#include "Common/AppCan.h"
#include "SysSe/EMotor/Ifx_MotorControl.h"

#define IFX_INTTOS_CAN_TXRX 	0

const uint32 OneEyeDecode[] = {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,13,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,0,0,0,0,0,0,0,17,0,0,0,0,0,0,0,18,0,0,0,0,
		0,0,0,19,0,0,0,0,0,0,0,20,0,0,0,0,0,0,0,21,0,0,0,0,0,0,0,22,0,0,0,0,0,0,0,23,
		0,0,0,0,0,0,0,24,0,0,0,0,0,0,0,25,0,0,0,0,0,0,0,26,0,0,0,0,0,0,0,27,0,0,0,0,0,
		0,0,28,0,0,0,0,0,0,0,29,0,0,0,0,0,0,0,30,0,0,0,0,0,0,0,31,0,0,0,0,0,0,0,32,0,
		0,0,33,0,0,0,34,0,0,0,35,0,0,0,36,0,0,0,37,0,0,0,38,0,0,0,39,0,0,0,40,0,0,0,
		41,0,0,0,42,0,0,0,43,0,0,0,44,0,0,0,45,0,0,0,46,0,0,0,47,0,0,0,48,0,0,0,49,0,
		0,0,50,0,0,0,51,0,0,0,52,0,0,0,53,0,0,0,54,0,0,0,55,0,0,0,56,0,0,0,57,0,0,0,
		58,0,0,0,59,0,0,0,60,0,0,0,61,0,0,0,62,0,0,0,63,0,0,0,64,0,65,0,66,0,67,0,68,
		0,69,0,70,0,71,0,72,0,73,0,74,0,75,0,76,0,77,0,78,0,79,0,80,0,81,0,82,0,83,0,
		84,0,85,0,86,0,87,0,88,0,89,0,90,0,91,0,92,0,93,0,94,0,95,0,96,0,97,0,98,0,99,
		0,100};

/** \addtogroup LB30_AppIsr_interrupts
 * \{ */

/** \name Serial interface 0 interrupts
 * \{ */
IFX_INTERRUPT(LB30_Isr_Asc0_rx, 0, ISR_PRIORITY_ASC0_RX);
IFX_INTERRUPT(LB30_Isr_Asc0_tx, 0, ISR_PRIORITY_ASC0_TX);
IFX_INTERRUPT(LB30_Isr_Asc0_ex, 0, ISR_PRIORITY_ASC0_EX);
/** \} */

/** \name Serial interface 1 interrupts
 * \{ */
IFX_INTERRUPT(LB30_Isr_Asc1_rx, 0, ISR_PRIORITY_ASC1_RX);
IFX_INTERRUPT(LB30_Isr_Asc1_tx, 0, ISR_PRIORITY_ASC1_TX);
IFX_INTERRUPT(LB30_Isr_Asc1_ex, 0, ISR_PRIORITY_ASC1_EX);
/** \} */

/** \name CAN 0 NODE 0 communication interrupts
 * \{ */
// TI MASTER //
IFX_INTERRUPT(Rx_InterrputCAN0NODE0_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN0_NODE0_RX);
IFX_INTERRUPT(Tx_InterrputCAN0NODE0_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN0_NODE0_TX);
// TI SLAVE //
IFX_INTERRUPT(Rx_InterrputCAN0NODE3_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN0_NODE3_RX);
IFX_INTERRUPT(Tx_InterrputCAN0NODE3_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN0_NODE3_TX);
// APM CAN //
IFX_INTERRUPT(Rx_InterrputCAN1NODE1_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN1_NODE1_RX);
IFX_INTERRUPT(Tx_InterrputCAN1NODE1_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN1_NODE1_TX);
// OBC CAN //
IFX_INTERRUPT(Rx_InterrputCAN1NODE2_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN1_NODE2_RX);
IFX_INTERRUPT(Tx_InterrputCAN1NODE2_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN1_NODE2_TX);
// EVCC CAN //
IFX_INTERRUPT(Rx_InterrputCAN2NODE0_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN2_NODE0_RX);
IFX_INTERRUPT(Tx_InterrputCAN2NODE0_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN2_NODE0_TX);
/** \} */

/** \name Timer interrupts
 * \{ */
IFX_INTERRUPT(LB30_Isr_Timer_1ms, 0, ISR_PRIORITY_TIMER_1MS);
IFX_INTERRUPT(LB30_Isr_Timer_10ms, 0, ISR_PRIORITY_TIMER_10MS);
/** \} */

/** \name M0 interrupts
 * \{ */
IFX_INTERRUPT(LB30_Isr_M0_Period, 0, ISR_PRIORITY_M0_PWMHL_PERIOD);
IFX_INTERRUPT(LB30_Isr_M0_Trigger, 0, ISR_PRIORITY_M0_PWMHL_TRIGGER);
IFX_INTERRUPT(LB30_Isr_Adc_M0, 0, ISR_PRIORITY_ADC_M0);
IFX_INTERRUPT(LB30_Isr_M0_IgbtTempU, 0, ISR_PRIORITY_IGBT_TEMPU);
IFX_INTERRUPT(LB30_Isr_M0_IgbtTempV, 0, ISR_PRIORITY_IGBT_TEMPV);
IFX_INTERRUPT(LB30_Isr_M0_IgbtTempW, 0, ISR_PRIORITY_IGBT_TEMPW);
/** \} */
/** \name Encoder interrupts
 * \{ */
IFX_INTERRUPT(LB30_Isr_Gpt12_encoder, 0, ISR_PRIORITY_ENCODER_GPT12);
/** \} */

/** \name SSC interrupts
 * \{ */
IFX_INTERRUPT(LB30_Isr_Qspi2_tx, 0, ISR_PRIORITY_QSPI2_TX);
IFX_INTERRUPT(LB30_Isr_Qspi2_rx, 0, ISR_PRIORITY_QSPI2_RX);
IFX_INTERRUPT(LB30_Isr_Qspi2_err, 0, ISR_PRIORITY_QSPI2_ERR);

IFX_INTERRUPT(LB30_Isr_Qspi5_tx, 0, ISR_PRIORITY_QSPI5_TX);
IFX_INTERRUPT(LB30_Isr_Qspi5_rx, 0, ISR_PRIORITY_QSPI5_RX);
IFX_INTERRUPT(LB30_Isr_Qspi5_err, 0, ISR_PRIORITY_QSPI5_ERR);
/** \} */

/** \name DSADC resolver interrupt
 * \{ */
IFX_INTERRUPT(LB30_Isr_Dsadc_rdc0, 0, ISR_PRIORITY_DSADC_RDC0);
/** \} */

/** \} */

//	TC397 LED Indicator Configurations //
App_CanBasic g_CanBasic;

uint32 TIME_CNT_10ms = 0;
#if ENABLEALLCAN
static volatile uint32 numTxmsgs[10];
static volatile uint32 numRxmsgs[10];
static volatile uint32 Can0Node0Txmsgs[10];
static volatile uint32 Can0Node0Rxmsgs[10];
#else if
static volatile uint32 numTxmsgs[IFXCAN_NUM_MODULES];
static volatile uint32 numRxmsgs[IFXCAN_NUM_MODULES];
static volatile uint32 Can0Node0Txmsgs[IFXCAN_NUM_MODULES];
static volatile uint32 Can0Node0Rxmsgs[IFXCAN_NUM_MODULES];
#endif
uint8 Flag_PWMEngineerTest = 0;
uint8 DutyTestU = 0, DutyTestV = 0, DutyTestW = 0;

sint16 result_vDCPhaseV = 0, result_vDCPhaseU = 0;
sint16 result_vANA50 = 0, result_vRef50 = 0, result_vDig50 = 0, result_vEncoder = 0, result_kl30 = 0, result_vRef25 = 0;
sint16 result_vHVILIn = 0, result_vHVILOut = 0;
sint16 result_iVdc = 0, result_iPhaseU = 0, result_iPhaseV = 0, result_iPhaseW = 0;
sint16 result_tempDRVBoard = 0, result_tempWater = 0, tempMotor = 0, tempCTRLBoard = 0;

//________________________________________________________________________________________
// FUNCTION IMPLEMENTATIONS

#define logicBoard g_Lb.logicboards.logicBoard_30

/** \brief Handle Asc0 Rx interrupt
 *
 * \isrProvider{ISR_PROVIDER_ASC0}
 * \isrPriority{ISR_PRIORITY_ASC0_RX}
 *
 * - Configuration of this interrupt is done by Asc_If_init()
 * - This interrupt is raised each time a data have been received on the serial interface.
 *   and Asc_If_receiveIrq() will be called
 */
void LB30_Isr_Asc0_rx(void)
{
    __enable();
    IfxStdIf_DPipe_onReceive(&logicBoard.stdIf.asc0);
}


/** \brief Handle Asc0 Tx interrupt
 *
 * \isrProvider{ISR_PROVIDER_ASC0}
 * \isrPriority{ISR_PRIORITY_ASC0_TX}
 *
 * - Configuration of this interrupt is done by Asc_If_init()
 * - This interrupt is raised each time the serial interface transmit buffer get empty
 *   and Asc_If_transmitIrq() will be called
 */
void LB30_Isr_Asc0_tx(void)
{
    __enable();
    IfxStdIf_DPipe_onTransmit(&logicBoard.stdIf.asc0);
}


/** \brief Handle Asc0 Ex interrupt.
 *
 * \isrProvider{ISR_PROVIDER_ASC0}
 * \isrPriority{ISR_PRIORITY_ASC0_EX}
 *
 * - Configuration of this interrupt is done by Asc_If_init()
 * - This interrupt is raised each time an error occurs on the serial interface and
 *   Asc_If_errorIrq() will be called.
 */
void LB30_Isr_Asc0_ex(void)
{
    __enable();
    IfxStdIf_DPipe_onError(&logicBoard.stdIf.asc0);
}


/** \brief Handle Asc1 Rx interrupt
 *
 * \isrProvider{ISR_PROVIDER_ASC1}
 * \isrPriority{ISR_PRIORITY_ASC1_RX}
 *
 * - Configuration of this interrupt is done by Asc_If_init()
 * - This interrupt is raised each time a data have been received on the serial interface.
 *   and Asc_If_receiveIrq() will be called
 */
void LB30_Isr_Asc1_rx(void)
{
    __enable();
    IfxStdIf_DPipe_onReceive(&logicBoard.stdIf.asc1);
}


/** \brief Handle Asc1 Tx interrupt
 *
 * \isrProvider{ISR_PROVIDER_ASC1}
 * \isrPriority{ISR_PRIORITY_ASC1_TX}
 *
 * - Configuration of this interrupt is done by Asc_If_init()
 * - This interrupt is raised each time the serial interface transmit buffer get empty
 *   and Asc_If_transmitIrq() will be called
 */
void LB30_Isr_Asc1_tx(void)
{
    __enable();
    IfxStdIf_DPipe_onTransmit(&logicBoard.stdIf.asc1);
}


/** \brief Handle Asc1 Ex interrupt.
 *
 * \isrProvider{ISR_PROVIDER_ASC1}
 * \isrPriority{ISR_PRIORITY_ASC1_EX}
 *
 * - Configuration of this interrupt is done by Asc_If_init()
 * - This interrupt is raised each time an error occurs on the serial interface and
 *   Asc_If_errorIrq() will be called.
 */
void LB30_Isr_Asc1_ex(void)
{
    __enable();
    IfxStdIf_DPipe_onError(&logicBoard.stdIf.asc1);
}

/** \brief Handle Tx interrupt on CAN0 node0 module 0.
 *
 * \isrProvider \ref IFX_INTTOS_CAN_TXRX
 * \isrPriority \ref ISR_PRIORITY_CAN0_NODE0_TX
 *
 */
IFX_INTERRUPT(Tx_InterrputCAN0NODE0_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN0_NODE0_TX)
{
#if 1
	Ifx_CAN   *canSfr = &MODULE_CAN0;

	if(CAN0_IR0.B.TC == 1)
	{
		Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(canSfr,(IfxCan_NodeId)0);
		IfxCan_Node_clearInterruptFlag(nodeSfr,IfxCan_Interrupt_transmissionCompleted);
		numTxmsgs[0]++;
	}
#endif
}

/** \brief Handle Rx interrupt on CAN0 node0 module 0.
 *
 * \isrProvider \ref IFX_INTTOS_CAN_TXRX
 * \isrPriority \ref ISR_PRIORITY_CAN0_NODE0_RX
 *
 */
IFX_INTERRUPT(Rx_InterrputCAN0NODE0_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN0_NODE0_RX)
{
#if 1
	Ifx_CAN   *canSfr =  &MODULE_CAN0;

	if(CAN0_IR0.B.DRX == 1)
	{
		Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(canSfr,(IfxCan_NodeId)0);
		IfxCan_Node_clearInterruptFlag(nodeSfr,IfxCan_Interrupt_messageStoredToDedicatedRxBuffer);
		numRxmsgs[0]++;
	}
#endif
}

/** \brief Handle Tx interrupt on CAN0 node3 module 0.
 *
 * \isrProvider \ref IFX_INTTOS_CAN_TXRX
 * \isrPriority \ref ISR_PRIORITY_CAN0_NODE3_TX
 *
 */
IFX_INTERRUPT(Tx_InterrputCAN0NODE3_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN0_NODE3_TX)
{
#if 1
	Ifx_CAN   *canSfr = &MODULE_CAN0;

	if(CAN0_IR1.B.TC == 1)
	{
		Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(canSfr,(IfxCan_NodeId)3);
		IfxCan_Node_clearInterruptFlag(nodeSfr,IfxCan_Interrupt_transmissionCompleted);
		numTxmsgs[1]++;
	}
#endif
}

/** \brief Handle Rx interrupt on CAN0 node3 module 0.
 *
 * \isrProvider \ref IFX_INTTOS_CAN_TXRX
 * \isrPriority \ref ISR_PRIORITY_CAN0_NODE3_RX
 *
 */
IFX_INTERRUPT(Rx_InterrputCAN0NODE3_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN0_NODE3_RX)
{
#if 1
	Ifx_CAN   *canSfr =  &MODULE_CAN0;

	if(CAN0_IR1.B.DRX == 1)
	{
		Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(canSfr,(IfxCan_NodeId)3);
		IfxCan_Node_clearInterruptFlag(nodeSfr,IfxCan_Interrupt_messageStoredToDedicatedRxBuffer);
		numRxmsgs[1]++;
	}
#endif
}

/** \brief Handle Tx interrupt on CAN1 node1 module 0.
 *
 * \isrProvider \ref IFX_INTTOS_CAN_TXRX
 * \isrPriority \ref ISR_PRIORITY_CAN1_NODE1_TX
 *
 */
IFX_INTERRUPT(Tx_InterrputCAN1NODE1_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN1_NODE1_TX)
{
#if 1
	Ifx_CAN   *canSfr = &MODULE_CAN1;

	if(CAN1_IR1.B.TC == 1)
	{
		Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(canSfr,(IfxCan_NodeId)1);
		IfxCan_Node_clearInterruptFlag(nodeSfr,IfxCan_Interrupt_transmissionCompleted);
		numTxmsgs[2]++;
	}
#endif
}

/** \brief Handle Rx interrupt on CAN1 node1 module 0.
 *
 * \isrProvider \ref IFX_INTTOS_CAN_TXRX
 * \isrPriority \ref ISR_PRIORITY_CAN1_NODE1_RX
 *
 */
IFX_INTERRUPT(Rx_InterrputCAN1NODE1_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN1_NODE1_RX)
{
#if 1
	Ifx_CAN   *canSfr =  &MODULE_CAN1;

	if(CAN1_IR1.B.DRX == 1)
	{
		Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(canSfr,(IfxCan_NodeId)1);
		IfxCan_Node_clearInterruptFlag(nodeSfr,IfxCan_Interrupt_messageStoredToDedicatedRxBuffer);
		numRxmsgs[2]++;
	}
#endif
}

/** \brief Handle Tx interrupt on CAN1 node2 module 0.
 *
 * \isrProvider \ref IFX_INTTOS_CAN_TXRX
 * \isrPriority \ref ISR_PRIORITY_CAN1_NODE2_TX
 *
 */
IFX_INTERRUPT(Tx_InterrputCAN1NODE2_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN1_NODE2_TX)
{
#if 1
	Ifx_CAN   *canSfr = &MODULE_CAN1;

	if(CAN1_IR2.B.TC == 1)
	{
		Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(canSfr,(IfxCan_NodeId)2);
		IfxCan_Node_clearInterruptFlag(nodeSfr,IfxCan_Interrupt_transmissionCompleted);
		numTxmsgs[3]++;
	}
#endif
}

/** \brief Handle Rx interrupt on CAN1 node2 module 0.
 *
 * \isrProvider \ref IFX_INTTOS_CAN_TXRX
 * \isrPriority \ref ISR_PRIORITY_CAN1_NODE2_RX
 *
 */
IFX_INTERRUPT(Rx_InterrputCAN1NODE2_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN1_NODE2_RX)
{
#if 1
	Ifx_CAN   *canSfr =  &MODULE_CAN1;

	if(CAN1_IR2.B.DRX == 1)
	{
		Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(canSfr,(IfxCan_NodeId)2);
		IfxCan_Node_clearInterruptFlag(nodeSfr,IfxCan_Interrupt_messageStoredToDedicatedRxBuffer);
		numRxmsgs[3]++;
	}
#endif
}

/** \brief Handle Tx interrupt on CAN2 node0 module 0.
 *
 * \isrProvider \ref IFX_INTTOS_CAN_TXRX
 * \isrPriority \ref ISR_PRIORITY_CAN2_NODE0_TX
 *
 */
IFX_INTERRUPT(Tx_InterrputCAN2NODE0_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN2_NODE0_TX)
{
#if 1
	Ifx_CAN   *canSfr = &MODULE_CAN2;

	if(CAN2_IR0.B.TC == 1)
	{
		Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(canSfr,(IfxCan_NodeId)0);
		IfxCan_Node_clearInterruptFlag(nodeSfr,IfxCan_Interrupt_transmissionCompleted);
		numTxmsgs[4]++;
	}
#endif
}

/** \brief Handle Rx interrupt on CAN2 node0 module 0.
 *
 * \isrProvider \ref IFX_INTTOS_CAN_TXRX
 * \isrPriority \ref ISR_PRIORITY_CAN2_NODE0_RX
 *
 */
IFX_INTERRUPT(Rx_InterrputCAN2NODE0_ISR, IFX_INTTOS_CAN_TXRX, ISR_PRIORITY_CAN2_NODE0_RX)
{
#if 1
	Ifx_CAN   *canSfr =  &MODULE_CAN2;

	if(CAN2_IR0.B.DRX == 1)
	{
		Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(canSfr,(IfxCan_NodeId)0);
		IfxCan_Node_clearInterruptFlag(nodeSfr,IfxCan_Interrupt_messageStoredToDedicatedRxBuffer);
		numRxmsgs[4]++;
	}
#endif
}

#if 0
/** \brief Handle the CAN 0 interrupt
 *
 * \isrProvider{ISR_PROVIDER_CAN0}
 * \isrPriority{ISR_PRIORITY_CAN0}
 */
void LB30_Isr_Can0(void)
{
    __enable();
#if CFG_SETUP == CFG_SETUP_TEST
    g_testData.isrCounter.canNode0++;
#endif
}


/** \brief Handle the CAN 1 interrupt
 *
 * \isrProvider{ISR_PROVIDER_CAN1}
 * \isrPriority{ISR_PRIORITY_CAN1}
 */
void LB30_Isr_Can1(void)
{
    __enable();
}
#endif

void Ifx_CanHandler_processInbox_PWMEngineerTest(void)
{
	uint8 flag_PWMengineertest = 0, dutyU = 0, dutyV = 0, dutyW = 0;

	flag_PWMengineertest = (uint8)(Can0Node0Rxmsgs[1] & 0xFF);
	dutyU = (uint8)((Can0Node0Rxmsgs[1] & 0xFF00) >> 8);
	dutyV = (uint8)((Can0Node0Rxmsgs[1] & 0xFF0000) >> 16);
	dutyW = (uint8)((Can0Node0Rxmsgs[1] & 0xFF000000) >> 24);
	
	if(flag_PWMengineertest == 0x55
		&& (dutyU <= 100
			&& dutyV <= 100
			&& dutyU <= 100))
	{
		Flag_PWMEngineerTest = 1;
		DutyTestU = dutyU;
		DutyTestV = dutyV;
		DutyTestW = dutyW;
	}
	else
	{
		Flag_PWMEngineerTest = 0;
		DutyTestU = 0;
		DutyTestV = 0;
		DutyTestW = 0;
	}
	if(flag_PWMengineertest == 0x33)
	{
		g_App.can.inbox.runCmd = 0;
	}
	else if(flag_PWMengineertest == 0x11)
	{
		g_App.can.inbox.runCmd = 1;
	}
}

void Ifx_CanHandler_processInbox_Jimmy(void)
{
	/*Initialize the message structure with dummy values, will be replaced by the received values*/
	IfxCan_Message rxMsg;
	IfxCan_Can_initMessage(&rxMsg);
	uint32 value;
	sint32 temp, temp1, temp2;
	float32 f32_temp;
	uint32 mode;

	 /*specify the rx buffer number where the data will be received*/
	rxMsg.bufferNumber = 0;

	if(IfxCan_Can_isNewDataReceived(&g_CanBasic.drivers.canNode[0], (IfxCan_RxBufferId)rxMsg.bufferNumber))
	{
		IfxCan_Can_readMessage(&g_CanBasic.drivers.canNode[0], &rxMsg, Can0Node0Rxmsgs);

#if ONEEYECOM
		value = Can0Node0Rxmsgs[0];
		temp = (value & 0xFFFF0000) >> 16;
		value = temp;
		if(value)
			value = value - 16256;

		if((value != 0)||(value > 840))
		{
			g_App.can.inbox.torqueCmd = OneEyeDecode[value];
		}
		else
		{
			g_App.can.inbox.torqueCmd = 0;
		}
#else if
		
#if CAN_FD_ENABLE
		temp = Can0Node0Rxmsgs[0];
		g_App.can.inbox.modeCmd = (uint8)(temp & 0x000000FF);
		f32_temp = (sint16)((temp & 0xFFFF0000) >> 16);
		g_App.can.inbox.torqueCmd = f32_temp/10;

		temp = Can0Node0Rxmsgs[1];
		f32_temp = (sint16)((temp & 0xFFFF0000) >> 16);
		g_App.can.inbox.currentIdCmd = f32_temp/10;
		f32_temp = (sint16)(temp & 0x0000FFFF);
		g_App.can.inbox.currentCmd = f32_temp/10;

		temp = Can0Node0Rxmsgs[2];
		f32_temp = (uint16)((temp & 0xFFFF0000) >> 16);
		g_App.can.inbox.amplitudeCmd = f32_temp/10;
		f32_temp = (sint16)(temp & 0x0000FFFF);
		g_App.can.inbox.currentIqCmd = f32_temp/10;

		temp = Can0Node0Rxmsgs[3];
		temp1 = (uint16)((temp & 0xFFFF0000) >> 16);
		temp2 = (sint16)(temp & 0x0000FFFF);
		if(temp2 == 0)
			g_App.can.inbox.speedCmd = (temp1*4)/120;	/* Polepair = 4 */
		else
			g_App.can.inbox.speedCmd = temp2;

		/* Process runCmd flag setting */
		if(g_App.can.inbox.modeCmd == Ifx_MotorControl_Mode_passive)
			g_App.can.inbox.runCmd = 0;
		else if(g_App.can.inbox.modeCmd == Ifx_MotorControl_Mode_off)
			g_App.can.inbox.runCmd = 0;
		else
			g_App.can.inbox.runCmd = 1;

#else if
		g_App.can.inbox.torqueCmd = Can0Node0Rxmsgs[0];
		Ifx_CanHandler_processInbox_PWMEngineerTest();
#endif
#endif
	}
}

void Ifx_CanHandler_processOutbox_Jimmy(void)
{
	static uint32 CanTx_Index = 0;
	uint8 u32_HiWordHiByte, u32_HiWordLoByte, u32_LoWordHiByte, u32_LoWordLoByte;
#if CAN_FD_ENABLE
	uint16 u16_HiWord, u16_LowWord;
	sint16 s16_HiWord, s16_LowWord;
#endif
	uint32 Buff, Buff2;

	// Initialize the message structure with default values
	IfxCan_Message txMsg;
	IfxCan_Can_initMessage(&txMsg);
//	txMsg.bufferNumber 				= 0;
#if CAN_FD_ENABLE
	txMsg.storeInTxFifoQueue        = FALSE;		// to use the dedicated TxBuffer
	txMsg.frameMode             	= IfxCan_FrameMode_fdLongAndFast;
	txMsg.messageIdLength       	= IfxCan_MessageIdLength_standard;
//	txMsg.dataLengthCode       	    = IfxCan_DataLengthCode_64;


	switch(CanTx_Index)
	{
		case 0:		// ID: 0x33 : 32 DCL, TractionInverter_Measure_1
			txMsg.messageId = 0x33;
			Buff = (uint16)(10*g_App.can.outbox.torqueMeas);
			u16_LowWord = (uint16)(Buff);
			Buff = (uint16)(10*g_App.can.outbox.speedMeas);
			u16_HiWord =  (uint16)(Buff);
			Can0Node0Txmsgs[0] = (u16_HiWord << 16) + u16_LowWord;

			Buff = (uint16)(10*g_App.can.outbox.elecPos);
			u16_LowWord =  (uint16)(Buff);
			Buff = (uint16)(10*g_App.can.outbox.mechPos);
			u16_HiWord =  (uint16)(Buff);
			Can0Node0Txmsgs[1] = (u16_HiWord << 16) + u16_LowWord;

			Buff = (uint16)(10*g_App.can.outbox.mechPosR);
			u16_LowWord =  (uint16)(Buff);
			Buff = (uint16)(10*g_App.can.outbox.mechSpeedR);
			u16_HiWord =  (uint16)(Buff);
			Can0Node0Txmsgs[2] = (u16_HiWord << 16) + u16_LowWord;

			Buff = (uint16)(10*g_App.can.outbox.voltageDcMeas);
			u16_LowWord =  (uint16)(Buff);
			Buff = 0;//(uint16)(10*g_App.can.outbox.currentDcMeas);	// currentDcMeas
			u16_HiWord =  (uint16)(Buff);
			Can0Node0Txmsgs[3] = (u16_HiWord << 16) + u16_LowWord;

			Buff = (uint16)(10*g_App.can.outbox.current[0]);
			u16_LowWord =  (uint16)(Buff);
			Buff = (uint16)(10*g_App.can.outbox.current[1]);
			u16_HiWord =  (uint16)(Buff);
			Can0Node0Txmsgs[4] = (u16_HiWord << 16) + u16_LowWord;

			Buff = (uint16)(10*g_App.can.outbox.current[2]);
			u16_LowWord =  (uint16)(Buff);
			Buff = (uint16)(g_App.can.outbox.current_raw[0]);	// CurrentU raw data
			u16_HiWord =  (uint16)(Buff);
			Can0Node0Txmsgs[5] = (u16_HiWord << 16) + u16_LowWord;

			Buff = (uint16)(g_App.can.outbox.current_raw[1]);	// CurrentV raw data
			u16_LowWord =  (uint16)(Buff);
			Buff = (uint16)(g_App.can.outbox.current_raw[2]);	// CurrentW raw data
			u16_HiWord =  (uint16)(Buff);
			Can0Node0Txmsgs[6] = (u16_HiWord << 16) + u16_LowWord;

			Buff = (uint16)(g_App.can.outbox.currentDcMeas_raw); // currentDcMeas raw data
			u16_LowWord =  (uint16)(Buff);
			Buff = (uint16)(g_App.can.outbox.voltageDcMeas_raw); // voltageDcMeas raw data
			u16_HiWord =  (uint16)(Buff);
			Can0Node0Txmsgs[7] = (u16_HiWord << 16) + u16_LowWord;
			txMsg.dataLengthCode = IfxCan_DataLengthCode_32;
			break;
		case 1:		// ID : 0x36 : 16 DCL, TractionInverter_Status
			txMsg.messageId = 0x36;
			Buff = (uint8)(g_App.can.outbox.mode);
			u16_LowWord = (uint16)(Buff);
			Buff = (uint32)(g_App.can.outbox.fault);
			u16_HiWord = (uint16)(Buff & 0x0000FFFF);
//			u16_HiWord = (uint16)((Buff & 0x000000FF) << 8);
//			u16_HiWord |= (uint16)((Buff & 0x0000FF00) >> 8);
			Can0Node0Txmsgs[0] = (u16_HiWord << 16) + u16_LowWord;
#if 0
			Buff2 = (uint32)((Buff & 0xFFFF0000) >> 16);
			Can0Node0Txmsgs[0] = (Buff2 << 16) + u16_LowWord;

			u16_LowWord = (uint16)(Buff & 0x0000FFFF);
#endif
			u16_LowWord = (uint16)((Buff & 0xFFFF0000) >> 16);
//			u16_LowWord = (uint16)((Buff & 0x00FF0000) >> 8);
//			u16_LowWord |= (uint16)((Buff & 0xFF000000) >> 24);
			Buff = (uint32)(g_App.can.outbox.PMIC_Info);
			Buff2 = (Buff & 0xFFFF0000) >> 16;
			Can0Node0Txmsgs[1] = (Buff2 << 16) + u16_LowWord;

			u16_LowWord = (uint16)(Buff & 0x0000FFFF);
			u16_HiWord = 0;
			Can0Node0Txmsgs[2] = (u16_HiWord << 16) + (u16_LowWord);

			Buff = (uint8)(g_App.can.outbox.GateDriver_Pri_Info[0]);	// Register : 0x02
			u16_LowWord = (uint16)((Buff & 0xFF) << 8);
			Buff = (uint8)(g_App.can.outbox.GateDriver_Sec_Info[0]);	// Register : 0x11
			u16_LowWord += (uint16)(Buff & 0xFF);
			Buff = (uint8)(g_App.can.outbox.GateDriver_Pri_Info[1]);	// Register : 0x02
			u16_HiWord = (uint16)(Buff & 0xFC);
			Buff = (uint8)(g_App.can.outbox.GateDriver_Sec_Info[1]);	// Register : 0x11
			u16_HiWord += (uint16)(Buff & 0xFF);
			Can0Node0Txmsgs[3] = (u16_HiWord << 16) + (u16_LowWord);

			Buff = (uint8)(g_App.can.outbox.GateDriver_Pri_Info[2]);	// Register : 0x02
			u16_LowWord = (uint16)((Buff & 0xFF) << 8);
			Buff = (uint8)(g_App.can.outbox.GateDriver_Sec_Info[2]);	// Register : 0x11
			u16_LowWord += (uint16)(Buff & 0xFF);
			Buff = (uint8)(g_App.can.outbox.GateDriver_Pri_Info[3]);	// Register : 0x02
			u16_HiWord = (uint16)(Buff & 0xFC);
			Buff = (uint8)(g_App.can.outbox.GateDriver_Sec_Info[3]);	// Register : 0x11
			u16_HiWord += (uint16)(Buff & 0xFF);
			Can0Node0Txmsgs[4] = (u16_HiWord << 16) + (u16_LowWord);

			Buff = (uint8)(g_App.can.outbox.GateDriver_Pri_Info[4]);	// Register : 0x02
			u16_LowWord = (uint16)((Buff & 0xFF) << 8);
			Buff = (uint8)(g_App.can.outbox.GateDriver_Sec_Info[4]);	// Register : 0x11
			u16_LowWord += (uint16)(Buff & 0xFF);
			Buff = (uint8)(g_App.can.outbox.GateDriver_Pri_Info[5]);	// Register : 0x02
			u16_HiWord = (uint16)(Buff & 0xFC);
			Buff = (uint8)(g_App.can.outbox.GateDriver_Sec_Info[5]);	// Register : 0x11
			u16_HiWord += (uint16)(Buff & 0xFF);
			Can0Node0Txmsgs[5] = (u16_HiWord << 16) + (u16_LowWord);

			s16_LowWord = (sint16)(g_App.can.outbox.torqueCtrl_set);
			s16_HiWord = 0;
			Can0Node0Txmsgs[6] = (s16_HiWord << 16) + (s16_LowWord);

			Can0Node0Txmsgs[7] = SW_REVISION;
			txMsg.dataLengthCode = IfxCan_DataLengthCode_32;
			break;
		default:
			break;
	}
	CanTx_Index++;
	if(CanTx_Index >= 2)
		CanTx_Index = 0;
#else if
	switch(CanTx_Index)
	{
		case 0:
			// change the relevent members of message structure
			txMsg.messageId = 0x33;
			Buff = (uint32)(10*g_App.can.outbox.torqueMeas);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[0] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			Buff = (uint32)(10*g_App.can.outbox.speedMeas);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[1] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			break;
		case 1:		// ID: 0x34
			txMsg.messageId = 0x34;
			Buff = (uint32)(10*g_App.can.outbox.mechPos);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[0] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			Buff = (uint32)(10*g_App.can.outbox.elecPos);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[1] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			break;
		case 2:		// ID: 0x35
			txMsg.messageId = 0x35;
			Buff = (uint32)(10*g_App.can.outbox.mechPosR);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[0] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			Buff = (uint32)(10*g_App.can.outbox.mechSpeedR);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[1] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			break;
		case 3:		// ID: 0x36
			txMsg.messageId = 0x36;
			Buff = (uint32)(10*g_App.can.outbox.voltageDcMeas);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[0] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			Buff = (uint32)(10*g_App.can.outbox.tempBoardMeas);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[1] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			break;
		case 4:		// ID: 0x55
			txMsg.messageId = 0x55;
			u32_LoWordHiByte = (uint8)(g_App.can.inbox.modeCmd);
			u32_LoWordLoByte = (uint8)(g_App.can.outbox.ecuState);
			Buff = (uint32)(g_App.can.inbox.torqueCmd);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_HiWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[0] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_LoWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_HiWordHiByte = 0;
			u32_HiWordLoByte = 0;
			Can0Node0Txmsgs[1] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			break;
		case 5:		// ID: 0x37
			txMsg.messageId = 0x37;
			Buff = (uint32)(10*g_App.can.outbox.current[0]);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[0] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			Buff = (uint32)(10*g_App.can.outbox.current[1]);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[1] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			break;
		case 6:		// ID: 0x38
			txMsg.messageId = 0x38;
			Buff = (uint32)(10*g_App.can.outbox.current[2]);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[0] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			Buff = (uint32)(10*g_App.can.outbox.tempMotorMeas);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[1] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			break;
		case 7:		// ID: 0X39
			txMsg.messageId = 0x39;
			Buff = (uint32)(g_App.can.outbox.error);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[0] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			Buff = (uint32)(0);
			u32_HiWordHiByte = (uint8)((Buff & 0xFF000000) >> 24);
			u32_HiWordLoByte = (uint8)((Buff & 0xFF0000) >> 16);
			u32_LoWordHiByte = (uint8)((Buff & 0xFF00) >> 8);
			u32_LoWordLoByte = (uint8)(Buff & 0xFF);
			Can0Node0Txmsgs[1] = (u32_HiWordHiByte << 24) + (u32_HiWordLoByte << 16) + (u32_LoWordHiByte << 8) + (u32_LoWordLoByte);
			break;
		default:
			break;
	}
	CanTx_Index++;
	if(CanTx_Index >= 8)
		CanTx_Index = 0;
	txMsg.dataLengthCode = IfxCan_DataLengthCode_8;
#endif
	txMsg.bufferNumber = 0;
	IfxCan_Can_sendMessage(&g_CanBasic.drivers.canNode[0], &txMsg, Can0Node0Txmsgs);
#if ENABLEALLCAN
if(txMsg.messageId == 0x33)
{
	txMsg.messageId = 0x40;
	IfxCan_Can_sendMessage(&g_CanBasic.drivers.canNode[2], &txMsg, Can0Node0Txmsgs);
	txMsg.messageId = 0x41;
	IfxCan_Can_sendMessage(&g_CanBasic.drivers.canNode[4], &txMsg, Can0Node0Txmsgs);
	txMsg.messageId = 0x42;
	IfxCan_Can_sendMessage(&g_CanBasic.drivers.canNode[1], &txMsg, Can0Node0Txmsgs);
	txMsg.messageId = 0x43;
	IfxCan_Can_sendMessage(&g_CanBasic.drivers.canNode[3], &txMsg, Can0Node0Txmsgs);
}
else if(txMsg.messageId == 0x36)
{
	txMsg.messageId = 0x50;
	IfxCan_Can_sendMessage(&g_CanBasic.drivers.canNode[2], &txMsg, Can0Node0Txmsgs);
	txMsg.messageId = 0x51;
	IfxCan_Can_sendMessage(&g_CanBasic.drivers.canNode[4], &txMsg, Can0Node0Txmsgs);
	txMsg.messageId = 0x52;
	IfxCan_Can_sendMessage(&g_CanBasic.drivers.canNode[1], &txMsg, Can0Node0Txmsgs);
	txMsg.messageId = 0x53;
	IfxCan_Can_sendMessage(&g_CanBasic.drivers.canNode[3], &txMsg, Can0Node0Txmsgs);
}
#endif
}

/** \brief PWN Engineering Test Code.
 *	\Has to use CAN to Enable/Disable
 *	\Duty is controlled by CAN
 */
void PWMEngineerTest(uint8 dutyU, uint8 dutyV, uint8 dutyW)
{
	float dividerU = 0, dividerV = 0, dividerW = 0;
	IfxGtm_Atom_PwmHl *pwmHl = &g_Lb.logicboards.logicBoard_30.driver.inverter.atom.pwmDriver;
    IfxGtm_Atom_Timer *timer = &g_Lb.logicboards.logicBoard_30.driver.inverter.atom.timerDriver;
	Ifx_TimerValue     tOn[3];

	dividerU = (100/(float)dutyU);
	dividerV = (100/(float)dutyV);
	dividerW = (100/(float)dutyW);	

	if(dutyU != 0
		&& dutyV != 0
		&& dutyW != 0)
	{
		IfxGtm_Atom_PwmHl_setMode(pwmHl, Ifx_Pwm_Mode_centerAligned);
	    tOn[0] = (uint32)(IfxGtm_Atom_Timer_getPeriod(timer) / dividerU);
	    tOn[1] = (uint32)(IfxGtm_Atom_Timer_getPeriod(timer) / dividerV);
	    tOn[2] = (uint32)(IfxGtm_Atom_Timer_getPeriod(timer) / dividerW);
	    IfxGtm_Atom_Timer_disableUpdate(timer);
	    IfxGtm_Atom_PwmHl_setOnTime(pwmHl, &tOn[0]);
	    IfxGtm_Atom_Timer_applyUpdate(timer);
	}
	else
	{
		IfxGtm_Atom_PwmHl_setMode(pwmHl, Ifx_Pwm_Mode_centerAligned);
	    tOn[0] = 0;
	    tOn[1] = 0;
	    tOn[2] = 0;
	    IfxGtm_Atom_Timer_disableUpdate(timer);
	    IfxGtm_Atom_PwmHl_setOnTime(pwmHl, &tOn[0]);
	    IfxGtm_Atom_Timer_applyUpdate(timer);
	}
}

/** \brief Handle 1ms interrupt.
 *
 * \isrProvider{ISR_PROVIDER_TIMER_1MS}
 * \isrPriority{ISR_PRIORITY_TIMER_1MS}
 *
 */
void LB30_Isr_Timer_1ms(void)
{
    __enable();
    uint32 i, temp;
    IfxGtm_Atom_Timer_acknowledgeTimerIrq(&logicBoard.driver.timerOneMs);
	
#if 1
    IfxTlf35584_Driver_process(&g_Lb.logicboards.logicBoard_30.driver.tlf35584);

    if (g_App.hwState.hwInitReady)
    {
        Ifx_AnalogInputF32_update(&logicBoard.analogInput.vAna50.input, AnalogInput_getRawResult(&logicBoard.analogInput.vAna50));
        Ifx_AnalogInputF32_update(&logicBoard.analogInput.vDig50.input, AnalogInput_getRawResult(&logicBoard.analogInput.vDig50));
        Ifx_AnalogInputF32_update(&logicBoard.analogInput.vRef50.input, AnalogInput_getRawResult(&logicBoard.analogInput.vRef50));
        Ifx_AnalogInputF32_update(&logicBoard.analogInput.tempBoard.input, AnalogInput_getRawResult(&logicBoard.analogInput.tempBoard));
        Ifx_AnalogInputF32_update(&logicBoard.analogInput.kl30.input, AnalogInput_getRawResult(&logicBoard.analogInput.kl30));
        Ifx_AnalogInputF32_update(&logicBoard.analogInput.motorTemp.input, AnalogInput_getRawResult(&logicBoard.analogInput.motorTemp));

#if 0	// Jovi
        for (i = 0; i < 4; i++)
        {
            Ifx_AnalogInputF32_update(&logicBoard.analogInput.in[i].input, AnalogInput_getRawResult(&logicBoard.analogInput.in[i]));
        }
#endif

        if(g_Lb.stdIf.tle5012->driver)
        {
        	/* Update iGMR over SPI at low rate */
	        IfxStdIf_Pos_update(&logicBoard.stdIf.tle5012);
        }

    }

//    Ifx_CanHandler_processInbox(&g_App.can.handler);
    Ifx_CanHandler_processInbox_Jimmy();

    g_App.can.outbox.configExit = FALSE;
	if (g_App.hwState.hwMode == AppMode_run)
    {
        /* Handle 1ms slot */
    	if(g_App.hwState.specialMode == AppModeSpecial_normal)
    	{
            ECU_slotOneMs();
    	}
    	else
    	{
            HandleSpecialModeOneMs();
    	}
    }
	else if (g_App.hwState.hwMode == AppMode_configuration)
	{
		if (g_App.can.inbox.configExit)
		{
			g_App.hwState.hwModeRequest = AppMode_hwInitialization;
			g_App.can.outbox.configExit = TRUE;
		}
	}
	else if (g_App.hwState.hwMode == AppMode_limitedConfiguration)
	{
    	if (g_App.can.inbox.configExit)
		{
			g_App.hwState.hwModeRequest = AppMode_appInitialization;
			g_App.can.outbox.configExit = TRUE;
		}
	}
	else if (g_App.hwState.hwMode == AppMode_error)
	{
		if (g_App.can.inbox.clearErrorCmd)
	    {
			IfxStdIf_Inverter_clearFlags(g_Lb.stdIf.inverter);

			if (g_Lb.stdIf.positionSensorX[0])
			{
				IfxStdIf_Pos_resetFaults(g_Lb.stdIf.positionSensorX[0]);
			}
			if (g_Lb.stdIf.positionSensorX[1])
			{
				IfxStdIf_Pos_resetFaults(g_Lb.stdIf.positionSensorX[1]);
			}
	    }
	}

	if (g_App.can.inbox.clearErrorCmd)
    {
		if (!g_App.can.outbox.clearError)
		{
			g_App.can.outbox.clearError = TRUE;
	    	g_App.hwState.hwModeRequest = AppMode_appInitialization;
		}
    }
	else
	{
		g_App.can.outbox.clearError = FALSE;
	}

    if (g_App.hwState.hwInitReady)
	{
        ECU_InverterStatus status = ECU_getInverterStatus(0);
	    g_App.can.outbox.error           = status.faultMain;
	    g_App.can.outbox.tempBoardMeas   = status.tempBoard;
	    g_App.can.outbox.tempIgbtMeas[0] = status.igbtTemp[0];
	    g_App.can.outbox.tempIgbtMeas[1] = status.igbtTemp[1];
	    g_App.can.outbox.tempIgbtMeas[2] = status.igbtTemp[2];
	    g_App.can.outbox.tempMotorMeas   = status.motorTemp;
	    g_App.can.outbox.voltageDcMeas   = ECU_getInverterVdc(0);

#if 1
	    /* 20200312 Jimmy created fault information loop */
	    // row_2 : positionSensor1Error | faultGDEN | phase1TopDriverError | phase1BottomDriverError | phase2TopDriverError
	    // row_3 : phase2BottomDriverError | phase3TopDriverError | phase3BottomDriverError | currentSensor1Error | currentSensor2Error
	    // row_4 : currentSensor3Error | faultMain | faultInput | faultLimit | faultIom | faultPBEN
	    // row_5 : Empty
	    temp = 0;
	    temp |= (status.phase2TopDriverError << 0) | (status.phase1BottomDriverError << 2) | (status.phase1TopDriverError << 4) | (status.faultGDEN << 6) | (status.positionSensor1Error << 7);
	    temp |= (status.currentSensor2Error << 8) | (status.currentSensor1Error << 9) | (status.phase3BottomDriverError << 10) | (status.phase3TopDriverError << 12) | (status.phase2BottomDriverError << 14);
	    temp |= (status.faultPBEN << 18) | (status.faultIom << 19) | (status.faultLimit << 20) | (status.faultInput << 21) | (status.faultMain << 22) | (status.currentSensor3Error << 23);
	    g_App.can.outbox.fault = temp;
#endif


	    ECU_getMotorPhaseCurrents(0, &g_App.can.outbox.current[0]);

	    g_App.can.outbox.mechPosR   = g_Lb.stdIf.positionSensorX[1] ? IfxStdIf_Pos_getPosition(g_Lb.stdIf.positionSensorX[1]) : 0;
	    g_App.can.outbox.mechSpeedR   = g_Lb.stdIf.positionSensorX[1] ? IfxStdIf_Pos_radsToRpm(IfxStdIf_Pos_getSpeed(g_Lb.stdIf.positionSensorX[1])) : 0;
#if CFG_CURRENT_SENSOR_TEST == 1
	    __disable();
	    g_App.can.outbox.logCurrent[ECU_MOTOR_INDEX_0][0]   = __float32_to_fixpoint(Ifx_AnalogInputF32_getValue(&logicBoard.analogInput.currents[ECU_MOTOR_INDEX_0][0].input),7);	// Q12.4 (+/-4096A)
	    g_App.can.outbox.logCurrent[ECU_MOTOR_INDEX_0][1]   = __float32_to_fixpoint(Ifx_AnalogInputF32_getValue(&logicBoard.analogInput.currents[ECU_MOTOR_INDEX_0][1].input),7);	// Q12.4 (+/-4096A)
	    g_App.can.outbox.logCurrent[ECU_MOTOR_INDEX_0][2]   = __float32_to_fixpoint(Ifx_AnalogInputF32_getValue(&logicBoard.analogInput.currents[ECU_MOTOR_INDEX_0][2].input),7);	// Q12.4 (+/-4096A)
	    g_App.can.outbox.logCurrent[ECU_MOTOR_INDEX_1][0]   = __float32_to_fixpoint(Ifx_AnalogInputF32_getValue(&logicBoard.analogInput.currents[ECU_MOTOR_INDEX_1][0].input),7);	// Q12.4 (+/-4096A)
	    g_App.can.outbox.logCurrent[ECU_MOTOR_INDEX_1][1]   = __float32_to_fixpoint(Ifx_AnalogInputF32_getValue(&logicBoard.analogInput.currents[ECU_MOTOR_INDEX_1][1].input),7);	// Q12.4 (+/-4096A)
	    g_App.can.outbox.logCurrent[ECU_MOTOR_INDEX_1][2]   = __float32_to_fixpoint(Ifx_AnalogInputF32_getValue(&logicBoard.analogInput.currents[ECU_MOTOR_INDEX_1][2].input),7);	// Q12.4 (+/-4096A)
	    __enable();
	    g_App.can.outbox.logCurrentDiff[0]   = g_App.can.outbox.logCurrent[1][0] - g_App.can.outbox.logCurrent[0][0];	// Q8.8 (+/-256A)
#endif
	}


    g_App.can.outbox.ecuState        = g_App.hwState.hwMode;
    Ifx_CanHandler_processOutbox_Jimmy();
//    Ifx_CanHandler_processOutbox(&g_App.can.handler);
    if (g_App.can.messageStat1->rxTxCount > 0)
    { /* Reset the ECU reset flag*/
    	g_App.can.outbox.ecuReset = FALSE;
    }
#endif
}


/** \brief Handle 10ms interrupt.
 *
 * \isrProvider{ISR_PROVIDER_TIMER_10MS}
 * \isrPriority{ISR_PRIORITY_TIMER_10MS}
 *
 */
void LB30_Isr_Timer_10ms(void)
{	
    __enable();
    IfxGtm_Atom_Timer_acknowledgeTimerIrq(&logicBoard.driver.timerTenMs);

    TIME_CNT_10ms++;
	
#if 1
    if (g_App.hwState.hwInitReady)
    {
    	/* Update Vdc every 10ms.
    	 * The driver board with EiceSence takes minimum to 106 us @1.8Mhz clock to update the Vdc. Measurements shows 130us
         */
        Lb_BoardType boardType = AppDbStdIf_getBoardVersion(logicBoard.driverBoardStdif)->boardType;
        if (
        		(boardType == Lb_BoardType_HybridKit_DriverBoardHp2)
        		)
        {
        	Ifx_AnalogInputF32_update(&logicBoard.analogInput.igbtTemp[0].input, IfxGtm_Tim_In_getPeriodSecond(&logicBoard.driver.inverter.igbtTemp[0]));
            Ifx_AnalogInputF32_update(&logicBoard.analogInput.igbtTemp[1].input, IfxGtm_Tim_In_getPeriodSecond(&logicBoard.driver.inverter.igbtTemp[1]));
            Ifx_AnalogInputF32_update(&logicBoard.analogInput.igbtTemp[2].input, IfxGtm_Tim_In_getPeriodSecond(&logicBoard.driver.inverter.igbtTemp[2]));
        }
        else if (
        		(boardType == Lb_BoardType_HybridKit_DriverBoardHpDrive)
        		)
        {
        	Ifx_AnalogInputF32_update(&logicBoard.analogInput.vDc.input, AnalogInput_getRawResult(&logicBoard.analogInput.vDc));

        	Ifx_AnalogInputF32_update(&logicBoard.analogInput.igbtTemp[0].input, IfxGtm_Tim_In_getPeriodSecond(&logicBoard.driver.inverter.igbtTemp[0]));
            Ifx_AnalogInputF32_update(&logicBoard.analogInput.igbtTemp[1].input, IfxGtm_Tim_In_getPeriodSecond(&logicBoard.driver.inverter.igbtTemp[1]));
            Ifx_AnalogInputF32_update(&logicBoard.analogInput.igbtTemp[2].input, IfxGtm_Tim_In_getPeriodSecond(&logicBoard.driver.inverter.igbtTemp[2]));
        }
        else if (
        		(boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
        		)
        {
        	if (g_App.hwState.vdcMeasurementEnabled)
        	{
        		/* FIXME Reading the VDC value make use of the QSPI, in case the QSPI is locked value updated is ignored  */
            	Ifx_AnalogInputF32_update(&logicBoard.analogInput.vDc.input, AppDbStdIf_getVdc(logicBoard.driverBoardStdif));
        	}

        	Ifx_AnalogInputF32_update(&logicBoard.analogInput.igbtTemp[0].input, IfxGtm_Tim_In_getPeriodSecond(&logicBoard.driver.inverter.igbtTemp[0]));
            Ifx_AnalogInputF32_update(&logicBoard.analogInput.igbtTemp[1].input, IfxGtm_Tim_In_getPeriodSecond(&logicBoard.driver.inverter.igbtTemp[1]));
            Ifx_AnalogInputF32_update(&logicBoard.analogInput.igbtTemp[2].input, IfxGtm_Tim_In_getPeriodSecond(&logicBoard.driver.inverter.igbtTemp[2]));

        }
        else if (boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
        {
        	/* FIXME read EiceSense ADC */
        	if (g_App.hwState.vdcMeasurementEnabled)
        	{
        		/* FIXME Reading the VDC and temp value make use of the QSPI, in case the QSPI is locked value updated is ignored  */
            	Ifx_AnalogInputF32_update(&logicBoard.analogInput.vDc.input, AppDbStdIf_getVdc(logicBoard.driverBoardStdif));

            	float32 tempArray[3];
            	AppDbStdIf_getIgbtTemp(logicBoard.driverBoardStdif, tempArray);
            	Ifx_AnalogInputF32_update(&logicBoard.analogInput.igbtTemp[0].input, tempArray[0]);
            	Ifx_AnalogInputF32_update(&logicBoard.analogInput.igbtTemp[1].input, tempArray[1]);

        	}
        }
        else
        {
        }

		/* Get ADC Raw Value of Voltage */		
		result_vANA50 = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.vAna50.input);		
		result_vRef50 = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.vRef50.input);
		result_vDig50 = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.vDig50.input);
		result_vEncoder = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.vEncoder.input);
		result_kl30 = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.kl30.input);
		result_vRef25 = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.vRef25.input);
		result_vHVILIn = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.vHVILIn.input);
		result_vHVILOut = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.vHVILOut.input);
		
		result_vDCPhaseV = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.vDc.input);
		result_vDCPhaseU = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.vDcPhaseU.input);

		/* Get ADC Raw Value of Current */
		result_iVdc = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.iVdc.input);
		result_iPhaseU = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.currents[ECU_MOTOR_INDEX_0][0].input);
		result_iPhaseV = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.currents[ECU_MOTOR_INDEX_0][1].input);
		result_iPhaseW = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.currents[ECU_MOTOR_INDEX_0][2].input);
		
		/* Get ADC Raw Value of Temperature */
		tempMotor = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.motorTemp.input);
		result_tempWater = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.tempWater.input);
		tempCTRLBoard = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.tempBoard.input);
		result_tempDRVBoard = AnalogInput_getRawResult(&g_Lb.logicboards.logicBoard_30.analogInput.tempDRVBoard.input);
    }

    if (g_App.hwState.hwMode == AppMode_run)
    {
        /* Handle 10ms slot */
    	if(g_App.hwState.specialMode == AppModeSpecial_normal)
    	{
            ECU_slotTenMs();
    	}
    	else
    	{
            HandleSpecialModeTenMs();
    	}

		if(g_App.hwState.specialMode == AppModeSpecial_normal
			&& Flag_PWMEngineerTest)
		{
			PWMEngineerTest(DutyTestU, DutyTestV, DutyTestW);
		}
    }
#endif
}


/** \brief Handle M0 period interrupt.
 *
 * \isrProvider{ISR_PROVIDER_M0_PWMHL}
 * \isrPriority{ISR_PRIORITY_M0_PWMHL_PERIOD}
 *
 * - This interrupt is raised each time the PWM timer match the period
 */
void LB30_Isr_M0_Period(void)
{
    __enable();

    if (g_App.hwState.hwMode == AppMode_run)
    {
    	if(g_App.hwState.specialMode == AppModeSpecial_normal)
    	{
            ECU_slotPwmPeriodStart(0);
    	}
    }
}


/** \brief Handle M0 trigger interrupt.
 *
 * \isrProvider{ISR_PROVIDER_M0_PWMHL}
 * \isrPriority{ISR_PRIORITY_M0_PWMHL_TRIGGER}
 *
 * - This interrupt is raised each time the PWM timer match the trigger point
 */
void LB30_Isr_M0_Trigger(void)
{
    __enable();

    if (g_App.hwState.hwMode == AppMode_run)
    {
    	if(g_App.hwState.specialMode == AppModeSpecial_normal)
    	{
            ECU_slotTriggerPoint(0);
    	}
    }
}


/** \brief Handle the end of conversion for phase current  interrupt
 *
 * \isrProvider{ISR_PROVIDER_ADC_M0}
 * \isrPriority{ISR_PRIORITY_ADC_M0}
 *
 * This interrupt is triggered when the ADC finishes conversion of the 3-phase currents.
 * This In this interrupt context, the following operations are performed:
 *
 */
void LB30_Isr_Adc_M0(void)
{
    uint32 i;

    if (g_App.hwState.hwInitReady)
    {
    	boolean updatePos0 = TRUE;
    	boolean updatePos1 = TRUE;
    	// 20200107 update AD2S1210 should execute "&Ad2s1210_Serial_update" //
    	if ((g_Lb.stdIf.positionSensorX[0] == g_Lb.stdIf.dsadcRdc[0]) || (g_Lb.stdIf.positionSensorX[0] == g_Lb.stdIf.dsadcRdc[1]))
    	{
            IfxStdIf_Pos_update(g_Lb.stdIf.positionSensorX[0]);/* FIXME remove dependency to g_Lb*/
            updatePos0 = FALSE;
    	}

    	if ((g_Lb.stdIf.positionSensorX[1] == g_Lb.stdIf.dsadcRdc[0]) || (g_Lb.stdIf.positionSensorX[1] == g_Lb.stdIf.dsadcRdc[1]))
    	{
            IfxStdIf_Pos_update(g_Lb.stdIf.positionSensorX[1]);/* FIXME remove dependency to g_Lb*/
            updatePos1 = FALSE;
    	}


    	__enable();// DSADC RDC requirement: enabling other interrupt shall be done after IfxDsadc_Rdc_update() to avoid interruption from LB30_Isr_Dsadc_rdc0()
    	if (g_Lb.stdIf.positionSensorX[0] && updatePos0)
    	{
            IfxStdIf_Pos_update(g_Lb.stdIf.positionSensorX[0]);
    	}

    	if (g_Lb.stdIf.positionSensorX[1] && updatePos1)
    	{
            IfxStdIf_Pos_update(g_Lb.stdIf.positionSensorX[1]);
    	}

        for (i = 0; i < ECU_PHASE_PER_MOTOR_COUNT; i++)
        {
            Ifx_AnalogInputF32_update(&logicBoard.analogInput.currents[ECU_MOTOR_INDEX_0][i].input, AnalogInput_getRawResult(&logicBoard.analogInput.currents[ECU_MOTOR_INDEX_0][i]));
#if CFG_CURRENT_SENSOR_TEST == 1
            Ifx_AnalogInputF32_update(&logicBoard.analogInput.currents[ECU_MOTOR_INDEX_1][i].input, AnalogInput_getRawResult(&logicBoard.analogInput.currents[ECU_MOTOR_INDEX_1][i]));
#endif
        }
    }
    else
    {
        __enable();
    }

    if (g_App.hwState.hwMode == AppMode_run)
    {
    	if(g_App.hwState.specialMode == AppModeSpecial_normal)
    	{
    		if(Flag_PWMEngineerTest == 0)
            	ECU_slotEndOfPhaseCurrentConversion(0);
    	}
    	else
    	{
            HandleSpecialModeEndOfPhaseCurrentConversion();
    	}
    }
    else if ((g_App.hwState.hwMode == AppMode_shutdown) && g_App.hwState.hwInitReady)
    {
    	if(g_App.hwState.specialMode == AppModeSpecial_normal)
    	{
            ECU_slotEndOfPhaseCurrentConversion(0);
    	}
    	else
    	{
            HandleSpecialModeEndOfPhaseCurrentConversion();
    	}
    }
    else if (g_App.hwState.hwMode == AppMode_hwInitialization)
    {
    	if (g_App.hwState.hwInitReady)
    	{ /* Complete hardware initialization done, ready for self test */
    		if(logicBoard.selftest.running)
    		{
        		if (!logicBoard.selftest.pwmEnabled)
        		{
            	    LB30_Inverter_setPwmOff(&logicBoard);
            	    logicBoard.selftest.running = FALSE;
        		}
        		else
        		{
            		LB30_Inverter_setPwmFaultInjection(&logicBoard, logicBoard.selftest.duty, logicBoard.selftest.dutyFaulty);
        		}

    		}
    		else
    		{
        		if (logicBoard.selftest.pwmEnabled)
        		{
            	    LB30_Inverter_setPwmOn(&logicBoard, Ifx_Pwm_Mode_centerAligned);
            	    logicBoard.selftest.running = TRUE;
        		}

    		}

    	}
    }

    Ifx_Osci_step(&g_App.osci);
}


/** \brief Handle the IGBT temperature measurement for phase U
 *
 * \isrProvider{ISR_PROVIDER_IGBT_TEMP}
 * \isrPriority{ISR_PRIORITY_IGBT_TEMPU}
 *
 */
void LB30_Isr_M0_IgbtTempU(void)
{
    IfxGtm_Tim_In_onIsr(&logicBoard.driver.inverter.igbtTemp[0]);
}


/** \brief Handle the IGBT temperature measurement for phase V
 *
 * \isrProvider{ISR_PROVIDER_IGBT_TEMP}
 * \isrPriority{ISR_PRIORITY_IGBT_TEMPV}
 *
 */
void LB30_Isr_M0_IgbtTempV(void)
{
    IfxGtm_Tim_In_onIsr(&logicBoard.driver.inverter.igbtTemp[1]);
}


/** \brief Handle the IGBT temperature measurement for phase W
 *
 * \isrProvider{ISR_PROVIDER_IGBT_TEMP}
 * \isrPriority{ISR_PRIORITY_IGBT_TEMPW}
 *
 */
void LB30_Isr_M0_IgbtTempW(void)
{
    IfxGtm_Tim_In_onIsr(&logicBoard.driver.inverter.igbtTemp[2]);
}


/** \brief Handle the encoder zero interrupt
 *
 * \isrProvider{ISR_PROVIDER_ENCODER_GPT12}
 * \isrPriority{ISR_PRIORITY_ENCODER_GPT12}
 *
 * This interrupt is raised each time the encoder zero event occurs.
 */
void LB30_Isr_Gpt12_encoder(void)
{
    __enable();
    IfxStdIf_Pos_onZeroIrq(&logicBoard.stdIf.encoder);
}


/** \brief Handle the Ssc transmit interrupt
 *
 * \isrProvider{ISR_PROVIDER_QSPI2}
 * \isrPriority{ISR_PRIORITY_QSPI2_TX}
 *
 * This interrupt is raised by the Ssc on each transmit event.
 */
void LB30_Isr_Qspi2_tx(void)
{
    __enable();
    /* FIXME implement StdIf */
    IfxQspi_SpiMaster_isrTransmit(&logicBoard.driver.qspi2);
}


/** \brief Handle the Ssc receive interrupt
 *
 * \isrProvider{ISR_PROVIDER_QSPI2}
 * \isrPriority{ISR_PRIORITY_QSPI2_RX}
 *
 * This interrupt is raised by the Ssc on each transmit event.
 */
void LB30_Isr_Qspi2_rx(void)
{
    __enable();
    IfxQspi_SpiMaster_isrReceive(&logicBoard.driver.qspi2);
}


/** \brief Handle the Ssc error interrupt
 *
 * \isrProvider{ISR_PROVIDER_QSPI2}
 * \isrPriority{ISR_PRIORITY_QSPI2_ERR}
 *
 * This interrupt is raised by the Ssc on each transmit event.
 */
void LB30_Isr_Qspi2_err(void)
{
    __enable();
    IfxQspi_SpiMaster_isrError(&logicBoard.driver.qspi2);
}


/** \brief Handle the Ssc transmit interrupt
 *
 * \isrProvider{ISR_PROVIDER_QSPI5}
 * \isrPriority{ISR_PRIORITY_QSPI5_TX}
 *
 * This interrupt is raised by the Ssc on each transmit event.
 */
void LB30_Isr_Qspi5_tx(void)
{
    __enable();
    /* FIXME implement StdIf */
    IfxQspi_SpiMaster_isrTransmit(&logicBoard.driver.qspi5);
}


/** \brief Handle the Ssc receive interrupt
 *
 * \isrProvider{ISR_PROVIDER_QSPI5}
 * \isrPriority{ISR_PRIORITY_QSPI5_RX}
 *
 * This interrupt is raised by the Ssc on each transmit event.
 */
void LB30_Isr_Qspi5_rx(void)
{
    __enable();
    IfxQspi_SpiMaster_isrReceive(&logicBoard.driver.qspi5);
}


/** \brief Handle the Ssc error interrupt
 *
 * \isrProvider{ISR_PROVIDER_QSPI5}
 * \isrPriority{ISR_PRIORITY_QSPI5_ERR}
 *
 * This interrupt is raised by the Ssc on each transmit event.
 */
void LB30_Isr_Qspi5_err(void)
{
    __enable();
    IfxQspi_SpiMaster_isrError(&logicBoard.driver.qspi5);
}


/** \brief andle the DSADC resolver interrupt
 *
 * \isrProvider{ISR_PROVIDER_DSADC_RDC0}
 * \isrPriority{ISR_PRIORITY_DSADC_RDC0}
 *
 */
void LB30_Isr_Dsadc_rdc0(void)
{
    if((g_Lb.stdIf.positionSensorX[0] == g_Lb.stdIf.dsadcRdc[0]) || (g_Lb.stdIf.positionSensorX[0] == g_Lb.stdIf.dsadcRdc[1]))
    {
        IfxStdIf_Pos_onEventA(g_Lb.stdIf.positionSensorX[0]);
    }

    if((g_Lb.stdIf.positionSensorX[1] == g_Lb.stdIf.dsadcRdc[0]) || (g_Lb.stdIf.positionSensorX[1] == g_Lb.stdIf.dsadcRdc[1]))
    {
        IfxStdIf_Pos_onEventA(g_Lb.stdIf.positionSensorX[1]);
    }
}

