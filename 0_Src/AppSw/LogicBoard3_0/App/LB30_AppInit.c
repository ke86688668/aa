/**
 * \file LB30_AppInit.c
 * \brief Logic board 2.0 initialization
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include "Configuration.h"
#include "SysSe/Comm/Ifx_Console.h"
#include "Common/Main.h"
#include "Can/Can/IfxCan_Can.h"
#include "Evadc/Adc/IfxEvadc_Adc.h"
#include "Gtm/Trig/IfxGtm_Trig.h"

#include "LogicBoard3_0/App/LB30.h"
#include "LogicBoard3_0/Config/LB30_Interrupts.h"
#include "LogicBoard3_0/App/LB30_AppInit.h"
#include "Common/TempIgbt_R2fV1.h"
#include "Common/TempIgbt_R2fV2.h"
#include "Common/TempIgbt_R2fV3.h"
#include "Common/TempStator_KTY84_130.h"
#include "LogicBoard3_0/Config/LB30_Configuration.h"
#include "SysSe/Ext/Tlf35584/Std/IfxTlf35584.h"
#include "Common/AppInterface.h"

#include "Smu/Std/IfxSmu.h"
#include  "Iom/Driver/IfxIom_Driver.h"
#include "Common/AppSetupWizard.h"
#include "Common/AppCan.h"
#include "Common/AppEvadc.h"

/* Jimmy Create the Global values for PB_EN status */
extern boolean Dio_PB_EN_Status;
/* end of created */


void watch(IfxTlf35584_Driver *driver)
{
    IfxTlf35584_Driver_invalidateCache(driver);
    IfxTlf35584_Driver_readSync(driver);
}

boolean LB30_AppInit_smu(LB30 *board)
{
    boolean           result = TRUE;

//    App_printStatusTitle("Initializing SMU FSP");


    if (board->configuration.safety.iom.enabled)
    {
    	IfxSmu_unlockConfigRegisters();	//    	IfxSmu_unlock(&MODULE_SMU); 20191212 Jimmy modified //
    	/* IOM SMU related part need to be initialized before SMU lock */
    	if (board->configuration.safety.iom.fspOnFaultEnabled)
    	{
    	    /* Enable SMU FSP event from IOM (ALM2[26]). Upon alarm event, an FSP is raised */
    		IfxSmu_activateFSP();	// 20191212 Jimmy modified //
//    	    IfxSmu_enableAlarmFsp(&MODULE_SMU, IfxSmu_Alarm_IomPinMismatchIndication, TRUE);
//            Ifx_Console_print(" - IOM FSP triggering enabled"ENDL);
    	}
    	if (board->configuration.safety.iom.nmiOnFaultEnabled)
    	{
    		/* Enable SMU NMI for IOM (ALM2[26])
    		 *
        	 * IOM generates an NMI through the SMU. See UM TC27x C "7.2.4 NMI Trap Generation"
        	 * The trap is generated to all CPUs but handled by CPU1
        	 */
    		IfxSmu_setAlarmAction(IfxSmu_Alarm_IOM_Pin_MismatchIndication,IfxSmu_InternalAlarmAction_nmi);		// 20191212 Jimmy Chiang Modified
//    		IfxSmu_setAlarmConfig(&MODULE_SMU, IfxSmu_Alarm_IomPinMismatchIndication, IfxSmu_AlarmConfig_nmi);
//            Ifx_Console_print(" - IOM NMI triggering enabled"ENDL);

            {  // Enable SMU trap
                uint16 endinit_pw;
        		MODULE_SCU.TRAPCLR.B.SMUT = 1; // Clear the trap status flag before enabling to avoid unintended traps
                endinit_pw                     = IfxScuWdt_getCpuWatchdogPassword();
                IfxScuWdt_clearCpuEndinit(endinit_pw);
                MODULE_SCU.TRAPDIS0.B.CPU0SMUT = 0;		// 20191212 Jimmy Chiang Modified
                MODULE_SCU.TRAPDIS0.B.CPU1SMUT = 0;
                MODULE_SCU.TRAPDIS0.B.CPU2SMUT = 0;
//                MODULE_SCU.TRAPDIS0.B.CPU3SMUT = 0;
//                MODULE_SCU.TRAPDIS1.B.CPU4SMUT = 0;
//                MODULE_SCU.TRAPDIS1.B.CPU5SMUT = 0;
//        		MODULE_SCU.TRAPDIS.B.SMUT = 0; // Enable Trap for SMU
                IfxScuWdt_setCpuEndinit(endinit_pw);
            }
    	}
    	IfxSmu_temporaryLockConfigRegisters();	//	IfxSmu_lock(&MODULE_SMU);
    }


    if (board->configuration.safety.fsp.enabled)
    {
        IfxSmu_configFsp config;

        IfxSmu_initFspConfig(&config);

        config.emergencyStopEnabled = FALSE;
    	config.enableFaultToRunStateTransition = FALSE;
    	config.faultFreeStateFrequencyMin = 1/50.1e-6; /* tDET,LF */
    	config.faultFreeStateFrequencyMax = 1/11.1e-6; /* tDET,HF */
    	config.faultStateTime = 1e-3;
    	config.mode = IfxSmu_FspMode_TimeSwitchingProtocol;

    	result &= IfxSmu_initFsp(&MODULE_SMU, &config);
    }

    IfxSmu_lockConfigRegisters();	//	IfxSmu_lockForever(&MODULE_SMU);

	if (board->configuration.safety.fsp.enabled)
	{
    	result &= IfxSmu_releaseFSP();	//	IfxSmu_releaseFsp(&MODULE_SMU);
//        Ifx_Console_print(" - Minimal fault state time: %fs"ENDL, IfxSmu_getFspFaultStateTime(&MODULE_SMU));
//        Ifx_Console_print(" - Fault free state frequency: %fHz"ENDL, IfxSmu_getFspFaultFreeFrequency(&MODULE_SMU));
	}

	result &= IfxSmu_activateRunState(); //	IfxSmu_start(&MODULE_SMU);

//        App_printStatus(result);

    return result;
}

boolean LB30_AppInit_tlf35584(LB30 *board)
{
    IfxTlf35584_Driver      *driver;
    boolean           result = TRUE;
//    App_printStatusTitle("Initializing TLF35584");

//    Ifx_Console_print(" (");
    {   /* Initialze the serial interface */
        IfxQspi_SpiMaster_ChannelConfig spiChannelConfig;
        IfxQspi_SpiMaster_initChannelConfig(&spiChannelConfig, &board->driver.qspi2);
        spiChannelConfig.base.baudrate             = 10e6;
        spiChannelConfig.base.mode.enabled         = TRUE;
        spiChannelConfig.base.mode.autoCS          = 1;
        spiChannelConfig.base.mode.loopback        = FALSE;
        spiChannelConfig.base.mode.clockPolarity   = SpiIf_ClockPolarity_idleLow;
        spiChannelConfig.base.mode.shiftClock      = SpiIf_ShiftClock_shiftTransmitDataOnTrailingEdge;
        spiChannelConfig.base.mode.dataHeading     = SpiIf_DataHeading_msbFirst;
        spiChannelConfig.base.mode.dataWidth       = 16;
        spiChannelConfig.base.mode.csActiveLevel   = Ifx_ActiveState_low;
        spiChannelConfig.base.mode.csLeadDelay     = SpiIf_SlsoTiming_2;
        spiChannelConfig.base.mode.csTrailDelay    = SpiIf_SlsoTiming_1;
        spiChannelConfig.base.mode.csInactiveDelay = SpiIf_SlsoTiming_7;
        spiChannelConfig.base.mode.parityCheck     = FALSE;             /* FIXME use HW parity */
        spiChannelConfig.base.mode.parityMode      = Ifx_ParityMode_even;
        spiChannelConfig.base.errorChecks.baudrate = FALSE;
        spiChannelConfig.base.errorChecks.phase    = FALSE;
        spiChannelConfig.base.errorChecks.receive  = FALSE;
        spiChannelConfig.base.errorChecks.transmit = FALSE;

        spiChannelConfig.sls.output.pin            = &IfxQspi2_SLSO0_P15_2_OUT;
        spiChannelConfig.sls.output.mode           = IfxPort_OutputMode_pushPull;
        spiChannelConfig.sls.output.driver         = IfxPort_PadDriver_cmosAutomotiveSpeed1;

        result                                    &= IfxQspi_SpiMaster_initChannel(&board->driver.sscTlf35584, &spiChannelConfig) == SpiIf_Status_ok;

//        Ifx_Console_print("QSPI%d, CS%d, %9.0f baud", IfxQspi_getIndex(board->driver.qspi2.qspi), spiChannelConfig.sls.output.pin->slsoNr, spiChannelConfig.base.baudrate);
    }

    IfxTlf35584_Driver_Config config;
    IfxTlf35584_Driver_initConfig(&config);
    config.hal.channel = &board->driver.sscTlf35584.base;

    { //TLF35584 interrupt on ESR1
        uint16 endinit_pw;
    	// Set ESR1 as input
    	// page 502
    	MODULE_SCU.IOCR.B.PC0 = 0; // input no pull up/down FIXME check/request iLLD API
    	/*
    	 * ESR1 generates an NMI thought the SCU (Trap). See UM TC27x C "7.2.4 NMI Trap Generation"
    	 * The trap is generated to all CPUs but handled by CPU1
    	 */

		MODULE_SCU.TRAPCLR.B.ESR1T = 1; // Clear the trap status flag before enabling to avoid unintended traps

        endinit_pw                     = IfxScuWdt_getCpuWatchdogPassword();
        IfxScuWdt_clearCpuEndinit(endinit_pw);
        MODULE_SCU.TRAPDIS0.B.CPU0ESR0T = 0;	// 20191212 Jimmy Modified
        MODULE_SCU.TRAPDIS0.B.CPU1ESR0T = 0;
        MODULE_SCU.TRAPDIS0.B.CPU2ESR0T = 0;
//        MODULE_SCU.TRAPDIS0.B.CPU3ESR0T = 0;
//        MODULE_SCU.TRAPDIS1.B.CPU4ESR0T = 0;
//        MODULE_SCU.TRAPDIS1.B.CPU5ESR0T = 0;
//		MODULE_SCU.TRAPDIS.B.ESR1T = 0; // Enable Trap for ESR1
        IfxScuWdt_setCpuEndinit(endinit_pw);
    }

    if (board->boardVersion->boardVersion == LB_BoardVersion_HybridKit_LogicBoard_3_0)
    {
        config.hal.wdi = (IfxPort_Pin) {&MODULE_P02, 11};
    }
    else
    {
        config.hal.wdi = (IfxPort_Pin) {&MODULE_P02, 11};
    }


    config.watchdogCycleTime = IfxTlf35584_WatchdogCycleTimeStatus_100us;/* FIXME make this configurable? */

    config.windowWatchdog.enabled = board->configuration.safety.windowWatchdog.enabled;
    config.windowWatchdog.useSpiTrigger = !board->configuration.safety.windowWatchdog.useWdiPin;
    config.windowWatchdog.openWindowTime = board->configuration.safety.windowWatchdog.openWindowTime;
    config.windowWatchdog.closedWindowTime = board->configuration.safety.windowWatchdog.closeWindowTime;

    config.functionalWatchdog.enabled = board->configuration.safety.functionalWatchdog.enabled;
    config.functionalWatchdog.heartbeatTimerPeriod = board->configuration.safety.functionalWatchdog.heartbeatTimerPeriod;
    config.functionalWatchdog.servicePeriod = board->configuration.safety.functionalWatchdog.servicePeriod;

    config.errPinMonitoring.enabled = board->configuration.safety.fsp.enabled;



    driver  = &board->driver.tlf35584;
    result &= IfxTlf35584_Driver_init(driver, &config);
/*
    Ifx_Console_print(", WDI=P%d.%d)"ENDL
        , IfxPort_getIndex(config.hal.wdi.port), config.hal.wdi.pinIndex
        );
*/
    /* FIXME add support for SSx */
    /* Go to normal mode before timeout */
    /* TLF35584 INIT timer timeout tINIT=550ms min */
    // To stop the INIT timer, 3 conditions are required:
    // - Valid SPI communication received from �C
    // - Watchdog(s) serviced once according to default configuration or according to reconfiguration
    // - ERR monitoring serviced properly (minimum 3 periods provided) or configured to be OFF

    wait(TimeConst_10us*6); /* Data sheet says wait min 60us for the configuration change to take effect. FIXME required?  FIXME Feedback: why such long delay ? */

    /* Enable Vref 5.0 */
    /* Enable Vana 5.0 */
    if (!IfxTlf35584_Driver_goToNormal(driver, TRUE, TRUE, TRUE, TRUE))
    {
//        Ifx_Console_print(" - Error entering NORMAL mode"ENDL);
    	result = FALSE;
    }

    boolean enabled;

    enabled = IfxTlf35584_isWindowWatchdogEnabled(driver, &result);
/*
    if (enabled)
    {
        Ifx_Console_print(" - Window watchdog: enabled, closed window = %fms, opened window = %fms"ENDL, (IfxTlf35584_Driver_getWindowWatchdogClosedWindow(driver)*1e3), (IfxTlf35584_Driver_getWindowWatchdogOpenedWindow(driver)*1e3));
    }
    else
    {
        Ifx_Console_print(" - Window watchdog: disabled"ENDL);
    }
*/
    enabled = IfxTlf35584_isFunctionalWatchdogEnabled(driver, &result);
/*
    if (enabled)
    {
        Ifx_Console_print(" - Functional watchdog: enabled, service period = %fms"ENDL, (IfxTlf35584_Driver_getFunctionalWatchdogPeriod(driver)*1e3));
    }
    else
    {
        Ifx_Console_print(" - Functional watchdog: disabled"ENDL);
    }
*/
    enabled = IfxTlf35584_isRequestErrPinMonitorEnabled(driver, &result);
//    Ifx_Console_print(" - Err : %s"ENDL, (enabled ? "enabled" : "disabled"));

//    App_printStatus(result);

    return result;
}


boolean LB30_AppInit_Asc0(LB30 *board, boolean primaryConsole)
{
    boolean              result = TRUE;

    /** - Standard IO and main CONSOLE */
    IfxAsclin_Asc_Config ascConfig;
    IfxAsclin_Asc_initModuleConfig(&ascConfig, &MODULE_ASCLIN0);
    ascConfig.baudrate.baudrate             = CFG_ASC0_BAUDRATE;
    ascConfig.baudrate.oversampling         = IfxAsclin_OversamplingFactor_16;
    ascConfig.bitTiming.medianFilter        = IfxAsclin_SamplesPerBit_three;
    ascConfig.bitTiming.samplePointPosition = IfxAsclin_SamplePointPosition_8;
    ascConfig.frame.parityBit               = FALSE;
    ascConfig.frame.parityType              = IfxAsclin_ParityType_even;
    ascConfig.interrupt.rxPriority          = ISR_PRIORITY(INTERRUPT_ASC0_RX);
    ascConfig.interrupt.txPriority          = ISR_PRIORITY(INTERRUPT_ASC0_TX);
    ascConfig.interrupt.erPriority          = ISR_PRIORITY(INTERRUPT_ASC0_EX);
    ascConfig.interrupt.typeOfService       = ISR_PROVIDER_ASC0;
    IfxAsclin_Asc_Pins ascPins = {
        .cts       = NULL_PTR,
        .ctsMode   = IfxPort_InputMode_noPullDevice,
        .rx        = &IfxAsclin0_RXD_P33_10_IN,		//IfxAsclin0_RXA_P14_1_IN,
        .rxMode    = IfxPort_InputMode_noPullDevice,
        .rts       = NULL_PTR,
        .rtsMode   = IfxPort_OutputMode_pushPull,
        .tx        = &IfxAsclin0_TX_P33_9_OUT,		//IfxAsclin0_TX_P14_0_OUT,
        .txMode    = IfxPort_OutputMode_pushPull,
        .pinDriver = IfxPort_PadDriver_cmosAutomotiveSpeed1
    };
    ascConfig.pins           = &ascPins;
    ascConfig.rxBuffer       = &board->asc0Buffer.rx;
    ascConfig.txBuffer       = &board->asc0Buffer.tx;
    ascConfig.txBufferSize   = CFG_ASC0_TX_BUFFER_SIZE + CFG_CONSOLE_TX_BUFFER_SIZE;//(primaryConsole ? CFG_CONSOLE_TX_BUFFER_SIZE : 0);
    ascConfig.rxBufferSize   = CFG_ASC0_RX_BUFFER_SIZE;
    ascConfig.dataBufferMode = Ifx_DataBufferMode_normal;
    result                   = IfxAsclin_Asc_initModule(&board->driver.asc0, &ascConfig) == IfxAsclin_Status_noError;

    /* Connect the standard asc interface to the device driver*/
    IfxAsclin_Asc_stdIfDPipeInit(&board->stdIf.asc0, &board->driver.asc0);

    return result;
}


boolean LB30_AppInit_Asc1(LB30 *board)
{
    boolean              result = TRUE;

    /** - Standard IO and main CONSOLE */
    IfxAsclin_Asc_Config ascConfig;
    IfxAsclin_Asc_initModuleConfig(&ascConfig, &MODULE_ASCLIN3);
    ascConfig.baudrate.baudrate             = CFG_ASC1_BAUDRATE;
    ascConfig.baudrate.oversampling         = IfxAsclin_OversamplingFactor_16;
    ascConfig.bitTiming.medianFilter        = IfxAsclin_SamplesPerBit_three;
    ascConfig.bitTiming.samplePointPosition = IfxAsclin_SamplePointPosition_8;
    ascConfig.frame.parityBit               = FALSE;
    ascConfig.frame.parityType              = IfxAsclin_ParityType_even;
    ascConfig.interrupt.rxPriority          = ISR_PRIORITY(INTERRUPT_ASC1_RX);
    ascConfig.interrupt.txPriority          = ISR_PRIORITY(INTERRUPT_ASC1_TX);
    ascConfig.interrupt.erPriority          = ISR_PRIORITY(INTERRUPT_ASC1_EX);
    ascConfig.interrupt.typeOfService       = ISR_PROVIDER_ASC1;
    IfxAsclin_Asc_Pins ascPins = {
        .cts       = NULL_PTR,
        .ctsMode   = IfxPort_InputMode_noPullDevice,
        .rx        = &IfxAsclin3_RXC_P20_3_IN,
        .rxMode    = IfxPort_InputMode_noPullDevice,
        .rts       = NULL_PTR,
        .rtsMode   = IfxPort_OutputMode_pushPull,
        .tx        = &IfxAsclin3_TX_P00_1_OUT,
        .txMode    = IfxPort_OutputMode_pushPull,
        .pinDriver = IfxPort_PadDriver_cmosAutomotiveSpeed1
    };
    ascConfig.pins           = &ascPins;
    ascConfig.rxBuffer       = &board->asc1Buffer.rx;
    ascConfig.txBuffer       = &board->asc1Buffer.rx;
    ascConfig.txBufferSize   = CFG_ASC1_TX_BUFFER_SIZE;
    ascConfig.rxBufferSize   = CFG_ASC1_RX_BUFFER_SIZE;
    ascConfig.dataBufferMode = Ifx_DataBufferMode_normal;
    result                   = IfxAsclin_Asc_initModule(&board->driver.asc1, &ascConfig) == IfxAsclin_Status_noError;

    /* Connect the standard asc interface to the device driver*/
    IfxAsclin_Asc_stdIfDPipeInit(&board->stdIf.asc1, &board->driver.asc1);

    board->driver.asc1Rs232Select = (IfxPort_Pin) {&MODULE_P23, 0};
    Pin_setMode(&board->driver.asc1Rs232Select, IfxPort_Mode_outputPushPullGeneral);
#if CFG_ASC1_USE_RS232
    /* High is RS232, low is RS845 */
    Pin_setState(&board->driver.asc1Rs232Select, IfxPort_State_high);
#else
    Pin_setState(&board->driver.asc1Rs232Select, IfxPort_State_low);
#endif
/*
    App_printStatusTitle("Initializing ASLIN");

    Ifx_Console_print("%d (%9.0f baud, RX=P%d.%d, TX=P%d.%d)"
        , IfxAsclin_getIndex(ascConfig.asclin)
        , ascConfig.baudrate.baudrate
        , IfxPort_getIndex(ascConfig.pins->rx->pin.port), ascConfig.pins->rx->pin.pinIndex
        , IfxPort_getIndex(ascConfig.pins->tx->pin.port), ascConfig.pins->tx->pin.pinIndex
        );
    App_printStatus(result);
*/
    return result;
}



// This is for Tlf35584 SPI interface //
boolean LB30_AppInit_Qspi2(LB30 *board)
{
    IfxQspi_SpiMaster_Config spiConfig;
    IfxQspi_SpiMaster_Pins   spiPins;
    spiPins.sclk      = &IfxQspi2_SCLK_P15_3_OUT;
    spiPins.sclkMode  = IfxPort_OutputMode_pushPull;
    spiPins.mtsr      = &IfxQspi2_MTSR_P15_5_OUT;
    spiPins.mtsrMode  = IfxPort_OutputMode_pushPull;
    spiPins.mrst      = &IfxQspi2_MRSTA_P15_4_IN;
    spiPins.mrstMode  = IfxPort_InputMode_noPullDevice;
    spiPins.pinDriver = IfxPort_PadDriver_cmosAutomotiveSpeed1;

    IfxQspi_SpiMaster_initModuleConfig(&spiConfig, &MODULE_QSPI2);
    spiConfig.base.rxPriority      = ISR_PRIORITY(INTERRUPT_QSPI2_RX);
    spiConfig.base.txPriority      = ISR_PRIORITY(INTERRUPT_QSPI2_TX);
    spiConfig.base.erPriority      = ISR_PRIORITY(INTERRUPT_QSPI2_ERR);
    spiConfig.base.bufferSize      = 0;
    spiConfig.base.buffer          = NULL_PTR;
    spiConfig.base.maximumBaudrate = 50.0e6;
    spiConfig.pins                 = &spiPins;
    IfxQspi_SpiMaster_initModule(&board->driver.qspi2, &spiConfig);

//    App_printQspiInitializationStatus(&spiConfig);
    return TRUE;
}

// This is for AD2S1200 Position Sensor SPI interface //
boolean LB30_AppInit_Qspi5(LB30 *board)
{
    IfxQspi_SpiMaster_Config spiConfig;
    IfxQspi_SpiMaster_Pins   spiPins;
    spiPins.sclk      = &IfxQspi5_SCLK_P14_10_OUT;
    spiPins.sclkMode  = IfxPort_OutputMode_pushPull;
    spiPins.mtsr      = &IfxQspi5_MTSR_P14_6_OUT;
    spiPins.mtsrMode  = IfxPort_OutputMode_pushPull;
    spiPins.mrst      = &IfxQspi5_MRSTB_P14_5_IN;
    spiPins.mrstMode  = IfxPort_InputMode_noPullDevice;	// IfxPort_InputMode_pullUp;
    spiPins.pinDriver = IfxPort_PadDriver_cmosAutomotiveSpeed1;

    IfxQspi_SpiMaster_initModuleConfig(&spiConfig, &MODULE_QSPI5);
    spiConfig.base.rxPriority      = ISR_PRIORITY(INTERRUPT_QSPI5_RX);
    spiConfig.base.txPriority      = ISR_PRIORITY(INTERRUPT_QSPI5_TX);
    spiConfig.base.erPriority      = ISR_PRIORITY(INTERRUPT_QSPI5_ERR);
    spiConfig.base.bufferSize      = 0;
    spiConfig.base.buffer          = NULL_PTR;
    spiConfig.base.maximumBaudrate = 50.0e6;
    spiConfig.pins                 = &spiPins;
    IfxQspi_SpiMaster_initModule(&board->driver.qspi5, &spiConfig);

//    App_printQspiInitializationStatus(&spiConfig);

    return TRUE;
}


boolean LB30_AppInit_1ms(LB30 *board)
{   /** - Timers: 1ms */
    boolean                  result = TRUE;

    IfxGtm_Atom_Timer_Config timerConfig;
    IfxGtm_Atom_Timer_initConfig(&timerConfig, &MODULE_GTM);
    timerConfig.base.frequency       = 1000;
    timerConfig.base.isrPriority     = ISR_PRIORITY(INTERRUPT_TIMER_1MS);
    timerConfig.base.isrProvider     = ISR_PROVIDER(INTERRUPT_TIMER_1MS);
    timerConfig.base.minResolution   = (1.0 / timerConfig.base.frequency) / 1000;
    timerConfig.base.trigger.enabled = FALSE;
    timerConfig.atom                 = IfxGtm_Atom_1;
    timerConfig.timerChannel         = IfxGtm_Atom_Ch_0;
    timerConfig.clock                = IfxGtm_Cmu_Clk_1;
    result                           = IfxGtm_Atom_Timer_init(&board->driver.timerOneMs, &timerConfig);

    IfxGtm_Atom_Timer_run(&board->driver.timerOneMs);
/*
 *  Ifx_Console_printAlign("Initializing 1ms timer (GTM, ATOM%d.%d)", timerConfig.atom, timerConfig.timerChannel);
 *  App_printStatus(result);
 */
    return result;
}


boolean LB30_AppInit_10ms(LB30 *board)
{   /** - Timers: 10ms */
    boolean                  result = TRUE;

    IfxGtm_Atom_Timer_Config timerConfig;
    IfxGtm_Atom_Timer_initConfig(&timerConfig, &MODULE_GTM);
    timerConfig.base.frequency       = 100;
    timerConfig.base.isrPriority     = ISR_PRIORITY(INTERRUPT_TIMER_10MS);
    timerConfig.base.isrProvider     = ISR_PROVIDER(INTERRUPT_TIMER_10MS);
    timerConfig.base.minResolution   = (1.0 / timerConfig.base.frequency) / 1000;
    timerConfig.base.trigger.enabled = FALSE;
    timerConfig.atom                 = IfxGtm_Atom_1;
    timerConfig.timerChannel         = IfxGtm_Atom_Ch_2;
    timerConfig.clock                = IfxGtm_Cmu_Clk_1;
    IfxGtm_Atom_Timer_init(&board->driver.timerTenMs, &timerConfig);

    IfxGtm_Atom_Timer_run(&board->driver.timerTenMs);

//    App_printStatusTitle("Initializing 1ms timer");
//    Ifx_Console_print(" (GTM, ATOM%d.%d)", timerConfig.atom, timerConfig.timerChannel);
//    App_printStatus(result);

    return result;
}




// canNode[0] //
boolean LB30_AppInit_Can0_Node0(LB30 *board)
{   /* CAN0 */
    boolean                    result = TRUE;
    IfxCan_Can_NodeConfig 	   nodeConfig;
//    App_CanBasic       		   g_CanBasic;
    // 20191213 Jimmy Modified //

    IfxCan_Can_initNodeConfig(&nodeConfig, &g_CanBasic.drivers.can[0]);
#if CAN_FD_ENABLE
    nodeConfig.baudRate.baudrate           			     = 500000; //Max (1MBaud), Real (0.5MBaud)
    nodeConfig.baudRate.samplePoint                      = 8000;
    nodeConfig.baudRate.syncJumpWidth					 = 3;
    nodeConfig.baudRate.prescaler                        = 7;
    nodeConfig.baudRate.timeSegment1                     = 0x0E;
    nodeConfig.baudRate.timeSegment2                     = 0x3;
    nodeConfig.frame.mode 								 = IfxCan_FrameMode_fdLongAndFast;
    nodeConfig.rxConfig.rxBufferDataFieldSize 			 = IfxCan_DataFieldSize_64;
    nodeConfig.txConfig.txBufferDataFieldSize 			 = IfxCan_DataFieldSize_64;
    nodeConfig.fastBaudRate.baudrate 					 = 2000000;	// 2M bps data rate
    nodeConfig.fastBaudRate.samplePoint                  = 8000;
    nodeConfig.fastBaudRate.syncJumpWidth			     = 3;
    nodeConfig.fastBaudRate.prescaler                    = 1;
    nodeConfig.fastBaudRate.timeSegment1                 = 0x0E;
    nodeConfig.fastBaudRate.timeSegment2                 = 0x3;
#else if
    nodeConfig.fastBaudRate.baudrate = 500000;				// Jimmy
    nodeConfig.fastBaudRate.prescaler = 0;					// Jimmy
#endif
	nodeConfig.nodeId						  			 = IfxCan_NodeId_0;
	nodeConfig.frame.type 					  			 = IfxCan_FrameType_transmitAndReceive;	//IfxCan_FrameType_receive;	//IfxCan_FrameType_transmitAndReceive
	nodeConfig.messageRAM.standardFilterListStartAddress = 0x100;
    nodeConfig.messageRAM.rxBuffersStartAddress          = 0x200;
    nodeConfig.messageRAM.txBuffersStartAddress          = 0x400;
    nodeConfig.messageRAM.baseAddress                    = (uint32)&MODULE_CAN0;

    IfxCan_Can_Pins 		   		pins;
    pins.rxPin					  = &IfxCan_RXD00C_P12_0_IN;
    pins.rxPinMode				  = IfxPort_InputMode_pullUp;
    pins.txPin					  = &IfxCan_TXD00_P12_1_OUT;
	pins.txPinMode				  = IfxPort_OutputMode_pushPull;
	pins.padDriver 				  = IfxPort_PadDriver_cmosAutomotiveSpeed2;
	nodeConfig.pins 			  = &pins;

	/* For interrupt configuration user needs to configure the priority and select one of the 16 interrupt lines*/
//	nodeConfig.interruptConfig.reint.priority = ISR_PRIORITY(INTERRUPT_CAN0);

//	nodeConfig.interruptConfig.rxFifo1NewMessageEnabled = TRUE;
//	nodeConfig.interruptConfig.rxFifo0NewMessageEnabled = TRUE;
//	nodeConfig.interruptConfig.transmissionCompletedEnabled = TRUE;
	nodeConfig.interruptConfig.reint.priority = ISR_PRIORITY_CAN0_NODE0_RX ;
	nodeConfig.interruptConfig.reint.interruptLine =  IfxCan_InterruptLine_14;
	nodeConfig.interruptConfig.reint.typeOfService = IfxCpu_Irq_getTos(IfxCpu_getCoreIndex());

    result    &= IfxCan_Can_initNode(&g_CanBasic.drivers.canNode[0], &nodeConfig) == IfxCan_Status_ok;

    Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(g_CanBasic.drivers.canNode[0].can, nodeConfig.nodeId);

    IfxCan_Node_enableConfigurationChange(nodeSfr);


    /* Enable interrupts in CAN */
    IfxCan_Node_enableInterrupt(nodeSfr, IfxCan_Interrupt_messageStoredToDedicatedRxBuffer);
    IfxCan_Node_disableConfigurationChange(nodeSfr);
//    IfxCan_Can_Node_activate(&board->driver.canNodes[0]);
#if 1
    /* Initialise the filter structure */
    IfxCan_Filter filter[10];

    /* Set a range filter to accept the CAN message with Ids 0x00- 0x7ff*/

    /* set filter0 for rxBuffer 0 */
    filter[0].number = 0;
    filter[0].elementConfiguration = IfxCan_FilterElementConfiguration_storeInRxBuffer;
    filter[0].id1 = 0x39;
    filter[0].rxBufferOffset = IfxCan_RxBufferId_0;

    IfxCan_Can_setStandardFilter(&g_CanBasic.drivers.canNode[0], &filter[0]);
#endif

    // NERR pin
    board->driver.can0ErrN = (IfxPort_Pin) {&MODULE_P13, 0};
    Pin_setMode(&board->driver.can0ErrN, IfxPort_Mode_inputNoPullDevice);
    Pin_setDriver(&board->driver.can0ErrN, IfxPort_PadDriver_cmosAutomotiveSpeed1);

    // EN pin
    board->driver.can0En = (IfxPort_Pin) {&MODULE_P11, 15};
    Pin_setMode(&board->driver.can0En, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(&board->driver.can0En, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    Pin_setState(&board->driver.can0En, IfxPort_State_high);

    // NSTB pin
    board->driver.can0StbN = (IfxPort_Pin) {&MODULE_P11, 13};
    Pin_setMode(&board->driver.can0StbN, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(&board->driver.can0StbN, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    Pin_setState(&board->driver.can0StbN, IfxPort_State_high);

    return result;
}

// canNode[1] //
boolean LB30_AppInit_Can0_Node3(LB30 *board)
{   /* CAN0 */
    boolean                    result = TRUE;
    IfxCan_Can_NodeConfig      nodeConfig;
//    App_CanBasic       		   g_CanBasic;

    IfxCan_Can_initNodeConfig(&nodeConfig, &g_CanBasic.drivers.can[0]);
#if CAN_FD_ENABLE
    nodeConfig.baudRate.baudrate           			     = 500000; //Max (1MBaud), Real (0.5MBaud)
    nodeConfig.baudRate.samplePoint                      = 8000;
    nodeConfig.baudRate.syncJumpWidth					 = 3;
    nodeConfig.baudRate.prescaler                        = 7;
    nodeConfig.baudRate.timeSegment1                     = 0x0E;
    nodeConfig.baudRate.timeSegment2                     = 0x3;
    nodeConfig.frame.mode 								 = IfxCan_FrameMode_fdLongAndFast;
    nodeConfig.rxConfig.rxBufferDataFieldSize 			 = IfxCan_DataFieldSize_64;
    nodeConfig.txConfig.txBufferDataFieldSize 			 = IfxCan_DataFieldSize_64;
    nodeConfig.fastBaudRate.baudrate 					 = 2000000;	// 2M bps data rate
    nodeConfig.fastBaudRate.samplePoint                  = 8000;
    nodeConfig.fastBaudRate.syncJumpWidth			     = 3;
    nodeConfig.fastBaudRate.prescaler                    = 1;
    nodeConfig.fastBaudRate.timeSegment1                 = 0x0E;
    nodeConfig.fastBaudRate.timeSegment2                 = 0x3;
#else if
    nodeConfig.fastBaudRate.baudrate 					 = 500000;		// Jimmy
    nodeConfig.fastBaudRate.prescaler 					 = 0;			// Jimmy
#endif
    nodeConfig.nodeId 									 = IfxCan_NodeId_3;
    nodeConfig.frame.type 								 = IfxCan_FrameType_transmitAndReceive;//IfxCan_FrameType_transmit;
	nodeConfig.messageRAM.standardFilterListStartAddress = 0x600;
    nodeConfig.messageRAM.rxBuffersStartAddress          = 0x800;
    nodeConfig.messageRAM.txBuffersStartAddress 		 = 0xA00;
    nodeConfig.messageRAM.baseAddress                    = (uint32)&MODULE_CAN0;

    IfxCan_Can_Pins pins;
    pins.rxPin = &IfxCan_RXD03D_P11_10_IN;
    pins.rxPinMode = IfxPort_InputMode_pullUp;
    pins.txPin = &IfxCan_TXD03_P11_12_OUT;
    pins.txPinMode = IfxPort_OutputMode_pushPull;
    pins.padDriver = IfxPort_PadDriver_cmosAutomotiveSpeed2;
    nodeConfig.pins = &pins;

    nodeConfig.interruptConfig.reint.priority = ISR_PRIORITY_CAN0_NODE3_RX ;
   	nodeConfig.interruptConfig.reint.interruptLine =  IfxCan_InterruptLine_14;
   	nodeConfig.interruptConfig.reint.typeOfService = IfxCpu_Irq_getTos(IfxCpu_getCoreIndex());

    result                                   &= IfxCan_Can_initNode(&g_CanBasic.drivers.canNode[1], &nodeConfig) == IfxCan_Status_ok;
    Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(g_CanBasic.drivers.canNode[1].can, nodeConfig.nodeId);

    IfxCan_Node_enableConfigurationChange(nodeSfr);
    /* Enable interrupts in CAN */
    IfxCan_Node_enableInterrupt(nodeSfr, IfxCan_Interrupt_messageStoredToDedicatedRxBuffer);
    IfxCan_Node_disableConfigurationChange(nodeSfr);

#if 0
    /* Initialise the filter structure */
    IfxCan_Filter filter[10];

    /* Set a range filter to accept the CAN message with Ids 0x00- 0x7ff*/

    /* set filter0 for rxBuffer 0 */
    filter[1].number = 1;
    filter[1].elementConfiguration = IfxCan_FilterElementConfiguration_storeInRxBuffer;
    filter[1].id1 = 0x12;
    filter[1].rxBufferOffset = IfxCan_RxBufferId_3;

    IfxCan_Can_setStandardFilter(&g_CanBasic.drivers.canNode[1], &filter[1]);
#endif
    // NERR pin
    board->driver.can0node3ErrN = (IfxPort_Pin) {&MODULE_P14, 9};
    Pin_setMode(&board->driver.can0node3ErrN, IfxPort_Mode_inputNoPullDevice);
    Pin_setDriver(&board->driver.can0node3ErrN, IfxPort_PadDriver_cmosAutomotiveSpeed1);

    // EN pin
    board->driver.can0node3En = (IfxPort_Pin) {&MODULE_P11, 4};
    Pin_setMode(&board->driver.can0node3En, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(&board->driver.can0node3En, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    Pin_setState(&board->driver.can0node3En, IfxPort_State_high);

    // NSTB pin
    board->driver.can0node3StbN = (IfxPort_Pin) {&MODULE_P23, 4};
    Pin_setMode(&board->driver.can0node3StbN, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(&board->driver.can0node3StbN, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    Pin_setState(&board->driver.can0node3StbN, IfxPort_State_high);

    return result;
}

// canNode[2] //
boolean LB30_AppInit_Can1_Node1(LB30 *board)
{
    boolean                    result = TRUE;
    IfxCan_Can_NodeConfig 	   nodeConfig;

    IfxCan_Can_initNodeConfig(&nodeConfig, &g_CanBasic.drivers.can[1]);
#if CAN_FD_ENABLE
    nodeConfig.baudRate.baudrate           			     = 500000; //Max (1MBaud), Real (0.5MBaud)
    nodeConfig.baudRate.samplePoint                      = 8000;
    nodeConfig.baudRate.syncJumpWidth					 = 3;
    nodeConfig.baudRate.prescaler                        = 7;
    nodeConfig.baudRate.timeSegment1                     = 0x0E;
    nodeConfig.baudRate.timeSegment2                     = 0x3;
    nodeConfig.frame.mode 								 = IfxCan_FrameMode_fdLongAndFast;
    nodeConfig.rxConfig.rxBufferDataFieldSize 			 = IfxCan_DataFieldSize_64;
    nodeConfig.txConfig.txBufferDataFieldSize 			 = IfxCan_DataFieldSize_64;
    nodeConfig.fastBaudRate.baudrate 					 = 2000000;	// 2M bps data rate
    nodeConfig.fastBaudRate.samplePoint                  = 8000;
    nodeConfig.fastBaudRate.syncJumpWidth			     = 3;
    nodeConfig.fastBaudRate.prescaler                    = 1;
    nodeConfig.fastBaudRate.timeSegment1                 = 0x0E;
    nodeConfig.fastBaudRate.timeSegment2                 = 0x3;
#else if
    nodeConfig.fastBaudRate.baudrate = 500000;				// Jimmy
    nodeConfig.fastBaudRate.prescaler = 0;					// Jimmy
#endif
	nodeConfig.nodeId						  			 = IfxCan_NodeId_1;
	nodeConfig.frame.type 					  			 = IfxCan_FrameType_transmitAndReceive;	//IfxCan_FrameType_receive;	//IfxCan_FrameType_transmitAndReceive
	nodeConfig.messageRAM.standardFilterListStartAddress = 0x100;
    nodeConfig.messageRAM.rxBuffersStartAddress          = 0x200;
    nodeConfig.messageRAM.txBuffersStartAddress          = 0x400;
    nodeConfig.messageRAM.baseAddress                    = (uint32)&MODULE_CAN1;

    IfxCan_Can_Pins 		   		pins;
    pins.rxPin					  = &IfxCan_RXD11D_P11_7_IN;
    pins.rxPinMode				  = IfxPort_InputMode_pullUp;
    pins.txPin					  = &IfxCan_TXD11_P11_0_OUT;
	pins.txPinMode				  = IfxPort_OutputMode_pushPull;
	pins.padDriver 				  = IfxPort_PadDriver_cmosAutomotiveSpeed2;
	nodeConfig.pins 			  = &pins;

	/* For interrupt configuration user needs to configure the priority and select one of the 16 interrupt lines*/
//	nodeConfig.interruptConfig.reint.priority = ISR_PRIORITY(INTERRUPT_CAN0);

//	nodeConfig.interruptConfig.rxFifo1NewMessageEnabled = TRUE;
//	nodeConfig.interruptConfig.rxFifo0NewMessageEnabled = TRUE;
//	nodeConfig.interruptConfig.transmissionCompletedEnabled = TRUE;
	nodeConfig.interruptConfig.reint.priority = ISR_PRIORITY_CAN1_NODE1_TX ;
	nodeConfig.interruptConfig.reint.interruptLine =  IfxCan_InterruptLine_15;
	nodeConfig.interruptConfig.reint.typeOfService = IfxCpu_Irq_getTos(IfxCpu_getCoreIndex());

    result    &= IfxCan_Can_initNode(&g_CanBasic.drivers.canNode[2], &nodeConfig) == IfxCan_Status_ok;

    Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(g_CanBasic.drivers.canNode[2].can, nodeConfig.nodeId);

    IfxCan_Node_enableConfigurationChange(nodeSfr);


    /* Enable interrupts in CAN */
    IfxCan_Node_enableInterrupt(nodeSfr, IfxCan_Interrupt_transmissionCompleted);
    IfxCan_Node_disableConfigurationChange(nodeSfr);
//    IfxCan_Can_Node_activate(&board->driver.canNodes[2]);
#if 0
    /* Initialise the filter structure */
    IfxCan_Filter filter[10];

    /* Set a range filter to accept the CAN message with Ids 0x00- 0x7ff*/

    /* set filter0 for rxBuffer 0 */
    filter[2].number = 2;
    filter[2].elementConfiguration = IfxCan_FilterElementConfiguration_storeInRxBuffer;
    filter[2].id1 = 0x12;
    filter[2].rxBufferOffset = IfxCan_RxBufferId_1;

    IfxCan_Can_setStandardFilter(&g_CanBasic.drivers.canNode[2], &filter[2]);
#endif

    // NERR pin
    board->driver.can0ErrN = (IfxPort_Pin) {&MODULE_P15, 8};
    Pin_setMode(&board->driver.can0ErrN, IfxPort_Mode_inputNoPullDevice);
    Pin_setDriver(&board->driver.can0ErrN, IfxPort_PadDriver_cmosAutomotiveSpeed1);

    // EN pin
    board->driver.can0En = (IfxPort_Pin) {&MODULE_P14, 7};
    Pin_setMode(&board->driver.can0En, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(&board->driver.can0En, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    Pin_setState(&board->driver.can0En, IfxPort_State_high);

    // NSTB pin
    board->driver.can0StbN = (IfxPort_Pin) {&MODULE_P23, 1};
    Pin_setMode(&board->driver.can0StbN, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(&board->driver.can0StbN, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    Pin_setState(&board->driver.can0StbN, IfxPort_State_high);

    return result;
}

// canNode[3] //
boolean LB30_AppInit_Can1_Node2(LB30 *board)
{
    boolean                    result = TRUE;
    IfxCan_Can_NodeConfig 	   nodeConfig;

    IfxCan_Can_initNodeConfig(&nodeConfig, &g_CanBasic.drivers.can[1]);
#if CAN_FD_ENABLE
    nodeConfig.baudRate.baudrate           			     = 500000; //Max (1MBaud), Real (0.5MBaud)
    nodeConfig.baudRate.samplePoint                      = 8000;
    nodeConfig.baudRate.syncJumpWidth					 = 3;
    nodeConfig.baudRate.prescaler                        = 7;
    nodeConfig.baudRate.timeSegment1                     = 0x0E;
    nodeConfig.baudRate.timeSegment2                     = 0x3;
    nodeConfig.frame.mode 								 = IfxCan_FrameMode_fdLongAndFast;
    nodeConfig.rxConfig.rxBufferDataFieldSize 			 = IfxCan_DataFieldSize_64;
    nodeConfig.txConfig.txBufferDataFieldSize 			 = IfxCan_DataFieldSize_64;
    nodeConfig.fastBaudRate.baudrate 					 = 2000000;	// 2M bps data rate
    nodeConfig.fastBaudRate.samplePoint                  = 8000;
    nodeConfig.fastBaudRate.syncJumpWidth			     = 3;
    nodeConfig.fastBaudRate.prescaler                    = 1;
    nodeConfig.fastBaudRate.timeSegment1                 = 0x0E;
    nodeConfig.fastBaudRate.timeSegment2                 = 0x3;
#else if
    nodeConfig.fastBaudRate.baudrate = 500000;				// Jimmy
    nodeConfig.fastBaudRate.prescaler = 0;					// Jimmy
#endif
	nodeConfig.nodeId						  			 = IfxCan_NodeId_2;
	nodeConfig.frame.type 					  			 = IfxCan_FrameType_transmitAndReceive;	//IfxCan_FrameType_receive;	//IfxCan_FrameType_transmitAndReceive
	nodeConfig.messageRAM.standardFilterListStartAddress = 0x600;
    nodeConfig.messageRAM.rxBuffersStartAddress          = 0x800;
    nodeConfig.messageRAM.txBuffersStartAddress          = 0xA00;
    nodeConfig.messageRAM.baseAddress                    = (uint32)&MODULE_CAN1;

    IfxCan_Can_Pins 		   		pins;
    pins.rxPin					  = &IfxCan_RXD12D_P11_8_IN;
    pins.rxPinMode				  = IfxPort_InputMode_pullUp;
    pins.txPin					  = &IfxCan_TXD12_P11_1_OUT;
	pins.txPinMode				  = IfxPort_OutputMode_pushPull;
	pins.padDriver 				  = IfxPort_PadDriver_cmosAutomotiveSpeed2;
	nodeConfig.pins 			  = &pins;

	/* For interrupt configuration user needs to configure the priority and select one of the 16 interrupt lines*/
//	nodeConfig.interruptConfig.reint.priority = ISR_PRIORITY(INTERRUPT_CAN0);

//	nodeConfig.interruptConfig.rxFifo1NewMessageEnabled = TRUE;
//	nodeConfig.interruptConfig.rxFifo0NewMessageEnabled = TRUE;
//	nodeConfig.interruptConfig.transmissionCompletedEnabled = TRUE;
	nodeConfig.interruptConfig.reint.priority = ISR_PRIORITY_CAN1_NODE2_TX ;
	nodeConfig.interruptConfig.reint.interruptLine =  IfxCan_InterruptLine_15;
	nodeConfig.interruptConfig.reint.typeOfService = IfxCpu_Irq_getTos(IfxCpu_getCoreIndex());

    result    &= IfxCan_Can_initNode(&g_CanBasic.drivers.canNode[3], &nodeConfig) == IfxCan_Status_ok;

    Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(g_CanBasic.drivers.canNode[3].can, nodeConfig.nodeId);

    IfxCan_Node_enableConfigurationChange(nodeSfr);


    /* Enable interrupts in CAN */
    IfxCan_Node_enableInterrupt(nodeSfr, IfxCan_Interrupt_transmissionCompleted);
    IfxCan_Node_disableConfigurationChange(nodeSfr);
//    IfxCan_Can_Node_activate(&board->driver.canNodes[3]);
#if 0
    /* Initialise the filter structure */
    IfxCan_Filter filter[10];

    /* Set a range filter to accept the CAN message with Ids 0x00- 0x7ff*/

    /* set filter3 for rxBuffer 0 */
    filter[3].number = 3;
    filter[3].elementConfiguration = IfxCan_FilterElementConfiguration_storeInRxBuffer;
    filter[3].id1 = 0x12;
    filter[3].rxBufferOffset = IfxCan_RxBufferId_1;

    IfxCan_Can_setStandardFilter(&g_CanBasic.drivers.canNode[3], &filter[3]);
#endif

    // NERR pin
    board->driver.can0ErrN = (IfxPort_Pin) {&MODULE_P10, 2};
    Pin_setMode(&board->driver.can0ErrN, IfxPort_Mode_inputNoPullDevice);
    Pin_setDriver(&board->driver.can0ErrN, IfxPort_PadDriver_cmosAutomotiveSpeed1);

    // EN pin
    board->driver.can0En = (IfxPort_Pin) {&MODULE_P10, 0};
    Pin_setMode(&board->driver.can0En, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(&board->driver.can0En, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    Pin_setState(&board->driver.can0En, IfxPort_State_high);

    // NSTB pin
    board->driver.can0StbN = (IfxPort_Pin) {&MODULE_P11, 11};
    Pin_setMode(&board->driver.can0StbN, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(&board->driver.can0StbN, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    Pin_setState(&board->driver.can0StbN, IfxPort_State_high);

    return result;
}

// canNode[4] //
boolean LB30_AppInit_Can2_Node0(LB30 *board)
{
    boolean                    result = TRUE;
    IfxCan_Can_NodeConfig 	   nodeConfig;

    IfxCan_Can_initNodeConfig(&nodeConfig, &g_CanBasic.drivers.can[2]);
#if CAN_FD_ENABLE
    nodeConfig.baudRate.baudrate           			     = 500000; //Max (1MBaud), Real (0.5MBaud)
    nodeConfig.baudRate.samplePoint                      = 8000;
    nodeConfig.baudRate.syncJumpWidth					 = 3;
    nodeConfig.baudRate.prescaler                        = 7;
    nodeConfig.baudRate.timeSegment1                     = 0x0E;
    nodeConfig.baudRate.timeSegment2                     = 0x3;
    nodeConfig.frame.mode 								 = IfxCan_FrameMode_fdLongAndFast;
    nodeConfig.rxConfig.rxBufferDataFieldSize 			 = IfxCan_DataFieldSize_64;
    nodeConfig.txConfig.txBufferDataFieldSize 			 = IfxCan_DataFieldSize_64;
    nodeConfig.fastBaudRate.baudrate 					 = 2000000;	// 2M bps data rate
    nodeConfig.fastBaudRate.samplePoint                  = 8000;
    nodeConfig.fastBaudRate.syncJumpWidth			     = 3;
    nodeConfig.fastBaudRate.prescaler                    = 1;
    nodeConfig.fastBaudRate.timeSegment1                 = 0x0E;
    nodeConfig.fastBaudRate.timeSegment2                 = 0x3;
#else if
    nodeConfig.fastBaudRate.baudrate = 500000;				// Jimmy
    nodeConfig.fastBaudRate.prescaler = 0;					// Jimmy
#endif
	nodeConfig.nodeId						  			 = IfxCan_NodeId_0;
	nodeConfig.frame.type 					  			 = IfxCan_FrameType_transmitAndReceive;	//IfxCan_FrameType_receive;	//IfxCan_FrameType_transmitAndReceive
	nodeConfig.messageRAM.standardFilterListStartAddress = 0x200;
    nodeConfig.messageRAM.rxBuffersStartAddress          = 0x400;
    nodeConfig.messageRAM.txBuffersStartAddress          = 0x600;
    nodeConfig.messageRAM.baseAddress                    = (uint32)&MODULE_CAN2;

    IfxCan_Can_Pins 		   		pins;
    pins.rxPin					  = &IfxCan_RXD20F_P11_14_IN;
    pins.rxPinMode				  = IfxPort_InputMode_pullUp;
    pins.txPin					  = &IfxCan_TXD20_P11_5_OUT;
	pins.txPinMode				  = IfxPort_OutputMode_pushPull;
	pins.padDriver 				  = IfxPort_PadDriver_cmosAutomotiveSpeed2;
	nodeConfig.pins 			  = &pins;

	/* For interrupt configuration user needs to configure the priority and select one of the 16 interrupt lines*/
//	nodeConfig.interruptConfig.reint.priority = ISR_PRIORITY(INTERRUPT_CAN0);

//	nodeConfig.interruptConfig.rxFifo1NewMessageEnabled = TRUE;
//	nodeConfig.interruptConfig.rxFifo0NewMessageEnabled = TRUE;
//	nodeConfig.interruptConfig.transmissionCompletedEnabled = TRUE;
	nodeConfig.interruptConfig.reint.priority = ISR_PRIORITY_CAN2_NODE0_TX ;
	nodeConfig.interruptConfig.reint.interruptLine =  IfxCan_InterruptLine_15;
	nodeConfig.interruptConfig.reint.typeOfService = IfxCpu_Irq_getTos(IfxCpu_getCoreIndex());

    result    &= IfxCan_Can_initNode(&g_CanBasic.drivers.canNode[4], &nodeConfig) == IfxCan_Status_ok;

    Ifx_CAN_N *nodeSfr = IfxCan_getNodePointer(g_CanBasic.drivers.canNode[4].can, nodeConfig.nodeId);

    IfxCan_Node_enableConfigurationChange(nodeSfr);


    /* Enable interrupts in CAN */
    IfxCan_Node_enableInterrupt(nodeSfr, IfxCan_Interrupt_transmissionCompleted);
    IfxCan_Node_disableConfigurationChange(nodeSfr);
//    IfxCan_Can_Node_activate(&board->driver.canNodes[4]);
#if 0
    /* Initialise the filter structure */
    IfxCan_Filter filter[10];

    /* Set a range filter to accept the CAN message with Ids 0x00- 0x7ff*/

    /* set filter0 for rxBuffer 0 */
    filter[4].number = 4;
    filter[4].elementConfiguration = IfxCan_FilterElementConfiguration_storeInRxBuffer;
    filter[4].id1 = 0x12;
    filter[4].rxBufferOffset = IfxCan_RxBufferId_1;

    IfxCan_Can_setStandardFilter(&g_CanBasic.drivers.canNode[4], &filter[4]);
#endif

    // NERR pin
    board->driver.can0ErrN = (IfxPort_Pin) {&MODULE_P20, 6};
    Pin_setMode(&board->driver.can0ErrN, IfxPort_Mode_inputNoPullDevice);
    Pin_setDriver(&board->driver.can0ErrN, IfxPort_PadDriver_cmosAutomotiveSpeed1);

    // EN pin
    board->driver.can0En = (IfxPort_Pin) {&MODULE_P15, 7};
    Pin_setMode(&board->driver.can0En, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(&board->driver.can0En, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    Pin_setState(&board->driver.can0En, IfxPort_State_high);

    // NSTB pin
    board->driver.can0StbN = (IfxPort_Pin) {&MODULE_P20, 9};
    Pin_setMode(&board->driver.can0StbN, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(&board->driver.can0StbN, IfxPort_PadDriver_cmosAutomotiveSpeed1);
    Pin_setState(&board->driver.can0StbN, IfxPort_State_high);

    return result;
}

boolean LB30_AppInit_CanModule(LB30 *board)
{   /* CAN */
    IfxCan_Can_Config  canConfig;
//    App_CanBasic       g_CanBasic;
    boolean            result = TRUE;
    // 20191213 Jimmy Modified //
    /* disable interrupts */
    boolean              interruptState = IfxCpu_disableInterrupts();

    /* Default clock initialisation */
    IfxScuCcu_Config IfxScuCcuConfig;

    IfxScuCcu_initConfig(&IfxScuCcuConfig);
    IfxScuCcu_init(&IfxScuCcuConfig);

    IfxCan_Can_initModuleConfig(&canConfig, &MODULE_CAN0);
//    canConfig.nodePointer[0].priority      = ISR_PRIORITY(INTERRUPT_CAN0);
//    canConfig.nodePointer[0].typeOfService = ISR_PROVIDER_CAN0;
//    canConfig.nodePointer[2].priority      = ISR_PRIORITY(INTERRUPT_CAN1);
//    canConfig.nodePointer[2].typeOfService = ISR_PROVIDER_CAN1;
    IfxCan_Can_initModule(&g_CanBasic.drivers.can[0], &canConfig);
#if ENABLEALLCAN
    IfxCan_Can_initModuleConfig(&canConfig, &MODULE_CAN1);
    IfxCan_Can_initModule(&g_CanBasic.drivers.can[1], &canConfig);\
    IfxCan_Can_initModuleConfig(&canConfig, &MODULE_CAN2);
    IfxCan_Can_initModule(&g_CanBasic.drivers.can[2], &canConfig);
#endif
    /* enable interrupts again */
    IfxCpu_restoreInterrupts(interruptState);

    return 1;
}


boolean LB30_AppInit_VadcChannelS(LB30 *board, pchar name, IfxEvadc_Adc_Channel *channel, IfxEvadc_Vadcg_In *inputPin, Ifx_Priority resultPriority, IfxEvadc_SrcNr resultSrcNr, boolean scan, boolean synchonize, Ifx_Priority priority)
{   /* Scan */
    boolean                    result = TRUE;
    IfxEvadc_Adc_ChannelConfig chConfig;
    IfxEvadc_Adc_initChannelConfig(&chConfig, &board->driver.group[inputPin->groupId]);
    chConfig.resultServProvider = priority;

    chConfig.channelId          = inputPin->channelId;
    chConfig.resultRegister     = (IfxEvadc_ChannelResult)inputPin->channelId;
    chConfig.resultPriority     = resultPriority;
    chConfig.resultSrcNr        = resultSrcNr;
    chConfig.synchonize         = synchonize;
    IfxEvadc_Adc_initChannel(channel, &chConfig);
// 20191215 Jimmy disable the Evadc scan function with TC397, because TC397 haven't it.
/*
    if (scan)
    {
        IfxVadc_Adc_setScan(chConfig.group, 1 << chConfig.channelId, 1 << chConfig.channelId);
    }

    if (strlen(name))
    {
        Ifx_Console_printAlign("- (group=%d, channel=%d,%s) %s"ENDL, inputPin->groupId, chConfig.channelId, name,
        		((scan && synchonize) ? "Synchronization master":
        				((!scan && !synchonize) ? "Synchronization slave":
        						((scan && !synchonize) ? "stand alone":("ERROR")))));
    }
*/
    return result;
}


boolean LB30_AppInit_VadcChannelQ(LB30 *board, pchar name, IfxEvadc_Adc_Channel *channel, IfxEvadc_Vadcg_In *inputPin, Ifx_Priority resultPriority, IfxEvadc_SrcNr resultSrcNr, boolean trigger, boolean synchonize, Ifx_Priority priority)
{   /* Queue */
    boolean                    result = TRUE;
    IfxEvadc_Adc_ChannelConfig chConfig;
    IfxEvadc_Adc_initChannelConfig(&chConfig, &board->driver.group[inputPin->groupId]);
    chConfig.resultServProvider = priority;

    chConfig.channelId          = inputPin->channelId;
    chConfig.resultRegister     = (IfxEvadc_ChannelResult)inputPin->channelId;
    chConfig.resultPriority     = resultPriority;
    chConfig.resultSrcNr        = resultSrcNr;
    chConfig.synchonize         = synchonize;
    IfxEvadc_Adc_initChannel(channel, &chConfig);
//    IfxEvadc_Adc_addToQueue(channel, (1 << IFX_EVADC_G_Q_Q0R_RF_OFF) /* Refill*/ | (trigger ? (1 << IFX_EVADC_G_Q_QBUR_EXTR_OFF) : 0 /* Trigger*/), IfxEvadc_RequestSource_queue0 /* FIXME add external trigger and interrupt if required */);
    IfxEvadc_Adc_addToQueue(channel,  IfxEvadc_RequestSource_queue0, (1 << IFX_EVADC_G_Q_Q0R_RF_OFF) /* Refill*/ | (trigger ? (1 << IFX_EVADC_G_Q_QBUR_EXTR_OFF) : 0 /* Trigger*/));

//    Ifx_Console_printAlign("- %s (group=%d, channel=%d)"ENDL, name, inputPin->groupId, chConfig.channelId);

    return result;
}


boolean LB30_AppInit_Vadc(LB30 *board)
{
    boolean result = TRUE;
//    App_printStatusTitle("Initializing VADC");
//    Ifx_Console_print(" (");
    IfxEvadc_Adc        		vadc; /* dummy module handle, used only for initialization */
    IfxEvadc_Status     		status;
    App_EvadcQueueTransfer 		g_EvadcQueueTransfer;
    {
    	/* VADC Module & groups & trigger */
        /** Initialise the VADC module */
        IfxEvadc_Adc_Config adcConfig;
        IfxEvadc_Adc_initModuleConfig(&adcConfig, &MODULE_EVADC);

        /* initialize module*/
        IfxEvadc_Adc_initModule(&g_EvadcQueueTransfer.evadc, &adcConfig);

//        adcConfig.moduleFrequency                = IFX_CFG_SCU_XTAL_FREQUENCY / 2; /* FIXME Should be config.digitalFrequency               = IfxScuCcu_getSpbFrequency(); */
//        adcConfig.analogFrequency                = IFXEVADC_ANALOG_FREQUENCY_MAX;
//        adcConfig.globalInputClass[0].resolution = IfxEvadc_ChannelResolution_12bit;
//        adcConfig.globalInputClass[0].sampleTime = 1.0e-6;
//        adcConfig.globalInputClass[1].resolution = IfxEvadc_ChannelResolution_12bit;
//        adcConfig.globalInputClass[1].sampleTime = 1.0e-6;

//        Ifx_Console_print("module frequency=%9.0f, analog frequency=%9.0f)"ENDL, adcConfig.moduleFrequency, adcConfig.analogFrequency);

        status = IfxEvadc_Adc_initModule(&g_EvadcQueueTransfer.evadc, &adcConfig);
        IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, status == IfxEvadc_Status_noError);

        {   /* Initialise VADC groups */
            IfxEvadc_Adc_GroupConfig adcGroupConfig;
            /* FIXME check that all kernel involved in // conversion are off before configuration */
            IfxEvadc_Adc_initGroupConfig(&adcGroupConfig, &g_EvadcQueueTransfer.evadc);

            adcGroupConfig.arbiter.requestSlotQueue0Enabled  = TRUE; // enable Queue0 mode
            adcGroupConfig.arbiter.requestSlotQueue1Enabled  = TRUE; // enable Queue1 mode
            adcGroupConfig.arbiter.requestSlotQueue2Enabled  = TRUE; // enable Queue2 mode

            /* Queue */
            adcGroupConfig.queueRequest[0].requestSlotPrio     = IfxEvadc_RequestSlotPriority_low;
            adcGroupConfig.queueRequest[0].flushQueueAfterInit = FALSE;

            if (board->configuration.inverter[0].pwmModule == LB_PwmModule_gtmAtom)
            {
                adcGroupConfig.queueRequest[0].triggerConfig.gatingSource = IfxEvadc_GatingSource_0;
            }
            else if (board->configuration.inverter[0].pwmModule == LB_PwmModule_gtmTom)
            {
                adcGroupConfig.queueRequest[0].triggerConfig.gatingSource = IfxEvadc_GatingSource_1;
            }
            else// if (board->configuration.inverter[0].pwmModule == LB_PwmModule_ccu6)
            {
                adcGroupConfig.queueRequest[0].triggerConfig.gatingSource = IfxEvadc_GatingSource_2;
            }

            adcGroupConfig.queueRequest[0].triggerConfig.triggerSource = IfxEvadc_TriggerSource_15;
            adcGroupConfig.queueRequest[0].triggerConfig.gatingMode    = IfxEvadc_GatingMode_always;
            adcGroupConfig.queueRequest[0].triggerConfig.triggerMode   = IfxEvadc_TriggerMode_uponRisingEdge;

            // GROUPS TRIGGERED AT THE TRIGGER ----------------------------------------------
            /* FIXME check trigger egde */

//            Ifx_Console_printAlign("- Slave={");
            // 20191224 Jimmy created
            // Init VADC group 8 as slave with group 8 as Master
            adcGroupConfig.groupId = IfxEvadc_GroupId_8;
            adcGroupConfig.master  = IfxEvadc_GroupId_8;	// 20200116 Jimmy Modified
            IfxEvadc_Adc_initGroup(&board->driver.group[8], &adcGroupConfig);

            // Init VADC group 3 as slave with group 0 as Master
            adcGroupConfig.groupId = IfxEvadc_GroupId_3;
            adcGroupConfig.master  = IfxEvadc_GroupId_0;
            IfxEvadc_Adc_initGroup(&board->driver.group[3], &adcGroupConfig);

            // Init VADC group 2 as slave with group 0 as Master
            adcGroupConfig.groupId = IfxEvadc_GroupId_2;
            adcGroupConfig.master  = IfxEvadc_GroupId_0;
            IfxEvadc_Adc_initGroup(&board->driver.group[2], &adcGroupConfig);

            // Init VADC group 1 as slave with group 0 as Master
            adcGroupConfig.groupId = IfxEvadc_GroupId_1;
            adcGroupConfig.master  = IfxEvadc_GroupId_0;
            IfxEvadc_Adc_initGroup(&board->driver.group[1], &adcGroupConfig);

            // Init VADC group 0 as slave with group 0 as Master
            adcGroupConfig.groupId = IfxEvadc_GroupId_0;
            adcGroupConfig.master  = IfxEvadc_GroupId_0;
            IfxEvadc_Adc_initGroup(&board->driver.group[0], &adcGroupConfig);

            /* Trigger is fixed for the 2nd motor */
//            adcGroupConfig.scanRequest.triggerConfig.gatingSource = IfxEvadc_GatingSource_0;
            adcGroupConfig.queueRequest[0].triggerConfig.gatingSource = IfxEvadc_GatingSource_0;

            // Init VADC group 6 as slave with group 4 as Master
            adcGroupConfig.groupId = IfxEvadc_GroupId_6;
            adcGroupConfig.master  = IfxEvadc_GroupId_4;
            IfxEvadc_Adc_initGroup(&board->driver.group[6], &adcGroupConfig);

            // Init VADC group 5 as slave with group 4 as Master
            adcGroupConfig.groupId = IfxEvadc_GroupId_5;
            adcGroupConfig.master  = IfxEvadc_GroupId_4;
            IfxEvadc_Adc_initGroup(&board->driver.group[5], &adcGroupConfig);

            // Init VADC group 4 as Master
            adcGroupConfig.groupId = IfxEvadc_GroupId_4;
            adcGroupConfig.master  = IfxEvadc_GroupId_4;
            IfxEvadc_Adc_initGroup(&board->driver.group[4], &adcGroupConfig);

			// Init VADC group 11 as slave with group 11 as Master
			adcGroupConfig.groupId = IfxEvadc_GroupId_11;
			adcGroupConfig.master  = IfxEvadc_GroupId_11;
			IfxEvadc_Adc_initGroup(&board->driver.group[11], &adcGroupConfig);
        }

        {   /* VADC Trigger */
            if (board->configuration.inverter[0].pwmModule == LB_PwmModule_gtmAtom)
            {
            	IfxGtm_Trig_toEVadc(&MODULE_GTM, IfxGtm_Trig_AdcGroup_0, IfxGtm_Trig_AdcTrig_0, IfxGtm_Trig_AdcTrigSource_atom0, IfxGtm_Trig_AdcTrigChannel_7);
            }
            else if (board->configuration.inverter[0].pwmModule == LB_PwmModule_gtmTom)
            {
                IfxGtm_Trig_toEVadc(&MODULE_GTM, IfxGtm_Trig_AdcGroup_0, IfxGtm_Trig_AdcTrig_1, IfxGtm_Trig_AdcTrigSource_tom0, IfxGtm_Trig_AdcTrigChannel_15);
            }
            else// if (board->configuration.inverter[0].pwmModule == LB_PwmModule_ccu6)
            {  /* NOTE DSADC can not be triggered by CCU6 */
                IfxCcu6_connectTrigger(&MODULE_CCU60, IfxCcu6_TrigOut_0, IfxCcu6_TrigSel_cout63);
            }

#if CFG_CURRENT_SENSOR_TEST == 1
            /* Trigger is only available if Inverter 0 uses ATOM */
            IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, board->configuration.inverter[0].pwmModule == LB_PwmModule_gtmAtom);

            IfxGtm_Trig_toEVadc(&MODULE_GTM, IfxGtm_Trig_AdcGroup_4, IfxGtm_Trig_AdcTrig_0, IfxGtm_Trig_AdcTrigSource_atom0, IfxGtm_Trig_AdcTrigChannel_7);

            result &= LB30_AppInit_VadcChannelS(board, "Current U" , &board->analogInput.currents[ECU_MOTOR_INDEX_1][0].hwInfo, &IfxVadc_G4_3_AN35_IN, 0, IfxVadc_SrcNr_group0, TRUE, TRUE, 0);
            result &= LB30_AppInit_VadcChannelS(board, "Current V" , &board->analogInput.currents[ECU_MOTOR_INDEX_1][1].hwInfo, &IfxVadc_G5_3_AN43_IN, 0, IfxVadc_SrcNr_group0, FALSE, FALSE, 0);
            result &= LB30_AppInit_VadcChannelS(board, "Current W" , &board->analogInput.currents[ECU_MOTOR_INDEX_1][2].hwInfo, &IfxVadc_G6_3_P00_9_IN, 0, IfxVadc_SrcNr_group1, FALSE, FALSE , 0);
#else
            /* FIXME no trigger source assigned for ADC G4,  */
            IfxGtm_Trig_toEVadc(&MODULE_GTM, IfxGtm_Trig_AdcGroup_4, IfxGtm_Trig_AdcTrig_1, IfxGtm_Trig_AdcTrigSource_atom1, IfxGtm_Trig_AdcTrigChannel_7);
            // 20200116 Jimmy Created //
            IfxGtm_Trig_toEVadc(&MODULE_GTM, IfxGtm_Trig_AdcGroup_8, IfxGtm_Trig_AdcTrig_1, IfxGtm_Trig_AdcTrigSource_atom0, IfxGtm_Trig_AdcTrigChannel_7);
			IfxGtm_Trig_toEVadc(&MODULE_GTM, IfxGtm_Trig_AdcGroup_11, IfxGtm_Trig_AdcTrig_1, IfxGtm_Trig_AdcTrigSource_atom0, IfxGtm_Trig_AdcTrigChannel_7);

#endif
        }
    }

    {   /* VADC Channels */
    	IfxEvadc_Adc_Channel dummy;
        /* Parallel conversions M0 */
        if (
        		(AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
        		|| (AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
        		)
        {
        	/* VDC is measured using the EiseSense device */
        }
        else
        {
            //result &= LB30_AppInit_VadcChannelQ(board, "VDC"   , &board->analogInput.vDc.hwInfo        , &IfxEvadc_G1CH4_AN12_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);
        }

        result &= LB30_AppInit_VadcChannelQ(board, "Current U" , &board->analogInput.currents[ECU_MOTOR_INDEX_0][0].hwInfo, &IfxEvadc_G2CH7_AN23_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);
        result &= LB30_AppInit_VadcChannelQ(board, "Current V" , &board->analogInput.currents[ECU_MOTOR_INDEX_0][1].hwInfo, &IfxEvadc_G1CH2_AN10_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);
        result &= LB30_AppInit_VadcChannelQ(board, "Current W" , &board->analogInput.currents[ECU_MOTOR_INDEX_0][2].hwInfo, &IfxEvadc_G0CH7_AN7_IN, ISR_PRIORITY(INTERRUPT_ADC_M0), IfxEvadc_SrcNr_group1, TRUE, TRUE, ISR_PROVIDER(INTERRUPT_ADC_M0));

		result &= LB30_AppInit_VadcChannelQ(board, "VDC_PhaseV", &board->analogInput.vDc.hwInfo        , &IfxEvadc_G3CH2_P40_2_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);		/* AN26/P40.2 */
		result &= LB30_AppInit_VadcChannelQ(board, "VDC_PhaseU", &board->analogInput.vDcPhaseU.hwInfo  , &IfxEvadc_G3CH3_P40_3_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);		/* AN27/P40.3 */
		result &= LB30_AppInit_VadcChannelQ(board, "VDig 5.0V" , &board->analogInput.vDig50.hwInfo     , &IfxEvadc_G2CH1_P40_10_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);		/* AN26/P40.2 */
        result &= LB30_AppInit_VadcChannelQ(board, "VRef 2.5V" , &board->analogInput.vRef25.hwInfo     , &IfxEvadc_G3CH5_P40_14_IN, 0, IfxEvadc_SrcNr_group0, FALSE , FALSE, 0);	/* AN29/P40.14 */
		result &= LB30_AppInit_VadcChannelQ(board, "vEncoder"  , &board->analogInput.vEncoder.hwInfo   , &IfxEvadc_G4CH1_AN41_IN, 0, IfxEvadc_SrcNr_group0, FALSE , FALSE, 0);		
		result &= LB30_AppInit_VadcChannelQ(board, "vHVILIn"   , &board->analogInput.vHVILIn.hwInfo    , &IfxEvadc_G8CH14_AN46_IN, 0, IfxEvadc_SrcNr_group0, FALSE , FALSE, 0);
		result &= LB30_AppInit_VadcChannelQ(board, "vHVILOut"  , &board->analogInput.vHVILOut.hwInfo   , &IfxEvadc_G8CH15_AN47_IN, 0, IfxEvadc_SrcNr_group0, FALSE , FALSE, 0);
        result &= LB30_AppInit_VadcChannelQ(board, "VRef 5.0V" , &board->analogInput.vRef50.hwInfo     , &IfxEvadc_G2CH6_AN22_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);
        result &= LB30_AppInit_VadcChannelQ(board, "KL30"      , &board->analogInput.kl30.hwInfo       , &IfxEvadc_G8CH12_AN44_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);
        result &= LB30_AppInit_VadcChannelQ(board, "VANA 5.0V" , &board->analogInput.vAna50.hwInfo     , &IfxEvadc_G2CH2_P40_11_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);		/* AN18/P40.11 */

		result &= LB30_AppInit_VadcChannelQ(board, "iVdc"	   , &board->analogInput.iVdc.hwInfo	   , &IfxEvadc_G11CH12_P40_4_IN, 0, IfxEvadc_SrcNr_group0, FALSE , FALSE, 0);

		result &= LB30_AppInit_VadcChannelQ(board, "Motor temp", &board->analogInput.motorTemp.hwInfo  , &IfxEvadc_G3CH7_AN31_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);        
		result &= LB30_AppInit_VadcChannelQ(board, "Board temp", &board->analogInput.tempBoard.hwInfo  , &IfxEvadc_G1CH7_AN15_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);
		result &= LB30_AppInit_VadcChannelQ(board, "tempWater" , &board->analogInput.tempWater.hwInfo  , &IfxEvadc_G3CH4_P40_13_IN, 0, IfxEvadc_SrcNr_group0, FALSE , FALSE, 0);	/* AN28/P40.13 */

		result &= LB30_AppInit_VadcChannelQ(board, "DRV Board temp", &board->analogInput.tempDRVBoard.hwInfo, &IfxEvadc_G8CH11_AN43_IN, 0, IfxEvadc_SrcNr_group0, FALSE , FALSE, 0);

		//result &= LB30_AppInit_VadcChannelQ(board, "IN2"       , &board->analogInput.in[2].hwInfo      , &IfxEvadc_G3CH4_P40_13_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);
        //result &= LB30_AppInit_VadcChannelQ(board, "IN1"       , &board->analogInput.in[1].hwInfo      , &IfxEvadc_G1CH4_AN12_IN, 0, IfxEvadc_SrcNr_group0, FALSE, FALSE, 0);
        //result &= LB30_AppInit_VadcChannelQ(board, "IN0"       , &board->analogInput.in[0].hwInfo      , &IfxEvadc_G0CH4_AN4_IN , 0, IfxEvadc_SrcNr_group0, TRUE , TRUE, 0);
		//result &= LB30_AppInit_VadcChannelQ(board, "IN3"       , &board->analogInput.in[3].hwInfo      , &IfxEvadc_G1CH3_AN11_IN, 0, IfxEvadc_SrcNr_group0, FALSE , FALSE, 0);
		
        //result &= LB30_AppInit_VadcChannelQ(board, "Dummy"     , &dummy                                , &IfxEvadc_G0CH6_AN6_IN , 0, IfxEvadc_SrcNr_group0, TRUE , TRUE, 0);
		//result &= LB30_AppInit_VadcChannelQ(board, "Dummy"     , &dummy                                , &IfxEvadc_G0CH5_AN5_IN , 0, IfxEvadc_SrcNr_group0, TRUE , TRUE, 0);
		//result &= LB30_AppInit_VadcChannelQ(board, "Dummy"     , &dummy                                , &IfxEvadc_G0CH3_AN3_IN, 0, IfxEvadc_SrcNr_group0, TRUE , TRUE, 0);
		//result &= LB30_AppInit_VadcChannelQ(board, "Dummy"     , &dummy                                , &IfxEvadc_G0CH2_AN2_IN, 0, IfxEvadc_SrcNr_group0, TRUE , TRUE, 0);

		/* start the Queue*/
//        IfxEvadc_Adc_startQueue(&g_EvadcQueueTransfer.adcGroup, IfxEvadc_RequestSource_queue0);
        /* FIXME add SINCOS channels */

        /* Non parallel conversion */
    }
//    App_printStatus(result);

    return result;
}


boolean LB30_AppInit_Inverter0PwmAtom(LB30 *board)
{   /* Inverter PWM */
    boolean        result = TRUE;
    AppDbStdIf_MainConfig *driverBoardConfig = AppDbStdIf_getMainConfig(board->driverBoardStdif);
//    App_printStatusTitle("Initializing Inverter 0 PWM on GTM ATOM");

    LB30_Inverter *driver = &board->driver.inverter;

    {   // PWM: GTM ATOM configuration
        IfxGtm_Atom_Timer_Config timerConfig;
        IfxGtm_Atom_Timer_initConfig(&timerConfig, &MODULE_GTM);

        timerConfig.base.frequency                  = driverBoardConfig->frequency;
        timerConfig.base.isrPriority                = ISR_PRIORITY(INTERRUPT_M0_PWMHL_PERIOD);
        timerConfig.base.isrProvider                = ISR_PROVIDER(INTERRUPT_M0_PWMHL_PERIOD);
        timerConfig.base.minResolution              = (1.0 / timerConfig.base.frequency) / 1000;
        timerConfig.base.trigger.enabled            = FALSE;
        timerConfig.atom                            = IfxGtm_Atom_0;
        timerConfig.timerChannel                    = IfxGtm_Atom_Ch_0;
        timerConfig.clock                           = IfxGtm_Cmu_Clk_0;
//        timerConfig.base.trigger.isrPriority        = ISR_PRIORITY(INTERRUPT_M0_PWMHL_TRIGGER);
//        timerConfig.base.trigger.isrProvider        = ISR_PROVIDER(INTERRUPT_M0_PWMHL_TRIGGER);
//        timerConfig.base.trigger.outputMode         = IfxPort_OutputMode_pushPull;
//        timerConfig.base.trigger.outputDriver       = IfxPort_PadDriver_cmosAutomotiveSpeed1;
//        timerConfig.base.countDir                   = IfxStdIf_Timer_CountDir_up;

        timerConfig.triggerOut                      = &IfxGtm_ATOM0_7_TOUT7_P02_7_OUT;//&IfxGtm_ATOM0_0_TOUT0_P02_0_OUT;
        timerConfig.base.trigger.outputEnabled      = TRUE;
        timerConfig.base.trigger.enabled            = TRUE;
        timerConfig.base.trigger.triggerPoint       = 10;
        timerConfig.base.trigger.risingEdgeAtPeriod = INVERTER_TRIGGER_RISING_EDGE_AT_PERIOD;

        result                                     &= IfxGtm_Atom_Timer_init(&driver->atom.timerDriver, &timerConfig);

        // Set trigger duty cycle to 50 %. EiceSense trigger requires min 20us low / high state of the signal
//        IfxGtm_Atom_Timer_setTrigger(&driver->atom.timerDriver, IfxGtm_Atom_Timer_getPeriod(&driver->atom.timerDriver)/2);

        // Transfer the new trigger value
//        IfxGtm_Atom_Timer_forceUpdate(&driver->atom.timerDriver);


//        Ifx_Console_printAlign("- Frequency=%9.0f Hz"ENDL, timerConfig.base.frequency);
//        Ifx_Console_printAlign("- Timer ATOM%d.%d"ENDL, timerConfig.atom, timerConfig.timerChannel);
//        Ifx_Console_printAlign("- Trigger ATOM%d.%d, P%d.%d"ENDL, timerConfig.triggerOut->atom, timerConfig.triggerOut->channel, IfxPort_getIndex(timerConfig.triggerOut->pin.port), timerConfig.triggerOut->pin.pinIndex);
//        Ifx_Console_printAlign("- Trigger point=%d ticks"ENDL, IfxGtm_Atom_Timer_getTrigger(&driver->atom.timerDriver));

        IfxGtm_Atom_PwmHl_Config       pwmHlConfig;
        IfxGtm_Atom_PwmHl_initConfig(&pwmHlConfig);
        IFX_CONST IfxGtm_Atom_ToutMapP ccxCh[3] =
        {&IfxGtm_ATOM0_6_TOUT6_P02_6_OUT, &IfxGtm_ATOM0_2_TOUT2_P02_2_OUT, &IfxGtm_ATOM0_4_TOUT4_P02_4_OUT};

        IFX_CONST IfxGtm_Atom_ToutMapP coutxCh[3] =
        {&IfxGtm_ATOM0_1_TOUT1_P02_1_OUT, &IfxGtm_ATOM0_3_TOUT3_P02_3_OUT, &IfxGtm_ATOM0_5_TOUT5_P02_5_OUT};

        pwmHlConfig.base.deadtime         = driverBoardConfig->deadtime;
        pwmHlConfig.base.minPulse         = driverBoardConfig->minPulse;
        pwmHlConfig.base.channelCount     = 3;
        pwmHlConfig.base.emergencyEnabled = FALSE;  /* FIXME Configure Emergency stop*/
        pwmHlConfig.base.outputMode       = IfxPort_OutputMode_pushPull;
        pwmHlConfig.base.outputDriver     = IfxPort_PadDriver_cmosAutomotiveSpeed1;
        pwmHlConfig.base.ccxActiveState   = Ifx_ActiveState_high;
        pwmHlConfig.base.coutxActiveState = Ifx_ActiveState_high;
        pwmHlConfig.timer                 = &driver->atom.timerDriver;
        pwmHlConfig.atom                  = timerConfig.atom;
        pwmHlConfig.ccx                   = ccxCh;
        pwmHlConfig.coutx                 = coutxCh;

        result                           &= IfxGtm_Atom_PwmHl_init(&driver->atom.pwmDriver, &pwmHlConfig);
        result                           &= IfxGtm_Atom_PwmHl_stdIfPwmHlInit(&driver->pwmStdIf, &driver->atom.pwmDriver);

//        IfxGtm_Atom_Timer_run(&driver->atom.timerDriver);
//        Ifx_Console_printAlign("- Dead time=%9.8f s"ENDL, pwmHlConfig.base.deadtime);
//        Ifx_Console_printAlign("- Min pulse=%9.8f s"ENDL, pwmHlConfig.base.minPulse);
//        Ifx_Console_printAlign("- Emergency enabled=%s"ENDL, (pwmHlConfig.base.emergencyEnabled ? "TRUE" : "FALSE"));

        uint32 i;

        for (i = 0; i < 3; i++)
        {
//            Ifx_Console_printAlign("- CC%d ATOM%d.%d, P%d.%d"ENDL, i, pwmHlConfig.atom, ccxCh[i]->channel, IfxPort_getIndex(ccxCh[i]->pin.port), ccxCh[i]->pin.pinIndex);
//            Ifx_Console_printAlign("- COUT%d ATOM%d.%d, P%d.%d"ENDL, i, pwmHlConfig.atom, coutxCh[i]->channel, IfxPort_getIndex(coutxCh[i]->pin.port), coutxCh[i]->pin.pinIndex);
        }

        if ((AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
    		|| (AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
    		)
        {
        	/* Forward trigger ATOM0.CH7 to EiceSil driver using signal ADCT_LS_V. ADCT_LS_W should be bridge on driver board to ADCT_LS_V   */

            IfxGtm_PinMap_setAtomTout(&IfxGtm_ATOM0_7_TOUT25_P33_3_OUT, IfxPort_OutputMode_pushPull, IfxPort_PadDriver_cmosAutomotiveSpeed1);
        }

    }

//    App_printStatus(result);
    return result;
}

boolean LB30_AppInit_Inverter0IomRefTom(LB30 *board)
{   /* Inverter PWM */
    boolean        result = TRUE;
    AppDbStdIf_MainConfig *driverBoardConfig = AppDbStdIf_getMainConfig(board->driverBoardStdif);
//    App_printStatusTitle("Initializing IOM reference PWM on GTM TOM0");

    LB30_Inverter *driver = &board->driver.inverter;

    {   // PWM: GTM TOM configuration
        IfxGtm_Tom_Timer_Config timerConfig;
        IfxGtm_Tom_Timer_initConfig(&timerConfig, &MODULE_GTM);

        timerConfig.base.frequency                  = driverBoardConfig->frequency;
        timerConfig.base.minResolution              = (1.0 / timerConfig.base.frequency) / 1000;
        timerConfig.base.trigger.enabled            = TRUE;
        timerConfig.base.trigger.triggerPoint       = 10;
        timerConfig.base.trigger.outputMode         = IfxPort_OutputMode_pushPull;
        timerConfig.base.trigger.outputDriver       = IfxPort_PadDriver_cmosAutomotiveSpeed1;
        timerConfig.base.trigger.risingEdgeAtPeriod = INVERTER_TRIGGER_RISING_EDGE_AT_PERIOD;
        timerConfig.base.trigger.outputEnabled      = FALSE;
        timerConfig.base.countDir                   = IfxStdIf_Timer_CountDir_up;
        timerConfig.tom                             = IfxGtm_Tom_0;
        timerConfig.timerChannel                    = IfxGtm_Tom_Ch_0;
        timerConfig.triggerOut                      = &IfxGtm_TOM0_0_TOUT26_P33_4_OUT;
        timerConfig.clock                           = IfxGtm_Cmu_Clk_0;

        timerConfig.base.startOffset = 0.01; /* Ensure the Ref arrive 1st at IOM compare to PWM */
        result                                     &= IfxGtm_Tom_Timer_init(&driver->iomRefTom.timerDriver, &timerConfig);

        IfxGtm_Tom_Timer_setTrigger(&driver->iomRefTom.timerDriver, IfxGtm_Tom_Timer_getPeriod(&driver->iomRefTom.timerDriver)/2);

        // Transfer the new trigger value
        IfxGtm_Tom_Timer_forceUpdate(&driver->iomRefTom.timerDriver);


//        Ifx_Console_printAlign("- Frequency=%9.0f Hz"ENDL, timerConfig.base.frequency);
//        Ifx_Console_printAlign("- Timer ATOM%d.%d"ENDL, timerConfig.tom, timerConfig.timerChannel);
//        Ifx_Console_printAlign("- Trigger ATOM%d.%d, P%d.%d"ENDL, timerConfig.triggerOut->tom, timerConfig.triggerOut->channel, IfxPort_getIndex(timerConfig.triggerOut->pin.port), timerConfig.triggerOut->pin.pinIndex);
//        Ifx_Console_printAlign("- Trigger point=%d ticks"ENDL, IfxGtm_Tom_Timer_getTrigger(&driver->iomRefTom.timerDriver));

        IfxGtm_Tom_PwmHl_Config       pwmHlConfig;
        IfxGtm_Tom_PwmHl_initConfig(&pwmHlConfig);
        // TOM1
        IFX_CONST IfxGtm_Tom_ToutMapP ccxCh[3] =
        {&IfxGtm_TOM0_1_TOUT27_P33_5_OUT, &IfxGtm_TOM0_3_TOUT29_P33_7_OUT, &IfxGtm_TOM0_5_TOUT23_P33_1_OUT};
        IFX_CONST IfxGtm_Tom_ToutMapP coutxCh[3] =
        {&IfxGtm_TOM0_2_TOUT28_P33_6_OUT, &IfxGtm_TOM0_4_TOUT22_P33_0_OUT, &IfxGtm_TOM0_6_TOUT24_P33_2_OUT};


        pwmHlConfig.base.deadtime         = driverBoardConfig->deadtime;
        pwmHlConfig.base.minPulse         = driverBoardConfig->minPulse;
        pwmHlConfig.base.channelCount     = 3;
        pwmHlConfig.base.emergencyEnabled = FALSE;  /* FIXME Configure Emergency stop*/
        pwmHlConfig.base.outputMode       = IfxPort_OutputMode_pushPull;
        pwmHlConfig.base.outputDriver     = IfxPort_PadDriver_cmosAutomotiveSpeed1;
        pwmHlConfig.base.ccxActiveState   = Ifx_ActiveState_high;
        pwmHlConfig.base.coutxActiveState = Ifx_ActiveState_high;
//        pwmHlConfig.base.ccxOutputEnabled = 0;	// Signals goes to IOM, not to the pin output
//        pwmHlConfig.base.coutxOutputEnabled = 0;// Signals goes to IOM, not to the pin output
        pwmHlConfig.timer                 = &driver->iomRefTom.timerDriver;
        pwmHlConfig.tom                   = timerConfig.tom;
        pwmHlConfig.ccx                   = ccxCh;
        pwmHlConfig.coutx                 = coutxCh;

        result                           &= IfxGtm_Tom_PwmHl_init(&driver->iomRefTom.pwmDriver, &pwmHlConfig);
        result                           &= IfxGtm_Tom_PwmHl_stdIfPwmHlInit(&driver->iomRefStdIf, &driver->iomRefTom.pwmDriver);

//        Ifx_Console_printAlign("- Dead time=%9.8f s"ENDL, pwmHlConfig.base.deadtime);
//        Ifx_Console_printAlign("- Min pulse=%9.8f s"ENDL, pwmHlConfig.base.minPulse);
//        Ifx_Console_printAlign("- Emergency enabled=%s"ENDL, (pwmHlConfig.base.emergencyEnabled ? "TRUE" : "FALSE"));

        uint32 i;

        for (i = 0; i < 3; i++)
        {
//            Ifx_Console_printAlign("- CC%d TOM%d.%d, P%d.%d"ENDL, i, pwmHlConfig.tom, ccxCh[i]->channel, IfxPort_getIndex(ccxCh[i]->pin.port), ccxCh[i]->pin.pinIndex);
//            Ifx_Console_printAlign("- COUT%d TOM%d.%d, P%d.%d"ENDL, i, pwmHlConfig.tom, coutxCh[i]->channel, IfxPort_getIndex(coutxCh[i]->pin.port), coutxCh[i]->pin.pinIndex);
        }


    }

//    App_printStatus(result);
    return result;
}

boolean LB30_AppInit_Inverter0Iom(LB30 *board)
{   /* Reference PWM for IOM */
    boolean        result = TRUE;
//    App_printStatusTitle("Initializing IOM for ATOM0 <=> TOM1");

    LB30_Inverter *driver = &board->driver.inverter;
    IfxIom_enableModule(&MODULE_IOM, 1);

	/* IOM REF signal is the PWM from pin
	 * IOM MON signal is the PWM from internal timer (Reference)
	 * REF and MON have been selected to match the hardware mapping
	 **/
	IfxIom_Driver_Config configDriver;
	IfxIom_Driver_LamConfig configLam;

	IfxIom_Driver_initConfig(&configDriver, &MODULE_IOM);
	IfxIom_Driver_init(&driver->iom.driver, &configDriver);

	IfxIom_Driver_initLamConfig(&configLam, &driver->iom.driver);


	configLam.ref.filter.mode = IfxIom_LamFilterMode_delayDebounceBothEdge;
	configLam.ref.filter.clearTimerOnGlitch = FALSE;
	configLam.ref.filter.fallingEdgeFilterTime = board->configuration.safety.iom.filterTime;
	configLam.ref.filter.risingEdgeFilterTime = board->configuration.safety.iom.filterTime;
	configLam.ref.inverted = TRUE;

	configLam.mon.filter.mode = IfxIom_LamFilterMode_noFilter;
	configLam.mon.inverted = FALSE;

	configLam.eventWindow.controlSource = IfxIom_LamEventWindowControlSource_mon;
	configLam.eventWindow.run = IfxIom_LamEventWindowRunControl_freeRunning;
	configLam.eventWindow.clearEvent = IfxIom_LamEventWindowClearEvent_anyEdge;
	configLam.eventWindow.threshold =board->configuration.safety.iom.eventWindowThreshold;

	configLam.event.source = IfxIom_LamEventSource_monXorRef;
	configLam.event.trigger = IfxIom_LamEventTrigger_fallingEdge;

	/* Configure for all 6 phases */
	configLam.channel = IfxIom_LamId_0; 			// U Top
	configLam.ref.input = IfxIom_RefInput_p33_0;
	configLam.mon.input= IfxIom_MonInput_gtmTout27;
	IfxIom_Driver_initLam(driver->iom.lam, &configLam);

	configLam.channel = IfxIom_LamId_1; 			// U Bottom
	configLam.ref.input = IfxIom_RefInput_p33_1;
	configLam.mon.input= IfxIom_MonInput_gtmTout28;
	IfxIom_Driver_initLam(driver->iom.lam, &configLam);

	configLam.channel = IfxIom_LamId_2; 			// V Top
	configLam.ref.input = IfxIom_RefInput_p33_2;
	configLam.mon.input= IfxIom_MonInput_gtmTout29;
	IfxIom_Driver_initLam(driver->iom.lam, &configLam);

	configLam.channel = IfxIom_LamId_3; 			// V Bottom
	configLam.ref.input = IfxIom_RefInput_p33_3;
	configLam.mon.input= IfxIom_MonInput_gtmTout22;
	IfxIom_Driver_initLam(driver->iom.lam, &configLam);

	configLam.channel = IfxIom_LamId_4; 			// W Top
	configLam.ref.input = IfxIom_RefInput_p33_4;
	configLam.mon.input= IfxIom_MonInput_gtmTout23;
	IfxIom_Driver_initLam(driver->iom.lam, &configLam);

	configLam.channel = IfxIom_LamId_5; 			// W Bottom
	configLam.ref.input = IfxIom_RefInput_p33_5;
	configLam.mon.input= IfxIom_MonInput_gtmTout24;
	IfxIom_Driver_initLam(driver->iom.lam, &configLam);

	/* Trigger */
	configLam.channel = IfxIom_LamId_6;
	configLam.ref.inverted = FALSE;
	configLam.eventWindow.threshold = 1e-6;
	configLam.ref.input = IfxIom_RefInput_gtmTout7;
	configLam.mon.input= IfxIom_MonInput_gtmTout26;
	IfxIom_Driver_initLam(driver->iom.lam, &configLam);

	driver->iom.eventMask = IfxIom_Driver_disableEvents(&driver->iom.driver);

//    App_printStatus(result);
    return result;
}


boolean LB30_AppInit_Inverter0PwmTom(LB30 *board)
{   /* Inverter PWM */
    boolean        result = TRUE;
    AppDbStdIf_MainConfig *driverBoardConfig = AppDbStdIf_getMainConfig(board->driverBoardStdif);
//    App_printStatusTitle("Initializing Inverter 0 PWM on GTM TOM");

    LB30_Inverter *driver = &board->driver.inverter;

    {   // PWM: GTM TOM configuration
        IfxGtm_Tom_Timer_Config timerConfig;
        IfxGtm_Tom_Timer_initConfig(&timerConfig, &MODULE_GTM);

        timerConfig.base.frequency                  = driverBoardConfig->frequency;
        timerConfig.base.isrPriority                = ISR_PRIORITY(INTERRUPT_M0_PWMHL_PERIOD);
        timerConfig.base.isrProvider                = ISR_PROVIDER(INTERRUPT_M0_PWMHL_PERIOD);
        timerConfig.base.minResolution              = (1.0 / timerConfig.base.frequency) / 1000;
        timerConfig.base.trigger.enabled            = TRUE;
        timerConfig.base.trigger.triggerPoint       = 10;
        timerConfig.base.trigger.isrPriority        = ISR_PRIORITY(INTERRUPT_M0_PWMHL_TRIGGER);
        timerConfig.base.trigger.isrProvider        = ISR_PROVIDER(INTERRUPT_M0_PWMHL_TRIGGER);
        timerConfig.base.trigger.outputMode         = IfxPort_OutputMode_pushPull;
        timerConfig.base.trigger.outputDriver       = IfxPort_PadDriver_cmosAutomotiveSpeed1;
        timerConfig.base.trigger.risingEdgeAtPeriod = INVERTER_TRIGGER_RISING_EDGE_AT_PERIOD;
        timerConfig.base.trigger.outputEnabled      = TRUE;
        timerConfig.base.countDir                   = IfxStdIf_Timer_CountDir_up;
        timerConfig.tom                             = IfxGtm_Tom_0;
        timerConfig.timerChannel                    = IfxGtm_Tom_Ch_8;
        timerConfig.triggerOut                      = &IfxGtm_ATOM0_6_TOUT59_P20_0_OUT;	//&IfxGtm_TOM0_15_TOUT7_P02_7_OUT;
        timerConfig.clock                           = IfxGtm_Cmu_Clk_0;
        result                                     &= IfxGtm_Tom_Timer_init(&driver->tom.timerDriver, &timerConfig);

        // Set trigger duty cycle to 50 %. EiceSense trigger requires min 20us low / high state of the signal
        IfxGtm_Tom_Timer_setTrigger(&driver->tom.timerDriver, IfxGtm_Tom_Timer_getPeriod(&driver->tom.timerDriver)/2);

        // Transfer the new trigger value
        IfxGtm_Tom_Timer_forceUpdate(&driver->tom.timerDriver);


//        Ifx_Console_printAlign("- Frequency=%9.0f Hz"ENDL, timerConfig.base.frequency);
//        Ifx_Console_printAlign("- Timer TOM%d.%d"ENDL, timerConfig.tom, timerConfig.timerChannel);
//        Ifx_Console_printAlign("- Trigger TOM%d.%d, P%d.%d"ENDL, timerConfig.triggerOut->tom, timerConfig.triggerOut->channel, IfxPort_getIndex(timerConfig.triggerOut->pin.port), timerConfig.triggerOut->pin.pinIndex);
//        Ifx_Console_printAlign("- Trigger point=%d ticks"ENDL, IfxGtm_Tom_Timer_getTrigger(&driver->tom.timerDriver));

        IfxGtm_Tom_PwmHl_Config       pwmHlConfig;
        IfxGtm_Tom_PwmHl_initConfig(&pwmHlConfig);
        IFX_CONST IfxGtm_Tom_ToutMapP ccxCh[3] =
        {&IfxGtm_TOM0_14_TOUT6_P02_6_OUT, &IfxGtm_TOM0_10_TOUT2_P02_2_OUT, &IfxGtm_TOM0_12_TOUT4_P02_4_OUT};

        IFX_CONST IfxGtm_Tom_ToutMapP coutxCh[3] =
        {&IfxGtm_TOM0_9_TOUT1_P02_1_OUT, &IfxGtm_TOM0_11_TOUT3_P02_3_OUT, &IfxGtm_TOM0_13_TOUT5_P02_5_OUT};

        pwmHlConfig.base.deadtime         = driverBoardConfig->deadtime;
        pwmHlConfig.base.minPulse         = driverBoardConfig->minPulse;
        pwmHlConfig.base.channelCount     = 3;
        pwmHlConfig.base.emergencyEnabled = FALSE;  /* FIXME Configure Emergency stop*/
        pwmHlConfig.base.outputMode       = IfxPort_OutputMode_pushPull;
        pwmHlConfig.base.outputDriver     = IfxPort_PadDriver_cmosAutomotiveSpeed1;
        pwmHlConfig.base.ccxActiveState   = Ifx_ActiveState_high;
        pwmHlConfig.base.coutxActiveState = Ifx_ActiveState_high;
        pwmHlConfig.timer                 = &driver->tom.timerDriver;
        pwmHlConfig.tom                   = timerConfig.tom;
        pwmHlConfig.ccx                   = ccxCh;
        pwmHlConfig.coutx                 = coutxCh;

        result                           &= IfxGtm_Tom_PwmHl_init(&driver->tom.pwmDriver, &pwmHlConfig);
        result                           &= IfxGtm_Tom_PwmHl_stdIfPwmHlInit(&driver->pwmStdIf, &driver->tom.pwmDriver);

//        Ifx_Console_printAlign("- Dead time=%9.8f s"ENDL, pwmHlConfig.base.deadtime);
//        Ifx_Console_printAlign("- Min pulse=%9.8f s"ENDL, pwmHlConfig.base.minPulse);
//        Ifx_Console_printAlign("- Emergency enabled=%s"ENDL, (pwmHlConfig.base.emergencyEnabled ? "TRUE" : "FALSE"));

        uint32 i;

        for (i = 0; i < 3; i++)
        {
//            Ifx_Console_printAlign("- CC%d ATOM%d.%d, P%d.%d"ENDL, i, pwmHlConfig.tom, ccxCh[i]->channel, IfxPort_getIndex(ccxCh[i]->pin.port), ccxCh[i]->pin.pinIndex);
//            Ifx_Console_printAlign("- COUT%d ATOM%d.%d, P%d.%d"ENDL, i, pwmHlConfig.tom, coutxCh[i]->channel, IfxPort_getIndex(coutxCh[i]->pin.port), coutxCh[i]->pin.pinIndex);
        }


    }

//    App_printStatus(result);
    return result;
}


boolean LB30_AppInit_Inverter0PwmCcu6(LB30 *board)
{
    boolean        result = TRUE;
    AppDbStdIf_MainConfig *driverBoardConfig = AppDbStdIf_getMainConfig(board->driverBoardStdif);
//    App_printStatusTitle("Initializing Inverter 0 PWM on CCU60");

    LB30_Inverter *driver = &board->driver.inverter;

    {   // GTM ATOM configuration
        IfxCcu6_TimerWithTrigger_Config timerConfig;
        IfxCcu6_TimerWithTrigger_initConfig(&timerConfig, &MODULE_CCU60);

        timerConfig.base.frequency                  = driverBoardConfig->frequency;
        timerConfig.base.minResolution              = (1.0 / timerConfig.base.frequency) / 1000;
        timerConfig.base.trigger.enabled            = TRUE;
        timerConfig.base.trigger.triggerPoint       = 10;
        timerConfig.base.trigger.outputMode         = IfxPort_OutputMode_pushPull;
        timerConfig.base.trigger.outputDriver       = IfxPort_PadDriver_cmosAutomotiveSpeed1;
        timerConfig.base.trigger.risingEdgeAtPeriod = INVERTER_TRIGGER_RISING_EDGE_AT_PERIOD;
        timerConfig.base.trigger.outputEnabled      = TRUE;
        timerConfig.base.countDir                   = IfxStdIf_Timer_CountDir_upAndDown;
        timerConfig.triggerOut                      = &IfxCcu60_COUT63_P00_0_OUT;
        result                                     &= IfxCcu6_TimerWithTrigger_init(&driver->ccu6.timerDriver, &timerConfig);

//        Ifx_Console_printAlign("- Frequency=%9.0f Hz"ENDL, timerConfig.base.frequency);
//        Ifx_Console_printAlign("- Trigger P%d.%d"ENDL, IfxPort_getIndex(timerConfig.triggerOut->pin.port), timerConfig.triggerOut->pin.pinIndex);
//        Ifx_Console_printAlign("- Trigger point=%d ticks"ENDL, timerConfig.base.trigger.triggerPoint);

        IfxCcu6_PwmHl_Config pwmHlConfig;
        IfxCcu6_PwmHl_initConfig(&pwmHlConfig);

        pwmHlConfig.base.deadtime         = driverBoardConfig->deadtime;
        pwmHlConfig.base.minPulse         = driverBoardConfig->minPulse;
        pwmHlConfig.base.channelCount     = 3;
        pwmHlConfig.base.emergencyEnabled = FALSE;
        pwmHlConfig.base.outputMode       = IfxPort_OutputMode_pushPull;
        pwmHlConfig.base.outputDriver     = IfxPort_PadDriver_cmosAutomotiveSpeed1;
        pwmHlConfig.base.ccxActiveState   = Ifx_ActiveState_high;
        pwmHlConfig.base.coutxActiveState = Ifx_ActiveState_high;
        pwmHlConfig.timer                 = &driver->ccu6.timerDriver;
        pwmHlConfig.cc0                   = &IfxCcu60_CC60_P02_6_OUT;
        pwmHlConfig.cc1                   = &IfxCcu60_CC61_P02_2_OUT;
        pwmHlConfig.cc2                   = &IfxCcu60_CC62_P02_4_OUT;
        pwmHlConfig.cout0                 = &IfxCcu60_COUT60_P02_1_OUT;
        pwmHlConfig.cout1                 = &IfxCcu60_COUT61_P02_3_OUT;
        pwmHlConfig.cout2                 = &IfxCcu60_COUT62_P02_5_OUT;

        result                           &= IfxCcu6_PwmHl_init(&driver->ccu6.pwmDriver, &pwmHlConfig);
        result                           &= IfxCcu6_PwmHl_stdIfPwmHlInit(&driver->pwmStdIf, &driver->ccu6.pwmDriver);

//        Ifx_Console_printAlign("- Dead time=%9.8f s"ENDL, pwmHlConfig.base.deadtime);
//        Ifx_Console_printAlign("- Min pulse=%9.8f s"ENDL, pwmHlConfig.base.minPulse);
//        Ifx_Console_printAlign("- Emergency enabled=%s"ENDL, (pwmHlConfig.base.emergencyEnabled ? "TRUE" : "FALSE"));

//        Ifx_Console_printAlign("- CC0 P%d.%d"ENDL, IfxPort_getIndex(pwmHlConfig.cc0->pin.port), pwmHlConfig.cc0->pin.pinIndex);
//        Ifx_Console_printAlign("- COUT0 P%d.%d"ENDL, IfxPort_getIndex(pwmHlConfig.cout0->pin.port), pwmHlConfig.cout0->pin.pinIndex);
//        Ifx_Console_printAlign("- CC1 P%d.%d"ENDL, IfxPort_getIndex(pwmHlConfig.cc1->pin.port), pwmHlConfig.cc1->pin.pinIndex);
//        Ifx_Console_printAlign("- COUT1 P%d.%d"ENDL, IfxPort_getIndex(pwmHlConfig.cout1->pin.port), pwmHlConfig.cout1->pin.pinIndex);
//        Ifx_Console_printAlign("- CC2 P%d.%d"ENDL, IfxPort_getIndex(pwmHlConfig.cc2->pin.port), pwmHlConfig.cc2->pin.pinIndex);
//        Ifx_Console_printAlign("- COUT2 P%d.%d"ENDL, IfxPort_getIndex(pwmHlConfig.cout2->pin.port), pwmHlConfig.cout2->pin.pinIndex);
    }
//    App_printStatus(result);

    return result;
}


boolean LB30_AppInit_Inverter0(LB30 *board)
{   /* Inverter interface */
    boolean        result = TRUE;

//    App_printStatusTitle("Initializing Inverter 0");

    LB30_Inverter *driver = &board->driver.inverter;
    uint32         i;
    driver->deadtimeComp = 0.0;

    for (i = 0; i < ECU_PHASE_PER_MOTOR_COUNT; i++)
    {
        driver->duty[i] = 0;
    }

    /* FIXME add emergency stop */

    // FIXME to do result &= LB30_AppInit_Inverter0Monitoring(); /* Preferred solution from Klaus: ATOM0 for generation / ATOM1 for IOM. Add SMU check at startup. Could a EiceSil driver disable be ok? */

    /* FIXME implement error out */
    Lb_BoardType driverBoardType = AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType;
    if (
    		(driverBoardType == Lb_BoardType_HybridKit_DriverBoardHp2)
    		|| (driverBoardType == Lb_BoardType_HybridKit_DriverBoardHpDrive)
    		|| (driverBoardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
    		)
    {   /* IGBT temp */
        IfxGtm_Tim_In_Config config;
        IfxGtm_Tim_In_initConfig(&config, &MODULE_GTM);
        config.capture.irqOnNewVal          = TRUE;
        config.capture.irqOnCntOverflow     = FALSE;
        config.capture.irqOnEcntOverflow    = FALSE;
        config.capture.irqOnDatalost        = FALSE;
        config.capture.clock                = IfxGtm_Cmu_Clk_0;
        config.capture.mode                 = Ifx_Pwm_Mode_leftAligned;

        config.filter.clock                 = IfxGtm_Cmu_Tim_Filter_Clk_0;
        config.filter.risingEdgeMode        = IfxGtm_Tim_In_ConfigFilterMode_none;
        config.filter.fallingEdgeMode       = IfxGtm_Tim_In_ConfigFilterMode_none;
        config.filter.risingEdgeFilterTime  = 0.0;
        config.filter.fallingEdgeFilterTime = 0.0;
        config.filter.irqOnGlitch           = FALSE;
        config.filter.inputPinMode          = IfxPort_InputMode_noPullDevice;

        config.timeout.clock                = IfxGtm_Cmu_Clk_3;
        config.timeout.timeout              = 10e-3;
        config.timeout.irqOnTimeout         = FALSE;

        config.isrPriority                  = ISR_PRIORITY(INTERRUPT_IGBT_TEMPU);
        config.isrProvider                  = ISR_PROVIDER(INTERRUPT_IGBT_TEMPU);
        config.filter.inputPin              = &IfxGtm_TIM3_5_P22_9_IN;
        result                             &= IfxGtm_Tim_In_init(&board->driver.inverter.igbtTemp[0], &config);

        config.isrPriority                  = ISR_PRIORITY(INTERRUPT_IGBT_TEMPV);
        config.isrProvider                  = ISR_PROVIDER(INTERRUPT_IGBT_TEMPV);
        config.filter.inputPin              = &IfxGtm_TIM3_6_P22_10_IN;
        result                             &= IfxGtm_Tim_In_init(&board->driver.inverter.igbtTemp[1], &config);

        config.isrPriority                  = ISR_PRIORITY(INTERRUPT_IGBT_TEMPW);
        config.isrProvider                  = ISR_PROVIDER(INTERRUPT_IGBT_TEMPW);
        config.filter.inputPin              = &IfxGtm_TIM3_7_P22_11_IN;
        result                             &= IfxGtm_Tim_In_init(&board->driver.inverter.igbtTemp[2], &config);
    }

    {   /* Message */
        Ifx_Message_Config messageConfig;

        Ifx_Message_initConfig(&messageConfig);
        messageConfig.count      = LB30_Message_COUNT;
        messageConfig.entries    = g_LB30_Messages;
        messageConfig.owner      = (uint32)driver;
        messageConfig.ownerName  = "Inverter";
        messageConfig.standardIo = &Ifx_g_console.standardIo;
        Ifx_Message_init(&driver->messages, &messageConfig);
    }

    result &= LB30_Inverter_stdIfInverterInit(&board->stdIf.inverter, board);

//    App_printStatus(result);

    result &= AppDbStdIf_initDriver(board->driverBoardStdif, board->driver.qspi0, board->boardVersion);

    if (board->configuration.inverter[0].pwmModule == LB_PwmModule_gtmAtom)
    {
        result &= LB30_AppInit_Inverter0PwmAtom(board);

        if(board->configuration.safety.iom.enabled)
        {
        	/* Initialize IOM */
            result &= LB30_AppInit_Inverter0IomRefTom(board);
            result &= LB30_AppInit_Inverter0Iom(board);
        }
    }
    else if (board->configuration.inverter[0].pwmModule == LB_PwmModule_gtmTom)
    {
        result &= LB30_AppInit_Inverter0PwmTom(board);
    }
    else// if (board->configuration.inverter[0].pwmModule == LB_PwmModule_ccu6)
    {
        result &= LB30_AppInit_Inverter0PwmCcu6(board);
    }

    return result;
}


/** \brief Initialise the position sensor interfaces */
boolean LB30_AppInit_encoderGpt12(LB30 *board)
{
    boolean result = TRUE;
    AppDbStdIf_MainConfig *driverBoardConfig = AppDbStdIf_getMainConfig(board->driverBoardStdif);
//    App_printStatusTitle("Initializing Encoder on GPT12");

    /** - Incremental encoder with GPT12 */
    IfxGpt12_enableModule(&MODULE_GPT120);
    IfxGpt12_setGpt1BlockPrescaler(&MODULE_GPT120, IfxGpt12_Gpt1BlockPrescaler_8);
    IfxGpt12_setGpt2BlockPrescaler(&MODULE_GPT120, IfxGpt12_Gpt2BlockPrescaler_4);

    IfxGpt12_IncrEnc_Config config;
    IfxGpt12_IncrEnc_initConfig(&config, &MODULE_GPT120);
    config.base.offset                    = board->configuration.positionSensors.encoder.offset;
    config.base.reversed                  = board->configuration.positionSensors.encoder.reversed;
    config.base.periodPerRotation         = 1;
    config.base.resolution                = board->configuration.positionSensors.encoder.resolution;
    config.base.updatePeriod              = 1.0 / driverBoardConfig->frequency;
    config.base.minSpeed                  = 0.5 / 60 * 2 * IFX_PI;   // 0.5 rpm
    config.base.maxSpeed                  = 10000 / 60 * 2 * IFX_PI; // 10000 rpm FIXME set this value depending on motor max speed

    config.zeroIsrPriority                = ISR_PRIORITY(INTERRUPT_ENCODER_GPT12);
    config.zeroIsrProvider                = ISR_PROVIDER(INTERRUPT_ENCODER_GPT12);
    config.pinA                           = &IfxGpt120_T2INA_P00_7_IN;		//&IfxGpt120_T3INB_P10_4_IN;
    config.pinB                           = &IfxGpt120_T2EUDA_P00_8_IN;		//&IfxGpt120_T3EUDB_P10_7_IN;
    config.pinZ                           = &IfxGpt120_T4EUDA_P00_9_IN;		//&IfxGpt120_T4INB_P10_8_IN;
    config.pinMode                        = IfxPort_InputMode_noPullDevice;
    config.base.speedFilterEnabled        = TRUE;
    config.base.speedFilerCutOffFrequency = config.base.maxSpeed / 2 * IFX_PI * 5;
    config.base.resolutionFactor          = IfxStdIf_Pos_ResolutionFactor_fourFold;
    config.base.updatePeriod              = IfxStdIf_Inverter_getPeriod(&board->stdIf.inverter);
#if 0
    /* FIXME set value */
    config.base.speedModeThreshold =;
#endif

    result &= IfxGpt12_IncrEnc_init(&board->driver.encoder, &config);
    result &= IfxGpt12_IncrEnc_stdIfPosInit(&board->stdIf.encoder, &board->driver.encoder);

//    Ifx_Console_printAlign("- Resolution=%d"ENDL, config.base.resolution);
//    Ifx_Console_printAlign("- Resolution factor=%d"ENDL, config.base.resolutionFactor);
//    Ifx_Console_printAlign("- A P%d.%d"ENDL, IfxPort_getIndex(config.pinA->pin.port), config.pinA->pin.pinIndex);
//    Ifx_Console_printAlign("- B P%d.%d"ENDL, IfxPort_getIndex(config.pinB->pin.port), config.pinB->pin.pinIndex);
//    Ifx_Console_printAlign("- Z P%d.%d"ENDL, IfxPort_getIndex(config.pinZ->pin.port), config.pinZ->pin.pinIndex);

//    App_printStatus(result);
    return result;
}


boolean LB30_AppInit_Igmr(LB30 *board)
{
    boolean           result = TRUE;
//    App_printStatusTitle("Initializing iGMR");

    /* Initialize the SPI channel */
    IfxQspi_SpiMaster_ChannelConfig spiChannelConfig;
     IfxQspi_SpiMaster_initChannelConfig(&spiChannelConfig, &board->driver.qspi5);
     spiChannelConfig.base.baudrate             = 3e6;
     spiChannelConfig.base.mode.enabled         = TRUE;
     spiChannelConfig.base.mode.autoCS          = 1; /* Required to be set to 1 here, but will be overwritten by the TLE5012 driver */
     spiChannelConfig.base.mode.loopback        = FALSE;
     spiChannelConfig.base.mode.clockPolarity   = SpiIf_ClockPolarity_idleLow;
     spiChannelConfig.base.mode.shiftClock      = SpiIf_ShiftClock_shiftTransmitDataOnLeadingEdge;
     spiChannelConfig.base.mode.dataHeading     = SpiIf_DataHeading_msbFirst;
     spiChannelConfig.base.mode.dataWidth       = 16;
     spiChannelConfig.base.mode.csActiveLevel   = Ifx_ActiveState_low;
     spiChannelConfig.base.mode.csLeadDelay     = SpiIf_SlsoTiming_1;
     spiChannelConfig.base.mode.csTrailDelay    = SpiIf_SlsoTiming_1;
     spiChannelConfig.base.mode.csInactiveDelay = SpiIf_SlsoTiming_1;
     spiChannelConfig.base.mode.parityCheck     = FALSE;
     spiChannelConfig.base.mode.parityMode      = Ifx_ParityMode_even;
     spiChannelConfig.base.errorChecks.baudrate = FALSE;
     spiChannelConfig.base.errorChecks.phase    = FALSE;
     spiChannelConfig.base.errorChecks.receive  = FALSE;
     spiChannelConfig.base.errorChecks.transmit = FALSE;

//     spiChannelConfig.sls.output.pin            = &IfxQspi4_SLSO5_P23_4_OUT;		// 20191212 Jimmy Modified
     spiChannelConfig.sls.output.mode           = IfxPort_OutputMode_pushPull;
     spiChannelConfig.sls.output.driver         = IfxPort_PadDriver_cmosAutomotiveSpeed1;

     result                                    &= IfxQspi_SpiMaster_initChannel(&board->driver.sscTle5012, &spiChannelConfig) == SpiIf_Status_ok;
//     Ifx_Console_printAlign("- QSPI%d, CS%d, %9.0f baud"ENDL, IfxQspi_getIndex(board->driver.qspi5.qspi), spiChannelConfig.sls.output.pin->slsoNr, spiChannelConfig.base.baudrate);


     {
    	 IfxTle5012_Driver_Config config;
    	 IfxTle5012_Driver_initConfig(&config);
		 config.chipSelect = spiChannelConfig.sls.output.pin->pin;
		 config.reversed = board->configuration.positionSensors.tle5012.reversed;
		 config.offset = board->configuration.positionSensors.tle5012.offset;
		 config.slaveReceive = (IfxPort_Pin){&MODULE_P33, 15};
		 config.channel = &board->driver.sscTle5012.base;
		 config.updatePeriod = 1e-3;
         result &= IfxTle5012_Driver_init(&board->driver.tle5012, &config);
         result &= IfxTle5012_Driver_stdIfPosInit(&board->stdIf.tle5012, &board->driver.tle5012);

     }
     {
         const IfxPort_Pin index = {&MODULE_P33, 14};
         /* Enable the iGMR driver  */
         Pin_setState(&index, IfxPort_State_low);
         Pin_setMode(&index, IfxPort_Mode_outputPushPullGeneral);
     }

//    App_printStatus(result);
    return result;
}


boolean LB30_AppInit_DsadcRdc(LB30 *board, uint8 index)
{
    boolean                         result = TRUE;
//    App_printStatusTitle("Initializing Resolver %d on AURIX DSADC", index);

    IfxDsadc_Rdc_Config               config;
    IfxDsadc_Rdc_ConfigHw             ConfigHw;
    IfxEdsadc_Edsadc_CarrierGenConfig configCarrier;
    IfxEdsadc_Edsadc_ChannelConfig    configChannel;


    /* FIXME add initConfig() */
    uint8 signDelay = 1;                                                                            /* APPLICATION SPECIFIC */

    /** Carrier generation configuration */
    configCarrier.bitReversed         = TRUE;                                           /* Default */
    configCarrier.carrierWaveformMode = IfxEdsadc_CarrierWaveformMode_sine;              /* Default */
    configCarrier.frequency           = (float32)board->configuration.positionSensors.aurixResolver.carrierFrequency;                                           /* Default. This is only the expected frequency, real frequency may differ */
    configCarrier.inverted            = FALSE;                                          /* Default */
    configCarrier.pinDriver           = IfxPort_PadDriver_cmosAutomotiveSpeed1;         /* Default. recommended, but it can be also board specific. */
    configCarrier.pinMode             = IfxPort_OutputMode_pushPull;                    /* Default. recommended, but it can be also board specific. */
    configCarrier.pinNeg              = &IfxEdsadc_CGPWMN_P00_5_OUT;                     /* APPLICATION SPECIFIC */
    configCarrier.pinPos              = &IfxEdsadc_CGPWMP_P00_6_OUT;                     /* APPLICATION SPECIFIC */

    /** Default configuration for one DSADC resolver input channel  */

    configChannel.auxFilter.bypassed             = TRUE;                                          /* Default. Don't change */
//    configChannel.auxFilter.combFilterShift      = IfxEdsadc_AuxCombFilterShift_noShift;           /* Don't care, unused */
//    configChannel.auxFilter.combFilterType       = IfxEdsadc_AuxCombFilterType_comb1;              /* Don't care, unused */
    configChannel.auxFilter.decimationFactor     = 4;                                             /* Don't care, unused */
//    configChannel.auxFilter.eventGate            = IfxEdsadc_AuxGate_definedByESEL;                /* Don't care, unused */
//    configChannel.auxFilter.eventSelect          = IfxEdsadc_AuxEvent_everyNewResult;              /* Don't care, unused */
//    configChannel.auxFilter.serviceRequest       = IfxEdsadc_AuxServiceRequest_never;              /* Default. Don't change */

    configChannel.channelId                      = 0;                                             /* Don't care, will be overwritten by driver */
	configChannel.channelPins                    = NULL_PTR;                                      /* Don't care, will be overwritten by driver */


//    configChannel.combFilter.bypassed            = FALSE;                                         /* Default. Don't change */
//    configChannel.combFilter.combFilterShift     = IfxEdsadc_MainCombFilterShift_shiftBy3;         /* APPLICATION SPECIFIC */
//    configChannel.combFilter.combFilterType      = IfxEdsadc_MainCombFilterType_comb3;             /* APPLICATION SPECIFIC */
    configChannel.combFilter.decimationFactor    = 16;                                            /* APPLICATION SPECIFIC */
//    configChannel.combFilter.serviceRequest      = IfxEdsadc_MainServiceRequest_everyNewResult;    /* Default. Don't change */
    configChannel.combFilter.startValue          = 1;                                             /* Default. Don't change */

//    configChannel.demodulator.inputDataSource    = IfxEdsadc_InputDataSource_onChipStandAlone;     /* Default. Don't change */
    configChannel.demodulator.inputDataSource	 = IfxEdsadc_InputDataSource_directInputA;		// 20191214 Jimmy DIY
    configChannel.demodulator.integrationTrigger = IfxEdsadc_IntegratorTrigger_alwaysActive;       /* Default. Don't change */
    configChannel.demodulator.sampleClockSource  = IfxEdsadc_SampleClockSource_internal;           /* Default. Don't change */
//    configChannel.demodulator.sampleStrobe       = IfxEdsadc_SampleStrobe_sampleOnRisingEdge;      /* Default. Don't change */
    configChannel.demodulator.timeStampMode       = TRUE;		// 20191214 Jimmy DIY
    configChannel.demodulator.timestampTrigger   = (INVERTER_TRIGGER_RISING_EDGE_AT_PERIOD
                                                    ? IfxEdsadc_TimestampTrigger_fallingEdge
                                                    : IfxEdsadc_TimestampTrigger_risingEdge);
    configChannel.demodulator.triggerInput       = IfxEdsadc_TriggerInput_a;                       /* APPLICATION SPECIFIC */

//    configChannel.firFilter.dataShift            = IfxEdsadc_FirDataShift_shiftBy2;                /* APPLICATION SPECIFIC */
    configChannel.firFilter.fir0Enabled          = TRUE;                                          /* APPLICATION SPECIFIC */
    configChannel.firFilter.fir1Enabled          = TRUE;                                          /* APPLICATION SPECIFIC */
//    configChannel.firFilter.internalShift        = IfxEdsadc_FirInternalShift_noShift;             /* APPLICATION SPECIFIC */
    configChannel.firFilter.offsetCompensation   = TRUE;                                          /* Default. Don't change */

    configChannel.integrator.discardCount        = signDelay + 1;                                 /* Default */
    configChannel.integrator.integrationCount    = 16;                                            /* Default */
    configChannel.integrator.integrationCycles   = 1;                                             /* Default. Don't change */
    configChannel.integrator.windowSize          = IfxEdsadc_IntegrationWindowSize_internalControl;/* Default. Don't change */

//    configChannel.modulator.commonModeVoltage    = IfxEdsadc_CommonModeVoltage_b;                  /* Default */
    configChannel.modulator.inputGain            = IfxEdsadc_InputGain_factor1;                    /* Default */
    configChannel.modulator.inputPin             = IfxEdsadc_InputPin_a;                           /* Don't care, will be overwritten by driver */
    configChannel.modulator.modulatorClockFreq   = 10.0e6;                                        /* Default. This is only the expected frequency, real frequency may differ */
    configChannel.modulator.negativeInput        = IfxEdsadc_InputConfig_inputPin;                 /* Default. Don't change */
    configChannel.modulator.positiveInput        = IfxEdsadc_InputConfig_inputPin;                 /* Default. Don't change */

//  20191214 Jimmy add
    configChannel.commonMode.commonModeHoldVoltage = IfxEdsadc_CommonModeHoldVoltage_enable;
//    configChannel.commonMode.fractionalRefVoltage  = ;
//    configChannel.commonMode.halfSupplyVoltge	   = ;
//    configChannel.commonMode.negativeInput		   = ;
//    configChannel.commonMode.positiveInput         = ;
//    configChannel.commonMode.refVoltageEnable	   = ;

//	20191214 end

    configChannel.module                         = &MODULE_EDSADC;                                 /* Default */

    configChannel.rectifier.enabled              = TRUE;                                          /* Default. Don't change */
    configChannel.rectifier.signDelay            = signDelay;                                     /* Default */
    configChannel.rectifier.signPeriod           = 16;                                            /* Default */
    configChannel.rectifier.signSource           = IfxEdsadc_RectifierSignSource_onChipGenerator;  /* Default */

    /** Hardware configuration for DSADC resolver interface */
    ConfigHw.carrierGen                          = configCarrier;

    /* Here DSADC event is connected into a TIM channel. In this example, TIM0 CH0 is used.
     * Then, from tc27xC_um_v2.0OM.pdf Table 26-72 or Table 26-73, MUX value of b1011 is obtained */
    ConfigHw.gtmTimestamp.gtm                    = &MODULE_GTM;
    /* IfxGtm_ATOM0_7_TOUT7_P02_7 can be connected to TIM1 CH7 with mux. value b0001 */
    ConfigHw.gtmTimestamp.pwmTim                 = &IfxGtm_TIM1_7_P02_7_IN;//&IfxGtm_TIM1_0_P02_0_IN;
    ConfigHw.gtmTimestamp.rdcTim                 = IfxGtm_Tim_1;
    ConfigHw.gtmTimestamp.rdcTimChannel          = IfxGtm_Tim_Ch_6;
    ConfigHw.gtmTimestamp.rdcTimMuxValue         = 0xE;

    ConfigHw.inputConfig                         = configChannel;
    if (index == 0)
    {
        ConfigHw.inputCosP                           = &IfxEdsadc_DS0PA_AN2_IN;
        ConfigHw.inputCosN                           = &IfxEdsadc_DS0NA_AN3_IN;
        ConfigHw.inputSinP                           = &IfxEdsadc_DS3PA_AN0_IN;
        ConfigHw.inputSinN                           = &IfxEdsadc_DS3NA_AN1_IN;
    }
    else
    {
        ConfigHw.inputCosP                           = &IfxEdsadc_DS2PA_AN20_IN;
        ConfigHw.inputCosN                           = &IfxEdsadc_DS2NA_AN21_IN;
        ConfigHw.inputSinP                           = &IfxEdsadc_DS1PC_AN44_IN;
        ConfigHw.inputSinN                           = &IfxEdsadc_DS1NC_AN45_IN;
    }


    ConfigHw.outputClock                         = NULL_PTR;
    ConfigHw.servReqPriority                     = ISR_PRIORITY_DSADC_RDC0;
    ConfigHw.servReqProvider                     = ISR_PROVIDER_DSADC_RDC0;
    ConfigHw.startScan                           = FALSE;

    /** Configuration for DSADC resolver interface */
    /* set ALL gain to zero for using default values */
    config.kd                = 0;
    config.ki                = 0;
    config.kp                = 0;
    config.errorThreshold    = 1.0f / 180 * IFX_PI;                                 /* Default */
    config.hardware          = ConfigHw;
    config.offset            = board->configuration.positionSensors.aurixResolver.input[index].offset;     /* APPLICATION SPECIFIC */
    config.periodPerRotation = board->configuration.positionSensors.aurixResolver.input[index].periodPerRotation;                                                   /* APPLICATION SPECIFIC */
    config.resolution        = board->configuration.positionSensors.aurixResolver.input[index].resolution; /* APPLICATION SPECIFIC */
    config.reversed          = board->configuration.positionSensors.aurixResolver.input[index].reversed;   /* APPLICATION SPECIFIC */
    config.speedLpfFc        = 100;                                                 /* Default */
    config.sqrAmplMax        = board->configuration.positionSensors.aurixResolver.input[index].signalAmplitudeMax * board->configuration.positionSensors.aurixResolver.input[index].signalAmplitudeMax;                                         /* APPLICATION SPECIFIC */
    config.sqrAmplMin        = board->configuration.positionSensors.aurixResolver.input[index].signalAmplitudeMin * board->configuration.positionSensors.aurixResolver.input[index].signalAmplitudeMin;                                         /* APPLICATION SPECIFIC */
    config.userTs            = IfxStdIf_Inverter_getPeriod(&board->stdIf.inverter); /* Default */
    config.dsadc             = &board->driver.dsadc;                                /* Default */

    result                  &= IfxDsadc_Rdc_init(&board->driver.dsadcRdc[index], &config);
    result                  &= IfxDsadc_Rdc_stdIfPosInit(&board->stdIf.dsadcRdc[index], &board->driver.dsadcRdc[index]);

//    App_printStatus(result);
    return result;
}


boolean LB30_AppInit_Dsadc(LB30 *board)
{
    IfxEdsadc_Edsadc_Config modCfg;
    IfxEdsadc_Edsadc_initModuleConfig(&modCfg, &MODULE_EDSADC);
    IfxEdsadc_Edsadc_initModule(&board->driver.dsadc, &modCfg);
    return TRUE;
}


boolean LB30_AppInit_resolverMux0(LB30 *board)
{
    boolean result = TRUE;
//    App_printStatusTitle("Initializing Resolver 0 gains selection");

    {
        /* Resolver 0 Gain multiplexer settings */

        uint32 i;

        board->driver.Resolver0GainSel[0] = (IfxPort_Pin) {&MODULE_P32, 6};
        board->driver.Resolver0GainSel[1] = (IfxPort_Pin) {&MODULE_P32, 7};
        board->driver.Resolver0GainSel[2] = (IfxPort_Pin) {&MODULE_P23, 6};
        board->driver.Resolver0GainSel[3] = (IfxPort_Pin) {&MODULE_P23, 7};


        if (board->boardVersion->boardVersion >= LB_BoardVersion_HybridKit_LogicBoard_3_1)
        {
        	if (board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver0)
        	{
                LB31_setResolver0Gain(board, board->configuration.positionSensors.aurixResolver.input[0].gainCode);
        	}
        	else
        	{
                LB31_setResolver0Gain(board, board->configuration.positionSensors.ad2s1210.gainCode);
        	}
        }
        else
        {
        	if (board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver0)
        	{
                LB30_setResolver0Gain(board, board->configuration.positionSensors.aurixResolver.input[0].gainCode);
        	}
        	else
        	{
                LB30_setResolver0Gain(board, board->configuration.positionSensors.ad2s1210.gainCode);
        	}
        }

        for (i = 0; i < 4; i++)
        {
/*
            Ifx_Console_printAlign("- Resolver 0 Gain Sel[%d]=P%d.%d"ENDL
                , i
                , IfxPort_getIndex(board->driver.Resolver0GainSel[i].port)
                , board->driver.Resolver0GainSel[i].pinIndex);
*/
        }

        for (i = 0; i < 4; i++)
        {
            Pin_setMode(&board->driver.Resolver0GainSel[i], IfxPort_Mode_outputPushPullGeneral);
            Pin_setDriver(&board->driver.Resolver0GainSel[i], IfxPort_PadDriver_cmosAutomotiveSpeed1);
        }
    }

    board->driver.ResolverOutputSel = (IfxPort_Pin) {&MODULE_P20, 7};

    if (board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_ad2s1210)
    {
        LB30_setResolverOutputSelect(board, LB30_ResolverOutputSelect_ad2s1210);
    }
    else
    {
        LB30_setResolverOutputSelect(board, LB30_ResolverOutputSelect_dsadcRdc);
    }

    Pin_setMode(&board->driver.ResolverOutputSel, IfxPort_Mode_outputPushPullGeneral);
    Pin_setDriver(&board->driver.ResolverOutputSel, IfxPort_PadDriver_cmosAutomotiveSpeed1);
/*
    Ifx_Console_printAlign("- Resolver 0 Select=P%d.%d"ENDL
        , IfxPort_getIndex(board->driver.ResolverOutputSel.port)
        , board->driver.ResolverOutputSel.pinIndex);

    App_printStatus(result);
*/
    return result;
}
boolean LB30_AppInit_resolverMux1(LB30 *board)
{
    boolean result = TRUE;
//    App_printStatusTitle("Initializing Resolver 1 gains selection");


    {
        /* Resolver 1 Gain multiplexer settings */
        uint32 i;

//        board->driver.Resolver1GainSel[1] = (IfxPort_Pin) {&MODULE_P14, 8};
//        board->driver.Resolver1GainSel[2] = (IfxPort_Pin) {&MODULE_P14, 9};
//        board->driver.Resolver1GainSel[3] = (IfxPort_Pin) {&MODULE_P14, 10};

        if (board->boardVersion->boardVersion == LB_BoardVersion_HybridKit_LogicBoard_3_0)
        {
            board->driver.Resolver1GainSel[0] = (IfxPort_Pin) {&MODULE_P34, 1};
            LB30_setResolver1Gain(board, LB30_ResolverGain_1_60);
        }
        else
        {
            board->driver.Resolver1GainSel[0] = (IfxPort_Pin) {&MODULE_P34, 2};
            LB31_setResolver1Gain(board, board->configuration.positionSensors.aurixResolver.input[1].gainCode);
        }

        for (i = 0; i < 4; i++)
        {
/*
            Ifx_Console_printAlign("- Resolver 1 Gain Sel[%d]=P%d.%d"ENDL
                , i
                , IfxPort_getIndex(board->driver.Resolver1GainSel[i].port)
                , board->driver.Resolver1GainSel[i].pinIndex);
*/
        }

        for (i = 0; i < 4; i++)
        {
            Pin_setMode(&board->driver.Resolver1GainSel[i], IfxPort_Mode_outputPushPullGeneral);
            Pin_setDriver(&board->driver.Resolver1GainSel[i], IfxPort_PadDriver_cmosAutomotiveSpeed1);
        }
    }

//    App_printStatus(result);
    return result;
}


boolean LB30_resetConfig(LB30 *board)
{
    boolean                    result = TRUE;
    AppDbStdIf_MainConfig *driverBoardConfig = AppDbStdIf_getMainConfig(board->driverBoardStdif);
//    App_printStatusTitle("Initializing signals");


    float32                    ts = IfxStdIf_Inverter_getPeriod(&board->stdIf.inverter);
#if BYPASSEEPROM
    board->configuration.inverter[0].currentSenor.max = 5000;
    board->configuration.inverter[0].currentSenor.gain = 0.4882813;
    board->configuration.inverter[0].currentSenor.offset = -2048.0;
#endif
    Ifx_AnalogInputF32_Config config;
    config.params.lut = NULL_PTR;

    /* On out of range settings */
    config.params.onOutOfRange = board->configuration.inverter[0].currentSenor.max > 0 ? (Limit_onOutOfRange) & LB30_Inverter_onLimitError : NULL_PTR;
    config.params.object       = config.params.onOutOfRange ? board : NULL_PTR;

    /* Low pass filter settings */
    config.params.filterConfig.cutOffFrequency = board->configuration.inverter[0].currentSenor.cutOffFrequency;
    config.params.filterConfig.gain            = 1.0;
    config.params.filterEnabled                = board->configuration.inverter[0].currentSenor.cutOffFrequency > 0.0;

    /* Sample frequency settings */
    config.params.filterConfig.samplingTime = ts;

    config.name                             = "Phase current U";
    config.params.info                      = LB30_Message_errorCurrentU;
    config.params.gain                      = board->configuration.inverter[0].currentSenor.gain;
    config.params.offset                    = board->configuration.inverter[0].currentSenor.offset;
    config.params.lower                     = config.params.onOutOfRange ? -board->configuration.inverter[0].currentSenor.max : 0.0;
    config.params.upper                     = config.params.onOutOfRange ? +board->configuration.inverter[0].currentSenor.max : 0.0;
    Ifx_AnalogInputF32_init(&board->analogInput.currents[ECU_MOTOR_INDEX_0][0].input, &config);
//    App_printAnalogInputConfig(&config);
    config.name        = "Phase current V";
    config.params.info = LB30_Message_errorCurrentV;
    Ifx_AnalogInputF32_init(&board->analogInput.currents[ECU_MOTOR_INDEX_0][1].input, &config);
//    App_printAnalogInputConfig(&config);
    config.name        = "Phase current W";
    config.params.info = LB30_Message_errorCurrentW;
    Ifx_AnalogInputF32_init(&board->analogInput.currents[ECU_MOTOR_INDEX_0][2].input, &config);
//    App_printAnalogInputConfig(&config);

#if CFG_CURRENT_SENSOR_TEST == 1
	/* no limit checking for this sensor */
	config.params.onOutOfRange = NULL_PTR;
	config.params.object       = NULL_PTR;
    /* Low pass filter settings */
    config.params.filterConfig.cutOffFrequency = board->configuration.inverter[1].currentSenor.cutOffFrequency;
    config.params.filterConfig.gain            = 1.0;
    config.params.filterEnabled                = board->configuration.inverter[1].currentSenor.cutOffFrequency > 0.0;
    config.params.gain                      = board->configuration.inverter[1].currentSenor.gain;
    config.params.offset                    = board->configuration.inverter[1].currentSenor.offset;
    config.params.lower                     = -board->configuration.inverter[0].currentSenor.max;
    config.params.upper                     = +board->configuration.inverter[0].currentSenor.max;

    config.name        = "Phase current U (Inverter 1)";
	config.params.info = LB30_Message_errorCurrentU;
	Ifx_AnalogInputF32_init(&board->analogInput.currents[ECU_MOTOR_INDEX_1][0].input, &config);
	App_printAnalogInputConfig(&config);
	config.name        = "Phase current V (Inverter 1)";
	config.params.info = LB30_Message_errorCurrentV;
	Ifx_AnalogInputF32_init(&board->analogInput.currents[ECU_MOTOR_INDEX_1][1].input, &config);
	App_printAnalogInputConfig(&config);
	config.name        = "Phase current W (Inverter 1)";
	config.params.info = LB30_Message_errorCurrentW;
	Ifx_AnalogInputF32_init(&board->analogInput.currents[ECU_MOTOR_INDEX_1][2].input, &config);
	App_printAnalogInputConfig(&config);
#endif

    /* On out of range settings */
    config.params.object       = board;
    config.params.onOutOfRange = (Limit_onOutOfRange) & LB30_Inverter_onLimitError;

    config.params.filterConfig.cutOffFrequency = 1000;
    config.params.filterConfig.gain            = 1.0;
    config.params.filterEnabled                = TRUE;

    config.name                                = "DC link voltage";
    config.params.info                         = LB30_Message_errorVdc;
    if ((AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
    		|| (AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
    		)
    { /* Scaling is done in driver board handler */
        config.params.gain                         = 1.025; /*Should theoretically be 1.0 but corrected to 1.025 after calibration*/
        config.params.offset                       = 0;
    }
    else if (AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDrive)
    {
    	config.params.gain                         = ((10.0/22.0)*((5*100.0+1.0+1.0)/(1.0+1.0))*CFG_ADC_GAIN);
        config.params.offset                       = 0;

        if (board->boardVersion->boardVersion >= LB_BoardVersion_HybridKit_LogicBoard_3_3)
        {

        }
        else
        {
        	config.params.gain                         = config.params.gain * ((68.0+1.0)/1.0);
        }
    }
    else if (AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHp2)
    { /* Measurement not available on HP2 driver board 3.1 and 3.2 */
    	config.params.gain                         = 1;
        config.params.offset                       = 0;
    }
    config.params.lower                        = driverBoardConfig->vdcMin;
    config.params.upper                        = driverBoardConfig->vdcMaxGen;
    Ifx_AnalogInputF32_init(&board->analogInput.vDc.input, &config);
    if (AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHp2)
    { /* Overwrite Vdc as measurement is not available o this boards */
    	board->analogInput.vDc.input.value = driverBoardConfig->vdcNom;
    }
//    App_printAnalogInputConfig(&config);

    /* Sample frequency settings */
    config.params.filterConfig.cutOffFrequency = 100;
    config.params.filterConfig.samplingTime = 1e-3;

    config.name                             = "IGBT temp U";
    config.params.info                      = LB30_Message_errorIgbtTempU;
    if (AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
    {
    	if (AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardVersion <= LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_1)
    	{
            config.params.lut                       = &g_TempIgbtR2fV2Lut;
    	}
    	else
    	{
            config.params.lut                       = &g_TempIgbtR2fV3Lut;
    	}
    }
    else if (AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
    {
        config.params.lut                       = NULL_PTR;
        config.params.gain   = 1;
        config.params.offset = 0;
    }
    else
    {
        config.params.lut                       = &g_TempIgbtR2fV1Lut;
    }
    config.params.lower                     = driverBoardConfig->igbtTempMin;
    config.params.upper                     = driverBoardConfig->igbtTempMax;
    Ifx_AnalogInputF32_init(&board->analogInput.igbtTemp[0].input, &config);
//    App_printAnalogInputConfig(&config);
    config.name        = "IGBT temp V";
    config.params.info = LB30_Message_errorIgbtTempV;
    Ifx_AnalogInputF32_init(&board->analogInput.igbtTemp[1].input, &config);
//    App_printAnalogInputConfig(&config);
    config.name        = "IGBT temp W";
    config.params.info = LB30_Message_errorIgbtTempW;
    Ifx_AnalogInputF32_init(&board->analogInput.igbtTemp[2].input, &config);
//    App_printAnalogInputConfig(&config);

    config.params.filterConfig.cutOffFrequency = 1000;
    config.params.lut    = NULL_PTR;

    config.name          = "Board temperature";
    config.params.info   = LB30_Message_errorBoardTemp;
    config.params.gain   = (140.0 - (-40.0)) / (1.750 - 0.100) * CFG_ADC_GAIN;
    config.params.offset = (-40.0) / config.params.gain - 0.100 / CFG_ADC_GAIN;
    config.params.lower  = CFG_LIMIT_MIN_BOARD_TEMP;
    config.params.upper  = CFG_LIMIT_MAX_BOARD_TEMP;
    Ifx_AnalogInputF32_init(&board->analogInput.tempBoard.input, &config);
//    App_printAnalogInputConfig(&config);

    config.name          = "Vana 5.0V";
    config.params.info   = LB30_Message_errorVana50;
    config.params.gain   = (10 + 10) / 10 * CFG_ADC_GAIN;
    config.params.offset = 0.0;
    config.params.lower  = 5.0 * 0.9;
    config.params.upper  = 5.0 * 1.1;
    Ifx_AnalogInputF32_init(&board->analogInput.vAna50.input, &config);
//    App_printAnalogInputConfig(&config);

    config.name          = "Vref 5.0V";
    config.params.info   = LB30_Message_errorVref50;
    config.params.gain   = (10 + 10) / 10 * CFG_ADC_GAIN;
    config.params.offset = 0.0;
    config.params.lower  = 5.0 * 0.9;
    config.params.upper  = 5.0 * 1.1;
    Ifx_AnalogInputF32_init(&board->analogInput.vRef50.input, &config);
//    App_printAnalogInputConfig(&config);

    config.name          = "Vdig 5.0V";
    config.params.info   = LB30_Message_errorVdig33;
    config.params.gain   = (4.7 + 47.0) / 47.0 * CFG_ADC_GAIN;
    config.params.offset = 0.0;
    config.params.lower  = 5.0 * 0.8;	//3.3 * 0.9;	20200116 Jimmy Modified
    config.params.upper  = 5.0 * 1.1;
    Ifx_AnalogInputF32_init(&board->analogInput.vDig50.input, &config);
//    App_printAnalogInputConfig(&config);

    config.name          = "KL30";
    config.params.info   = LB30_Message_errorKl30;
    config.params.gain   = (39.0 + 11.0) / 11.0 * CFG_ADC_GAIN;
    config.params.offset = 0.0;
    config.params.lower  = CFG_LIMIT_MIN_KL30;
    config.params.upper  = CFG_LIMIT_MAX_KL30;
    Ifx_AnalogInputF32_init(&board->analogInput.kl30.input, &config);
//    App_printAnalogInputConfig(&config);


#if 0	// Jovi
    config.name                 = "Analog input 0";
    config.params.info         = LB30_Message_errorAn0;
    config.params.gain          = board->configuration.analogInput[0].gain;
    config.params.offset        = board->configuration.analogInput[0].offset;
    config.params.filterConfig.cutOffFrequency = board->configuration.analogInput[0].cutOffFrequency;
    config.params.filterConfig.gain            = 1.0;
    config.params.filterEnabled                = board->configuration.analogInput[0].cutOffFrequency > 0.0;
    config.params.onOutOfRange = board->configuration.analogInput[0].min < board->configuration.analogInput[0].max ? (Limit_onOutOfRange) & LB30_Inverter_onLimitError : NULL_PTR;
    config.params.object       = config.params.onOutOfRange ? board : NULL_PTR;
    config.params.lower        = config.params.onOutOfRange ? board->configuration.analogInput[0].min : 0.0;
    config.params.upper        = config.params.onOutOfRange ? board->configuration.analogInput[0].max : 0.0;
    Ifx_AnalogInputF32_init(&board->analogInput.in[0].input, &config);
//    App_printAnalogInputConfig(&config);

    config.name                 = "Analog input 1";
    config.params.info         = LB30_Message_errorAn1;
    config.params.gain          = board->configuration.analogInput[1].gain;
    config.params.offset        = board->configuration.analogInput[1].offset;
    config.params.filterConfig.cutOffFrequency = board->configuration.analogInput[1].cutOffFrequency;
    config.params.filterConfig.gain            = 1.0;
    config.params.filterEnabled                = board->configuration.analogInput[1].cutOffFrequency > 0.0;
    config.params.onOutOfRange = board->configuration.analogInput[1].min < board->configuration.analogInput[1].max ? (Limit_onOutOfRange) & LB30_Inverter_onLimitError : NULL_PTR;
    config.params.object       = config.params.onOutOfRange ? board : NULL_PTR;
    config.params.lower        = config.params.onOutOfRange ? board->configuration.analogInput[1].min : 0.0;
    config.params.upper        = config.params.onOutOfRange ? board->configuration.analogInput[1].max : 0.0;
    Ifx_AnalogInputF32_init(&board->analogInput.in[1].input, &config);
//    App_printAnalogInputConfig(&config);

    config.name                 = "Analog input 2";
    config.params.info         = LB30_Message_errorAn2;
    config.params.gain          = board->configuration.analogInput[2].gain;
    config.params.offset        = board->configuration.analogInput[2].offset;
    config.params.filterConfig.cutOffFrequency = board->configuration.analogInput[2].cutOffFrequency;
    config.params.filterConfig.gain            = 1.0;
    config.params.filterEnabled                = board->configuration.analogInput[2].cutOffFrequency > 0.0;
    config.params.onOutOfRange = board->configuration.analogInput[2].min < board->configuration.analogInput[2].max ? (Limit_onOutOfRange) & LB30_Inverter_onLimitError : NULL_PTR;
    config.params.object       = config.params.onOutOfRange ? board : NULL_PTR;
    config.params.lower        = config.params.onOutOfRange ? board->configuration.analogInput[2].min : 0.0;
    config.params.upper        = config.params.onOutOfRange ? board->configuration.analogInput[2].max : 0.0;
    Ifx_AnalogInputF32_init(&board->analogInput.in[2].input, &config);
//    App_printAnalogInputConfig(&config);

    config.name                 = "Analog input 3";
    config.params.info         = LB30_Message_errorAn3;
    config.params.gain          = board->configuration.analogInput[3].gain;
    config.params.offset        = board->configuration.analogInput[3].offset;
    config.params.filterConfig.cutOffFrequency = board->configuration.analogInput[3].cutOffFrequency;
    config.params.filterConfig.gain            = 1.0;
    config.params.filterEnabled                = board->configuration.analogInput[3].cutOffFrequency > 0.0;
    config.params.onOutOfRange = board->configuration.analogInput[3].min < board->configuration.analogInput[3].max ? (Limit_onOutOfRange) & LB30_Inverter_onLimitError : NULL_PTR;
    config.params.object       = config.params.onOutOfRange ? board : NULL_PTR;
    config.params.lower        = config.params.onOutOfRange ? board->configuration.analogInput[3].min : 0.0;
    config.params.upper        = config.params.onOutOfRange ? board->configuration.analogInput[3].max : 0.0;
    Ifx_AnalogInputF32_init(&board->analogInput.in[3].input, &config);
//    App_printAnalogInputConfig(&config);
#endif


    /* On out of range settings */
    config.params.object       = NULL_PTR;
    config.params.onOutOfRange = NULL_PTR;
    config.params.info         = 0;
    config.params.lower        = CFG_LIMIT_MIN_BOARD_TEMP;	// 20200116 Jimmy modified
    config.params.upper        = CFG_LIMIT_MAX_BOARD_TEMP;	// 20200116 Jimmy modified

    /* Low pass filter settings */
    config.params.filterEnabled = FALSE;

    config.name        = "Motor temperature";
    config.params.info = LB30_Message_errorMotorTemp;
    config.params.lut  = &g_TempStatorLU;
    Ifx_AnalogInputF32_init(&board->analogInput.motorTemp.input, &config);
//    App_printAnalogInputConfig(&config);

//    App_printStatus(result);
    return result;
}


boolean LB30_AppInitConsole(LB30 *board, boolean primaryConsoleAsc)
{
    boolean result = TRUE;
    // Initial USB_Tx, USB_Rx Module //

    result &= LB30_AppInit_Asc0(board, primaryConsoleAsc); /* Required for shell message at startup */
    result &= LB30_AppInit_CanModule(board);
    result &= LB30_AppInit_Can0_Node0(board);                    /* Required for shell message at startup */
#if ENABLEALLCAN
    result &= LB30_AppInit_Can0_Node3(board);
    result &= LB30_AppInit_Can1_Node1(board);
    result &= LB30_AppInit_Can1_Node2(board);
    result &= LB30_AppInit_Can2_Node0(board);
#endif
    return result;

}


boolean LB30_AppInit_Dio(LB30 *board)
{
    boolean result = TRUE;
    uint32  i;
//    App_printStatusTitle("Initializing general purpose digital IOs");


    board->driver.digitalInputs[0]  = (IfxPort_Pin) {&MODULE_P22, 5};	// DIN_0, Debug IO
    board->driver.digitalInputs[1]  = (IfxPort_Pin) {&MODULE_P22, 6};	// DIN_1, Debug IO

    // TC397 Dio Configurations //
    board->driver.LED_Indicator[0] = (IfxPort_Pin) {&MODULE_P10, 1};
    board->driver.LED_Indicator[1] = (IfxPort_Pin) {&MODULE_P10, 3};
    board->driver.LED_Indicator[2] = (IfxPort_Pin) {&MODULE_P10, 4};

    if (board->boardVersion->boardVersion <= LB_BoardVersion_HybridKit_LogicBoard_3_1)
    {
        board->driver.digitalInputs[2] = (IfxPort_Pin) {&MODULE_P22, 7};	// RSTn_HS, GateDriver's M1_NRST_HS
        board->driver.digitalInputs[3] = (IfxPort_Pin) {&MODULE_P22, 8};	// RSTn_HS_M2, GateDriver's M2_NRST_HS
    }
    else
    {   /* Setup P40 */
        uint16 endinit_pw, endinitSfty_pw;
        board->driver.digitalInputs[2] = (IfxPort_Pin) {&MODULE_P40, 2};	// DIN_2, Debug IO
        board->driver.digitalInputs[3] = (IfxPort_Pin) {&MODULE_P40, 4};	// DIN_3, Debug IO
        endinit_pw                     = IfxScuWdt_getCpuWatchdogPassword();
        endinitSfty_pw                 = IfxScuWdt_getSafetyWatchdogPassword();

        IfxScuWdt_clearCpuEndinit(endinit_pw);
        IfxScuWdt_clearSafetyEndinit(endinitSfty_pw);

        board->driver.digitalInputs[2].port->PDISC.B.PDIS2 = 0;
        board->driver.digitalInputs[3].port->PDISC.B.PDIS4 = 0;

        IfxScuWdt_setSafetyEndinit(endinitSfty_pw);
        IfxScuWdt_setCpuEndinit(endinit_pw);
    }

    for (i = 0; i < 4; i++)
    {
        Pin_setMode(&board->driver.digitalInputs[i], IfxPort_Mode_inputNoPullDevice);
/*
        Ifx_Console_printAlign("- Digital input %d =P%d.%d"ENDL
            , i
            , IfxPort_getIndex(board->driver.digitalInputs[i].port)
            , board->driver.digitalInputs[i].pinIndex);
*/
//        Pin_setMode(&board->driver.digitalOutputs[i], IfxPort_Mode_outputPushPullGeneral);
//        Pin_setDriver(&board->driver.digitalOutputs[i], IfxPort_PadDriver_cmosAutomotiveSpeed1);
//        Pin_setState(&board->driver.digitalOutputs[i], IfxPort_State_low);
/*
        Ifx_Console_printAlign("- Digital outputs %d =P%d.%d"ENDL
            , i
            , IfxPort_getIndex(board->driver.digitalOutputs[i].port)
            , board->driver.digitalOutputs[i].pinIndex);
*/
    }
    // TC397 Dio Configurations //
    for (i = 0; i<3; i++)
    {
    	Pin_setMode(&board->driver.LED_Indicator[i], IfxPort_Mode_outputPushPullGeneral);
    	Pin_setDriver(&board->driver.LED_Indicator[i], IfxPort_PadDriver_cmosAutomotiveSpeed1);
    	Pin_setState(&board->driver.LED_Indicator[i], IfxPort_State_low);
    }

	/* TC397 HW Setting */
#if 1		//DEBUG_1
    /* Jimmy Create P00.11 Power Board Enable pin */
    IfxPort_setPinMode(&MODULE_P00, 11,  IfxPort_Mode_outputPushPullGeneral);
    IfxPort_setPinState(&MODULE_P00, 11,	IfxPort_State_high);
    Dio_PB_EN_Status = TRUE;
#endif
    /*P02.8 Set Low for AD2S1210_SOE*/
  	IfxPort_setPinMode(&MODULE_P02, 8,  IfxPort_Mode_outputPushPullGeneral);
    IfxPort_setPinState(&MODULE_P02, 8,	IfxPort_State_low);

	/*P00.2 Set Low */
	IfxPort_setPinMode(&MODULE_P00, 2,  IfxPort_Mode_outputPushPullGeneral);
    IfxPort_setPinState(&MODULE_P00, 2,	IfxPort_State_low);

	/*P00.3 Set Low */
	IfxPort_setPinMode(&MODULE_P00, 3,  IfxPort_Mode_outputPushPullGeneral);
    IfxPort_setPinState(&MODULE_P00, 3,	IfxPort_State_low);

	/*P00.10 Set Low */
	//IfxPort_setPinMode(&MODULE_P00, 10,  IfxPort_Mode_outputPushPullGeneral);
    //IfxPort_setPinState(&MODULE_P00, 10, IfxPort_State_low);

	IfxPort_setPinMode(&MODULE_P23, 2,  IfxPort_Mode_outputPushPullGeneral);
	IfxPort_setPinState(&MODULE_P23, 2,	IfxPort_State_low);

	/* PMIC DIO interface configuration */
	IfxPort_setPinMode(&MODULE_P02, 10,  IfxPort_Mode_outputPushPullGeneral);
	IfxPort_setPinState(&MODULE_P02, 10,	IfxPort_State_low);
	/* RESET Lasting 2ms */
	IfxPort_setPinState(&MODULE_P23, 2,	IfxPort_State_high);
#if BYPASSPROTECTIONLATCH
#else if
	for(i=0; i<200000; i++)
		__nop();
	
    IfxPort_setPinState(&MODULE_P23, 2,	IfxPort_State_low);
#endif

    /* P23.3 Set high for 12V_Encoder_Power */
  	IfxPort_setPinMode(&MODULE_P23, 3,  IfxPort_Mode_outputPushPullGeneral);
    IfxPort_setPinState(&MODULE_P23, 3,	IfxPort_State_high);	// Jovi
	//IfxPort_setPinState(&MODULE_P23, 3,	IfxPort_State_low);

	printf("HW RESET OK!!\n");
	
//    App_printStatus(result);
    return result;
}


boolean LB30_AppInit(LB30 *board)
{
    boolean result = TRUE;


    /* Console is not available before this point */
//    Ifx_Console_printAlign("************************"ENDL);
//    Ifx_Console_printAlign("* Board initialization *"ENDL);
//    Ifx_Console_printAlign("************************"ENDL);

//    Ifx_g_console.standardIo->txDisabled = board->configuration.verboseLevel <= IFX_VERBOSE_LEVEL_WARNING;
    result 								= TRUE;		// look around the CAN1 initial //
    result                              &= LB30_AppInit_Asc1(board);
    result                              &= LB30_AppInit_Qspi5(board);
    result                              &= LB30_AppInit_10ms(board);
    result                              &= LB30_AppInit_Inverter0(board);
    result                              &= LB30_AppInit_Vadc(board);
    AppCan_initMessageCan1();
    // FIXME to do result &= LB30_AppInit_Inverter1();

    {
//        const IfxPort_Pin index = {&MODULE_P33, 14};
        /* Put the iGMR driver in 3 state / required to enable AD2S1210 */
//        Pin_setState(&index, IfxPort_State_high);
//        Pin_setMode(&index, IfxPort_Mode_outputPushPullGeneral);
    }

    result &= LB30_AppInit_Dsadc(board);


    if ((board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver0)
    		|| (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_internalResolver0))
    {
//        result &= LB30_AppInit_resolverMux0(board);
        result &= LB30_AppInit_DsadcRdc(board, 0);
        IfxDsadc_Rdc_startConversion(&board->driver.dsadcRdc[0]);
    }

    if ((board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver1)
    		|| (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_internalResolver1))
    {
//        result &= LB30_AppInit_resolverMux1(board);
        result &= LB30_AppInit_DsadcRdc(board, 1);
        IfxDsadc_Rdc_startConversion(&board->driver.dsadcRdc[1]);
    }


    if ((board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_encoder)
    		|| (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_encoder))
    {
        result &= LB30_AppInit_encoderGpt12(board);
        // FIXME to do result &= LB30_AppInit_EncoderSpe();
    }


    if ((board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_ad2s1210)
    		|| (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_ad2s1210))
    {
//        result &= LB30_AppInit_resolverMux0(board);
        // AD2S1210 Initialized at post initialization
    }

    if ((board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_tle5012)
    		|| (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_tle5012))
    {
//        result &= LB30_AppInit_Igmr(board);		// This function have be disabled on TC397 CB //
        result &= LB30_AppInit_encoderGpt12(board);
    }


    if (board->configuration.wizard.enabled)
    {
    	/* Provide stub to avoid sensor fault during wizard */
    	board->stdIf.dsadcRdc[0].getFault = (IfxStdIf_Pos_GetFault)AppSetupWizard_dummyIfxStdIfPosGetFault;
    	board->stdIf.dsadcRdc[1].getFault = (IfxStdIf_Pos_GetFault)AppSetupWizard_dummyIfxStdIfPosGetFault;
    	board->stdIf.encoder.getFault = (IfxStdIf_Pos_GetFault)AppSetupWizard_dummyIfxStdIfPosGetFault;
    	board->stdIf.ad2s1210.getFault = (IfxStdIf_Pos_GetFault)AppSetupWizard_dummyIfxStdIfPosGetFault;
    }

    // FIXME to do result &= LB30_AppInit_DsadcRdc1();
    // FIXME to do result &= LB30_AppInit_DsadcRdc1();
    // FIXME to do result &= LB30_AppInit_HallCcu6();
    // FIXME to do result &= LB30_AppInit_HallSpe();

    result &= LB30_resetConfig(board);
//    App_printStatusTitle("Board initialization");

//    App_printStatus(result);
//    Ifx_g_console.standardIo->txDisabled = FALSE;
    return result;
}

