/**
 * \file LB30.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 */
#include "LB30.h"
#include "_Utilities/Ifx_Assert.h"
#include "string.h"
#include "SysSe/EMotor/Ifx_SvmF32.h"
#include "SysSe/Comm/Ifx_Console.h"
#include "LogicBoard3_0/Config/LB30_Configuration.h"
#include "LB30_AppInit.h"
#include "LB30_AppPreInit.h"
#include "LB30_AppPostInit.h"
#include "LB30_AppSelfTest.h"
#include "Common/main.h"
#include "Common/AppShell.h"

/* Jimmy Create the Global values for PB_EN status */
boolean Dio_PB_EN_Status = FALSE;
/* end of created */

//________________________________________________________________________________________

/** \brief Inverter application messages */
const Ifx_Message_Entry g_LB30_Messages[LB30_Message_COUNT] = {
    {TRUE, "Error: phase current U limit reached"   },
    {TRUE, "Error: phase current V limit reached"   },
    {TRUE, "Error: phase current W limit reached"   },
    {TRUE, "Error: Vdc limit reached"           },
    {TRUE, "Error: Error: IGBT temp U limit reached"},
    {TRUE, "Error: Error: IGBT temp V limit reached"},
    {TRUE, "Error: Error: IGBT temp W limit reached"},
    {TRUE, "Error: Current input U failure"         },
    {TRUE, "Error: Current input V failure"         },
    {TRUE, "Error: Current input W failure"         },

    {TRUE, "Error: board temperature limit reached" },
    {TRUE, "Error: Vana 5.0V limit reached"         },
    {TRUE, "Error: Vref 5.0V limit reached"         },
    {TRUE, "Error: Vdig 3.3V limit reached"         },
    {TRUE, "Error: KL30 limit reached"              },
    {TRUE, "Error: Motor temperature limit reached" },
    {TRUE, "Error: Error:Analog input 0 limit reached" },
    {TRUE, "Error: Error:Analog input 1 limit reached" },
    {TRUE, "Error: Error:Analog input 2 limit reached" },
    {TRUE, "Error: Error:Analog input 3 limit reached" },

    {TRUE, "Error: IOM event phase U top"           },
    {TRUE, "Error: IOM event phase V top"           },
    {TRUE, "Error: IOM event phase W top"           },
    {TRUE, "Error: IOM event phase U bottom"           },
    {TRUE, "Error: IOM event phase V bottom"           },
    {TRUE, "Error: IOM event phase W bottom"           },
};

/******************************************************************************/
/*-----------------------Private Function Prototypes--------------------------*/
/******************************************************************************/
void    LB30_setDigitalOutput(LB30 *board, uint32 index, boolean state);
boolean LB30_getDigitalInput(LB30 *board, uint32 index);
boolean LB30_getDigitalOutput(LB30 *board, uint32 index);

/******************************************************************************/
/*-------------------------Function Implementations---------------------------*/
/******************************************************************************/

boolean LB30_Inverter_clearFlags(LB30 *driver)
{
	boolean result;
    driver->driver.inverter.limitError = FALSE;
    driver->driver.inverter.inputError = FALSE;
    driver->driver.inverter.iomError = FALSE;
    Ifx_Message_enableAll(&driver->driver.inverter.messages);

	result = AppDbStdIf_clearFaults(driver->driverBoardStdif);

	return result;
}


void LB30_Inverter_disable(LB30 *driver)
{
	AppDbStdIf_disableDriver(driver->driverBoardStdif);
}


void LB30_Inverter_enable(LB30 *driver)
{
	AppDbStdIf_enableDriver(driver->driverBoardStdif);
}


float32 LB30_Inverter_getDeadtime(LB30 *driver)
{
    return IfxStdIf_PwmHl_getDeadtime(&driver->driver.inverter.pwmStdIf);
}


float32 LB30_Inverter_getMinPulse(LB30 *driver)
{
    return IfxStdIf_PwmHl_getMinPulse(&driver->driver.inverter.pwmStdIf);
}


float32 LB30_Inverter_getPeriod(LB30 *driver)
{
    IfxStdIf_PwmHl *pwm   = &driver->driver.inverter.pwmStdIf;
    IfxStdIf_Timer *timer = IfxStdIf_PwmHl_getTimer(pwm);
    return IfxStdIf_Timer_getPeriodSecond(timer);
}


Ifx_TimerValue LB30_Inverter_getPeriodTicks(LB30 *driver)
{
    IfxStdIf_PwmHl *pwm   = &driver->driver.inverter.pwmStdIf;
    IfxStdIf_Timer *timer = IfxStdIf_PwmHl_getTimer(pwm);
    return IfxStdIf_Timer_getPeriod(timer);
}


void LB30_Inverter_getPhaseCurrents(LB30 *driver, float32 *currents)
{
    currents[0] = Ifx_AnalogInputF32_getValue(&driver->analogInput.currents[ECU_MOTOR_INDEX_0][0].input);
    currents[1] = Ifx_AnalogInputF32_getValue(&driver->analogInput.currents[ECU_MOTOR_INDEX_0][1].input);
	currents[2] = Ifx_AnalogInputF32_getValue(&driver->analogInput.currents[ECU_MOTOR_INDEX_0][2].input);

#if 0
    if (driver->configuration.inverter[0].currentSenor.useThreeSensors)
    {
        /* FIXME if sum is not exactly 0, the 3rd current won't be average 0 and will produce ripple in the torque */
        currents[2] = Ifx_AnalogInputF32_getValue(&driver->analogInput.currents[ECU_MOTOR_INDEX_0][2].input);
    }
    else
    {
        currents[2] = -(currents[0] + currents[1]);
    }
#endif
}


void LB30_Inverter_getPwm(LB30 *driver, Ifx_TimerValue *duty)
{
    uint32 i;

    for (i = 0; i < ECU_PHASE_PER_MOTOR_COUNT; i++)
    {
        duty[i] = driver->driver.inverter.duty[i];
    }
}


IfxStdIf_Inverter_Status LB30_Inverter_getStatus(LB30 *driver)
{
    uint32 driverError;
    driverError                          = AppDbStdIf_getFault(driver->driverBoardStdif);

    driver->driver.inverter.status.error =
        driver->driver.inverter.inputError
        || driver->driver.inverter.limitError
        || driver->driver.inverter.iomError
        || (driverError != 0 ? 1 : 0);

    driverError                                            = driverError | (driverError >> 16); // Merge of FLTA and FLTB status flags
    driver->driver.inverter.status.phase1TopDriverError    = (driverError & 0x15) != 0 ? 1 : 0;
    driver->driver.inverter.status.phase1BottomDriverError = (driverError & 0x2A) != 0 ? 1 : 0;
    driver->driver.inverter.status.phase2TopDriverError    = (driverError & 0x15) != 0 ? 1 : 0;
    driver->driver.inverter.status.phase2BottomDriverError = (driverError & 0x2A) != 0 ? 1 : 0;
    driver->driver.inverter.status.phase3TopDriverError    = (driverError & 0x15) != 0 ? 1 : 0;
    driver->driver.inverter.status.phase3BottomDriverError = (driverError & 0x2A) != 0 ? 1 : 0;
#if 1		//DEBUG_2
    /* Jimmy create for protection gate driver enable pin is low */
    driver->driver.inverter.status.faultGDEN			   = (driverError & 0x80) != 0 ? 1 : 0;
#if 1
    if (Dio_PB_EN_Status == TRUE)
    {
    	if(IfxPort_getPinState(&MODULE_P00, 11) == FALSE)
    		driver->driver.inverter.status.faultPBEN = 1;
    	else
    		driver->driver.inverter.status.faultPBEN = 0;
    }
#endif
    /* end of created */
#endif
    /* FIXME update temp and current status */
    driver->driver.inverter.status.phase1CurrentSensorError = 0;
    driver->driver.inverter.status.phase2CurrentSensorError = 0;
    driver->driver.inverter.status.phase3CurrentSensorError = 0;

    driver->driver.inverter.status.phase1SwitchTempError    = 0;
    driver->driver.inverter.status.phase2SwitchTempError    = 0;
    driver->driver.inverter.status.phase3SwitchTempError    = 0;

    driver->driver.inverter.status.phase1Overcurrent        = 0;
    driver->driver.inverter.status.phase2Overcurrent        = 0;
    driver->driver.inverter.status.phase3Overcurrent        = 0;

    driver->driver.inverter.status.faultInput        = driver->driver.inverter.inputError;
    driver->driver.inverter.status.faultLimit        = driver->driver.inverter.limitError;
    driver->driver.inverter.status.faultIom          = driver->driver.inverter.iomError;

    return driver->driver.inverter.status;
}


void LB30_Inverter_getSwitchTemps(LB30 *driver, float32 *temps)
{
    uint32 i;

    for (i = 0; i < ECU_PHASE_PER_MOTOR_COUNT; i++)
    {
        temps[i] = Ifx_AnalogInputF32_getValue(&driver->analogInput.igbtTemp[i].input);
    }
}


float32 LB30_Inverter_getVdc(LB30 *driver)
{
    return Ifx_AnalogInputF32_getValue(&driver->analogInput.vDc.input);
}


void LB30_Inverter_startPwmTimer(LB30 *driver)
{
    IfxStdIf_PwmHl *pwm   = &driver->driver.inverter.pwmStdIf;
    IfxStdIf_Timer *timer = IfxStdIf_PwmHl_getTimer(pwm);
    IfxStdIf_Timer_run(timer);
#if BYPASSIOM
#else if
    if (driver->configuration.safety.iom.enabled)
    {
		/* Start IOM reference PWM*/
		pwm   = &driver->driver.inverter.iomRefStdIf;
		timer = IfxStdIf_PwmHl_getTimer(pwm);
		IfxStdIf_Timer_run(timer);
    }
#endif
}


void LB30_Inverter_setDeadtime(LB30 *driver, float32 deadtime)
{
    IfxStdIf_PwmHl_setDeadtime(&driver->driver.inverter.pwmStdIf, deadtime);
}


void LB30_Inverter_setMinPulse(LB30 *driver, float32 minPulse)
{
    IfxStdIf_PwmHl_setMinPulse(&driver->driver.inverter.pwmStdIf, minPulse);
}


void LB30_Inverter_setPwm(LB30 *driver, Ifx_TimerValue *duty)
{
    uint32          i;
    IfxStdIf_PwmHl *pwm   = &driver->driver.inverter.pwmStdIf;
    IfxStdIf_Timer *timer = IfxStdIf_PwmHl_getTimer(pwm);

    // Jimmy test
    {
//    	IfxGtm_Atom_PwmHl_setMode(pwm, Ifx_Pwm_Mode_centerAligned);
    	for (i = 0; i < 6; i++)
    	{
    		duty[i] = IfxGtm_Atom_Timer_getPeriod(timer) / 2;
    	}
    }
//    IfxGtm_Atom_Timer_disableUpdate(timer);
//    IfxGtm_Atom_PwmHl_setOnTime(pwm, &duty[0]);
//    IfxGtm_Atom_Timer_applyUpdate(timer);
    // test end
    IfxStdIf_Timer_disableUpdate(timer);
    IfxStdIf_PwmHl_setOnTime(pwm, &duty[0]);
    IfxStdIf_Timer_applyUpdate(timer);

    if (driver->configuration.safety.iom.enabled)
    {
		/* Update IOM reference */
		pwm   = &driver->driver.inverter.iomRefStdIf;
		timer = IfxStdIf_PwmHl_getTimer(pwm);
		IfxStdIf_Timer_disableUpdate(timer);
		IfxStdIf_PwmHl_setOnTime(pwm, &duty[0]);
		IfxStdIf_Timer_applyUpdate(timer);
    }

    for (i = 0; i < ECU_PHASE_PER_MOTOR_COUNT; i++)
    {
        driver->driver.inverter.duty[i] = duty[i];
    }
}

void LB30_Inverter_setPwmFaultInjection(LB30 *driver, Ifx_TimerValue *duty, Ifx_TimerValue *dutyFaulty)
{
    uint32          i;
    IfxStdIf_PwmHl *pwm   = &driver->driver.inverter.pwmStdIf;
    IfxStdIf_Timer *timer = IfxStdIf_PwmHl_getTimer(pwm);

    IfxStdIf_Timer_disableUpdate(timer);
    IfxStdIf_PwmHl_setOnTime(pwm, &dutyFaulty[0]);
    IfxStdIf_Timer_applyUpdate(timer);

    if (driver->configuration.safety.iom.enabled)
    {
		/* Update IOM reference */
		pwm   = &driver->driver.inverter.iomRefStdIf;
		timer = IfxStdIf_PwmHl_getTimer(pwm);
		IfxStdIf_Timer_disableUpdate(timer);
		IfxStdIf_PwmHl_setOnTime(pwm, &duty[0]);
		IfxStdIf_Timer_applyUpdate(timer);
    }

    for (i = 0; i < ECU_PHASE_PER_MOTOR_COUNT; i++)
    {
        driver->driver.inverter.duty[i] = duty[i];
    }
}


void LB30_Inverter_setPwmOff(LB30 *driver)
{
    IfxStdIf_PwmHl *pwm;
    IfxStdIf_Timer *timer;

    if (driver->configuration.safety.iom.enabled)
    {
        /* Disable the IOM events to avoid undanted events during PWM disabling */
    	IfxIom_Driver_disableEvents(&driver->driver.inverter.iom.driver);
		/* Update IOM reference */
		pwm   = &driver->driver.inverter.iomRefStdIf;
		timer = IfxStdIf_PwmHl_getTimer(pwm);
		IfxStdIf_Timer_disableUpdate(timer);
		IfxStdIf_PwmHl_setMode(pwm, Ifx_Pwm_Mode_off);
		IfxStdIf_PwmHl_setOnTime(pwm, NULL_PTR);
		IfxStdIf_Timer_applyUpdate(timer);
    }

    pwm   = &driver->driver.inverter.pwmStdIf;
    timer = IfxStdIf_PwmHl_getTimer(pwm);
    IfxStdIf_Timer_disableUpdate(timer);
    IfxStdIf_PwmHl_setMode(pwm, Ifx_Pwm_Mode_off);
    IfxStdIf_PwmHl_setOnTime(pwm, NULL_PTR);
    IfxStdIf_Timer_applyUpdate(timer);

}


void LB30_Inverter_setPwmOn(LB30 *driver, Ifx_Pwm_Mode mode)
{
    IfxStdIf_PwmHl *pwm;
    IfxStdIf_Timer *timer;
    Ifx_TimerValue  tOn[3] = {0, 0, 0};

    if (driver->configuration.safety.iom.enabled)
    {
		IfxIom_Driver_clearHistory(&driver->driver.inverter.iom.driver);
    	IfxIom_Driver_restoreEvents(&driver->driver.inverter.iom.driver, driver->driver.inverter.iom.eventMask);
		/* Update IOM reference */
		pwm   = &driver->driver.inverter.iomRefStdIf;
		timer = IfxStdIf_PwmHl_getTimer(pwm);
		IfxStdIf_Timer_disableUpdate(timer);
		IfxStdIf_PwmHl_setMode(pwm, mode);
		IfxStdIf_PwmHl_setOnTime(pwm, &tOn[0]);
		IfxStdIf_Timer_applyUpdate(timer);
    }

    pwm    = &driver->driver.inverter.pwmStdIf;
    timer  = IfxStdIf_PwmHl_getTimer(pwm);
    IfxStdIf_Timer_disableUpdate(timer);
    IfxStdIf_PwmHl_setMode(pwm, mode);
    IfxStdIf_PwmHl_setOnTime(pwm, &tOn[0]);
    IfxStdIf_Timer_applyUpdate(timer);

}


void LB30_Inverter_setPulse(LB30 *driver, float32 *tOn, float32 *offset)
{
    IfxStdIf_PwmHl *pwm   = &driver->driver.inverter.pwmStdIf;
    IfxStdIf_Timer *timer = &driver->driver.inverter.pwmStdIf.timer;//IfxStdIf_PwmHl_getTimer(pwm);

    IfxStdIf_Timer_disableUpdate(timer);
    IfxGtm_Atom_PwmHl_setMode(pwm, Ifx_Pwm_Mode_leftAligned);
    IfxGtm_Atom_PwmHl_setPulse(pwm, tOn, offset);
    IfxStdIf_Timer_applyUpdate(timer);
}


void LB30_Inverter_setPeriod(LB30 *driver, float32 period)
{
/* FIXME to do*/
}


void LB30_Inverter_setPeriodTicks(LB30 *driver, Ifx_TimerValue period)
{
/* FIXME to do*/
}


IFX_INLINE float32 LB30_Inverter_compensateDeadtime(float32 ton, float32 compensation, float32 current)
{   /* FIXME hystLevel=4A histeresis level should be configurable?  */
    float32 hystLevel = 0.1;
    float32 currSign  = (current < 0.0) ? -1.0 : 1.0;
#if 1
    current = __absf(current);

    if (current < hystLevel)
    {
        compensation = (current / hystLevel) * compensation;
    }

#endif
    ton = ton + (compensation * currSign);
    return ton;
}


void LB30_Inverter_setVoltage(LB30 *driver, cfloat32 mab)
{
    uint32          i;
    Ifx_TimerValue  duty[3];
    IfxStdIf_PwmHl *pwm    = &driver->driver.inverter.pwmStdIf;
    IfxStdIf_Timer *timer  = IfxStdIf_PwmHl_getTimer(pwm);
    Ifx_TimerValue  period = IfxStdIf_Timer_getPeriod(timer);

    Ifx_SvmF32_do(mab, period, duty);

    if (driver->driver.inverter.deadtimeComp > 0)
    {
        float32 currents[3];
        LB30_Inverter_getPhaseCurrents(driver, currents);

        for (i = 0; i < ECU_PHASE_PER_MOTOR_COUNT; i++)
        {
            duty[i] = (Ifx_TimerValue)LB30_Inverter_compensateDeadtime((float32)duty[i], driver->driver.inverter.deadtimeComp, currents[i]);
        }
    }

    IfxStdIf_Timer_disableUpdate(timer);
    IfxStdIf_PwmHl_setOnTime(pwm, &duty[0]);
    IfxStdIf_Timer_applyUpdate(timer);

    if (driver->configuration.safety.iom.enabled)
    {
        /* Update IOM reference */
        pwm   = &driver->driver.inverter.iomRefStdIf;
        timer = IfxStdIf_PwmHl_getTimer(pwm);

        IfxStdIf_Timer_disableUpdate(timer);
        IfxStdIf_PwmHl_setOnTime(pwm, &duty[0]);
        IfxStdIf_Timer_applyUpdate(timer);
    }

    for (i = 0; i < ECU_PHASE_PER_MOTOR_COUNT; i++)
    {
        driver->driver.inverter.duty[i] = duty[i];
    }
}


void LB30_Inverter_showMessage(LB30 *driver)
{
    Ifx_Message_show(&driver->driver.inverter.messages);
}


void LB30_Inverter_onLimitError(LB30 *driver, Limits_Status status, sint32 info)
{
    if (g_App.hwState.hwMode == AppMode_run)
    {
        driver->driver.inverter.limitError = TRUE;
        Ifx_Message_set(&driver->driver.inverter.messages, info);
    }
}


/** Valid sensor offset value is offset +/-5% of \ref CFG_ADC_RESOLUTION
 */
boolean LB30_Inverter_inputFault(AppAnalogInput *Current, float32 offset)
{
    boolean result;
    result = (__absf(Current->input.scale.offset - offset) > (CFG_ADC_RESOLUTION * 0.05));
    return result;
}


/** \brief Check the current sensor is faulty.
 * \return None
 */
void LB30_Inverter_checkCurrentInputFault(LB30 *driver)
{
	if (driver->configuration.inverter[ECU_MOTOR_INDEX_0].currentSenor.max > 0)
	{
	    if (LB30_Inverter_inputFault(&driver->analogInput.currents[ECU_MOTOR_INDEX_0][0], driver->configuration.inverter[ECU_MOTOR_INDEX_0].currentSenor.offset))
	    {
	        driver->driver.inverter.inputError = TRUE;
	        Ifx_Message_set(&driver->driver.inverter.messages, LB30_Message_errorCurrentInputU);
	    }

	    if (LB30_Inverter_inputFault(&driver->analogInput.currents[ECU_MOTOR_INDEX_0][1], driver->configuration.inverter[ECU_MOTOR_INDEX_0].currentSenor.offset))
	    {
	        driver->driver.inverter.inputError = TRUE;
	        Ifx_Message_set(&driver->driver.inverter.messages, LB30_Message_errorCurrentInputV);
	    }

	    if (driver->configuration.inverter[ECU_MOTOR_INDEX_0].currentSenor.useThreeSensors)
	    {
	        if (LB30_Inverter_inputFault(&driver->analogInput.currents[ECU_MOTOR_INDEX_0][2], driver->configuration.inverter[ECU_MOTOR_INDEX_0].currentSenor.offset))
	        {
	            driver->driver.inverter.inputError = TRUE;
	            Ifx_Message_set(&driver->driver.inverter.messages, LB30_Message_errorCurrentInputW);
	        }
	    }
	}

#if CFG_CURRENT_SENSOR_TEST == 1
	if (driver->configuration.inverter[ECU_MOTOR_INDEX_1].currentSenor.max > 0)
	{
	    /* FIXME output message matching the inverter index */
	    if (LB30_Inverter_inputFault(&driver->analogInput.currents[ECU_MOTOR_INDEX_1][0], driver->configuration.inverter[ECU_MOTOR_INDEX_1].currentSenor.offset))
	    {
	        driver->driver.inverter.inputError = TRUE;
	        Ifx_Message_set(&driver->driver.inverter.messages, LB30_Message_errorCurrentInputU);
	    }

	    if (LB30_Inverter_inputFault(&driver->analogInput.currents[ECU_MOTOR_INDEX_1][1], driver->configuration.inverter[ECU_MOTOR_INDEX_1].currentSenor.offset))
	    {
	        driver->driver.inverter.inputError = TRUE;
	        Ifx_Message_set(&driver->driver.inverter.messages, LB30_Message_errorCurrentInputV);
	    }

	    if (driver->configuration.inverter[ECU_MOTOR_INDEX_1].currentSenor.useThreeSensors)
	    {
	        if (LB30_Inverter_inputFault(&driver->analogInput.currents[ECU_MOTOR_INDEX_1][2], driver->configuration.inverter[ECU_MOTOR_INDEX_1].currentSenor.offset))
	        {
	            driver->driver.inverter.inputError = TRUE;
	            Ifx_Message_set(&driver->driver.inverter.messages, LB30_Message_errorCurrentInputW);
	        }
	    }

	}
#endif

}


boolean LB30_Inverter_stdIfInverterInit(IfxStdIf_Inverter *stdif, LB30 *driver)
{
    /* Ensure the stdif is reset to zeros */
    memset(stdif, 0, sizeof(IfxStdIf_Inverter));

    /* Set the driver */
    stdif->driver = driver;

    /* *INDENT-OFF* Note: this file was indented manually by the author. */
    /* Set the API link */

    stdif->clearFlags       = (IfxStdIf_Inverter_ClearFlags      ) &LB30_Inverter_clearFlags         ;
    stdif->disable          = (IfxStdIf_Inverter_Disable         ) &LB30_Inverter_disable            ;
    stdif->enable           = (IfxStdIf_Inverter_Enable          ) &LB30_Inverter_enable             ;
    stdif->getDeadtime      = (IfxStdIf_Inverter_GetDeadtime     ) &LB30_Inverter_getDeadtime        ;
    stdif->getMinPulse      = (IfxStdIf_Inverter_GetMinPulse     ) &LB30_Inverter_getMinPulse        ;
    stdif->getPeriod        = (IfxStdIf_Inverter_GetPeriod       ) &LB30_Inverter_getPeriod          ;
    stdif->getPeriodTicks   = (IfxStdIf_Inverter_GetPeriodTicks  ) &LB30_Inverter_getPeriodTicks     ;
    stdif->getPhaseCurrents = (IfxStdIf_Inverter_GetPhaseCurrents) &LB30_Inverter_getPhaseCurrents   ;
    stdif->getPwm           = (IfxStdIf_Inverter_GetPwm          ) &LB30_Inverter_getPwm             ;
    stdif->getStatus        = (IfxStdIf_Inverter_GetStatus       ) &LB30_Inverter_getStatus          ;
    stdif->getSwitchTemps   = (IfxStdIf_Inverter_GetSwitchTemps  ) &LB30_Inverter_getSwitchTemps     ;
    stdif->getVdc           = (IfxStdIf_Inverter_GetVdc          ) &LB30_Inverter_getVdc             ;
    stdif->startPwmTimer    = (IfxStdIf_Inverter_StartPwmTimer   ) &LB30_Inverter_startPwmTimer      ;
    stdif->setDeadtime      = (IfxStdIf_Inverter_SetDeadtime     ) &LB30_Inverter_setDeadtime        ;
    stdif->setMinPulse      = (IfxStdIf_Inverter_SetMinPulse     ) &LB30_Inverter_setMinPulse        ;
    stdif->setPwm           = (IfxStdIf_Inverter_SetPwm          ) &LB30_Inverter_setPwm             ;
    stdif->setPwmOff        = (IfxStdIf_Inverter_SetPwmOff       ) &LB30_Inverter_setPwmOff          ;
    stdif->setPwmOn         = (IfxStdIf_Inverter_SetPwmOn        ) &LB30_Inverter_setPwmOn           ;
    stdif->setPulse         = (IfxStdIf_Inverter_SetPulse        ) &LB30_Inverter_setPulse           ;
    stdif->setPeriod        = (IfxStdIf_Inverter_SetPeriod       ) &LB30_Inverter_setPeriod          ;
    stdif->setPeriodTicks   = (IfxStdIf_Inverter_SetPeriodTicks  ) &LB30_Inverter_setPeriodTicks     ;
    stdif->setVoltage       = (IfxStdIf_Inverter_SetVoltage      ) &LB30_Inverter_setVoltage         ;
    stdif->showMessage      = (IfxStdIf_Inverter_ShowMessage     ) &LB30_Inverter_showMessage        ;

    /* *INDENT-ON* */

    return TRUE;
}


/** \brief Calibrate the current sensors offset.
 * \param driver Pointer to the logic board object
 *
 * \return none
 */
void LB30_currentOffsetsCalibrationStep(LB30 *driver)
{
    uint32 i;

    /* FIXME use average over multiple samples */
    // End of calibration
    for (i = 0; i < ECU_PHASE_PER_MOTOR_COUNT; i++)
    {
        driver->analogInput.currents[ECU_MOTOR_INDEX_0][i].input.scale.offset += -driver->analogInput.currents[ECU_MOTOR_INDEX_0][i].input.value / driver->analogInput.currents[ECU_MOTOR_INDEX_0][i].input.scale.gain;
    }
#if CFG_CURRENT_SENSOR_TEST == 1
    for (i = 0; i < ECU_PHASE_PER_MOTOR_COUNT; i++)
    {
        driver->analogInput.currents[ECU_MOTOR_INDEX_1][i].input.scale.offset += -driver->analogInput.currents[ECU_MOTOR_INDEX_1][i].input.value / driver->analogInput.currents[ECU_MOTOR_INDEX_1][i].input.scale.gain;
    }
#endif

    if (!driver->disableCurrentSensorCheck)
    {
    	LB30_Inverter_checkCurrentInputFault(driver);
    }
}


static void LB3x_setResolverGain(IfxPort_Pin *gainSel, uint32 gain)
{
    uint32 i;
    uint32 g;
    g = (uint32)gain;

    for (i = 0; i < 4; i++)
    {
        Pin_setState(&gainSel[i], (g & 0x1) ? IfxPort_State_high : IfxPort_State_low);
        g = g >> 1;
    }
}


void LB30_setResolver0Gain(LB30 *driver, LB30_ResolverGain gain)
{
    LB3x_setResolverGain(&driver->driver.Resolver0GainSel[0], (uint32)gain);
}


void LB31_setResolver0Gain(LB30 *driver, LB31_ResolverGain gain)
{
    LB3x_setResolverGain(&driver->driver.Resolver0GainSel[0], (uint32)gain);
}


void LB30_setResolver1Gain(LB30 *driver, LB30_ResolverGain gain)
{
    LB3x_setResolverGain(&driver->driver.Resolver1GainSel[0], (uint32)gain);
}


void LB31_setResolver1Gain(LB30 *driver, LB31_ResolverGain gain)
{
    LB3x_setResolverGain(&driver->driver.Resolver1GainSel[0], (uint32)gain);
}


void LB3x_setResolverXGain(LB30 *driver, uint8 index, uint8 gain)
{
	if (index == 0)
	{
	    LB3x_setResolverGain(&driver->driver.Resolver0GainSel[0], gain);
	}
	else
	{
	    LB3x_setResolverGain(&driver->driver.Resolver1GainSel[0], gain);
	}
}

void LB30_setResolverOutputSelect(LB30 *driver, LB30_ResolverOutputSelect select)
{
    Pin_setState(&driver->driver.ResolverOutputSel, select == LB30_ResolverOutputSelect_ad2s1210 ? IfxPort_State_high : IfxPort_State_low);
}


boolean LB30_init(LB30 *board)
{
    boolean result = TRUE;
    board->configuration.fileVersion = Lb_FileVersion_undefined;
    board->disableCurrentSensorCheck = FALSE;
    board->selftest.pwmEnabled		 = FALSE;
    board->selftest.running     		 = FALSE;
    board->selftest.dutyFaulty[0]		 = 0;
    board->selftest.dutyFaulty[1]		 = 0;
    board->selftest.dutyFaulty[2]		 = 0;
    board->selftest.duty[0]     		 = 0;
    board->selftest.duty[1]     		 = 0;
    board->selftest.duty[2]     		 = 0;


    result &= LB30_loadLogicBoardConfiguration(board);

#if CFG_AUTO_CONGIF_SETUP
    if (board->configuration.fileVersion == Lb_FileVersion_undefined)
    {
    	result &= LB30_createLogicBoardConfigFile(board, TRUE);

    	if (g_Lb.motorConfiguration.efs)
    	{ /* Make sure the EFS exists */
        	/* If the config file does not exist or version does not match, also delete the motor config file so that a clean version is created later  */
    		Ifx_MotorModelConfigF32_deleteFile(&g_Lb.motorConfiguration);
    	}

    }
#endif
    return result;
}


boolean LB30_createLogicBoardConfigFile(LB30 *board, boolean quiet)
{
    LB_FileConfiguration *configuration = &board->configuration;
    LB_setupDefaultValue(configuration);
    return App_createConfigFile(board->driver.efsConfig, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION, configuration, sizeof(*configuration), EFS_CFG_FILE_VERSION_LOGIC_BOARD_CONFIGURATION, quiet);
}


boolean LB30_updateLogicBoardConfigFile(LB30 *board)
{
    return App_updateConfigFile(board->driver.efsConfig, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION, &board->configuration, sizeof(board->configuration));
}


/**
 *  EFS is at offset 0
 */
boolean LB30_loadLogicBoardConfiguration(LB30 *board)
{
    boolean result;
    result                     = App_loadConfigFile(board->driver.efsConfig, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION, &board->configuration, sizeof(board->configuration), EFS_CFG_FILE_VERSION_LOGIC_BOARD_CONFIGURATION);

    board->configuration.ready = FALSE; /* FIXME disabled because on LB3.0 and LB3.1 the LB EEPROM and Motor1 Driver
                                         * use the same ECON0, which conflict. This has the effect that after driver
                                         * initialization the eeprom can not be accessed */
    return result;
}


boolean Lb3x_shellSet(LB30 *board, pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean result = FALSE;

    if (Ifx_Shell_matchToken(&args, "dout") != FALSE)
    {
        uint32 index;
        uint32 state;

        if ((Ifx_Shell_parseUInt32(&args, &index, FALSE) != FALSE) && (Ifx_Shell_parseUInt32(&args, &state, FALSE) != FALSE))
        {
            if (index > 3)
            {
                IfxStdIf_DPipe_print(io, "Error invalid index"ENDL);
            }
            else if (state > 1)
            {
                IfxStdIf_DPipe_print(io, "Error invalid state"ENDL);
            }
            else
            {
                LB30_setDigitalOutput(board, index, state != 0);
                result = TRUE;
            }
        }
    }
    else
    {}

    return result;
}


boolean Lb3x_shellGet(LB30 *board, pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean result = FALSE;

    if (Ifx_Shell_matchToken(&args, "dout") != FALSE)
    {
        uint32 index;

        if (Ifx_Shell_parseUInt32(&args, &index, FALSE) != FALSE)
        {
            if (index > 3)
            {
                IfxStdIf_DPipe_print(io, "Error invalid index"ENDL);
            }
            else
            {
                boolean state;
                state  = LB30_getDigitalOutput(board, index);
                IfxStdIf_DPipe_print(io, "%s"ENDL, (state ? "high" : "low"));
                result = TRUE;
            }
        }
    }
    else if (Ifx_Shell_matchToken(&args, "din") != FALSE)
    {
        uint32 index;

        if (Ifx_Shell_parseUInt32(&args, &index, FALSE) != FALSE)
        {
            if (index > 3)
            {
                IfxStdIf_DPipe_print(io, "Error invalid index"ENDL);
            }
            else
            {
                IfxPort_State state;
                state  = LB30_getDigitalInput(board, index);
                IfxStdIf_DPipe_print(io, "%s"ENDL, (state ? "high" : "low"));
                result = TRUE;
            }
        }
    }
    else
    {}

    return result;
}


boolean Lb3x_shellSetup(LB30 *board, pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean              result              = FALSE;
    boolean              updateConfiguration = FALSE;
    App                 *app                 = (App *)data;

    if (Ifx_Shell_matchToken(&args, "print") != FALSE)
    {
        LB_printBoardConfiguration(io, &board->configuration, board->boardVersion, TRUE);
        AppDbStdIf_printBoardConfiguration(board->driverBoardStdif, io);
        AppDbStdIf_isMinimalSetup(board->driverBoardStdif, io);
        result = TRUE;
    }
    else if (!AppShell_isValidCommand(io, AppShell_setup, app->hwState.hwMode))
    {
        return TRUE;
    }
    else if (Ifx_Shell_matchToken(&args, "ready") != FALSE)
    {
        uint32 value;

        if (Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE)
        {
            board->configuration.ready = value ? 1 : 0;
            updateConfiguration        = TRUE;
            result                     = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "console") != FALSE)
    {
        if (Ifx_Shell_matchToken(&args, "asc0") != FALSE)
        {
            board->configuration.primaryConsole = LB_PrimaryConsole_asc0;
            updateConfiguration                 = TRUE;
            result                              = TRUE;
        }
        else if (Ifx_Shell_matchToken(&args, "can0") != FALSE)
        {
            board->configuration.primaryConsole = LB_PrimaryConsole_can0;
            updateConfiguration                 = TRUE;
            result                              = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "posdir") != FALSE)
    {
        if (Ifx_Shell_matchToken(&args, "encoder") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.encoder.reversed = value == 0 ? FALSE : TRUE;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "tle5012") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.tle5012.reversed = value == 0 ? FALSE : TRUE;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "ad2s1210") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.ad2s1210.reversed = value == 0 ? FALSE : TRUE;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "rdc0") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.aurixResolver.input[0].reversed = value == 0 ? FALSE : TRUE;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "rdc1") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.aurixResolver.input[1].reversed = value == 0 ? FALSE : TRUE;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
    }
    else if (Ifx_Shell_matchToken(&args, "posresolution") != FALSE)
    {
        if (Ifx_Shell_matchToken(&args, "ad2s1210") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.ad2s1210.resolution = value;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "rdc0") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.aurixResolver.input[0].resolution = value;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "rdc1") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.aurixResolver.input[1].resolution = value;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "encoder") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.encoder.resolution = value;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
    }
    else if (Ifx_Shell_matchToken(&args, "posoffset") != FALSE)
    {
        if (Ifx_Shell_matchToken(&args, "ad2s1210") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.ad2s1210.offset = value;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "rdc0") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.aurixResolver.input[0].offset = value;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "rdc1") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.aurixResolver.input[1].offset = value;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "encoder") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.encoder.offset = value;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "tle5012") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.tle5012.offset = value;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
    }
    else if (Ifx_Shell_matchToken(&args, "poscarrier") != FALSE)
    {
        if (Ifx_Shell_matchToken(&args, "ad2s1210") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.ad2s1210.carrierFrequency = value;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "rdc") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.aurixResolver.carrierFrequency = value;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
    }
    else if (Ifx_Shell_matchToken(&args, "posgain") != FALSE)
    {
    	uint32 min;
    	uint32 max;

    	if (board->boardVersion->boardVersion >= LB_BoardVersion_HybridKit_LogicBoard_3_1)
    	{
        	min = LB31_ResolverGain_0_75;
        	max = LB31_ResolverGain_2_63;
    	}
    	else
    	{
        	min = LB30_ResolverGain_2_08;
        	max = LB30_ResolverGain_1_60;
    	}

    	if (Ifx_Shell_matchToken(&args, "ad2s1210") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {

				if ((value >= min) && (value <= max))
				{
					board->configuration.positionSensors.ad2s1210.gainCode = (uint8)value;
					updateConfiguration                             = TRUE;
					result                = TRUE;
				}
				else
				{
					IfxStdIf_DPipe_print(io, "Code value out of range [%d,%d]"ENDL, LB30_ResolverGain_2_08, LB30_ResolverGain_1_60);
					result                = TRUE;
				}
            }
        }
        else if (Ifx_Shell_matchToken(&args, "rdc0") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {

				if ((value >= min) && (value <= max))
				{
					board->configuration.positionSensors.aurixResolver.input[0].gainCode = (uint8)value;
					updateConfiguration                             = TRUE;
					result                = TRUE;
				}
				else
				{
					IfxStdIf_DPipe_print(io, "Code value out of range [%d,%d]"ENDL, LB30_ResolverGain_2_08, LB30_ResolverGain_1_60);
					result                = TRUE;
				}
            }
        }
        else if (Ifx_Shell_matchToken(&args, "rdc1") != FALSE)
        {
            uint32                     value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {

				if ((value >= min) && (value <= max))
				{
					board->configuration.positionSensors.aurixResolver.input[1].gainCode = (uint8)value;
					updateConfiguration                             = TRUE;
					result                = TRUE;
				}
				else
				{
					IfxStdIf_DPipe_print(io, "Code value out of range [%d,%d]"ENDL, LB30_ResolverGain_2_08, LB30_ResolverGain_1_60);
					result                = TRUE;
				}
            }
        }
    }
    else if (Ifx_Shell_matchToken(&args, "possignalrange") != FALSE)
    {
        if (Ifx_Shell_matchToken(&args, "rdc0") != FALSE)
        {
            uint32                     min;
            uint32                     max;
            if ((Ifx_Shell_parseUInt32(&args, &min, FALSE) != FALSE) && (Ifx_Shell_parseUInt32(&args, &max, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.aurixResolver.input[0].signalAmplitudeMax = max;
            	board->configuration.positionSensors.aurixResolver.input[0].signalAmplitudeMin = min;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "rdc1") != FALSE)
        {
            uint32                     min;
            uint32                     max;
            if ((Ifx_Shell_parseUInt32(&args, &min, FALSE) != FALSE) && (Ifx_Shell_parseUInt32(&args, &max, FALSE) != FALSE))
            {
            	board->configuration.positionSensors.aurixResolver.input[1].signalAmplitudeMax = max;
            	board->configuration.positionSensors.aurixResolver.input[1].signalAmplitudeMin = min;
                updateConfiguration                             = TRUE;
                result                = TRUE;
            }
        }
    }
    else if (Ifx_Shell_matchToken(&args, "m0") != FALSE)
    {
        if (Ifx_Shell_matchToken(&args, "position") != FALSE)
        {
        	ECU_PositionSensor positionSensor = ECU_PositionSensor_none;
            if (Ifx_Shell_matchToken(&args, "encoder") != FALSE)
            {
            	positionSensor = ECU_PositionSensor_encoder;
                result                                          = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "tle5012") != FALSE)
            {
            	positionSensor = ECU_PositionSensor_tle5012;
                result                                          = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "ad2s1210") != FALSE)
            {
            	positionSensor = ECU_PositionSensor_ad2s1210;
                result                                          = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "rdc0") != FALSE)
            {
            	positionSensor = ECU_PositionSensor_internalResolver0;
                result                                          = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "rdc1") != FALSE)
            {
            	positionSensor = ECU_PositionSensor_internalResolver1;
                result                                          = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "none") != FALSE)
            {
            	positionSensor = ECU_PositionSensor_none;
                result                                          = TRUE;
            }

        	if (result)
        	{
        		if (board->configuration.inverter[0].positionSensor[1] == positionSensor)
        		{
        			board->configuration.inverter[0].positionSensor[1] = ECU_PositionSensor_none;
					IfxStdIf_DPipe_print(io, "Redundant sensor must be different than the primary sensor, redundant sensor set to none"ENDL);
        		}
        		else if ((positionSensor == ECU_PositionSensor_tle5012)
        				&& (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_encoder))
        		{
        			board->configuration.inverter[0].positionSensor[1] = ECU_PositionSensor_none;
					IfxStdIf_DPipe_print(io, "Redundant sensor can't be encoder if primary sensor is iGMR"ENDL);
        		}
                board->configuration.inverter[0].positionSensor[0] = positionSensor;
                updateConfiguration                             = TRUE;
        	}
        }
        else if (Ifx_Shell_matchToken(&args, "position1") != FALSE)
        {
        	ECU_PositionSensor positionSensor = ECU_PositionSensor_none;
            if (Ifx_Shell_matchToken(&args, "encoder") != FALSE)
            {
            	positionSensor = ECU_PositionSensor_encoder;
                result                                          = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "tle5012") != FALSE)
            {
            	positionSensor = ECU_PositionSensor_tle5012;
                result                                          = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "ad2s1210") != FALSE)
            {
            	positionSensor = ECU_PositionSensor_ad2s1210;
                result                                          = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "rdc0") != FALSE)
            {
            	positionSensor = ECU_PositionSensor_internalResolver0;
                result                                          = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "rdc1") != FALSE)
            {
            	positionSensor = ECU_PositionSensor_internalResolver1;
                result                                          = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "none") != FALSE)
            {
            	positionSensor = ECU_PositionSensor_none;
                result                                          = TRUE;
            }

        	if (result)
        	{
        		if (board->configuration.inverter[0].positionSensor[0] == positionSensor)
        		{
					IfxStdIf_DPipe_print(io, "Redundant sensor must be different than the primary sensor"ENDL);
        		}
        		else if ((board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_tle5012)
        				&& (positionSensor == ECU_PositionSensor_encoder))
        		{
					IfxStdIf_DPipe_print(io, "Redundant sensor can't be iGMR if primary sensor is encoder"ENDL);
        		}
        		else
        		{
                    board->configuration.inverter[0].positionSensor[1] = positionSensor;
                    updateConfiguration                             = TRUE;
        		}
        	}
        }
        else if (Ifx_Shell_matchToken(&args, "current") != FALSE)
        {
            if (Ifx_Shell_matchToken(&args, "count") != FALSE)
            {
                uint32 count;

                if ((Ifx_Shell_parseUInt32(&args, &count, FALSE) != FALSE))
                {
                    if ((count >= 2) && (count <= 3))
                    {
                        board->configuration.inverter[0].currentSenor.useThreeSensors = count == 3;
                        updateConfiguration                                           = TRUE;
                    }
                    else
                    {
                        IfxStdIf_DPipe_print(io, "invalid parameter <count>"ENDL);
                    }

                    result = TRUE;
                }
            }
            else if (Ifx_Shell_matchToken(&args, "gain") != FALSE)
            {
                float32 gain;

                if ((Ifx_Shell_parseFloat32(&args, &gain) != FALSE))
                {
                    board->configuration.inverter[0].currentSenor.gain = gain * CFG_ADC_GAIN;
                    updateConfiguration                                = TRUE;
                    result                                             = TRUE;
                }
            }
            else if (Ifx_Shell_matchToken(&args, "offset") != FALSE)
            {
                float32 offset;

                if ((Ifx_Shell_parseFloat32(&args, &offset) != FALSE))
                {
                    board->configuration.inverter[0].currentSenor.offset = -offset / CFG_ADC_GAIN;
                    updateConfiguration                                  = TRUE;
                    result                                               = TRUE;
                }
            }
            else if (Ifx_Shell_matchToken(&args, "max") != FALSE)
            {
                float32 max;

                if ((Ifx_Shell_parseFloat32(&args, &max) != FALSE))
                {
                    board->configuration.inverter[0].currentSenor.max = max;
                    updateConfiguration                               = TRUE;
                    result                                            = TRUE;
                }
            }
            else if (Ifx_Shell_matchToken(&args, "digfilter") != FALSE)
            {
                float32 cutoff;

                if ((Ifx_Shell_parseFloat32(&args, &cutoff) != FALSE))
                {
                    board->configuration.inverter[0].currentSenor.cutOffFrequency = cutoff;
                    updateConfiguration                                           = TRUE;
                    result                                                        = TRUE;
                }
            }
            else if (Ifx_Shell_matchToken(&args, "checkoff") != FALSE)
            { /* Undocumented feature in shell help */
            	board->disableCurrentSensorCheck = TRUE;
                result = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "pwm") != FALSE)
        {
            if (Ifx_Shell_matchToken(&args, "ccu6") != FALSE)
            {
                board->configuration.inverter[0].pwmModule = LB_PwmModule_ccu6;
                updateConfiguration                        = TRUE;
                result                                     = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "atom") != FALSE)
            {
                board->configuration.inverter[0].pwmModule = LB_PwmModule_gtmAtom;
                updateConfiguration                        = TRUE;
                result                                     = TRUE;
            }
            else if (Ifx_Shell_matchToken(&args, "tom") != FALSE)
            {
                board->configuration.inverter[0].pwmModule = LB_PwmModule_gtmTom;
                updateConfiguration                        = TRUE;
                result                                     = TRUE;
            }

        }
    }
    else if (Ifx_Shell_matchToken(&args, "db") != FALSE)
    {
		result = AppDbStdIf_shellSetup(board->driverBoardStdif, args, data, io);
    }
    else if (Ifx_Shell_matchToken(&args, "verbose") != FALSE)
    {
        uint32 level;

        if ((Ifx_Shell_parseUInt32(&args, &level, FALSE) != FALSE))
        {
            board->configuration.verboseLevel = (uint8)level;
            updateConfiguration               = TRUE;
        }

        Ifx_Assert_printLevel(board->configuration.verboseLevel, io);
        result = TRUE;
    }
    else if (Ifx_Shell_matchToken(&args, "reset") != FALSE)
    {
        LB_setupDefaultValue(&board->configuration);
        LB30_updateLogicBoardConfigFile(board);
        AppDbStdIf_setDefaultConfig(board->driverBoardStdif);
        AppDbStdIf_saveConfig(board->driverBoardStdif);
        Ifx_MotorModelConfigF32_setupDefaultValue(&g_Lb.motorConfiguration);
        Ifx_MotorModelConfigF32_updateFile(&g_Lb.motorConfiguration);
        result = TRUE;
    }
    else if (Ifx_Shell_matchToken(&args, "wwd") != FALSE)
    {
        uint32 enabled;

        if (Ifx_Shell_matchToken(&args, "wdi") != FALSE)
        {
            board->configuration.safety.windowWatchdog.useWdiPin = TRUE;
            updateConfiguration               = TRUE;
            result = TRUE;
        }
        else if (Ifx_Shell_matchToken(&args, "spi") != FALSE)
        {
            board->configuration.safety.windowWatchdog.useWdiPin = FALSE;
            updateConfiguration               = TRUE;
            result = TRUE;
        }
        else if (Ifx_Shell_matchToken(&args, "ow") != FALSE)
        {
        	float32 value;
            if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
            {
                board->configuration.safety.windowWatchdog.openWindowTime = value;
                updateConfiguration               = TRUE;
                result = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "cw") != FALSE)
        {
        	float32 value;
            if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
            {
                board->configuration.safety.windowWatchdog.closeWindowTime = value;
                updateConfiguration               = TRUE;
                result = TRUE;
            }
        }
        else if ((Ifx_Shell_parseUInt32(&args, &enabled, FALSE) != FALSE))
        {
            board->configuration.safety.windowWatchdog.enabled = enabled == 0 ? FALSE : TRUE;
            updateConfiguration               = TRUE;
            result = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "fwd") != FALSE)
    {
        uint32 enabled;

        if (Ifx_Shell_matchToken(&args, "hb") != FALSE)
		{
			float32 value;
			if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
			{
				board->configuration.safety.functionalWatchdog.heartbeatTimerPeriod = value;
				updateConfiguration               = TRUE;
				result = TRUE;
			}
		}
        else if (Ifx_Shell_matchToken(&args, "s") != FALSE)
		{
			float32 value;
			if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
			{
				board->configuration.safety.functionalWatchdog.servicePeriod = value;
				updateConfiguration               = TRUE;
				result = TRUE;
			}
		}
        else if ((Ifx_Shell_parseUInt32(&args, &enabled, FALSE) != FALSE))
        {
            board->configuration.safety.functionalWatchdog.enabled = enabled == 0 ? FALSE : TRUE;
            updateConfiguration               = TRUE;
            result = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "fsp") != FALSE)
    {
        uint32 enabled;

        if ((Ifx_Shell_parseUInt32(&args, &enabled, FALSE) != FALSE))
        {
            board->configuration.safety.fsp.enabled = enabled == 0 ? FALSE : TRUE;
            updateConfiguration               = TRUE;
            result = TRUE;
        }
    }
    else if (Ifx_Shell_matchToken(&args, "iom") != FALSE)
    {

        if (Ifx_Shell_matchToken(&args, "fsp") != FALSE)
        {
            uint32 enabled;
            if ((Ifx_Shell_parseUInt32(&args, &enabled, FALSE) != FALSE))
            {
				board->configuration.safety.iom.fspOnFaultEnabled = enabled == 0 ? FALSE : TRUE;
                updateConfiguration               = TRUE;
                result = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "nmi") != FALSE)
        {
            uint32 enabled;
            if ((Ifx_Shell_parseUInt32(&args, &enabled, FALSE) != FALSE))
            {
				board->configuration.safety.iom.nmiOnFaultEnabled = enabled == 0 ? FALSE : TRUE;
                updateConfiguration               = TRUE;
                result = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "threshold") != FALSE)
        {
        	float32 value;
            if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
            {
                board->configuration.safety.iom.eventWindowThreshold = value * 1e-6;
                updateConfiguration               = TRUE;
                result = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "flitertime") != FALSE)
        {
        	float32 value;
            if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
            {
                board->configuration.safety.iom.filterTime = value * 1e-6;
                updateConfiguration               = TRUE;
                result = TRUE;
            }
        }
        else
        {
            uint32 enabled;
            if ((Ifx_Shell_parseUInt32(&args, &enabled, FALSE) != FALSE))
            {
				board->configuration.safety.iom.enabled = enabled == 0 ? FALSE : TRUE;
                updateConfiguration               = TRUE;
                result = TRUE;
            }

        }

    }
    else if (Ifx_Shell_matchToken(&args, "hwpulse") != FALSE)
    {

        if (Ifx_Shell_matchToken(&args, "enable") != FALSE)
        {
            uint32 value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
				board->configuration.specialMode.hwPulseMode.enabled = value == 1;
                updateConfiguration               = TRUE;
                result = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "period") != FALSE)
        {
            float32 value;
            if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
            {
				board->configuration.specialMode.hwPulseMode.pulsePeriod = value*1e-6;
                updateConfiguration               = TRUE;
                result = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "start") != FALSE)
        {
            float32 value;
            if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
            {
				board->configuration.specialMode.hwPulseMode.startPulse = value*1e-6;
                updateConfiguration               = TRUE;
                result = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "inc") != FALSE)
        {
            float32 value;
            if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
            {
				board->configuration.specialMode.hwPulseMode.pulseIncrement = value*1e-6;
                updateConfiguration               = TRUE;
                result = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "count") != FALSE)
        {
            uint32 value;
            if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
            {
				board->configuration.specialMode.hwPulseMode.pulseCountPerPhase = value;
                updateConfiguration               = TRUE;
                result = TRUE;
            }
        }
        else if (Ifx_Shell_matchToken(&args, "pwmperiod") != FALSE)
        {
            float32 value;
            if ((Ifx_Shell_parseFloat32(&args, &value) != FALSE))
            {
				board->configuration.specialMode.hwPulseMode.pwmPeriod = value*1e-6;
                updateConfiguration               = TRUE;
                result = TRUE;
            }
        }
    }


    if (result)
    {
        if (updateConfiguration)
        {
            LB30_updateLogicBoardConfigFile(board);
        }

    }

    return result;
}


/* FIXME LB30 should be renamed LB3x*/


void LB30_setDigitalOutput(LB30 *board, uint32 index, boolean state)
{
    if (index < 4)
    {
        Pin_setState(&board->driver.digitalOutputs[index], state ? IfxPort_State_high : IfxPort_State_low);
    }
}


boolean LB30_getDigitalInput(LB30 *board, uint32 index)
{
    if (index < 4)
    {
        return Pin_getState(&board->driver.digitalInputs[index]);
    }
    else
    {
        return FALSE;
    }
}


boolean LB30_getDigitalOutput(LB30 *board, uint32 index)
{
    if (index < 4)
    {
        return Pin_getState(&board->driver.digitalOutputs[index]);
    }
    else
    {
        return FALSE;
    }
}

float32 LB30_getAnalogInput(LB30 *board, uint32 index)
{
    if (index < 4)
    {
        //return board->analogInput.in[index].input.value;
    }
    else
    {
        return 0.0;
    }
}

void LB30_raiseIomFault(LB30 *board)
{
	if (board->configuration.safety.iom.enabled)
	{
		if (board->selftest.pwmEnabled)
		{
			return; // ignore the fault while in self test mode
		}
		uint16 a, b, c, d;
		uint16 flags;
		board->driver.inverter.iomError = TRUE;

		IfxIom_Driver_getHistory(&board->driver.inverter.iom.driver, &a, &b, &c, &d);
		IfxIom_Driver_clearHistory(&board->driver.inverter.iom.driver);
		flags = a | b | c| d;

		if (flags & 0x01)
		{
			Ifx_Message_setFromTrap(&board->driver.inverter.messages, 	LB30_Message_errorIomUTop);
		}
		if (flags & 0x02)
		{
			Ifx_Message_setFromTrap(&board->driver.inverter.messages, 	LB30_Message_errorIomUBottom);
		}
		if (flags & 0x04)
		{
			Ifx_Message_setFromTrap(&board->driver.inverter.messages, 	LB30_Message_errorIomVTop);
		}
		if (flags & 0x08)
		{
			Ifx_Message_setFromTrap(&board->driver.inverter.messages, 	LB30_Message_errorIomVBottom);
		}
		if (flags & 0x10)
		{
			Ifx_Message_setFromTrap(&board->driver.inverter.messages, 	LB30_Message_errorIomWTop);
		}
		if (flags & 0x20)
		{
			Ifx_Message_setFromTrap(&board->driver.inverter.messages, 	LB30_Message_errorIomWBottom);
		}
	}
}

boolean Lb3x_createConfigEfs(LB30 *board, boolean quiet)
{
    return App_createEfs(board->driver.efsConfig, CFG_LB_CONFIG_EFS_OFFSET, CFG_LB_CONFIG_EFS_PARTITION_SIZE, CFG_LB_CONFIG_EFS_FILE_SIZE, quiet);
}

boolean Lb3x_isConfigFile(LB30 *board)
{
	return board->configuration.fileVersion != Lb_FileVersion_undefined;
}


boolean Lb3x_isBoardTypeSupported(LB30 *board)
{
	return (board->boardVersion->boardType == Lb_BoardType_HybridKit_LogicBoard);
}

boolean Lb3x_isBoardVersionSupported(LB30 *board)
{
	return (board->boardVersion->boardType == Lb_BoardType_HybridKit_LogicBoard)
    		&& (
    				(board->boardVersion->boardVersion == LB_BoardVersion_HybridKit_LogicBoard_3_0)
    				                    || (board->boardVersion->boardVersion == LB_BoardVersion_HybridKit_LogicBoard_3_1)
    				                    || (board->boardVersion->boardVersion == LB_BoardVersion_HybridKit_LogicBoard_3_3)
				);
}

boolean Lb3x_isConfigFileVersionActual(LB30 *board)
{
	return board->configuration.fileVersion == EFS_CFG_FILE_VERSION_LOGIC_BOARD_CONFIGURATION;
}

boolean Lb3x_isConfigValid(LB30 *board)
{
    boolean result = TRUE;
//    Ifx_Console_printAlign("**************************"ENDL);
//    Ifx_Console_printAlign("* Checking configuration *"ENDL);
//    Ifx_Console_printAlign("**************************"ENDL);

    if ((AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
    		|| (AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
    		)
    {
        if (board->configuration.inverter[0].pwmModule == LB_PwmModule_gtmAtom)
        {
        }
        else if (board->configuration.inverter[0].pwmModule == LB_PwmModule_gtmTom)
        {/* Trigger to EiceSence not available from TOM for ADC conversion */
        	result = FALSE;
//            Ifx_Console_printAlign("- ERROR Configuration not supported:");
//            Ifx_Console_printAlign("	- 'HP Drive Sense', 'HP Drive Sense DSC' driver board ");
//            Ifx_Console_printAlign("	- GTM TOM can not be used for PWM generation");

            if(board->configuration.safety.iom.enabled)
            {
            	result = FALSE;
//                Ifx_Console_printAlign("- ERROR Configuration not supported:");
//                Ifx_Console_printAlign("	- IOM feature not supported with 'HP Drive Sense'and  'HP Drive Sense' driver board ");
            }
        }
        else// if (board->configuration.inverter[0].pwmModule == LB_PwmModule_ccu6)
        {
            if ((AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
            		|| (AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
            		)
            {
            	result = FALSE;
//                Ifx_Console_printAlign("- ERROR Configuration not supported:");
//                Ifx_Console_printAlign("	- 'HP Drive Sense', 'HP Drive Sense DSC' driver board ");
//                Ifx_Console_printAlign("	- CCU6 can not be used for PWM generation");
            }
        }
    }


    if (board->boardVersion->boardVersion == LB_BoardVersion_HybridKit_LogicBoard_3_0)
    {
        if ((board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_ad2s1210)
        		|| (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_ad2s1210))
        {   /* AD2S1210 is not functional on LB 3.0, LB3.1  */
        	result = FALSE;
//            Ifx_Console_printAlign("- ERROR Configuration not supported:");
//            Ifx_Console_printAlign("	- AD2S1210 not supported by this version of the logic board ");

        }
    	if (((board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_tle5012)
    			|| (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_tle5012)))
    	{
        	result = FALSE;
//            Ifx_Console_printAlign("- ERROR Configuration not supported:");
//            Ifx_Console_printAlign("	- iGMR over SPI not supported by this version of the board");
    	}

    }
    else if (board->boardVersion->boardVersion == LB_BoardVersion_HybridKit_LogicBoard_3_1)
    {
        if ((board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_ad2s1210)
        		|| (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_ad2s1210))
        {   /* AD2S1210 is not functional on LB 3.0, LB3.1  */
        	result = FALSE;
//            Ifx_Console_printAlign("- ERROR Configuration not supported:");
//            Ifx_Console_printAlign("	- AD2S1210 not supported by this version of the logic board ");

        }
    }


	if ((board->configuration.inverter[0].positionSensor[0] == board->configuration.inverter[0].positionSensor[1])
			&& (board->configuration.inverter[0].positionSensor[0] != ECU_PositionSensor_none))
	{
    	result = FALSE;
//        Ifx_Console_printAlign("- ERROR Configuration not supported:");
//        Ifx_Console_printAlign("	- Position sensor 1 can not be the same as position sensor 0");
	}

	if (((board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_tle5012)
			|| (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_tle5012))
			&& ((board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_encoder)
					|| (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_encoder)))
	{
    	result = FALSE;
//        Ifx_Console_printAlign("- ERROR Configuration not supported:");
//        Ifx_Console_printAlign("	- iGMR and encoder can not be used at the same time.");
	}

#if BYPASSEEPROM
	return TRUE;
#else if
    return result;
#endif
}

boolean LB30_LbStdIfInit(AppLbStdIf *stdif, LB30 *board)
{
	uint32 motorIndex;
	uint32 phaseIndex;
    memset(stdif, 0, sizeof(AppLbStdIf));

    stdif->canNodes[0]       = &board->driver.canNodes[0];
    stdif->canNodes[1]       = &board->driver.canNodes[1];
    stdif->asc0              = &board->stdIf.asc0;
    stdif->dsadcRdcDriver[0]   = &board->driver.dsadcRdc[0];
    stdif->dsadcRdcDriver[1]   = &board->driver.dsadcRdc[1];
    stdif->encoder           = &board->stdIf.encoder;
    stdif->tle5012              = &board->stdIf.tle5012;
    stdif->ad2s1210          = &board->stdIf.ad2s1210;
    stdif->dsadcRdc[0]         = &board->stdIf.dsadcRdc[0];
    stdif->dsadcRdc[1]         = &board->stdIf.dsadcRdc[1];
    stdif->inverter          = &board->stdIf.inverter;
    for (motorIndex=ECU_MOTOR_INDEX_0;motorIndex<=ECU_MOTOR_INDEX_1;motorIndex++)
    {
        for (phaseIndex=0;phaseIndex<ECU_PHASE_PER_MOTOR_COUNT;phaseIndex++)
        {
            stdif->currents[motorIndex][phaseIndex]         = &board->analogInput.currents[motorIndex][phaseIndex];
        }
    }

    stdif->motorTemp         = &board->analogInput.motorTemp.input.value;
    //stdif->in[0]             = &board->analogInput.in[0].input.value;	// Jovi
    //stdif->in[1]             = &board->analogInput.in[1].input.value;
    //stdif->in[2]             = &board->analogInput.in[2].input.value;
    //stdif->in[3]             = &board->analogInput.in[3].input.value;
    stdif->tempBoard         = &board->analogInput.tempBoard.input.value;
    stdif->vAna50            = &board->analogInput.vAna50.input.value;
    stdif->vRef50            = &board->analogInput.vRef50.input.value;
    stdif->vDig50            = &board->analogInput.vDig50.input.value;
    stdif->kl30              = &board->analogInput.kl30.input.value;

    stdif->configuration     = &board->configuration;
    stdif->disableCurrentSensorCheck     = &board->disableCurrentSensorCheck;

    /* Set the API link */
    stdif->driver                        = board;
    stdif->shellSetup                    = (AppLbStdIf_ShellSetup) & Lb3x_shellSetup;
    stdif->shellSet                      = (AppLbStdIf_ShellSet) & Lb3x_shellSet;
    stdif->shellGet                      = (AppLbStdIf_ShellGet) & Lb3x_shellGet;
    stdif->appInit                       = (AppLbStdIf_AppInit) & LB30_AppInit;
    stdif->appPreInit                    = (AppLbStdIf_AppPreInit) & LB30_AppPreInit;
    stdif->currentOffsetsCalibrationStep = (AppLbStdIf_CurrentOffsetsCalibrationStep) & LB30_currentOffsetsCalibrationStep;
    stdif->updateLogicBoardConfigFile    = (AppLbStdIf_UpdateLogicBoardConfigFile) & LB30_updateLogicBoardConfigFile;
    stdif->appInitConsole                = (AppLbStdIf_AppInitConsole) & LB30_AppInitConsole;
    stdif->appInit1MsTimer               = (AppLbStdIf_AppInit1MsTimer) & LB30_AppInit_1ms;
    stdif->appPostInit                   = (AppLbStdIf_AppPostInit) & LB30_AppPostInit;
    stdif->appSelfTest                   = (AppLbStdIf_AppSelfTest) & LB30_AppSelfTest;
    stdif->setDigitalOutput              = (AppLbStdIf_SetDigitalOutput) & LB30_setDigitalOutput;
    stdif->getDigitalInput               = (AppLbStdIf_GetDigitalInput) & LB30_getDigitalInput;
    stdif->getDigitalOutput              = (AppLbStdIf_GetDigitalOutput) & LB30_getDigitalOutput;
    stdif->getAnalogInput                = (AppLbStdIf_GetAnalogInput) & LB30_getAnalogInput;	// Jovi
    stdif->raiseIomFault                 = (AppLbStdIf_RaiseIomFault) & LB30_raiseIomFault;

    stdif->isBoardTypeSupported          = (AppLbStdIf_IsBoardTypeSupported     )&Lb3x_isBoardTypeSupported     ;
    stdif->isBoardVersionSupported       = (AppLbStdIf_IsBoardVersionSupported  )&Lb3x_isBoardVersionSupported  ;
    stdif->isConfigFileVersionActual     = (AppLbStdIf_IsConfigFileVersionActual)&Lb3x_isConfigFileVersionActual;
    stdif->isConfigValid     			 = (AppLbStdIf_IsConfigValid)&Lb3x_isConfigValid;

    stdif->createConfigEfs               = (AppLbStdIf_CreateConfigEfs          )&Lb3x_createConfigEfs          ;
    stdif->createConfigFile              = (AppLbStdIf_CreateConfigFile         )&LB30_createLogicBoardConfigFile         ;
    stdif->isConfigFile                  = (AppLbStdIf_IsConfigFile             )&Lb3x_isConfigFile             ;
    stdif->loadConfigFile                = (AppLbStdIf_LoadConfigFile           )&LB30_loadLogicBoardConfiguration           ;
	stdif->setResolverGain              = (AppLbStdIf_SetResolverGain          )&LB3x_setResolverXGain           ;


    return TRUE;
}
