/**
 * \file LB30_AppInit.h
 * \brief Logic board 3.0 initialization
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */
#ifndef LB30_APPINIT_H
#define LB30_APPINIT_H
#include "SysSe/Bsp/Bsp.h"
#include "Common/LB.h"

boolean LB30_AppInitConsole(LB30 *board, boolean primaryConsoleAsc);
boolean LB30_AppInit(LB30 *board);
boolean LB30_AppInit_smu(LB30 *board);
boolean LB30_AppInit_tlf35584(LB30 *board);
boolean LB30_AppInit_Qspi2(LB30 *board);
boolean LB30_AppInit_Dio(LB30 *board);
boolean LB30_AppInit_1ms(LB30 *board);

#endif
