/**
 * \file LB30_AppSelfTest.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include "LogicBoard3_0/App/LB30_AppSelfTest.h"
#include "SysSe/Comm/Ifx_Console.h"

boolean LB30_AppSelfTest_dsadcRdc(LB30 *board)
{
	boolean success = TRUE;
    IfxStdIf_Pos_Status status;
    if ( (board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver0) || (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_internalResolver0))
    {
        uint8 index = 0;
        App_printStatusTitle("Checking AURIX RDC %d", index);
        status = IfxStdIf_Pos_getFault(&board->stdIf.dsadcRdc[index]);
        IfxStdIf_Pos_printStatus(status, Ifx_g_console.standardIo);
        success &= status.status == 0;
    }
    if ( (board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver1) || (board->configuration.inverter[0].positionSensor[1] == ECU_PositionSensor_internalResolver1))
    {
        uint8 index = 1;
        App_printStatusTitle("Checking AURIX RDC %d", index);
        status = IfxStdIf_Pos_getFault(&board->stdIf.dsadcRdc[index]);
        IfxStdIf_Pos_printStatus(status, Ifx_g_console.standardIo);
        success &= status.status == 0;
    }

    App_printStatus(success);
	return success;
}

void LB30_AppSelfTest_iomSetDuty(LB30 *board, uint32 phaseIndex, Ifx_TimerValue duty, Ifx_TimerValue faultyDuty)
{
	uint32 i;

	for (i=0;i<3;i++)
	{
	    board->selftest.duty[i]     		 = phaseIndex == i ? duty : faultyDuty;
	    board->selftest.dutyFaulty[i]		 = faultyDuty; /* Ensure a NULL vector at the motor */
	}
}


// Generate single errors on each phase and check the occurrence of the fault after each event
// Warning: This test requires the IGBT to be driver, which would produce a voltage on the motor phases!
// Check at IOM history
boolean LB30_AppSelfTest_iom(LB30 *board)
{
	boolean success = TRUE;
	Ifx_TickTime upadteWaitTime;
	Ifx_TimerValue pwmPeriodTick;
    float32 timerFrequency;
	uint16 a, b, c, d;
    Ifx_TimerValue defaultDuty;

    timerFrequency = IfxStdIf_Timer_getInputFrequency(IfxStdIf_PwmHl_getTimer(&board->driver.inverter.pwmStdIf));

	upadteWaitTime = LB30_Inverter_getPeriod(board) * TimeConst_1s*2;
	pwmPeriodTick = LB30_Inverter_getPeriodTicks(board);


//    App_printStatusTitle("Checking IOM feature");

    defaultDuty = pwmPeriodTick * 0.5;
    LB30_AppSelfTest_iomSetDuty(board, 0, defaultDuty, defaultDuty); // Set all phase duty to defaultDuty, no fault
    board->selftest.pwmEnabled = TRUE;
    wait(upadteWaitTime);
    IfxIom_Driver_clearHistory(&board->driver.inverter.iom.driver);
    wait(upadteWaitTime);
    IfxIom_Driver_getHistory(&board->driver.inverter.iom.driver, &a, &b, &c, &d);
    if ((a | b | c | d) ) // Expected events: none
    {
    	success = FALSE;
//        Ifx_Console_printAlign("Error, event occurred but none expected"ENDL);
    }

    uint32 phaseIndex = 0;
    for (phaseIndex=0;phaseIndex<3;phaseIndex++)
    {

    	uint16 ref;
    	ref = (0x0003 << (phaseIndex *2));
        // Test Phase top / bottom shift
        LB30_AppSelfTest_iomSetDuty(board, phaseIndex, defaultDuty, defaultDuty); // Set all phase duty to defaultDuty, no fault
        wait(upadteWaitTime);
        IfxIom_Driver_clearHistory(&board->driver.inverter.iom.driver);
        LB30_AppSelfTest_iomSetDuty(board, phaseIndex, defaultDuty, defaultDuty + IfxStdIf_Timer_sToTick(timerFrequency, board->configuration.safety.iom.eventWindowThreshold + 1e-6) * 2);
        wait(upadteWaitTime);
        IfxIom_Driver_getHistory(&board->driver.inverter.iom.driver, &a, &b, &c, &d);
        if ((a | b | c | d) !=ref) // Expected events: one per edge of each top and bottom signals
        {
        	success = FALSE;
//            Ifx_Console_printAlign("Error phase %d, shift test"ENDL, phaseIndex);
        }

        // Test Phase top / bottom flat high
        LB30_AppSelfTest_iomSetDuty(board, phaseIndex, defaultDuty, defaultDuty); // Set all phase duty to defaultDuty, no fault
        wait(upadteWaitTime);
        IfxIom_Driver_clearHistory(&board->driver.inverter.iom.driver);
        LB30_AppSelfTest_iomSetDuty(board, phaseIndex, defaultDuty, pwmPeriodTick);
        wait(upadteWaitTime*2);
        IfxIom_Driver_getHistory(&board->driver.inverter.iom.driver, &a, &b, &c, &d);
        if ((a | b | c | d) !=ref)// Expected events: one per falling edge of each top and bottom signals
        {
        	success = FALSE;
//            Ifx_Console_printAlign("Error phase %d, flat high test"ENDL, phaseIndex);
        }

        // Test Phase top / bottom flat low
        LB30_AppSelfTest_iomSetDuty(board, phaseIndex, defaultDuty, defaultDuty); // Set all phase duty to defaultDuty, no fault
        wait(upadteWaitTime);
        IfxIom_Driver_clearHistory(&board->driver.inverter.iom.driver);
        LB30_AppSelfTest_iomSetDuty(board, phaseIndex, defaultDuty, 0);
        wait(upadteWaitTime*2);
        IfxIom_Driver_getHistory(&board->driver.inverter.iom.driver, &a, &b, &c, &d);
        if ((a | b | c | d) !=ref)// Expected events: one per falling edge of each top and bottom signals
        {
        	success = FALSE;
//            Ifx_Console_printAlign("Error phase %d, flat low test"ENDL, phaseIndex);
        }

    }

    board->selftest.pwmEnabled = FALSE;
    wait(upadteWaitTime);

//    App_printStatus(success);
	return success;
}


boolean LB30_AppSelfTest_TLF35584(LB30 *board)
{
    boolean success = TRUE;
    uint8   wktimcfg0Org;
    uint8   wktimcfg0;
//    App_printStatusTitle("Checking TLF35584");

    /* Invalidate the cache
     * Backup the original WKTIMCFG0 value
     * Write WKTIMCFG0 register with a dummy value and read the value back
     * Original value is written after the test
     */

    IfxTlf35584_Driver_invalidateCache(&board->driver.tlf35584);

    /* Backup the register value */
    success &= IfxTlf35584_Driver_readRegister(&board->driver.tlf35584, (uint32)&MODULE_TLF35584.WKTIMCFG0, &wktimcfg0Org);

    /* Write a dummy value */
    wktimcfg0 = 0xDB;
    success  &= IfxTlf35584_Driver_writeRegister(&board->driver.tlf35584, (uint32)&MODULE_TLF35584.WKTIMCFG0, wktimcfg0);
    success  &= IfxTlf35584_Driver_writeSync(&board->driver.tlf35584);

    /* Read back the dummy value */
    IfxTlf35584_Driver_invalidateCache(&board->driver.tlf35584);
    success &= IfxTlf35584_Driver_readRegister(&board->driver.tlf35584, (uint32)&MODULE_TLF35584.WKTIMCFG0, &wktimcfg0);

    /* Write back the original value */
    success &= IfxTlf35584_Driver_writeRegister(&board->driver.tlf35584, (uint32)&MODULE_TLF35584.WKTIMCFG0, wktimcfg0Org);
    success &= IfxTlf35584_Driver_writeSync(&board->driver.tlf35584);

    if (!success)
    {
//        Ifx_Console_printAlign("Frame check: failed"ENDL);
    }
    else if (wktimcfg0 != 0xDB)
    {
//        Ifx_Console_printAlign("Register read / write: failed"ENDL);
        success = FALSE;
    }
    else
    {
//        Ifx_Console_printAlign("Register read / write: passed"ENDL);
    }

//    App_printStatus(success);
    return success;
}


boolean LB30_AppSelfTest_monitoring(LB30 *board)
{
    boolean success = TRUE;
//    App_printStatusTitle("Checking monitored signals");
    success &= App_checkInput(&board->analogInput.vAna50.input);
    success &= App_checkInput(&board->analogInput.vDig50.input);
    success &= App_checkInput(&board->analogInput.vRef50.input);
    success &= App_checkInput(&board->analogInput.tempBoard.input);
    success &= App_checkInput(&board->analogInput.kl30.input);

    success &= App_checkInput(&board->analogInput.igbtTemp[0].input);

    Lb_BoardType boardType = AppDbStdIf_getBoardVersion(board->driverBoardStdif)->boardType;
    if (
    		(boardType == Lb_BoardType_HybridKit_DriverBoardHp2)
    		|| (boardType == Lb_BoardType_HybridKit_DriverBoardHpDrive)
    		|| (boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
    		|| (boardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
    		)

    {
        success &= App_checkInput(&board->analogInput.igbtTemp[1].input);
        success &= App_checkInput(&board->analogInput.igbtTemp[2].input);
    }

//    App_printStatus(success);
    return success;
}


boolean LB30_AppSelfTest(LB30 *board)
{
    boolean success = TRUE;
//    Ifx_Console_printAlign("-------------------------------------------------------------------------------"ENDL);
//    Ifx_Console_printAlign("Logic board Self test started."ENDL);
//    Ifx_g_console.standardIo->txDisabled = board->configuration.verboseLevel <= IFX_VERBOSE_LEVEL_WARNING;

    success                             &= LB30_AppSelfTest_TLF35584(board);
    success                             &= LB30_AppSelfTest_monitoring(board);

    if (board->configuration.safety.iom.enabled)
    {
    	success &= LB30_AppSelfTest_iom(board);
    }

	success &= LB30_AppSelfTest_dsadcRdc(board);

    success &= AppDbStdIf_selfTest(board->driverBoardStdif);

//    App_printStatusTitle("Logic board Self test");
//    App_printStatus(success);

//    Ifx_g_console.standardIo->txDisabled = FALSE;
    /*FIXME Add additional safety and functionality tests */
    return success;
}
