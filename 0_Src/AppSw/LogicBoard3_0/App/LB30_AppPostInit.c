/**
 * \file LB30_AppPostInit.c
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */
#include "LogicBoard3_0/App/LB30_AppPostInit.h"

#include "Configuration.h"
#include "LogicBoard3_0/App/LB30.h"
#include "LogicBoard3_0/Config/LB30_Interrupts.h"
#include "SysSe/Comm/Ifx_Console.h"
#include "Common/Main.h"

boolean LB30_AppInit_Ad2s1210(LB30 *board)
{
    boolean         result = TRUE;

    boolean         status;
    Ad2s1210_Config config;


//    App_printStatusTitle("Initializing Resolver 0 on AD2S1210");

    boolean                         interruptState = areInterruptsEnabled();

    // because of SPI communication, interrupt is needed

    IfxQspi_SpiMaster_ChannelConfig spiChannelConfig;
    IfxQspi_SpiMaster_initChannelConfig(&spiChannelConfig, &board->driver.qspi5);
    spiChannelConfig.base.baudrate             = 20e6;
    spiChannelConfig.base.mode.enabled         = TRUE;
    spiChannelConfig.base.mode.autoCS          = 0;
    spiChannelConfig.base.mode.loopback        = FALSE;
    spiChannelConfig.base.mode.clockPolarity   = SpiIf_ClockPolarity_idleLow;
    spiChannelConfig.base.mode.shiftClock      = SpiIf_ShiftClock_shiftTransmitDataOnLeadingEdge;
    spiChannelConfig.base.mode.dataHeading     = SpiIf_DataHeading_msbFirst;
    spiChannelConfig.base.mode.dataWidth       = 8; /* Uses 8 bit config because of the configuration mode, could be optimized for data and config if two channel are used */
    spiChannelConfig.base.mode.csActiveLevel   = Ifx_ActiveState_low;
    spiChannelConfig.base.mode.csLeadDelay     = SpiIf_SlsoTiming_2;
    spiChannelConfig.base.mode.csTrailDelay    = SpiIf_SlsoTiming_2;
    spiChannelConfig.base.mode.csInactiveDelay = SpiIf_SlsoTiming_2;
    spiChannelConfig.base.mode.parityCheck     = FALSE;
    spiChannelConfig.base.mode.parityMode      = Ifx_ParityMode_even;
    spiChannelConfig.base.errorChecks.baudrate = FALSE;
    spiChannelConfig.base.errorChecks.phase    = FALSE;
    spiChannelConfig.base.errorChecks.receive  = FALSE;
    spiChannelConfig.base.errorChecks.transmit = FALSE;

    spiChannelConfig.sls.output.pin            = &IfxQspi5_SLSO3_P15_6_OUT;
    spiChannelConfig.sls.output.mode           = IfxPort_OutputMode_pushPull;
    spiChannelConfig.sls.output.driver         = IfxPort_PadDriver_cmosAutomotiveSpeed1;

    result                                    &= IfxQspi_SpiMaster_initChannel(&board->driver.sscAd2s1210, &spiChannelConfig) == SpiIf_Status_ok;
//    Ifx_Console_printAlign("- QSPI%d, CS%d, %9.0f baud"ENDL, IfxQspi_getIndex(board->driver.qspi5.qspi), spiChannelConfig.sls.output.pin->slsoNr, spiChannelConfig.base.baudrate);
    enableInterrupts();

    Ad2s1210_initConfig(&config);
#if BYPASSEEPROM
    board->configuration.positionSensors.ad2s1210.offset = 0;
    board->configuration.positionSensors.ad2s1210.periodPerRotation = 1;
    board->configuration.positionSensors.ad2s1210.reversed = 0;
    board->configuration.positionSensors.ad2s1210.carrierFrequency = 10000;
#endif
    config.offset               = board->configuration.positionSensors.ad2s1210.offset;
    config.periodPerRotation    = board->configuration.positionSensors.ad2s1210.periodPerRotation;
    config.reversed             = board->configuration.positionSensors.ad2s1210.reversed;
    config.frequency            = board->configuration.positionSensors.ad2s1210.carrierFrequency;
    config.updateFrequency      = 1.0 / IfxStdIf_Inverter_getPeriod(&board->stdIf.inverter); /* update is called at each PWM period */
    config.modes.selected       = Ad2s1210_Mode_serial;
    config.modes.common.A0      = (IfxPort_Pin) {&MODULE_P33, 6};
    config.modes.common.A1      = (IfxPort_Pin) {&MODULE_P13, 3};
    config.modes.common.RESET   = (IfxPort_Pin) {&MODULE_P13, 2};
    config.modes.common.RES0    = (IfxPort_Pin) {&MODULE_P34, 1};
    config.modes.common.RES1    = (IfxPort_Pin) {&MODULE_P33, 7};
    config.modes.parallel       = NULL_PTR;
    config.modes.extbus         = NULL_PTR;
    config.modes.serial.channel = &board->driver.sscAd2s1210.base;

    if (board->boardVersion->boardVersion == LB_BoardVersion_HybridKit_LogicBoard_3_0)
    {
        config.modes.serial.WRn = (IfxPort_Pin) {NULL_PTR, 0};
    }
    else
    {
        config.modes.serial.WRn = (IfxPort_Pin) {&MODULE_P34, 4};
    }

    config.modes.serial.CSn          = IfxQspi5_SLSO3_P15_6_OUT.pin;
    config.resolution           = Ad2s1210_getResolutionCode(board->configuration.positionSensors.ad2s1210.resolution);

    config.speedFilerCutOffFrequency = 1000;	/*FIXME set this value depending on motor max speed */
    config.speedUpdateFactor         = config.updateFrequency / 1000.0; /* Speed is updated in the ms interrupt */

    status                           = Ad2s1210_init(&board->driver.ad2s1210, &config);
    result                          &= status;
    result                          &= Ad2s1210_stdIfPosInit(&board->stdIf.ad2s1210, &board->driver.ad2s1210);
    IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, status != FALSE);

    restoreInterrupts(interruptState);

    Ad2s1210_enable(&board->driver.ad2s1210, &waitTime);


//    Ifx_Console_printAlign("- Frequency=%9.0f Hz"ENDL, config.frequency);
//    Ifx_Console_printAlign("- Resolution=%d"ENDL, Ad2s1210_getResolution(&board->driver.ad2s1210));
//    Ifx_Console_printAlign("- Period per rotation=%d"ENDL, config.periodPerRotation);
//    Ifx_Console_printAlign("- A0 P%d.%d"ENDL, IfxPort_getIndex(config.modes.common.A0.port), config.modes.common.A0.pinIndex);
//    Ifx_Console_printAlign("- A1 P%d.%d"ENDL, IfxPort_getIndex(config.modes.common.A1.port), config.modes.common.A1.pinIndex);
//    Ifx_Console_printAlign("- RESET P%d.%d"ENDL, IfxPort_getIndex(config.modes.common.RESET.port), config.modes.common.RESET.pinIndex);
//    Ifx_Console_printAlign("- RES0 P%d.%d"ENDL, IfxPort_getIndex(config.modes.common.RES0.port), config.modes.common.RES0.pinIndex);
//    Ifx_Console_printAlign("- RES1 P%d.%d"ENDL, IfxPort_getIndex(config.modes.common.RES1.port), config.modes.common.RES1.pinIndex);

//    App_printStatus(result);
    return result;
}

boolean LB30_AppInit_Tle5012(LB30 *board)
{
    boolean         result = TRUE;


//    App_printStatusTitle("Initializing TLE5012");
    /* Sync the encoder and iGMR offsets */
    IfxStdIf_Pos_setOffset(&board->stdIf.encoder, 0);

	/* Update iGMR over SPI at low rate, not yet updated by the 1ms interrupt  */
    IfxStdIf_Pos_update(&board->stdIf.tle5012);

    IfxStdIf_Pos_setOffset(&board->stdIf.encoder,
    		(IfxStdIf_Pos_getRawPosition(&board->stdIf.tle5012)*IfxStdIf_Pos_getResolution(&board->stdIf.encoder))/IfxStdIf_Pos_getResolution(&board->stdIf.tle5012)
    		- IfxStdIf_Pos_getRawPosition(&board->stdIf.encoder)
    		);

    IfxStdIf_Pos_resetFaults(&board->stdIf.encoder); /* Clear the synchronization flag */

//    App_printStatus(result);
    return result;
}


boolean LB30_AppPostInit(LB30 *board)
{
    boolean result = TRUE;

    /* Console is not available before this point */
//    Ifx_Console_printAlign("*****************************"ENDL);
//    Ifx_Console_printAlign("* Board post initialization *"ENDL);
//    Ifx_Console_printAlign("*****************************"ENDL);

//    Ifx_g_console.standardIo->txDisabled = board->configuration.verboseLevel <= IFX_VERBOSE_LEVEL_WARNING;
#if  BYPASSDRVBD

#else if
    result                              &= AppDbStdIf_postInitialization(board->driverBoardStdif);
#endif
    if (board->configuration.inverter[0].positionSensor[0] == ECU_PositionSensor_ad2s1210)
    {
        result &= LB30_AppInit_Ad2s1210(board);
    }

    if(g_Lb.stdIf.tle5012->driver)
    {
        result &= LB30_AppInit_Tle5012(board);
    }


//    App_printStatusTitle("Board post initialization");
//    App_printStatus(result);
//    Ifx_g_console.standardIo->txDisabled = FALSE;
    return result;
}
