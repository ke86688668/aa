/**
 * \file LB30_Interrupts.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup LB30_AppIsr_interrupts Interrupts
 * \ingroup LB30
 *
 * \defgroup LB30_AppIsr_interruptsPrio Interrupts priorities
 * \ingroup LB30_AppIsr_interrupts
 */

#ifndef LB30_INTERRUPTS_H
#define LB30_INTERRUPTS_H

#include "Cfg_Interrupts.h"

/**
 * \addtogroup LB30_AppIsr_interruptsPrio
 * \{ */

#define ISR_PRIORITY_BASE             (100)                    /**< \brief Interrupt base priority */

/**
 * \name Interrupt priority configuration.
 * The interrupt priority range is [1,255]
 * \{
 */
#define ISR_PRIORITY_ASC0_RX          (ISR_PRIORITY_BASE + 4)  /**< \brief Define the ASC0 receive interrupt priority.  */
#define ISR_PRIORITY_ASC0_TX          (ISR_PRIORITY_BASE + 8)  /**< \brief Define the ASC0 transmit interrupt priority.  */
#define ISR_PRIORITY_ASC0_EX          (ISR_PRIORITY_BASE + 12) /**< \brief Define the ASC0 error interrupt priority.  */

#define ISR_PRIORITY_ASC1_RX          (ISR_PRIORITY_BASE + 13) /**< \brief Define the ASC1 receive interrupt priority.  */
#define ISR_PRIORITY_ASC1_TX          (ISR_PRIORITY_BASE + 14) /**< \brief Define the ASC1 transmit interrupt priority.  */
#define ISR_PRIORITY_ASC1_EX          (ISR_PRIORITY_BASE + 15) /**< \brief Define the ASC1 error interrupt priority.  */

#define ISR_PRIORITY_CAN0             (ISR_PRIORITY_BASE + 16) /**< \brief Define the CAN 0 interrupt priority */

#define ISR_PRIORITY_CAN1             (ISR_PRIORITY_BASE + 17) /**< \brief Define the CAN 1 interrupt priority */

#define ISR_PRIORITY_TIMER_10MS       (ISR_PRIORITY_BASE + 20) /**< \brief Define the 10ms timer interrupt priority.  */
#define ISR_PRIORITY_TIMER_1MS        (ISR_PRIORITY_BASE + 24) /**< \brief Define the 1ms timer interrupt priority.  */

#define ISR_PRIORITY_IGBT_TEMPU       (ISR_PRIORITY_BASE + 25) /**< \brief Define the IGBT temp U interrupt priority.  */
#define ISR_PRIORITY_IGBT_TEMPV       (ISR_PRIORITY_BASE + 26) /**< \brief Define the IGBT temp V interrupt priority.  */
#define ISR_PRIORITY_IGBT_TEMPW       (ISR_PRIORITY_BASE + 27) /**< \brief Define the IGBT temp W interrupt priority.  */

#define ISR_PRIORITY_ADC_M0           (ISR_PRIORITY_BASE + 28) /**< \brief Define the ADC M0 group interrupt priority.  */
#define ISR_PRIORITY_M0_PWMHL_PERIOD  (ISR_PRIORITY_BASE + 32) /**< \brief Define the PWM M0 period interrupt priority.  */
#define ISR_PRIORITY_M0_PWMHL_TRIGGER (ISR_PRIORITY_BASE + 36) /**< \brief Define the PWM M0 trigger interrupt priority.  */

#define ISR_PRIORITY_DSADC_RDC0       (ISR_PRIORITY_BASE + 38) /**< \brief Define the resolver (RDC) interrupt priority. */

#define ISR_PRIORITY_ENCODER_GPT12    (ISR_PRIORITY_BASE + 40) /**< \brief Define the encoder with GPT12 interrupt priority.  */

#define ISR_PRIORITY_QSPI2_TX         (ISR_PRIORITY_BASE + 56) /**< \brief Define the QSPI2 transmit interrupt priority.  */
#define ISR_PRIORITY_QSPI2_RX         (ISR_PRIORITY_BASE + 60) /**< \brief Define the QSPI2 receive interrupt priority.  */
#define ISR_PRIORITY_QSPI2_ERR        (ISR_PRIORITY_BASE + 64) /**< \brief Define the QSPI2 error interrupt priority.  */

#define ISR_PRIORITY_QSPI5_TX         (ISR_PRIORITY_BASE + 68) /**< \brief Define the QSPI5 transmit interrupt priority.  */
#define ISR_PRIORITY_QSPI5_RX         (ISR_PRIORITY_BASE + 72) /**< \brief Define the QSPI5 receive interrupt priority.  */
#define ISR_PRIORITY_QSPI5_ERR        (ISR_PRIORITY_BASE + 76) /**< \brief Define the QSPI5 error interrupt priority.  */

/** \} */

/**
 * \name Interrupt service provider configuration.
 * \{ */
#define ISR_PROVIDER_ASC0             IfxSrc_Tos_cpu0 /**< \brief Define the ASC0 interrupt provider.  */

#define ISR_PROVIDER_ASC1             IfxSrc_Tos_cpu0 /**< \brief Define the ASC0 interrupt provider.  */

#define ISR_PROVIDER_CAN0             IfxSrc_Tos_cpu0 /**< \brief Define the CAN 0 interrupt provider */
#define ISR_PROVIDER_CAN1             IfxSrc_Tos_cpu0 /**< \brief Define the CAN 1 interrupt provider */



#define ISR_PROVIDER_CAN1             IfxSrc_Tos_cpu0 /**< \brief Define the CAN 1 interrupt provider */

#define ISR_PROVIDER_TIMER_10MS       IfxSrc_Tos_cpu0 /**< \brief Define the 10ms timer interrupt provider.  */
#define ISR_PROVIDER_TIMER_1MS        IfxSrc_Tos_cpu0 /**< \brief Define the 1ms timer interrupt provider.  */

#define ISR_PROVIDER_IGBT_TEMP        IfxSrc_Tos_cpu1 /**< \brief Define the IGBT temp interrupt provider.  */

#define ISR_PROVIDER_ADC_M0           IfxSrc_Tos_cpu0 /**< \brief Define the ADC M0 group interrupt provider.  */
#define ISR_PROVIDER_M0_PWMHL         IfxSrc_Tos_cpu0 /**< \brief Define the PWM M0 interrupt provider.  */

#define ISR_PROVIDER_DSADC_RDC0       IfxSrc_Tos_cpu0 /**< \brief Define the resolver (RDC) interrupt provider.  */

#define ISR_PROVIDER_ENCODER_GPT12    IfxSrc_Tos_cpu0 /**< \brief Define the encoder with GPT12 interrupt provider.  */


#define ISR_PROVIDER_QSPI2            IfxSrc_Tos_cpu0 /**< \brief Define the QSPI2 interrupt provider.  */
#define ISR_PROVIDER_QSPI5            IfxSrc_Tos_cpu0 /**< \brief Define the QSPI5 interrupt provider.  */

/** \} */

/**
 * \name Interrupt configuration.
 * \{ */
#define INTERRUPT_ASC0_RX             ISR_ASSIGN(ISR_PRIORITY_ASC0_RX, ISR_PROVIDER_ASC0)                /**< \brief Define the ASC0 receive interrupt priority.  */
#define INTERRUPT_ASC0_TX             ISR_ASSIGN(ISR_PRIORITY_ASC0_TX, ISR_PROVIDER_ASC0)                /**< \brief Define the ASC0 transmit interrupt priority.  */
#define INTERRUPT_ASC0_EX             ISR_ASSIGN(ISR_PRIORITY_ASC0_EX, ISR_PROVIDER_ASC0)                /**< \brief Define the ASC0 error interrupt priority.  */

#define INTERRUPT_ASC1_RX             ISR_ASSIGN(ISR_PRIORITY_ASC1_RX, ISR_PROVIDER_ASC1)                /**< \brief Define the ASC1 receive interrupt priority.  */
#define INTERRUPT_ASC1_TX             ISR_ASSIGN(ISR_PRIORITY_ASC1_TX, ISR_PROVIDER_ASC1)                /**< \brief Define the ASC1 transmit interrupt priority.  */
#define INTERRUPT_ASC1_EX             ISR_ASSIGN(ISR_PRIORITY_ASC1_EX, ISR_PROVIDER_ASC1)                /**< \brief Define the ASC1 error interrupt priority.  */

#define INTERRUPT_CAN0                ISR_ASSIGN(ISR_PRIORITY_CAN0, ISR_PROVIDER_CAN0)                   /**< \brief Define the CAN 0 interrupt priority */

#define INTERRUPT_CAN1                ISR_ASSIGN(ISR_PRIORITY_CAN1, ISR_PROVIDER_CAN1)                   /**< \brief Define the CAN 1 interrupt priority */

#define INTERRUPT_TIMER_10MS          ISR_ASSIGN(ISR_PRIORITY_TIMER_10MS, ISR_PROVIDER_TIMER_10MS)       /**< \brief Define the 10ms timer interrupt priority.  */
#define INTERRUPT_TIMER_1MS           ISR_ASSIGN(ISR_PRIORITY_TIMER_1MS, ISR_PROVIDER_TIMER_1MS)         /**< \brief Define the 1ms timer interrupt priority.  */

#define INTERRUPT_IGBT_TEMPU          ISR_ASSIGN(ISR_PRIORITY_IGBT_TEMPU, ISR_PROVIDER_IGBT_TEMP)        /**< \brief Define the IGBT temp U interrupt priority.  */
#define INTERRUPT_IGBT_TEMPV          ISR_ASSIGN(ISR_PRIORITY_IGBT_TEMPV, ISR_PROVIDER_IGBT_TEMP)        /**< \brief Define the IGBT temp U interrupt priority.  */
#define INTERRUPT_IGBT_TEMPW          ISR_ASSIGN(ISR_PRIORITY_IGBT_TEMPW, ISR_PROVIDER_IGBT_TEMP)        /**< \brief Define the IGBT temp U interrupt priority.  */

#define INTERRUPT_ADC_M0              ISR_ASSIGN(ISR_PRIORITY_ADC_M0, ISR_PROVIDER_ADC_M0)               /**< \brief Define the ADC M0 group interrupt priority.  */

#define INTERRUPT_M0_PWMHL_PERIOD     ISR_ASSIGN(ISR_PRIORITY_M0_PWMHL_PERIOD, ISR_PROVIDER_M0_PWMHL)    /**< \brief Define the PWM M0 interrupt priority.  */
#define INTERRUPT_M0_PWMHL_TRIGGER    ISR_ASSIGN(ISR_PRIORITY_M0_PWMHL_TRIGGER, ISR_PROVIDER_M0_PWMHL)   /**< \brief Define the PWM M0 interrupt priority.  */

#define INTERRUPT_ENCODER_GPT12       ISR_ASSIGN(ISR_PRIORITY_ENCODER_GPT12, ISR_PROVIDER_ENCODER_GPT12) /**< \brief Define the encoder with GPT12 interrupt priority.  */


#define INTERRUPT_QSPI2_TX            ISR_ASSIGN(ISR_PRIORITY_QSPI2_TX, ISR_PROVIDER_QSPI2)              /**< \brief Define the QSPI2 transmit interrupt priority.  */
#define INTERRUPT_QSPI2_RX            ISR_ASSIGN(ISR_PRIORITY_QSPI2_RX, ISR_PROVIDER_QSPI2)              /**< \brief Define the QSPI2 receive interrupt priority.  */
#define INTERRUPT_QSPI2_ERR           ISR_ASSIGN(ISR_PRIORITY_QSPI2_ERR, ISR_PROVIDER_QSPI2)             /**< \brief Define the QSPI2 error interrupt priority.  */

#define INTERRUPT_QSPI5_TX            ISR_ASSIGN(ISR_PRIORITY_QSPI5_TX, ISR_PROVIDER_QSPI5)              /**< \brief Define the QSPI3 transmit interrupt priority.  */
#define INTERRUPT_QSPI5_RX            ISR_ASSIGN(ISR_PRIORITY_QSPI5_RX, ISR_PROVIDER_QSPI5)              /**< \brief Define the QSPI3 receive interrupt priority.  */
#define INTERRUPT_QSPI5_ERR           ISR_ASSIGN(ISR_PRIORITY_QSPI5_ERR, ISR_PROVIDER_QSPI5)             /**< \brief Define the QSPI3 error interrupt priority.  */

/** \} */
/** \} */


#define ISR_PRIORITY_CAN0_NODE0_TX 	0x3D
#define ISR_PRIORITY_CAN0_NODE0_RX 	0x30
#define ISR_PRIORITY_CAN0_NODE3_TX 	0x3E
#define ISR_PRIORITY_CAN0_NODE3_RX 	0x31
#define ISR_PRIORITY_CAN1_NODE1_TX 	0x3F
#define ISR_PRIORITY_CAN1_NODE1_RX 	0x32
#define ISR_PRIORITY_CAN1_NODE2_TX 	0x40
#define ISR_PRIORITY_CAN1_NODE2_RX 	0x33
#define ISR_PRIORITY_CAN2_NODE0_TX 	0x41
#define ISR_PRIORITY_CAN2_NODE0_RX 	0x34

#endif /* LB30_INTERRUPTS_H */
