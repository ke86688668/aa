/**
 * \file AppTest.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */
#ifndef APPTEST_H
#define APPTEST_H

#include "AppInterface.h"
/** Test data / for source code development testing only
 *
 */
typedef struct
{
    float32 pwmDuty[ECU_PHASE_PER_MOTOR_COUNT];
    struct
    {
        uint32 slotOneMs;
        uint32 slotTenMs;
        uint32 slotPwmPeriodStart;
        uint32 slotTriggerPoint;
        uint32 slotEndOfPhaseCurrentConversion;
        uint32 canNode0;
        uint32 canGroup0;
    }     isrCounter;
    uint8 selectedMotor;
    struct
    {
        float32            currents[ECU_PHASE_PER_MOTOR_COUNT];
        ECU_InverterStatus status;
        float32            vDc;
        float32            position;
        float32            speed;
        float32            in[4];
        float32            tempBoard;
        float32            vAna50;
        float32            vRef50;
        float32            vDig50;
        float32            kl30;
    }AppState;
} AppTest;
IFX_EXTERN AppTest g_testData;

#endif /* APPTEST_H */
