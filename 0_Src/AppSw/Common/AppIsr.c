/**
 * \file AppIsr.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 */
#include "Cfg_Interrupts.h"
#include "LB.h"
#include "Library/AppLibrary.h"
#include "SysSe/EMotor/Ifx_MotorModelPmsmF32.h"
#include "SysSe/EMotor/Ifx_SvmF32.h"
#include "SysSe/EMotor/Ifx_ClarkeF32.h"
#include "SysSe/EMotor/Ifx_ParkF32.h"
#include "SysSe/Math/Ifx_PicF32.h"
#include "SysSe/Math/Ifx_LutLSincosF32.h"
#include "IfxCpu_Trap.h"
#include "Smu/Std/IfxSmu.h"

/** \addtogroup LB_AppIsr_interrupts
 * \{ */

/** \name CPU0 interrupts
 * \{ */
IFX_INTERRUPT(ISR_CPU0_src, 0, ISR_PRIORITY_CPU0);
IFX_INTERRUPT(ISR_CPU1_src, 0, ISR_PRIORITY_CPU1);
IFX_INTERRUPT(ISR_CPU2_src, 0, ISR_PRIORITY_CPU2);
/** \} */

/** \name SSC interrupts
 * \{ */
IFX_INTERRUPT(AppIsr_Qspi0_tx, 0, ISR_PRIORITY_QSPI0_TX);
IFX_INTERRUPT(AppIsr_Qspi0_rx, 0, ISR_PRIORITY_QSPI0_RX);
IFX_INTERRUPT(AppIsr_Qspi0_err, 0, ISR_PRIORITY_QSPI0_ERR);
/** \} */

/** \} */

//________________________________________________________________________________________
// FUNCTION IMPLEMENTATIONS


/** \brief Handle the Ssc transmit interrupt
 *
 * \isrProvider{ISR_PROVIDER_QSPI0}
 * \isrPriority{ISR_PRIORITY_QSPI0_TX}
 *
 * This interrupt is raised by the Ssc on each transmit event.
 */
void AppIsr_Qspi0_tx(void)
{
    __enable();
    /* FIXME implement StdIf */
    IfxQspi_SpiMaster_isrTransmit(&g_Lb.driver.qspi0);
}


/** \brief Handle the Ssc receive interrupt
 *
 * \isrProvider{ISR_PROVIDER_QSPI0}
 * \isrPriority{ISR_PRIORITY_QSPI0_RX}
 *
 * This interrupt is raised by the Ssc on each transmit event.
 */
void AppIsr_Qspi0_rx(void)
{
    __enable();
    IfxQspi_SpiMaster_isrReceive(&g_Lb.driver.qspi0);
}


/** \brief Handle the Ssc error interrupt
 *
 * \isrProvider{ISR_PROVIDER_QSPI0}
 * \isrPriority{ISR_PRIORITY_QSPI0_ERR}
 *
 * This interrupt is raised by the Ssc on each transmit event.
 */
void AppIsr_Qspi0_err(void)
{
    __enable();
    IfxQspi_SpiMaster_isrError(&g_Lb.driver.qspi0);
}


void scalePerf(IfxCpu_Perf *perf, uint32 loopCount)
{
    perf->clock.counter       /= loopCount;
    perf->counter1.counter    /= loopCount;
    perf->counter2.counter    /= loopCount;
    perf->counter3.counter    /= loopCount;
    perf->instruction.counter /= loopCount;
}


#include "main.h"
IFX_INLINE void benchmark(IfxCpu_ResourceCpu index)
{
#if CFG_SETUP == CFG_SETUP_LIBRARY
    uint32 i;
    uint32 loopCounter;
    uint32 maxLoop = 1000;
    Cpu   *cpu;
    cpu = g_Cpus[index];

    {
        IfxCpu_resetAndStartCounters(IfxCpu_CounterMode_normal);

        for (loopCounter = 0; loopCounter < maxLoop; loopCounter++)
        {
            __nop();
        }

        cpu->benchmark.empty = IfxCpu_stopCounters();
        scalePerf(&cpu->benchmark.empty, maxLoop);
    }
    {
        /*
         * Data:
         * - Ifx_g_LutSincosF32_table
         * Code:
         * - Ifx_LutSincosF32_cossin
         */
        volatile cfloat32         m;
        volatile Ifx_Lut_FxpAngle angle = IFX_LUT_ANGLE_RESOLUTION / 10;

        IfxCpu_resetAndStartCounters(IfxCpu_CounterMode_normal);

        for (loopCounter = 0; loopCounter < maxLoop; loopCounter++)
        {
            m = Ifx_LutLSincosF32_cossin(angle);
            // __nop();
        }
        (void)m;

        cpu->benchmark.cosSinLookup = IfxCpu_stopCounters();
        scalePerf(&cpu->benchmark.cosSinLookup, maxLoop);
    }
    {
        /*
         * Data:
         * - None
         * Code:
         * - Ifx_ClarkeF32
         */
        volatile float32  currents[3];
        currents[0] = 0.1;
        currents[1] = 0.2;
        currents[2] = 0.3;

        IfxCpu_resetAndStartCounters(IfxCpu_CounterMode_normal);

        for (loopCounter = 0; loopCounter < maxLoop; loopCounter++)
        {
            Ifx_ClarkeF32(currents);
        }

        cpu->benchmark.clarke = IfxCpu_stopCounters();
        scalePerf(&cpu->benchmark.clarke, maxLoop);
    }

    {
        /*
         * Data:
         * - None
         * Code:
         * - Ifx_ParkF32_forward
         */
        volatile cfloat32 m;
        volatile cfloat32 m1;
        m.real  = 1.0;
        m.imag  = 2.0;
        m1.real = 3.0;
        m1.imag = 4.0;

        IfxCpu_resetAndStartCounters(IfxCpu_CounterMode_normal);

        for (loopCounter = 0; loopCounter < maxLoop; loopCounter++)
        {
            m = Ifx_ParkF32_forward(&m, &m1);
        }

        cpu->benchmark.park = IfxCpu_stopCounters();
        scalePerf(&cpu->benchmark.park, maxLoop);
    }

    {
        /*
         * Data:
         * - None
         * Code:
         * - Ifx_ParkF32_reverse
         */
        volatile cfloat32 m;
        volatile cfloat32 m1;
        m.real  = 1.0;
        m.imag  = 2.0;
        m1.real = 3.0;
        m1.imag = 4.0;

        IfxCpu_resetAndStartCounters(IfxCpu_CounterMode_normal);

        for (loopCounter = 0; loopCounter < maxLoop; loopCounter++)
        {
            m = Ifx_ParkF32_reverse(&m, &m1);
        }

        cpu->benchmark.reversePark = IfxCpu_stopCounters();
        scalePerf(&cpu->benchmark.reversePark, maxLoop);
    }

    {
        /*
         * Data:
         * - None
         * Code:
         * - Ifx_PicF32_step
         */
        volatile float32     i = 1.0;
        volatile Ifx_PicF32 pic;

        Ifx_PicF32_init(&pic);

        IfxCpu_resetAndStartCounters(IfxCpu_CounterMode_normal);

        for (loopCounter = 0; loopCounter < maxLoop; loopCounter++)
        {
            Ifx_PicF32_step(&pic, i);
        }

        cpu->benchmark.piController = IfxCpu_stopCounters();
        scalePerf(&cpu->benchmark.piController, maxLoop);
    }

    {
        /*
         * Data:
         * - Ifx_g_SvmF32_SinCosVector
         * Code:
         * - Ifx_SvmF32_do
         */
        cfloat32       m[6];
        Ifx_TimerValue period      = 1000;
        Ifx_TimerValue channels[3] = {0.1, 0.2, 0.3};

        for (i = 0; i < 6; i++)
        {
            m[i] = Ifx_LutLSincosF32_cossin(IFX_LUT_ANGLE_RESOLUTION / 6 * i + IFX_LUT_ANGLE_RESOLUTION / 12);
        }

        IfxCpu_resetAndStartCounters(IfxCpu_CounterMode_normal);

        for (loopCounter = 0; loopCounter < maxLoop; loopCounter++)
        {
            Ifx_SvmF32_do(m[0], period, channels);
            Ifx_SvmF32_do(m[1], period, channels);
            Ifx_SvmF32_do(m[2], period, channels);
            Ifx_SvmF32_do(m[3], period, channels);
            Ifx_SvmF32_do(m[4], period, channels);
            Ifx_SvmF32_do(m[5], period, channels);
        }

        cpu->benchmark.svm = IfxCpu_stopCounters();
        scalePerf(&cpu->benchmark.svm, maxLoop * 6);
    }

    {
        /*
         * Data:
         * - None
         * Code:
         * - Ifx_MotorModelPmsmF32_currentStep
         * - Ifx_FocF32_stepIn
         * - Ifx_FocF32_stepOut
         */

        Ifx_F32_MotorModel_input  input;
        Ifx_F32_MotorModel_output output;
        Ifx_MotorModelPmsmF32   model = g_appLibrary.motorModel.pmsm;

        input.state.currents[0]        = 1;
        input.state.currents[1]        = 1;
        input.state.currents[2]        = 1;
        input.state.mechanicalSpeed    = 1;
        input.state.rawMechanicalAngle = 1;
        input.state.vdc                = 10;

        IfxCpu_resetAndStartCounters(IfxCpu_CounterMode_normal);

        for (loopCounter = 0; loopCounter < maxLoop; loopCounter++)
        {
            Ifx_MotorModelPmsmF32_currentStep(&model, &input, &output);
        }

        cpu->benchmark.foc = IfxCpu_stopCounters();
        scalePerf(&cpu->benchmark.foc, maxLoop);
    }

    {
        /*
         * Data:
         * - None
         * Code:
         * - IfxDsadc_Rdc_update
         * - IfxDsadc_Rdc_onEventA
         * - Ifx_AngleTrkF32_step
         * - Ifx_AngleTrkF32_updateStatus
         */

        IfxDsadc_Rdc rdc;

        if (Lb_isVersion3x(&g_Lb.boardVersion))
        {
            rdc = g_Lb.logicboards.logicBoard_30.driver.dsadcRdc[0];
        }

        IfxCpu_resetAndStartCounters(IfxCpu_CounterMode_normal);

        for (loopCounter = 0; loopCounter < maxLoop; loopCounter++)
        {
            IfxDsadc_Rdc_update(&rdc);
            IfxDsadc_Rdc_onEventA(&rdc);
        }

        cpu->benchmark.rdc = IfxCpu_stopCounters();
        scalePerf(&cpu->benchmark.rdc, maxLoop);
    }
    {
        /*
         * Data:
         * - None
         * Code:
         * - IfxGtm_Atom_Timer_disableUpdate
         * - IfxGtm_Atom_Timer_applyUpdate
         * - IfxGtm_Atom_PwmHl_setMode
         * - IfxGtm_Atom_PwmHl_setOnTime
         * - IfxGtm_Atom_PwmHl_updateCenterAligned
         * - IfxGtm_Atom_Ch_setCompareShadow
         * - IfxGtm_Atom_Agc_enableChannelsUpdate
         *
         */

        IfxGtm_Atom_PwmHl pwm;
        IfxGtm_Atom_Timer timer;
        Ifx_TimerValue    duty[3] = {500, 500, 500};

        if (Lb_isVersion3x(&g_Lb.boardVersion))
        {
            pwm   = g_Lb.logicboards.logicBoard_30.driver.inverter.atom.pwmDriver;
            timer = g_Lb.logicboards.logicBoard_30.driver.inverter.atom.timerDriver;
        }

        pwm.timer = &timer;

        IfxGtm_Atom_PwmHl_setMode(&pwm, Ifx_Pwm_Mode_off);
        IfxGtm_Atom_PwmHl_setMode(&pwm, Ifx_Pwm_Mode_centerAligned);

        IfxCpu_resetAndStartCounters(IfxCpu_CounterMode_normal);

        for (loopCounter = 0; loopCounter < maxLoop; loopCounter++)
        {
            IfxGtm_Atom_Timer_disableUpdate(&timer);
            IfxGtm_Atom_PwmHl_setOnTime(&pwm, &duty[0]);
            IfxGtm_Atom_Timer_applyUpdate(&timer);
        }

        cpu->benchmark.gtmpwm = IfxCpu_stopCounters();
        scalePerf(&cpu->benchmark.gtmpwm, maxLoop);
    }
#endif
}


/** \brief Handle the CPU0 interrupt  interrupt
 *
 * \isrProvider \ref ISR_PROVIDER_CPU0
 * \isrPriority \ref ISR_PRIORITY_CPU0
 */
void ISR_CPU0_src(void)
{
    benchmark(IfxCpu_ResourceCpu_0);
    g_Cpu0.benchmark.ready = TRUE;
}


/** \brief Handle the CPU1 interrupt  interrupt
 *
 * \isrProvider \ref ISR_PROVIDER_CPU1
 * \isrPriority \ref ISR_PRIORITY_CPU1
 */
void ISR_CPU1_src(void)
{
    benchmark(IfxCpu_ResourceCpu_1);
    g_Cpu1.benchmark.ready = TRUE;
}


/** \brief Handle the CPU2 interrupt  interrupt
 *
 * \isrProvider \ref ISR_PROVIDER_CPU2
 * \isrPriority \ref ISR_PRIORITY_CPU2
 */
void ISR_CPU2_src(void)
{
    benchmark(IfxCpu_ResourceCpu_2);
    g_Cpu2.benchmark.ready = TRUE;
}

volatile uint32 g_trapNmiTlf35584 = 0;
volatile uint32 g_trapNmiIom = 0;

void TrapNmi(IfxCpu_Trap *trapWatch)
{

	Ifx_SCU_TRAPCLR clearCmd;
	clearCmd.U = 0;
	//TRAP ISR: ESR1 is used by TLF35584 on the LB3.x boards
	// Trap is handled by CPU1
	if (trapWatch->tCpu == IfxCpu_ResourceCpu_0)
	{
		if (MODULE_SCU.TRAPSTAT.B.SMUT)
		{
			// An interrupt has been generated by the SMU
			// Nothing is done here in response to the fault
			{
				/* Handles SMU alarms */

//				20191212 Jimmy Should be correct
				IfxSmu_Alarm alarm;
//				alarm = IfxSmu_getAlarm(&MODULE_SMU);

//				while (alarm != IfxSmu_Alarm_noAlarm)
//				{
//					switch (alarm)
//					{
//					case IfxSmu_Alarm_IomPinMismatchIndication:
//						g_trapNmiIom++;
//						AppLbStdIf_raiseIomFault(&g_Lb.stdIf);
//						break;
//					default:
						/* not handled in HK demo */
//						break;
//					}
//					IfxSmu_enableClearAlarmStatus(&MODULE_SMU);
//					IfxSmu_clearAlarm(&MODULE_SMU, alarm);

//					alarm = IfxSmu_getAlarm(&MODULE_SMU);
//				}
				clearCmd.B.SMUT = 1; // clear trap flag at the end of the ISR
			}
		}

		if (MODULE_SCU.TRAPSTAT.B.ESR1T)
		{
			g_trapNmiTlf35584++;
			// An interrupt has been generated by the TLF35584
			// Nothing is done here in response to the fault
			// Simply wait for the TLF35584 to reset the board
			clearCmd.B.ESR1T = 1; // clear trap flag at the end of the ISR
		}

	}

	MODULE_SCU.TRAPCLR.U = clearCmd.U; // clear trap flag at the end of the ISR
}

#define logicBoard g_Lb
