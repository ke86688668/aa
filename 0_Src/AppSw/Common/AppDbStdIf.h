/**
 * \file AppDbStdIf.h
 * \brief Standard interface: Driver board
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 * \defgroup AppDbStdIf Driver board standard interface
 * \ingroup LB
 *
 */
#ifndef APPDBSTDIF_H
#define APPDBSTDIF_H 1

#include "StdIf/IfxStdIf.h"
#include "StdIf/IfxStdIf_DPipe.h"
#include "Qspi/SpiMaster/IfxQspi_SpiMaster.h"
#include "SysSe/Ext/At25xxx/Ifx_At25xxx.h"
#include "AppGlobalDef.h"
#include "SysSe/Comm/Ifx_Shell.h"

//----------------------------------------------------------------------------------------
#define CFG_DB_CONFIG_EFS_FILE_SIZE                        (512)
#define CFG_DB_BOARD_EFS_FILE_SIZE                         (256)

#define CFG_DB_WITH_EEPROM_BOARD_EFS_OFFSET          (0x6000)
#define CFG_DB_WITH_EEPROM_BOARD_EFS_PARTITION_SIZE  (0x2000)
#define CFG_DB_WITH_EEPROM_CONFIG_EFS_OFFSET         (0x0000)
#define CFG_DB_WITH_EEPROM_CONFIG_EFS_PARTITION_SIZE (0x4000)
//----------------------------------------------------------------------------------------
/** \brief Forward declaration */
typedef struct AppDbStdIf_ AppDbStdIf;

typedef struct
{
     float32            frequency; /**< \brief Define the PWM frequency in Hz. */
     float32            deadtime;  /**< \brief Define the dead time between the top and bottom PWM in s. */
     float32            minPulse;  /**< \brief Define the smallest PWM pulse in s. */
     float32            vdcNom;    /**< \brief Define the nominal DC link voltage in V */
     float32            vdcMaxGen; /**< \brief Maximum allowed DC-link voltage in V  FIXME to  be renamed */
     float32            vdcMin;    /**< \brief Maximum allowed DC-link voltage in V */
     float32            igbtTempMax;    /**< \brief Maximum allowed IGBT temperature in �C */
     float32            igbtTempMin;    /**< \brief Minimum allowed IGBT temperature in �C */
}AppDbStdIf_MainConfig;
/** Disable the driver
 *
 */
typedef void          (*AppDbStdIf_DisableDriver)(IfxStdIf_InterfaceDriver driver);

/** Dump driver info
 *
 */
typedef void          (*AppDbStdIf_DumpDriverInfo)(IfxStdIf_InterfaceDriver driver, IfxStdIf_DPipe *io, boolean diff);

/** Enable the driver
 *
 */
typedef void          (*AppDbStdIf_EnableDriver)(IfxStdIf_InterfaceDriver driver);

/** Return the board version
 *
 */
typedef LB_FileBoardVersion*       (*AppDbStdIf_GetBoardVersion)(IfxStdIf_InterfaceDriver driver);

/** Return a pointer to the main configuration parameters
 *
 */
typedef AppDbStdIf_MainConfig*       (*AppDbStdIf_GetMainConfig)(IfxStdIf_InterfaceDriver driver);

/** Return a pointer to the config EFS
 *
 */
typedef Ifx_Efs*      (*AppDbStdIf_GetConfigEfs)(IfxStdIf_InterfaceDriver driver);

/** Return a fault mask
 *  bit  0: phase1 Top Driver Error
 *  bit  1: phase1 Bottom Driver Error
 *  bit  2: phase2 Top Driver Error
 *  bit  3: phase2 Bottom Driver Error
 *  bit  4: phase3 Top Driver Error
 *  bit  5: phase3 Bottom Driver Error
 *  bit 15: Main error
 *
 */
typedef uint32      (*AppDbStdIf_GetFault)(IfxStdIf_InterfaceDriver driver);

/** Return the Vdc in Volt
 *
 */
typedef float32      (*AppDbStdIf_GetVdc)(IfxStdIf_InterfaceDriver driver);

/** Return the IGBT temperature in �C
 *
 * \param tempArray array of 3 temperature value (U, V, W)
 *
 */
typedef void      (*AppDbStdIf_GetIgbtTemp)(IfxStdIf_InterfaceDriver driver, float32 *tempArray);

/** Return a pointer to the version EFS
 *
 */
typedef Ifx_Efs*      (*AppDbStdIf_GetBoardVersionEfs)(IfxStdIf_InterfaceDriver driver);

/** Initialize the board (1st part)
 *
 * \param eeprom Optional, to be passed if the eeprom is not on driver board
 */
typedef boolean       (*AppDbStdIf_Init)(IfxStdIf_InterfaceDriver driver, IfxQspi_SpiMaster *qspi, Ifx_At25xxx *eeprom);

/** Init driver
 *
 */
typedef boolean      (*AppDbStdIf_InitDriver)(IfxStdIf_InterfaceDriver driver, IfxQspi_SpiMaster *qspi, LB_FileBoardVersion *logicBoardVersion);

/** Post initialisation (2n part)
 *
 */
typedef boolean      (*AppDbStdIf_PostInitialization)(IfxStdIf_InterfaceDriver driver);

/** Print board configuration
 *
 */
typedef void          (*AppDbStdIf_PrintBoardConfiguration)(IfxStdIf_InterfaceDriver driver, IfxStdIf_DPipe *io);

/** Save config to eeprom
 *
 */
typedef boolean      (*AppDbStdIf_SaveConfig)(IfxStdIf_InterfaceDriver driver);

/** Self test
 *
 */
typedef boolean      (*AppDbStdIf_SelfTest)(IfxStdIf_InterfaceDriver driver);

/** Set default config values
 *
 */
typedef void      (*AppDbStdIf_SetDefaultConfig)(IfxStdIf_InterfaceDriver driver);

/** Shell setup commands
 *
 */
typedef boolean      (*AppDbStdIf_ShellSetup)(IfxStdIf_InterfaceDriver driver, pchar args, void *data, IfxStdIf_DPipe *io);




/** Create the board version EFS, return TRUE in case of success
 *
 */
typedef boolean (*AppDbStdIf_CreateBoardVersionEfs )(IfxStdIf_InterfaceDriver driver, boolean quiet);
/** Create the board version file, return TRUE in case of success
 *
 */
typedef boolean (*AppDbStdIf_CreateBoardVersionFile)(IfxStdIf_InterfaceDriver driver, boolean quiet);
/** Create the configuration EFS, return TRUE in case of success
 *
 */
typedef boolean (*AppDbStdIf_CreateConfigEfs       )(IfxStdIf_InterfaceDriver driver, boolean quiet);
/** Create the configuration file, return TRUE in case of success
 *
 */
typedef boolean (*AppDbStdIf_CreateConfigFile      )(IfxStdIf_InterfaceDriver driver, boolean quiet);
/** Return TRUE if the board version file exist
 *
 */
typedef boolean (*AppDbStdIf_IsBoardVersionFile    )(IfxStdIf_InterfaceDriver driver);
/** Return TRUE if the configuration file exist
 *
 */
typedef boolean (*AppDbStdIf_IsConfigFile          )(IfxStdIf_InterfaceDriver driver);
/** Load the board version file, return TRUE in case of success
 *
 */
typedef boolean (*AppDbStdIf_LoadBoardVersionFile  )(IfxStdIf_InterfaceDriver driver);
/** Load the configuration file, return TRUE in case of success
 *
 */
typedef boolean (*AppDbStdIf_LoadConfigFile        )(IfxStdIf_InterfaceDriver driver);

/** Save the board version file, return TRUE in case of success
 *
 */
typedef boolean (*AppDbStdIf_SaveBoardVersion        )(IfxStdIf_InterfaceDriver driver);

/** Return TRUE if the board type is supported
 *
 */
typedef boolean (*AppDbStdIf_IsBoardTypeSupported     )(IfxStdIf_InterfaceDriver driver);
/** Return TRUE if the board type / version combination is supported
 *
 */
typedef boolean (*AppDbStdIf_IsBoardVersionSupported  )(IfxStdIf_InterfaceDriver driver);
/** Return TRUE if the configuration file version is up to date
 *
 */
typedef boolean (*AppDbStdIf_IsConfigFileVersionActual)(IfxStdIf_InterfaceDriver driver);
/** Setup wizard
 *
 */
typedef char (*AppDbStdIf_SetupWizard)(IfxStdIf_InterfaceDriver driver, Ifx_Shell *shell);

/** Clear faults
 *
 * Clears the DESAT fault (sticky flag) only if it is the only sticky fault present
 *
 * Return TRUE if the DESAT flag is request to be cleared
 */
typedef boolean (*AppDbStdIf_ClearFaults)(IfxStdIf_InterfaceDriver driver);

/** \brief Standard interface object
 */
struct AppDbStdIf_
{
    IfxStdIf_InterfaceDriver driver;         /**< \brief Pointer to the specific driver object */

    /* Standard interface APIs */
    /* *INDENT-OFF* */
    AppDbStdIf_DisableDriver              disableDriver           ; /**< \brief \ref  AppDbStdIf_DisableDriver           */
    AppDbStdIf_DumpDriverInfo             dumpDriverInfo          ; /**< \brief \ref  AppDbStdIf_DumpDriverInfo          */
    AppDbStdIf_EnableDriver               enableDriver            ; /**< \brief \ref  AppDbStdIf_EnableDriver            */
    AppDbStdIf_GetBoardVersion            getBoardVersion         ; /**< \brief \ref  AppDbStdIf_GetBoardVersion         */
    AppDbStdIf_GetMainConfig              getmainConfig           ; /**< \brief \ref  AppDbStdIf_GetMainConfig         */
    AppDbStdIf_GetConfigEfs               getConfigEfs            ; /**< \brief \ref  AppDbStdIf_GetConfigEfs            */
    AppDbStdIf_GetFault                   getFault                ; /**< \brief \ref  AppDbStdIf_GetFault                */
    AppDbStdIf_GetVdc					  getVdc                  ; /**< \brief \ref  AppDbStdIf_GetVdc                  */
    AppDbStdIf_GetIgbtTemp				  getIgbtTemp             ; /**< \brief \ref  AppDbStdIf_GetIgbtTemp                  */
    AppDbStdIf_GetBoardVersionEfs         getBoardVersionEfs      ; /**< \brief \ref  AppDbStdIf_GetBoardVersionEfs      */
    AppDbStdIf_Init                       init                    ; /**< \brief \ref  AppDbStdIf_Init                    */
    AppDbStdIf_InitDriver                 initDriver              ; /**< \brief \ref  AppDbStdIf_InitDriver              */
    AppDbStdIf_PostInitialization         postInitialization      ; /**< \brief \ref  AppDbStdIf_PostInitialization      */
    AppDbStdIf_PrintBoardConfiguration    printBoardConfiguration ; /**< \brief \ref  AppDbStdIf_PrintBoardConfiguration */
    AppDbStdIf_SaveConfig                 saveConfig              ; /**< \brief \ref  AppDbStdIf_SaveConfig              */
    AppDbStdIf_SelfTest                   selfTest                ; /**< \brief \ref  AppDbStdIf_SelfTest                */
    AppDbStdIf_SetDefaultConfig           setDefaultConfig        ; /**< \brief \ref  AppDbStdIf_SetDefaultConfig        */
    AppDbStdIf_ShellSetup                 shellSetup              ; /**< \brief \ref  AppDbStdIf_ShellSetup              */


    AppDbStdIf_CreateBoardVersionEfs      createBoardVersionEfs   ; /**< \brief \ref AppDbStdIf_CreateBoardVersionEfs     */
    AppDbStdIf_CreateBoardVersionFile     createBoardVersionFile  ; /**< \brief \ref AppDbStdIf_CreateBoardVersionFile    */
    AppDbStdIf_CreateConfigEfs            createConfigEfs         ; /**< \brief \ref AppDbStdIf_CreateConfigEfs           */
    AppDbStdIf_CreateConfigFile           createConfigFile        ; /**< \brief \ref AppDbStdIf_CreateConfigFile          */
    AppDbStdIf_IsBoardVersionFile         isBoardVersionFile      ; /**< \brief \ref AppDbStdIf_IsBoardVersionFile        */
    AppDbStdIf_IsConfigFile               isConfigFile            ; /**< \brief \ref AppDbStdIf_IsConfigFile              */
    AppDbStdIf_LoadBoardVersionFile       loadBoardVersionFile    ; /**< \brief \ref AppDbStdIf_LoadBoardVersionFile      */
    AppDbStdIf_LoadConfigFile             loadConfigFile          ; /**< \brief \ref AppDbStdIf_LoadConfigFile            */
    AppDbStdIf_SaveBoardVersion           saveBoardVersion        ; /**< \brief \ref AppDbStdIf_SaveBoardVersion          */

    AppDbStdIf_IsBoardTypeSupported      isBoardTypeSupported     ; /**< \brief \ref AppDbStdIf_IsBoardTypeSupported      */
    AppDbStdIf_IsBoardVersionSupported   isBoardVersionSupported  ; /**< \brief \ref AppDbStdIf_IsBoardVersionSupported   */
    AppDbStdIf_IsConfigFileVersionActual isConfigFileVersionActual; /**< \brief \ref AppDbStdIf_IsConfigFileVersionActual */
    AppDbStdIf_SetupWizard 				 setupWizard			  ;	/**< \brief \ref AppDbStdIf_SetupWizard */
    AppDbStdIf_ClearFaults 				 clearFaults			  ;	/**< \brief \ref AppDbStdIf_ClearFaults */

    /* *INDENT-ON* */
};



/** \addtogroup AppDbStdIf
 * \{ */
/** Common shell commands
 *
 */
boolean AppDbStdIf_shellSetupCommon(AppDbStdIf *stdif, pchar args, void *data, IfxStdIf_DPipe *io);

/** Check the minimal setup configuration
 *
 */
boolean AppDbStdIf_isMinimalSetup(AppDbStdIf *stdif, IfxStdIf_DPipe *io);


/** \copybrief AppDbStdIf_DisableDriver
 * See \ref AppDbStdIf_DisableDriver
 */
IFX_INLINE void                 AppDbStdIf_disableDriver          (AppDbStdIf *stdif)
{
	stdif->disableDriver          (stdif->driver);
}
/** \copybrief AppDbStdIf_DumpDriverInfo
 * See \ref AppDbStdIf_DumpDriverInfo
 */
IFX_INLINE void                 AppDbStdIf_dumpDriverInfo         (AppDbStdIf *stdif, IfxStdIf_DPipe *io, boolean diff)
{
	stdif->dumpDriverInfo         (stdif->driver, io, diff);
}
/** \copybrief AppDbStdIf_EnableDriver
 * See \ref AppDbStdIf_EnableDriver
 */
IFX_INLINE void                 AppDbStdIf_enableDriver           (AppDbStdIf *stdif)
{
	stdif->enableDriver           (stdif->driver);
}
/** \copybrief AppDbStdIf_GetBoardVersion
 * See \ref AppDbStdIf_GetBoardVersion
 */
IFX_INLINE LB_FileBoardVersion* AppDbStdIf_getBoardVersion        (AppDbStdIf *stdif)
{
	return               stdif->getBoardVersion        (stdif->driver);
}
/** \copybrief AppDbStdIf_GetMainConfig
 * See \ref AppDbStdIf_GetMainConfig
 */
IFX_INLINE AppDbStdIf_MainConfig* AppDbStdIf_getMainConfig        (AppDbStdIf *stdif)
{
	return               stdif->getmainConfig        (stdif->driver);
}
/** \copybrief AppDbStdIf_GetConfigEfs
 * See \ref AppDbStdIf_GetConfigEfs
 */
IFX_INLINE Ifx_Efs*             AppDbStdIf_getConfigEfs           (AppDbStdIf *stdif)
{
	return               stdif->getConfigEfs           (stdif->driver);
}
/** \copybrief AppDbStdIf_GetFault
 * See \ref AppDbStdIf_GetFault
 */
IFX_INLINE uint32               AppDbStdIf_getFault               (AppDbStdIf *stdif)
{
	return               stdif->getFault               (stdif->driver);
}
/** \copybrief AppDbStdIf_GetVdc
 * See \ref AppDbStdIf_GetVdc
 */
IFX_INLINE float32             AppDbStdIf_getVdc           (AppDbStdIf *stdif)
{
	return               stdif->getVdc           (stdif->driver);
}
/** \copybrief AppDbStdIf_GetIgbtTemp
 * See \ref AppDbStdIf_GetIgbtTemp
 */
IFX_INLINE void             AppDbStdIf_getIgbtTemp           (AppDbStdIf *stdif, float32 *tempArray)
{
	stdif->getIgbtTemp           (stdif->driver, tempArray);
}
/** \copybrief AppDbStdIf_GetBoardVersionEfs
 * See \ref AppDbStdIf_GetBoardVersionEfs
 */
IFX_INLINE Ifx_Efs*             AppDbStdIf_getBoardVersionEfs           (AppDbStdIf *stdif)
{
	return               stdif->getBoardVersionEfs           (stdif->driver);
}
/** \copybrief AppDbStdIf_Init
 * See \ref AppDbStdIf_Init
 */
IFX_INLINE boolean              AppDbStdIf_init                   (AppDbStdIf *stdif, IfxQspi_SpiMaster *qspi, Ifx_At25xxx *eeprom)
{
	return               stdif->init                   (stdif->driver, qspi, eeprom);
}
/** \copybrief AppDbStdIf_InitDriver
 * See \ref AppDbStdIf_InitDriver
 */
IFX_INLINE boolean              AppDbStdIf_initDriver             (AppDbStdIf *stdif, IfxQspi_SpiMaster *qspi, LB_FileBoardVersion *logicBoardVersion)
{
	return               stdif->initDriver             (stdif->driver, qspi, logicBoardVersion);
}
/** \copybrief AppDbStdIf_PostInitialization
 * See \ref AppDbStdIf_PostInitialization
 */
IFX_INLINE boolean              AppDbStdIf_postInitialization     (AppDbStdIf *stdif)
{
	return               stdif->postInitialization     (stdif->driver);
}
/** \copybrief AppDbStdIf_PrintBoardConfiguration
 * See \ref AppDbStdIf_PrintBoardConfiguration
 */
IFX_INLINE void                 AppDbStdIf_printBoardConfiguration(AppDbStdIf *stdif, IfxStdIf_DPipe *io)
{
	stdif->printBoardConfiguration(stdif->driver, io);
}
/** \copybrief AppDbStdIf_SaveConfig
 * See \ref AppDbStdIf_SaveConfig
 */
IFX_INLINE boolean              AppDbStdIf_saveConfig             (AppDbStdIf *stdif)
{
	return               stdif->saveConfig             (stdif->driver);
}
/** \copybrief AppDbStdIf_SelfTest
 * See \ref AppDbStdIf_SelfTest
 */
IFX_INLINE boolean              AppDbStdIf_selfTest               (AppDbStdIf *stdif)
{
	return               stdif->selfTest               (stdif->driver);
}
/** \copybrief AppDbStdIf_SetDefaultConfig
 * See \ref AppDbStdIf_SetDefaultConfig
 */
IFX_INLINE void                 AppDbStdIf_setDefaultConfig       (AppDbStdIf *stdif)
{
	stdif->setDefaultConfig       (stdif->driver);
}
/** \copybrief AppDbStdIf_ShellSetup
 * See \ref AppDbStdIf_ShellSetup
 */
IFX_INLINE boolean              AppDbStdIf_shellSetup             (AppDbStdIf *stdif, pchar args, void *data, IfxStdIf_DPipe *io)
{
	boolean result;
	result = AppDbStdIf_shellSetupCommon(stdif, args, data, io);
	if (!result)
	{
    	result = stdif->shellSetup             (stdif->driver, args, data, io);
	}

	return result;
}


/** \copybrief AppDbStdIf_CreateBoardVersionEfs
 * See \ref AppDbStdIf_CreateBoardVersionEfs
 */
IFX_INLINE boolean AppDbStdIf_createBoardVersionEfs(AppDbStdIf *stdif, boolean quiet)
{
	return              stdif->createBoardVersionEfs             (stdif->driver, quiet);
}
/** \copybrief AppDbStdIf_CreateBoardVersionFile
 * See \ref AppDbStdIf_CreateBoardVersionFile
 */
IFX_INLINE boolean AppDbStdIf_createBoardVersionFile(AppDbStdIf *stdif, boolean quiet)
{
	return              stdif->createBoardVersionFile             (stdif->driver, quiet);
}
/** \copybrief AppDbStdIf_CreateConfigEfs
 * See \ref AppDbStdIf_CreateConfigEfs
 */
IFX_INLINE boolean AppDbStdIf_createConfigEfs(AppDbStdIf *stdif, boolean quiet)
{
	return              stdif->createConfigEfs             (stdif->driver, quiet);
}
/** \copybrief AppDbStdIf_CreateConfigFile
 * See \ref AppDbStdIf_CreateConfigFile
 */
IFX_INLINE boolean AppDbStdIf_createConfigFile(AppDbStdIf *stdif, boolean quiet)
{
	return              stdif->createConfigFile             (stdif->driver, quiet);
}
/** \copybrief AppDbStdIf_IsBoardVersionFile
 * See \ref AppDbStdIf_IsBoardVersionFile
 */
IFX_INLINE boolean AppDbStdIf_isBoardVersionFile(AppDbStdIf *stdif)
{
	return              stdif->isBoardVersionFile             (stdif->driver);
}
/** \copybrief AppDbStdIf_IsConfigFile
 * See \ref AppDbStdIf_IsConfigFile
 */
IFX_INLINE boolean AppDbStdIf_isConfigFile(AppDbStdIf *stdif)
{
	return              stdif->isConfigFile             (stdif->driver);
}
/** \copybrief AppDbStdIf_LoadBoardVersionFile
 * See \ref AppDbStdIf_LoadBoardVersionFile
 */
IFX_INLINE boolean AppDbStdIf_loadBoardVersionFile(AppDbStdIf *stdif)
{
	return              stdif->loadBoardVersionFile             (stdif->driver);
}
/** \copybrief AppDbStdIf_LoadConfigFile
 * See \ref AppDbStdIf_LoadConfigFile
 */
IFX_INLINE boolean AppDbStdIf_loadConfigFile(AppDbStdIf *stdif)
{
	return              stdif->loadConfigFile             (stdif->driver);
}
/** \copybrief AppDbStdIf_SaveBoardVersion
 * See \ref AppDbStdIf_AppDbStdIf_SaveBoardVersion
 */
IFX_INLINE boolean AppDbStdIf_saveBoardVersion(AppDbStdIf *stdif)
{
	return              stdif->saveBoardVersion             (stdif->driver);
}

/** \copybrief AppDbStdIf_IsBoardTypeSupported
 * See \ref AppDbStdIf_IsBoardTypeSupported
 */
IFX_INLINE boolean AppDbStdIf_isBoardTypeSupported(AppDbStdIf *stdif)
{
	return              stdif->isBoardTypeSupported                  (stdif->driver);
}
/** \copybrief AppDbStdIf_IsBoardVersionSupported
 * See \ref AppDbStdIf_IsBoardVersionSupported
 */
IFX_INLINE boolean AppDbStdIf_isBoardVersionSupported(AppDbStdIf *stdif)
{
	return              stdif->isBoardVersionSupported               (stdif->driver);
}
/** \copybrief AppDbStdIf_IsConfigFileVersionActual
 * See \ref AppDbStdIf_IsConfigFileVersionActual
 */
IFX_INLINE boolean AppDbStdIf_isConfigFileVersionActual(AppDbStdIf *stdif)
{
	return              stdif->isConfigFileVersionActual             (stdif->driver);
}
/** \copybrief AppDbStdIf_SetupWizard
 * See \ref AppDbStdIf_SetupWizard
 */
IFX_INLINE char AppDbStdIf_setupWizard(AppDbStdIf *stdif, Ifx_Shell *shell)
{
	return              stdif->setupWizard             (stdif->driver, shell);
}

/** \copybrief AppDbStdIf_ClearFaults
 * See \ref AppDbStdIf_ClearFaults
 */
IFX_INLINE boolean AppDbStdIf_clearFaults(AppDbStdIf *stdif)
{
	return              stdif->clearFaults(stdif->driver);
}


/** \} */

//----------------------------------------------------------------------------------------

#endif /* APPDBSTDIF_H */
