/**
 * \file AppStateMachine.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#ifndef APPSTATEMACHINE_H_
#define APPSTATEMACHINE_H_

#include "SysSe/Bsp/Bsp.h"
#include "Library/AppSpecialMode.h"

/** Application modes
 * \note Item order matters
 */
typedef enum
{
    AppMode_off                	   = 0,/**< \brief off state  */
    AppMode_boot                   = 1,/**< \brief boot state  */
    AppMode_configuration          = 2,/**< \brief configuration state */
    AppMode_hwInitialization       = 3,/**< \brief Hardware initialization */
    AppMode_appInitialization      = 4,/**< \brief Application initialization */
    AppMode_run                    = 5,/**< \brief run state */
    AppMode_limitedConfiguration   = 6,/**< \brief configuration state (limited) */
    AppMode_error                  = 7,/**< \brief error state  */
    AppMode_shutdown               = 8,/**< \brief shutdown state  */
}AppMode;

typedef struct
{
    boolean hwEmergency;
    volatile AppMode hwMode;             /**< \brief current mode */
    boolean hwInitReady;
    volatile AppMode hwModeRequest;      /**< \brief requested mode */
    AppModeSpecial specialMode; /**< \brief special mode flag */
    boolean vdcMeasurementEnabled; /**< \brief Flag to protect against simultaneous access to the QSPI from different interrupt levels */
}AppStateMachine;

void AppStateMachine_process(AppStateMachine *stateMachine);

IFX_EXTERN const pchar AppStateMachine_ModeNames[9];

#endif /* APPSTATEMACHINE_H_ */
