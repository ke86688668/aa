/**
 * \file AppTest.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include "AppInterface.h"
#include "AppTest.h"
#include "Main.h"
#include "AppCan.h"

#if CFG_SETUP == CFG_SETUP_TEST
AppTest g_testData;

void ECU_slotBackground(void)
{}

void ECU_slotOneMs(void)
{
    g_testData.isrCounter.slotOneMs++;
}


void ECU_slotTenMs(void)
{
    g_testData.isrCounter.slotTenMs++;
}


void ECU_slotPwmPeriodStart(ECU_MotorIndex motorIndex)
{
    g_testData.isrCounter.slotPwmPeriodStart++;
}


void ECU_slotTriggerPoint(ECU_MotorIndex motorIndex)
{
    g_testData.isrCounter.slotTriggerPoint++;
}


void ECU_slotEndOfPhaseCurrentConversion(ECU_MotorIndex motorIndex)
{
    g_testData.isrCounter.slotEndOfPhaseCurrentConversion++;
    ECU_getMotorPhaseCurrents(0, g_testData.AppState.currents);
    g_testData.AppState.vDc      = ECU_getInverterVdc(motorIndex);
    g_testData.AppState.status   = ECU_getInverterStatus(motorIndex);
    g_testData.AppState.position = ECU_getMotorMechanicalPosition(motorIndex);
    g_testData.AppState.speed    = ECU_getMotorMechanicalSpeed(motorIndex);

    /* Fill in CAN outbox */
    //g_App.can.outbox.mode = 0;
    //g_App.can.outbox.error = 0;
    //g_App.can.outbox.torqueMeas = 0;
    g_App.can.outbox.speedMeas       = g_testData.AppState.speed;
    g_App.can.outbox.voltageDcMeas   = g_testData.AppState.vDc;
    g_App.can.outbox.tempIgbtMeas[0] = g_testData.AppState.status.igbtTemp[0];
    g_App.can.outbox.tempIgbtMeas[1] = g_testData.AppState.status.igbtTemp[1];
    g_App.can.outbox.tempIgbtMeas[2] = g_testData.AppState.status.igbtTemp[2];
    g_App.can.outbox.tempMotorMeas   = g_testData.AppState.status.motorTemp;
    //g_App.can.outbox.tempBoardMeas = 0;
}


boolean ECU_slotInit(void)
{
    uint32 i;

    g_testData.selectedMotor = 0;

    for (i = 0; i < ECU_PHASE_PER_MOTOR_COUNT; i++)
    {
        g_testData.pwmDuty[i] = 0.0;
    }

    g_testData.isrCounter.slotOneMs                       = 0;
    g_testData.isrCounter.slotTenMs                       = 0;
    g_testData.isrCounter.slotPwmPeriodStart              = 0;
    g_testData.isrCounter.slotTriggerPoint                = 0;
    g_testData.isrCounter.slotEndOfPhaseCurrentConversion = 0;
    g_testData.isrCounter.canNode0                        = 0;
    g_testData.isrCounter.canGroup0                       = 0;
    ECU_selectPositionSensor(0, ECU_PositionSensor_encoder);
    return TRUE;
}


#endif
