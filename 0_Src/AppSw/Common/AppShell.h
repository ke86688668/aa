/**
 * \file AppShell.h
 * \brief Shell interface
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup AppShell 6 - Application shell
 * \ingroup main_sourceCodeApplication
 *
 *
 *
 */

#if !defined(APP_SHELL_H)
#define APP_SHELL_H

//----------------------------------------------------------------------------------------
#include "SysSe/Comm/Ifx_Shell.h"
#include "AppStateMachine.h"

//----------------------------------------------------------------------------------------
/** \addtogroup AppShell
 * \{ */
/** \name Command handlers
 *
 * The purpose of the shell interface is to give an easy access to the application, esp.
 * while testing. Commands can be sent using a simple serial interface console (e.g.
 * HyperTerminal) and connected to the USB port of the PC.
 *
 * The list of all available commands is displayed when the command "help" is
 * entered. A question mark after the command name will display details about it.
 * Please note that a command is case sensitive.
 * \code
 * >help
 * >acommand ?
 * \endcode
 *
 */
/** \name Auxiliary functions
 * \{ */
boolean AppShell_info(pchar args, void *data, IfxStdIf_DPipe *io);
void    AppShell_init(Ifx_Shell *shell, IfxStdIf_DPipe *standardIo);
void    AppShell_process(Ifx_Shell *shell);
boolean AppShell_isValidCommand(IfxStdIf_DPipe *io, Ifx_Shell_Call cmd, AppMode mode);
boolean AppShell_setup(pchar args, void *data, IfxStdIf_DPipe *io);

/** \} */
/** \} */
//----------------------------------------------------------------------------------------

#endif
