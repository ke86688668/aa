#ifndef IFX_DEVICEDATA_H
#define IFX_DEVICEDATA_H 1

#include "Ifx_Types.h"
#include "SysSe/General/Ifx_DeviceCache.h"


/** Generic device driver type
 */
typedef void *Ifx_DeviceData_DeviceDriver;


/** API prototype: Read from device
 * \param driver Driver used to communicate with the device
 * \param value Buffer to store the data read from the device.
 */
typedef boolean (*Ifx_DeviceData_ReadBitfieldFromDevice)(Ifx_DeviceData_DeviceDriver driver, uint32 *value);

/** API prototype: Write a bitfield to the device
 * \param driver Driver used to communicate with the device
 * \param value Value to be written to the device.
 */
typedef boolean (*Ifx_DeviceData_WriteBitfieldToDevice)(Ifx_DeviceData_DeviceDriver driver, uint32 value);


#define IFX_DEVICEDATA_ACCESS_FLAG_R (0x1)		/**< \brief read access possible */
#define IFX_DEVICEDATA_ACCESS_FLAG_W (0x2)		/**< \brief write access possible */
#define IFX_DEVICEDATA_ACCESS_FLAG_H (0x4)		/**< \brief can be modified by hardware */

typedef struct
{
	pchar name;											 /**< \brief bitfield name */
	uint8 flags;										 /**< \brief bitfield access flags. mask={IFX_DEVICEDATA_ACCESS_FLAG_R, IFX_DEVICEDATA_ACCESS_FLAG_W, IFX_DEVICEDATA_ACCESS_FLAG_H} */
	uint8 offset;										 /**< \brief bitfield offset */
	uint16 mask;										 /**< \brief bitfield mask */
	uint16 index;										 /**< \brief index used for bitfield identification*/
}Ifx_DeviceData_Bitfield;
#define IFX_DEVICEDATA_BITFIELD_END     {0, 0, 0, 0, 0}

typedef struct
{
	pchar name;											/**< \brief Register name */
	uint8 flags;										 /**< \brief Register access flags. mask={IFX_DEVICEDATA_ACCESS_FLAG_R, IFX_DEVICEDATA_ACCESS_FLAG_W, IFX_DEVICEDATA_ACCESS_FLAG_H} */
	uint16 offset;										/**< \brief register offset */
	const Ifx_DeviceData_Bitfield *bitfieldList;
}Ifx_DeviceData_Register;

#define IFX_DEVICEDATA_REGISTER_END     {0, 0, 0, 0}

typedef struct
{
	pchar name;											/**< \brief Device name */
	const Ifx_DeviceData_Register *registerList;
}Ifx_DeviceData;



#endif /* IFX_DEVICEDATA_H */
