/**
 * \file MatlabLayer.c
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */
#include "AppInterface.h"

#if CFG_SETUP == CFG_SETUP_MATLAB
#include "controllerThreePhase.h"
#include "controllerThreePhase_types.h"

extern RT_MODEL_controllerThreePhase *const  controllerThreePhase_M;     /* Real-time model */
extern BlockIO_controllerThreePhase          controllerThreePhase_B;     /* Observable signals */
extern D_Work_controllerThreePhase           controllerThreePhase_DWork; /* Observable states */
extern ExternalInputs_controllerThreeP       controllerThreePhase_U;     /* External inputs */
extern ExternalOutputs_controllerThree       controllerThreePhase_Y;     /* External outputs */
extern const Parameters_controllerThreePhase controllerThreePhase_P;

void ECU_slotBackground(void)
{}

void ECU_slotOneMs(void)
{}
void ECU_slotShutdown(void)
{}

void ECU_slotTenMs(void)
{}

void ECU_slotPwmPeriodStart(ECU_MotorIndex motorIndex)
{}

void ECU_slotTriggerPoint(ECU_MotorIndex motorIndex)
{}

void ECU_slotEndOfPhaseCurrentConversion(ECU_MotorIndex motorIndex)
{
    ECU_InverterStatus inverterStatus;
    inverterStatus                                    = ECU_getInverterStatus(0);

    controllerThreePhase_U.driverPhaseFaultAbc.faultA = (inverterStatus.phase1TopDriverError || inverterStatus.phase1BottomDriverError) != 0;
    controllerThreePhase_U.driverPhaseFaultAbc.faultB = (inverterStatus.phase2TopDriverError || inverterStatus.phase2BottomDriverError) != 0;
    controllerThreePhase_U.driverPhaseFaultAbc.faultC = (inverterStatus.phase3TopDriverError || inverterStatus.phase3BottomDriverError) != 0;

    float32 measuredCurrents[3];

    ECU_getMotorPhaseCurrents(0, measuredCurrents);
    controllerThreePhase_U.currentMeas.iA = measuredCurrents[0];
    controllerThreePhase_U.currentMeas.iB = measuredCurrents[1];
    controllerThreePhase_U.currentMeas.iC = measuredCurrents[2];

    controllerThreePhase_U.voltageDcMeas  = ECU_getInverterVdc(0); // function should have name ECU_getInverterVdc()
    controllerThreePhase_U.angleMeas      = ECU_getMotorMechanicalPosition(0);
    //controllerThreePhase_U.speedMeas = ECU_getMotorMechanicalSpeed(0);

    controllerThreePhase_U.tempIgbtMeas.temp1 = inverterStatus.igbtTemp[0]; // structure should have name inverterStatus
    //controllerThreePhase_U.tempIgbtMeas.temp2 = inverterStatus.IgbtTemp[1];
    //controllerThreePhase_U.tempIgbtMeas.temp3 = inverterStatus.IgbtTemp[2];

    controllerThreePhase_U.tempMotorMeas = inverterStatus.motorTemp; // motorTemp should be array as well

    controllerThreePhase_step(
        &controllerThreePhase_P,
        &controllerThreePhase_B,
        &controllerThreePhase_DWork,
        &controllerThreePhase_U,
        &controllerThreePhase_Y
        );
    controllerThreePhase_U.driverPhaseFaultAbc.clearError = 0; //???????????????
}


boolean ECU_slotInit(void)
{
    ECU_selectPositionSensor(0, ECU_PositionSensor_encoder);
    controllerThreePhase_initialize(
        controllerThreePhase_M,
        &controllerThreePhase_P,
        &controllerThreePhase_B,
        &controllerThreePhase_DWork,
        &controllerThreePhase_U,
        &controllerThreePhase_Y
        );

    return TRUE;
}


#endif
