/*
 * AppSetupWizard.h
 *

 */

#ifndef APPSETUPWIZARD_H_
#define APPSETUPWIZARD_H_

#include "SysSe/Comm/Ifx_Shell.h"
#include "StdIf/IfxStdIf_Pos.h"

boolean AppSetupWizard_run(IfxStdIf_DPipe *io);
IfxStdIf_Pos_Status AppSetupWizard_dummyIfxStdIfPosGetFault(void *handle);


sint32 AppSetupWizard_ask(Ifx_Shell *shell, pchar text, pchar prefill);
sint32 AppSetupWizard_askOptions(Ifx_Shell *shell, pchar text, sint32 defaultValue, sint32 prefill, pchar answers[]);
boolean AppSetupWizard_continue(Ifx_Shell *shell);
sint32 AppSetupWizard_askUint32(Ifx_Shell *shell, pchar text, uint32 defaultValue, uint32 min, uint32 max, uint32 *value);
sint32 AppSetupWizard_askSint32(Ifx_Shell *shell, pchar text, sint32 defaultValue, sint32 min, sint32 max, sint32 *value);
sint32 AppSetupWizard_askUint8(Ifx_Shell *shell, pchar text, uint8 defaultValue, uint8 min, uint8 max, uint8 *value);
sint32 AppSetupWizard_askBoolean(Ifx_Shell *shell, pchar text, boolean defaultValue, boolean *value);
sint32 AppSetupWizard_askUint16(Ifx_Shell *shell, pchar text, uint16 defaultValue, uint16 min, uint16 max, uint16 *value);
sint32 AppSetupWizard_askFloat32(Ifx_Shell *shell, pchar text, float32 defaultValue, float32 min, float32 max, float32 *value);



#endif /* APPSETUPWIZARD_H_ */
