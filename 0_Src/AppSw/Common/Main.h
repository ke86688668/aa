/**
 * \file Main.h
 * \brief System initialization and main program implementation.
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup main_sourceCode Source code documentation
 *
 * \defgroup main_sourceCodeApplication 1 - Application code
 * \ingroup main_sourceCode
 *
 * \defgroup library 3 - Library
 * \ingroup main_sourceCode
 *
 * \defgroup IfxLld 4 - iLLD
 * \ingroup main_sourceCode
 *
 * \defgroup IfxLld_Ext 5 - iLLD external device
 * \ingroup main_sourceCode
 *
 *
 * \defgroup main_sourceCodeGlobals 3 - Global variables
 * \ingroup main_sourceCodeApplication
 *
 */

#ifndef MAIN_H
#define MAIN_H

//________________________________________________________________________________________
// INCLUDES

#include "Configuration.h"
#include "SysSe/Bsp/Bsp.h"
#include "SysSe/Comm/Ifx_Console.h"
#include "SysSe/Comm/Ifx_Shell.h"

#include "AppInterface.h"
#include "SysSe/Debug/Ifx_Osci.h"
#include "SysSe/Debug/Ifx_OsciShell.h"
#include "IfxGlobal_cfg.h"
#include "SysSe/Comm/Ifx_CanHandler.h"

#include "LogicBoard3_0/App/AppCanConsole.h"

#include "LB.h"
#include "AppStateMachine.h"

//________________________________________________________________________________________
// CONFIGURATION MACROS
//#define SW_REVISION         (0x0000011C)      // Software version and revision 0x0000vvrr, with v the version number and rr the revision number. even rr are public release
#define SW_REVISION         (0x1A000203)		// 1A : means for EA test; 200: means for version control
#define SW_COMPILER_VERSION ((COMPILER_VERSION / 1000) << 16) | ((COMPILER_VERSION % 10) << 8) | (COMPILER_REVISION)
#define BYPASSDRVBD 			(1)
#define BYPASSONEEYE			(1)
#define AUTORUN					(0)
#define BYPASSEEPROM			(1)
#define BYPASSSHELL				(1)
#define ANALOGSIGNALFLOATING	(1)
#define BYPASSIOM				(1)
#define BYPASSPROTECTION		(0)
#define BYPASSPROTECTIONLATCH 	(1)
#define DISABLERESOLVER12V		(0)
#define CAN_FD_ENABLE			(1)
#define ENABLEALLCAN			(0)
#define ONEEYECOM				(0)
#define ENABLE_ASC				(0)
//________________________________________________________________________________________
// HELPER MACROS

//________________________________________________________________________________________
// ENUMERATIONS

typedef struct
{
    float32 sysFreq;                /**< \brief Actual SPB frequency */
    float32 cpuFreq;                /**< \brief Actual CPU frequency */
    float32 pllFreq;                /**< \brief Actual PLL frequency */
    float32 stmFreq;                /**< \brief Actual STM frequency */
    float32 gtmFreq;
    float32 gtmGclkFreq;
    float32 gtmCmuClk1Freq;         /**< \brief CMU CLK 1 frequency, used by GPT12 incremental encoder */
    sint32  srcRev;                 /**< \brief Project source code revision number */
    sint32  compilerVer;            /**< \brief Compiler version */
} AppInfo;

/** \brief Application information */
typedef struct
{
    AppInfo         info;                       /**< \brief Info object */
    Ifx_Shell       primaryShell;               /**< \brief Primary shell */
    Ifx_Shell       secondaryShell;             /**< \brief Secondary shell */
    boolean         initialized;                /**< \brief if TRUE, indicates that the initialization phase is done */

    AppStateMachine hwState;

    struct
    {
        IfxCan_MsgObjId          msgObjId;
        struct
        {
            uint8   modeCmd;
            boolean clearErrorCmd;
            boolean pulseMode;
            boolean configExit;
            boolean runCmd;
            float32 currentIdCmd;
            float32 currentIqCmd;
            float32 currentCmd;
            float32 slipCmd;
            float32 torqueCmd;
            float32 speedCmd;
            float32 amplitudeCmd;   /* Amplitude command for open loopp mode */
            float32 pulseOffset[6]; /* Pulse offset U top, V top, W top, U bottom, V bottom, W bottom */
            float32 pulseOn[6];     /* Pulse on U top, V top, W top, U bottom, V bottom, W bottom */
        }inbox;
        struct
        {
            uint8   mode;
            boolean error;
            boolean clearError;
            boolean run;
            boolean configExit;
            boolean ecuReset;
            uint8   ecuState;
            float32 torqueMeas;
            float32 speedMeas;
            float32 voltageDcMeas;
            float32 tempIgbtMeas[ECU_PHASE_PER_MOTOR_COUNT];
            float32 tempMotorMeas;
            float32 tempBoardMeas;

            float32 current[ECU_PHASE_PER_MOTOR_COUNT];
            float32 mechPos;
            float32 elecPos;

            float32 mechPosR;
            float32 mechSpeedR;
            /* 20200221 be created for more information with CAN message */
            float32 torqueCtrl_set;
            uint16 current_raw[ECU_PHASE_PER_MOTOR_COUNT];
            uint16 currentDcMeas_raw;
            uint16 voltageDcMeas_raw;
            uint32 fault;
            uint32 PMIC_Info;
            uint8  GateDriver_Pri_Info[ECU_GATEDRIVER_COUNT];
            uint8  GateDriver_Sec_Info[ECU_GATEDRIVER_COUNT];
            /* end of created*/
			uint32 swVersion;

#if CFG_CURRENT_SENSOR_TEST == 1
            sfract logCurrent[ECU_MOTOR_COUNT][ECU_PHASE_PER_MOTOR_COUNT];
            sfract logCurrentDiff[ECU_PHASE_PER_MOTOR_COUNT];
#endif

        }              outbox;
        Ifx_CanHandler handler;
        AppCanConsole  canConsole;              /**< \brief CAN console */
        struct
        {
            IfxCan_Can_MsgObj modeCmd;     /**< \brief  APPCAN_MSG_ID_RX_MODECMD */
            IfxCan_Can_MsgObj modeCmd1;    /**< \brief  APPCAN_MSG_ID_RX_MODECMD1 */
            IfxCan_Can_MsgObj modeCmd2A;   /**< \brief  APPCAN_MSG_ID_RX_MODECMD2A */
            IfxCan_Can_MsgObj modeCmd2S;   /**< \brief  APPCAN_MSG_ID_RX_MODECMD2S */
            IfxCan_Can_MsgObj modeCmd3;    /**< \brief  APPCAN_MSG_ID_RX_MODECMD3 */
            IfxCan_Can_MsgObj modeCmd4;    /**< \brief  APPCAN_MSG_ID_RX_MODECMD4 */
            IfxCan_Can_MsgObj modeCmd5[6]; /**< \brief  APPCAN_MSG_ID_RX_MODECMD5[A-F] */
            IfxCan_Can_MsgObj stat[9];     /**< \brief  APPCAN_MSG_ID_TX_STAT1 up to APPCAN_MSG_ID_TX_STAT9 */
            IfxCan_Can_MsgObj consoleRx;   /**< \brief  APPCAN_MSG_ID_RX_CONSOLE */
            IfxCan_Can_MsgObj consoleTx;   /**< \brief  APPCAN_MSG_ID_TX_CONSOLE */
#if CFG_CURRENT_SENSOR_TEST == 1
            IfxCan_Can_MsgObj log[3];     /**< \brief  APPCAN_MSG_ID_TX_LOG0 up to APPCAN_MSG_ID_TX_LOG2 */
#endif
        }msg;
        Ifx_CanHandler_MessagePointer messageStat1;
    }             can;
    Ifx_Osci      osci;
    Ifx_OsciShell osciShell;
    boolean boardVersionLock;					/**< Board version lock. TRUE: locked, FALSE: unlocked */
} App;

//________________________________________________________________________________________
// IMPORTED VARIABLES

//________________________________________________________________________________________
// GLOBAL VARIABLES

IFX_EXTERN App g_App;

//________________________________________________________________________________________
// FUNCTION PROTOTYPES
//________________________________________________________________________________________
//________________________________________________________________________________________
IFX_INLINE boolean App_isLocked(void)
{
    return g_App.boardVersionLock;
}

#endif
