/**
 * \file AppLbStdIf.h
 * \brief Standard interface: Logic board
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 * \defgroup AppLbStdIf Logic board standard interface
 * \ingroup LB
 *
 */
#ifndef APPLBSTDIF_H
#define APPLBSTDIF_H 1

#include "StdIf/IfxStdIf.h"
#include "Config/Ifx_Cfg.h"
#include "StdIf/IfxStdIf_DPipe.h"
#include "StdIf/IfxStdIf_Pos.h"
#include "StdIf/IfxStdIf_Inverter.h"
#include "Can/Can/IfxCan_Can.h"
#include "Edsadc/Rdc/IfxDsadc_Rdc.h"
#include "AppGlobalDef.h"

//----------------------------------------------------------------------------------------
#define CFG_LB_CONFIG_EFS_FILE_SIZE                        (1024)
#define CFG_LB_CONFIG_EFS_OFFSET         (0x0000)
#define CFG_LB_CONFIG_EFS_PARTITION_SIZE (0x4000)

#define CFG_LB_BOARD_EFS_FILE_SIZE                         (256)

#define CFG_LB30_BOARD_EFS_OFFSET          (0x6000)
#define CFG_LB30_BOARD_EFS_PARTITION_SIZE  (0x2000)

//----------------------------------------------------------------------------------------

/** \brief Forward declaration */
typedef struct AppLbStdIf_ AppLbStdIf;

typedef boolean          (*AppLbStdIf_ShellSetup)(IfxStdIf_InterfaceDriver driver, pchar args, void *data, IfxStdIf_DPipe *io);
typedef boolean          (*AppLbStdIf_ShellSet)(IfxStdIf_InterfaceDriver driver, pchar args, void *data, IfxStdIf_DPipe *io);
typedef boolean          (*AppLbStdIf_ShellGet)(IfxStdIf_InterfaceDriver driver, pchar args, void *data, IfxStdIf_DPipe *io);

typedef boolean          (*AppLbStdIf_AppInit)(IfxStdIf_InterfaceDriver driver);
typedef boolean          (*AppLbStdIf_AppPreInit)(IfxStdIf_InterfaceDriver driver);
typedef void             (*AppLbStdIf_CurrentOffsetsCalibrationStep)(IfxStdIf_InterfaceDriver driver);
typedef boolean          (*AppLbStdIf_UpdateLogicBoardConfigFile)(IfxStdIf_InterfaceDriver driver);

typedef boolean          (*AppLbStdIf_AppInitConsole)(IfxStdIf_InterfaceDriver driver, boolean primaryConsoleAsc);
typedef boolean          (*AppLbStdIf_AppInit1MsTimer)(IfxStdIf_InterfaceDriver driver);
typedef boolean          (*AppLbStdIf_AppPostInit)(IfxStdIf_InterfaceDriver driver);
typedef boolean          (*AppLbStdIf_AppSelfTest)(IfxStdIf_InterfaceDriver driver);
typedef void             (*AppLbStdIf_SetDigitalOutput)(IfxStdIf_InterfaceDriver driver, uint32 index, boolean state);
typedef boolean          (*AppLbStdIf_GetDigitalInput)(IfxStdIf_InterfaceDriver driver, uint32 index);

typedef boolean          (*AppLbStdIf_GetDigitalOutput)(IfxStdIf_InterfaceDriver driver, uint32 index);
typedef float32          (*AppLbStdIf_GetAnalogInput)(IfxStdIf_InterfaceDriver driver, uint32 index);
typedef void             (*AppLbStdIf_RaiseIomFault)(IfxStdIf_InterfaceDriver drive);

/** Create the configuration EFS, return TRUE in case of success
 *
 */
typedef boolean (*AppLbStdIf_CreateConfigEfs       )(IfxStdIf_InterfaceDriver driver, boolean quiet);
/** Create the configuration file, return TRUE in case of success
 *
 */
typedef boolean (*AppLbStdIf_CreateConfigFile      )(IfxStdIf_InterfaceDriver driver, boolean quiet);
/** Return TRUE if the configuration file exist
 *
 */
typedef boolean (*AppLbStdIf_IsConfigFile          )(IfxStdIf_InterfaceDriver driver);
/** Load the configuration file, return TRUE in case of success
 *
 */
typedef boolean (*AppLbStdIf_LoadConfigFile        )(IfxStdIf_InterfaceDriver driver);
/** Set the resolver gain
 *
 */
typedef boolean (*AppLbStdIf_SetResolverGain        )(IfxStdIf_InterfaceDriver driver, uint8 index, uint8 gain);

/** Return TRUE if the board type is supported
 *
 */
typedef boolean (*AppLbStdIf_IsBoardTypeSupported     )(IfxStdIf_InterfaceDriver driver);
/** Return TRUE if the board type / version combination is supported
 *
 */
typedef boolean (*AppLbStdIf_IsBoardVersionSupported  )(IfxStdIf_InterfaceDriver driver);
/** Return TRUE if the configuration file version is up to date
 *
 */
typedef boolean (*AppLbStdIf_IsConfigFileVersionActual)(IfxStdIf_InterfaceDriver driver);
/** Return TRUE if the configuration is valid
 *
 */
typedef boolean (*AppLbStdIf_IsConfigValid)(IfxStdIf_InterfaceDriver driver);


/** \brief Standard interface object
 */
struct AppLbStdIf_
{
    IfxStdIf_InterfaceDriver driver;         /**< \brief Pointer to the specific driver object */

    /** Link to specific logic board standard interfaces, drivers and data
    **/
    IfxStdIf_Pos         *positionSensorX[2];    /** Selected position sensor */
    IfxStdIf_Pos         *encoder;
    IfxStdIf_Pos         *tle5012;
    IfxStdIf_Pos         *ad2s1210;
    IfxStdIf_Pos         *dsadcRdc[2];
    IfxStdIf_Inverter    *inverter;
    IfxStdIf_DPipe       *asc0;

    IfxCan_Can_Node      *canNodes[2];             /**< \brief CAN nodes */
    IfxDsadc_Rdc         *dsadcRdcDriver[2];         /**< \brief DSADC-based resolver object */

    AppAnalogInput* currents[ECU_MOTOR_COUNT][ECU_PHASE_PER_MOTOR_COUNT];
    float32              *motorTemp;
    float32              *in[4];	/* FIXME redundant with getAnalogInput, remove this member */
    float32              *tempBoard;
    float32              *vAna50;
    float32              *vRef50;
    float32              *vDig50;
    float32              *kl30;

    LB_FileConfiguration *configuration;
    boolean 			 *disableCurrentSensorCheck;

    /* Standard interface APIs */
    AppLbStdIf_ShellSetup                    shellSetup;                    /**< \brief \see AppLbStdIf_ShellSetup */
    AppLbStdIf_ShellSet                      shellSet;                      /**< \brief \see AppLbStdIf_ShellSet */
    AppLbStdIf_ShellGet                      shellGet;                      /**< \brief \see AppLbStdIf_ShellGet */

    AppLbStdIf_AppInit                       appInit;                       /**< \brief \see AppLbStdIf_AppInit */
    AppLbStdIf_AppPreInit                    appPreInit;                    /**< \brief \see AppLbStdIf_AppPreInit */
    AppLbStdIf_CurrentOffsetsCalibrationStep currentOffsetsCalibrationStep; /**< \brief \see AppLbStdIf_CurrentOffsetsCalibrationStep */
    AppLbStdIf_UpdateLogicBoardConfigFile    updateLogicBoardConfigFile;    /**< \brief \see AppLbStdIf_UpdateLogicBoardConfigFile */

    AppLbStdIf_AppInitConsole                appInitConsole;                /**< \brief \see AppLbStdIf_AppInitConsole */
    AppLbStdIf_AppInit1MsTimer               appInit1MsTimer;               /**< \brief \see AppLbStdIf_AppInit1MsTimer */
    AppLbStdIf_AppPostInit                   appPostInit;                   /**< \brief \see AppLbStdIf_AppPostInit */
    AppLbStdIf_AppSelfTest                   appSelfTest;                   /**< \brief \see AppLbStdIf_AppSelfTest */
    AppLbStdIf_SetDigitalOutput              setDigitalOutput;              /**< \brief \see AppLbStdIf_SetDigitalOutput */
    AppLbStdIf_GetDigitalInput               getDigitalInput;               /**< \brief \see AppLbStdIf_GetDigitalInput */
    AppLbStdIf_GetDigitalOutput              getDigitalOutput;              /**< \brief \see AppLbStdIf_GetDigitalOutput */
    AppLbStdIf_GetAnalogInput                getAnalogInput;                /**< \brief \see AppLbStdIf_GetAnalogInput */
    AppLbStdIf_RaiseIomFault                 raiseIomFault;                 /**< \brief \see AppLbStdIf_RaiseIomFault */

    AppLbStdIf_CreateConfigEfs            createConfigEfs         ; /**< \brief \ref AppLbStdIf_CreateConfigEfs           */
    AppLbStdIf_CreateConfigFile           createConfigFile        ; /**< \brief \ref AppLbStdIf_CreateConfigFile          */
    AppLbStdIf_IsConfigFile               isConfigFile            ; /**< \brief \ref AppLbStdIf_IsConfigFile              */
    AppLbStdIf_LoadConfigFile             loadConfigFile          ; /**< \brief \ref AppLbStdIf_LoadConfigFile            */
    AppLbStdIf_SetResolverGain            setResolverGain         ; /**< \brief \ref AppLbStdIf_SetResolverGain            */

    AppLbStdIf_IsBoardTypeSupported      isBoardTypeSupported     ; /**< \brief \ref AppLbStdIf_IsBoardTypeSupported      */
    AppLbStdIf_IsBoardVersionSupported   isBoardVersionSupported  ; /**< \brief \ref AppLbStdIf_IsBoardVersionSupported   */
    AppLbStdIf_IsConfigFileVersionActual isConfigFileVersionActual; /**< \brief \ref AppLbStdIf_IsConfigFileVersionActual */
    AppLbStdIf_IsConfigValid 				isConfigValid; 			/**< \brief \ref AppLbStdIf_IsConfigValid */
};


/** \addtogroup AppLbStdIf
 * \{ */
/** Common shell commands
 *
 */
boolean AppLbStdIf_shellSetupCommon(AppLbStdIf *stdif, pchar args, void *data, IfxStdIf_DPipe *io);
/** Check the minimal setup configuration
 *
 */
boolean AppLbStdIf_isMinimalSetup(AppLbStdIf *stdif, IfxStdIf_DPipe *io);


IFX_INLINE boolean AppLbStdIf_shellSetup(AppLbStdIf *stdif, pchar args, void *data, IfxStdIf_DPipe *io)
{
    return stdif->shellSetup(stdif->driver, args, data, io);
}


IFX_INLINE boolean AppLbStdIf_shellSet(AppLbStdIf *stdif, pchar args, void *data, IfxStdIf_DPipe *io)
{
    return stdif->shellSet(stdif->driver, args, data, io);
}


IFX_INLINE boolean AppLbStdIf_shellGet(AppLbStdIf *stdif, pchar args, void *data, IfxStdIf_DPipe *io)
{
    return stdif->shellGet(stdif->driver, args, data, io);
}


IFX_INLINE boolean AppLbStdIf_appInit(AppLbStdIf *stdif)
{
    return stdif->appInit(stdif->driver);
}


IFX_INLINE boolean AppLbStdIf_appPreInit(AppLbStdIf *stdif)
{
    return stdif->appPreInit(stdif->driver);
}


IFX_INLINE void AppLbStdIf_currentOffsetsCalibrationStep(AppLbStdIf *stdif)
{
    stdif->currentOffsetsCalibrationStep(stdif->driver);
}


IFX_INLINE boolean AppLbStdIf_updateLogicBoardConfigFile(AppLbStdIf *stdif)
{
    return stdif->updateLogicBoardConfigFile(stdif->driver);
}


IFX_INLINE boolean AppLbStdIf_appInitConsole(AppLbStdIf *stdif, boolean primaryConsoleAsc)
{
    return stdif->appInitConsole(stdif->driver, primaryConsoleAsc);
}

IFX_INLINE boolean AppLbStdIf_appInit1MsTimer(AppLbStdIf *stdif)
{
    return stdif->appInit1MsTimer(stdif->driver);
}


IFX_INLINE boolean AppLbStdIf_appPostInit(AppLbStdIf *stdif)
{
    return stdif->appPostInit(stdif->driver);
}


IFX_INLINE boolean AppLbStdIf_appSelfTest(AppLbStdIf *stdif)
{
    return stdif->appSelfTest(stdif->driver);
}




IFX_INLINE void AppLbStdIf_setDigitalOutput(AppLbStdIf *stdif, uint32 index, boolean state)
{
    stdif->setDigitalOutput(stdif->driver, index, state);
}


IFX_INLINE boolean AppLbStdIf_getDigitalInput(AppLbStdIf *stdif, uint32 index)
{
    return stdif->getDigitalInput(stdif->driver, index);
}


IFX_INLINE boolean AppLbStdIf_getDigitalOutput(AppLbStdIf *stdif, uint32 index)
{
    return stdif->getDigitalOutput(stdif->driver, index);
}


IFX_INLINE float32 AppLbStdIf_getAnalogInput(AppLbStdIf *stdif, uint32 index)
{
    return stdif->getAnalogInput(stdif->driver, index);
}

IFX_INLINE void AppLbStdIf_raiseIomFault(AppLbStdIf *stdif)
{
    stdif->raiseIomFault(stdif->driver);
}


/** \copybrief AppLbStdIf_CreateConfigEfs
 * See \ref AppLbStdIf_CreateConfigEfs
 */
IFX_INLINE boolean AppLbStdIf_createConfigEfs(AppLbStdIf *stdif, boolean quiet)
{
	return              stdif->createConfigEfs             (stdif->driver, quiet);
}
/** \copybrief AppLbStdIf_CreateConfigFile
 * See \ref AppLbStdIf_CreateConfigFile
 */
IFX_INLINE boolean AppLbStdIf_createConfigFile(AppLbStdIf *stdif, boolean quiet)
{
	return              stdif->createConfigFile             (stdif->driver, quiet);
}
/** \copybrief AppLbStdIf_IsConfigFile
 * See \ref AppLbStdIf_IsConfigFile
 */
IFX_INLINE boolean AppLbStdIf_isConfigFile(AppLbStdIf *stdif)
{
	return              stdif->isConfigFile             (stdif->driver);
}
/** \copybrief AppLbStdIf_LoadConfigFile
 * See \ref AppLbStdIf_LoadConfigFile
 */
IFX_INLINE boolean AppLbStdIf_loadConfigFile(AppLbStdIf *stdif)
{
	return              stdif->loadConfigFile             (stdif->driver);
}

/** \copybrief AppLbStdIf_SetResolverGain
 * See \ref AppLbStdIf_SetResolverGain
 */
IFX_INLINE boolean AppLbStdIf_setResolverGain(AppLbStdIf *stdif, uint8 index, uint8 gain)
{
	return              stdif->setResolverGain             (stdif->driver, index, gain);
}

/** \copybrief AppLbStdIf_IsBoardTypeSupported
 * See \ref AppLbStdIf_IsBoardTypeSupported
 */
IFX_INLINE boolean AppLbStdIf_isBoardTypeSupported(AppLbStdIf *stdif)
{
	return              stdif->isBoardTypeSupported                  (stdif->driver);
}
/** \copybrief AppLbStdIf_IsBoardVersionSupported
 * See \ref AppLbStdIf_IsBoardVersionSupported
 */
IFX_INLINE boolean AppLbStdIf_isBoardVersionSupported(AppLbStdIf *stdif)
{
	return              stdif->isBoardVersionSupported               (stdif->driver);
}
/** \copybrief AppLbStdIf_IsConfigFileVersionActual
 * See \ref AppLbStdIf_IsConfigFileVersionActual
 */
IFX_INLINE boolean AppLbStdIf_isConfigFileVersionActual(AppLbStdIf *stdif)
{
	return              stdif->isConfigFileVersionActual             (stdif->driver);
}

/** \copybrief AppLbStdIf_IsConfigValid
 * See \ref AppLbStdIf_IsConfigValid
 */
IFX_INLINE boolean AppLbStdIf_isConfigValid(AppLbStdIf *stdif)
{
	return              stdif->isConfigValid             (stdif->driver);
}


/** \} */
//----------------------------------------------------------------------------------------

#endif /* APPLBSTDIF_H */
