/**
 * \file AppGlobalDef.h
 * \brief Global definition
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup sourceCodeApplication_appglobaldef 7 - Global definition
 * \ingroup main_sourceCodeApplication
 *
 *
 *
 */
#ifndef APPGLOBALDEF_H_
#define APPGLOBALDEF_H_
#include "SysSe/Math/Ifx_AnalogInputF32.h"
#include "StdIf/IfxStdIf_DPipe.h"
#include "SysSe/Ext/At25xxx/Ifx_At25xxx.h"

#include "Qspi/SpiMaster/IfxQspi_SpiMaster.h"
#include "Evadc/Adc/IfxEvadc_Adc.h"
#include "SysSe/Ext/At25xxx/Ifx_Efs.h"
#include "configuration.h"

typedef enum
{
	AppModeSpecial_normal = 0,
	AppModeSpecial_hardwareDebugMode = 1,
	AppModeSpecial_hardwarePulseMode = 2,
	AppModeSpecial_hardwareReservedMode3 = 3,
	AppModeSpecial_hardwareReservedMode4 = 4,
	AppModeSpecial_hardwareReservedMode5 = 5,
	AppModeSpecial_hardwareReservedMode6 = 6,
	AppModeSpecial_unknown = 0xFF,

}AppModeSpecial;

typedef struct
{
    Ifx_AnalogInputF32 input;
    IfxEvadc_Adc_Channel hwInfo;
} AppAnalogInput;

typedef enum
{
    ECU_PositionSensor_encoder,
    ECU_PositionSensor_ad2s1210,
    ECU_PositionSensor_internalResolver0,
    ECU_PositionSensor_internalResolver1,
    ECU_PositionSensor_tle5012,				/* When the iGMR is used, the encoder is used for position sensing at run time and the iGMR is used to sync the absolute position at startup */
    ECU_PositionSensor_virtual,
    ECU_PositionSensor_none,
}ECU_PositionSensor;

typedef enum
{
    Lb_BoardType_undefined                = 0, /**<\brief Undefined board type */
    Lb_BoardType_HybridKit_LogicBoard     = 1, /**<\brief Logic board */
    Lb_BoardType_HybridKit_DriverBoardHp2 = 3, /**<\brief Driver board */
    Lb_BoardType_HybridKit_DriverBoardHpDrive = 4, /**<\brief HP Drive board with EiceSil */
    Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense = 5, /**<\brief HP Drive board with EiceSence */
    Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc = 6, /**<\brief HP Drive board with EiceSence and DSC module */

    Lb_BoardType_DataSize                         = 0x7F /**<\brief Ensure that sizeof(Lb_BoardType) is 1 byte  for file in EEPROM */
}Lb_BoardType;

/** \brief Board version
 * \<flags\>\<version\>\<revision\>\<step\> each value being 1 byte wide
 */
typedef enum
{
    LB_BoardVersion_undefined                        = 0x000000,   /**< \brief Logic board version undefined */

    LB_BoardVersion_HybridKit_LogicBoard_3_x         = 0x01030000, /**< \brief Logic board version 3.x */
    LB_BoardVersion_HybridKit_LogicBoard_3_0         = 0x00030000, /**< \brief Logic board version 3.0 */
    LB_BoardVersion_HybridKit_LogicBoard_3_1         = 0x00030100, /**< \brief Logic board version 3.1 */
    LB_BoardVersion_HybridKit_LogicBoard_3_3         = 0x00030300, /**< \brief Logic board version 3.3 */

    LB_BoardVersion_HybridKit_DriverBoardHp2_3_1     = 0x00030100, /**< \brief Driver board version 3.1 */
    LB_BoardVersion_HybridKit_DriverBoardHp2_3_2     = 0x00030200, /**< \brief Driver board version 3.2 */

    LB_BoardVersion_HybridKit_DriverBoardHpDrive_1_3 = 0x00010300, /**< \brief Driver board HPDrive with EiceSil version 1.3 */

    LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_0 = 0x00010000, /**< \brief Driver board HPDrive with EiceSence version 1.0 */
    LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_1 = 0x00010100, /**< \brief Driver board HPDrive with EiceSence version 1.1 */
    LB_BoardVersion_HybridKit_DriverBoardHpDriveSense_1_2 = 0x00010200, /**< \brief Driver board HPDrive with EiceSence version 1.2 */

    LB_BoardVersion_HybridKit_DriverBoardHpDriveSenseDsc_1_0 = 0x00010000, /**< \brief Driver board HPDrive with EiceSence, DSC version, version 1.0 */
    LB_BoardVersion_HybridKit_DriverBoardHpDriveSenseDsc_1_1 = 0x00010100, /**< \brief Driver board HPDrive with EiceSence, DSC version, version 1.1 */

    LB_BoardVersion_DataSize                         = 0x7FFFFFFF /**<\brief Ensure that sizeof(LB_BoardVersion) is 4 byte  for file in EEPROM */
} LB_BoardVersion;


/* FILE_VERSION_FLAG is used to ensure correct decoding of the file data. THis is required, because Tasking, Hightec, Dcc may not use the same data layout within a struct. */
#if defined(__DCC__)
#define FILE_VERSION_FLAG (0x20000000)
#elif defined(__GNUC__)
#define FILE_VERSION_FLAG (0x10000000)
#elif defined(__TASKING__)
#define FILE_VERSION_FLAG (0x00000000)
#endif

/** \brief File version
 * Format: \<00\>\<00\>\<version\>\<revision\>
 */
typedef enum
{
    Lb_FileVersion_undefined                              = 0x0000,    /**<\brief Undefined version */

    Lb_FileVersion_LogicBoardConfiguration_01_00          = (FILE_VERSION_FLAG | 0x0100),    /**<\brief \ref EFS_CFG_FILE_NAME_BOARD_CONFIGURATION version 1.00 */
    Lb_FileVersion_LogicBoardConfiguration_01_01          = (FILE_VERSION_FLAG | 0x0101),    /**<\brief \ref EFS_CFG_FILE_NAME_BOARD_CONFIGURATION version 1.01 */
    Lb_FileVersion_LogicBoardConfiguration_01_02          = (FILE_VERSION_FLAG | 0x0102),    /**<\brief \ref EFS_CFG_FILE_NAME_BOARD_CONFIGURATION version 1.02 */
    Lb_FileVersion_LogicBoardConfiguration_01_03          = (FILE_VERSION_FLAG | 0x0103),    /**<\brief \ref EFS_CFG_FILE_NAME_BOARD_CONFIGURATION version 1.03 */
    Lb_FileVersion_LogicBoardConfiguration_01_04          = (FILE_VERSION_FLAG | 0x0104),    /**<\brief \ref EFS_CFG_FILE_NAME_BOARD_CONFIGURATION version 1.04 */
    Lb_FileVersion_LogicBoardConfiguration_01_05          = (FILE_VERSION_FLAG | 0x0105),    /**<\brief \ref EFS_CFG_FILE_NAME_BOARD_CONFIGURATION version 1.05 */
    Lb_FileVersion_LogicBoardConfiguration_01_06          = (FILE_VERSION_FLAG | 0x0106),    /**<\brief \ref EFS_CFG_FILE_NAME_BOARD_CONFIGURATION version 1.06 */
    Lb_FileVersion_LogicBoardConfiguration_01_07          = (FILE_VERSION_FLAG | 0x0107),    /**<\brief \ref EFS_CFG_FILE_NAME_BOARD_CONFIGURATION version 1.07 */
    Lb_FileVersion_LogicBoardConfiguration_01_08          = (FILE_VERSION_FLAG | 0x0108),    /**<\brief \ref EFS_CFG_FILE_NAME_BOARD_CONFIGURATION version 1.08 */
    Lb_FileVersion_LogicBoardConfiguration_01_09          = (FILE_VERSION_FLAG | 0x0109),    /**<\brief \ref EFS_CFG_FILE_NAME_BOARD_CONFIGURATION version 1.09 */

    Lb_FileVersion_DriverBoard3xConfiguration_01_00       = (FILE_VERSION_FLAG | 0x0100),    /**<\brief \ref EFS_CFG_FILE_NAME_DRIVER_BOARD_CONFIGURATION version 1.00 */
    Lb_FileVersion_DriverBoard3xConfiguration_01_01       = (FILE_VERSION_FLAG | 0x0101),    /**<\brief \ref EFS_CFG_FILE_NAME_DRIVER_BOARD_CONFIGURATION version 1.01 */
    Lb_FileVersion_DriverBoard3xConfiguration_01_02       = (FILE_VERSION_FLAG | 0x0102),    /**<\brief \ref EFS_CFG_FILE_NAME_DRIVER_BOARD_CONFIGURATION version 1.02 */

    Lb_FileVersion_DriverBoardHPDSenseConfiguration_01_00 = (FILE_VERSION_FLAG | 0x0100),    /**<\brief \ref EFS_CFG_FILE_NAME_DRIVER_BOARD_CONFIGURATION version 1.00 */
    Lb_FileVersion_DriverBoardHPDSenseConfiguration_01_01 = (FILE_VERSION_FLAG | 0x0101),    /**<\brief \ref EFS_CFG_FILE_NAME_DRIVER_BOARD_CONFIGURATION version 1.01 */
    Lb_FileVersion_DriverBoardHPDSenseConfiguration_01_02 = (FILE_VERSION_FLAG | 0x0102),    /**<\brief \ref EFS_CFG_FILE_NAME_DRIVER_BOARD_CONFIGURATION version 1.02 */
    Lb_FileVersion_DriverBoardHPDSenseConfiguration_01_03 = (FILE_VERSION_FLAG | 0x0103),    /**<\brief \ref EFS_CFG_FILE_NAME_DRIVER_BOARD_CONFIGURATION version 1.03 */

    Lb_FileVersion_BoardVersion_01_00                     = 0x0100,    /**<\brief \ref EFS_CFG_FILE_NAME_BOARD_VERSION version 1.00 */

    LB_FileVersion_DataSize                               = 0x7FFFFFFF /**<\brief Ensure that sizeof(LB_FileVersion) is 4 byte  for file in EEPROM */
}LB_FileVersion;

typedef enum
{
    LB_PrimaryConsole_asc0, /**< \brief ASC0 is used for the primary console */
    LB_PrimaryConsole_can0, /**< \brief CAN0 is used for the primary console */
}LB_PrimaryConsole;

typedef struct
{
    LB_FileVersion  fileVersion;        /**< \brief File version */
    LB_BoardVersion boardVersion;       /**< \brief Board version. */
    Lb_BoardType    boardType;          /**< \brief Board type */
    struct
    {
        uint32 year : 16;
        uint32 month : 8;
        uint32 day : 8;
    }      productionDate
#if defined(__GNUC__)
    /* Required to get the same alignment as Tasking compiler */
    __attribute__ ((aligned(2)))
#endif
    ; /**< \brief Board production date */
    uint32 number;         /**< \brief Board number */
    struct
    {
        uint32 year : 16;
        uint32 month : 8;
        uint32 day : 8;
    }     TestDate;   /**< \brief Board test date */
    uint8 TestStatus; /**< \brief Board test status: 0:failed, 1 passed */
}
#if defined(__GNUC__)
/* Required to get the same alignment as Tasking compiler */
	__attribute__ ((packed))
#endif
LB_FileBoardVersion;

/** Configuration file minimal data
 *  A configuration file must have the fileVersion set as 1st data in the file
 */
typedef struct
{
    LB_FileVersion fileVersion;         /**< \brief File version */
}App_Configuration;

/** Module used to generate the PWM
 *
 */
typedef enum
{
    LB_PwmModule_gtmAtom = 0, /**< \brief use GTM ATOM to generate the PWM */
    LB_PwmModule_gtmTom  = 1, /**< \brief use GTM TOM to generate the PWM */
    LB_PwmModule_ccu6    = 2, /**< \brief use CCU6 to generate the PWM */
}LB_PwmModule;

/** Configuration file data structure
 *
 * If the data structure is modified, the \ref LB_FileVersion need to be enhanced, and the \ref EFS_CFG_FILE_VERSION_LOGIC_BOARD_CONFIGURATION
 * need to be updated.
 * Additionally LB_setupDefaultValue() must be updated
 */
typedef struct
{
    LB_FileVersion    fileVersion;      /**< \brief File version */
    boolean           valid;            /**< \brief TRUE if the configuration data are valid else FALSE */
    boolean           ready;            /**< \brief If TRUE, the software directly start the application after reset */
    LB_PrimaryConsole primaryConsole;   /**< \brief Primary console */
    uint8             verboseLevel;     /**< \brief Verbose level as defined by IFX_VERBOSE_LEVEL_* */

    struct
    {
        struct
    	{
            boolean	reversed;							  /**< \brief If TRUE, the position sensor direction is reversed */
            sint32 offset;								  /**< \brief Position sensor offset in ticks */
            sint32	resolution;							  /**< \brief Position sensor resolution in ticks */
    	}encoder;
    	struct
    	{
            sint32	resolution;							  /**< \brief Position sensor resolution in ticks */
            sint32 carrierFrequency;					  /**< \brief Carrier frequency in Hz */
            boolean	reversed;							  /**< \brief If TRUE, the position sensor direction is reversed */
            sint32 offset;								  /**< \brief Position sensor offset in ticks */
            uint16 periodPerRotation;				      /**< \brief Resolver signal period count per mechanical rotation */
            uint8 gainCode;							  /**< \brief Signal gain code See board specific values \ref LB30_ResolverGain \ref LB31_ResolverGain*/
    	}ad2s1210;
    	struct
    	{
            boolean	reversed;							  /**< \brief If TRUE, the position sensor direction is reversed */
            sint32 offset;								  /**< \brief Position sensor offset in ticks */
    	}tle5012;
    	struct
    	{
            sint32 carrierFrequency;					  /**< \brief Carrier frequency in Hz */
    		struct
    		{
                sint32	resolution;							  /**< \brief Position sensor resolution in ticks */
                boolean	reversed;							  /**< \brief If TRUE, the position sensor direction is reversed */
                sint32 offset;								  /**< \brief Position sensor offset in ticks */
                uint16 periodPerRotation;				      /**< \brief Resolver signal period count per mechanical rotation */
                sint32 signalAmplitudeMax;                    /**< \brief Max signal amplitude of cosine and sine in DSADC result unit */
                sint32 signalAmplitudeMin;                    /**< \brief Min signal amplitude of cosine and sine in DSADC result unit */
                uint8 gainCode;							  /**< \brief Signal gain code See board specific values \ref LB30_ResolverGain \ref LB31_ResolverGain*/
    		}input[2];
    	}aurixResolver;
    	/* FIXME add configuration for 2nd AURIX resolver */
    }positionSensors;
    /** Inverter configuration
     *
     */
    struct
    {
        ECU_PositionSensor positionSensor[2];
        LB_PwmModule       pwmModule;
        struct
        {
            boolean useThreeSensors;  /* \brief If TRUE, the 3rd phase current sensor is used */
            float32 gain;             /**< \brief Current sensor gain*/
            float32  offset;           /**< \brief Current sensor offset */
            float32 max;              /**< \brief Current error  threshold, value <= 0 disables the limit check */
            float32 cutOffFrequency;  /**< \brief Digital filter cut off frequency, 0 disables the filter */
        }currentSenor;
    }inverter[2];
    struct
    {
        float32 gain;             /**< \brief Analog input gain*/
        float32  offset;           /**< \brief Analog input offset */
        float32 min;              /**< \brief Analog input threshold min, min<=max disables the limit check */
        float32 max;              /**< \brief Analog input threshold max, min<=max disables the limit check */
        float32 cutOffFrequency;  /**< \brief Digital filter cut off frequency, 0 disables the filter */
    }analogInput[4];
    struct
    {
    	struct
    	{
        	boolean enabled;		/**< \brief Enables / disable the window watchdog */
        	float32 openWindowTime;  /**< \brief Watchdog open window time in s */
        	float32 closeWindowTime;  /**< \brief Watchdog close window time in s */
            boolean useWdiPin;		/**< \brief If TRUE, use the WDI pin to service the watchdog, else use the SPI */
    	}windowWatchdog;
    	struct
    	{
        	boolean enabled;	/**< \brief  Enables / disable the functional watchdog  */
        	float32 heartbeatTimerPeriod;  /**< \brief Watchdog heartbeat period in s */
        	float32 servicePeriod;  /**< \brief Watchdog service period in s */
    	}functionalWatchdog;
    	struct
    	{
        	boolean enabled;					/**< \brief  Enable / disable the Fault signaling protocol */
    	}fsp;
    	struct
    	{
    		boolean enabled;						/** \brief Enable / disable the IOM feature */
    		boolean fspOnFaultEnabled;				/** \brief Enable / disable the FSP on iom fault */
    		boolean nmiOnFaultEnabled;				/** \brief Enable / disable the NMI on iom fault */
    		float32 eventWindowThreshold;			/** \brief Event window threshold in second */
    		float32 filterTime;						/** \brief Pin input signal filter time for delay debounce filter */
    	}iom;
    }safety;
    struct
    {
        boolean enabled;   /**< \brief If TRUE, the wizard mode is enabled. */
        float32 vdcNom;    /**< \brief Define the nominal DC link voltage in V used during the wizard */
        float32 vf;        /**< \brief Define the V/F constant used for open loop during the wizard */
    }wizard;
    struct
    {
    	struct
    	{
    		boolean enabled;			/**< \brief Hardware pulse mode enable */
    		float32 pwmPeriod;			/**< \brief PWM period used as time base for the pulse generation. The generated pulse can't be bigger than this value */
        	float32 pulsePeriod;		/**< \brief Pulse period in s */
        	float32 startPulse;         /**< \brief Start pulse in s */
        	float32 pulseIncrement;     /**< \brief Pulse increment in s */
        	uint32 pulseCountPerPhase;  /**< \brief Number of pulse per phase per sequence */
    		uint8 sequence[6][6];		/**< \brief Pulse sequence definition. 1 is ON, 0 is OFF, 2 is pulse */
    	}hwPulseMode;
    }specialMode;
}LB_FileConfiguration;


/* FIXME INVERTER_FREQUENCY, DEAD_TIME, MIN_PULSE, Vdc min, vdc max, Vdc nom, current sensor gain should be part of the driver board config not logic board config */

#define EFS_CFG_FILE_NAME_BOARD_CONFIGURATION              "config.lb"
#define EFS_CFG_FILE_VERSION_LOGIC_BOARD_CONFIGURATION     (Lb_FileVersion_LogicBoardConfiguration_01_09)    /**< \brief currently supported file version for logic board configuration file  */

#define EFS_CFG_FILE_NAME_DRIVER_BOARD_CONFIGURATION       "config.lb"
#define EFS_CFG_FILE_VERSION_DRIVER_BOARD_3X_CONFIGURATION (Lb_FileVersion_DriverBoard3xConfiguration_01_02) /**< \brief currently supported file version for driver board configuration file */
#define EFS_CFG_FILE_VERSION_DRIVER_BOARD_HPDSENSE_CONFIGURATION (Lb_FileVersion_DriverBoardHPDSenseConfiguration_01_03) /**< \brief currently supported file version for driver board configuration file */

#define EFS_CFG_FILE_NAME_BOARD_VERSION                    "boardVersion.lb"
#define EFS_CFG_FILE_VERSION_BOARD_VERSION                 (Lb_FileVersion_BoardVersion_01_00)               /**< \brief currently supported file version for board version file */

#define EFS_CFG_FILE_NAME_MOTOR_CONFIGURATION              "motor.lb"


#define ECU_MOTOR_COUNT                          			(2) /**< \brief Number of motor. Note: currently only one motor is supported by the sw */
#define ECU_MOTOR_INDEX_0	(0)
#define ECU_MOTOR_INDEX_1	(1)
#define ECU_PHASE_PER_MOTOR_COUNT                          (3) /**< \brief Number of phase per motor */
#define ECU_GATEDRIVER_COUNT							   (6) /* 20200221 be created */

void    App_printBoardVersion(LB_FileBoardVersion *boardVersion, IfxStdIf_DPipe *io);
void    App_printStatusTitle(pchar title, ...);
void    App_printStatus(boolean status);
void    App_printQspiInitializationStatus(IfxQspi_SpiMaster_Config *config);
void    App_printAnalogInputConfig(Ifx_AnalogInputF32_Config *config);
boolean App_checkInput(Ifx_AnalogInputF32 *input);
boolean App_initEeprom(Ifx_At25xxx *eeprom, IfxQspi_SpiMaster *qspi, IfxQspi_SpiMaster_Channel *sscEeprom, const IfxQspi_Slso_Out *cs, float32 baudrate);
boolean App_detectEeprom(Ifx_At25xxx *eeprom);

boolean App_createEfs(Ifx_Efs *efs, Ifx_Efs_Address offset, Ifx_Efs_PartitionSize partitionSize, Ifx_Efs_FileSize defaultFileSize, boolean quiet);

boolean App_createBoardVersionFile(Ifx_Efs *efs, LB_FileBoardVersion *configuration, Lb_BoardType boardType, LB_BoardVersion version, boolean quiet);
boolean App_updateBoardVersionFile(Ifx_Efs *efs, LB_FileBoardVersion *configuration);
boolean App_loadBoardVersionFile(Ifx_Efs *efs, LB_FileBoardVersion *configuration);
boolean App_createConfigFile(Ifx_Efs *efs, pchar filename, void *configuration, uint16 fileSize, LB_FileVersion fileVersion, boolean quiet);
boolean App_updateConfigFile(Ifx_Efs *efs, pchar filename, void *configuration, uint16 fileSize);
boolean App_loadConfigFile(Ifx_Efs *efs, pchar filename, void *configuration, uint16 fileSize, LB_FileVersion fileVersion);
boolean protectEeprom(Ifx_At25xxx *eeprom);
boolean unprotectEeprom(Ifx_At25xxx *eeprom);

IFX_INLINE uint32 App_getBoardVersion(LB_BoardVersion version)
{
        return (version >> 16) & 0xFF;
}
IFX_INLINE uint32 App_getBoardRevision(LB_BoardVersion version)
{
    return (version >> 8) & 0xFF;
}

IFX_INLINE boolean App_isBoardVersionFamillyFlag(LB_BoardVersion version)
{
    return ((version >> 24) & 0x1) != 0;

}

#endif /* APPGLOBALDEF_H_ */
