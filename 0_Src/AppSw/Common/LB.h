/**
 * \file LB.h
 * \brief
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 *
 * \defgroup LB 1 - Logic board
 * \ingroup main_sourceCodeApplication
 *
 */

#ifndef LB_H
#define LB_H

#include "LogicBoard3_0/App/AppCanConsole.h"
#include "AppInterface.h"

#include "StdIf/IfxStdIf_Inverter.h"
#include "Can/Can/IfxCan_Can.h"
#include "Edsadc/Rdc/IfxDsadc_Rdc.h"
#include "AppGlobalDef.h"

#include "LogicBoard3_0/App/LB30.h"
#include "DriverBoard3x/App/Db3x.h"
#include "DriverBoardHPDSense/App/DbHPDSense.h"
#include "SysSe/EMotor/Ifx_MotorModelConfigF32.h"
#include "AppLbStdIf.h"

/** Common part to all logic board versions
 *
 */
typedef struct
{
    AppLbStdIf stdIf;
    union
    {
        LB30 logicBoard_30;              /**< \brief Logic board 3.x, only valid if a logic board 3.x is used */
    }                         logicboards;
    union
    {
        Db3x driverBoard_3x;			/**< \brief Driver board 3.x, only valid if a driver board 3.x is used */
        DbHPDSense driverBoard_HPDSense;		/**< \brief Driver board HP Drive Sense, only valid if a driver board HP Drive Sense is used */
    }                         driverBoards;
    AppDbStdIf driverBoardStdif;

    volatile Ifx_SRC_SRCR    *srcCpu[3]; /** \brief CPU[0..2] SRC register address */

    /** Common drivers to all logic boards
     *
     */
    LB_FileBoardVersion  boardVersion;
    struct
    {
        IfxQspi_SpiMaster         qspi0;        /**< \brief QSPI0 driver data.  */
        IfxQspi_SpiMaster_Channel sscEeprom;    /**< \brief SSC channel for EEPROM */
        Ifx_At25xxx               eeprom;       /**< Ifx_At25xxx driver */
        Ifx_Efs                   efsBoard;     /**< Ifx_At25xxx file system (Board)*/
        Ifx_Efs                   efsConfig;    /**< Ifx_At25xxx file system (configuration) */
        Ifx_EfsShell              efsShell;     /**< Ifx_At25xxx file system shell */
    }driver;

    Ifx_MotorModelConfigF32 motorConfiguration;
}LB;

typedef struct
{
    struct
    {
        IfxCpu_Perf      empty;
        IfxCpu_Perf      cosSinLookup;
        IfxCpu_Perf      clarke;
        IfxCpu_Perf      park;
        IfxCpu_Perf      reversePark;
        IfxCpu_Perf      piController;
        IfxCpu_Perf      svm;
        IfxCpu_Perf      foc;
        IfxCpu_Perf      rdc;
        IfxCpu_Perf      gtmpwm;
        volatile boolean ready;
    }benchmark;
}Cpu;

IFX_EXTERN LB   g_Lb;
IFX_EXTERN Cpu  g_Cpu0;
IFX_EXTERN Cpu  g_Cpu1;
IFX_EXTERN Cpu  g_Cpu2;
IFX_EXTERN Cpu *g_Cpus[3];

IFX_INLINE sint16 AnalogInput_getRawResult(AppAnalogInput *input)
{
    return (sint16)IfxEvadc_Adc_getResult(&input->hwInfo).B.RESULT;
}


void LB_printShellInfo(IfxStdIf_DPipe *io, LB_PrimaryConsole primaryConsole, LB_PrimaryConsole currentConsole);

void    LB_setupDefaultValue(LB_FileConfiguration *configuration);
void    LB_printBoardConfiguration(IfxStdIf_DPipe *io, LB_FileConfiguration *configuration, LB_FileBoardVersion *boardVersion, boolean all);

/* FIXME Obsolete use App_getBoardVersion() instead */
IFX_INLINE boolean Lb_isVersion3x(LB_FileBoardVersion *boardVersion)
{
    if (boardVersion == NULL_PTR)
    {
        return FALSE;
    }
    else
    {
        return ((boardVersion->boardVersion >> 16) & 0xFF) == 3;
    }
}


IFX_INLINE void Lb_raiseIsr(LB *board, IfxCpu_ResourceCpu index)
{
    IfxSrc_setRequest(board->srcCpu[index]);
}


void Lb_initBenchmark(LB *board);
LB_BoardVersion Lb_discoverLogicBoardVersion(LB *board);


#endif /* LB_H */
