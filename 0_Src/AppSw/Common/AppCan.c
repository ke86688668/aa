/**
 * \file AppCan.c
 * \brief System initialization and main program implementation.
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

#include "AppCan.h"
#include "Main.h"

Ifx_CanHandler_MessagePointer AppCan_initMo(IfxCan_Can_Node *node, IfxCan_MsgObjId msgObjId, boolean transmit, uint32 Id, Ifx_CanHandler_Interval interval, IfxCan_Can_MsgObj *msgObj, Ifx_CanHandler_Protocol protocol)
{
//    20191216 Jimmy have corrected
//    IfxCan_Can_MsgObjConfig msgCfg;
	IfxCan_Message				 msgCfg;
    Ifx_CanHandler_MessageConfig canHandlerMessageConfig;

//    IfxCan_Can_MsgObj_initConfig(&msgCfg, node);
    IfxCan_Can_initMessage(&msgCfg);
    Ifx_CanHandler_initMessageConfig(&canHandlerMessageConfig);

//    msgCfg.msgObjId  = msgObjId;
//    msgCfg.frame     = transmit ? IfxMultican_Frame_transmit : IfxMultican_Frame_receive;
//    msgCfg.messageId = Id;
//    IfxCan_Can_MsgObj_init(msgObj, &msgCfg);

    canHandlerMessageConfig.id        = Id;
    canHandlerMessageConfig.msgObj    = msgObj;
    canHandlerMessageConfig.interval  = interval;
    canHandlerMessageConfig.eventTime = 0;
    canHandlerMessageConfig.type      = transmit ? Ifx_CanHandler_MessageType_transmit : Ifx_CanHandler_MessageType_receive;
    canHandlerMessageConfig.protocol  = protocol;
    return Ifx_CanHandler_registerMessage(&g_App.can.handler, &canHandlerMessageConfig);
}


void AppCan_init(void)
{
    Ifx_CanHandler_Config canHandlerConfig;
    IfxCan_Can_Node *node = g_Lb.stdIf.canNodes[0];

    g_App.can.msgObjId = 0;
    Ifx_CanHandler_initConfig(&canHandlerConfig);
    Ifx_CanHandler_init(&g_App.can.handler, &canHandlerConfig);
    {
        Ifx_CanHandler_MessagePointer message;
        Ifx_CanHandler_FieldConfig    fieldConfig;
        Ifx_CanHandler_initFieldConfig(&fieldConfig);
        uint32                        i;

        message            = AppCan_initMo(node, g_App.can.msgObjId++, FALSE, APPCAN_MSG_ID_RX_MODECMD, 10, &g_App.can.msg.modeCmd, Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.inbox.modeCmd;
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.inbox.modeCmd) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.inbox.runCmd;
        fieldConfig.offset = 8;
        fieldConfig.size   = 0;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.inbox.clearErrorCmd;
        fieldConfig.offset = 9;
        fieldConfig.size   = 0;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.inbox.pulseMode;
        fieldConfig.offset = 10;
        fieldConfig.size   = 0;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.inbox.configExit;
        fieldConfig.offset = 11;
        fieldConfig.size   = 0;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, FALSE, APPCAN_MSG_ID_RX_MODECMD1, 10, &g_App.can.msg.modeCmd1, Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.inbox.currentIdCmd;
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.inbox.currentIdCmd) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.inbox.currentIqCmd;
        fieldConfig.offset = 32;
        fieldConfig.size   = sizeof(g_App.can.inbox.currentIqCmd) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, FALSE, APPCAN_MSG_ID_RX_MODECMD2A, 10, &g_App.can.msg.modeCmd2A, Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.inbox.currentCmd;
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.inbox.currentCmd) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.inbox.slipCmd;
        fieldConfig.offset = 32;
        fieldConfig.size   = sizeof(g_App.can.inbox.slipCmd) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, FALSE, APPCAN_MSG_ID_RX_MODECMD2S, 10, &g_App.can.msg.modeCmd2S, Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.inbox.torqueCmd;
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.inbox.torqueCmd) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, FALSE, APPCAN_MSG_ID_RX_MODECMD3, 10, &g_App.can.msg.modeCmd3, Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.inbox.speedCmd;
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.inbox.speedCmd) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, FALSE, APPCAN_MSG_ID_RX_MODECMD4, 10, &g_App.can.msg.modeCmd4, Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.inbox.amplitudeCmd;
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.inbox.amplitudeCmd) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        for (i = 0; i < 6; i++)
        {
            message            = AppCan_initMo(node, g_App.can.msgObjId++, FALSE, APPCAN_MSG_ID_RX_MODECMD5A + i, 10, &g_App.can.msg.modeCmd5[i], Ifx_CanHandler_Protocol_none);
            fieldConfig.data   = &g_App.can.inbox.pulseOffset[i];
            fieldConfig.offset = 0;
            fieldConfig.size   = sizeof(g_App.can.inbox.pulseOffset[i]) * 8;
            Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
            fieldConfig.data   = &g_App.can.inbox.pulseOn[i];
            fieldConfig.offset = 32;
            fieldConfig.size   = sizeof(g_App.can.inbox.pulseOn[i]) * 8;
            Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        }

        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_STAT1, 10, &g_App.can.msg.stat[0], Ifx_CanHandler_Protocol_none);
        g_App.can.messageStat1 = message;
        fieldConfig.data   = &g_App.can.outbox.mode;
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.outbox.mode) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.run;
        fieldConfig.offset = 8;
        fieldConfig.size   = 0;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.error;
        fieldConfig.offset = 9;
        fieldConfig.size   = 0;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.clearError;
        fieldConfig.offset = 10;
        fieldConfig.size   = 0;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.configExit;
        fieldConfig.offset = 11;
        fieldConfig.size   = 0;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.ecuReset;
        fieldConfig.offset = 12;
        fieldConfig.size   = 0;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.ecuState;
        fieldConfig.offset = 24;
        fieldConfig.size   = 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_STAT2, 10, &g_App.can.msg.stat[1], Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.outbox.torqueMeas;
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.outbox.torqueMeas) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.speedMeas;
        fieldConfig.offset = 32;
        fieldConfig.size   = sizeof(g_App.can.outbox.speedMeas) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_STAT3, 10, &g_App.can.msg.stat[2], Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.outbox.voltageDcMeas;
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.outbox.voltageDcMeas) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.current[2];
        fieldConfig.offset = 32;
        fieldConfig.size   = sizeof(g_App.can.outbox.current[2]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_STAT4, 100, &g_App.can.msg.stat[3], Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.outbox.tempIgbtMeas[0];
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.outbox.tempIgbtMeas[0]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.tempIgbtMeas[1];
        fieldConfig.offset = 32;
        fieldConfig.size   = sizeof(g_App.can.outbox.tempIgbtMeas[1]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_STAT5, 100, &g_App.can.msg.stat[4], Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.outbox.tempIgbtMeas[2];
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.outbox.tempIgbtMeas[2]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_STAT6, 100, &g_App.can.msg.stat[5], Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.outbox.tempMotorMeas;
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.outbox.tempMotorMeas) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.tempBoardMeas;
        fieldConfig.offset = 32;
        fieldConfig.size   = sizeof(g_App.can.outbox.tempBoardMeas) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_STAT7, 10, &g_App.can.msg.stat[6], Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.outbox.current[0];
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.outbox.current[0]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.current[1];
        fieldConfig.offset = 32;
        fieldConfig.size   = sizeof(g_App.can.outbox.current[1]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_STAT8, 10, &g_App.can.msg.stat[7], Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.outbox.elecPos;
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.outbox.elecPos) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.mechPos;
        fieldConfig.offset = 32;
        fieldConfig.size   = sizeof(g_App.can.outbox.mechPos) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_STAT9, 10, &g_App.can.msg.stat[8], Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.outbox.mechPosR;
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.outbox.mechPosR) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.mechSpeedR;
        fieldConfig.offset = 32;
        fieldConfig.size   = sizeof(g_App.can.outbox.mechSpeedR) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, FALSE, APPCAN_MSG_ID_RX_CONSOLE, 5, &g_App.can.msg.consoleRx, Ifx_CanHandler_Protocol_streamBasic);
        fieldConfig.data   = g_App.can.canConsole.rx;
        fieldConfig.offset = 0;
        fieldConfig.size   = 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_CONSOLE, 10 /*5*/, &g_App.can.msg.consoleTx, Ifx_CanHandler_Protocol_streamBasic);
        fieldConfig.data   = g_App.can.canConsole.tx;
        fieldConfig.offset = 0;
        fieldConfig.size   = 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
    }
}

void AppCan_initMessageCan1(void)
{
#if CFG_CURRENT_SENSOR_TEST == 1
    {
        Ifx_CanHandler_MessagePointer message;
        Ifx_CanHandler_FieldConfig    fieldConfig;
        Ifx_CanHandler_initFieldConfig(&fieldConfig);

        IfxMultican_Can_Node *node = g_Lb.stdIf.canNodes[1];
        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_LOG0, 1, &g_App.can.msg.log[0], Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.outbox.logCurrent[0][0];
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.outbox.logCurrent[0][0]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.logCurrent[1][0];
        fieldConfig.offset = 16;
        fieldConfig.size   = sizeof(g_App.can.outbox.logCurrent[1][0]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.logCurrentDiff[0];
        fieldConfig.offset = 32;
        fieldConfig.size   = sizeof(g_App.can.outbox.logCurrentDiff[0]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_LOG1, 1, &g_App.can.msg.log[1], Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.outbox.logCurrent[0][1];
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.outbox.logCurrent[0][1]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.logCurrent[1][1];
        fieldConfig.offset = 16;
        fieldConfig.size   = sizeof(g_App.can.outbox.logCurrent[1][1]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);

        message            = AppCan_initMo(node, g_App.can.msgObjId++, TRUE, APPCAN_MSG_ID_TX_LOG2, 1, &g_App.can.msg.log[2], Ifx_CanHandler_Protocol_none);
        fieldConfig.data   = &g_App.can.outbox.logCurrent[0][2];
        fieldConfig.offset = 0;
        fieldConfig.size   = sizeof(g_App.can.outbox.logCurrent[0][2]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
        fieldConfig.data   = &g_App.can.outbox.logCurrent[1][2];
        fieldConfig.offset = 16;
        fieldConfig.size   = sizeof(g_App.can.outbox.logCurrent[1][2]) * 8;
        Ifx_CanHandler_addMessageField(&g_App.can.handler, message, &fieldConfig);
    }
#endif

}
