/**
 * \file TempStator_KTY84_130.c
 * \brief Lookup functions for KTY84 motor stator temperature
 *
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * This file may be used, copied, and distributed, with or without modification, provided
 * that all copyright notices are retained; that all modifications to this file are
 * prominently noted in the modified file; and that this paragraph is not modified.
 */

//////////////////////////////////////////////////////////////////////////////////////////
// TEMPERATURE SENSOR  LOOK-UP TABLE /////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
#include "Configuration.h"
#include "TempStator_KTY84_130.h"

#define ADC_GAIN          (CFG_ADC_RESOLUTION / CFG_ADC_FULL_VOLTAGE)

#define TEMP_LOOKUP_ITEMS (26)

#define TEMP_0            (-40.0)
#define TEMP_1            (-30.0)
#define TEMP_2            (-20.0)
#define TEMP_3            (-10.0)
#define TEMP_4            (0.0)
#define TEMP_5            (10.0)
#define TEMP_6            (20.0)
#define TEMP_7            (25.0)
#define TEMP_8            (30.0)
#define TEMP_9            (40.0)
#define TEMP_10           (50.0)
#define TEMP_11           (60.0)
#define TEMP_12           (70.0)
#define TEMP_13           (80.0)
#define TEMP_14           (90.0)
#define TEMP_15           (100.0)
#define TEMP_16           (110.0)
#define TEMP_17           (120.0)
#define TEMP_18           (130.0)
#define TEMP_19           (140.0)
#define TEMP_20           (150.0)
#define TEMP_21           (160.0)
#define TEMP_22           (170.0)
#define TEMP_23           (180.0)
#define TEMP_24           (190.0)
#define TEMP_25           (200.0)

/* NOTE: this is with 1kOhm pull-up and 5V pull
 * Adc_RAW_x = ADC_GAIN * V_AIN */
#define Adc_RAW_0         (ADC_GAIN * 1.320824135)
#define Adc_RAW_1         (ADC_GAIN * 1.405463695)
#define Adc_RAW_2         (ADC_GAIN * 1.488764045)
#define Adc_RAW_3         (ADC_GAIN * 1.575342466)
#define Adc_RAW_4         (ADC_GAIN * 1.662216288)
#define Adc_RAW_5         (ADC_GAIN * 1.749024707)
#define Adc_RAW_6         (ADC_GAIN * 1.837444655)
#define Adc_RAW_7         (ADC_GAIN * 1.880848409)
#define Adc_RAW_8         (ADC_GAIN * 1.92496925)
#define Adc_RAW_9         (ADC_GAIN * 2.009569378)
#define Adc_RAW_10        (ADC_GAIN * 2.096399535)
#define Adc_RAW_11        (ADC_GAIN * 2.179921038)
#define Adc_RAW_12        (ADC_GAIN * 2.26177437)
#define Adc_RAW_13        (ADC_GAIN * 2.34325186)
#define Adc_RAW_14        (ADC_GAIN * 2.422680412)
#define Adc_RAW_15        (ADC_GAIN * 2.5)
#define Adc_RAW_16        (ADC_GAIN * 2.575169738)
#define Adc_RAW_17        (ADC_GAIN * 2.649271274)
#define Adc_RAW_18        (ADC_GAIN * 2.721057429)
#define Adc_RAW_19        (ADC_GAIN * 2.789566755)
#define Adc_RAW_20        (ADC_GAIN * 2.857754927)
#define Adc_RAW_21        (ADC_GAIN * 2.922725384)
#define Adc_RAW_22        (ADC_GAIN * 2.985495568)
#define Adc_RAW_23        (ADC_GAIN * 3.046875)
#define Adc_RAW_24        (ADC_GAIN * 3.106060606)
#define Adc_RAW_25        (ADC_GAIN * 3.163115356)

#define TEMP_GAIN(a, b)       ((TEMP_##a - TEMP_##b) / (Adc_RAW_##a - Adc_RAW_##b))
#define TEMP_OFFSET(a, b)     (TEMP_##a - (TEMP_GAIN(a, b) * Adc_RAW_##a))
#define TEMP_ADC_RAW_LU(a, b) {TEMP_GAIN(a, b), TEMP_OFFSET(a, b), Adc_RAW_##a}

static const Ifx_LutLinearF32_Item g_TempStatorLUItems[TEMP_LOOKUP_ITEMS] = {
    {0, TEMP_0, Adc_RAW_0},
    TEMP_ADC_RAW_LU(1, 0),
    TEMP_ADC_RAW_LU(2, 1),
    TEMP_ADC_RAW_LU(3, 2),
    TEMP_ADC_RAW_LU(4, 3),
    TEMP_ADC_RAW_LU(5, 4),
    TEMP_ADC_RAW_LU(6, 5),
    TEMP_ADC_RAW_LU(7, 6),
    TEMP_ADC_RAW_LU(8, 7),
    TEMP_ADC_RAW_LU(9, 8),
    TEMP_ADC_RAW_LU(10, 9),
    TEMP_ADC_RAW_LU(11, 10),
    TEMP_ADC_RAW_LU(12, 11),
    TEMP_ADC_RAW_LU(13, 12),
    TEMP_ADC_RAW_LU(14, 13),
    TEMP_ADC_RAW_LU(15, 14),
    TEMP_ADC_RAW_LU(16, 15),
    TEMP_ADC_RAW_LU(17, 16),
    TEMP_ADC_RAW_LU(18, 17),
    TEMP_ADC_RAW_LU(19, 18),
    TEMP_ADC_RAW_LU(20, 19),
    TEMP_ADC_RAW_LU(21, 20),
    TEMP_ADC_RAW_LU(22, 21),
    TEMP_ADC_RAW_LU(23, 22),
    TEMP_ADC_RAW_LU(24, 23),
    TEMP_ADC_RAW_LU(25, 24),
};

const Ifx_LutLinearF32             g_TempStatorLU = {
    TEMP_LOOKUP_ITEMS,
    g_TempStatorLUItems
};
