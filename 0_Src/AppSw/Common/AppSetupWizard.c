#include "AppSetupWizard.h"
#include "Common/Main.h"
#include "Ifx_Assert.h"
#include <stdio.h>

#include "Library/AppLibrary.h"
#include "Common/AppDbStdIf.h"

typedef char (*AppSetupWizard_JumpYesNo)(Ifx_Shell *shell, LB_FileConfiguration *configuration);
#define APPSETUPWINZARD_MAX_LINE_LENGTH (255)

void AppSetupWizard_init(Ifx_Shell *shell, IfxStdIf_DPipe *io)
{
    Ifx_Shell_Config config;
    Ifx_Shell_initConfig(&config);
    config.standardIo           = io;
    config.execute = FALSE;
    Ifx_Shell_init(shell, &config);
}

boolean AppSetupWizard_stopMotor(Ifx_TickTime timeout)
{
	Ifx_TickTime deadline = getDeadLine(timeout);

    Ifx_MotorControlF32_stop(&g_appLibrary.motorControl);
    while (Ifx_MotorControlF32_isRunning(&g_appLibrary.motorControl) && !isDeadLine(deadline)){}
    Ifx_MotorControlF32_setSpeed(&g_appLibrary.motorControl, 0);
    Ifx_MotorControlF32_setOpenLoopPosition(&g_appLibrary.motorControl, 0);

    return !Ifx_MotorControlF32_isRunning(&g_appLibrary.motorControl);
}

boolean AppSetupWizard_startMotor(Ifx_Shell *shell, Ifx_MotorControl_Mode mode, Ifx_TickTime timeout)
{
	IfxStdIf_DPipe *io = shell->io;
	Ifx_TickTime deadline = getDeadLine(timeout);

	if (Ifx_MotorControlF32_isRunning(&g_appLibrary.motorControl))
	{
	    Ifx_MotorControlF32_stop(&g_appLibrary.motorControl);
	    wait(TimeConst_1ms);
	    while (Ifx_MotorControlF32_isRunning(&g_appLibrary.motorControl) && !isDeadLine(deadline)){}
	}

	cfloat32 current;
	current.real = 0;
	current.imag = 0;
    Ifx_MotorControlF32_setSpeed(&g_appLibrary.motorControl, 0);
    Ifx_MotorControlF32_setCurrent(&g_appLibrary.motorControl, &current);
    Ifx_MotorControlF32_setTorque(&g_appLibrary.motorControl, 0);
    Ifx_MotorControlF32_setOpenLoopAmplitude(&g_appLibrary.motorControl, 0);
    Ifx_MotorControlF32_setOpenLoopPosition(&g_appLibrary.motorControl, 0);

    Ifx_MotorControlF32_setMode(&g_appLibrary.motorControl, mode);
    wait(TimeConst_1ms);

    Ifx_MotorControlF32_start(&g_appLibrary.motorControl);

    while (!Ifx_MotorControlF32_isRunning(&g_appLibrary.motorControl) && !isDeadLine(deadline)){}

    if (Ifx_MotorControlF32_isRunning(&g_appLibrary.motorControl) && (Ifx_MotorControlF32_getMode(&g_appLibrary.motorControl) == mode))
    {
        return TRUE;

    }
    else
    {
        IfxStdIf_DPipe_print(io, "Error, can't start the motor"ENDL);
    	return FALSE;
    }
}

sint32 AppSetupWizard_ask(Ifx_Shell *shell, pchar text, pchar prefill)
{
	IfxStdIf_DPipe *io = shell->io;
	sint32 key;

    IfxStdIf_DPipe_print(io, text);
    Ifx_Shell_prefill(shell, prefill);
    while((key=Ifx_Shell_process(shell)) == IFX_SHELL_KEY_NULL);
    return key;
}


/** Ask the question until one of the option is selected or CTRL+C pressed
 *
 * \param defaultValue default value to be displayed. If -1, no default is shown
 * \param answers array of options. last element must be an empty string
 * \retval -1 if CTRL+C is pressed
 * \retval 0..255 if an option is selected
 * FIXME enable value/key selection
 */
sint32 AppSetupWizard_askOptions(Ifx_Shell *shell, pchar text, sint32 defaultValue, sint32 prefill, pchar answers[])
{
	char string[APPSETUPWINZARD_MAX_LINE_LENGTH];
	char temp[APPSETUPWINZARD_MAX_LINE_LENGTH];

	pchar*cmd = (pchar*)&shell->cmdHistory[0];
	sint32 i;

	IFX_ASSERT(IFX_VERBOSE_LEVEL_ERROR, answers[0][0]!= 0x00);

	strncpy(string, text, APPSETUPWINZARD_MAX_LINE_LENGTH);

	if(defaultValue >=0)
	{
		strncat(string, " (default=%s)", APPSETUPWINZARD_MAX_LINE_LENGTH);
	    snprintf(temp, APPSETUPWINZARD_MAX_LINE_LENGTH, string, answers[defaultValue]);
		strncpy(string, temp, APPSETUPWINZARD_MAX_LINE_LENGTH);
	}

	strncat(string, " [", APPSETUPWINZARD_MAX_LINE_LENGTH);

	i=0;
    while (answers[i][0]!= 0x00)
	{
    	strncat(string, "%s/", APPSETUPWINZARD_MAX_LINE_LENGTH);
	    snprintf(temp, APPSETUPWINZARD_MAX_LINE_LENGTH, string, answers[i]);
		strncpy(string, temp, APPSETUPWINZARD_MAX_LINE_LENGTH);
		i++;
	}
    string[strlen(string)-1] = 0; // Delete the last '/'
	strncat(string, "]? ", APPSETUPWINZARD_MAX_LINE_LENGTH);


	do
	{
		if (AppSetupWizard_ask(shell, string, answers[prefill]) == IFX_SHELL_KEY_CTRL_C)
		{
			return -1;
		}

		i=0;
		while (answers[i][0]!= 0x00)
		{
			if (Ifx_Shell_matchToken(cmd, answers[i]) != FALSE)
		    {
				return i;
		    }

			i++;
		}
	}while(1);
}

/* Return TRUE if continue else FALSE*/
boolean AppSetupWizard_continue(Ifx_Shell *shell)
{
	pchar answers[3] = {"y", "n", ""};
	sint32 option;
	option = AppSetupWizard_askOptions(shell, "Continue", -1, 0, answers);
	if (option == -1)
	{
		return FALSE;
	}
    return option == 0;
}

/** Ask the question until a uint value is entered or CTRL+C pressed
 *
 * \retval -1 if CTRL+C is pressed
 * \retval 0 a value has been entered
 *
 */
sint32 AppSetupWizard_askUint32(Ifx_Shell *shell, pchar text, uint32 defaultValue, uint32 min, uint32 max, uint32 *value)
{
	char string[APPSETUPWINZARD_MAX_LINE_LENGTH];
	char temp[APPSETUPWINZARD_MAX_LINE_LENGTH];

	pchar*cmd = (pchar*)&shell->cmdHistory[0];

	strncpy(string, text, APPSETUPWINZARD_MAX_LINE_LENGTH);

	strncat(string, " (default=%d)", APPSETUPWINZARD_MAX_LINE_LENGTH);
    snprintf(temp, APPSETUPWINZARD_MAX_LINE_LENGTH, string, defaultValue);
	strncpy(string, temp, APPSETUPWINZARD_MAX_LINE_LENGTH);


	if (max > min)
	{
		strncat(string, " [%d, %d]? ", APPSETUPWINZARD_MAX_LINE_LENGTH);
	    snprintf(temp, APPSETUPWINZARD_MAX_LINE_LENGTH, string, min, max);
		strncpy(string, temp, APPSETUPWINZARD_MAX_LINE_LENGTH);
	}
	else
	{
		strncat(string, "? ", APPSETUPWINZARD_MAX_LINE_LENGTH);
	}


    snprintf(temp, APPSETUPWINZARD_MAX_LINE_LENGTH, "%d", (unsigned int)*value);

	do
	{

		if (AppSetupWizard_ask(shell, string, temp) == IFX_SHELL_KEY_CTRL_C)
		{
			return -1;
		}

		uint32 tmp;
        if (Ifx_Shell_parseUInt32(cmd, &tmp, FALSE))
		{
        	if ((max<=min) || ((tmp >=min) && (tmp<=max)))
			{
        		*value = tmp;
    			return 0;
			}
		}
	}while(1);
}
sint32 AppSetupWizard_askSint32(Ifx_Shell *shell, pchar text, sint32 defaultValue, sint32 min, sint32 max, sint32 *value)
{
	char string[APPSETUPWINZARD_MAX_LINE_LENGTH];
	char temp[APPSETUPWINZARD_MAX_LINE_LENGTH];

	pchar*cmd = (pchar*)&shell->cmdHistory[0];

	strncpy(string, text, APPSETUPWINZARD_MAX_LINE_LENGTH);

	strncat(string, " (default=%d)", APPSETUPWINZARD_MAX_LINE_LENGTH);
    snprintf(temp, APPSETUPWINZARD_MAX_LINE_LENGTH, string, defaultValue);
	strncpy(string, temp, APPSETUPWINZARD_MAX_LINE_LENGTH);


	if (max > min)
	{
		strncat(string, " [%d, %d]? ", APPSETUPWINZARD_MAX_LINE_LENGTH);
	    snprintf(temp, APPSETUPWINZARD_MAX_LINE_LENGTH, string, min, max);
		strncpy(string, temp, APPSETUPWINZARD_MAX_LINE_LENGTH);
	}
	else
	{
		strncat(string, "? ", APPSETUPWINZARD_MAX_LINE_LENGTH);
	}


    snprintf(temp, APPSETUPWINZARD_MAX_LINE_LENGTH, "%d", (unsigned int)*value);

	do
	{

		if (AppSetupWizard_ask(shell, string, temp) == IFX_SHELL_KEY_CTRL_C)
		{
			return -1;
		}

		sint32 tmp;
        if (Ifx_Shell_parseSInt32(cmd, &tmp))
		{
        	if ((max<=min) || ((tmp >=min) && (tmp<=max)))
			{
        		*value = tmp;
    			return 0;
			}
		}
	}while(1);
}

sint32 AppSetupWizard_askUint8(Ifx_Shell *shell, pchar text, uint8 defaultValue, uint8 min, uint8 max, uint8 *value)
{
	sint32 result;
	uint32 tmp = *value;

	result =  AppSetupWizard_askUint32(shell, text, defaultValue, min, max, &tmp);
	if (result==0)
	{
		*value = (uint8)tmp;
	}

	return result;

}

sint32 AppSetupWizard_askBoolean(Ifx_Shell *shell, pchar text, boolean defaultValue, boolean *value)
{
	pchar answers[3] = {"y", "n", ""};
	sint32 option;

	option = AppSetupWizard_askOptions(shell,text, defaultValue ? 0:1, *value?0:1, answers);
	if (option == -1)
	{
		return -1;
	}
	else
	{
		*value = option == 0;
	    return 0;
    }
}

sint32 AppSetupWizard_askUint16(Ifx_Shell *shell, pchar text, uint16 defaultValue, uint16 min, uint16 max, uint16 *value)
{
	sint32 result;
	uint32 tmp = *value;

	result =  AppSetupWizard_askUint32(shell, text, defaultValue, min, max, &tmp);
	if (result==0)
	{
		*value = (uint16)tmp;
	}

	return result;

}

/** Ask the question until a float value is entered or CTRL+C pressed
 *
 * \retval -1 if CTRL+C is pressed
 * \retval 0 a value has been entered
 *
 */
sint32 AppSetupWizard_askFloat32(Ifx_Shell *shell, pchar text, float32 defaultValue, float32 min, float32 max, float32 *value)
{
	char string[APPSETUPWINZARD_MAX_LINE_LENGTH];
	char temp[APPSETUPWINZARD_MAX_LINE_LENGTH];

	pchar*cmd = (pchar*)&shell->cmdHistory[0];

	strncpy(string, text, APPSETUPWINZARD_MAX_LINE_LENGTH);

	strncat(string, " (default=%f)", APPSETUPWINZARD_MAX_LINE_LENGTH);
    snprintf(temp, APPSETUPWINZARD_MAX_LINE_LENGTH, string, defaultValue);
	strncpy(string, temp, APPSETUPWINZARD_MAX_LINE_LENGTH);

	if (max > min)
	{
		strncat(string, " [%f, %f]? ", APPSETUPWINZARD_MAX_LINE_LENGTH);
	    snprintf(temp, APPSETUPWINZARD_MAX_LINE_LENGTH, string, min, max);
		strncpy(string, temp, APPSETUPWINZARD_MAX_LINE_LENGTH);
	}
	else
	{
		strncat(string, "? ", APPSETUPWINZARD_MAX_LINE_LENGTH);
	}


    snprintf(temp, APPSETUPWINZARD_MAX_LINE_LENGTH, "%f", *value);

	do
	{

		if (AppSetupWizard_ask(shell, string, temp) == IFX_SHELL_KEY_CTRL_C)
		{
			return -1;
		}

		float32 tmp;
        if (Ifx_Shell_parseFloat32(cmd, &tmp))
		{
        	if ((max<=min) || ((tmp >=min) && (tmp<=max)))
			{
        		*value = tmp;
    			return 0;
			}
		}
	}while(1);
}


sint32 AppSetupWizard_jumpYesNo(Ifx_Shell *shell, const char *text, AppSetupWizard_JumpYesNo jump, LB_FileConfiguration *configuration)
{
	sint32 key;
	pchar*cmd = (pchar*)&shell->cmdHistory[0];

	do
    {
        key = AppSetupWizard_ask(shell, text, "");
        if (key == IFX_SHELL_KEY_CTRL_C)
        {
        	return IFX_SHELL_KEY_CTRL_C;
        }
        else if (Ifx_Shell_matchToken(cmd, "y"))
        {
        	return jump(shell, configuration);
        }
        else if (Ifx_Shell_matchToken(cmd, "n"))
        {
        	return IFX_SHELL_KEY_NULL;
        }
        else
        {
        }

    }while(1);

}


char AppSetupWizard_checkDcLinkVoltage(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
    /*************************************************************************/
    /* Check DC-link voltage  */
    {
		Lb_BoardType DriverBoardType = AppDbStdIf_getBoardVersion(&g_Lb.driverBoardStdif)->boardType;
		if (
				(DriverBoardType == Lb_BoardType_HybridKit_DriverBoardHpDrive)
				|| (DriverBoardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense)
				|| (DriverBoardType == Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc)
				)
		{

			boolean error;
			do
			{
				error = FALSE;
				float32 vdc = IfxStdIf_Inverter_getVdc(g_Lb.stdIf.inverter);;

				if(vdc < configuration->wizard.vdcNom*0.9)
				{
					error = TRUE;
				    IfxStdIf_DPipe_print(io, "ERROR: DC-link voltage too low, measured %f V, expected %f V, please check"ENDL, vdc, configuration->wizard.vdcNom);
				    if (!AppSetupWizard_continue(shell))
				    {
						return IFX_SHELL_KEY_CTRL_C;
				    }
				}
				else if(vdc > configuration->wizard.vdcNom*1.9)
				{
					error = TRUE;
				    IfxStdIf_DPipe_print(io, "ERROR: DC-link voltage too high, measured %f V, expected f V, please check"ENDL, vdc, configuration->wizard.vdcNom);
				    if (!AppSetupWizard_continue(shell))
				    {
						return IFX_SHELL_KEY_CTRL_C;
				    }
				}
				else
				{
				    IfxStdIf_DPipe_print(io, "DC-link voltage: %f V"ENDL, vdc);
				}
			}while(error);


		}
		else
		{ /* No DC-link available */

		}
    }

    return IFX_SHELL_KEY_NULL;
}


char AppSetupWizard_goToRun(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;

    { /* Go to run state */
//        IfxStdIf_DPipe_print(io, "Exit configuration state..."ENDL);
        g_App.hwState.hwModeRequest = AppMode_hwInitialization;
        while (1)
        {
            AppStateMachine_process(&g_App.hwState);
        	if (g_App.hwState.hwMode == AppMode_run)
        	{ /* Run state entered */
//                IfxStdIf_DPipe_print(io, "Run state entered"ENDL);
        		break;
        	}
        	else if (g_App.hwState.hwMode == AppMode_error)
        	{ /* Error state entered */
//                IfxStdIf_DPipe_print(io, "Error state entered"ENDL);
            	return IFX_SHELL_KEY_CTRL_C;
        	}
        	else if ((g_App.hwState.hwMode == AppMode_configuration) && (g_App.hwState.hwModeRequest == AppMode_configuration))
        	{ /* Could not exit configuration state */
//                IfxStdIf_DPipe_print(io, "Can't exit configuration state"ENDL);
            	return IFX_SHELL_KEY_CTRL_C;
        	}
        }
    }

	return IFX_SHELL_KEY_NULL;

}
/* API prototype match AppSetupWizard_JumpYesNo */
char AppSetupWizard_inverterPreInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
	LB_FileConfiguration defaultConfig;
	LB_setupDefaultValue(&defaultConfig);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                           Inverter wizard (Pre initialization)               -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
    /*************************************************************************/
    /* Inverter PWM frequency in Hz */
	if (AppDbStdIf_setupWizard(&g_Lb.driverBoardStdif, shell) == IFX_SHELL_KEY_CTRL_C)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}

//	IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                           Inverter wizard end                                -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
	return IFX_SHELL_KEY_NULL;
}
/* API prototype match AppSetupWizard_JumpYesNo */
char AppSetupWizard_inverterPostInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                           Inverter wizard (post initialization)              -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
    /*************************************************************************/
    if (AppSetupWizard_checkDcLinkVoltage(shell, configuration) == IFX_SHELL_KEY_CTRL_C)
    {
		return IFX_SHELL_KEY_CTRL_C;
    }

//	IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                           Inverter wizard end                                -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
	return IFX_SHELL_KEY_NULL;
}
/* FIXME add driver board wizard */

/* API prototype match AppSetupWizard_JumpYesNo */
char AppSetupWizard_motorPreInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
    Ifx_MotorModelConfigF32 defaultMotorConfig;
    Ifx_MotorModelConfigF32_setupDefaultValue(&defaultMotorConfig);

//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                              Motor wizard (Pre initialization)                -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
    /*************************************************************************/
    /* Motor type selection  */
    {
    	pchar answers[3] = {"pmsm", "acim", ""};
    	sint32 option;
    	sint32 defaultOption;
        option =
        		(g_Lb.motorConfiguration.data.type == Ifx_F32_MotorModel_Type_pmsmStandard ? 0
        			   : 0);
        defaultOption =
        		(defaultMotorConfig.data.type == Ifx_F32_MotorModel_Type_pmsmStandard ? 0
        			   : 0);
    	option = AppSetupWizard_askOptions(shell, "Motor type", defaultOption, option, answers);
    	if (option == -1)
    	{
    		return IFX_SHELL_KEY_CTRL_C;
    	}
    	else if (option == 0)
        {
    		g_Lb.motorConfiguration.data.type = Ifx_F32_MotorModel_Type_pmsmStandard;
        }
    }
    /*************************************************************************/
    /* Motor pole pair count */
	if (AppSetupWizard_askUint8(shell, "Motor pole pair count", defaultMotorConfig.data.polePair, 0, 0, &g_Lb.motorConfiguration.data.polePair) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    /*************************************************************************/
    /* Motor max phase current in A */
	if (AppSetupWizard_askFloat32(shell, "Motor max phase current in A", defaultMotorConfig.data.iMax, 0, 0, &g_Lb.motorConfiguration.data.iMax) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}

    /*************************************************************************/
    /* Motor max mechanical speed in rpm */
	if (AppSetupWizard_askFloat32(shell, "Motor max mechanical speed in rpm", defaultMotorConfig.data.speedMax, 0, 0, &g_Lb.motorConfiguration.data.speedMax) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}

	if ((g_Lb.motorConfiguration.data.type == Ifx_F32_MotorModel_Type_pmsmStandard) || (g_Lb.motorConfiguration.data.type == Ifx_F32_MotorModel_Type_pmsmSalient))
	{
	    /*************************************************************************/
	    /* Motor stator phase resistance in Ohm */
		if (AppSetupWizard_askFloat32(shell, "Motor stator phase resistance in Ohm", defaultMotorConfig.data.rs, 0, 0, &g_Lb.motorConfiguration.data.rs) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}

		if (g_Lb.motorConfiguration.data.type == Ifx_F32_MotorModel_Type_pmsmStandard)
		{
			/* Motor phase inductance in H */
			if (AppSetupWizard_askFloat32(shell, "Motor phase inductance in H", defaultMotorConfig.data.ld, 0, 0, &g_Lb.motorConfiguration.data.ld) == -1)
			{
				return IFX_SHELL_KEY_CTRL_C;
			}
			else
			{
				g_Lb.motorConfiguration.data.lq = g_Lb.motorConfiguration.data.ld;
			}
		}
		else
		{
			/* Motor phase inductance Ld in H */
			if (AppSetupWizard_askFloat32(shell, "Motor phase inductance Ld in H", defaultMotorConfig.data.ld, 0, 0, &g_Lb.motorConfiguration.data.ld) == -1)
			{
				return IFX_SHELL_KEY_CTRL_C;
			}

			/* Motor phase inductance Lq in H */
			if (AppSetupWizard_askFloat32(shell, "Motor phase inductance Lq in H", defaultMotorConfig.data.lq, 0, 0, &g_Lb.motorConfiguration.data.lq) == -1)
			{
				return IFX_SHELL_KEY_CTRL_C;
			}

		}


		/* Motor phase stall current in A */
		if (AppSetupWizard_askFloat32(shell, "Motor phase stall current in A", defaultMotorConfig.data.iStall, 0, 0, &g_Lb.motorConfiguration.data.iStall) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}

		/* Motor torque constant in Nm/Arms */
		if (AppSetupWizard_askFloat32(shell, "Motor torque constant in Nm/Arms", defaultMotorConfig.data.kt, 0, 0, &g_Lb.motorConfiguration.data.kt) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}

	}
	else if (g_Lb.motorConfiguration.data.type == Ifx_F32_MotorModel_Type_acim)
	{
			/* FIXME TBD*/
	}

//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                              Motor wizard end                                -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
	return IFX_SHELL_KEY_NULL;
}

void AppSetupWizard_setDutyCycleAndSpeed(float32 rpm)
{
	float32 vdc = IfxStdIf_Inverter_getVdc(g_appLibrary.motorControl.inverter);
	float32 dutyCycle;
    Ifx_MotorControlF32_setSpeed(&g_appLibrary.motorControl, rpm);

    if (rpm > 20)
    {
    	dutyCycle = g_Lb.stdIf.configuration->wizard.vf * rpm/1000 / vdc;
        dutyCycle = __minf(__maxf(dutyCycle, 0), 1.0);
    }
    else
    {
        IfxStdIf_MotorModelF32_Param param = IfxStdIf_MotorModelF32_getParam(g_appLibrary.motorControl.motorModel);

        /* Use the 40% of Istall current
         */
        dutyCycle = (param.rs * 1.5 * param.iStall * 0.4) / vdc;
    }

    Ifx_MotorControlF32_setOpenLoopAmplitude(&g_appLibrary.motorControl, dutyCycle);
}

/* API prototype match AppSetupWizard_JumpYesNo */
char AppSetupWizard_currentSensorPreInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
	LB_FileConfiguration defaultConfig;

	LB_setupDefaultValue(&defaultConfig);

//	IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                     Current sensor wizard (Pre initialization)               -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);

    /*************************************************************************/
    /* Number of current sensors */
    {
    	uint8 count= configuration->inverter[0].currentSenor.useThreeSensors ? 3 : 2;
    	if (AppSetupWizard_askUint8(shell, "Number of current sensors", defaultConfig.inverter[0].currentSenor.useThreeSensors ? 3 : 2, 0, 0, &count) == -1)
    	{
    		return IFX_SHELL_KEY_CTRL_C;
    	}
    	else
    	{
    		configuration->inverter[0].currentSenor.useThreeSensors = count == 3;
    	}
    }
    /*************************************************************************/
    /* Current sensor gain in A/V */
    {
    	float32 gain = configuration->inverter[0].currentSenor.gain / CFG_ADC_GAIN;
    	if (AppSetupWizard_askFloat32(shell, "Current sensor gain in A/V", defaultConfig.inverter[0].currentSenor.gain / CFG_ADC_GAIN, 0, 0, &gain) == -1)
    	{
    		return IFX_SHELL_KEY_CTRL_C;
    	}
    	else
    	{
    		configuration->inverter[0].currentSenor.gain = gain * CFG_ADC_GAIN;
    	}
    }

    /*************************************************************************/
    /* Current sensor offset in V */
    {
    	float32 offset = -configuration->inverter[0].currentSenor.offset * CFG_ADC_GAIN;
    	if (AppSetupWizard_askFloat32(shell, "Current sensor offset in V", -defaultConfig.inverter[0].currentSenor.offset*CFG_ADC_GAIN, 0, 0, &offset) == -1)
    	{
    		return IFX_SHELL_KEY_CTRL_C;
    	}
    	else
    	{
    		configuration->inverter[0].currentSenor.offset = -offset / CFG_ADC_GAIN;
    	}
    }
    /*************************************************************************/
    /* Maximum allowed phase current in A */
	if (AppSetupWizard_askFloat32(shell, "Maximum allowed phase current in A", defaultConfig.inverter[0].currentSenor.max, 0, 0, &configuration->inverter[0].currentSenor.max) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
    /*************************************************************************/
    /* Current sensor digital low pass filter cut off frequency in Hz */
	if (AppSetupWizard_askFloat32(shell, "Current sensor digital low pass filter cut off frequency in Hz. 0 disables the filter", defaultConfig.inverter[0].currentSenor.cutOffFrequency, 0, 0, &configuration->inverter[0].currentSenor.cutOffFrequency) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}

//	IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                     Current sensor wizard end                                -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
	return IFX_SHELL_KEY_NULL;
}

/* API prototype match AppSetupWizard_JumpYesNo */
char AppSetupWizard_currentSensorPostInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
	boolean error;
//	IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                     Current sensor wizard (Post initialization)               -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the motor phases are connected"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the current sensors are connected"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the motor parameters are properly configured (stator phase resistance, Stall current)"ENDL);

	do
	{
    	error = FALSE;
//	    IfxStdIf_DPipe_print(io, "INFO:Switch the DC-link voltage ON now. (Low voltage and current limitation recommended)"ENDL);
//	    IfxStdIf_DPipe_print(io, "INFO:WARNING: Voltage will be applied to the motor phases, motor may move during the setup!"ENDL);

	    if (!AppSetupWizard_continue(shell))
	    {
			return IFX_SHELL_KEY_CTRL_C;
	    }

	    if (AppSetupWizard_checkDcLinkVoltage(shell, configuration) ==IFX_SHELL_KEY_CTRL_C)
	    {
			return IFX_SHELL_KEY_CTRL_C;
	    }

    	Ifx_MotorControlF32_resetVirtualPositionSensor(&g_appLibrary.motorControl);
	    if (AppSetupWizard_startMotor(shell, Ifx_MotorControl_Mode_openLoop, TimeConst_100ms))
	    {

	    	float32 i0[3];
	    	float32 i1[3];
	    	float32 i2[3];

	        IfxStdIf_MotorModelF32_Param param = IfxStdIf_MotorModelF32_getParam(g_appLibrary.motorControl.motorModel);

	        /* Expected current 30% of Istall. Allow +/- 20% error */

	    	wait(TimeConst_1s);
            AppSetupWizard_setDutyCycleAndSpeed(1);

	        while (Ifx_MotorControlF32_getPosition(&g_appLibrary.motorControl) < 2.0*IFX_PI/3.0/param.polePair){}
	        IfxStdIf_Inverter_getPhaseCurrents(g_appLibrary.motorControl.inverter, i1);

	        while (Ifx_MotorControlF32_getPosition(&g_appLibrary.motorControl) < 4.0*IFX_PI/3.0/param.polePair){}
	        IfxStdIf_Inverter_getPhaseCurrents(g_appLibrary.motorControl.inverter, i2);

	        while (Ifx_MotorControlF32_getPosition(&g_appLibrary.motorControl) < 2.0*IFX_PI /param.polePair){}
	        IfxStdIf_Inverter_getPhaseCurrents(g_appLibrary.motorControl.inverter, i0);
            AppSetupWizard_setDutyCycleAndSpeed(0);

        	AppSetupWizard_stopMotor(TimeConst_100ms);

//        	IfxStdIf_DPipe_print(io, "Current 0PI/3: Iu=%f A  ; Iv=%f A  ; Iw=%f A  ;"ENDL, i0[0], i0[1], i0[2]);
//			IfxStdIf_DPipe_print(io, "Current 2PI/3: Iu=%f A  ; Iv=%f A  ; Iw=%f A  ;"ENDL, i1[0], i1[1], i1[2]);
//			IfxStdIf_DPipe_print(io, "Current 4PI/3: Iu=%f A  ; Iv=%f A  ; Iw=%f A  ;"ENDL, i2[0], i2[1], i2[2]);


			if (!((__absf(i0[0]) > __absf(i0[1]) && (__absf(i0[0]) > __absf(i0[2])))))
			{
//				IfxStdIf_DPipe_print(io, "ERROR: Phase current sensor mismatch phase U. Switch DC-link voltage OFF, check sensor connection."ENDL);
				error = TRUE;
			}

			if (!((__absf(i1[1]) > __absf(i1[0]) && (__absf(i1[1]) > __absf(i1[2])))))
			{
//				IfxStdIf_DPipe_print(io, "ERROR: Phase current sensor mismatch phase V. Switch DC-link voltage OFF, check sensor connection."ENDL);
				error = TRUE;
			}

			if (!((__absf(i2[2]) > __absf(i2[0]) && (__absf(i2[2]) > __absf(i2[1])))))
			{
//				IfxStdIf_DPipe_print(io, "ERROR: Phase current sensor mismatch phase W. Switch DC-link voltage OFF, check sensor connection."ENDL);
				error = TRUE;
			}

			if (error)
	        {
	    	    if (!AppSetupWizard_continue(shell))
	    	    {
	    			return IFX_SHELL_KEY_CTRL_C;
	    	    }
	        }

			if (!(i0[0] > 0))
			{
//				IfxStdIf_DPipe_print(io, "ERROR: Phase sensor direction error for phase U. Switch DC-link voltage OFF, check sensor connection."ENDL);
				error = TRUE;
			}
			if (!(i1[1] > 0))
			{
//				IfxStdIf_DPipe_print(io, "ERROR: Phase sensor direction error for phase V. Switch DC-link voltage OFF, check sensor connection."ENDL);
				error = TRUE;
			}
			if (!(i2[2] > 0))
			{
//				IfxStdIf_DPipe_print(io, "ERROR: Phase sensor direction error for phase W. Switch DC-link voltage OFF, check sensor connection."ENDL);
				error = TRUE;
			}

	        if (error)
	        {
	    	    if (!AppSetupWizard_continue(shell))
	    	    {
	    			return IFX_SHELL_KEY_CTRL_C;
	    	    }
	        }


	    }
	}while(error);

	AppSetupWizard_stopMotor(TimeConst_100ms);


//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                     Current sensor wizard end                                -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
	return IFX_SHELL_KEY_NULL;
}

char AppSetupWizard_positionSensorAurixRecolverPreInit(Ifx_Shell *shell, LB_FileConfiguration *configuration, uint8 index)
{
	IfxStdIf_DPipe *io = shell->io;
	LB_FileConfiguration defaultConfig;
	LB_setupDefaultValue(&defaultConfig);

//    IfxStdIf_DPipe_print(io, "Sensor: AURIX RDC %d"ENDL, index);

    if (AppSetupWizard_askSint32(shell, "Resolution in tick", defaultConfig.positionSensors.aurixResolver.input[index].resolution, 0, 0, &configuration->positionSensors.aurixResolver.input[index].resolution) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
	if (AppSetupWizard_askSint32(shell, "Carrier frequency in Hz", defaultConfig.positionSensors.aurixResolver.carrierFrequency, 0, 0, &configuration->positionSensors.aurixResolver.carrierFrequency) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
	if (AppSetupWizard_askUint16(shell, "Period per rotation", defaultConfig.positionSensors.aurixResolver.input[index].periodPerRotation, 0, 0, &configuration->positionSensors.aurixResolver.input[index].periodPerRotation) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
	return  IFX_SHELL_KEY_NULL;
}

char AppSetupWizard_positionSensorAurixRecolverPostInit(Ifx_Shell *shell, LB_FileConfiguration *configuration, uint8 index)
{
	IfxStdIf_DPipe *io = shell->io;
	LB_FileConfiguration defaultConfig;
	LB_setupDefaultValue(&defaultConfig);
    IfxStdIf_MotorModelF32_Param param = IfxStdIf_MotorModelF32_getParam(g_appLibrary.motorControl.motorModel);

    /*************************************************************************/
//    IfxStdIf_DPipe_print(io, "Sensor: AURIX RDC"ENDL);
    /* Start PWM  */
	{
//	    IfxStdIf_DPipe_print(io, "INFO:Switch the DC-link voltage ON now. (Low voltage and current limitation recommended)"ENDL);
//	    IfxStdIf_DPipe_print(io, "INFO:WARNING: Voltage will be applied to the motor phases, motor will turn (up to about 100 rpm) during the setup!"ENDL);
//	    IfxStdIf_DPipe_print(io, "INFO:WARNING: No load should be applied to the motor"ENDL);

	    if (!AppSetupWizard_continue(shell))
	    {
			return IFX_SHELL_KEY_CTRL_C;
	    }

	    if (AppSetupWizard_checkDcLinkVoltage(shell, configuration) ==IFX_SHELL_KEY_CTRL_C)
	    {
			return IFX_SHELL_KEY_CTRL_C;
	    }

    	Ifx_MotorControlF32_resetVirtualPositionSensor(&g_appLibrary.motorControl);
	    if (AppSetupWizard_startMotor(shell, Ifx_MotorControl_Mode_openLoop, TimeConst_100ms))
	    {

	    	IfxStdIf_Pos_Dir dir = IfxStdIf_Pos_Dir_unknown;
	    	float32 speed = 0;
	    	sint32 cosMax = 0;
	    	sint32 sinMax = 0;
            sint32 offset;
    		IfxStdIf_Pos *sensor = g_Lb.stdIf.dsadcRdc[index];
            IfxStdIf_Pos_setOffset(sensor, 0);

            /* Do 1 full turn low speed */
            AppSetupWizard_setDutyCycleAndSpeed(2);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 1) {}


            AppSetupWizard_setDutyCycleAndSpeed(25);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 2) {}
            AppSetupWizard_setDutyCycleAndSpeed(50);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 3) {}
            AppSetupWizard_setDutyCycleAndSpeed(75);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 4) {}
            AppSetupWizard_setDutyCycleAndSpeed(100);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 5) {}


	        /* Wait for 2 full turn to find out signal amplitude */
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 7)
	        {
				if ((dir == IfxStdIf_Pos_Dir_unknown) && (Ifx_MotorControlF32_getPosition(&g_appLibrary.motorControl) > IFX_PI))
				{
					dir = IfxDsadc_Rdc_getDirection(g_Lb.stdIf.dsadcRdcDriver[index]);
					speed = IfxDsadc_Rdc_getSpeed(g_Lb.stdIf.dsadcRdcDriver[index]);
				}

				cosMax = __max(cosMax, __abs(g_Lb.stdIf.dsadcRdcDriver[index]->cosIn));
				sinMax = __max(sinMax, __abs(g_Lb.stdIf.dsadcRdcDriver[index]->sinIn));
	        }

	        /* Measure offset */
            AppSetupWizard_setDutyCycleAndSpeed(0);
	        while (Ifx_MotorControlF32_getSpeed(&g_appLibrary.motorControl) > 0);
        	wait(TimeConst_1s);
	        Ifx_MotorControlF32_setPosition(&g_appLibrary.motorControl, 0);
        	wait(TimeConst_1s);

			offset = (IfxStdIf_Pos_getRawPosition(sensor) - IfxStdIf_Pos_getOffset(sensor));
			offset = -(offset & (IfxStdIf_Pos_getResolution(sensor) - 1));

			uint8 i = param.polePair;
			while ((offset <0) && (i))
			{
				offset += IfxStdIf_Pos_getResolution(sensor) / param.polePair;
				i--;
			}

			/* Do 1 full turn with correct offset */
            IfxStdIf_Pos_setOffset(sensor, offset);
            AppSetupWizard_setDutyCycleAndSpeed(2);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 8) {}
            IfxStdIf_Pos_setOffset(sensor, 0);

        	AppSetupWizard_stopMotor(TimeConst_100ms);

//            IfxStdIf_DPipe_print(io, "INFO: If the motor did not run smoothly, modify the V/F constant "ENDL);


        	IfxStdIf_Pos_printStatus(IfxDsadc_Rdc_getFault(g_Lb.stdIf.dsadcRdcDriver[index]), io);
//            IfxStdIf_DPipe_print(io, "Detected motor speed (Expected ~100rpm): %f rpm"ENDL, (speed / 2.0 / IFX_PI * 60));
//            IfxStdIf_DPipe_print(io, "Detected sine signal amplitude: %d"ENDL, sinMax);
//            IfxStdIf_DPipe_print(io, "Detected cosine signal amplitude: %d"ENDL, cosMax);

        	if (AppSetupWizard_askSint32(shell, "Signal amplitude min for sine and cosine in DSADC result unit", defaultConfig.positionSensors.aurixResolver.input[index].signalAmplitudeMin, 0, 0, &configuration->positionSensors.aurixResolver.input[index].signalAmplitudeMin) == -1)
        	{/* FIXME need to set range*/
        		return IFX_SHELL_KEY_CTRL_C;
        	}
        	if (AppSetupWizard_askSint32(shell, "Signal amplitude max for sine and cosine in DSADC result unit", defaultConfig.positionSensors.aurixResolver.input[index].signalAmplitudeMax, 0, 0, &configuration->positionSensors.aurixResolver.input[index].signalAmplitudeMax) == -1)
        	{/* FIXME need to set range*/
        		return IFX_SHELL_KEY_CTRL_C;
        	}

        	if (AppSetupWizard_askUint8(shell, "Gain code", defaultConfig.positionSensors.aurixResolver.input[index].gainCode, 0, 0, &configuration->positionSensors.aurixResolver.input[index].gainCode) == -1)
        	{ /* FIXME need to set range*/
        		return IFX_SHELL_KEY_CTRL_C;
        	}
        	AppLbStdIf_setResolverGain(&g_Lb.stdIf, index, configuration->positionSensors.aurixResolver.input[index].gainCode);

//            IfxStdIf_DPipe_print(io, "Detected motor direction: %s"ENDL, (dir == IfxStdIf_Pos_Dir_forward ? "Clock wise": "Counter clock wise"));
        	boolean reversed = configuration->positionSensors.aurixResolver.input[index].reversed;
        	if (AppSetupWizard_askBoolean(shell, "Reversed", defaultConfig.positionSensors.aurixResolver.input[index].reversed, &configuration->positionSensors.aurixResolver.input[index].reversed) == -1)
        	{
        		return IFX_SHELL_KEY_CTRL_C;
        	}

        	if (configuration->positionSensors.aurixResolver.input[index].reversed == reversed)
        	{
//                IfxStdIf_DPipe_print(io, "Detected sensor offset: %d"ENDL, offset);
                if (AppSetupWizard_askSint32(shell, "Offset in ticks", defaultConfig.positionSensors.aurixResolver.input[index].offset, 0, 0, &configuration->positionSensors.aurixResolver.input[index].offset) == -1)
            	{/* FIXME need to set range*/
            		return IFX_SHELL_KEY_CTRL_C;
            	}
                IfxStdIf_Pos_setOffset(sensor, configuration->positionSensors.aurixResolver.input[index].offset);
        	}
        	else
        	{
//                IfxStdIf_DPipe_print(io, "Changing the sensor direction require the ECU to be reset to take over the change"ENDL);
//                IfxStdIf_DPipe_print(io, "Please save the modification and restart the ECU before setting the offset"ENDL);
        	}
	    }
	}
	return  IFX_SHELL_KEY_NULL;

}


char AppSetupWizard_positionSensorAd2s1210PreInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
	LB_FileConfiguration defaultConfig;
	LB_setupDefaultValue(&defaultConfig);

//    IfxStdIf_DPipe_print(io, "Sensor: AD2S1210"ENDL);


	if (AppSetupWizard_askSint32(shell, "Resolution in tick", defaultConfig.positionSensors.ad2s1210.resolution, 0, 0, &configuration->positionSensors.ad2s1210.resolution) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
	if (AppSetupWizard_askSint32(shell, "Carrier frequency in Hz", defaultConfig.positionSensors.ad2s1210.carrierFrequency, 0, 0, &configuration->positionSensors.ad2s1210.carrierFrequency) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
	if (AppSetupWizard_askUint16(shell, "Period per rotation", defaultConfig.positionSensors.ad2s1210.periodPerRotation, 0, 0, &configuration->positionSensors.ad2s1210.periodPerRotation) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}
	return  IFX_SHELL_KEY_NULL;
}
char AppSetupWizard_positionSensorAd2s1210PostInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
	LB_FileConfiguration defaultConfig;
	LB_setupDefaultValue(&defaultConfig);
    IfxStdIf_MotorModelF32_Param param = IfxStdIf_MotorModelF32_getParam(g_appLibrary.motorControl.motorModel);

    /*************************************************************************/
//    IfxStdIf_DPipe_print(io, "Sensor: AD2S1210"ENDL);
    /* Start PWM  */
	{
//	    IfxStdIf_DPipe_print(io, "INFO:Switch the DC-link voltage ON now. (Low voltage and current limitation recommended)"ENDL);
//	    IfxStdIf_DPipe_print(io, "INFO:WARNING: Voltage will be applied to the motor phases, motor will turn (up to 100 rpm) during the setup!"ENDL);
//	    IfxStdIf_DPipe_print(io, "INFO:WARNING: No load should be applied to the motor"ENDL);


	    if (!AppSetupWizard_continue(shell))
	    {
			return IFX_SHELL_KEY_CTRL_C;
	    }

	    if (AppSetupWizard_checkDcLinkVoltage(shell, configuration) ==IFX_SHELL_KEY_CTRL_C)
	    {
			return IFX_SHELL_KEY_CTRL_C;
	    }

    	Ifx_MotorControlF32_resetVirtualPositionSensor(&g_appLibrary.motorControl);
	    if (AppSetupWizard_startMotor(shell, Ifx_MotorControl_Mode_openLoop, TimeConst_100ms))
	    {

	    	IfxStdIf_Pos_Dir dir = IfxStdIf_Pos_Dir_unknown;
	    	float32 speed = 0;
            sint32 offset;
    		IfxStdIf_Pos *sensor = g_Lb.stdIf.ad2s1210;
            IfxStdIf_Pos_setOffset(sensor, 0);

            /* Do 1 full turn low speed */
            AppSetupWizard_setDutyCycleAndSpeed(2);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 1) {}


            AppSetupWizard_setDutyCycleAndSpeed(25);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 2) {}
            AppSetupWizard_setDutyCycleAndSpeed(50);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 3) {}
            AppSetupWizard_setDutyCycleAndSpeed(75);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 4) {}
            AppSetupWizard_setDutyCycleAndSpeed(100);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 5) {}


	        /* Wait for 2 full turn to find out signal amplitude */
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 7)
	        {
				if ((dir == IfxStdIf_Pos_Dir_unknown) && (Ifx_MotorControlF32_getPosition(&g_appLibrary.motorControl) > IFX_PI))
				{
					dir = IfxStdIf_Pos_getDirection(g_Lb.stdIf.ad2s1210);
					speed = IfxStdIf_Pos_getSpeed(g_Lb.stdIf.ad2s1210);
				}

	        }

	        /* Measure offset */
            AppSetupWizard_setDutyCycleAndSpeed(0);
	        while (Ifx_MotorControlF32_getSpeed(&g_appLibrary.motorControl) > 0);
        	wait(TimeConst_1s);
	        Ifx_MotorControlF32_setPosition(&g_appLibrary.motorControl, 0);
        	wait(TimeConst_1s);

			offset = (IfxStdIf_Pos_getRawPosition(sensor) - IfxStdIf_Pos_getOffset(sensor));
			offset = -(offset & (IfxStdIf_Pos_getResolution(sensor) - 1));

			uint8 i = param.polePair;
			while ((offset <0) && (i))
			{
				offset += IfxStdIf_Pos_getResolution(sensor) / param.polePair;
				i--;
			}

			/* Do 1 full turn with correct offset */
            IfxStdIf_Pos_setOffset(sensor, offset);
            AppSetupWizard_setDutyCycleAndSpeed(2);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 8) {}
            IfxStdIf_Pos_setOffset(sensor, 0);

        	AppSetupWizard_stopMotor(TimeConst_100ms);


        	IfxStdIf_Pos_printStatus(IfxStdIf_Pos_getFault(g_Lb.stdIf.ad2s1210), io);
//            IfxStdIf_DPipe_print(io, "Detected motor speed (Expected ~100rpm): %f rpm"ENDL, (speed / 2.0 / IFX_PI * 60));

            boolean reversed = configuration->positionSensors.ad2s1210.reversed;
        	if (AppSetupWizard_askUint8(shell, "Gain code", defaultConfig.positionSensors.ad2s1210.gainCode, 0, 0, &configuration->positionSensors.ad2s1210.gainCode) == -1)
        	{ /* FIXME need to set range*/
        		return IFX_SHELL_KEY_CTRL_C;
        	}

//        	IfxStdIf_DPipe_print(io, "Detected motor direction: %s"ENDL, (dir == IfxStdIf_Pos_Dir_forward ? "Clock wise": "Counter clock wise"));
//        	IfxStdIf_DPipe_print(io, "Hint: Change sensor reverse parameter if detected direction is 'Counter clock wise'"ENDL);

        	if (AppSetupWizard_askBoolean(shell, "Reversed", defaultConfig.positionSensors.ad2s1210.reversed, &configuration->positionSensors.ad2s1210.reversed) == -1)
        	{
        		return IFX_SHELL_KEY_CTRL_C;
        	}

        	if (configuration->positionSensors.ad2s1210.reversed == reversed)
        	{
//                IfxStdIf_DPipe_print(io, "Detected sensor offset: %d"ENDL, offset);
                if (AppSetupWizard_askSint32(shell, "Offset in ticks", defaultConfig.positionSensors.ad2s1210.offset, 0, 0, &configuration->positionSensors.ad2s1210.offset) == -1)
            	{/* FIXME need to set range*/
            		return IFX_SHELL_KEY_CTRL_C;
            	}
                IfxStdIf_Pos_setOffset(sensor, configuration->positionSensors.ad2s1210.offset);
        	}
        	else
        	{
//                IfxStdIf_DPipe_print(io, "Changing the sensor direction require the ECU to be reset to take over the change"ENDL);
//                IfxStdIf_DPipe_print(io, "Please save the modification and restart the ECU before setting the offset"ENDL);
        	}


	    }
	}
	return  IFX_SHELL_KEY_NULL;

}




char AppSetupWizard_positionSensorEncoderPreInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
	LB_FileConfiguration defaultConfig;
	LB_setupDefaultValue(&defaultConfig);

//    IfxStdIf_DPipe_print(io, "Sensor: Encoder"ENDL);

	if (AppSetupWizard_askSint32(shell, "Resolution in tick", defaultConfig.positionSensors.encoder.resolution, 0, 0, &configuration->positionSensors.encoder.resolution) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}


	return  IFX_SHELL_KEY_NULL;
}

char AppSetupWizard_positionSensorEncoderPostInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
	LB_FileConfiguration defaultConfig;
	LB_setupDefaultValue(&defaultConfig);
    IfxStdIf_MotorModelF32_Param param = IfxStdIf_MotorModelF32_getParam(g_appLibrary.motorControl.motorModel);

    /*************************************************************************/
//    IfxStdIf_DPipe_print(io, "Sensor: Encoder"ENDL);
    /* Start PWM  */
	{
//	    IfxStdIf_DPipe_print(io, "INFO:Switch the DC-link voltage ON now. (Low voltage and current limitation recommended)"ENDL);
//	    IfxStdIf_DPipe_print(io, "INFO:WARNING: Voltage will be applied to the motor phases, motor will turn (up to 100 rpm) during the setup!"ENDL);
//	    IfxStdIf_DPipe_print(io, "INFO:WARNING: No load should be applied to the motor"ENDL);


	    if (!AppSetupWizard_continue(shell))
	    {
			return IFX_SHELL_KEY_CTRL_C;
	    }

	    if (AppSetupWizard_checkDcLinkVoltage(shell, configuration) ==IFX_SHELL_KEY_CTRL_C)
	    {
			return IFX_SHELL_KEY_CTRL_C;
	    }

    	Ifx_MotorControlF32_resetVirtualPositionSensor(&g_appLibrary.motorControl);
	    if (AppSetupWizard_startMotor(shell, Ifx_MotorControl_Mode_openLoop, TimeConst_100ms))
	    {

	    	IfxStdIf_Pos_Dir dir = IfxStdIf_Pos_Dir_unknown;
	    	float32 speed = 0;
            sint32 offset;
    		IfxStdIf_Pos *sensor = g_Lb.stdIf.encoder;
            IfxStdIf_Pos_setOffset(sensor, 0);

            /* Do 1 full turn low speed */
            AppSetupWizard_setDutyCycleAndSpeed(2);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 1) {}


            AppSetupWizard_setDutyCycleAndSpeed(25);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 2) {}
            AppSetupWizard_setDutyCycleAndSpeed(50);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 3) {}
            AppSetupWizard_setDutyCycleAndSpeed(75);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 4) {}
            AppSetupWizard_setDutyCycleAndSpeed(100);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 5) {}


	        /* Wait for 2 full turn to find out signal amplitude */
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 7)
	        {
				if ((dir == IfxStdIf_Pos_Dir_unknown) && (Ifx_MotorControlF32_getPosition(&g_appLibrary.motorControl) > IFX_PI))
				{
					dir = IfxStdIf_Pos_getDirection(g_Lb.stdIf.encoder);
					speed = IfxStdIf_Pos_getSpeed(g_Lb.stdIf.encoder);
				}

	        }

	        /* Measure offset */
            AppSetupWizard_setDutyCycleAndSpeed(0);
	        while (Ifx_MotorControlF32_getSpeed(&g_appLibrary.motorControl) > 0);
        	wait(TimeConst_1s);
	        Ifx_MotorControlF32_setPosition(&g_appLibrary.motorControl, 0);
        	wait(TimeConst_1s);

			offset = (IfxStdIf_Pos_getRawPosition(sensor) - IfxStdIf_Pos_getOffset(sensor));
			offset = -(offset & (IfxStdIf_Pos_getResolution(sensor) - 1));

			uint8 i = param.polePair;
			while ((offset <0) && (i))
			{
				offset += IfxStdIf_Pos_getResolution(sensor) / param.polePair;
				i--;
			}

			/* Do 1 full turn with correct offset */
            IfxStdIf_Pos_setOffset(sensor, offset);
            AppSetupWizard_setDutyCycleAndSpeed(2);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 8) {}
            IfxStdIf_Pos_setOffset(sensor, 0);

        	AppSetupWizard_stopMotor(TimeConst_100ms);


        	IfxStdIf_Pos_printStatus(IfxStdIf_Pos_getFault(g_Lb.stdIf.encoder), io);
//            IfxStdIf_DPipe_print(io, "Detected motor speed (Expected ~100rpm): %f rpm"ENDL, (speed / 2.0 / IFX_PI * 60));

            boolean reversed = configuration->positionSensors.encoder.reversed;

//        	IfxStdIf_DPipe_print(io, "Detected motor direction: %s"ENDL, (dir == IfxStdIf_Pos_Dir_forward ? "Clock wise": "Counter clock wise"));
//        	IfxStdIf_DPipe_print(io, "Hint: Change sensor reverse parameter if detected direction is 'Counter clock wise'"ENDL);

        	if (AppSetupWizard_askBoolean(shell, "Reversed", defaultConfig.positionSensors.encoder.reversed, &configuration->positionSensors.encoder.reversed) == -1)
        	{
        		return IFX_SHELL_KEY_CTRL_C;
        	}

        	if (configuration->positionSensors.encoder.reversed == reversed)
        	{
//                IfxStdIf_DPipe_print(io, "Detected sensor offset: %d"ENDL, offset);
                if (AppSetupWizard_askSint32(shell, "Offset in ticks", defaultConfig.positionSensors.encoder.offset, 0, 0, &configuration->positionSensors.encoder.offset) == -1)
            	{/* FIXME need to set range*/
            		return IFX_SHELL_KEY_CTRL_C;
            	}
                IfxStdIf_Pos_setOffset(sensor, configuration->positionSensors.encoder.offset);
        	}
        	else
        	{
//                IfxStdIf_DPipe_print(io, "Changing the sensor direction require the ECU to be reset to take over the change"ENDL);
//                IfxStdIf_DPipe_print(io, "Please save the modification and restart the ECU before setting the offset"ENDL);
        	}


	    }
	}
	return  IFX_SHELL_KEY_NULL;

}


char AppSetupWizard_positionSensorIGmrPreInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
	LB_FileConfiguration defaultConfig;
	LB_setupDefaultValue(&defaultConfig);

//    IfxStdIf_DPipe_print(io, "Sensor: iGMR"ENDL);

	if (AppSetupWizard_askSint32(shell, "Encoder resolution in tick", defaultConfig.positionSensors.encoder.resolution, 0, 0, &configuration->positionSensors.encoder.resolution) == -1)
	{
		return IFX_SHELL_KEY_CTRL_C;
	}


	return  IFX_SHELL_KEY_NULL;
}

char AppSetupWizard_positionSensorIGmrPostInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
	LB_FileConfiguration defaultConfig;
	LB_setupDefaultValue(&defaultConfig);
    IfxStdIf_MotorModelF32_Param param = IfxStdIf_MotorModelF32_getParam(g_appLibrary.motorControl.motorModel);

    /*************************************************************************/
//    IfxStdIf_DPipe_print(io, "Sensor: iGMR"ENDL);
    /* Start PWM  */
	{
//	    IfxStdIf_DPipe_print(io, "INFO:Switch the DC-link voltage ON now. (Low voltage and current limitation recommended)"ENDL);
//	    IfxStdIf_DPipe_print(io, "INFO:WARNING: Voltage will be applied to the motor phases, motor will turn (up to 100 rpm) during the setup!"ENDL);
//	    IfxStdIf_DPipe_print(io, "INFO:WARNING: No load should be applied to the motor"ENDL);


	    if (!AppSetupWizard_continue(shell))
	    {
			return IFX_SHELL_KEY_CTRL_C;
	    }

	    if (AppSetupWizard_checkDcLinkVoltage(shell, configuration) ==IFX_SHELL_KEY_CTRL_C)
	    {
			return IFX_SHELL_KEY_CTRL_C;
	    }

    	Ifx_MotorControlF32_resetVirtualPositionSensor(&g_appLibrary.motorControl);
	    if (AppSetupWizard_startMotor(shell, Ifx_MotorControl_Mode_openLoop, TimeConst_100ms))
	    {

	    	IfxStdIf_Pos_Dir dirEncoder = IfxStdIf_Pos_Dir_unknown;
	    	float32 speedEncoder = 0;
	    	IfxStdIf_Pos_Dir dirIGmr = IfxStdIf_Pos_Dir_unknown;
	    	float32 speedIGmr = 0;
            sint32 offsetIGmr;
            IfxStdIf_Pos_reset(g_Lb.stdIf.encoder);
            IfxStdIf_Pos_setOffset(g_Lb.stdIf.encoder, 0);
            IfxStdIf_Pos_setOffset(g_Lb.stdIf.tle5012, 0);


            /* Do 1 full turn low speed */
            AppSetupWizard_setDutyCycleAndSpeed(2);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 1) {}


            AppSetupWizard_setDutyCycleAndSpeed(25);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 2) {}
            AppSetupWizard_setDutyCycleAndSpeed(50);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 3) {}
            AppSetupWizard_setDutyCycleAndSpeed(75);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 4) {}
            AppSetupWizard_setDutyCycleAndSpeed(100);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 5) {}


	        /* Wait for 2 full turn to find out signal amplitude */
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 7)
	        {
				if ((dirEncoder == IfxStdIf_Pos_Dir_unknown) && (Ifx_MotorControlF32_getPosition(&g_appLibrary.motorControl) > IFX_PI))
				{
					dirEncoder = IfxStdIf_Pos_getDirection(g_Lb.stdIf.encoder);
					speedEncoder = IfxStdIf_Pos_getSpeed(g_Lb.stdIf.encoder);
				}
	        }

	        /* Get iGMR speed and dir */
			dirIGmr = IfxStdIf_Pos_getDirection(g_Lb.stdIf.tle5012);
			speedIGmr = IfxStdIf_Pos_getSpeed(g_Lb.stdIf.tle5012);

	        /* Measure offset */
            AppSetupWizard_setDutyCycleAndSpeed(0);
	        while (Ifx_MotorControlF32_getSpeed(&g_appLibrary.motorControl) > 0);
        	wait(TimeConst_1s);
	        Ifx_MotorControlF32_setPosition(&g_appLibrary.motorControl, 0);
        	wait(TimeConst_1s);

        	offsetIGmr = (IfxStdIf_Pos_getRawPosition(g_Lb.stdIf.tle5012) - IfxStdIf_Pos_getOffset(g_Lb.stdIf.tle5012));
        	offsetIGmr = -(offsetIGmr & (IfxStdIf_Pos_getResolution(g_Lb.stdIf.tle5012) - 1));

			uint8 i = param.polePair;
			while ((offsetIGmr <0) && (i))
			{
				offsetIGmr += IfxStdIf_Pos_getResolution(g_Lb.stdIf.tle5012) / param.polePair;
				i--;
			}

			/* Do 1 full turn with correct offset */
            IfxStdIf_Pos_setOffset(g_Lb.stdIf.encoder,
            		((IfxStdIf_Pos_getRawPosition(g_Lb.stdIf.tle5012) + offsetIGmr)*IfxStdIf_Pos_getResolution(g_Lb.stdIf.encoder))/IfxStdIf_Pos_getResolution(g_Lb.stdIf.tle5012)
            		- IfxStdIf_Pos_getRawPosition(g_Lb.stdIf.encoder)
            		);

            AppSetupWizard_setDutyCycleAndSpeed(2);
	        while (Ifx_MotorControlF32_getTurn(&g_appLibrary.motorControl) < 8) {}
//            IfxStdIf_Pos_setOffset(g_Lb.stdIf.encoder, 0);

        	AppSetupWizard_stopMotor(TimeConst_100ms);


//            IfxStdIf_DPipe_print(io, "iGMR configuration:"ENDL);
//            IfxStdIf_DPipe_print(io, "    - Detected motor speed (Expected ~100rpm): %f rpm"ENDL, (speedIGmr / 2.0 / IFX_PI * 60));
            boolean reversedIGmr = configuration->positionSensors.encoder.reversed;

//        	IfxStdIf_DPipe_print(io, "    - Detected motor direction: %s"ENDL, (dirIGmr == IfxStdIf_Pos_Dir_forward ? "Clock wise": "Counter clock wise"));
//        	IfxStdIf_DPipe_print(io, "    - Hint: Change sensor reverse parameter if detected direction is 'Counter clock wise'"ENDL);
        	IfxStdIf_Pos_printStatus(IfxStdIf_Pos_getFault(g_Lb.stdIf.tle5012), io);

        	if (AppSetupWizard_askBoolean(shell, "Reversed", defaultConfig.positionSensors.tle5012.reversed, &configuration->positionSensors.tle5012.reversed) == -1)
        	{
        		return IFX_SHELL_KEY_CTRL_C;
        	}

        	if (configuration->positionSensors.encoder.reversed == reversedIGmr)
        	{
//                IfxStdIf_DPipe_print(io, "    - Detected sensor offset: %d"ENDL, offsetIGmr);
                if (AppSetupWizard_askSint32(shell, "Offset in ticks", defaultConfig.positionSensors.tle5012.offset, 0, 0, &configuration->positionSensors.tle5012.offset) == -1)
            	{/* FIXME need to set range*/
            		return IFX_SHELL_KEY_CTRL_C;
            	}
                IfxStdIf_Pos_setOffset(g_Lb.stdIf.tle5012, configuration->positionSensors.tle5012.offset);
        	}
        	else
        	{
//                IfxStdIf_DPipe_print(io, "    - Changing the sensor direction require the ECU to be reset to take over the change"ENDL);
//                IfxStdIf_DPipe_print(io, "    - Please save the modification and restart the ECU before setting the offset"ENDL);
        	}

//            IfxStdIf_DPipe_print(io, "Encoder configuration:"ENDL);
        	IfxStdIf_Pos_printStatus(IfxStdIf_Pos_getFault(g_Lb.stdIf.encoder), io);
//            IfxStdIf_DPipe_print(io, "    - Detected motor speed (Expected ~100rpm): %f rpm"ENDL, (speedEncoder / 2.0 / IFX_PI * 60));

//        	IfxStdIf_DPipe_print(io, "    - Detected motor direction: %s"ENDL, (dirEncoder == IfxStdIf_Pos_Dir_forward ? "Clock wise": "Counter clock wise"));
//        	IfxStdIf_DPipe_print(io, "    - Hint: Change sensor reverse parameter if detected direction is 'Counter clock wise'"ENDL);

        	if (AppSetupWizard_askBoolean(shell, "    - Reversed", defaultConfig.positionSensors.encoder.reversed, &configuration->positionSensors.encoder.reversed) == -1)
        	{
        		return IFX_SHELL_KEY_CTRL_C;
        	}
	    }
	}
	return  IFX_SHELL_KEY_NULL;

}


/* API prototype match AppSetupWizard_JumpYesNo */
char AppSetupWizard_save(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
//	IfxStdIf_DPipe_print(io, "Saving configuration..."ENDL);
	AppLbStdIf_updateLogicBoardConfigFile(&g_Lb.stdIf);
	AppDbStdIf_saveConfig(&g_Lb.driverBoardStdif);
    Ifx_MotorModelConfigF32_updateFile(&g_Lb.motorConfiguration);
//	IfxStdIf_DPipe_print(io, "Done."ENDL);
	return IFX_SHELL_KEY_NULL;
}

/* API prototype match AppSetupWizard_JumpYesNo */
char AppSetupWizard_positionSensorPreInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
	LB_FileConfiguration defaultConfig;
	LB_setupDefaultValue(&defaultConfig);

//	IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                    Position sensor wizard (Pre initialization)               -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);

    { /* Position sensor selection */
    	pchar answers[6] = {"encoder", "ad2s1210", "rdc0", "rdc1", "tle5012", ""};
    	sint32 option;
    	sint32 defaultOption;
        option =
        		(configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_encoder ? 0
        		 : (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_ad2s1210 ? 1
        			: (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver0 ? 2
        				: (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver1 ? 3
        					: (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_tle5012 ? 4
        			   : 2)))));
        defaultOption =
        		(defaultConfig.inverter[0].positionSensor[0] == ECU_PositionSensor_encoder ? 0
        		 : (defaultConfig.inverter[0].positionSensor[0] == ECU_PositionSensor_ad2s1210 ? 1
        			: (defaultConfig.inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver0 ? 2
        			    : (defaultConfig.inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver1 ? 3
        			    	: (defaultConfig.inverter[0].positionSensor[0] == ECU_PositionSensor_tle5012 ? 4
        			   : 2)))));

    	option = AppSetupWizard_askOptions(shell, "Position sensor type", defaultOption, option, answers);
    	if (option == -1)
    	{
    		return IFX_SHELL_KEY_CTRL_C;
    	}
    	else if (option == 0)
        {
            configuration->inverter[0].positionSensor[0] = ECU_PositionSensor_encoder;
        }
        else if (option == 1)
        {
        	configuration->inverter[0].positionSensor[0] = ECU_PositionSensor_ad2s1210;
        }
        else if (option == 2)
        {
        	configuration->inverter[0].positionSensor[0] = ECU_PositionSensor_internalResolver0;
        }
        else if (option == 3)
        {
        	configuration->inverter[0].positionSensor[0] = ECU_PositionSensor_internalResolver1;
        }
        else if (option == 4)
        {
        	configuration->inverter[0].positionSensor[0] = ECU_PositionSensor_tle5012;
        }
    }

    if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver0)
	{
		if (AppSetupWizard_positionSensorAurixRecolverPreInit(shell, configuration, 0) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}
	else if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver1)
	{
		if (AppSetupWizard_positionSensorAurixRecolverPreInit(shell, configuration, 1) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}
	else if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_ad2s1210)
	{
		if (AppSetupWizard_positionSensorAd2s1210PreInit(shell, configuration) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}
	else if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_encoder)
	{
		if (AppSetupWizard_positionSensorEncoderPreInit(shell, configuration) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}
	else if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_tle5012)
	{
		if (AppSetupWizard_positionSensorIGmrPreInit(shell, configuration) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}

	if (configuration->inverter[0].positionSensor[1] == ECU_PositionSensor_internalResolver0)
	{
		if (AppSetupWizard_positionSensorAurixRecolverPreInit(shell, configuration, 0) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}
	else if (configuration->inverter[0].positionSensor[1] == ECU_PositionSensor_internalResolver1)
	{
		if (AppSetupWizard_positionSensorAurixRecolverPreInit(shell, configuration, 1) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}


//    IfxStdIf_DPipe_print(io, ""ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                    Position sensor wizard end                                -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
	return IFX_SHELL_KEY_NULL;
}

/* API prototype match AppSetupWizard_JumpYesNo */
char AppSetupWizard_positionSensorPostInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                    Position sensor wizard (Post initialization)              -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the motor phases are connected"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the current sensors are connected and properly configured"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the motor parameters are properly configured (pole pair, IStall)"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the position sensor is connected"ENDL);


	if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver0)
	{
		if (AppSetupWizard_positionSensorAurixRecolverPostInit(shell, configuration, 0) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}
	else if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_internalResolver1)
	{
		if (AppSetupWizard_positionSensorAurixRecolverPostInit(shell, configuration, 1) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}
	else if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_ad2s1210)
	{
		if (AppSetupWizard_positionSensorAd2s1210PostInit(shell, configuration) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}
	else if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_encoder)
	{
		if (AppSetupWizard_positionSensorEncoderPostInit(shell, configuration) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}
	else if (configuration->inverter[0].positionSensor[0] == ECU_PositionSensor_tle5012)
	{
		if (AppSetupWizard_positionSensorIGmrPostInit(shell, configuration) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}

	if (configuration->inverter[0].positionSensor[1] == ECU_PositionSensor_internalResolver0)
	{
		if (AppSetupWizard_positionSensorAurixRecolverPostInit(shell, configuration, 0) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}
	else if (configuration->inverter[0].positionSensor[1] == ECU_PositionSensor_internalResolver1)
	{
		if (AppSetupWizard_positionSensorAurixRecolverPostInit(shell, configuration, 1) == IFX_SHELL_KEY_CTRL_C)
		{
			return  IFX_SHELL_KEY_CTRL_C;
		}
	}


//    IfxStdIf_DPipe_print(io, ""ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                    Position sensor wizard end                                -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
	return IFX_SHELL_KEY_NULL;
}


/* API prototype match AppSetupWizard_JumpYesNo */
char AppSetupWizard_CurrentPiPostInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
    Ifx_MotorModelConfigF32 defaultMotorConfig;
    Ifx_MotorModelConfigF32_setupDefaultValue(&defaultMotorConfig);

    boolean exit = FALSE;
	cfloat32 testCurrent1;
	cfloat32 testCurrent2;
	float32 delaySecond1 = 3;
	float32 delaySecond2 = 0.05;
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                    Phase current PI controller wizard (Post initialization)  -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the motor phases are connected"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the current sensors are connected and properly configured"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO:Switch the DC-link voltage ON now. (Low voltage and current limitation recommended)"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO:WARNING: Voltage will be applied to the motor phases, motor will turn during the setup!"ENDL);
//    IfxStdIf_DPipe_print(io, ENDL"The test will run the motor in current control mode. "ENDL);
//    IfxStdIf_DPipe_print(io, "To tune the PI controller, two current steps will be generated."ENDL);
//    IfxStdIf_DPipe_print(io, "The 1st current pulse should move the motor shaft (no load recommened) so that the north"ENDL);
//    IfxStdIf_DPipe_print(io, "pole is aligned with the magnet (d axix)"ENDL);
//    IfxStdIf_DPipe_print(io, "The 2nd current pulse can be monitored on an oscilloscope to tune the PI controller parameters"ENDL);
//    IfxStdIf_DPipe_print(io, "The start current is 0A, then raise to 'Current 1' for the set delay 1 time"ENDL);
//    IfxStdIf_DPipe_print(io, "then raise to 'Current 2' for the set delay 2 time,"ENDL);
//    IfxStdIf_DPipe_print(io, "then set to 0A and switch of the PWM"ENDL);

    testCurrent1.real = 1;
    testCurrent2.real = 2;
    testCurrent1.imag = 0;
    testCurrent2.imag = 0;
	do
	{
		if (AppSetupWizard_askFloat32(shell, "Current 1 (A)", testCurrent1.real, 0, 0, &testCurrent1.real) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}
		if (AppSetupWizard_askFloat32(shell, "Delay 1 (s)", delaySecond1, 0, 0, &delaySecond1) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}
		if (AppSetupWizard_askFloat32(shell, "Current 2 (A)", testCurrent2.real, 0, 0, &testCurrent2.real) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}
		if (AppSetupWizard_askFloat32(shell, "Delay 2 (s)", delaySecond2, 0, 0, &delaySecond2) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}
		if (AppSetupWizard_askFloat32(shell, "Kp", defaultMotorConfig.data.idKp, 0, 0, &g_Lb.motorConfiguration.data.idKp) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}
		if (AppSetupWizard_askFloat32(shell, "Ki", defaultMotorConfig.data.idKi, 0, 0, &g_Lb.motorConfiguration.data.idKi) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}
		g_Lb.motorConfiguration.data.iqKp = g_Lb.motorConfiguration.data.idKp;
		g_Lb.motorConfiguration.data.iqKi = g_Lb.motorConfiguration.data.idKi; /* FIXME enable different parameters */

		if (!AppSetupWizard_continue(shell))
		{
			exit = TRUE;
		}
		else
		{
			/* FIXME need to update the running parameters */
			Ifx_FocF32_Pic_Config piD;
			Ifx_FocF32_Pic_Config piQ;

			piD.kp = g_Lb.motorConfiguration.data.idKp;
			piD.ki = g_Lb.motorConfiguration.data.idKi;
			piQ.kp = g_Lb.motorConfiguration.data.iqKp;
			piQ.ki = g_Lb.motorConfiguration.data.iqKi;
			Ifx_MotorControlF32_setCurrentControllers(&g_appLibrary.motorControl, &piD, &piQ);


		    if (AppSetupWizard_startMotor(shell, Ifx_MotorControl_Mode_testPI, TimeConst_10ms))
		    {
		    	cfloat32 testCurrent0;

		    	testCurrent0.real = 0;
		    	testCurrent0.imag = 0;

                Ifx_MotorControlF32_setCurrent(&g_appLibrary.motorControl, &testCurrent1);
		        wait(TimeConst_1s*delaySecond1);
                Ifx_MotorControlF32_setCurrent(&g_appLibrary.motorControl, &testCurrent2);
		        wait(TimeConst_1s*delaySecond2);
                Ifx_MotorControlF32_setCurrent(&g_appLibrary.motorControl, &testCurrent0);
		        wait(TimeConst_10ms);
		    	AppSetupWizard_stopMotor(TimeConst_10ms);

		    }
		}


	}while(!exit);


//    IfxStdIf_DPipe_print(io, ""ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                    Phase current PI controller wizard end                    -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
	return IFX_SHELL_KEY_NULL;
}


/* API prototype match AppSetupWizard_JumpYesNo */
char AppSetupWizard_SpeedPiPostInit(Ifx_Shell *shell, LB_FileConfiguration *configuration)
{
	IfxStdIf_DPipe *io = shell->io;
	IfxStdIf_Pos* oldSensorR;
    Ifx_MotorModelConfigF32 defaultMotorConfig;
    Ifx_MotorModelConfigF32_setupDefaultValue(&defaultMotorConfig);

	IfxStdIf_Pos_setSpeed(&g_appLibrary.motorControl.stdifVirtualPosition, 0);
    oldSensorR = ECU_getPositionSensorR(0);
    ECU_selectPositionSensorR(0, ECU_PositionSensor_virtual); /* Show reference */

    boolean exit = FALSE;
	float32 testSpeed1 = 0;
	float32 testSpeed2 = 100;
	float32 delaySecond = 3;
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                    Speed PI controller wizard (Post initialization)           -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the motor phases are connected"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the current sensors are connected and properly configured"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the motor parameters are properly configured (pole pair, IStall)"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the position sensor is connected and position / speed feedback is correct"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO: Make sure the position sensor is synchronized (for encoder index)"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO:Switch the DC-link voltage ON now. (Low voltage and current limitation recommended)"ENDL);
//    IfxStdIf_DPipe_print(io, "INFO:WARNING: Voltage will be applied to the motor phases, motor will turn during the setup!"ENDL);
//    IfxStdIf_DPipe_print(io, ENDL"The test will run the motor in speed control mode. "ENDL);
//    IfxStdIf_DPipe_print(io, "The start speed is 0rpm, then raise to 'Speed 1' for the set delay time"ENDL);
//    IfxStdIf_DPipe_print(io, "then raise to 'Speed 2' for the set delay time,"ENDL);
//    IfxStdIf_DPipe_print(io, "then set to 0rpm and switch of the PWM"ENDL);
// FIXME add : make sure the sensor is sync (encoder)
	do
	{
		if (AppSetupWizard_askFloat32(shell, "Speed 1 (rpm)", testSpeed1, 0, 0, &testSpeed1) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}
		if (AppSetupWizard_askFloat32(shell, "Speed 2 (rpm)", testSpeed2, 0, 0, &testSpeed2) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}
		if (AppSetupWizard_askFloat32(shell, "Delay (s)", delaySecond, 0, 0, &delaySecond) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}
		if (AppSetupWizard_askFloat32(shell, "Kp", defaultMotorConfig.data.speedKp, 0, 0, &g_Lb.motorConfiguration.data.speedKp) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}
		if (AppSetupWizard_askFloat32(shell, "Ki", defaultMotorConfig.data.speedKi, 0, 0, &g_Lb.motorConfiguration.data.speedKi) == -1)
		{
			return IFX_SHELL_KEY_CTRL_C;
		}

		if (!AppSetupWizard_continue(shell))
		{
			exit = TRUE;
		}
		else
		{
			Ifx_VelocityCtrF32_setKpKi(&g_appLibrary.motorControl.velocity, g_Lb.motorConfiguration.data.speedKp, g_Lb.motorConfiguration.data.speedKi);



		    if (AppSetupWizard_startMotor(shell, Ifx_MotorControl_Mode_speedControl, TimeConst_100ms))
		    {

				IfxStdIf_Pos_setSpeed(&g_appLibrary.motorControl.stdifVirtualPosition, IfxStdIf_Pos_rpmToRads(testSpeed1));
		        Ifx_MotorControlF32_setSpeed(&g_appLibrary.motorControl, testSpeed1);
		        wait(TimeConst_1s*delaySecond);
				IfxStdIf_Pos_setSpeed(&g_appLibrary.motorControl.stdifVirtualPosition, IfxStdIf_Pos_rpmToRads(testSpeed2));
		        Ifx_MotorControlF32_setSpeed(&g_appLibrary.motorControl, testSpeed2);
		        wait(TimeConst_1s*delaySecond);
				IfxStdIf_Pos_setSpeed(&g_appLibrary.motorControl.stdifVirtualPosition, 0);
		        Ifx_MotorControlF32_setSpeed(&g_appLibrary.motorControl, 0);
		        wait(TimeConst_1s);
		    	AppSetupWizard_stopMotor(TimeConst_100ms);

		    }
		}


	}while(!exit);

    ECU_setPositionSensorR(0, oldSensorR);

//    IfxStdIf_DPipe_print(io, ""ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
//    IfxStdIf_DPipe_print(io, "-                    Speed PI controller wizard end                            -"ENDL);
//    IfxStdIf_DPipe_print(io, "--------------------------------------------------------------------------------"ENDL);
	return IFX_SHELL_KEY_NULL;
}


boolean AppSetupWizard_run(IfxStdIf_DPipe *io)
{
	/* Get the shell used for the wizard */
	Ifx_Shell shell;
	LB_FileConfiguration *configuration;
	LB_FileConfiguration defaultConfig;
	LB_setupDefaultValue(&defaultConfig);
	boolean again = FALSE;

	configuration = g_Lb.stdIf.configuration;
	configuration->wizard.enabled = TRUE;



	AppSetupWizard_init(&shell, io);



//    IfxStdIf_DPipe_print(io, ENDL);
//    IfxStdIf_DPipe_print(io, "================================================================================"ENDL);
//    IfxStdIf_DPipe_print(io, "=                              Setup wizard                                    ="ENDL);
//    IfxStdIf_DPipe_print(io, "================================================================================"ENDL);
//    IfxStdIf_DPipe_print(io, "= Answers are case sensitive                                                   ="ENDL);
//    IfxStdIf_DPipe_print(io, "= Use the ENTER key to validate answers                                        ="ENDL);
//    IfxStdIf_DPipe_print(io, "= Wizard section can be quit with answer 'CTRL+C'                              ="ENDL);
//    IfxStdIf_DPipe_print(io, "================================================================================"ENDL);

    /* FIXME should not be asked if VDC available */
	AppSetupWizard_askFloat32(&shell, "DC-link voltage used during wizard in V  (Low voltage and current limitation recommended)", defaultConfig.wizard.vdcNom, 20, 400, &configuration->wizard.vdcNom);
	AppSetupWizard_askFloat32(&shell, "The V/F constant is used to set the voltage in open loop during the wizard execution."ENDL"V/F constant (in V per 1000 rpm)", defaultConfig.wizard.vf, 0, 100, &configuration->wizard.vf);

    if (g_App.hwState.hwMode == AppMode_configuration)
    {
		AppSetupWizard_jumpYesNo(&shell, "Enter inverter wizard (pre-initialization) [y/n]?"ENDL, AppSetupWizard_inverterPreInit, configuration);

		AppSetupWizard_jumpYesNo(&shell, "Enter motor wizard (pre-initialization) [y/n]?"ENDL, AppSetupWizard_motorPreInit, configuration);

		AppSetupWizard_jumpYesNo(&shell, "Enter current sensor wizard (pre-initialization) [y/n]?"ENDL, AppSetupWizard_currentSensorPreInit, configuration);

		AppSetupWizard_jumpYesNo(&shell, "Enter position sensor wizard (pre-initialization) [y/n]?"ENDL, AppSetupWizard_positionSensorPreInit, configuration);
    }
    else
    {
//        IfxStdIf_DPipe_print(io, "Pre-initialization wizard part can only be done in configuration state!"ENDL);
    }

    if (AppSetupWizard_goToRun(&shell, configuration) == IFX_SHELL_KEY_CTRL_C)
    {
//        IfxStdIf_DPipe_print(io, "Error!"ENDL);
    	return TRUE;
    }


    if (AppDbStdIf_getBoardVersion(&g_Lb.driverBoardStdif)->boardType == Lb_BoardType_HybridKit_DriverBoardHp2)
    { /* Overwrite Vdc as measurement is not available o this boards */
    	if (App_getBoardVersion(g_Lb.boardVersion.boardVersion) == 3)
    	{
        	g_Lb.logicboards.logicBoard_30.analogInput.vDc.input.value = configuration->wizard.vdcNom;
    	}
    }


    while (1)
    { /* Endless loop in wizard. Only way to exit is to power down*/
#if 1
		if (App_getBoardVersion(g_Lb.boardVersion.boardVersion) == 3)
		{ /* FIXME find formula for those values */
			g_Lb.logicboards.logicBoard_30.driver.inverter.deadtimeComp = 145;
		}
#endif


		AppSetupWizard_jumpYesNo(&shell, "Enter inverter wizard (post initialization) [y/n]?"ENDL, AppSetupWizard_inverterPostInit, configuration);

		AppSetupWizard_jumpYesNo(&shell, "Enter current sensor wizard (post initialization) [y/n]?"ENDL, AppSetupWizard_currentSensorPostInit, configuration);

		AppSetupWizard_jumpYesNo(&shell, "Enter position sensor wizard (post initialization) [y/n]?"ENDL, AppSetupWizard_positionSensorPostInit, configuration);

		AppSetupWizard_jumpYesNo(&shell, "Enter current PI controller wizard (post initialization) [y/n]?"ENDL, AppSetupWizard_CurrentPiPostInit, configuration);

		AppSetupWizard_jumpYesNo(&shell, "Enter speed PI controller wizard (post initialization) [y/n]?"ENDL, AppSetupWizard_SpeedPiPostInit, configuration);

		AppSetupWizard_stopMotor(TimeConst_100ms);

		AppSetupWizard_jumpYesNo(&shell, "Save changes [y/n]?"ENDL, AppSetupWizard_save, configuration);

		do
		{
			AppSetupWizard_askBoolean(&shell, "Re-enter post initialization wizard", FALSE, &again);
			if (!again)
			{
		        IfxStdIf_DPipe_print(io, "Power cycle the ECU to exit the wizard or re-enter pre-initialization wizard"ENDL);
			}
		}while (!again);

    }


    return TRUE; /* Code never comes here */

}


IfxStdIf_Pos_Status AppSetupWizard_dummyIfxStdIfPosGetFault(void *handle)
{
	IfxStdIf_Pos_Status status;
	status.status = 0;
    return status;
}
