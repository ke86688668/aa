/**
 * \file AppDbStdIf.c
 * \brief Standard interface: Driver board
 *
 * \version disabled
 * \copyright Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 */
#include "AppDbStdIf.h"
#include "Main.h"

boolean AppDbStdIf_shellSetupCommon(AppDbStdIf *stdif, pchar args, void *data, IfxStdIf_DPipe *io)
{
    boolean              result              = FALSE;

    if (Ifx_Shell_matchToken(&args, "efs") != FALSE)
    {
        if (!AppDbStdIf_isBoardVersionFile(stdif))
        {
//            IfxStdIf_DPipe_print(io, "Please set the board version with 'setup db version <type> <version> <revision> [<step>]'"ENDL);
        }
        else
        {
//            IfxStdIf_DPipe_print(io, "Creating configuration file system"ENDL);
            AppDbStdIf_createConfigEfs(stdif, FALSE);
            /* FIXME shouldn't the default value be set ? */
            if (!AppDbStdIf_createConfigFile(stdif, FALSE))
            {
//                IfxStdIf_DPipe_print(io, "Error board configuration could not be written"ENDL);
            }
        }

        result = TRUE;
    }
    else if (Ifx_Shell_matchToken(&args, "load") != FALSE)
    {
	    boolean isBoardVersionFile;
		boolean isConfigFile;

        isBoardVersionFile = AppDbStdIf_loadBoardVersionFile(stdif);
		isConfigFile = AppDbStdIf_loadConfigFile(stdif);

        if (!isBoardVersionFile)
        {
//            IfxStdIf_DPipe_print(io, "Error board version could not be load"ENDL);
        }

        if (!isConfigFile)
        {
//            IfxStdIf_DPipe_print(io, "Error board configuration could not be load"ENDL);
        }

        result = TRUE;
    }
	else if (!App_isLocked())
	{
	    LB_FileBoardVersion *boardVersion        = AppDbStdIf_getBoardVersion(stdif);
	    boolean              updateBoardVersion  = FALSE;

	    if (Ifx_Shell_matchToken(&args, "version") != FALSE)
	    { /* FIXME should be done globally */
	        Lb_BoardType type = Lb_BoardType_undefined;
	        uint32       version;
	        uint32       revision;
	        uint32       step = 0;

	        if (Ifx_Shell_matchToken(&args, "dbhp2") != FALSE)
	        {
	            type = Lb_BoardType_HybridKit_DriverBoardHp2;
	        }
	        else if (Ifx_Shell_matchToken(&args, "dbhpd") != FALSE)
	        {
	            type = Lb_BoardType_HybridKit_DriverBoardHpDrive;
	        }
	        else if (Ifx_Shell_matchToken(&args, "dbhpdsense") != FALSE)
	        {
	            type = Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSense;
	        }
	        else if (Ifx_Shell_matchToken(&args, "dbdsc") != FALSE)
	        {
	            type = Lb_BoardType_HybridKit_DriverBoardHpDriveEiceSenseDsc;
	        }

	        if (type != Lb_BoardType_undefined)
	        {
	            if ((Ifx_Shell_parseUInt32(&args, &version, FALSE) != FALSE)
	                && (Ifx_Shell_parseUInt32(&args, &revision, FALSE) != FALSE)
	                )
	            {
				    boolean isBoardVersionFile;
					isBoardVersionFile = AppDbStdIf_isBoardVersionFile(stdif);
	                Ifx_Shell_parseUInt32(&args, &step, TRUE);

	                if (!isBoardVersionFile)
	                {
//	                    IfxStdIf_DPipe_print(io, "Creating board version file system"ENDL);
	                    AppDbStdIf_createBoardVersionEfs(stdif, FALSE);
	                    isBoardVersionFile = AppDbStdIf_createBoardVersionFile(stdif, FALSE);
	                }

	                if (!isBoardVersionFile)
	                {
//	                    IfxStdIf_DPipe_print(io, "Error board version could not be written"ENDL);
	                }
	                else
	                {
	                    boardVersion->boardType    = type;
	                    boardVersion->boardVersion = (version << 16) | (revision << 8) | step;

	                    if (!AppDbStdIf_isConfigFile(stdif))
	                    {
//	                        IfxStdIf_DPipe_print(io, "Creating configuration file system"ENDL);
	                        AppDbStdIf_createConfigEfs(stdif, FALSE);
	                        AppDbStdIf_createConfigFile(stdif, FALSE);
	                    }



	                    updateBoardVersion  = TRUE;
	                }

	                result = TRUE;
	            }
	        }
	    }
	    else if (Ifx_Shell_matchToken(&args, "number") != FALSE)
	    {
	        uint32 value;

	        if ((Ifx_Shell_parseUInt32(&args, &value, FALSE) != FALSE))
	        {
	            boardVersion->number = (uint8)value;
	            updateBoardVersion         = TRUE;
	        }

	        result = TRUE;
	    }
	    else if (Ifx_Shell_matchToken(&args, "date") != FALSE)
	    {
	        uint32 year, month, day;

	        if (
	            (Ifx_Shell_parseUInt32(&args, &year, FALSE) != FALSE)
	            && (Ifx_Shell_parseUInt32(&args, &month, FALSE) != FALSE)
	            && (Ifx_Shell_parseUInt32(&args, &day, FALSE) != FALSE)
	            )
	        {
	            if ((month < 12) && (day < 31))
	            {
	                boardVersion->productionDate.year  = year;
	                boardVersion->productionDate.month = month;
	                boardVersion->productionDate.day   = day;
	                updateBoardVersion                 = TRUE;
	            }
	            else
	            {
//	                IfxStdIf_DPipe_print(io, "ERROR: invalid date"ENDL);
	            }
	        }

	        result = TRUE;
	    }
	    else if (Ifx_Shell_matchToken(&args, "test") != FALSE)
	    {
	        uint32 satus, year, month, day;

	        if (
	            (Ifx_Shell_parseUInt32(&args, &satus, FALSE) != FALSE)
	            && (Ifx_Shell_parseUInt32(&args, &year, FALSE) != FALSE)
	            && (Ifx_Shell_parseUInt32(&args, &month, FALSE) != FALSE)
	            && (Ifx_Shell_parseUInt32(&args, &day, FALSE) != FALSE)
	            )
	        {
	            if ((month < 12) && (day < 31))
	            {
	                boardVersion->TestStatus     = satus != 0;
	                boardVersion->TestDate.year  = year;
	                boardVersion->TestDate.month = month;
	                boardVersion->TestDate.day   = day;
	                updateBoardVersion                 = TRUE;
	            }
	            else
	            {
//	                IfxStdIf_DPipe_print(io, "ERROR: invalid date"ENDL);
	            }
	        }

	        result = TRUE;
	    }
	    /* FIXME missing "setup db clean" to erase the EEPROM as for LB */

	    if (result)
	    {

	        if (updateBoardVersion)
	        {
	            if (!AppDbStdIf_saveBoardVersion(stdif))
	            {
//	                IfxStdIf_DPipe_print(io, "Error board version could not be written"ENDL);
	            }
	            /* FIXME print info that the application should restart to take effect*/
	        }
	    }

	}


    return result;
}


boolean AppDbStdIf_isMinimalSetup(AppDbStdIf *stdif, IfxStdIf_DPipe *io)
{
    boolean error = FALSE;
    /* Check EEPROM*/

    if (Ifx_Efs_isLoaded(AppDbStdIf_getBoardVersionEfs(stdif)))
    {
        /* Check if files exist  */
        if (!AppDbStdIf_isBoardVersionFile(stdif))
        {
            error = TRUE;
//            IfxStdIf_DPipe_print(io, "WARNING: Driver board, File %s not found or file version not supported. Please create the file system with the command 'setup db version <type> <version> <revision> [<step>]'"ENDL, EFS_CFG_FILE_NAME_BOARD_VERSION);
        }
        else if (AppDbStdIf_getBoardVersion(stdif)->fileVersion != EFS_CFG_FILE_VERSION_BOARD_VERSION)
        {   /* Check files version */
            error = TRUE;
//            IfxStdIf_DPipe_print(io, "WARNING: Driver board, Version of file '%s'not supported. Please create the file system with the command 'setup db version <type> <version> <revision> [<step>]'"ENDL, EFS_CFG_FILE_NAME_BOARD_VERSION);
        }

        /* Check board type */
        if (!error)
        {
            if (!AppDbStdIf_isBoardTypeSupported(stdif))
            {
                error = TRUE;
//                IfxStdIf_DPipe_print(io, "WARNING: Driver board type not supported.'"ENDL);
            }
        }

        /* Check board version*/
        if (!error)
        {
            if (AppDbStdIf_isBoardVersionSupported(stdif))
            {}
            else
            {
                error = TRUE;
//                IfxStdIf_DPipe_print(io, "WARNING: Driver board version not supported."ENDL);
            }
        }
    }
    else
    {
        error = TRUE;
//        IfxStdIf_DPipe_print(io, "WARNING: Driver board, Board file system dose not exist. Please create the file system with the command 'setup db version <type> <version> <revision> [<step>]'"ENDL);
    }

    if (!error)
    {
        if (Ifx_Efs_isLoaded(AppDbStdIf_getConfigEfs(stdif)))
        {
            if (!AppDbStdIf_isConfigFile(stdif))
            {
                error = TRUE;
//                IfxStdIf_DPipe_print(io, "WARNING: Driver board, File %s not found or file version not supported. Please create the file system with the command 'setup db efs'"ENDL, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION);
            }
            else if (!AppDbStdIf_isConfigFileVersionActual(stdif))
            {   /* Check files version */
                error = TRUE;
//                IfxStdIf_DPipe_print(io, "WARNING: Driver board, Version of file '%s'not supported. Please create the file system with the command 'setup db efs'"ENDL, EFS_CFG_FILE_NAME_BOARD_CONFIGURATION);
            }
        }
        else
        {
            error = TRUE;
//            IfxStdIf_DPipe_print(io, "WARNING: Driver board, Configuration file system dose not exist. Please create the file system with the command 'setup db efs'"ENDL);
        }
    }

    return !error;
}
